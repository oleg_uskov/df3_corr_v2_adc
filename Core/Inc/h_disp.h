#ifndef __H_DISP_H
#define __H_DISP_H

//--------------------------------------------------------------------------
#define     rcommlcd  0x300
#define     rdatalcd  0x301
#define     lptsr     0x379     //p�����p �����
#define     base54    0x300     //������ p�����p ⠩��p�
//--------------------------------------------------------------------------
struct printform
{
    int     nwindow;    //⥪�饥 ���� ��� �뢮�� �� ��ᯫ��
    int     prirun;     //����p ��⪨ ��� �뢮��
    int     ligt;       //�p��� ᢥ祭�� �������p�
};
struct printform2
{   int     err;
    float   p;
    float   t;
    float   dp;
    float   q;
};
/*-----------------------------���⨯�-------------------------------*/
void clear_lcd();
void LCDputch(unsigned char);
void LCDputs(unsigned char *);
void LCDinit();
void LCDSendPos(char Sym,unsigned char Row,unsigned char Col);

void PrintForm0();
void PrintForm1();
void PrintForm2();
void PrintForm3(int);
void PrintForm4();
void PrintForm5();
void PrintForm6();
void PrintForm6E();
void PrintNeedCfg();
void PrintCurGas();
void LCDprint_80();
void LCDputcommand(unsigned char);
void LCDsetligt();
void LCDresligt();
void GlPrint();
void SetWindows();
void Kl_0();        //�⥭�� ���� ������ �� ࠡ�祬 室�
void Kl_1();        //�⥭�� ���� ������ �� ��横������� �� ���. ����.
void Kl_2();        //�⥭�� ���� ������ �� ����⪥
void LCDtest();
void Disp_SetErrForm2(int);
void Disp_ResErrForm2(int);

void MoveToHartSens(
   unsigned char Param,     // �����塞� ��ࠬ���
   unsigned char TypeRun,   // ����� ���稪��
   unsigned char Run);      // ����� ��⪨

void MoveTo485Sens(
   unsigned char Param,     // �����塞� ��ࠬ���
   unsigned char Run);      // ����� ��⪨

#endif//__H_DISP_H
