#ifndef __H_TIME_H
#define __H_TIME_H

#define PORT_DATA      0xFF74
#define PORT_DIRECTION 0xFF72
#define PORT_MODE      0xFF70
#define PORT_DATA1      0xFF7A
#define PORT_DIRECTION1 0xFF78
#define PORT_MODE1      0xFF76

#define CLOCK_WRT_BURST   0xD0
#define CLOCK_SECS      0x00
#define CLOCK_MINS      0x01
#define CLOCK_HRS       0x02
#define CLOCK_DATE      0x04
#define CLOCK_MONTH     0x05
#define CLOCK_DAY       0x03
#define CLOCK_YEAR      0x06
#define CLOCK_CONTROL   0x07

#define CLOCK_TRICKLE_CHARGER  0x08
#define CLOCK_FLAG 0x09
#define ACK               0
#define NOACK             1

#define MISC_PORT      0x1d5
//#define CLOCK              1
//#define ANALOG             2
#define READ               1

#define baseadr82C54  0x0310

#define MONO   1                        // ������� ������ �����������
#define STEREO 2                        // ������� ������ ������� - �������� RMG
//---------------------------------------------------------------------------
struct clock_image {
   unsigned char seconds;
   unsigned char minutes;
   unsigned char hours;
   unsigned char day;
   unsigned char date;
   unsigned char month;
   unsigned char year;
   unsigned char control;
};

typedef  struct bintime {
   unsigned char seconds;
   unsigned char minutes;
   unsigned char hours;
   unsigned char date;
   unsigned char month;
   unsigned char year;
} Time;

void CopyTime(void);                        // ����p������ � ��������� ��p����p� �p�����
void write_time(struct clock_image *address);
void read_time(struct clock_image *address);
void clk_stop(void);
void clk_start(void);
void conf_ds1340(void);
void ds1340_AB(void);
unsigned char read_byte(unsigned char ack);
void write_byte(unsigned char value);
void CopyTimeBCD_BIN(int n);            // ������ �� ��p���� BCD � BIN ����� p�������� �p�����
//void Ini53(void);                           // ������������� �����p�

void Time_SetNumrun(void);
void Time_SetCalcNumRun(void);
int  Time_GetNumrun(void);                  // ������ �������� ����p� �����
//int  Time_GetCalcNumrun(void);                  // ������ �������� ����p� �����
void Time_StartReadClock(void);

unsigned char   BinBCD(unsigned int);

void Time_SetTime(void);
void Time_SetFlagTime(void);                // ��������� ����� ��������� �p�����
void Time_ResFlagTime(void);                // ��p�� ����� ��������� �p�����
unsigned char  Time_GetFlagTime(void);
void Time_Ini82C54(void);
void Time_GetCount(void);
void Time_GetFirstCount(int);
void Time_SetCountData(int);               // ���������� ������ � ��p����p� �����

void Time_CountAlarmReset(void);            // ����� ������ ������ ���������

void Time_SetStartConfigFlag(void);
void Time_SetWiSoTime(void);                // ��p���� �� ������\������ �p���
void Time_ResFlagSomWi(void);

void Time_WatchdogReset(void);              // ��p�� ���p������� �����p�
void Time_WatchdogEnable(void);             // p��p������ ���p������� �����p�
void Time_WatchdogDisable(void);            // ���p�� ���p������� �����p�
void Time_WatchdogSetFlag(void);            // ��������� ����� ���p������� �����p�
void Time_WatchdogResFlag(void);            // ��p�� ����� ���p������� �����p�

void Time_CorrSecond(void);                 // �p��p������ ��pp����� �����
void Time_SetStartTime(int);
int  Time_GetStartTime(void);
void Time_SetStartFlag(void);
void Time_ResStartFlag(void);
int  Time_GetStartFlag(void);
/*
long  BinToSec(bintime tbin);
void SecToBin(long *tt, struct bintime *Bin);
void Time_CopyTimeBIN_BCD(struct bintime *pbintime,struct clock_image *pclockimage);

                                        // ��������������� ������ ��������� ����������
                                        // �� ��������������� �������� ������� ���������� �������� ������
void TimeCorrectionForDay(bintime* pbt, unsigned short contrHour);

void PrintTime(bintime* pbt);           // for debug - out to console

int read_analog(int channel);
                                        // ������-������ � CMOS-������
void LastWorkTime(char Direction, char far *buf);
*/
#endif  //__H_TIME_H
