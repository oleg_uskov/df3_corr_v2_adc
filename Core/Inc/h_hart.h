//  #ifndef __H_HART_H
//  #define __H_HART_H
#define  RTS_C3   0xFF7A        // RTS COM3
#define IMASK   0xFF28     // ॣ���� ��᪨ ���뢠��� ���⮢, Int0-INT6
#define LHART 24
#define LHARTCOM 17 // ����� ������� �⥭�� HART-���稪��
#define LMAXIN    64   // p����p ���p� �p����
#define LMAXOUT   64   // p����p ���p� ��।��
//---------------------------------------------------------------------------
struct hart_data
  { unsigned char   ed;     //��� ������� ����p����
    float           val;    //���祭�� 䨧��᪮� ����稭�
  };
//---------------------------------------------------------------------------
/*
union hart_answer
{   hart_data       HART_DATA;
    unsigned char   data[5];
};
*/
//---------------------------------------------------------------------------
struct  hartdata
{   unsigned char num;     // ���-�� ����p塞�� ��p����p��(1 ��� 3)
    unsigned char type;    // 1-⥬��p���p�, 2-��������, 4-��p�., 8-����.��p����
                           // ��� 2-� � 3-� ��ࠬ����᪨� ���稪��
                           // dP+P   - 6
                           // P+T    - 3
                           // dP+t   - 5
                           // P+dP+T - 7
    unsigned char run;     // ����p ��⪨ (0...2)
    unsigned char hartadr[5];

    unsigned char stat0;   // HART-����� ���稪�
    unsigned char stat1;

    unsigned char edp;     // ��� ������� ����p���� 1-� ��p�������
    unsigned char eddp;    // ��� ������� ����p���� 2-� ��p�������
    unsigned char edt;     // ��� ������� ����p���� 3-� ��p�������
    unsigned char edtm;     // ��� ������� ����p���� 4-� ��p�������

    float         valp;    // ���祭�� 䨧��᪮� ����稭� 1-� ��p�������
    float         valdp;   // ���祭�� 䨧��᪮� ����稭� 2-� ��p�������
    float         valt;    // ���祭�� 䨧��᪮� ����稭� 3-� ��p�������
    float         valtm;    // ���祭�� 䨧��᪮� ����稭� 4-� ��p�������
    unsigned char cheksum; // �訡�� ����p��쭮� �㬬�
    unsigned char timeaut; // ���稪 ���⢥⮢ �� ���稪�

    unsigned char DiaT;    // ���稪 ��室�� �� �࠭��� T
    unsigned char DiaP;    //
    unsigned char DiadPh;  //
    unsigned char DiadPl;  //

    unsigned char eredT;   // ���稪 �訡�� ������ ����७�� T
    unsigned char eredP;   //
    unsigned char ereddPh; //
    unsigned char ereddPl; //

    unsigned char statt;   // ����� �訡�� �� ������ T
    unsigned char statP;   // 2 - �訡�� ⠩����
    unsigned char statdPh; //
    unsigned char statdPl; //

    unsigned char SensCfgAlarm; // �ਧ��� ������ ���ਨ �� ��������� ���䨣��樨 ���稪��
    unsigned char SensCfgReset; // �ਧ��� ���뫪� ��� ��������� ���䨣��樨 ���稪��
    unsigned char rNRep;   // ⥪�饥 �᫮ ����������� ����஢
    unsigned char HartChan;   // ����� HART-������ /1,2/
    unsigned char tdP;     // ==0 ��� ����������� dP
};
//---------------------------------------------------------------------------
void Set_Chip8251();
void IniHart1();                           // ���-�� ����-� ������� ⠩��p� � p����� p����� �p������p����稪�
void HartInitRes ();                    // ���樠������ �p���� �������� ���p�
void HartInitTrans ();                  // ���樠������ ��p���� �������� ���p�
void Int_Com_In_Hart();                 // �p��� ���� � ����� - � ⥫� �p�p뢠���
void Int_Com_Out_Hart();                // ��p���� �������� ���p� - � ⥫� �p�p뢠���
//void interrupt Int09(...);              // ��ࠡ��稪 ���뢠��� ���-3
void HartIni();                         // ���樠����p�� ��p����� ���p�� ������ � ���稪��
void HartIni_Hartservice();             // ���樠����p�� ���p��-��।��� � ०��� HART<->COM
void HartIni_Hartservice_1();           // ���樠����p�� ���p��-��।��� � ०��� HART<->COM
void Result_Hart();                     // ������ �p���⮣� ���p� �� HART ������
void ResultHart_Hartservice();          // ������ �p���⮣� ���p� � ०��� HART<->COM
void ResultHart_Hartservice_1();        // ������ �p���⮣� ���p� � ०��� HART<->COM
void HartStop();
void SetHartDataAdr(int);                  // �p��p-� ᯨ᪠ ���稪�� �� ���䨣-� ��������
void HartDataWr();                      // ������� ����p����� ���祭��
float Char4ToFloat(unsigned char*);
void Hart_SetHartData(int k);                // ���������� ������ ��� ��⪨ �� ��p��. ������ ���稪�
void Hart_SetHartErr();
void Hart_Watch();                      // ��� ����� � ����ᠭ�ﬨ ����㭨��樨 �� HART

int GetEdErr(int nRun);                 // �뤠�� ����稥 ��� �� ����� �訡�� ������� ����७��

                                        // ����஫� ������ ����७��
int ChkEdT(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);
int ChkEdP(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);
int ChkEddPh(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);
int ChkEddPl(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);
                                        // ����஫� ���������� ���祭��
int ChkDiaT(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);
int ChkDiaP(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);
int ChkDiadPh(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);
int ChkDiadPl(struct IdRun *prun, struct AdRun *aprun, struct hartdata *ph);

void ChkSensCfgChange(int);            // �஢�ઠ ��������� ���䨣��樨 ���稪�
int GetDpMin();                         // ����� ������ �࠭��� ��९���
int GetDpMinShelf();                    // �ਧ��� "�����" �� dPmin

int GetNRep();                          // ����.�᫮ ������.����஢ 14.02.05

void SetHartOut() ;
//---------------------------------------------------------------------------

// #endif\\__H_HART_H
