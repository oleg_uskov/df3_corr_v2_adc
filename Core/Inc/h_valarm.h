#include "h_mem.h"
//----------------------------------------
struct alarm {
   unsigned int  Code;                  // ��� ���ਨ
   unsigned char Flag;                  // �ਧ��� ������
   unsigned char Mask;                  // ��᪠ - 0 - �� ���뢠��
};

//----------------------------------------
struct min_count {
   unsigned char Flag;                  // �ਧ��� ��� ���樨 � QminFlag
   struct bintime   StartTime;            // ���-�६� ��砫� ���ਨ
   struct bintime EndTime;              // ���-�६� ���� ���ਨ
};

//----------------------------------------
struct valarm_count {
   unsigned char Flag;                  // �ਧ��� ���਩��� ���権
   struct bintime StartTime;            // ���-�६� ��砫� ���ਨ
   struct bintime EndTime;              // ���-�६� ���� ���ਨ
   float Vstart;                        // ��ꥬ �� ��砫� ���ਨ
   float Vend;                          // ��ꥬ �� ����� ���ਨ
   float Vstart_r;                      // ��ꥬ �� ��砫� ���ਨ �.�.
   float Vend_r;                        // ��ꥬ �� ����� ���ਨ �.�.
};

//----------------------------------------
struct valarm_data {                    // 32 ����
   struct bintime  STARTTIME;           // ���-�६�

   float VTotSt;                        // �ਢ������ ��ꥬ �� ��� ������
   float VTotWc;                        // ࠡ�稩 ��ꥬ �� ��� ������
   float VAddSt;                        // ���������� �ਢ������ ��ꥬ ��� ���稪�

   long  TTot;                          // ���⥫쭮��� ��� ���਩ �� ��⪨, �஬� ��⠭��
   long  Toff;                          // ���⥫쭮��� �⪫�祭�� ��⠭��
   long  Tqmin;                         // ���⥫쭮��� ���� �� Qmin,Pmin,dPmin
                                        // (�� ���਩���)
   unsigned int Crc;                    // ����. �㬬� �����
};
//----------------------------------------
//int _fastcall ValarmDataRd (unsigned int num,unsigned char numpage,struct valarm_data *);
//int _fastcall ValarmDataRd_1 (unsigned int num,unsigned char numpage);
unsigned int ValarmDataSearchStart(unsigned char numpage,struct startendtime *);
unsigned int ValarmDataSearchStart2(unsigned char numpage,struct startendtime *);
unsigned int ValarmDataSearchEnd(unsigned char numpage,struct startendtime *);
//unsigned int ValarmDataSearchStart_1(unsigned char numpage);
//unsigned int ValarmDataSearchStart2_1(unsigned char numpage);
//unsigned int ValarmDataSearchEnd_1(unsigned char numpage);
unsigned int NextIndexValarm(unsigned int start, unsigned int index, int Page);
//unsigned int NextIndex_1Valarm(unsigned int start, unsigned int index, int Page);


//unsigned char _fastcall GetValarmPage(unsigned int numrun);
void WriteValarmData(int Page,int nRun);    // ������ ���਩��� ��ꥬ�� � ��娢
void SetValarmtdata();                  // ������ ���਩��� ��ꥬ�� � �㬬���
void ValarmIni();                       // ���樠������ ��� ���਩
                                        // ���������� ���稪�� ���਩��� ��ꥬ��
void SetValarmCount(unsigned int Code, int nRun, float Volume);
void ChkValarmSumm();                   // �஢�ઠ �㬬��஢ � �����⪠ 墮�⮢
void ValarmSummIni(struct bintime B,int nRun);  // ���樠������ �㬬��஢ ���਩��� ��ꥬ��
long GetContrHSec (struct bintime);     // ����ࠪ⭠� ��� � ᥪ㭤��
void PowerOffCorrection();              // ���४�� ���⥫쭮�� �몫�祭��
void CorrectAlarmTables();              // ���४�� ⠡��� �� �ਧ���� Qmin

void ClearValarmArc(char empr);                  // ��⪠ ��娢�� ���਩��� ��ꥬ��
void ClearValarmSumm(char);                 // ��⪠ �㬬��஢ ���਩��� ��ꥬ��

void ChkQminFlag();                     // ॠ��� �� �����⠭���� 䫠���
unsigned char GetValarmFlag(int nRun);  // c㬬��� �ਧ��� ���਩
unsigned char GetVminFlag(int nRun);    // c㬬��� �ਧ��� �६��� �� Qmin

void ValarmSummCorrMin(int nRun);
