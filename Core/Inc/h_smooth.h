
#ifndef _SMOOTH_H_
#define _SMOOTH_H_

#define     QBUFLEN   10               // ����� ������

struct new_count {
   int      tcas;                       // ��������� �������� 1 ��� (� �������� ��� �������)

   unsigned interval_min,               // ��������, ��������������� ���. �������
            interval_max;               // ��������, ��������������� ����. �������

   float    dem;                        // ����-� �������������

   unsigned Kimp_n,                     // �-�� ��������� �� ������� �����
            Kimp_n1;                    // ���������� ��������� �-�� ���������
   unsigned long     Tn,                         // ����, ��������������� �imp_n
            Tn1,                        // ����, ��������������� �imp_n1
            Tt;                         // ������� ���� � ��������

//   unsigned AKimp_n[QBUFLEN];           // �-�� ��������� �� ������� �����
   float ATn[QBUFLEN];               // ����, ��������������� �imp_n
   float  AKimp_n[QBUFLEN];


   int RBLen;                           // ����� ����������� ����� ������
   float ImpSumm,                        //long ����� ��������� � ������
        TimeSumm;                       // ����� ��� ���������
   float ImpSummAverage;                // ������� �� ������

   unsigned char pr1ob;                 // ������� ������� ���y������ ����������
   unsigned char pr1ob_0;               // ������� ������� �������� ����������


   float    interval,                   // �������� ����� ������� � ��������� �-��� ���������
            interval_1;                 // ����������� ��������

   long     j,                          // ������� �������� ���������
            jj;                         // ������� ���������� �������� ���������

   double    Q1,                         // ������ - ��������� ����������
            Q;                          // ������ - �������� ����������

   float    Q1Raw,                      // �� �� - ��� �����������
            QRaw;

   unsigned char CountAlarm;            // ������� ������
};

void Disp_count_0(unsigned char Run);
void Disp_count_1(unsigned char Run);
void RashCalc(unsigned char, float);

//float GetQw(int nRun);                  // �������� ������ ��� �������� ������
float GetRealQw(int nRun);              // �������� ������ ��� �������� ������ ��� ����� ������ �� ���������
double GetQ(int nRun);                   // �������� ������ ��� ������������ ������
float GetQw3(int nRun);
float GetQw4(int nRun);
                                        // �� �� ������� ��� ������� ��� �����������
double GetQwRaw(int nRun);               // �������� ������ ��� �������� ������
float GetRealQwRaw(int nRun);           // �������� ������ ��� �������� ������ ��� ����� ������ �� ���������
float GetQRaw(int nRun);                // �������� ������ ��� ������������ ������

#endif
