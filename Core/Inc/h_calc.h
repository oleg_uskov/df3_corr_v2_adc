//#ifndef __H_CALC_H
//#define __H_CALC_H

#define ROmin  0.549f
#define ROmax  1.101f

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
struct calcdata {
   int typeOtb;                         // 0-�������, 1-���������
   int typeP;                           // 0 - ����������, 1 - ����������
   unsigned char TypeCount;             // ��� 0: ��50\GERG\NX19 [0\1]
                                        // ��� 1: ��p-const\��p-p���. [0\1]
   int run;                             // ����� �����
   float   Dt20;                        // ������� ������������ ��� �.�., ��
   float   Dd20;                        // ������� ��������� ��� �.�., ��
   float   R;                           // ���. ������������� ������������, ��
   float   Bd;                          // ����-�� ���-�� ����-� ��������� ���������, ���^-1
   float   kAd;                         // ����-�� A
   float   kBd;                         // ����-�� B
   float   kCd;                         // ����-�� C
   float   Bt;                          // ����-�� ���-�� ����-� ��������� �p���, ���^-1
   float   kAt;                         // ����-�� A
   float   kBt;                         // ����-�� B
   float   kCt;                         // ����-�� C
   double   DP;                          // ������� ��������, ���/�2
   float   Pin;                         // ���. ��� �����. �������� �� ����� �p��p����, ���/��2
   float   Pb;                          // ��������������� ��������, ���/c�2
   float   Pa;                          // ���. ��������, ���/��2
   float   t;                           // �����������, �p. �������
   float   ROnom;                       // ��������� ���� ��� �.�., ��/�^3  // if==0, calcRasxod(void) break && return -1 !!!
   float   NN2;                         // ����p��� ���� ����� � ������� ����� �p� ������p���� ��������, %
   float   NCO2;                        // ����p��� ���� �������� ����p��� � ������� ����� �p� ������p���� ��������, %
   float   Heat;                        // �������� �������������� ������� �����
   double   FullHeat;                    // ������ �������������� ������� �����
   double   V;                           // ����� ���� �p� p������ ��������
   double   Qw;                          // p����� �p� p.�., �3/���
/*-------- �p����������� � �������� ��������� ������ ----------------------*/
//   float   Qz;                          // ������ �p� Re = 10e6;
   double   Q;                           // ������, ����-��, �^3/���
   float   Ktt;                         // ���p������� ��������� �� ���-�� ����. ���-�� ��-��
   float   Ktd;                         // ���p������� ��������� �� ���-�� ����. ���-�� ����p-��
   float   Dt;                          // ������� ������������ ��� �.�., ��
   float   Dd;                          // ������� ��������� ��� �.�., ��
   float   mod;                         // ������ ��������� ��� �.�.
   float   T;                           // �����������, �p. ��������
   double   K;                           // ����������� �����������
   double   Mu;                          // ������������ ��������, ���*c/�2
   double   capa;                        // ���������� ��������
   double   Eps;                         // ����-�� ����������
//   float   Aks;                         // ������������� ������������� ������p�����
   double   Ksh;                         // ������-� ����-�� �� ���-��� ��-��
   double   Kp;                          // ������-� ����-�� �� �������-� ��. ������ �-��
                                        // ��� ����� ����� �p� p.�. ��� ��������
//   float   Alpha0;                      // ����-�� ������� ��� Re=10e6
//   float   Alpha;                       // ����-�� �������
//   float   Rez;                         // ����� ����������
   double   Re;                          // ��������-� ����� ����������
//   float   kRe;                         // ����-�� ��������� �� ����� ����������
   double   sqrtQ;
//   float   Vnom;                        // ����� ���� �p� ��p������� ��������
   float   Beta;
   float   Beta2;
   float   Beta4;
   float   Beta8;
//--------���� ��������������� ������, ������������ �p� p������--------------
//   float   sqmod;                       // ����p����� ��p��� �� ������
   float   mod2;                        // ����p�� ������ ��������� ��� �.�.
   float   Ppkr;                        // ����������������� �������� (66)
   float   Tpkr;                        // ����������������� ����������� (67)
//   float   Ppr;                         // ����������� ��������
//   float   Tpr;                         // ����������� �����������
//   float   Pc;
//   float   Tc;
//   float   B0;
//   float   B1;
//   float   B2;
//   float   CRe;
//   float   BRe;
//   float   Q1;
//   float   Q2;
//   float   F;
   float   kQind;                       // ������������� ���������� � ����������� Q ��� ���������
//   unsigned int err[12];                // ���� ������ �������
   float Qw_s;
//--------������ ��� p������ �� GERG-91--------------------------------------
   float   ROc;                         // ��������� ���� � ������p���� ��������, ��/�^3
   float   H;                           // �������� ��������������
   float   Xa;                          // ����p��� ���� ����� � ������� �����
   float   Xy;                          // ����p��� ���� �������� ����p��� � ������� �����
   float   Zc;                          // �����p ����������� �p� ������p���� ��������
   float   Z;                           // �����p ����������� �p� p������ ��������
//   float   C1;
   float   Bm;                          // ����. �p������� ���������
   float   Cm;                          // ����. �p������� ���������
   float   Kgerg;                       // ����. ����������� �� GERG-91
//---------------- ��� ���� -------------------------------------------------
   float taupp;                         // ����� ����������� �������� - �������� � ����-�
   float Rn;                            // ����� ����������� �������� - �������� � ����-�

   float p_T;                           // ��������� �� � �
   float R0;                            // ������������� ����������� ������
   float ROmk1;                         // ��������� � ������� ��������
//   float Ksh_;                          //
   float Rk;                            //
   double C_;                            // ����������� ���������
   double E;                             // ����������� �������� �����
   double Q_;                            // ������ �� �������

//   struct bintime  CALCTIME;            // �������� ��p����p� �p�����
};                                 //     5+12+6+4*88=375*3 = 1125 ����
//------------------------------------------------------------------------
/*
struct intpar {
   unsigned char HartNumCyclRep;                 // ����� ������ ������� ��� ���
   unsigned char HartNumImmedRep;                // ����� ����������� ��������
   unsigned char HartReqDelay;                   // ���.�������� ����� �������� HART
   unsigned char RS485Com5Speed;                 // �������� �� COM5 - RS485  ���
   unsigned char Com4Speed;                      // �������� �� �OM4 - RS485  ���
   unsigned char Com2Mode;                       // ��������� � ������, 0-���
   unsigned char Com1NoWrite;                    // ������ ������ �� ���1
   unsigned char OdoMask;                        // ����� ����������
   unsigned char CntBufLen;                      // ����� ������ �������� ���������
   unsigned char CntAddBufShift;                 // ����� ���.������
   float VolumeDelta;                   // ���������� ����������� �� ������
   unsigned char ChannelJoin;                    // ������� ����������� ������
   unsigned char QminFlag;                       // ������� �������� �� ����������� ������
   int  AlarmVolumeMinTime;             // �����. ����� ��� ���������� ������
   unsigned char FullNormVolume;                 // � ����� ���� ����� ��� ������������
   unsigned char Keyboard;                       // ������� ����������   ASCII - RTU
   unsigned char HeatReport;                     // ����� � ������
   unsigned int  crc;
};
*/
//-----------------------------------------------------------------------------
unsigned int    Testcalcdata(struct calcdata*);// ����p��� ��p����p��
void            Calc_KtPoli(struct calcdata*); // p����� �����p-�� ����. ��������� p����p����
unsigned int    CalcDiam(struct calcdata*);    // p����� ������p�� � ������
unsigned int    CalcKsh(struct calcdata*);     // p����� ���p�������� ��������� �� ��p����������
unsigned int    CalcCoeffd(struct calcdata*);  // p����� ������������ ����p����
unsigned int    CalcKp(struct calcdata*);      // p����� ���p�������� ��������� �� �p��������� ������� �p����
unsigned int    CalcCoeffK(struct calcdata*);  // p����� ������������ ����������� ������ �p�p����� ����� �
void            CalcAdiab(struct calcdata*);   // p����� ���������� �������� ����
unsigned int    CalcEps(struct calcdata*);     // p����� ������������ p����p���� ����
unsigned int    CalcMu(struct calcdata*);      // p����� ������������ ��������
unsigned int    CalcQz(struct calcdata*);      // p����� p������ �p� Re = 10e6
unsigned int CalcRe(struct calcdata*);         // ��p�������� ����� ����������
                                        // p����� ������������ ����������� ������ �p�p����� ����� �
unsigned int CalcCoeffK_GERG_91(struct calcdata*);
void CopyDataGerg91(struct calcdata*);         // ����p������ �������� ������ ��� p������ �� ������ GERG-91
void K_Gerg91_1(struct calcdata* uk);
void TaskCalc_Gerg91_1(void);

void Emulator(struct calcdata*);
//void PrintParam(void);
//void PrintStream(void);
void Calc(void);

void SetCalcData(void);
void SetCopyCalcData(unsigned char);
void SetCopyCalcData_1(unsigned char);
void Calc_CorrT(struct calcdata*);
unsigned int Calc_CorrQnom(struct calcdata*);
void Calc_CorrSetCalcData(void);
void Calc_AdiabGOST30319_1(struct calcdata*);
unsigned int Calc_MuGOST30319_1(struct calcdata*);
void Calc_CorSetNoErr(int);
void Calc_SetNoErr(void);
void Calc_SetNoErrTypeOtb(void);
unsigned int* Calc_GetPointBuferr(int nRun);

unsigned int Calc_CorTestInDataAlarm(int nRun);
unsigned int Calc_CorrTestInData(int nRun);

unsigned int Calc_dDBetaE(struct calcdata*);
unsigned int Calc_KnPT(struct calcdata*);
unsigned int Calc_PT(struct calcdata*);
unsigned int Calc_RO(struct calcdata*);
unsigned int Calc_Miu(struct calcdata*);
unsigned int Calc_Kapa(struct calcdata*);
unsigned int Calc_Epsilon(struct calcdata*);
unsigned int Calc_Eps2(struct calcdata*);


unsigned int Calc_Q(struct calcdata*);
unsigned int Calc_Re(struct calcdata*);

unsigned int Calc_C(struct calcdata*);
unsigned int Calc_K_sh_1(struct calcdata*);
unsigned int Calc_K_sh_2(struct calcdata*);
unsigned int Calc_K_sh_3(struct calcdata*);
unsigned int Calc_Q1(struct calcdata*);
unsigned int Calc_Re1(struct calcdata*);
float CalcHeat(int nRun);
//#endif //__H_CALC_H
