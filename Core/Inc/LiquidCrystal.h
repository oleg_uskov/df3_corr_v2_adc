

#ifndef LiquidCrystal_h
#define LiquidCrystal_h

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

// enum for
#define LCD_DRAM_Normal 0x00
#define LCD_DRAM_WH1601 0x01
// low-level functions
void send(uint8_t, GPIO_PinState);

void write_bits(uint8_t);
void pulseEnable(void);

// initializers
void LiquidCrystal(GPIO_TypeDef *gpioport,
  uint16_t d0, uint16_t d1, uint16_t d2, uint16_t d3,
  uint16_t d4, uint16_t d5, uint16_t d6, uint16_t d7);

void init(uint8_t fourbitmode, GPIO_TypeDef *gpioport,
    uint16_t d0, uint16_t d1, uint16_t d2, uint16_t d3,
    uint16_t d4, uint16_t d5, uint16_t d6, uint16_t d7);
  
void begin(uint8_t cols, uint8_t rows);


void enableClock(void);

// high-level functions
void clear(void);
void home(void);

void noDisplay(void);
void display(void);
void noBlink(void);
void blink(void);
void noCursor(void);
void cursor(void);
void scrollDisplayLeft(void);
void scrollDisplayRight(void);
void leftToRight(void);
void rightToLeft(void);
void autoscroll(void);
void noAutoscroll(void);
size_t print(const char []);

void setRowOffsets(int row1, int row2, int row3, int row4);
void createChar(uint8_t, uint8_t[]);
void setCursor(uint8_t, uint8_t); 
size_t _write(uint8_t);
void command(uint8_t);
void delay_micros(uint32_t us);
void LCD_build(uint8_t location, uint8_t *ptr);
void ConvertSymToDisplayCode(char sym);

void setDRAMModel(uint8_t);

uint8_t _displaymode;
uint8_t _dram_model,_displaycontrol;
uint8_t utf_hi_char; // UTF-8 high part





#endif
