/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
struct addrTemper {
	uint8_t addrDataTemper   [2];
	uint8_t addrConfigTemper [2];
};
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
int Runsensor(void);
void RunCalculate();
void RunHart(void);
void InitSensor(void);
void HART_start(void);
void HART_stop(void);
float Temperature1(void);
float Temperature2(void);
float PressIn(void);
float PressOut(void);
float CodePIn(void);
float CodePOut(void);
void ReadCalibrADC_FromFlash();
void WriteCalibrADC_ToFlash();

//unsigned cntADC1();
//unsigned cntADC2();
//unsigned cntADC3();

void ConsolePrint(char *, int);
void CalculateTemper(void);
void HART_FinishRx(void);
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define FL_2_EXTI_4_Pin GPIO_PIN_4
#define FL_2_EXTI_4_GPIO_Port GPIOE
#define FL_2_EXTI_4_EXTI_IRQn EXTI4_IRQn
#define FL_1_EXTI_5_Pin GPIO_PIN_5
#define FL_1_EXTI_5_GPIO_Port GPIOE
#define FL_1_EXTI_5_EXTI_IRQn EXTI9_5_IRQn
#define FL_1HF_EXTI_1_Pin GPIO_PIN_1
#define FL_1HF_EXTI_1_GPIO_Port GPIOA
#define FL_1HF_EXTI_1_EXTI_IRQn EXTI1_IRQn
#define ADC3_Pin GPIO_PIN_2
#define ADC3_GPIO_Port GPIOA
#define ADC2_Pin GPIO_PIN_3
#define ADC2_GPIO_Port GPIOA
#define ADC1_Pin GPIO_PIN_4
#define ADC1_GPIO_Port GPIOA
#define K1_out_Pin GPIO_PIN_0
#define K1_out_GPIO_Port GPIOB
#define U3_RTS_Pin GPIO_PIN_1
#define U3_RTS_GPIO_Port GPIOB
#define K2_out_Pin GPIO_PIN_2
#define K2_out_GPIO_Port GPIOB
#define K3_out_Pin GPIO_PIN_7
#define K3_out_GPIO_Port GPIOE
#define K4_out_Pin GPIO_PIN_8
#define K4_out_GPIO_Port GPIOE
#define K1_in_Pin GPIO_PIN_9
#define K1_in_GPIO_Port GPIOE
#define K2_in_Pin GPIO_PIN_10
#define K2_in_GPIO_Port GPIOB
#define K3_in_Pin GPIO_PIN_11
#define K3_in_GPIO_Port GPIOB
#define K4_in_Pin GPIO_PIN_12
#define K4_in_GPIO_Port GPIOB
#define D0_Pin GPIO_PIN_8
#define D0_GPIO_Port GPIOD
#define D1_Pin GPIO_PIN_9
#define D1_GPIO_Port GPIOD
#define D2_Pin GPIO_PIN_10
#define D2_GPIO_Port GPIOD
#define D3_Pin GPIO_PIN_11
#define D3_GPIO_Port GPIOD
#define D4_Pin GPIO_PIN_12
#define D4_GPIO_Port GPIOD
#define D5_Pin GPIO_PIN_13
#define D5_GPIO_Port GPIOD
#define D6_Pin GPIO_PIN_14
#define D6_GPIO_Port GPIOD
#define D7_Pin GPIO_PIN_15
#define D7_GPIO_Port GPIOD
#define CARD_CS_Pin GPIO_PIN_6
#define CARD_CS_GPIO_Port GPIOC
#define CARD_Pin GPIO_PIN_7
#define CARD_GPIO_Port GPIOC
#define CARD_VCC_Pin GPIO_PIN_8
#define CARD_VCC_GPIO_Port GPIOC
#define RS_LCD_Pin GPIO_PIN_9
#define RS_LCD_GPIO_Port GPIOC
#define E_LCD_Pin GPIO_PIN_8
#define E_LCD_GPIO_Port GPIOA
#define U1_RTS_Pin GPIO_PIN_12
#define U1_RTS_GPIO_Port GPIOA
#define LED_Pin GPIO_PIN_3
#define LED_GPIO_Port GPIOD
#define U2_RTS_Pin GPIO_PIN_4
#define U2_RTS_GPIO_Port GPIOD
#define GERCON_Pin GPIO_PIN_7
#define GERCON_GPIO_Port GPIOD
#define SPI3_CS_Pin GPIO_PIN_8
#define SPI3_CS_GPIO_Port GPIOB
#define check_220_Pin GPIO_PIN_1
#define check_220_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */
#define PER  1
#define HOU  2
#define DAY  3
#define ALR 4
#define AUD 5
#define SECR 6
#define AVOL 7
#define SEC  8
#define BASEADR 4000 

struct pointARch
{    uint16_t di1p[3];  //dip[3][2]
     uint16_t di2p[3];
     uint16_t di1h[3];
     uint16_t di2h[3];
     uint16_t di1d[3];
     uint16_t di2d[3];
     uint16_t di1Al[3];
     uint16_t di2Al[3];
     uint16_t di1Au[3];
     uint16_t di2Au[3];
     uint16_t di1Scr[3];
     uint16_t di2Scr[3];
     uint16_t di1AV[3];
     uint16_t di2AV[3];
     uint16_t di1s[3];    
     uint16_t di2s[3];
};

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
