#ifndef __H_MAIN_H
#define __H_MAIN_H
#define SEGMENT 0xF800
#define PER  1
#define HOU  2
#define DAY  3
#define ALR 4
#define AUD 5
#define SECR 6
#define AVOL 7
#define BASEADR 4000 
#define ArchSize  (sizeof (struct memdata))
#define BUTMAX  20 //������������ ������� ������ � 5��  (100 ��)

//---------------------------------------------------------------------------
typedef void (*pf)(void);
//---------------------------------------------------------------------------

typedef struct OdoStrucTag {            // ��������� ��� ������ ���������
   int Nimp;                            // ����� ���������
   int ImpLen;                          // ����� ��������
   int StartTaktList[10];               // ��������� ����� ���������
   int EndTaktList[10];                 // �������� ����� ���������
} OdoStruc;

struct compress {
float K;
float Z;
};

#define     startdelay    3             // �p��� ����p��� � ��������
//---------------------------------------------------------------------------
int  InQueue(int,pf);                   // ������� ���������� � ���p���
int  InQueueInt(int,pf);                // �. ���������� � �p��� ����p������ �p�p������

int  InTimeQueue(unsigned int,int,pf);  // ���������� � ���p��� � �����p�                               /
//void interrupt time(...);               // ��p������� �p�p������ �����p�
//void interrupt NewNmi(...);             // ��p������� NMI
void SetTaskTakt(void);                     // ����������� � ������ ����� p����� �p���p�
void TaskSetCalcData(void);
void TaskCalcDiam(void);
void TaskTestcalcdata(void);                // ����p��� ������� ��p����p��
void TaskTestcalcdata586(void);             // ����p��� ������� ��p����p��
void TaskCalcKsh(void);
void TaskCalcKp(void);
void TaskCalcCoeffd(void);                  // p����� ������������ p������ ����p����
int TaskCalcCoeffK(void);
void TaskCalcAdiab(void);
void TaskCalcMu(void);                      // p����� ������������ ��������
void TaskCalcQz(void);                      // p����� p������ �p� Re = 10e6
void TaskCalcRe(void);
void TaskCalcEnd(void);
void Task_Calc_CorrSetCalcData(void);
int Task_Calc_CorrCoeffK(void);
void Task_Calc_CorrQnom(void);
void Task_Calc_CorrEnd(void);
void Task_Calc_CorrTestInData(void);
void GetTakt(void);
void NulAddCfg(void);
void TaskCalc_K_sh2_32(void);
void TaskCalc_GOST_8_586(void);             // �������� ������ ��� ����

void TaskCalc_dDBetaE(void);                // ������ �-�� �����������
int TaskCalc_Kn(void);
void TaskCalc_PT(void);
int TaskCalc_K_8_586G(void);
void Task_Gerg91_2(void);
void Task_Gerg91_22(void);
void Task_Gerg91_3(void);
int TaskCalc_K_8_586N(void);
void TaskCalc_RO(void);
void TaskCalc_Miu(void);
void TaskCalc_Kapa(void);
void TaskCalc_Epsilon(void);
void TaskCalc_Eps2(void);

void TaskCalc_C1(void);                     // ������ ������� � 4 ����
void TaskCalc_K_sh1_1(void);
void TaskCalc_K_sh1_2(void);
void TaskCalc_K_sh1_3(void);
void TaskCalc_Q1(void);
void TaskCalc_Re1(void);

void TaskCalc_C2(void);
void TaskCalc_K_sh2_1(void);
void TaskCalc_K_sh2_2(void);
void TaskCalc_K_sh2_3(void);
void TaskCalc_Q2(void);
void TaskCalc_Re2(void);

void TaskCalc_C3(void);
void TaskCalc_K_sh3_1(void);
void TaskCalc_K_sh3_2(void);
void TaskCalc_K_sh3_3(void);
void TaskCalc_Q3(void);
void TaskCalc_Re3(void);

void TaskCalc_C4(void);
void TaskCalc_K_sh4_1(void);
void TaskCalc_K_sh4_2(void);
void TaskCalc_K_sh4_3(void);
void TaskCalc_Q4(void);
void TaskCalc_Re4(void);
void TaskWriteArcData(void);                // ������ �������� ������
void CopyKoef(void);
void RS485FBIni(int,char);
void QuiFBIni0(void);
void QuiFBIni1(void);
void QuiFBIni2(void);
void HartIni1(void);
void HartIni2(void);
void HartIni3(void);
#endif//__H_MAIN_H
