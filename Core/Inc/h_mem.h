// Day - 744; Hour,Oper - 1488; Cycle - 744; Valarm - 511, Audit - 948; alarm-1152
#ifndef __H_MEM_H
#define __H_MEM_H
#include <stdint.h>
#include "h_time.h"
#include "h_analog.h"
#define   KOL_RUN        3         // ���������� �����, ����. ������������
#define   RUN_ARC_LEN        15         // ����� ������ ����� ����� � ���������

#define   ARC1              372         // ������� � ����� ��������
#define   ARC2              744
#define   ARC3             1116
#define   ARC4             1488         // ������� � ������� ���������
#define   ARCAL            1152         // ������� ������
#define   ARCAU            948         // ������� ������������

#define   VALARM_PAGE      0x90         // ���.�������� ��������� �������
#define   V_ALARM           511         // ������� ��� �����

#define   BASE_CFG_PAGE    0x80         // �������� �������� ������������
#define   RESERV_CFG_PAGE  0x81         // �������� ��������� ������������
#define   SECURITY_PAGE    0x81
#define   ALARM_PAGE       0x82         // ���.�������� ������ ������
#define   AUDIT_PAGE       0x83         // ���.�������� ������ ������������
#define   ARCH_SECURITY     600         // ������� � ������ ������������
#define   LEN_SECURITY     18           // ����� ����� ������ ������ �����.
#define   BEGADDR_SECURITY 5270         // ���������  ����� ������ �����.
#define ARCS 192   // ���������� ����� � ������
#define ARCS_B 12288 //���������� ���� � ������
#define BLARC 64   //4096/64=64  ���-�� ����� � �����
#define BLARC_B  4096 // ���-�� ���� � �����
#define LEN_DATA 64  // ����� ������ � ������
extern  uint16_t BSectorErase[9]; // {0,4,79,154,175,193,226,250,262}; // �� 427
extern  uint16_t KolSectorArch[9];// {0,25,25,7,6,11,8,4,55};  
extern  uint16_t RecSizeW[9]; //{0, 64,64,64,16,32,32,32,64};// 0, REC,HOR,DAY,ALR,AUD,SCR,AVOL
extern  uint16_t RecSizeR[9]; // {0, 64,64,64,16,18,18,32,64};
extern  uint16_t KolZapArcAll[9];// {0,1600,1600,448,1536,1408,1024,512,3520};
extern  uint16_t KolZapArc[9]; // {0,1536,1536,384,1280,1280,896,384,3456};

//---------------------------------------------------------------------------
//  ������������� ������� ���
//  1  0x80 - �������� �������p���� � ���������
//            0000-0575 - ��������� ������� ������
//            0576-1023 - c������� ��������� �������
//            1024-2765 - �������� ������������  (1742) ����
//            2766-3017 - ������������� XI*3 =252
//            3018-3053 - �������� Zc,Z.K ��� AGA8- 36 � //��������� ������ ������������ (64 �) 16 ����� �� 4 �����
//            3054-3077 - �������� BMIX[n] ��� AGA8- 24 �
//            3078-3081 - �������� NumChrom[3] - ������ ������� ������.
//            3082-3107 - �������������
//            3108-3119 - ������� ��������� ����� �����������
//            3120-3131 - 12 ���� ��� �������� Qw3 ��������
//            3132-3247 - 116 ���� ��� �������� ����. KoefP,KoefdP,KoefE,KoRo
//            3248-3583 - ��������  336 ����
//            3584-3607 - ���������� ���������  24 �����
//            3608-3629 - ��������� ������ ������������ 22 �����
//            3630-3659 - ����� ���������� ����������   30 �
//            3660-4089 - ��������   430 ����
//            4090-4095 - ������� ���������� � ���� ���������
//            4096-5119 - ������ ��������� 0 � 1 //1024 ����, ������ 72 �
//            5120-6143 - �������� ���������
//            6144-6149 - ����� ����������
//            6150-6155 - ������� �������� ��
//            6156-6179 - ������ ��� �� ������
//            6180-7721 - ����� ��� �� 512+2 ��� ������ � SD ��� 3-� �����
//            7722-9623 - ����� ������. �� 512+2 ��� ������ � SD ��� 3-� �����
//            9624-16343 -������� ������ 6*1120=6720, �� 140 �����, 1120 �
//            16344-16355 - 6 int �������  �������� ������
//            16356-16367 - 4*3 -������� ������ ������ "������ ����� �� RS-485
//            16368-16371 -Tlogin1
//            16372-16373 -Dostup1
//            16374-16377 -Tlogin
//            16378-16379 -Dostup
//            16380-16383- �������� 4 ����
//  2  0x81 - ����� �������� ������������ � ��������������(������� �������� � �.�.)
//            0000-2047 - ����� �������� ������������
//            2048-2419 - �������������� ������������
//            2420-2443 - �������� 24 � // ����� ���������� ����������   24 �
//            2444-3619 - �������� 1176 ����
//            3620-3627 - ������� ��� ������ ������� ����� � ���  ������������
//            4096-4245 - ������ ������� 6*15=90 (150 ����)
//            4246-6285 - ����� ������� ����� 60*17=1020 (1020 ����)
//            5270-16325 - ����� ������������ 600*18 = 10800+256=11056
//            16326-16329 - KS
//            16330-16383 - �������� (54 ����)
//////////////////////////////////////////////////
//
// ������ ��� �������
//
struct SensorData
{
   TypeInterface nInterface; // 1 ���� // ��� ����������
   struct IPoint aTable[5];         // 8*5=40 ������ ���������������� ������� (5 �����)
};



/*-----------  �������� ��p����p� �������p���� ����������� --------------- */
struct IdRun {                          // ��p����p� �����
   uint8_t   NameRun[16];   //16      // ��� �����
   uint8_t   TypeP;         //17      // ��� ������� �������� - 0\1 ���.\�����.
   uint8_t   TypeRun;      // 18  // ������ ����p���� �� �����
                                        // 0 - 3095, 2 - 3095+3051CD, 1 - P+T+dP,
                                        // 3 - P+T+dP+dP, 4 - �������, 5 -������� ��
                                        // 6 - ��3   7 - 2-� ����������. dP+P EJX110A
                                        // 8 - Micromotion  9 - Rotamass 10 - ������� ���������� �� RS485

   float           NumTurbo;            // ���-�� �3 �� 1 �������
   float           Qmin;   // ������    // ����������� p����� // � ������ 41-075
   float           Qmax;                // ������������ p�����
   float           Qw;           // 34  // p����� �p� p.�., �3/���

   float           Pmin;                // ������ �p���� ������� ��������, ���\��2
   float           Pmax;                // ��p���� �p���� ������� ��������, ���\��2
   float           dPhmin;              // ����. �p���� ��p�����, ���\�2
   float           dPhmax;              // ��p�. �p���� ��p�����, ���\�2
   float           R0min;              // ����. �p���� ���������, ��\�3
   float           R0max;              // ��p�. �p���� ���������, ��\�3
   float           tmin;                // ����. �p���� �����p���p�, �p. �
   float           tmax;        // 66   // ��p�. �p���� �����p���p�, �p. �

   float           D20;                 // ����p����� ������p �p���, ��
   float           d20;                 // ����p����� ������p ����p����, ��

   float           Rn;                  // ������ ����������� ������, ��
   float           TauP;                // ������ ��������, ���

   float           dPots;               // �p����� �������, ���/�2
   float           ValueSwitch;  // 90  // ��p�� ��p��������� �� ��p�����, ���/�2
   uint16_t             typeOtb;      // 92  // 0 - �������, 1 - ���������
   float           R;                   // ���. ������������� ������������, ��
   float           Bd;                  // ����-�� ���-�� ����-� ��������� ���������, ���^-1(��� A)
   float           kBd;                 // ����-�� B
   float           kCd;                 // ����-�� C
   float           Bt;                  // ����-�� ���-�� ����-� ��������� �p���, ���^-1(��� A)
   float           kBt;                 // ����-�� B
   float           kCt;                 // ����-�� C
   float           Q;            // 124 // p�����

   uint8_t   constdata;           // �������� ���������\������ 0\1
   uint8_t   NoAlarmConstFlag;    // 126 ���� "��������� ��� ������"
   float           constP;              // ��������� ��������, ���\��2
   float           constdP;             // ��������� ��p�����, ���\�2
   float           constt;              // ��������� �����p���p�, �p��. �
   float           constqw;      // 142 // ��������� p������ �p� p.�., �3/���
   float           ROnom;               // ��������� ���� ��� �.�., ��/�^3
   float           Pb;                  // ��������������� ��������, �� p�. ��.
   float           NCO2;                // ����p��� ���� �������� ����p��� � ������� �����, %
   float           NN2;                 // ����p��� ���� ����� � ������� �����, %
   float           Heat;         // 162 // �������������� ���� - �������� ������� �������� ���/�3

   uint8_t   hartadrt[5];         // �����p���p�
   uint8_t   hartadrp[5];         // ��������
   uint8_t   hartadrdph[5];       // ��p���� ��p����
   uint8_t   hartadrdpl[5]; //182 // ������ ��p����

   uint8_t  nKcompress;     // ����� ������� ����.����. 1=Gerg91 0=NX-19
   uint8_t  Dens;                    //184 // ������ ���������� � ������: 0\1
   struct SensorData sensData[4];        // + 164 = 348 // ������ ��� �������� T, P, DP_H, DP_L
                                    //41*4 =164  184+164=348
   uint8_t InterfaceDens;     //349   
   uint8_t NumHartC[4];   // ����� ������ HART (1 ��� 2) -   353
   uint8_t TypeCounter;  // ��� ��������: �������=0, RMG USZ08=1, FlowSic600=2  � ��
   uint8_t AddrMODBUS;   // MODBUS ����� ����������� �� ��������
   uint8_t typeDens;    // 0/1 ���/��� ���������        //356
   uint8_t KUzs;                  //357
   uint8_t GOST_ISO;         //358  ����� ������� ������� 1=ISO 5167 0 = ���� 8.586
   float           RoVV;      //359 �������� ���������
   float           HsVV;      //360    �������� ������� �������� ������
   uint8_t Vlag;                 //361  
/*
   uint16_t DensNReg;    //452 ��������� ����� �������� ����������
   uint16_t DensKReg;   //454  ���������� ���������  � �������
   uint16_t DensKB;    //456  ���������� ���� � ��������
   uint8_t DensPosRo; //458 ������� ��������� � ������
   uint8_t DensPosHe; //459 ������� ������� ����. � ������
   uint8_t DensPosCO2; // 460 ������� CO2 � ������
   uint8_t DensPosN2; // 461 ������� N2 � ������
   uint8_t DensOrder; //462 ������� ���� � ������  0-L->H, 1 - H->L
   uint8_t DensInterv; //463 �������� ��������� ������ � ����������� (0,1,2)
   uint16_t DensReq;    //464  �������� ������ ����������
   uint16_t DensAddr;    //466  MODBUS-����� ����������
   uint16_t DensSpeed;    //468 �������� �� ����� ���������� (���)
*/
   uint8_t RunResrv[56];            //469-520 // ������  ����� 520 ����
   uint16_t  crc;             //392      // 452 ����������� ����p������ �����
// �������� 448 ����
} ;

//---------------------------------------------------------------------------
struct Edizm
  {
   struct bintime  TIME_SU0;           // ����� �������� � �� 0 ��.������
   uint8_t T_SU;          // 0 - 20, 1-0, 2-15  ����.���������� � ��
   uint8_t T_VVP;          // ����. �������� ���������
   uint8_t T_AP;          // ����. ��������������� ��������������
   uint8_t EdIzP;        // 0-��/��2, 1-���, 2- ���
   uint8_t EdIzmdP;      // 0-��/�2, 1-���, 2- ����
   uint8_t EdIzmE;       // 0-����, 1- ���, 2- ���.�
  };

struct glconfig {
   uint8_t NameCal[16];           // ��� �����������
   uint8_t NamePlant[16];         // ������������ �p���p�����
   uint8_t PASSWORD[16];    // 48 // ��p���
   uint8_t NumCal;                // ����p �����������
   uint8_t ContrHour;             // ����p������ ���
   uint8_t NumRun;                // ���-�� �����
   uint8_t TypeCount;             // ��� 1,0: ��50\GERG91\NX19 [00\01\10]
                                        // ��� 2: ��p-const\��p-p���. [0\1]
   uint8_t StartConfigFlag;       // ���� ���. �������p����
   uint8_t Period;                // ����p��� ���������� � �������
   uint8_t Ver;                   // ��p��� �p��p������� �����������
   uint8_t SummerMonth;           // �����
   uint8_t SummerDay;             // ����
   uint8_t SummerHours;           // ��� ��p����� �� ������ �p���
   uint8_t WinterMonth;           // �����
   uint8_t WinterDay;             // ����
   uint8_t WinterHours;           // ��� ��p����� �� ������ �p���
   uint8_t Speed_1;                 // ���p���� �� COM2-��p��
   uint8_t Speed_2;                // ���p���� �� COM1-��p��
   uint8_t Com1NoWrite;           // ���� ������� ������ �� COM1
   uint8_t scr_conf;        //65  // ����� ������������ �������
   float         vol_odor;              // ����� ��� 1-�� �����������, �3/���
   float         termQmax;              // ����� ��� 2-�� �����������, �3/���
   float         termQmin;        //77  // ������
   uint16_t  term;            // ��� ��������� ��� ����� 0-���/1-���
   uint16_t  termDACmin;      //81  // ������
   uint8_t PreambNum;             // ����� �������� �� ���2
   uint8_t PreambNum_1;     //83  // ����� �������� �� ���1
   float TauTable[5];                   // ����     - �������  - ��� ����������
   float RoTable[5];              //123 // �������� - ���������
   uint8_t Serial[8];             //131 ��������� ����� �����������
   uint8_t Telephone[13];          // 144 ����
   uint8_t IP_Adress[13];         //  157
   uint8_t IP_Port[4];            // 161
   uint8_t StartDostup;           // 162 ��������� ��� �������
   uint8_t StartDostup1;           // 163 ��������� ��� �������
//   struct IdRun IDRUN[3]; // 520*3=1560+168=1728 // ��p����p� �����
   struct Edizm   EDIZM_T;    //180
   uint8_t reserve[40];     //222
   uint16_t  crc;                   // ����������� ����p������ �����
// �������� 224 �����
};


/*   struct bintime  TIME_SU0;           // ����� �������� � �� 0 ��.������
   uint8_t T_SU;          // 0 - 20, 1-0, 2-15  ����.���������� � ��
   uint8_t T_VVP;          // ����. �������� ���������
   uint8_t T_AP;          // ����. ��������������� ��������������
   uint8_t EdIzP;        // 0-��/��2, 1-���, 2-��� 3- ���
   uint8_t EdIzmdP;      // 0-��/�2, 1-���, 2- ����
   uint8_t EdIzmE;       // 0-����, 1- ���, 2- ���.�
*/                                // 1740
//   uint16_t  crc;           // 1742 // ����������� ����p������ �����


//-------------------------------------------------------------------------
struct AdRun {
   float           P;                   //4 ��������, ��� ��� ��� - � ������
   float           P_s;                 //8 ��������, ��� ��� ��� - ����������

   double           dP ;                 //16 ��p����, ���

   double          dPh;                 //24 ��p���� ��p����, ��� - � �������
   float           dPh_s;               //28 ��p���� ��p����, ��� - ����������

   double          dPl;                 //36 ������ ��p����, ���
   float           dPl_s;               //40 ������ ��p����, ���

   float           t;                   //44 �����p���p�, �p��. �

   double          Qw_s;                //52 ������������������ ������ �� ��������

   float           ROdens;              //56 ��������� ���� ��� �.�., ��/�^3
   float           ROchr;               //60 ��������� ���� ��� �.�., ��/�^3

   float           NCO2chr;             //64 ����p��� ���� �������� ����p��� � ������� �����, %
   float           NN2chr;              //58 ����p��� ���� ����� � ������� �����, %
   float           Heatchr;             //72 �������������� ����

   uint8_t   typet;               //73 1-�����p���p�, 2-��������, 4-��p��., 8-����.��p����
   uint8_t   typep;               //74
   uint8_t   typedph;             //75
   uint8_t   typedpl;             //76
   uint8_t   edt;                 //77 ��� ������� ����p���� �����p���p�
   uint8_t   edp;                 //78 ��� ������� ����p���� ��������
   uint8_t   eddph;               //79 ��� ������� ����p���� ��p����� ��������
   uint8_t   eddpl;               //80 ��� ������� ����p���� ��p����� ��������
   uint8_t   statP;               //81 ������ �������
   uint8_t   statdPh;             //82 ������ �������
   uint8_t   statdPl;             //83 ������ �������
   uint8_t   statt;               //84 ������ �������
   uint8_t   stat0t;              //85 HART-������ ������� �����p���p�
   uint8_t   stat1t;              //86
   uint8_t   stat0p;              //87 HART-������ ������� ��������
   uint8_t   stat1p;              //88
   uint8_t   stat0dph;            //89 HART-������ ������� ��p����� ��p�����
   uint8_t   stat1dph;            //90
   uint8_t   stat0dpl;            //91 HART-������ ������� ������� ��p�����
   uint8_t   stat1dpl;            //92
   uint32_t Vtot_h;                     //96
   double Vtot_l;                       //104
   double Vtrush;                       //112
   double Vtrual;                       //120
   double Qw1;                           //128 ������ � ���������� �����
   float Ksq;                           //132 ���������� ������, ��� Qw<Qst(==0)
   float t1,P1;                         //140 ���������� �������� t � P
   uint8_t reserv[10];                  // 160
 uint16_t  crc;                   // 160 ����������� ����p������ �����
 // �������� 160 ���� ���� 168
};
//-------------------------------------------------------------------------
struct AddCfg {                         // �������� ����-��, ����������� Conflow
   uint16_t  Takt;                  //2 ���� p����� ������� � ��������
   char corrsek;               //3 �p��� ��pp����� � �������� �� ����� +-24 ���
   uint8_t room;                  //4 ??? ��� ���������� �������p����
   uint8_t   statq;               //5 ������ ��������
   struct bintime  LASTTIME;
   uint8_t Reserv[19];                  //30
   uint16_t  crc;                   // ����������� ����p������ �����
// �������� 32 �����
};

//---------------------------------------------------------------------------
struct memdata {                        // ��p����p� ������ ������ - 64 ����� + 8 +4+4+4 = 64
   struct bintime  STARTTIME;           // ����� 6
   uint8_t   stat;                // ������ �������� ������ � p������   7
   uint8_t   count;               // ������ ���������� � �������   8
   float           p;                   // �p����� ��������    12
   float           t;                   // 24 �p����� ����������� ���� � ��������
   float           RO;                  //  36 ������� ��������� ���� � ��������
   float           CO2;                 // 48 ������� ������� CO2
   float           N2;                  // 52 ������� ������� N2
   float           Heat;                // 56 ������� �������� ������� ��������
   double          dp_Qr;               // 20 �p����� �������- ����� � ��, �������� ������
   double          q;                   // 32 ����� ��� ��.���. �� ������
   double          Vru_total; //M��� // 44 ����� ����� � �� - ����� ����� ��������
   double          FullHeat;            // 64 ������ ������� �������� ������ q
};

//---------------------------------------------------------------------------
/*struct index
{   uint16_t          indexstart;
    uint16_t          indexend;
    uint16_t          checksum;
};*/
//---------------------------------------------------------------------------
struct startendtime//��p����p� ��� ������ ������� � �p�����
{   struct bintime  STARTTIME;      //��������� �p��� ��������� ������
    struct bintime  ENDTIME;        //�������� �p��� ��������� ������
//    uint8_t   RequestNum;     //����� ������� � ������������������ ��������
    int   RequestNum;     //����� ������� � ������������������ ��������
    uint8_t   num;            //����p �����
};
//---------------------------------------------------------------------------
struct tdata {                          // ��� �����p������ ������
   struct bintime  STARTTIME;   //6
   struct bintime  ENDTIME;     //6
   uint8_t   stat;                //7-8 ������ �������� ������ � p������
   uint16_t    num;                 //10 ���-�� ����p����� ��������
   uint16_t    seconds;             //12 ���-�� ����p����� �������� � ��������
   float           p;              // 16
   float           t;               //20
   float           RO;              // 24 ������� ���������
   float           CO2;             // 28 ������� ������� CO2
   float           N2;              // 32 ������� ������� N2
   float           Heat;            // 36 ������� �������� ������� ��������
   float           TCcpv;          // 40 ��.������� �������� ����������������
   double           dp_Qr;          // 48
   double          q;               // 56
   double          Vru_total; //M��� //64
   double          FullHeat;         // 72 ������ ������� �������� ������ q
   uint16_t    checksum;            // 74 ����p������ �����
};      // 55
//---------------------------------------------------------------------------
struct memaudit //��p����p� ������ �p���� ������������ - 17 ���� - 948 �������
{   struct bintime  TIME;
    uint8_t   type;     //��� ������������
    float           altvalue; //�p�������� ��������
    float           newvalue; //����� ��������
    uint16_t    checksum;
};
//---------------------------------------------------------------------------
struct memalarm //��p����p� ������ �p���� ���p�� - 14 ���� - 1152 ������
{   struct bintime  TIME;
    uint16_t    type;     //��� ���p�� � �������p���� ��������
    float           value;    //��������
    uint16_t    crc;
};
//---------------------------------------------------------------------------
struct memalarmC //��p����p� ������ �p���� ���p�� - 14 ���� - 1152 ������
{   struct bintime  TIME;
    uint16_t    type;     //��� ���p�� � �������p���� ��������
    uint8_t           C[4];    //��������
    uint16_t    crc;
};
//---------------------------------------------------------------------------
struct memsecurit //��p����p� ������ �p���� ������������ - 18 ���� - 600 �������
{
    uint8_t login[4];
    struct bintime  TIMEBEG;
    struct bintime  TIMEEND;
    uint8_t mode; //0-������������� �����; 1-����� ���������
    uint8_t Port;
//    uint16_t    checksum;
};
//--------------------------------------------------------------------------
struct mon_summ {                       // ��������� �������� ����������
   double PrevMonthV;                   // ����� �� ������� �����
   double MonthV;                       // ����� �� ������� �����
   double PrevMonthFHeat;               // ������� �� ������� �����
   double MonthFHeat;                   // ������� �� ������� �����
   double Reserve[12];                  // ������ �� 6 ���������� ������ �� ����
};                                      // ����� ����� 128 ������
// �������� 128 * 3(�����) = 384 ����� �
// ������������� �� �������� 80 � ������ 5120

//------------------------------------------------------------------------
struct intpar {
   uint8_t HartNumCyclRep;                 // ����� ������ ������� ��� ���
   uint8_t HartNumImmedRep;                // ����� ����������� ��������
   uint8_t HartReqDelay;                   // ���.�������� ����� �������� HART
   uint8_t RS485Com5Speed;                 // �������� �� COM5 - RS485  ���
   uint8_t Com4Speed;                      // �������� �� �OM4 - RS485  ���
   uint8_t Com2Mode;                       // ��������� � ������, 0-���
   uint8_t Com1NoWrite;                    // ������ ������ �� ���1
   uint8_t OdoMask;                        // ����� ����������
   uint8_t CntBufLen;                      // ����� ������ �������� ���������
   uint8_t CntAddBufShift;                 // ����� ���.������
   float VolumeDelta;                   // ���������� ����������� �� ������
   uint8_t ChannelJoin;                    // ������� ����������� ������
   uint8_t QminFlag;                       // ������� �������� �� ����������� ������
   uint16_t  AlarmVolumeMinTime;             // �����. ����� ��� ���������� ������
   uint8_t FullNormVolume;                 // � ����� ���� ����� ��� ������������
   uint8_t Keyboard;                       // ������� ����������   ASCII - RTU
   uint8_t HeatReport;                     // ����� � ������
   uint16_t  crc;
};     //28 �������� 32 � ������
//---------------------------------------------------------------------------
struct GornalDostup {  // ������ ������� ����� 1020 ���� (60 �������) � 4246 81
    uint8_t login[4];
    struct bintime  TIMEBEG;
    struct bintime  TIMEEND;
    uint8_t Port;
}; // 17
struct GornalDostup1 {  // ��������������� ������ ������� ����� 18 ����
    uint8_t login[4];
    struct bintime  TIMEBEG;
    struct bintime  TIMEEND;
    uint8_t Port;
    uint8_t t;
}; // 18
//---------------------------------------------------------------------------
struct GornalPassw { //  ������ ������� 90 ���� (6 �������) c 4096 ��� 81
    uint8_t login[4];
    uint8_t Password[10];
    uint8_t kodostup; // ������� ������� (7-��������� 0 - ������)
}; // 15
//------------------------------------------------------------------------
struct intpar2 {    // ��������� ���������� ���������� 2
   char HartNumCyclRep;                 // ����� ������ ������� ��� ���
   char HartNumImmedRep;                // ����� ����������� ��������
   char HartReqDelay;                   // ������ �.6 �� ���-������ (101 ��� 1)
   char Com1NoWrite;                    // ASCII =  0 / MOSCSD RTU = 1
   char CntBufLen;                      // ����� ������ �.6 (0 - 23, 1-27 ����)
   char CntAddBufShift;                 // �������� ������ �������������� (0,1,...)
   float VolumeDelta;                   // ������ � �.�. 4 �����
   uint16_t  AlarmVolumeMinTime;             // �����. ����� ��� ���������� ������
//   uint16_t  crc;
}; //14 ������������� �� �������� 80 � ������ 3584 + 22 =3606 �� 3619
//------------------------------------------------------------------------
struct addpar {  //�������������� ��������� ������������
   uint8_t RS485Com5Speed;                 // �������� �� COM5 - RS485
   uint8_t Com4Speed;                      // �������� �� �OM3
   uint8_t Com2Mode;                       // ��������� � ������, 0-���
   uint8_t OdoMask;                        // ����� ����������
   uint8_t ChannelJoin;                    // ��������� ��������� ������������
   uint8_t QminFlag;                       // ������� �������� �� ����������� ������
   uint8_t FullNormVolume;                 // � ����� ���� ����� ��� ������������
   uint8_t Keyboard;                       // ������� ����������
   uint8_t HeatReport;                     // ����� � ������
//   uint16_t  crc;
}; //9 � ������������� �� �������� 80 � ������ 3606 + 14 =3620 �� 3629

//---------------------------------------------------------------------------
void SetPage(uint8_t);
void TestPeriodRes(void);
void TestPeriodRead(void);
void ClrArc(uint8_t empr);
//������ ������ ������ TypeArch = (PER, HOU,DAY) c ������� di1, ����� - di2
uint8_t  MemdataWr (uint8_t TypeArch, uint8_t run,uint16_t*,uint16_t*, struct memdata Zapis); 
//void MemdataWr(uint8_t); //������ ��p����������� ������
uint16_t   MemPerioddataRd(uint16_t num,uint8_t numpage);//������ ������ ��p����������� ������
uint16_t   MemPerioddataRd_1(uint16_t num,uint8_t numpage);//������ ������ ��p����������� ������
void MemPerioddataRdEnd(void);  //������ ��������� ������ ��p����������� ������
void MemPerioddataRdEnd_1(void);  //������ ��������� ������ ��p����������� ������
void PriMemperiod(struct memdata*);
void SetMemperiod(struct memdata*,uint8_t,uint8_t);
void SetStartEndTime(struct startendtime*);
void TestMem(void);
uint16_t MemPeriodWrNum(uint16_t num);
uint16_t MemPerioddataSearchStart(uint8_t numpage,struct startendtime *);
uint16_t MemPerioddataSearchStart2(uint8_t numpage,struct startendtime *);
uint16_t MemPerioddataSearchEnd(uint8_t,struct startendtime *);
//uint16_t MemPerioddataSearchStart_1(uint8_t numpage);
//uint16_t MemPerioddataSearchStart2_1(uint8_t numpage);
//uint16_t MemPerioddataSearchEnd_1(uint8_t);
void Mem_Wrtdata(uint8_t, uint8_t, struct tdata); // ������ ���������
void Mem_Rdtdata(uint8_t, uint8_t, struct tdata*);//run=(0-2), type=(PER,HOU,DAY)
void Mem_Testtdata(void);
uint8_t WriteDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *wrData);
void Setperiodtdata(void);  //��� �����p������ ������ �� �����
void Mem_Clrtdata(char empr);    //��p�� ���-�� ������� � �������p��
void Sethourtdata(void);    //��� �����p������ ������� ������ �� �����
void Setdaytdata(void);     //��� �����p������ �������� ������ �� �����
uint8_t GetPage(uint16_t, uint16_t);
void Setmonthtdata(void);   //������ ��������� ������
uint16_t NextIndex(uint16_t, uint16_t, uint8_t);
//uint16_t NextIndex_1(uint16_t, uint16_t, uint8_t);
float   Mem_GetDeyQ(uint8_t run);
float   Mem_GetDayE(uint8_t run);

uint16_t Mem_SecDayRdEnd(uint8_t, uint8_t);//������ ��������� ������ ��������� ������
uint8_t Mem_SecDayRd (uint8_t TypeArch, uint8_t run, uint16_t);
uint8_t Mem_DayWr(uint8_t numpage);  //������ ��������� ������
uint16_t Mem_DaySearchStart(uint8_t,struct startendtime *);
uint16_t Mem_DaySearchStart2(uint8_t,struct startendtime *);
uint16_t Mem_SearchStart(uint8_t TypeArch,uint8_t run,  struct startendtime *STARTENDTIM);
uint16_t Mem_SearchEnd(uint8_t TypeArch,uint8_t run, struct startendtime *STARTENDTIM);
//uint16_t Mem_DaySearchStartp(uint8_t,struct startendtime *);
//uint16_t Mem_DaySearchStart2p(uint8_t,struct startendtime *);
uint16_t Mem_DaySearchEnd(uint8_t,struct startendtime *);
//uint16_t Mem_DaySearchEndp(uint8_t,struct startendtime *);
//uint16_t Mem_DaySearchStart_1(uint8_t);
//uint16_t Mem_DaySearchStart2_1(uint8_t);
//uint16_t Mem_DaySearchEnd_1(uint8_t);
uint16_t Mem_NextIndex(uint8_t, uint16_t, uint16_t,uint16_t, uint16_t);

uint8_t   Mem_GetAuditPage(uint16_t);
uint8_t   Mem_AuditWr(uint8_t,struct memaudit);//������ ������ ������������
uint16_t   Mem_AuditWr_1(uint8_t);//������ ������ ������������
uint8_t Mem_AuditRd (uint8_t run, uint16_t ip, uint8_t* MemDat);
uint16_t Mem_AuditSearchStart(uint8_t,struct startendtime *);
uint16_t Mem_AuditSearchStart2(uint8_t,struct startendtime *);
uint16_t Mem_AuditSearchEnd(uint8_t,struct startendtime *);
//uint16_t Mem_AuditSearchStart_1(uint8_t);
//uint16_t Mem_AuditSearchStart2_1(uint8_t);
//uint16_t Mem_AuditSearchEnd_1(uint8_t);
uint16_t Mem_AuditNextIndex(uint16_t, uint16_t, uint8_t);
//uint16_t Mem_AuditNextIndex_1(uint16_t, uint16_t, uint8_t);
void  Mem_AuditClr(uint8_t empr); //������� ��p����� ������������
int   Mem_ConfigRunWr(uint8_t);   // ������ ���. �������p���� �� ������ � ���p������������� ������
int   Mem_ConfigWr(void);//������ �������� �������p����
uint16_t   Mem_AddCfgWr(uint16_t);   // ������ ���.�������p���� � ���p������������� ������
void  Mem_ConfigRd(void);   // ������ ���.�������p���� �� ���p������������� ������
void  Mem_ConfigRdEx(void); // ������ ���.�������p���� �� ���p������������� ������
void  Mem_AddCfgRd(void);   // ������ ���.�������p���� �� ���p������������� ������
uint16_t Mem_AuditTestReqTimeSAER(struct startendtime *); //����p��� ����������� �p���� � �p���� � ���p���
uint16_t Mem_AuditTestReqTimeSABR(struct startendtime *);
//uint16_t Mem_AuditTestReqTimeSAER_1(void); //����p��� ����������� �p���� � �p���� � ���p���
//uint16_t Mem_AuditTestReqTimeSABR_1(void);
//uint8_t   Mem_AlarmGetPage(uint16_t);
void Mem_AlarmClr(uint8_t empr);                                //������� ��p����� ���p��
//uint16_t   Mem_AlarmWr(uint8_t,char);  //������ ������ ���p��
uint8_t Mem_AlarmRd(uint16_t run, uint16_t ip, uint8_t* MemDat);//������ ������ ������ ���p��
//uint16_t Mem_AlarmRd_1(uint16_t, uint8_t);//������ ������ ������ ���p��
uint16_t Mem_AlarmSearchStart(uint8_t,struct startendtime *);
uint16_t Mem_AlarmSearchStart2(uint8_t,struct startendtime *);
uint16_t Mem_AlarmSearchEnd(uint8_t,struct startendtime *);
//uint16_t Mem_AlarmSearchStart_1(uint8_t);
//uint16_t Mem_AlarmSearchStart2_1(uint8_t);
//uint16_t Mem_AlarmSearchEnd_1(uint8_t);
uint16_t Mem_AlarmNextIndex(uint16_t, uint16_t, uint8_t);
//uint16_t Mem_AlarmNextIndex_1(uint16_t, uint16_t, uint8_t);
//void Mem_AlarmSetDataErr(void);

void Mem_AlarmSetCrc(void);
void Mem_AlarmSetNoalarm(void);
//uint16_t    Mem_AlarmSetValEdErr(void);
uint16_t Mem_AlarmSetValErr(uint16_t nRun);
void Mem_AlarmWrCod(uint8_t,uint16_t, char*);
void Mem_AlarmWrCodHart(uint16_t,float);
void Mem_AlarmWrCodRun(uint8_t, uint16_t,float);
void Mem_AlarmSetStart(void);
void Mem_AlarmSetStop(void);
//void Mem_AlarmWrType(uint16_t);
void Mem_AlarmWrCodPower(uint16_t); //������ ���� ���p�� �� ������� � ��p���� ���p��

uint16_t Mem_AlarmTestReqTimeSAER(struct startendtime *);//����p��� ����������� �p���� � �p���� � ���p���
//int Mem_AlarmTestReqTimeSAER_1(void);//����p��� ����������� �p���� � �p���� � ���p���
uint16_t Mem_AlarmTestReqTimeSABR(struct startendtime *);  //����p��� ����������� �p���� � �p���� � ���p���
//uint16_t Mem_AlarmTestReqTimeSABR_1(void);
uint16_t Mem_Crc16(uint8_t* buf, uint16_t count);
//uint16_t Mem_Crc16Int(uint8_t* buf, uint16_t count);

uint16_t Mem_TestReqTime(struct bintime*);   // ����p��� �p����� ���p���
//uint16_t Mem_TestReqTime_1(struct bintime*); // ����p��� �p����� ���p���
uint16_t Mem_TestReqTimeSE(void);      // ����p��� ����������� �p���� ���p���
uint16_t Mem_TestReqTimeSEp(struct startendtime *);      // ����p��� ����������� �p���� ���p���
uint16_t Mem_TestReqTimeEq(void);      // ����p��� ����������� �p���� ���p��� �� p��������
uint16_t Mem_TestReqTimeEqp(struct startendtime *);      // ����p��� ����������� �p���� ���p��� �� p��������
//int Mem_TestReqTimeEq_1(void);    // ����p��� ����������� �p���� ���p��� �� p��������
uint16_t Mem_TestReqTimeEnd(void);     // ����p��� ����������� �p���� � �p���� � ���p���
uint16_t  Mem_TestReqTimeEndp(struct startendtime *,uint8_t *s1);  //����p��� ����������� �p���� � �p���� � ���p���
//uint16_t Mem_TestReqTimeEndp(struct startendtime *);     // ����p��� ����������� �p���� � �p���� � ���p���
uint16_t Mem_TestReqTimeStart(struct startendtime *,uint8_t *); 
//uint16_t  Mem_TestReqTimeStart(struct startendtime *STARTENDTIM);  //����p��� ����������� �p���� � �p���� � ���p���
//uint16_t Mem_TestReqTimeEnd_1(void);   // ����p��� ����������� �p���� � �p���� � ���p���
uint16_t Mem_TestReqTimeBeg(void);
uint16_t Mem_TestReqTimeBegp(struct startendtime *);
//uint16_t Mem_TestReqTimeBeg_1(void);
uint16_t Mem_VTestReqTimeEnd(struct startendtime *);    // ����p��� ����������� �p���� � ��.�p���� � ���p���
//uint16_t Mem_VTestReqTimeEnd_1(void);  // ����p��� ����������� �p���� � ��.�p���� � ���p���
uint8_t Mem_CyklGetPage(uint16_t);
//uint8_t Mem_CyklGetPage_1(uint16_t);

void Mem_StartPeriodHour(uint16_t numrun);       // ��pp����� �������p�� �p� ���p��
void Mem_StartHourDay(uint8_t numrun);          // ��pp����� �������p�� �p� ���p��
void Mem_StartDayMonth(struct bintime, uint8_t ); // ��pp����� �������p�� �p� ���p��

void Mem_AlarmSetPowerOff(void);
void Mem_AlarmSetPowerOn(void);

void Mem_AddToMonth(void);
void Mem_CheckMonth(struct bintime bt);

double ReadDouble (uint8_t Type, uint8_t nRun);
void   WriteDouble (double dVal, uint8_t Type, uint8_t nRun);

float GetVm (uint8_t nRun);
float GetEm (uint8_t nRun);

void Mem_AlarmHartComOn(void);  // ������ ������ HART-COM
void Mem_AlarmHartComOff(void); // ����� ������ HART-COM

void IniChkCfg(void); // ������������� �������� ����������� ������������ � ���
void ChkCfg(void);    // �������� ����������� ������������ � ���
void Mem_ConfigRdChk(uint8_t); // ����������� ������
uint16_t  Mem_ConfigWrChk(uint8_t); // ����������� ������

uint16_t ChkSensMax (uint16_t Sens, float Val, float Max);
uint16_t ChkSensMin (uint16_t Sens, float Val, float Min);

void ReadMonthSumm (uint8_t nRun);

//void Mem_IntParWr(void);
void Mem_IntParRd(uint16_t adz);
void  Mem_LoginRd(void);
void  Mem_LoginWr(void);
//uint16_t Mem_ArhivDRd(uint16_t*);
//void  Mem_ArhivDWr(uint16_t);
// uint8_t   Mem_SecurGetPage(void);
void Mem_SecurClr(char empr);                                //������� ��p����� ���p��
uint16_t   Mem_SecurWr_2(uint8_t,uint8_t, char);  //������ ������ ���p��
//uint16_t   Mem_SecurWr_1(uint8_t,uint8_t);  //������ ������ ���p��
uint16_t Mem_SecurRd(uint16_t, uint8_t);//������ ������ ������ ���p��
//uint16_t Mem_SecurRd_1(uint16_t, uint8_t);//������ ������ ������ ���p��
uint16_t Mem_SecurSearchStart(uint8_t,struct startendtime *);
uint16_t Mem_SecurSearchStart2(uint8_t,struct startendtime *);
uint16_t Mem_SecurSearchEnd(uint8_t,struct startendtime *);
/*
uint16_t Mem_SecurSearchStart_1(uint8_t);
uint16_t Mem_SecurSearchStart2_1(uint8_t);
uint16_t Mem_SecurSearchEnd_1(uint8_t);
*/
uint16_t Mem_SecurNextIndex(uint16_t, uint16_t, uint8_t);
//uint16_t Mem_SecurNextIndex_1(uint16_t, uint16_t, uint8_t);
uint16_t Mem_SecurTestReqTimeSAER(struct startendtime *);//����p��� ����������� �p���� � �p���� � ���p���
//uint16_t Mem_SecurTestReqTimeSAER_1(void);//����p��� ����������� �p���� � �p���� � ���p���
uint16_t Mem_SecurTestReqTimeSABR(struct startendtime *);  //����p��� ����������� �p���� � �p���� � ���p���
//uint16_t Mem_SecurTestReqTimeSABR_1(void);
//---------------------------------------------------------------------------
#endif//__H_MEM_H
