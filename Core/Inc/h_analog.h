#ifndef _ANALOG_
#define _ANALOG_

#include <stdio.h>

// ���������������� �����
struct IPoint {
   float code;
   float value;
};

// ������������ ��� �������� ������������
struct ICoeff {
   float codeLow;
   float valueLow;
   float slope;
};

//
// ��� ���������� �������
//

typedef unsigned char TypeInterface;

#define tiHART     1
#define tiAN_WS    0
#define tiRS485DPH 2
#define tiAN_RS485 3

// ������� �������
#define TIMER_HZ 40
// ������� �������������
#define A2D_HZ 10

// ����� ����� ������� �� ������� �������������
#define TICKS_PER_SAMPLE (TIMER_HZ / A2D_HZ)
// ��������� ���������� ��������� ������ �������
#define SAMPLES_PER_RESULT A2D_HZ

//////////////////////////////////////////////////////////////////
#ifdef MAIN_ANALOG
   unsigned int m_aCode[3][5][10];  // ���������� ���������
// float m_aCode[3][4];             // ���������� ���������� ���������
                                    // �������, �������� ������ �������������.
   int m_cTick = 0;                 // ���������� ����� TICKS_PER_SAMPLE ����� �������
                                    // �������, �������� ������ ���������� ����������� ���������
   int m_cSample = 0;               // ���������� ����� SAMPLES_PER_RESULT �������� �������������
   int m_pSample;                   //
   struct ICoeff m_pCoeff[3][5][5]; // ������ �-��� ������������
   int m_nPoints = 5;               // ����� �����
   float kf[3][5];                  // ����������� �����������
   float flt_ankan[3][5];           // ��������� ��������
#else
// extern unsigned int m_aCodeOper[3][4];  // ����� ����������� ���������
   extern unsigned int m_aCode[3][5][10];  // ���������� ���������
   extern unsigned int AnCodeSum;
// extern float m_aCode[3][4];      // ���������� ���������� ���������
                                    // �������, �������� ������ �������������.
   extern int m_cTick;              // ���������� ����� TICKS_PER_SAMPLE ����� �������
                                    // �������, �������� ������ ���������� ����������� ���������
   extern int m_cSample;            // ���������� ����� SAMPLES_PER_RESULT �������� �������������
   extern int m_pSample;            //
   extern struct ICoeff m_pCoeff[3][5][5]; // ������ �-��� ������������
   extern struct IPoint pPoints[3][5][5];  // ������ ����� ������������
   extern int m_nPoints;            // ����� �����
   extern float kf[3][5];           // ����������� �����������
   extern float flt_ankan[3][5];    // ��������� ��������
#endif

//////////////////////////////////////////////////////////////////
void SetChanToSensTable(void);        // ����� ����. ���. �� ������ ������� � �������
void SetChanToHartAdr(unsigned char); // ����� ����. ���. �� ������� � ����� �������
void CalibrateIni(void);              // ��������� ����-��
void CAnalogRead(int nRun);           // ������ ����� � �������� �������� ������
float GetCalibrValue(unsigned char nR,unsigned char nS,float Cd); // �������� ����������������� ��������
void AnalogToCalc(int NumRun);        // ������ ���������� �������� �� ���� �������
void Task_AnalogToCalc(void);             //

#endif // #ifndef _ANALOG_
