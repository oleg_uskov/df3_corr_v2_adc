#ifndef _PERSIST_H
#define _PERSIST_H

struct PersistData {
   double QwCountTotal;                 // ��������� (�����) �������� ������ ��� �� ��� ��������, �3
   double QTotal;                       // ����� �����, �3
   double ETotal;                       // ����� �������, ���
};

struct DataWithCRC {
  struct PersistData run[3];
   unsigned int wCrc;                   // ����������� �����
};

int    Persist_Read(void);
void   Persist_Write(void);

double Persist_GetQwCountTotal(int iRun);
void   Persist_SetQwCountTotal(int iRun, double QwCountTotal);
void   Persist_AddQwCountTotal(int iRun, double QwCountTotal);

double Persist_GetQTotal(int iRun);
void   Persist_AddQTotal(int iRun, double QTotal);
void   Persist_SetQTotal(int iRun, double QTotal);
void   Persist_SetDefault(char empr);
//void   Persist_ReadAndSetDefault(void);

#endif // #ifndef _PERSIST_H
