#include "main.h"

//--------------------------------------------------------------------------
void Port_Ini(void); //�㭪�� ���樠����樨 COM ����
//void Int_Com_In();      //�p��� ���p�
void Int_Com_Out(void);     //��p���� ���p�
void SearchKadr(void);      //���� �p���⮣� ���p�
void SearchKadr_Hartservice(void);      //���� �p���⮣� ���p�
void CaseNumFun(void);      //����� ����㭨��樮���� �㭪樨
void AnswError(void);
void AnswId(void);          //���p�� �����䨪��p�
void SaveMessagePrefix(unsigned char,unsigned char,uint8_t*);//�p��p������ ��������� � �⢥�
void SaveMessagePrefix_2(unsigned char*,unsigned char,unsigned char);//�p��p������ ��������� � �⢥�
void CheckBufTrans(uint8_t*);   //�p��p������ ����p��쭮� �㬬� � ��室��� ���p�
void AnswSys(void);         //�⥭�� ��⥬��� ��p����p��
void AnswStCfg(void);       //����室������ ��砫쭮� ���䨣�p�樨
void WrSys(void);           //������ ��⥬��� ��p����p��
void AnswChim(void);        //�⥭�� 娬��᪮�� ��⠢�
void WrChim(void);          //������ 娬��᪮�� ��⠢�
void WrStCfg(void);         //������ ��砫쭮� ���䨣�p�樨
void WrRecfg(void);         //������ ��p����䨣�p�樨
void Ini_Int_Com2(void);    //��⠭���� ����p� �p�p뢠��� �� com2-IRQ3
void Ini_Int_Com4(void);    //��⠭���� ����p� �p�p뢠��� �� com4-IRQ3
void Ini_Reciv1(void);       //���樠������ �p����
void Ini_Send1(void);        //���樠������ ��p����
void Ini_SendHCom(void);    // ���樠������ ��p���� � ०��� HART<->COM
void ChekBufResiv(void);    //�p��-�� �p� �맮�� ����p�⭮�� �p�p뢠��� ⠩��p�
void CheckBufTransIniSend(void);
void IniCfgSpeed(void);     //���樠������ ᪮p��� �� ��p����p�� ���䨣�p�樨
void RdStat(void);          //�⥭�� ����᪨� ��p����p��
void WrStat(void);          //������ ����᪨� ��p����p��
void RdConst(void);         //�⥭�� ����⠭�
void WrConst(void);         //������ ����⠭�
void RdInstparam(void);     //��������� ��p����p�
void RdCalcparam(void);     //��������� ��p����p�
void RdParamdat(void);      //�⥭�� ��p����p�� ���稪��  ��
void WrParamdat(void);      //������ ��p����p�� ���稪��  ��
void RdParamdat46(void);      //�⥭�� ��p����p�� ���稪�� ��
//void WrParamdat47();      //������ ��p����p�� ���稪�� ��
void RdPeriod(void);        //�⥭�� ���p�⨢��� (��p�����᪨�) ������
void RdHour(void);          //�⥭�� �ᮢ��� ����
void RdDay(void);           //�⥭�� ���筮�� ����
void RdValarm(void);        //�⥭�� ���਩��� ��ꥬ��
void RdAudit(void);         //�⥭�� ��p���� ����⥫���
void RdAlarm(void);         //�⥭�� ��p���� ���p��
void RdSecur(void);
void SetTime(void);         //��⠭���� �p�����
void Comm_RdCykl(void);     //�⥭�� 横���᪨� ������
void AnswCode(void);        // �⢥� �� ����� �⥭�� ���� ��� � ���௮��஢������ ���祭��
//void WrClbr();          // ������ �窨 �����஢��
void AnswClbrTable(void);   // �⢥� �� ����� �⥭�� ⠡���� �����஢��
void WrClbrTable(void);     // �⢥� �� ����� �⥭�� ⠡���� �����஢��
void AnswerHCom(void);      // �⢥� �� ��४��祭�� � ०�� HART<->COM
void AnswerHComY(void);      // �⢥� �� ��४��祭�� � ०�� HART<->COMvoid RdDiaph();
void RdDiaph(void);
void WrDiaph(void);
void RdNoAlarmConstFlag(void);
void WrNoAlarmConstFlag(void);
void RdPeriodChr(void); // �� �����. ��ࠬ��஢  - ���
void RdDayChr(void);    // �� ������ ��ࠬ��஢ - ���
void RdCyclChr(void);   // �� 横����� ��ࠬ��஢ - ���
void RdHourChr(void);   // �� �ᮢ�� ��ࠬ��஢  - ���
void NoDostup1(void);
void RdPeriodDen(void); // �� �����. ��ࠬ��஢  - ���⭮���
void RdDayDen(void);    // �� ������ ��ࠬ��஢ - ���⭮���
void RdCyclDen(void);   // �� 横����� ��ࠬ��஢ - ���⭮���
void RdHourDen(void);   // �� �ᮢ�� ��ࠬ��஢  - ���⭮���

void RdIntPar(void);    // �⥭�� ����७��� ��ࠬ��஢
void WrIntPar(void);    // ������ ����७��� ��ࠬ��஢
void RdIntPar2(void);    // �⥭�� ����७��� ��ࠬ��஢2
void WrIntPar2(void);    // ������ ����७��� ��ࠬ��஢2
void WrAddConf(void);    // �⥭�� �������⥫��� ��ࠬ. ���䨣��樨
void RdAddConf(void);    // ������ �������⥫��� ��ࠬ. ���䨣��樨

void RdNames(void);     // �⥭�� ������������
void WrNames(void);     // ������ ������������
void fPassword(void);    // ���� ��஫� - ��砫� ᥠ��
void fendSeans(void);   // ����� ᥠ�� �裡
void fCreateUser(void); // ᮧ����� ���짮��⥫�� �㭪�� 0x56
//void fRdArchDost(void);  // �⥭�� ��娢� ������᭮�� �㭪�� 0x57
void fUserRd(void);     // �⥭�� ���짮��⥫��
void RdVolume(void); // �⥭�� ���� ��ꥬ��
void SD_RdCykl(void); // �⥭�� ���������� �� SD-�����
void SD_RdPeriod(void); // �⥭�� ��ਮ���᪨� �� SD-�����
void RdNameSD(void);    // �� ���� �� SD-�����
//void GornalD_Wr(int);   //���������� ��ୠ�� ����㯠
void InitSD(void);
void RdEdizm(void);
void WrEdizm(void);
void Mem_WrEndSeans(void);
void Mem_WrEndSeans_2(unsigned char* , char);
void  RecievMODBUS(void);
void MoveToRS485FBSens(unsigned char,unsigned char,unsigned char);

//--------------------------------------------------------------------------

void Com2_Watch(void);       // ��� ����� � ����ᠭ�ﬨ ����㭨��樨 �� COM2
void SetHartCom2Mode(void);  // ��⠭����� �� ���2  HART-०��