#include "main.h"

int IsCom1Jumper(void);                     // ��६�窠 ��� �⫠��� �� ���1
void Com2Ini(void);                         // ���樠������ ���� ���1

void IniCfgSpeed2(void);
void Port_Ini2(void);
void Ini_Int_Com_1(void);
//void interrupt Int_0C(...);
void Com1Stop(void);
void Int_Com_In2(void);                    // �p��� ���p� �� �p�p뢠���
void Int_Com_Out2(void);                   // ��p���� ���p� �� �p�p뢠���
void Ini_Send2(void);
void Ini_Reciv2(void);
void ChekBufResiv2(void);                  // �p�������� �p� �맮�� ����p�⭮�� �p�p뢠��� ⠩��p�
void SearchKadr2(void);                    // ���� �p���⮣� ���p� � ���p� �p����
void SearchKadr_Hartservice2(void);      //���� �p���⮣� ���p�
void CheckBufTrans2IniSend(void);
void CheckBufTrans2(void);                 // �p��p������ ����p��쭮� �㬬� � ��室��� ���p�
void SD_RdCykl_1(void); // �⥭�� ���������� �� SD-�����
void SD_RdPeriod_1(void); // �⥭�� ��ਮ���᪨� �� SD-�����

//-------------------------------------------------------------------
void CaseNumFun_1(void);      //����� ����㭨��樮���� �㭪樨
void AnswError_1(void);
void AnswId_1(void);          //���p�� �����䨪��p�
//void SaveMessagePrefix_1(unsigned char,unsigned char);//�p��p������ ��������� � �⢥�
void SaveMessagePrefix_2(unsigned char*,unsigned char,unsigned char);//�p��p������ ��������� � �⢥�
void SaveMessagePrefix(unsigned char,unsigned char,uint8_t*);//�p��p������ ��������� � �⢥�

//void CheckBufTrans_1(void);   //�p��p������ ����p��쭮� �㬬� � ��室��� ���p�
void AnswSys_1(void);         //�⥭�� ��⥬��� ��p����p��
void AnswStCfg_1(void);       //����室������ ��砫쭮� ���䨣�p�樨
void WrSys_1(void);           //������ ��⥬��� ��p����p��
void AnswChim_1(void);        //�⥭�� 娬��᪮�� ��⠢�
void WrChim_1(void);          //������ 娬��᪮�� ��⠢�
void WrStCfg_1(void);         //������ ��砫쭮� ���䨣�p�樨
void WrRecfg_1(void);         //������ ��p����䨣�p�樨
//void Ini_Int_Com1(void);    //��⠭���� ����p� �p�p뢠��� �� com2-IRQ3
void Ini_Reciv_1(void);       //���樠������ �p����
void Ini_Send_1(void);        //���樠������ ��p����
void Ini_SendHCom_1(void);    // ���樠������ ��p���� � ०��� HART<->COM
void ChekBufResiv_1(void);    //�p��-�� �p� �맮�� ����p�⭮�� �p�p뢠��� ⠩��p�
void CheckBufTransIniSend_1(void);
void IniCfgSpeed_1(void);     //���樠������ ᪮p��� �� ��p����p�� ���䨣�p�樨
void RdStat_1(void);          //�⥭�� ����᪨� ��p����p��
void WrStat_1(void);          //������ ����᪨� ��p����p��
void RdConst_1(void);         //�⥭�� ����⠭�
void WrConst_1(void);         //������ ����⠭�
void RdInstparam_1(void);     //��������� ��p����p�

void RdCalcparam_1(void);     //��������� ��p����p�
void RdParamdat_1(void);      //�⥭�� ��p����p�� ���稪��
void WrParamdat_1(void);      //������ ��p����p�� ���稪��
void RdPeriod_1(void);        //�⥭�� ���p�⨢��� (��p�����᪨�) ������
void RdHour_1(void);          //�⥭�� �ᮢ��� ����
void RdDay_1(void);           //�⥭�� ���筮�� ����
void RdValarm_1(void);        //�⥭�� ���਩��� ��ꥬ��
void RdAudit_1(void);         //�⥭�� ��p���� ����⥫���
void RdAlarm_1(void);         //�⥭�� ��p���� ���p��
void SetTime_1(void);         //��⠭���� �p�����
void Comm_RdCykl_1(void);     //�⥭�� 横���᪨� ������
void AnswCode_1(void);        // �⢥� �� ����� �⥭�� ���� ��� � ���௮��஢������ ���祭��
//void WrClbr_1(void);          // ������ �窨 �����஢��
void AnswClbrTable_1(void);   // �⢥� �� ����� �⥭�� ⠡���� �����஢��
void WrClbrTable_1(void);     // �⢥� �� ����� �⥭�� ⠡���� �����஢��
void AnswerHCom_1(void);      // �⢥� �� ��४��祭�� � ०�� HART<->COM
void AnswerHComY_1(void);      // �⢥� �� ��४��祭�� � ०�� HART<->COM
void AnswDbg_1(void);
void WrDbg_1(void);
void RdVolume_1(void); // �⥭�� ���� ��ꥬ��
void RdDiaph_1(void);
void WrDiaph_1(void);
void RdSecur_1(void);
void RdNoAlarmConstFlag_1(void);
void WrNoAlarmConstFlag_1(void);
void NoDostup2(void);

void FloatValueChange(float*,float *,unsigned char,unsigned char);
void FloatValueChangeEd(float* ,float *,unsigned char,unsigned char,double);

void HartAddrChange(unsigned char*,unsigned char *,unsigned char,unsigned char,unsigned char);
int  ChkCurrDate(void);
void dayMinus(unsigned char,unsigned char,unsigned char,unsigned char *,unsigned char *,unsigned char*);
void dayPlus(unsigned char,unsigned char,unsigned char,unsigned char *,unsigned char *,unsigned char*);
void SetVauditCount(unsigned char,unsigned char,unsigned char,unsigned char);
//void ConstCorr(tagSENSOR,float *,unsigned char);

void RdPeriodChr_1(void); // �� �����. ��ࠬ��஢  - ���
void RdDayChr_1(void);    // �� ������ ��ࠬ��஢ - ���
void RdCyclChr_1(void);   // �� 横����� ��ࠬ��஢ - ���
void RdHourChr_1(void);   // �� �ᮢ�� ��ࠬ��஢  - ���

void RdPeriodDen_1(void); // �� �����. ��ࠬ��஢  - ���⭮���
void RdDayDen_1(void);    // �� ������ ��ࠬ��஢ - ���⭮���
void RdCyclDen_1(void);   // �� 横����� ��ࠬ��஢ - ���⭮���
void RdHourDen_1(void);   // �� �ᮢ�� ��ࠬ��஢  - ���⭮���

void RdIntPar_1(void);    // �⥭�� ����७��� ��ࠬ��஢
void WrIntPar_1(void);    // ������ ����७��� ��ࠬ��஢
void RdIntPar2_1(void);    // �⥭�� ����७��� ��ࠬ��஢2
void WrIntPar2_1(void);    // ������ ����७��� ��ࠬ��஢2
void WrAddConf_1(void);    // �⥭�� �������⥫��� ��ࠬ. ���䨣��樨
void RdAddConf_1(void);    // ������ �������⥫��� ��ࠬ. ���䨣��樨

void RdNames_1(void);     // �⥭�� ������������
void WrNames_1(void);     // ������ ������������
void fPassword_1(void);    // ���� ��஫� - ��砫� ᥠ��
void fendSeans_1(void);   // ����� ᥠ�� �裡
void fCreateUser_1(void); // ᮧ����� ���짮��⥫�� �㭪�� 0x56
//void fRdArchDost_1(void);  // �⥭�� ��娢� ������᭮�� �㭪�� 0x57
void fUserRd_1(void);     // �⥭�� ���짮��⥫��
void SD_RdCykl_1(void); // �⥭�� ���������� �� SD-�����
void SD_RdPeriod_1(void); // �⥭�� ��ਮ���᪨� �� SD-�����
void InitSD_1(void);  // ���樠������ SD-�����
void RdNameSD_1(void);    // �� ���� �� SD-�����
//void GornalD_Wr_1(int);   //���������� ��ୠ�� ����㯠
//void fRdArchD_1();
void RdEdizm_1(void);
void WrEdizm_1(void);
void Mem_WrEndSeans_1(void);