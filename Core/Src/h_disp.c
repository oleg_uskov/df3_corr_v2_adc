///////////////////////////////////////////////////////////////
// ������������ ��-����������.
///////////////////////////////////////////////////////////////


#include <string.h>
#include <stdio.h>
#include <math.h>


//#include "density.h"
#include "main.h"
#include "h_hart.h"
#include "h_disp.h"
#include "h_time.h"
#include "h_main.h"
#include "h_calc.h"
#include "h_analog.h"
#include "h_mem.h"
#include "h_smooth.h"
#include "h_persist.h"
//#include "h_debug.h"
#include "h_485.h"
#include "comm_str.h"

//#define BUTMAX  40 //������������ ������� ������ � 5��  (200 ��)
//---------------------------------------------------------------------------
void n_delay (int mlsek);
void GetVersion(struct Id *pid);
void MoveToRS485FBSens(unsigned char,unsigned char,unsigned char);
void PrintForm0(void);
size_t print(const char str[]);
void LCDPrint(uint8_t stroka, uint8_t colonka, char * Text);
void setCursor(uint8_t col, uint8_t row);
uint16_t Mem_SecDayRdEnd(uint8_t TypeArch, uint8_t run);
float GetEm (uint8_t nRun);

extern float TAU_flt,                   // ��।���� ���祭��
             Ros_flt;
extern unsigned int  counttime;         // ���-�� ⠪⮢ ��� ���뢠��� �p�����
//extern struct hartdata  HARTDATA[];    // ����� ������ � �������� HART
//extern char hartnum;                     // ���-�� ��������
extern struct rs485data  RS485DATA[]; // ����� ������ � �������� RS-485
extern int rs485num;                     // ���-�� ��������
extern unsigned int rs485FBnum;
extern struct rs485FBdata  RS485FBDATA[];
extern double IndDP[],IndQ[],IndQs[];
extern char TPlat;
extern unsigned char FreqAlarm;          // ������� ������ �� ���������
extern unsigned char Err485;           // ������� ������ �� RS-485
extern char Bdisp;
extern int sms_send;
extern long ta2,tb2;
extern  unsigned char  Mtype[];
//extern  float MValue[3][4];  // ������ ������ - ����� ����� 0,1,2
//extern int noAsk;
extern char  RS485HF[];
//extern struct rs485FBdata  *p485FB;
extern unsigned char errMODBUS[];
//extern double KVolumeUZS[];
extern int SysTakt;
extern int NoWriteMgn;
extern double KoefP[];  // = {1.0,0.0980665,98.0655,0.980665};
extern double KoefdP[];  // = {1.0, 0.00980655, 0.0980665};
extern float KoRo[3][3];
extern double KoefEn[]; // = {238.846, 1.0, 0.277778};
extern uint8_t  FlagW1, FlagW2;

//---------------------------------------------------------------------------
//extern unsigned char          str[100];
char          str1[82],StatusForm[13];    // 0-��������� ; 1-��������
extern unsigned char Flag_Hartservice, Flag_Hartservice_1;
//---------------------------------------------------------------------------
struct printform  PRINTFORM;          // ��� ��p������� ������� �� ��������p
struct printform2 PRINTFORM2[4];
struct hartdata *ph;                     // ����� ���� ��������
extern struct rs485data *p485d;            // ����� ���� �������� RD-485
//---------------------------------------------------------------------------
extern struct bintime   BINTIME;
extern struct glconfig  GLCONFIG;
extern struct AddCfg    ADCONFIG;
extern struct calcdata  CALCDATA[];
//extern struct sumtdata  SUMTDATA[];      // ��� �����p������ ������ �� ������, ���, �����
extern struct calcdata  COPY_CALCDATA[];
extern struct tdata     TDATA;           // ��� �����p������ ������ �� ��p���, ���, �����
extern struct memdata   MEMDATA;          // ��p����p� ������ ������ - 26 ����
extern int ResetHartS;
extern unsigned char KbdScr;
extern int CErrMB;
extern uint8_t  SetW1,  SetW2;
extern uint8_t  In_But1,In_But2;
extern uint16_t DelayW1, DelayW2;	

char  str1[82]; 
unsigned char LCD_Light;
//---------------------------------------------------------------------------
//unsigned char column[] =
//  { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
///*1X*/0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
///*2X*/0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,
///*3X*/0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,
///*4X*/0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,
///*5X*/0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,
///*6X*/0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,
///*7X*/0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,
///*8X*/0x41,0xA0,0x42,0xA1,0xE0,0x45,0xA3,0xA4,0xA5,0xA6,0x4B,0xA7,0x4D,0x48,0x4F,0xA8,
///*9X*/0x50,0x43,0x54,0xA9,0xAA,0x58,0xE1,0xAB,0xAC,0xE2,0xAD,0xAE,0xC4,0xAF,0xB0,0xB1,
///*Ax*/0x61,0xB2,0xB3,0xB4,0xE3,0x65,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0x6F,0xBE,
///*Bx*/0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
///*Cx*/0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
///*Dx*/0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
///*Ex*/0x70,0x63,0xBF,0x79,0xE4,0x78,0xE5,0xC0,0xC1,0xE6,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,
///*Fx*/0xA2,0xB5,0xAF,0xC5,0x49,0x69,0x59,0x79,0xEF,0xDF,0xDF,0xDE,0xCC,0xEE,0xFF,0x20};

//const double    cnt5 = 100000.0;      //cnt1
//const double    cnt7 = 10000000.0;   //cnt2
float    cnt3 = 1000.0;       //cnt3
float    cnt6 = 1000000.0;    //cnt4
/*----------------------------------------------------------------------*/

//void clear_lcd (void) {

////   outportb(rcommlcd,0x01);
//   n_delay(3);                            // minimum 1.64ms
////   outportb(rcommlcd,0x0c);             // display on control
//   n_delay(2);                            // minimum 40 mks
//}
//-----------------------------------------------------------------------/
// /* ������������� �������.                                                */                                              /
//-------------------------------------------------------------------------
//void LCDinit(void) {
//   unsigned char lcd_init_data[] = { 0x30,0x30,0x30,0x38,0x08,0x01,0x06};
//                                        //  0x30 - ���뫪� ��᫥ ����祭�� ��⠭��
//                                        //  0x38 - 8 ��⮢, 2 ��p���, 5�7 �祪
//                                        //  0x08 -
//                                        //  0x01 - �p�� ��ᯫ��
//                                        //  0x06 - ���p����� �p� �����

//   n_delay(20);                           // ��㧠 ��᫥ ����祭�� ��⠭��
//   for (int j=0; j!=sizeof(lcd_init_data); j++) {
////      outportb(rcommlcd,lcd_init_data[j]);
//      n_delay(5);
//   }
//   clear_lcd();
//   LCDresligt();              // �몫���� ���ᢥ�� ��������
//}
//---------------------------------------------------------------------------



// *  䫠�� ���䨣��樨 ��࠭�� glconfig.scr_conf
// *  bit 0: ࠧ�襭�� ��࠭�    0
// *  bit 1: ࠧ�襭�� ��࠭�    1
// *  bit 2: ࠧ�襭�� ��࠭�    2
// *  bit 3: ࠧ�襭�� ��࠭�� 3-4
// *  bit 4: ࠧ�襭�� V�� ��� ���稪� �� ��࠭� 2
// *  bit 5: ࠧ�襭�� ��࠭�    5
// *  bit 6: ��४��票� ��࠭�    5  �� ���ࣨ�(=1) ��� ���⭮���(0)
// *  bit 7: ��⠭���� ᪮��� �� ��㣮�� �����
// *

void GlPrint() {                         // ����� �� ���
   unsigned char VisibleScr[16];        // �������� ��������� �������
   unsigned char mask;
   int i,k;
                                         // ������� ������� �� ���������,
                                        // � ������� �������� �������������

  mask = (GLCONFIG.scr_conf & 0x2F) | ((GLCONFIG.scr_conf & 0x8)<<1);
//printf("mask=%d\n",mask);
   for (i=0; i<6; i++) {                // ��������� �������� �������
      if ( (mask & (1 << i)) != 0 )
         VisibleScr[i] = 1;              // ����� ������������
      else
         VisibleScr[i] = 0;              // ����� �� ������������
   }

   for (i=1; i<3; i++)                    // ���������� �� 2 � 3 �����
      for (k=0; k<5; k++)                // �� ����� �������
         VisibleScr[i*5+k+1] = VisibleScr[k+1];

   for (i=PRINTFORM.nwindow; i<16; i++) {
      if (VisibleScr[i] != 0) {        // ���������� ������� ���������
         switch (i) {                   // �������� ��������������� ����
            case  0:                     InQueue(1,PrintForm0); break;
            case  1: PRINTFORM.prirun=0; InQueue(1,PrintForm1); break;
            case  2: PRINTFORM.prirun=0; InQueue(1,PrintForm2); break;
            case  3: PRINTFORM.prirun=0; InQueue(1,PrintForm5); break;
            case  4: PRINTFORM.prirun=0; InQueue(1,PrintForm4); break;
            case  5:  if (GLCONFIG.scr_conf & 0x40)
                       { PRINTFORM.prirun=0; InQueue(1,PrintForm6E);}
                      else
                       { PRINTFORM.prirun=0; InQueue(1,PrintForm6);}
                      break;
             
            case  6: PRINTFORM.prirun=1; InQueue(1,PrintForm1); break;
            case  7: PRINTFORM.prirun=1; InQueue(1,PrintForm2); break;

            case  8: PRINTFORM.prirun=1; InQueue(1,PrintForm5); break;
            case  9: PRINTFORM.prirun=1; InQueue(1,PrintForm4); break;
            case 10:  if (GLCONFIG.scr_conf & 0x40)
                       { PRINTFORM.prirun=1; InQueue(1,PrintForm6E);}
                      else
                       { PRINTFORM.prirun=1; InQueue(1,PrintForm6);}
                      break;
                      
            case 11: PRINTFORM.prirun=2; InQueue(1,PrintForm1); break;
            case 12: PRINTFORM.prirun=2; InQueue(1,PrintForm2); break;

            case 13: PRINTFORM.prirun=2; InQueue(1,PrintForm5); break;
            case 14: PRINTFORM.prirun=2; InQueue(1,PrintForm4); break;
            case 15:  if (GLCONFIG.scr_conf & 0x40)
                       { PRINTFORM.prirun=2; InQueue(1,PrintForm6E);}
                      else
                       { PRINTFORM.prirun=2; InQueue(1,PrintForm6);}
                      break;
 
            default: InQueue(1,PrintForm0);
         }
         break;
      }
      else {                         // ���������� ����� ����
          if (FlagW1 > 0) 
          { PRINTFORM.nwindow++;
            if (PRINTFORM.nwindow > (GLCONFIG.NumRun*5))
               PRINTFORM.nwindow = 0;
          } 
          else if (FlagW2 > 0)
          { PRINTFORM.nwindow--;
            if (PRINTFORM.nwindow < 0)
               PRINTFORM.nwindow = (GLCONFIG.NumRun*5);
          } 
      }
   }
   if (PRINTFORM.ligt) {
      PRINTFORM.ligt--;
      if (PRINTFORM.ligt == 0)       // �p��� �������� ��������p�
         LCDresligt();
   }
}
//---------------------------------------------------------------------------
//void LCDprint_80() {
//uint8_t i;
//   LCDputcommand(0x80);                 // ������� | ����p ������� | �������

//   for (int i=0; i<20; i++)
//      LCDputch(str[i]);

//   for (i=40; i<60; i++)
//      LCDputch(str[i]);

//   LCDputcommand(0xC0);              // ������� | ����p ������� | �������

//   for (i=20; i<40; i++)
//      LCDputch(str[i]);

//   for (i=60; i<80; i++)
//      LCDputch(str[i]);
//}
//---------------------------------------------------------------------------
//void LCDputch(unsigned char col) {
//   int j;
//   unsigned char symb;

//   symb = column[col];

////   outportb(rdatalcd,symb);
// for (j=0; j<10; j++);
//}
////---------------------------------------------------------------------------
//void LCDputs(unsigned char *str) {
//   unsigned char   column;

//   for (int i=0; i<20; i++) {
//      column = str[i];

//      if (column == 0)
//         break;

//      LCDputch(column);
//   }
//}
//---------------------------------------------------------------------------
//void LCDputcommand(unsigned char col) {

////   outportb(rcommlcd,col);
//   n_delay(1);
//}
//---------------------------------------------------------------------------
void LCDsetligt() {

     HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_SET); // ���������� ������� ���
     LCD_Light = 1;                       // ���ᢥ⪠ ����祭�
}
//--------------------------------------------------------
//void LCDSendPos(char Sym,unsigned char Row,unsigned char Col) {
//   char st[2];

//   st[0] = Sym;
//   st[1] = 0;

//   switch(Row) {
//      case 0:
//         LCDputcommand(0x80 | 0x00 | Col);   // ������� | ����p ��ᯫ�� | ������
//         break;
//      case 1:
//         LCDputcommand(0x80 | 0x40 | Col);
//         break;
//      case 2:
//         LCDputcommand(0x80 | 0x00 | 20+Col);
//         break;
//      case 3:
//         LCDputcommand(0x80 | 0x40 | 20+Col);
//         break;
//   }

//   LCDputs(st);
//}

//---------------------------------------------------------------------------
void LCDresligt() { //�������� LCD

   HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_RESET); // ���������� ������� ����
   LCD_Light = 0;                       // ���ᢥ⪠ �몫�祭�
}
//---------------------------------------------------------------------------
// ��������, ����p �������, ������� �p���
// �p��� ����������: �p� ��p��� ��������� - 12.8 ��, �p� ��������� - 8.7 ��
// ��������, ����p �������, ������� �p���
void PrintForm0(void) {
   static unsigned int  spd[] = {1200,2400,4800,9600,19200,38400,57600,11520};
   char C1,C2;
//   char  str1[82]; 
   
   if (StatusForm[0] == 0) {
      memset(StatusForm,0,sizeof(StatusForm));
      StatusForm[0]=1;  //0-��������� ; 1-��������
      //sprintf(str1,"    ��������-2021   ");
//      LCDPrint(0,0,str1);
   }
//	memset(str1,0,82); 
	sprintf(str1,"T %7.4f  %7.4f    ", Temperature1() , Temperature2()); //Temperature2()
   LCDPrint(0,0,str1);

   //sprintf(str1,"������ %03u   ����� %1u",GLCONFIG.NumCal,GLCONFIG.NumRun);
   sprintf(str1,"P %7.4f  %7.4f    ", PressIn() , PressOut()); //Temperature2()
   LCDPrint(1,0,str1);

   if (GLCONFIG.Speed_2==7)  C2='0'; else C2=' ';
   if (GLCONFIG.Speed_1==7)   C1='0'; else C1=' ';
   sprintf(str1," C1-%5u%c C2-%5u%c",spd[GLCONFIG.Speed_1],C1,spd[GLCONFIG.Speed_2],C2);
   LCDPrint(2,0,str1);

   sprintf(str1," %02u.%02u.%02u  %02u:%02u:%02u ",
      BINTIME.date,BINTIME.month,BINTIME.year%100,
      BINTIME.hours,BINTIME.minutes,BINTIME.seconds);
   LCDPrint(3,0,str1);
   Bdisp = 1;
}

//---------------------------------------------------------------------------
//��p����� �p�� ⥪��� p��室� � ��ꥬ� ��� �p㡮�p�����
//�p��� �믮������ - 29 ��
void PrintForm1() {
   int   run;
   float q;

   if (StatusForm[1] == 0) {            // 0-���ᨢ��� ; 1-��⨢���
      memset(StatusForm,0,sizeof(StatusForm));
      StatusForm[1]=1;
   }

   run = PRINTFORM.prirun;              // ����p ��⪨
   sprintf(str1,"��%d-%16s",run+1,CONFIG_IDRUN[run].NameRun);

   str1[18]='~';
   str1[19]='1';
   LCDPrint(0,0,str1);


      // ����� c ������ ������, ���. �3
   q = GetVm(run);

   if (q < 1.0e+03)
      sprintf(str1, "V� %10.3f �3    ", q);
   else
   if (q < 1.0e+06)
      sprintf(str1, "V� %10.3f �.�3  ", q / 1.0e+03);
   else
   if (q < 1.0e+09)
      sprintf(str1, "V� %10.3f ���.�", q / 1.0e+06);
   else
      if (q < 1.0e+12)
      sprintf(str1, "V� %10.3f ���.�3", q / 1.0e+09);
   else
      sprintf(str1,"    ������������    ");

   LCDPrint(1,0,str1);

   //����� �� �p����� ����� �� ��������� ������ ��������� ������
   //q = Mem_SecDayRdEnd(DAY, run);
   //���������� �� ������� �������, � ����� �� ������� �����������

   //--   �������� ��������� ������
                                
   if (Mem_SecDayRdEnd(DAY, run) == 1) {    // ������ �������
         q = MEMDATA.q;
   }
   else
      q = 0.0;

   if (q<cnt3)
      sprintf(str1,"Vc %10.3f �3    ",q);
   else {
      q /= cnt3;
      if (q<cnt3)
         sprintf(str1,"Vc %10.3f �.�3  ",q);
      else {
      q /= cnt3;
      if (q<cnt3)
         sprintf(str1,"Vc %10.3f ���.�3",q);
      else
         sprintf(str1,"    �������H�H��    ");
      }
   }
   LCDPrint(2,0,str1);

   q = (float)10500; //Mem_GetDayQ(run);//

   if (q<cnt3)
      sprintf(str1,"V� %10.3f �3    ",q);
   else {
      q /= cnt3;
      if (q<cnt3)
         sprintf(str1,"V� %10.3f �.�3  ",q);
      else {
        q /= cnt3;
        if (q<cnt3)
         sprintf(str1,"V� %10.3f ���.�3",q);
        else
         sprintf(str1,"    �������H�H��    ");
      }
   }
   LCDPrint(3,0,str1);

   Bdisp = 1;
}
//---------------------------------------------------------------------------
//��p����� ��p�� ������� ��������, ��p�����, �����p���p� � p������
//�p��� ���������� -  ��
void PrintForm2() {
   int   run, kR,i;
// float q;
   float Mscom;
   double q;
   long int qh;
   struct calcdata *pcalcdata;
   struct IdRun *pidrun;             // ��p����p� �����
   struct AdRun *aprun;           
   int ip;
   run = PRINTFORM.prirun;
   kR = run+1; if (kR==3) kR=4; kR=kR*16;
   pidrun = &CONFIG_IDRUN[run];
   aprun = &CONFIG_ADRUN[run];       

   SetCopyCalcData(run);              // �������� ������ ��� ���������� ����������
   pcalcdata = &COPY_CALCDATA[run];     // 㪠��⥫� �� ⥪���� ����
   if ((pidrun->TypeRun) == 8 || (pidrun->TypeRun) == 9)
    {
//      MoveToHartSens(1, pidrun->TypeRun, run); // ��।����c� ph
//printf("edt=%X %X t=%f %f\n",ph->edt,aprun->edt,pcalcdata->t,MValue[run][0]);
     sprintf(str1," ��%6.3f ��/� ��%1i ",pcalcdata->t,run+1);
     if (aprun->statdPh)
	 str1[0] = 0x23;                 // '#'-����� �裡 - ��� �⢥�
    }
   else if (pidrun->Vlag == 1)
    sprintf(str1," T�%6.2f �p.� ��%1i  ",pcalcdata->t,run+1);
   else
    sprintf(str1," T%6.2f �p.� ��%1i  ",pcalcdata->t,run+1);
   str1[18]='~';
   str1[19]='2';
   float Ped;
   if ((pidrun->constdata & 0x04)==0)
      str1[0]=0x2a;                   // �������� ���������\������ *
   else
   if (aprun->statt)
	 str1[0] = 0x23;                 // '#'-����� �裡 - ��� �⢥�

   LCDPrint(0,0,str1);

    if ((pidrun->TypeRun) == 8 || (pidrun->TypeRun) == 9)
   { Mscom= pcalcdata->Pa;
   sprintf(str1," Mo %11.3f ����",Mscom);}
   else
   { if (GLCONFIG.EDIZM_T.EdIzP == 0)
      Ped = pcalcdata->Pin;
     else
      Ped = pcalcdata->Pin*KoefP[GLCONFIG.EDIZM_T.EdIzP];
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       sprintf(str1," P  %8.4f ���/��2",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzP == 1)
       sprintf(str1," P  %8.5f     ���",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzP == 2)
       sprintf(str1," P  %8.2f     ���",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzP == 3)
       sprintf(str1," P  %8.4f     ���",Ped);
   }
   if ((pidrun->constdata & 0x02)==0)
      str1[0]=0x2a;//�������� ���������\������ * 
   else
   if (aprun->statP)
	 str1[0] = 0x23;         // '#'-������ ����� - ��� ������
						// '�' ��� '�' - ��� ������� �������� - 0\1 ���.\�����.
   if ((pidrun->TypeRun) != 8 && (pidrun->TypeRun) != 9)
   str1[2] = (pidrun->TypeP == 0) ? 0xE0 : 0xE8;
   LCDPrint(1,0,str1);


					// ������ ����p���� �� ����� - ������� ����
   if ((pidrun->TypeRun) == 4 || (pidrun->TypeRun) == 5
   || (pidrun->TypeRun) == 10) {
				//	��������� ��� 4 � ����� ������ ������������ �������
      if (!(GLCONFIG.scr_conf & 0x10))
     { q = pcalcdata->kQind;
	 sprintf(str1," ��� %9.5f      ",q);
      }
      else {
					// ������ �����  �p� p.�.
     if ((CONFIG_IDRUN[run].TypeRun) == 10)
    { q = aprun->Vtot_l;// qh=aprun->Vtot_h;
	 sprintf(str1," V��%13.2lf �3",q);
     }
      else
	 {q = Persist_GetQwCountTotal(run);
	  sprintf(str1," V��%13.2lf �3",q);
	 }

    }
   }
   else           // ������ ����p���� �� ����� - �� ����p����
    if ((GLCONFIG.TypeCount & kR) != 0)   //��������
     sprintf(str1," T4� %8.3f ��.� ",IndDP[run]);
   else {            // ������ ����p���� �� ����� - �� ����p����
      if ((pidrun->TypeRun) == 8 || (pidrun->TypeRun) == 9)
      sprintf(str1," Qm %8.3f  �/��� ",IndDP[run]);
      else
   { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
      Ped = IndDP[run];
     else
      Ped = IndDP[run]*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       sprintf(str1," dP %8.1f  ���/�2",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzmdP == 1)
       sprintf(str1," dP %8.3f    ���",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzmdP == 2)
       sprintf(str1," dP %8.2f    ����",Ped);
   }

      if ((pidrun->constdata & 0x01)==0)
	 str1[0]=0x2a;            // �������� ���������\������ 0\1
      else {
	 if ((CONFIG_IDRUN[run].TypeRun) == 3  || (CONFIG_IDRUN[run].TypeRun) == 2
	 || (CONFIG_IDRUN[run].TypeRun) == 11) {
	    if (aprun->statdPh || aprun->statdPl)
	       str1[0] = 0x23;               // '#'-������ ����� - ��� ������
	 }
	 else
	 if (aprun->statdPh)
	    str1[0] = 0x23;             // '#'-������ ����� - ��� ������
      }
   }
   LCDPrint(2,0,str1);

   q =  GetQ(run);

   if (((pidrun->TypeRun) == 8)||((pidrun->TypeRun) == 9)) // Micromotion
     sprintf(str1," Q %9.3f �3/��� ", pcalcdata->Q); //Qmass/aprun->dp);

   else
  {
   if (q<cnt3)
      sprintf(str1," Q %11.3f �3/�",q);
   else {
      q /= cnt3;

      if (q<cnt6)
	 sprintf(str1," Q %10.3f �.�3/�",q);
      else
	 sprintf(str1,"    �������H�H��    ");
   }
  }
// if (PRINTFORM2[run].err)             // �訡�� �室��� ��p����p��
//    str1[1] = '#';
      if (errMODBUS[run] > (CErrMB/SysTakt)) str1[0] = '#';
      if ((pidrun->constdata & 0x08)==0)
         str1[0]=0x2a;                // ���筨� ����⠭�\���稪 0\1
   LCDPrint(3,0,str1);
 
   Bdisp = 1;
}
//---------------------------------------------------------------------------
void PrintForm3(int sec) {              // ��p����� �p�� �p� ��p� ��⥬�
int KS;
 struct Id *glid;
 struct Id GID;

//     outportb(0x1D0,RESERV_CFG_PAGE);// � ����� �� ���
 //    �⥭��    �� ���  KS
//  poke(SEGMENT,16326, 0x00FE);
//      KS=peek(SEGMENT,16326);
     glid = &GID;
   sprintf(str1,"   �����  �������   ");
   LCDPrint(0,0,str1);


//   GetVersion(glid);
   sprintf(str1," v%2d  �� ��� = %04X  ",glid->Ver,KS);
   LCDPrint(1,0,str1);


   sprintf(str1,"      %2i  ���       ",sec);
   LCDPrint(2,0,str1);
   

   sprintf(str1,"����������%2i ���-���",2);
   LCDPrint(3,0,str1);


   if (sec == 1 && GLCONFIG.StartConfigFlag == 1)
      InQueue(1,PrintNeedCfg);
   Bdisp = 1;
}
//---------------------------------------------------------------------------

void PrintForm4() {               // ����������������� ��������� ��������
   int run;
   struct IdRun  *prun;                  // ��p����p� �����
   struct AdRun  *aprun;       
   float   value,fTmp,Ped;

   memset(StatusForm,0,sizeof(StatusForm));
   StatusForm[4]=1;         // 0-��������� ; 1-��������

   run = PRINTFORM.prirun;

   prun = &CONFIG_IDRUN[run];      // ��������� �� ������� �����
   aprun = &CONFIG_ADRUN[run];       
				      // ��������� �� � HART
   if ((prun->sensData[0].nInterface == tiHART) || (prun->sensData[0].nInterface == tiRS485DPH))
   {
//      MoveToHartSens(1, prun->TypeRun, run);
//      fTmp = GetCalibrValue(run,0,ph->valt);
//  fTmp = GetCalibrValue(run,0,MValue[run][0]);
   }
   else                                 // ��������� �� �  ��485
   if (prun->sensData[0].nInterface == tiAN_RS485) {
      MoveTo485Sens(0, run);
      fTmp = GetCalibrValue(run,0,p485d->Value);
   }
   else
      fTmp = aprun->t;
      if (prun->TypeRun==8)  // Rotamass
      { if (aprun->edt == 0x5C)
	 sprintf(str1," ��%5.0f ��/�3 ��%1i ", fTmp*1000.0, PRINTFORM.prirun+1);
        else
         sprintf(str1," ��%6.3f ��/� ��%1i  ", fTmp, PRINTFORM.prirun+1);
      }
      else
      if (prun->TypeRun==9)   // Micromition
        {
         if  (aprun->edt == 0x5C)
	  sprintf(str1," ��%5.0f ��/�3 ��%1i ", fTmp*1000.0, PRINTFORM.prirun+1);
         else
	  sprintf(str1," ��%6.3f ��/� ��%1i ", fTmp, PRINTFORM.prirun+1);
        }
   else
   sprintf(str1," T %5.2f �p.� ��%1i   ", fTmp, PRINTFORM.prirun+1);
   str1[18]='~';
   str1[19]='4';
                                        // ��������� �� � �� ������
   if (prun->sensData[0].nInterface != tiAN_WS)
      if (aprun->statt)
	 str1[0] = 0x23;                 // '#'-������ ����� - ��� ������
   LCDPrint(0,0,str1);
                                        // ��������� �� P hart
   if ((prun->sensData[1].nInterface == tiHART) || (prun->sensData[1].nInterface == tiRS485DPH))
   {
//      MoveToHartSens(2, prun->TypeRun, run);
//      fTmp = GetCalibrValue(run,1,MValue[run][1]);
   }
   else                                  // ��������� �� P  ��485
   if (prun->sensData[1].nInterface == tiAN_RS485) {
      MoveTo485Sens(1, run);
      fTmp = GetCalibrValue(run,1,p485d->Value);
   }
   else
      fTmp = aprun->P;
   if ((prun->TypeRun) == 8||(prun->TypeRun) == 9)
    {
    sprintf(str1," Mo %11.4f ����", fTmp);
    }
   else
   { if (GLCONFIG.EDIZM_T.EdIzP == 0)
      Ped = fTmp;
     else
      Ped = fTmp*KoefP[GLCONFIG.EDIZM_T.EdIzP];
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       sprintf(str1," P  %8.4f ���/��22",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzP == 1)
       sprintf(str1," P  %8.5f     ���",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzP == 2)
       sprintf(str1," P  %8.2f     ���",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzP == 3)
       sprintf(str1," P  %8.4f     ���",Ped);
   }
					/// ��������� �� P �� ������
   if (prun->sensData[1].nInterface != tiAN_WS)
      if(aprun->statP)
         str1[0] = 0x23;               // '#'-������ ����� - ��� ������
   LCDPrint(1,0,str1);

                               // ������ ����p���� - ��p�����(�������)
   if ((prun->TypeRun) == 4 || (prun->TypeRun) == 5
   || (prun->TypeRun) == 10) {
     value = GetQw4(run);
      if (value<cnt3)
         sprintf(str1," Q��  %9.3f �3/�",value);
      else {
         value /= cnt3;
         if (value<cnt3)
            sprintf(str1," Q��%9.3f �.�3/�",value);
         else
            sprintf(str1,"    �������H�H��    ");
      }
       if (errMODBUS[run] > (CErrMB/SysTakt)) str1[0] = '#';

   LCDPrint(2,0,str1);
       if (GLCONFIG.EDIZM_T.EdIzmE == 1)
        fTmp = GetQ(run)*prun->Heat/1000.0;
       else
        fTmp = GetQ(run)*prun->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1000.0;
       if (GLCONFIG.EDIZM_T.EdIzmE == 0)
       sprintf(str1," W %10.4f ����/�", fTmp/1000.0);
       else if (GLCONFIG.EDIZM_T.EdIzmE == 1)
       sprintf(str1," W %11.4f ���/�", fTmp);
       else
       sprintf(str1," W %11.4f ���  ", fTmp);

   LCDPrint(3,0,str1);
   }
   else {                 // ������ ����p���� - ����p����
                     // ��������� �� dPh - hart
      if ((prun->sensData[2].nInterface == tiHART) || (prun->sensData[2].nInterface == tiRS485DPH)) {
//         MoveToHartSens(4, prun->TypeRun, run);
//         fTmp = GetCalibrValue(run,2,MValue[run][2]); //dPh
      }
      else                   // ��������� �� dPh ��485
      if (prun->sensData[2].nInterface == tiAN_RS485) {
         MoveTo485Sens(2, run);
         fTmp = GetCalibrValue(run,2,p485d->Value);
      }
      else
         fTmp = aprun->dPh;
      if ((prun->TypeRun) == 8||(prun->TypeRun) == 9)
          sprintf(str1," Qm %10.4f �/���", fTmp);
      else
   { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
      Ped = fTmp;
     else
     Ped = fTmp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       sprintf(str1," dP� %8.1f ���/�2",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzmdP == 1)
       sprintf(str1," dP� %8.3f    ���",Ped);
      else if (GLCONFIG.EDIZM_T.EdIzmdP == 2)
       sprintf(str1," dP� %8.2f   ����",Ped);
   }
                                 // ��������� �� dPh �� ������
      if (prun->sensData[2].nInterface != tiAN_WS)

         if (aprun->statdPh)
            str1[0] = 0x23;       // '#'-������ ����� - ��� ������;

   LCDPrint(2,0,str1);
                                          // ������ ����p���� �� �����
         if ((prun->TypeRun) == 8 || (prun->TypeRun) == 9)
         {   sprintf(str1," T %8.3f  �C     ",aprun->ROdens);
            if (aprun->statdPh & 0x02)
            str1[0] = 0x23;                 //'#'-����� �裡 - ��� �⢥�
         }
         else
//         sprintf(str1," dP�  H� ����H����H ");
        { fTmp = GetQ(run)*prun->Heat/1000.0;
          sprintf(str1," W %11.4f ���/�", fTmp);
        }
                                        // ������� | ����p ��ᯫ�� | ������
   LCDPrint(3,0,str1);
   }
   Bdisp = 1;
}
//---------------------------------------------------------------------------
void PrintForm5()  // "����� ����" �������� 3-� �����
{
   int run,i_flt;
   struct IdRun  *prun;                 // ��p����p� ��⪨
   struct AdRun  *aprun;              
   float   fTmp;
   double  value;

   memset(StatusForm,0,sizeof(StatusForm));
   StatusForm[5]=1;        // 0-��������� ; 1-��������

   run = PRINTFORM.prirun;

   prun = &CONFIG_IDRUN[run];         // ��������� �� ������� �����
   aprun = &CONFIG_ADRUN[run];       
                                          // ��������� �� � ������
   if (prun->sensData[0].nInterface == tiAN_WS) {
         fTmp = flt_ankan[run][0];
      if (aprun->edt == 0x20)
         sprintf(str1," T %5.2f    ��%1i     ", fTmp, PRINTFORM.prirun+1);
      else
         sprintf(str1," T %5.2f    ��%1i     ", fTmp, PRINTFORM.prirun+1);
      if ((fTmp < prun->sensData[0].aTable[0].code) ||
          (fTmp > prun->sensData[0].aTable[m_nPoints-1].code))
          str1[0] = 0x23;            // '#'-����� �� ����� ����������
   }
   else                             // ��������� �� � - hart
   if (prun->sensData[0].nInterface == tiHART) {
//      MoveToHartSens(1, prun->TypeRun, run);

      if ((prun->TypeRun==8) || (prun->TypeRun==9))  // Rotamass
      {
         if ((ph->edt == 0x5B) || (ph->edt == 0x60))
	  sprintf(str1,"  �� %5.3f ��/� ��%1i ", ph->valt, PRINTFORM.prirun+1);
         else
         if  ((ph->edt == 0x5C) || (ph->edt == 0x5E))
	  sprintf(str1," �� %4.0f ��/�3 ��%1i ", ph->valt*1000.0, PRINTFORM.prirun+1);
         else
       	  sprintf(str1," �� %5.3f %02X?? ��%1i ",0.0, ph->edt,  PRINTFORM.prirun+1);
      }
//      else
//       if (ph->edt == 0x20)
//         sprintf(str1," T %5.2f �p.�  ��%1i ", MValue[run][0], PRINTFORM.prirun+1);
//       else
//         sprintf(str1," T %5.2f %02X??  ��%1i ", MValue[run][0], ph->edt, PRINTFORM.prirun+1);

      if (aprun->statt & 0x02)
          str1[0] = 0x23;            // '#'-������ ����� - ��� ������
   }
   else                        // ��������� �� � - ��485
   if (prun->sensData[0].nInterface == tiAN_RS485) {
      MoveTo485Sens(0, run);

      sprintf(str1," T %7.1f �� ��%1i     ", p485d->Value, PRINTFORM.prirun+1);

      if (aprun->statt & 0x02)
          str1[0] = 0x23;              // '#'-������ ����� - ��� ������
   }

   str1[18] = '~';
   str1[19] = '3';

   LCDPrint(0,0,str1);

                                     // ��������� �� P ������
   if (prun->sensData[1].nInterface == tiAN_WS) {
         fTmp = flt_ankan[run][1];
      switch(aprun->edp) {              // ��������
         case 0xED:
            sprintf(str1," P  %11.5f    ", fTmp);
            break;
         case 0x0C:
            sprintf(str1," P  %11.2f    ", fTmp);
            break;
         case 0x0A:
            sprintf(str1," P %8.3f        ", fTmp);
            break;
         default:
            sprintf(str1," P  %11.5f    ", fTmp);
      }
      if ((fTmp < prun->sensData[1].aTable[0].code) ||
          (fTmp > prun->sensData[1].aTable[m_nPoints-1].code))
          str1[0] = 0x23;         // '#'-����� �� ����� ����������
   }
   else                      // ��������� �� P - hart
   if (prun->sensData[1].nInterface == tiHART) {
//      MoveToHartSens(2, prun->TypeRun, run);
      switch(ph->edp) {                  // ��������
         case 0xED:                      // ���
//            sprintf(str1," P  %11.5f M�� ", MValue[run][1]); //ph->valp
            break;
         case 0x0C:                     // ���
//            sprintf(str1," P  %11.2f ��� ", MValue[run][1]);
            break;
         case 0x0A:                     // ��/��2
//            sprintf(str1," P  %8.4f ���/��2", MValue[run][1]);
            break;
//Micro
         case 0x4E:                    // �����
            sprintf(str1," M�% 12.4f  ���� ", ph->valp);
            break;
         case 0x3E: // �����
            sprintf(str1," M�% 12.4f  ���� ", ph->valp);
            break;
         case 0x3D:                     // ��
            sprintf(str1," M�% 12.4f  ���� ", ph->valp);
            break;
         default:
        if ((prun->TypeRun==8) || (prun->TypeRun==9))  // Rotamass
            sprintf(str1," M�  %11.5f ???",   ph->valp);
            else
            sprintf(str1,"  P  %11.5f ???",   ph->valp);
      }
      if (aprun->statP & 0x02)
         str1[0] = 0x23;                   // '#'-������ ����� - ��� ������
   }
  else                                    // ��������� �� P - an485
   if (prun->sensData[1].nInterface == tiAN_RS485) {
      MoveTo485Sens(1, run);

      sprintf(str1," P  %8.1f ��    ", p485d->Value);

      if (aprun->statP & 0x02)
         str1[0] = 0x23;             // '#'-������ ����� - ��� ������
   }
   LCDPrint(1,0,str1);
  
   if ((prun->TypeRun) == 4 || (prun->TypeRun) == 5
     || (prun->TypeRun) == 10)
   {
    value = (float)GetQw3(run);
      if (value<cnt3)
         sprintf(str1," Qp�  %9.3f �3/�",value);
      else {
         value /= cnt3;
         if (value<cnt3)
            sprintf(str1," Qp�%9.3f �.�3/�",value);
         else
            sprintf(str1,"    �������H�H��    ");
      }
      if (errMODBUS[run] > (CErrMB/SysTakt)) str1[0] = '#';
      if ((prun->constdata & 0x08)==0)
         str1[0]=0x2a;           // �������� ���������\������ *

      LCDPrint(2,0,str1);
       value = COPY_CALCDATA[run].kQind;
       sprintf(str1," ��� %9.5f      ", value);
      LCDPrint(3,0,str1);
   }
   else {                         // ������ ����p���� - ����p����
                             // ��������� �� dPh ������
      if (prun->sensData[2].nInterface == tiAN_WS) {
         fTmp = flt_ankan[run][2];
         switch (aprun->eddph) {
            case 0x0C:
               sprintf(str1,"dP� %10.3f      ", fTmp);
               break;
            case 0x82:
            case 0xEE:
               sprintf(str1,"dP� %8.3f        ", fTmp);
               break;
            default:
               sprintf(str1,"dP� %10.3f      ", fTmp);
         }
         if ((fTmp < prun->sensData[2].aTable[0].code) ||
             (fTmp > prun->sensData[2].aTable[m_nPoints-1].code))
            str1[0] = 0x23;            // '#'-����� �� ����� ����������
      }
      else                         // ��������� �� dPh - hart
      if (prun->sensData[2].nInterface == tiHART) {
//         MoveToHartSens(4, prun->TypeRun, run);

         switch (ph->eddp) {
            case 0x0C:
//               sprintf(str1," dP� %10.4f ��� ", MValue[run][2]);//ph->valdp
               break;
            case 0x82:
            case 0xEE:
//               sprintf(str1," dP� %8.2f ��c/�2", MValue[run][2]);
               break;
            case 0x09:
//               sprintf(str1," dP� %8.2f ��/c�2", MValue[run][2]);
               break;
            case 0x0A:
//               sprintf(str1," dP� %7.5f ���/��2", MValue[run][2]);
               break;
            case 0x4E:
            case 0x4B:
            case 0x4D:
                 sprintf(str1," Qm   %8.3f T/���", ph->valdp);
               break;
            default:
//               sprintf(str1,"  dP� %10.3f ???", MValue[run][2]);
							break;
         }
         if (aprun->statdPh & 0x02)
            str1[0] = 0x23;        // '#'-������ ����� - ��� ������;
      }
      else                         // ��������� �� dPh - an485
      if (prun->sensData[2].nInterface == tiAN_RS485) {
         MoveTo485Sens(2, run);

         sprintf(str1,"  dP� %8.1f ��   ", p485d->Value);

         if (aprun->statdPh & 0x02)
            str1[0] = 0x23;             // '#'-������ ����� - ��� ������;
      }

   LCDPrint(2,0,str1);
                                      // ������ ����p���� �� ����� ���������
      if( ((prun->TypeRun) == 2) || ((prun->TypeRun) == 3)
          || ((prun->TypeRun) == 11))  {
                                        // ��������� �� dPl ������
         if (prun->sensData[3].nInterface == tiAN_WS) {
         fTmp = flt_ankan[run][3];

            switch (aprun->eddpl) {
               case 0x0C:
                  sprintf(str1," dP� %10.3f     ", fTmp);
                  break;
               case 0x82:
               case 0xEE:
                  sprintf(str1," dP� %8.3f       ", fTmp);
                  break;
               default:
                  sprintf(str1," dP� %10.3f     ", fTmp);
            }
            if ((fTmp < prun->sensData[3].aTable[0].code) ||
                (fTmp > prun->sensData[3].aTable[m_nPoints-1].code))
               str1[0] = 0x23;       // '#'-����� �� ����� ����������
         }
         else                          // ��������� �� dPl - hart
         if (prun->sensData[3].nInterface == tiHART) {
//            MoveToHartSens(8, prun->TypeRun, run);
            switch (ph->eddp) {
               case 0x0C:
//                  sprintf(str1," dP� %10.4f ��� ", MValue[run][3]);//ph->valdp
                  break;
               case 0x82:
               case 0xEE:
//                  sprintf(str1," dP� %8.3f ��c/�2", MValue[run][3]);
                   break;
               case 0x09:
//                  sprintf(str1," dP� %8.3f �c/��2", MValue[run][3]);
                  break;
               case 0x0A:
//                  sprintf(str1," dP� %7.5f ��c/��2", MValue[run][3]);
                  break;
               default:
//                  sprintf(str1," dP�  %10.3f ??? ", MValue[run][3]);
									break;
            }

            if (aprun->statdPl & 0x02)
               str1[0] = 0x23;          // '#'-������ ����� - ��� ������;
         }
         else                         // ��������� �� dPl - an485
         if (prun->sensData[3].nInterface == tiAN_RS485) {
            MoveTo485Sens(3, run);
            sprintf(str1," dP� %8.1f ��    ", p485d->Value);
            if (aprun->statdPl & 0x02)
               str1[0] = 0x23;          // '#'-������ ����� - ��� ������;
         }
      }
      else
         if (((prun->TypeRun) == 8) || ((prun->TypeRun) == 9)) //Rotamass
         { sprintf(str1," T %10.3f �C    ",aprun->ROdens);
           if (aprun->statdPh & 0x02)
               str1[0] = 0x23;      // '#'-������ ����� - ��� ������;
         }
         else
         sprintf(str1," dP�  H� ����H����H ");
                                        // ������� | ����p ��ᯫ�� | ������
   LCDPrint(3,0,str1);
   }
Bdisp = 1;
}

//..................................................................
void PrintNeedCfg() {

   sprintf(str1,"   ENTER PGM KEY    ");
   LCDPrint(0,0,str1);

   sprintf(str1,"                    ");
   LCDPrint(1,0,str1);

    sprintf(str1,"     ���������      ");
   LCDPrint(2,0,str1);

   sprintf(str1,"    ������������    ");
   LCDPrint(3,0,str1);
}
//---------------------------------------------------------------------------
/*
void Kl_0() {                           // �⥭�� ���� ������ �� ࠡ�祬 室�
   unsigned int a;
   static unsigned int  KlTime;         // �p��� ������ ������
//   if (KbdScr)                          // �᫨ ��࠭ ����� ��������ன,
//      return;                           // � ��祣� �� ������
//   a=inport(PORT_DATA);  //  ���� ������ ��४�祭�� ��࠭��
   if (a & 0x0020)         // ����⨥ ������
      KlTime = 0;                       // �p���� �p��� ������ ������
   else
      KlTime++;                         // 㢥����� �p��� ������ ������
   if (KlTime == 4) {
      PRINTFORM.ligt = 60;              // �p��� ᢥ祭�� �������p�, ᥪ
      if (Bdisp)
      {  InQueueInt(1, SetWindows);
//      LCDsetligt();                     // ������� ���ᢥ�� �������p�
      Bdisp = 0;
      }
     // ������� ��室 �� ०��� HART<->COM, �᫨ ���᫨⥫� � �⮬ ०���
      if ((Flag_Hartservice ==1)||(Flag_Hartservice_1 ==1)) ResetHartS=1;
//      outportb(0x1D1, (inp(0x1D1) & 0xfe)); //����� ᨣ���� ���ਨ
      sms_send = 1;
      ta2 = 0; tb2 = 0;  //����� ᨣ��� (������) ���ਨ
   }
}
/////////////////////////////////////////////////////////////////////
void Kl_1() {                           // �⥭�� ���� ������ �� ����⪥ � �� ��横������� �� ���.����-��
   unsigned int a;
   static unsigned int  KlTime;         // �p��� ������ ������

//   a=inport(PORT_DATA);  //  ���� ������ ��४�祭�� ��࠭��
   if (a & 0x0020)         // ����⨥ ������
      KlTime = 0;                       // �p���� �p��� ������ ������
   else
      KlTime++;                         // 㢥����� �p��� ������ ������

   if (KlTime == 4) {
      GLCONFIG.StartConfigFlag = 0;     // ����� 䫠� ���. ����-��
//      outportb(0x1D0,BASE_CFG_PAGE);
//      pokeb(SEGMENT,1076,0); // ������� StartConfigFlag = EraseConf = 0
//      Mem_ConfigWr();                   // ������� ��p���p� ����-�� ���᫨⥫�
      PRINTFORM.nwindow = -1;           // ��� ��室� �� �᭮���� ��࠭
   }
}
/////////////////////////////////////////////////////////////////////
void Kl_2() {                           // �⥭�� ���� ������ �� ����⪥

    return;

// ********************************************************************
   unsigned int a;
   static unsigned int  KlTime;         // �p��� ������ ������

   a=inport(PORT_DATA);  //  ���� ������ ��४�祭�� ��࠭��
   if (a & 0x0020)
      KlTime = 0;                       // �p���� �p��� ������ ������
   else
      KlTime++;                         // 㢥����� �p��� ������ ������

   if (KlTime == 4) {                   // ᬥ�� ���
      OnButton();                       // ��⠭���� �������� ��ࠬ��஢
   }
// ******************************************************************** /
}
//---------------------------------------------------------------------------
void SetWindows() {

   PRINTFORM.nwindow++;

   if (PRINTFORM.nwindow > GLCONFIG.NumRun*5)
      PRINTFORM.nwindow = 0;
//printf("SetW- nwindow=%d \n",PRINTFORM.nwindow);
   LCDsetligt();                     // ������� ���ᢥ�� �������p�
   InQueue(1,GlPrint);
}*/
//---------------------------------------------------------------------------
/*
void LCDtest() {

   for (int i=0; i<256; i++) {

      for (int j=0; j<20; j++)
         str[j] = (unsigned char)i;
      LCDputcommand(0x80 | 0x0 | 0);    // ������� | ����p ��ᯫ�� | ������
      LCDputs(str);

      for (j=0; j<20; j++)
          str[j] = (unsigned char)i+1;
      LCDputcommand(0x80 | 0x40 | 0);   // ������� | ����p ��ᯫ�� | ������
      LCDputs(str);

      for (j=0; j<20; j++)
         str[j] = (unsigned char)i+2;
      LCDputcommand(0x80 | 0x0 | 20);   // ������� | ����p ��ᯫ�� | ������
      LCDputs(str);

      for (j=0; j<20; j++)
         str[j] = (unsigned char)i+1;
      LCDputcommand(0x80 | 0x40 | 20);  // ������� | ����p ��ᯫ�� | ������
      LCDputs(str);

      n_delay(250);
   }
}
*/
//---------------------------------------------------------------------------
void Disp_SetErrForm2(int run) {

   PRINTFORM2[run].err = 1;
}
//---------------------------------------------------------------------------
void Disp_ResErrForm2(int run) {

   PRINTFORM2[run].err = 0;
}
//---------------------------------------------------------------------------
/*
void MoveToHartSens(
   unsigned char Param,                   // ���������� ��������
   unsigned char TypeRun,                // ����� ��������
   unsigned char Run) {                  // ����� �����

   unsigned char Type;

   switch (Param) {
      case 1:                          // �����������
         switch(TypeRun) {
            case 0:                     // (P+T+dP)
               Type = 7;
               break;
            case 1:                     // P + T + dP
               Type = 1;
               break;
            case 2:                     // (P+T+dP) + dp
               Type = 7;
               break;
            case 3:                     // P + T + dP + dp
               Type = 1;
               break;
            case 4:                     // P + T + ��
               Type = 1;
               break;
            case 5:                     // (P+T) + ��
//               Type = 3;
               Type = 1;
               break;
            case 6:
               Type = 5;               //  ��-3
//               Type = 3;              // (P+T) + dP
               break;
            case 7:                     // (P+dP) + T
               Type = 1;
               break;
	    case 8:                     // Rotamass
	       Type = 7;
	       break;
	    case 9:                     // Micromotion
	       Type = 7;
	       break;
            case 10:                     // P + T + �� ��� MODBUS
               Type = 1;
               break;
            case 11:                     // P + T + �� ��� MODBUS
               Type = 1;
               break;
         }
         break;
      case 2:                            // ��������
         switch(TypeRun) {
            case 0:                     // (P+T+dP)
               Type = 7;
               break;
            case 1:                     // P + T + dP
               Type = 2;
               break;
            case 2:                     // (P+T+dP) + dp
               Type = 7;
               break;
            case 3:                     // P + T + dP + dp
               Type = 2;
               break;
            case 4:                     // P + T + ��
               Type = 2;
               break;
            case 5:                     // P+T + �� ��
               Type = 2;
               break;
            case 6:
               Type = 5;               // ��-3
               break;
            case 7:                     // (P+dP) + T
               Type = 6;
               break;
 	    case 8:                     // Rotamass
	       Type = 7;
	       break;
	    case 9:                     // Micromotion
	       Type = 7;
               break;
            case 10:                     // P+T + �� ��� MODBUS
               Type = 2;
               break;
            case 11:                     // (P+dP) + T + dp
               Type = 6;
               break;

         }
         break;
      case 4:                          // ������� �������
         switch(TypeRun) {
            case 0:                     // (P+T+dP)
               Type = 7;
               break;
            case 1:                     // P + T + dP
               Type = 4;
               break;
            case 2:                     // (P+T+dP) + dp
               Type = 7;
               break;
            case 3:                     // P + T + dP + dp
               Type = 4;
               break;
            case 6:
               Type = 5;               // ��-3
//               Type = 4;              // (P+T) + dP
               break;
            case 7:                     // (P+dP) + T
               Type = 6;
               break;
             case 8:                     // Rotamass
               Type = 7;
               break;
            case 9:                     // Micromotion
               Type = 7;
               break;
           case 11:                     // (P+dP) + T +dp
               Type = 6;
               break;
         }
         break;
      case 8:
         switch(TypeRun) {
            case 0:
               Type = 7;
               break;
            case 1:
               Type = 8;
               break;
            case 2:
               Type = 8;
               break;
            case 3:
               Type = 8;
               break;
            case 6:
               Type = 8;
               break;
            case 7:
               Type = 6;
               break;
            case 11:
               Type = 8;
               break;
         }
         break;
   }
 
   ph  = &HARTDATA[0];
   for (int ii=0; ii<hartnum; ii++) {    // ����� � HARTDATA ���� ������
   if (ph->type == Type && ph->run == Run)
         break;
      ph++;
   }
}
*/
//--------------------------------------------
/*
void MoveToRS485FBSens(
   unsigned char Param,                 // �����塞� ��ࠬ���
   unsigned char TypeRun,               // ����� ���稪��
   unsigned char Run)                  // ����� ��⪨
{   unsigned char Type;
   int ii;

   switch (Param) {
      case 1:                           // ⥬������
         switch(TypeRun & 0x3F) {
            case 0:                     // (P+T+dP)
               Type = 7;
               break;
            case 1:                     // P + T + dP
               Type = 1;
               break;
            case 2:                     // (P+T+dP) + dp
               Type = 7;
               break;
            case 3:                     // P + T + dP + dp
               Type = 1;
               break;
            case 4:                     // P + T + ��
               Type = 1;
               break;
            case 5:                     // (P+T) + ��
               Type = 3;
               break;
            case 6:
               Type = 5;               // ��-3
//               Type = 3;              // (P+T) + dP
               break;
            case 7:                     // (P+dP) + T
               Type = 1;
               break;
            case 10:                     // P + T + �� ��� MODBUS
               Type = 1;
               break;
         }
         break;
      case 2:                           // ��������
         switch(TypeRun & 0x3F) {
            case 0:                     // (P+T+dP)
               Type = 7;
               break;
            case 1:                     // P + T + dP
               Type = 2;
               break;
            case 2:                     // (P+T+dP) + dp
               Type = 7;
               break;
            case 3:                     // P + T + dP + dp
               Type = 2;
               break;
            case 4:                     // P + T + ��
               Type = 2;
               break;
            case 5:                     // (P+T) + ��
               Type = 3;
               break;
            case 6:
               Type = 5;               // ��-3
//               Type = 3;              // (P+T) + dP
               break;
            case 7:                     // (P+dP) + T
               Type = 6;
             break;
            case 10:                     // P+T + �� ��� MODBUS
               Type = 2;
               break;
         }
         break;
      case 4:                           // ���孨� ��९��
         switch(TypeRun & 0x3F) {
            case 0:                     // (P+T+dP)
               Type = 7;
               break;
            case 1:                     // P + T + dP
               Type = 4;
               break;
            case 2:                     // (P+T+dP) + dp
               Type = 7;
               break;
            case 3:                     // P + T + dP + dp
               Type = 4;
               break;
            case 6:
               Type = 5;               // ��-3
//               Type = 4;              // (P+T) + dP
               break;
            case 7:                     // (P+dP) + T
               Type = 6;
               break;
         }
         break;
      case 8:
         switch(TypeRun & 0x3F) {
            case 0:
               Type = 7;
               break;
            case 1:
               Type = 8;
               break;
            case 2:
               Type = 8;
               break;
            case 3:
               Type = 8;
               break;
            case 6:
               Type = 8;
               break;
            case 7:
               Type = 6;
               break;
         }
         break;
   }

//   p485FB  =  &RS485FBDATA[Run];

   for (ii=0; ii < rs485FBnum; ii++) {    // ���� � HARTDATA ��� ���稪
      if (p485FB->type == Type && p485FB->run == Run)
         break;
      p485FB++;
   }

//printf("type=%d run=%d Type=%d\n",p485FB->type,p485FB->run,Type);
 }
*/
 //---------------------------------------------------------------------------
void MoveTo485Sens(
   unsigned char Param,                  // �����塞� ��ࠬ���
   unsigned char Run) {                  // ����� ��⪨

   //p485d  = &RS485DATA[];

   for (int ii=0; ii<rs485num; ii++) {   // ���� � RS485DATA ��� ���稪
      if (RS485DATA[ii].type == Param && RS485DATA[ii].run == Run)
         break;
//      p485d++;
   }

}
//---------------------------------------------------------------------------
//
//void MoveToRS485FBSens2(
//   unsigned char Param,                  // �����塞� ��ࠬ���
//   unsigned char Run) {                  // ����� ��⪨

//   p485FB  = &RS485FBDATA[0];

//   for (int ii=0; ii<rs485FBnum; ii++) {   // ���� � RS485DATA ��� ���稪
//      if (p485FB->type == Param && p485d->run == Run)
//         break;
//      p485FB++;
//   }
//}
///
//----------------------------------------------------------------------------

void PrintForm6() {
                                           // ������ � ����������
   if (CONFIG_IDRUN[PRINTFORM.prirun].Dens == 1) {
// 1-� ������ - ��������� � ������� ------------------------------
      sprintf(str1," RO��� %7.4f    ~5", CONFIG_ADRUN[PRINTFORM.prirun].ROdens);  //Ros_flt*KoRo[0][GLCONFIG.EDIZM_T.T_SU]);

//      if (Ros_flt > 1.05 ||  Ros_flt < 0.66)
        if (Err485 || FreqAlarm)
         str1[0] = 0x23;                  // '#'-����� �� �������
      LCDPrint(0,0,str1);

// 2-� ������ - ��������� � ������ �� ����� ----------------------
      sprintf(str1," RO��� %7.4f ��/�3", CALCDATA[PRINTFORM.prirun].ROnom);
      if (((CONFIG_IDRUN[PRINTFORM.prirun].constdata & 0x10) == 0)
       || Err485 || FreqAlarm) {
         str1[0]=0x2a;                    // '*' - �������� ���������\������ 0\1
      }
      LCDPrint(1,0,str1);

// 3 ������ - ������ � ���������� - TAU_flt ----------------------
      if (CONFIG_IDRUN[PRINTFORM.prirun].sensData[4].nInterface == 4)
      sprintf(str1," ������ %7.3f  ���", TAU_flt);
      else sprintf(str1," ���    %7.3f     ", TAU_flt);
      LCDPrint(2,0,str1);
   }
   else {                          // ������ �� � ����������
/// 1-� ������
      sprintf(str1,"   ���������  �/� %d ",PRINTFORM.prirun+1);
      LCDPrint(0,0,str1);
/// 2-� ������-
      sprintf(str1,"    �� ����������  ");
      LCDPrint(1,0,str1);
/// 3 ������
      sprintf(str1,"                    ");
      LCDPrint(2,0,str1);   }

/// 4 ������ - ����������� �����������       ----------------------
      sprintf(str1,"K-� ����.=%8.6f  ",COPY_CALCDATA[PRINTFORM.prirun].K);
//    sprintf(str1,"                    ");
      LCDPrint(3,0,str1);
      Bdisp = 1;
//printf("Form-6      Counttime=%d\n",counttime);
}
//----------------------------------------------------------------------------
void PrintForm6E()   // �������
{
   int   run,Ede;
   float e;
   float hs;

   if (StatusForm[1] == 0) {             // 0-��������� ; 1-��������
      memset(StatusForm,0,sizeof(StatusForm));
      StatusForm[1]=1;
   }
   run = PRINTFORM.prirun;              // ����p �����
   Ede = GLCONFIG.EDIZM_T.EdIzmE;
   if (Ede == 0)  Ede = 1;
   hs = CONFIG_IDRUN[run].Heat*KoefEn[Ede];
//   sprintf(str,"  Energy �/� %d    ",run+1);
  if (Ede == 1)
   sprintf(str1,"Hs %5.2f ���/�3 ��%1i ", hs, run+1);
  else
   sprintf(str1,"Hs %6.3f����/�3 ��%1i", hs, run+1);

   str1[18]='~';
   str1[19]='5';

  LCDPrint(0,0,str1);
         // ������� c ������ ������, ���
   e = 600.0;  //GetEm(run)*KoefEn[Ede]/cnt3;  //
   if (e < cnt3)
      if (Ede == 1)
       sprintf(str1, "E� %10.6f    ���",e );
      else
       sprintf(str1, "E� %10.6f  ���*�",e );
   else
   if (e < cnt6)
      if (Ede == 1)
       sprintf(str1, "E�%10.6f ���.���", e/cnt3);
      else
       sprintf(str1, "E�%11.6f���.���", e/cnt3);
   else
   if (e < 1.0e+10)
      if (Ede == 1)
        sprintf(str1, "E�%11.6f���.���", e/cnt6);
      else
        sprintf(str1, "E�%11.6f���.���", e/cnt6);
   else
      sprintf(str1,"    �������H�H��    ");

  LCDPrint(1,0,str1);


   //--   �������� ��������� ������
   if (Mem_SecDayRdEnd(DAY, run) == 1)
   {
         e = MEMDATA.FullHeat*KoefEn[Ede];

   }
   else
      e = 0.0;

   if (e<cnt3)
      if (Ede == 1)
      sprintf(str1,"Ec %10.6f    ���",e);
      else
      sprintf(str1,"Ec %10.6f  ���*�",e);
   else {
      e /= cnt3;
      if (e<cnt3)
         if (Ede == 1)
           sprintf(str1,"Ec %10.6f    ���",e);
         else
           sprintf(str1,"Ec %10.6f  ���*�",e);
      else
      { e /= cnt3;
        if (e<10000.0)
        { if (Ede == 1)
           sprintf(str1,"E�%11.6f���.���",e);
          else
           sprintf(str1,"E�%11.6f  ���*�",e);
        }
        else
         sprintf(str1,"    �������H�H��    ");
      }
   }
  LCDPrint(2,0,str1);

   e = 55000.0; //Mem_GetDayE(run)*KoefEn[Ede];//  � ������ �����

   if (e<cnt3)
      if (Ede == 1)
        sprintf(str1,"E� %10.6f    ���",e);
      else
        sprintf(str1,"E� %10.6f  ���*�",e);
   else {
      e /= cnt3;
      if (e<cnt3)
         if (Ede == 1)
           sprintf(str1,"E� %11.6f   ���",e);
         else
           sprintf(str1,"E� %11.6f ���*�",e);
      else
      { e /= cnt3;
        if (e<10000.0)
         if (Ede == 1)
           sprintf(str1,"E�%11.6f���.���",e);
         else
           sprintf(str1,"E�%11.6f  ���*�",e);
        else
         sprintf(str1,"    �������H�H��    ");
      }
   }

  LCDPrint(3,0,str1);
   Bdisp = 1;
}
//===========================================================================
// ����� �� ����� LCD
void LCDPrint(uint8_t stroka, uint8_t colonka, char * Text)
{
  setCursor(colonka, stroka);
  print(Text);
 }    

//---------------------------------------------------------------------------
void SetWindows1() {
 //  if (SetW1 == 0)
   { PRINTFORM.nwindow++;
     if (PRINTFORM.nwindow > GLCONFIG.NumRun*5)
      PRINTFORM.nwindow = 0;
     HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_SET); // ���������� ������� ���
     PRINTFORM.ligt = 60; 
     FlagW1 = 1; FlagW2 = 0;
     DelayW1 = 0;
     GlPrint();    
   }
    SetW1++;
//    if (SetW1 > 1) 
//    {  SetW1 = 0;
//    }   
       In_But1 = 0;
    
}
//---------------------------------------------------------------------------
void SetWindows2() {
 //  if (SetW2 == 0)
   { PRINTFORM.nwindow--;
     if (PRINTFORM.nwindow < 0)
      PRINTFORM.nwindow = GLCONFIG.NumRun*5;
     HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_SET); // ���������� ������� ����
     PRINTFORM.ligt = 60;  
     FlagW2 = 1; FlagW1 = 0;
     DelayW2 = 0;
     GlPrint();    
   }
//    SetW2++; if (SetW2 > 1) 
//    { SetW2 = 0;
//    }    
      In_But2 = 0;

}
//------------------------------------------------------------------------
void SetWindows()
{
    if (In_But1 > BUTMAX) 
      { SetWindows1();
      }   
    if (In_But2 > BUTMAX) 
     { SetWindows2();
     }   
 }
