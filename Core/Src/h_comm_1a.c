#include "main.h"
#include "string.h"
#include "h_mem.h"
#include "h_comm_1.h"
#include "comm_str.h"
#include "h_calc.h"
#include "h_persist.h"
#include "h_smooth.h"
#include <math.h>


#define   maxindex          384   //768
#define   transindex        526
#define   maxcounttimeaut   20          // ?a�?. ��?-o� ?a�?�o �� 25 ??
#define   SyncByte          0xAA        // ????p�ia?? �p? ?a�p�?N
#define   wPolynom          0xA001      // ��???�?
#define cIntMB    20
#define cFloatMB  50
#define cDoubleMB 10
#define MAX_MB 100
#define PER  1
#define HOU  2
#define DAY  3
#define ALR 4
#define AUD 5
#define SECR 6
#define AVOL 7
#define SEC  8

//static void MX_USART1_UART_Init(void);
extern struct I2C_HandleTypeDef hi2c1;
void SearchKadr1();
void IniCfgSpeed1();
void UART_EndRxTransfer(UART_HandleTypeDef *huart); //, uint8_t *pData);
void CaseNumFun1();
uint8_t ReadDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData);
void SetCopyCalcData(unsigned char nRun);
void P23K256_Write_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
void P23K256_Read_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *rxData, uint16_t RxSize);
void T24_I2C_256_Write_Data(struct I2C_HandleTypeDef *hi2c, uint16_t MemAddress,  uint8_t *txData, uint16_t TxSize); 
int  Mem_ConfigRunWr(uint8_t run);
//uint8_t Mem_AuditRd (uint8_t run, uint16_t ip, uint8_t* MemDat);
//uint8_t Mem_AlarmRd(uint16_t run, uint16_t ip, uint8_t* MemDat);
//float GetQw3(int nRun);
//double GetQ(int nRun);
double Persist_GetQTotal(int iRun);
double Persist_GetETotal(int iRun);
//void ConstCorr(SENSOR enSens, float&  fValue, unsigned char fConst);
void SaveMessagePrefix_2(uint8_t * BufTrans,unsigned char l, unsigned char res);
uint8_t Mem_SecDayRd (uint8_t TypeArch, uint8_t run, uint16_t);
uint16_t Mem_Crc16(uint8_t *buf, uint16_t count);
float  Mem_GetDayQ(uint8_t run);
float   Mem_GetDayE(uint8_t run);
//void IniCfg1_HARTOff() ;
void HART_stop();

extern struct glconfig    GLCONFIG;
//extern uint16_t BaudRate_huart1;
extern struct intpar    INTPAR;
extern char  Comnd;
extern uint8_t ModbusTCP, ModbusRTU,ModbusTCP_1,ModbusRTU_1;
extern uint8_t res[];
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern SPI_HandleTypeDef hspi3;
extern uint32_t TimeReq;       // ����� �� ���������� ������, �
extern int Nwr,Nwr2; // ������ ������
extern struct calcdata COPY_CALCDATA[];
extern struct calcdata CALCDATA[];
extern struct bintime     BINTIME;      // �������� ��p����p� �p�����
extern struct IdRun CONFIG_IDRUN[];
extern unsigned char errMODBUS[];
extern double KoefP[];  // = {1.0,0.0980665,98.0655,0.980665};
extern double KoefdP[];  // = {1.0, 0.00980655, 0.0980665};
extern double KoefEn[]; // = {238.846, 1.0, 0.277778};
extern float IndRe[3],IndC_[3],IndKsh[3],Qsn[3];
extern float KoRo[3][3];
extern double IndQ[3],IndDP[3],IndQs[3];
extern struct memdata      MEMDATA;   // ��p����p� ������ ������ - 26 ����
extern  struct memaudit    MEMAUDIT;     // ��p����p� ������ �p���� ������������
extern struct memalarm   MEMALARM;      // ??p?�??pa ?a�??? ap??oa aoap??
extern uint8_t CalcZc[3];
uint32_t BaudRates_huart12[]={1200,2400,4800,9600,19200,38400,57600,115200};
extern uint16_t CErrMB; 
extern uint8_t errMODBUS[3];     
extern float StartRe[];
extern char PASSWR2[];
extern char PASSWR1[];
extern struct AdRun CONFIG_ADRUN[]; 
extern struct glconfig  GLCONFIG;
extern struct AddCfg    ADCONFIG;
extern struct IdRun CONFIG_IDRUN[3];
extern struct mon_summ MonthSumm[3];    // ?N?????N ????a?�??
extern float XJ[3][21];
extern char NumChrom[],NumChrom_1[];
extern struct startendtime STARTENDTIME;
       // ��p����p� ��� ������ �������
extern _Bool b_Massomer[3],b_Counter[3];
extern struct valarm_data  *ValarmSumm;
extern struct valarm_data  *ValarmSumm2;
extern float RLair[3];
extern unsigned char attempt[]; //,attempt1[];
//extern struct GornalPassw LogPassw[6];
extern struct bintime logTimeb,logTimeb1;
extern struct memsecurit   MEMSECUR;
extern unsigned char  Tlogin[];
extern unsigned char Tlogin1[];
extern char CalcBR[];
extern float Dmin;//0.000001;   //�������� ��������� ���� ������
extern float Dmax;//40.0;    //�������� ��������� ���� ������
extern struct pointARch pArchiv;
extern uint8_t AdrSumm[3][3][2];
extern uint8_t AdrFHP[2];
extern uint16_t SizeFHP;
extern uint8_t AdrIdRun[3][2];
extern uint16_t  SizeIdR;
extern uint8_t *pAdr;
extern unsigned char Flag_Hartservice;
unsigned int    SendCount;
unsigned int    comindex1; //���?? inindex1 o ?�?N?? �pNp?oa??? �? ?a??Npa
uint16_t    inindex1;  //??. ?a �???�? ??N?N??, inc �� �pNp. �p?N???�a, =0 �p? 
unsigned int    outindex1,Dostup,Dostupr,outindex2;
uint8_t   flagspeed1,  flagspeed2;              //??au ???N?N??? ?��p�??? �� COM-��p??
uint8_t   BufTrans1[transindex]; //i??Np n?? �NpNna??

uint8_t BufResiv1[maxindex];
uint8_t BufResiv2[maxindex];
unsigned char   BufOutKadr[256];        //i??Np �an?�o n?? �NpNna??
unsigned char   BufInKadr1[256];         //i??Np �p????�u� �an?a
typedef void (*pf)(void);
uint8_t MBAPH[7],MBAPH_1[7];
uint16_t MaxSizeRx=255;
struct bintime BinTmp,BinTmp2; 
uint16_t FlagTime;                  // ���� ��������� �p�����
uint16_t FlagTimeCopy;              // ��������. ���� ��������� �p�����
struct IdRun CIDRUN;


int InQueueInt(int n,pf func);
int InQueue(int n,pf func);
float g_Qst;                            // ?a?a????? �i?N? �?? Eo, ?3 ?a ?�?N?? 
uint8_t NumCal2;
char portC;

//---------------------------------------------------------------------------
union send SEND;
//---------------------------------------------------------------------------
union resiv  RESIV;
/*--------------- ?a?a?� ��??a??? ??p?�??p? � ???��?? 0x56 ----------------*/
struct GornalPassw LogPassw[6];   //6*15=90 ia?? c 4096 ia??a ???. 81
/*--------------- ?a?a?� ��??a??? ??p?�??p? � ???��?? 0x57 ----------------*/
struct GornalDostup ArhivDostup[10]; //60*17=1020    ia?? c 4246 ia??a ???. 81
/*--------------- ?a?a?� ��??a??? ??p?�??p? � ???��?? 0x58 ----------------*/

//---------------------------------------------------------------------------
float Round_toKg(float, double ,float*);
//--------------------------------------------------------------------------

//---------------------------------------------------------------------------
//???�?a???a�?? �NpNna?? �anp�o ?��i?N??? �� �pNp?oa???? �NpNna???�a
void Ini_Send1()
{
  int kkk = BufTrans1[2];
  if (kkk > 254)  kkk = 0;
  if (((BufTrans1[3] == 0xA0)||(BufTrans1[3] == 0xA1))
     && (BufTrans1[2] == 0x0C)) kkk+=512;
//printf("Trans= %X %X %X %X %X %X\n",BufTrans1[0],BufTrans1[1],BufTrans1[2],BufTrans1[3],BufTrans1[4],BufTrans1[5]);
//printf("Trans=%d\n",kkk);
  memmove(BufTrans1,BufTrans1+GLCONFIG.PreambNum,kkk);  // ?no????? i??Np �N?Nna??
  memset(BufTrans1,0xFF,GLCONFIG.PreambNum);          // n�iao??? �?Na?i???
  if (GLCONFIG.PreambNum != 0) {
     BufTrans1[kkk+GLCONFIG.PreambNum] = 0xFF;
     BufTrans1[kkk+GLCONFIG.PreambNum+1] = 0xFF;
     SendCount = kkk+GLCONFIG.PreambNum+1;  //n???a �anpa
     }
   else
     SendCount = kkk;//+GLCONFIG.PreambNum-1;  //n???a �anpa
/* printf("Send: ");
for (int i=0; i<SendCount; i++)
 printf("%02X ",BufTrans1[i]);
 printf("\n");
*/
 //  memset(BufResiv1,0,maxindex+1);
   inindex1 = 0;
   IniCfgSpeed1(); 
   huart1.RxXferCount = MaxSizeRx;
//   if(flagspeed1)
       //SendCount += 2;//n?? ?�?pa?N??? 2-? ia??�o �p? ???N?N??? ?��p�???
       
   outindex1=0;       //??N?N??N o �NpNnaoaN?�? �anpN
   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);   //RTS USART1 set
   HAL_StatusTypeDef res = HAL_UART_Transmit_IT(&huart1,BufTrans1,SendCount);
  // if (res != HAL_OK ) 
//   Ini_Reciv1();
}
//-----------------------------------------
void IniCfgSpeed1() 
{
   if (GLCONFIG.Speed_1 > 7) GLCONFIG.Speed_1 = 5;
   huart1.Init.BaudRate = BaudRates_huart12[GLCONFIG.Speed_1];
   if (HAL_UART_Init(&huart1) != HAL_OK)
   {
    Error_Handler();
   }
   flagspeed1 = 0;                       // ?ip�? ??aua ?pNi�oa??? ???N?N??? ?��p�???
}
//---------------------------------------------------------------------------
void IniCfgSpeed2() 
{
   if (GLCONFIG.Speed_2 > 7) GLCONFIG.Speed_2 = 5;
   huart2.Init.BaudRate = BaudRates_huart12[GLCONFIG.Speed_2];
   if (HAL_UART_Init(&huart2) != HAL_OK)
   {
    Error_Handler();
   }
   flagspeed2 = 0;                       // ?ip�? ??aua ?pNi�oa??? ???N?N??? ?��p�???
}
//---------------------------------------------------------------------------
void Ini_Reciv1()   //������������� �p����
{
memset(BufResiv1,0,maxindex);          //������� �������� ����p�
//printf("Speed2=%d flagspeed=%d\n",GLCONFIG.Speed,flagspeed);
  if (flagspeed1) IniCfgSpeed1(); 
//  else if (flagspeed2) IniCfgSpeed2(); 
  inindex1=0;                    //�������� �p����
  comindex1 = inindex1;  //����� inindex � ������ �p�p������ �� �����p�
  HAL_GPIO_WritePin(GPIOA, U1_RTS_Pin, GPIO_PIN_RESET);   //RTS USART1 reset  GPIO_PIN_12  
  HAL_UART_Receive_IT(&huart1, BufResiv1, MaxSizeRx);
//  g_cntIniRecCom2++; // ??N???� ???�?a???a�?? �??N?a �� COM2
}
//---------------------------------------------------------------------------
// �p??N??N??? �p? o??�oN a��apa??�u� �pNp?oa??? ?a??Npa
void ChekBufResiv1() {
   unsigned char           *up,*bsrch,*esrch;
   register unsigned int   insrch;
   int i;

 //   if (inindex1 > 0){
//        while (HAL_UART_GetState(&huart1) != HAL_OK){};
//       HAL_UART_Receive_IT(&huart1, BufResiv1, 10); 
//    } 
    inindex1 = MaxSizeRx - huart1.RxXferCount;
//    if ((inindex1==comindex1)&&(inindex1 > 0))
     if ((inindex1==comindex1)&&(inindex1 > 5))
     { UART_EndRxTransfer(&huart1); 
       //huart1.RxXferCount = MaxSizeRx;
//       inindex1=0;
 //      Ini_Reciv1();
     }
    else
    {   comindex1=inindex1; 
    }    

    if (inindex1>5)
   {
    if (Flag_Hartservice)  // ����� HART �������
    {// printf("ind_s=%d %d\n",inindex1,comindex1);
       TimeReq=0;// ?i?�? ?a??N?a ��?�?a TimeReq=0;
    if (inindex1==comindex1)
     { SearchKadr_Hartservice();  // ��� ������� ������ �� ������ HART
       inindex1=0;
     }
     else comindex1=inindex1;
    }
    else //(Flag_Hartservice) == 0
    { bsrch = BufResiv1;
      insrch = inindex1;
	up=0;			  // ��??� ????p�???o�?a
      up = (unsigned char*)memchr(bsrch,SyncByte,insrch);
//printf("in=%d Buf2=%d\n",inindex1,BufResiv1[2]);
     if ((*(bsrch+2)==0)&&(*(bsrch+3)==0)) // ??� MOSCAD TCP
     { ModbusTCP=1; up = 0; }
     else  ModbusTCP=0;
     if (up)
     { if (*(up+2) <= inindex1)
      {                                         // ��??� �p????�u� �anpa o i??NpN �p?N?a
//        if (Flag_Hartservice == 0)
          UART_EndRxTransfer(&huart1);//,BufResiv1);
          InQueueInt(0,SearchKadr1);
//        SearchKadr1();               // o �i???�? ?N???N
      }
     }
      else  // N??? up=0, ?????�ia?? ?N ?a?nN?
     {// for (insrch=0; insrch<=inindex1; insrch++)
//    printf("%X ",BufResiv1[insrch]);
//printf("ind=%d\n",inindex1);
//       flagspeed1=1;
//printf("TCP=%d bs=%X\n",ModbusTCP,*(bsrch+1));
       if (ModbusTCP) bsrch += 6;
       up = (unsigned char*)memchr(bsrch,GLCONFIG.NumCal,insrch);
       if ((up) && ((up[1]==0x04) || (up[1]==0x03)))
       { ModbusRTU = 1;
         UART_EndRxTransfer(&huart1); //,BufResiv1);
         InQueueInt(0,SearchKadr1); // �??N? ?a�?�?a MODBUS
       }
       else
       { ModbusRTU = 0;
         UART_EndRxTransfer(&huart1);//,BufResiv1);
         Ini_Reciv1();
      }
      }
     } //else  (Flag_Hartservice)
    } //if (inindex>5)
}
//---------------------------------------------------------------------------
void SearchKadr1()//����� �p������� ���p� � ����p� �p����
{
   unsigned char           *uk,*begin,*bsrch,*esrch;
   unsigned char           count,Nfun_FU;           // ����� ���p�
   register unsigned int   insrch;
   unsigned int            crc,blength,b1,b2,b3,b4,i;
//LCDputs("                    ");
//LCDputcommand(0x80 | 0x00 | 0);//������� | ����p ������� | �������
//LCDputs(str1);
//str1[0]=0;

   bsrch = BufResiv1;
   esrch  = BufResiv1 + inindex1;
   insrch = inindex1;
   blength = inindex1;
//for (i=0; i<inindex1; i++)
//printf("%02X ",    BufResiv1[i]);
// printf("inindex1=%d TCP=%d RTU=%d\n",inindex1,ModbusTCP,ModbusRTU);
  while (bsrch<esrch)
    {
                                 // ����� ����p��������
      uk = (unsigned char*)memchr(bsrch,SyncByte,insrch);
      if (ModbusTCP) uk=0;  // // MODBUS TCP
      if (ModbusRTU) uk=0;  // MODBUS RTU
      if (uk) {                   // ��������� ������������ ������
         inindex1 = 0;             // ** //  ������� - ����� - ������� - �����
                                  // ����� �� ���� �����p��� ������ �� �����p�
b1= *(uk+1);
b2= *(uk+2);
b3= *(uk+3);
b4= *(uk+4);
//printf("1=%X 2=%X 3=%X 4=%X ",b1,b2,b3,b4);
Nfun_FU = ((b3 == 1) || (b3 == 2) || (b3 == 3) || (b3 == 6) || (b3 == 10));
         if ((((b1 == GLCONFIG.NumCal) ||
         ((b1 == (unsigned char)INTPAR.RS485Com5Speed) && Nfun_FU)) && (b4>0)) ||
         ((b1 == GLCONFIG.NumCal)&&((b3 == 0x7E)||(b3 == 0x7D))) ||
             (b1 == 255 && b2 == 6 && b3 == 1))
         {   begin = uk;           // ��������� �� ����p�����
            count = *(uk+2) - 2;  // ��� 2-� ������ ����p������ �����
            if (count >= 4) {
               crc = Mem_Crc16(uk,count);
               uk += count;
               if (count >  254) count = 0;
               if (crc == *((unsigned int*)uk)) {         // �p������� ����p������ �����
                  memmove(BufInKadr1,begin,count);          // ���������� ����p �p����
                  NumCal2 = b1;
                  InQueueInt(0,CaseNumFun1);               // �� ���������� �������
                  break;                                  // �������� �����
               }
               else {                                     // �� �� �������
//                bsrch += 1;                             // ������� � ����������
                  bsrch = begin + 1;                      // ������� � ����������
                  insrch = blength - (bsrch - BufResiv1);  // ��������� ����� ������
                                                          // ������� � BufResiv1
//                insrch -= 1;                            // ��������� ����� ������
//                insrch = blength - (bsrch - BufResiv1);  // ��������� ����� ������
               }
            }
            else {                            // count < 4
               bsrch = begin + 1;                      // ������� � ����������
               insrch = blength - (bsrch - BufResiv1);  // ��������� ����� ������
            }
         }
         else {                                        // �� ������
            bsrch = uk + 1;                            // ������� � ����������
            insrch = blength - (bsrch - BufResiv1);     // ��������� ����� ������
                                                       // ������� � BufResiv1
            insrch -= 1;                               // ��������� ����� ������

//         inindex1 = 0;
//         Ini_Reciv1();                      // ������������� �p����
         }
      }
      else {                                           // ������������� ��� � ������
       if  (ModbusTCP) bsrch += 6;
       uk = (unsigned char*)memchr(bsrch,GLCONFIG.NumCal,insrch);
//printf("MODBUS count=%d\n", *(uk-1));
       if ((uk) && ((uk[1]==0x04) || (uk[1]==0x03)))
       {   if  (((*(uk-1) + 6) <= inindex1) && (ModbusTCP))
         { count = *(uk-1);
           memmove(uk-6,MBAPH,7);
           memmove(uk,BufInKadr1,count);          // ���������� ����p �p����
           Comnd = uk[1];
//           InQueueInt(0,RecievMODBUS);
         }
         else
         if  (inindex1 - (uk-bsrch) == 8)
         {  count = 6;
            begin = uk;
            crc = Mem_Crc16(uk,count);
            Comnd = uk[1];
            uk += count;
            if (crc == *((unsigned int*)uk))          // �p������� ����p������ �����
            {
              uk = uk -  count;
              memmove(uk,BufInKadr1,count);          // ���������� ����p �p����
//  printf("Reciev MODBUS %d %d C=%d",BufInKadr1[2],BufInKadr1[3],Comnd);
//              InQueueInt(0,RecievMODBUS);
            }
         }
     } 
         inindex1 = 0;
         Ini_Reciv1();                      // ������������� �p����
	 break;
      }
   } //end While
}
//---------------------------------------------------------------------------
void UART_EndRxTransfer(UART_HandleTypeDef *huart) //, uint8_t *pData)
{
  /* Disable RXNE, PE and ERR (Frame error, noise error, overrun error) interrupts */
#if defined(USART_CR1_FIFOEN)
  CLEAR_BIT(huart->Instance->CR1, (USART_CR1_RXNEIE_RXFNEIE | USART_CR1_PEIE));
  CLEAR_BIT(huart->Instance->CR3, (USART_CR3_EIE | USART_CR3_RXFTIE));
#else
  CLEAR_BIT(huart->Instance->CR1, (USART_CR1_RXNEIE | USART_CR1_PEIE));
  CLEAR_BIT(huart->Instance->CR3, USART_CR3_EIE);
#endif /* USART_CR1_FIFOEN */

  /* At end of Rx process, restore huart->RxState to Ready */
  huart->RxState = HAL_UART_STATE_READY;
//  huart->RxXferCount = MaxSizeRx;
//  huart->pRxBuffPtr = pData; 
  /* Reset RxIsr function pointer */
  huart->RxISR = NULL;
}
void UART_EndTxTransfer(UART_HandleTypeDef *huart)
{
#if defined(USART_CR1_FIFOEN)
  /* Disable TXEIE, TCIE, TXFT interrupts */
  CLEAR_BIT(huart->Instance->CR1, (USART_CR1_TXEIE_TXFNFIE | USART_CR1_TCIE));
  CLEAR_BIT(huart->Instance->CR3, (USART_CR3_TXFTIE));
#else
  /* Disable TXEIE and TCIE interrupts */
  CLEAR_BIT(huart->Instance->CR1, (USART_CR1_TXEIE | USART_CR1_TCIE));
#endif /* USART_CR1_FIFOEN */

  /* At end of Tx process, restore huart->gState to Ready */
  huart->gState = HAL_UART_STATE_READY;
}

//---------------------------------------------------------------------
// ?�p??p�oa??N ��??p�???�? ????? o o??�n?�? i??NpN
void CheckBufTrans(uint8_t* uk) {
  
   int             count;
   unsigned int    crc;
   int             bNeedXor;

//   uk = BufTrans;

   count = (int)(*(uk+2) - 2);//iN? 2-? ia??�o ��??p�???�? ?????
   crc = Mem_Crc16(uk, count);
   uk += count;

   *((unsigned int*)uk) = crc; //?a�??? ��??p�???�? ?????
}
//---------------------------------------------------------------------------

void IntMODBUS(int run,unsigned int  IntReg[3][cIntMB],unsigned int NReg,unsigned int KReg)
{ unsigned int k;

   for (k=NReg; k < (KReg+NReg); k++)
   {
    switch(k) {
     case 0:
       IntReg[run][k]= GLCONFIG.EDIZM_T.EdIzmdP;    // ������ ����७�� ��९���
      break;
     case 1:
       IntReg[run][k] = GLCONFIG.EDIZM_T.EdIzP;  // ������ ����७�� ��������
      break;
     case 2:
       IntReg[run][k]=GLCONFIG.EDIZM_T.EdIzmE;       // ������ ����७�� ���ࣨ�
      break;
     case 3:
       IntReg[run][k] = GLCONFIG.EDIZM_T.T_SU;  // ⥬������ �ਢ������ � �.�.
      break;
     case 4:
       IntReg[run][k] = BINTIME.year;      // ��� - ���
      break;
     case 5:
       IntReg[run][k]=BINTIME.month;    //��� - �����
      break;
     case 6:
       IntReg[run][k] = BINTIME.date;  //  ��� - �᫮ �����
      break;
     case 7:
       IntReg[run][k] = BINTIME.hours;   // �६� - ��
      break;
     case 8:
       IntReg[run][k]=BINTIME.minutes;   // �६� - ������
      break;
     case 9:
       IntReg[run][k] = BINTIME.seconds;      //�६� - ᥪ㭤�
      break;
     case 10:
       IntReg[run][k] = GLCONFIG.ContrHour;    // ����ࠪ�� ��;
      break;
     case 11:
       IntReg[run][k] = GLCONFIG. NumRun;  // ������⢮ ��⮪
      break;
     case 12:
       IntReg[run][k]= GLCONFIG.Period;  // ���ࢠ� ����������, �����
      break;
     case 13:
       IntReg[run][k]= CONFIG_IDRUN[0].TypeRun;  // ���䨣���� ��㡮�஢��� �� 1-� ��⪥
      break;
     case 14:
       IntReg[run][k]= CONFIG_IDRUN[1].TypeRun;  // ���䨣���� ��㡮�஢��� �� 2-� ��⪥
      break;
     case 15:
       IntReg[run][k]= CONFIG_IDRUN[2].TypeRun;  // ���䨣���� ��㡮�஢��� �� 3-� ��⪥
      break;
     }
   }
}
//----------------------------------------------------------
 void FloatMODBUS(int run,float FloatReg[3][cFloatMB],unsigned int NReg,unsigned int KReg)
 { unsigned int k;
   struct IdRun    *prun;      // ��p����p� �����
   struct AdRun    *aprun;        // ������
   struct calcdata   *pcalc;

   pcalc = &CALCDATA[run];    // ��������� �� ��������� ������
   prun = &CONFIG_IDRUN[run];        // ��������� �� ��p����p� ���. �����
   aprun   = &CONFIG_ADRUN[run];       
//printf("KReg=%d NReg=%d\n", KReg, NReg);
   NReg /=2;
   for (k=NReg; k < (KReg+NReg); k++)
   {
    switch(2*k) {
     case 0:  // ������� - ������ � �� ��� �������� ������
       FloatReg[run][k] = (prun->TypeRun == 4 || prun->TypeRun == 5
       || prun->TypeRun == 10) ? GetQw3(run) : IndDP[run]*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];

//  printf("run=%d dP-Q=%f\n",run,FloatReg[run][k]);
      break;
     case 2:
       FloatReg[run][k]=pcalc->Pin*KoefP[GLCONFIG.EDIZM_T.EdIzP];     // �������� - ���� ����, ⮭�
      break;
     case 4:
       FloatReg[run][k]=pcalc->t;    //����������� - ��������� ���. �/�3
      break;
     case 6:
       FloatReg[run][k]=pcalc->Q;    // �������� ������ �3/���
      break;
     case 8:
      if ((CONFIG_IDRUN[run].TypeRun)==8    //Rotamass
      || (CONFIG_IDRUN[run].TypeRun)==9) //Micromotion
         FloatReg[run][k] = CONFIG_ADRUN[run].ROdens;  //- ����������� ��������
       else
         FloatReg[run][k]=pcalc->ROnom;      // ��������� � ��
      break;
     case 10:
       FloatReg[run][k]=pcalc->Heat;    // �������� ������� �������� ����.
      break;
     case 12:
       FloatReg[run][k]=pcalc->Heat*pcalc->Q/1000.0;   // ������ ������� ���/��� ��� ���(��������)
      break;
     case 14:
      FloatReg[run][k] = Mem_GetDayQ(run);    // ����� �� � ������ �����
      break;
     case 16:
       Mem_SecDayRdEnd(DAY,run);
       FloatReg[run][k] = MEMDATA.q;    // ����� �� ������� �����
      break;
     case 18:
//        FloatReg[run][k] = GetVm(run);    // ����� �� ������� �����
      break;
     case 20:
                                 // ����� ����� ��, ���. �3
      FloatReg[run][k] = Persist_GetQTotal(run) / 1000.0;
      break;
     case 22:
              // ����� ����� �� ��� ��������, ���. �3
//      FloatReg[run][k] = Persist_GetQwCountTotal(run) / 1000.0;
      break;
     case 24:
              // ��������� ����� �� ��� ���, ���. �3
      FloatReg[run][k] = aprun->Vtrual  / 1000.0;
      break;
     case 26:
            //  ����� �� �������(��� ������) ��� ���, ���. �3
      FloatReg[run][k] = (aprun->Vtot_l -  aprun->Vtrual) / 1000.0;
      break;
     case 28:
               // ���� �������� �������� �3/���
      FloatReg[run][k] = prun->NumTurbo;
      break;
     case 30:
           // % ���������� N2
      FloatReg[run][k] = pcalc->NN2;
      break;
     case 32:
            // % ᮤ�ঠ��� CO2
      FloatReg[run][k] = pcalc->NCO2;
      break;
     case 34:
          // ����������� ���������� ������ � ��    ��. �������
//      FloatReg[run][k] = floor(T_st-273.15+0.5);     //  0,15 , 20
      break;
     case 36:
               // ������ ����������� ��� ��
      FloatReg[run][k] = pcalc->Zc;
      break;
     case 38:
             // ������ ����������� ��� ��
      FloatReg[run][k] = pcalc->Z;
      break;
     case 40:
            // �����樥�� ᦨ�������
      FloatReg[run][k] = pcalc->K;
      break;
     case 42:
            // �����樥�� �ਢ������ ���稪� � ��
      FloatReg[run][k] = pcalc->kQind;
      break;
     case 44:
            //���⭮��� � ��
      FloatReg[run][k] = pcalc->ROmk1;
      break;
     case 46:             // ���ࣨ� �� ���� ��⪨
       FloatReg[run][k] =  (Mem_SecDayRdEnd(DAY,run)==0) ? MEMDATA.FullHeat : 0;
      break;
     case 48:             // ����� �3 � �� �� ��������� ���
       FloatReg[run][k] =  (Mem_SecDayRdEnd(HOU,run)==0) ? MEMDATA.q : 0;
      break;
     }
   }
}
//---------------------------------------------------------
void DoubleMODBUS(int run,double  DoubleReg[3][cDoubleMB],unsigned int NReg,unsigned int KReg)
 { unsigned int k;
   struct AdRun    *aprun;       // ������

   aprun   = &CONFIG_ADRUN[run];        // 㪠��⥫� �� ��p����p� ⥪. ���
   NReg /=4;
   for (k=NReg; k < (KReg+NReg); k++)
   {
    switch(4*k) {
     case 0:  // ��ꥬ �� ������਩�� ��� ���
       DoubleReg[run][k]=aprun->Vtot_l-aprun->Vtrual;    // ��ꥬ �� ����(��� ���਩) ��� ��� �3
      break;
     case 4:
       DoubleReg[run][k]=aprun->Vtrual;     //  ��ꥬ �� ���਩�� ��� ���, �3
      break;
     case 8:
       DoubleReg[run][k]=Persist_GetQTotal(run);    // ��ꥬ �� ��騩,�3
      break;
     case 12:
//       DoubleReg[run][k]=Persist_GetQwCountTotal(run);    // ��ꥬ �� ��騩, �3
      break;
     }
   }
}

//----------------------------------------------------------------------
void  RecievMODBUS()
{
  if (Dostup > 0)
  RecievMODBUSP(BufInKadr1,BufTrans1,&SendCount,2);
     CheckBufTrans(BufTrans1); //�������� ��
     Ini_Send1();  // �������� BufTrans 
}
void  RecievMODBUSP(unsigned char* BufInKadr,unsigned char* BufTrans,unsigned int* SendCount, char nPort)
{ unsigned int kkk,C1,C2,C3,Razdel;
 int i;
 char c;
 unsigned int NumReg;
 unsigned int KolReg;
 int Run,nreg;
 unsigned int  IntReg[3][cIntMB];
 double  DoubleReg[3][cDoubleMB];
 float  FloatReg[3][cFloatMB];
 unsigned int  crc;
 unsigned char* p;
 char type;
 unsigned char Mri[2];

      C1 = 1000; C2 = 2*C1; C3 = 3*C1;
      Razdel = 30001;
      BufTrans[0] = GLCONFIG.NumCal;
      BufTrans[1] = Comnd;//0x04;
      c=BufInKadr[2];
      BufInKadr[2] = BufInKadr[3];
      BufInKadr[3] = c;
      NumReg = *(int*)(BufInKadr+2);
      c=BufInKadr[4];
      BufInKadr[4] = BufInKadr[5];
      BufInKadr[5] = c;
      KolReg = *(int*)(BufInKadr+4);
//printf(" NumReg=%u  %u  ",NumReg,KolReg);
      if (NumReg >= Razdel)
       NumReg = NumReg-Razdel;
      if ((NumReg >= C2) && (NumReg < (C2+3*MAX_MB)))
        KolReg = KolReg/2;
      if ((NumReg >= C3) && (NumReg < (C3+3*MAX_MB)))
        KolReg = KolReg/4;

      type = 0;
       if ((NumReg >= C1) && (NumReg < (C1+3*MAX_MB)))
      {
        Run = (NumReg - C1) / MAX_MB;
        nreg = NumReg - C1 - MAX_MB*Run;
        type = 1;
      }
       else if ((NumReg >= C2) && (NumReg < (C2+3*MAX_MB)))
      {
        Run = (NumReg - C2) / MAX_MB ;
        nreg = NumReg - C2 - MAX_MB*Run;
        type = 2;
      }
       else if ((NumReg >= C3) && (NumReg < (C3+3*MAX_MB)))
      {
        Run = (NumReg - C3) / MAX_MB;
        nreg = NumReg - C3 - MAX_MB*Run;
        type = 3;
      }

      if ((nreg >= 0) && (nreg < cIntMB) && (type==1))
       {
        if ((KolReg+nreg) > cIntMB) KolReg = cIntMB-nreg;
        IntMODBUS(Run,IntReg,nreg,KolReg);
        if (2*KolReg < 251)
         *(BufTrans+2) =  2*KolReg;
        else  *(BufTrans+2) = 251;
        for (i=nreg; i < (nreg+KolReg); i++)
        { *(int*)Mri = IntReg[Run][i];
         *(BufTrans+3+2*(i-nreg)) = Mri[1];
         *(BufTrans+4+2*(i-nreg)) = Mri[0];
//printf("r=%u Reg=%d %04X\n",i,IntReg[Run][i],IntReg[Run][i]);
        }
        crc = Mem_Crc16(BufTrans,2*KolReg+3);
        *(unsigned int*)(BufTrans+3+2*(KolReg)) = crc;
        kkk= 5+2*(KolReg);
//printf("crc=%04X  IntReg0=%u IntReg1=%u\n",crc,IntReg[Run][0],IntReg[Run][1]);
       }
      else
      if ((nreg >= 0) && (nreg < cFloatMB) && (type == 2))
       {
        if ((KolReg+nreg/2) > cFloatMB) KolReg = cFloatMB-nreg/2;
        FloatMODBUS(Run,FloatReg,nreg,KolReg); //���������� ॣ���஢

         if (4*KolReg < 251)
         *(BufTrans+2) =  4*KolReg;
         else  *(BufTrans+2) = 251;
         for (i=nreg/2; i < (KolReg + nreg/2); i++)
         { p = BufTrans+3+4*(i-nreg/2);
          *(float*)(p) = FloatReg[Run][i];
//          Swap2(p,4);
//printf("r=%u Run=%d Buf=%f\n",i,Run,FloatReg[Run][i]);
         }
        crc = Mem_Crc16(BufTrans,4*KolReg+3);
        *(unsigned int*)(BufTrans+3+4*(KolReg)) = crc;
        kkk= 5+4*(KolReg);
//sprintf(str,"crc=%04X kkk=%d  FloatReg0=%f FloatReg1=%f FloatReg2=%f\n",crc,kkk,FloatReg[Run][0],FloatReg[Run][1],FloatReg[Run][2]);
//ComPrint_2(str);
       }
       else if ((nreg >= 0) && (nreg/4 < cDoubleMB) && (type==3))
       {
        if ((KolReg+nreg/4) > cDoubleMB) KolReg = cDoubleMB-nreg/4;
        DoubleMODBUS(Run,DoubleReg,nreg,KolReg);
        if (8*KolReg < 251)
         *(BufTrans+2) =  8*KolReg;
        else  *(BufTrans+2) = 251;
        for (i=nreg/4; i < (KolReg+nreg/4); i++)
        {  p = BufTrans+3+8*(i-nreg/4);
          *(double*)(p) = DoubleReg[Run][i];
//          Swap2(p,8);
//printf("r=%u Double=%lf\n",i,DoubleReg[Run][i]);
        }
         crc = Mem_Crc16(BufTrans,8*KolReg+3);
         *(unsigned int*)(BufTrans+3+8*(KolReg)) = crc;
         kkk= 5+8*(KolReg);
//printf("crc=%04X  DoubleReg0=%lf DoubleReg1=%lf\n",crc,DoubleReg[Run][0],DoubleReg[Run][1]);
       }
       else
       {
         BufTrans[1] =  0x83;
         BufTrans[2] =  0x01;
         crc = Mem_Crc16(BufTrans,4);
         *(int*)(BufTrans+4) = crc;
         kkk=5;
       }
        if (((ModbusTCP)&&(nPort==2)) || ((ModbusTCP_1)&&(nPort==1)))
        {
         kkk -= 2;
//printf("kkk=%d ",kkk);
         for (i=kkk-1; i >= 0; i--)
         {
//printf("%02X ",BufTrans[i]);
         BufTrans[i+6] = BufTrans[i];
         }
         if (nPort==2)
         {  *(MBAPH+5) = (unsigned char)kkk;
             memmove(BufTrans,MBAPH,6);
         }
         else
         {  *(MBAPH_1+5) = (unsigned char)kkk;
            memmove(BufTrans,MBAPH_1,6);
         }
         kkk += 6;
        }

     *SendCount = kkk;  //����� ���p�
/* printf("Send: ");
for ( i=0; i< *SendCount; i++)
 printf("%02X ",BufTrans[i]);
 printf("\n");
*/
   if (nPort==2)
   {
// //    outportb(comport+2,0x85);  // ������ ���� ��।��. FIFO
//     outindex1=0;       //ᬥ饭�� � ��p��������� ���p�
//     outportb(comport+4,0x02);               // RTS=1
//     outportb(comport+1,0x02);    //p��p���� �p�p뢠��� ��p����稪� � ����� �ਥ�
     ModbusRTU = 0;
     ModbusTCP = 0;
   }
   else
   {
      ModbusTCP_1 = 0;
      ModbusRTU_1 = 0;
   }
}
//---------------------------------------------------------------------------
//void CheckBufTransIniSend1()
//{ CheckBufTrans(BufTrans1); //�������� ��
//  Ini_Send1();
//}
////---------------------------------------------------------------------------
void CaseNumFun1() {
   int sel = 0;
   int ex,ex1,ex2,ex3;
   ex = ex1=ex2=ex3=1;
   Nwr = 1;
   Dostup = 3;
     if (Nwr == 0) { if (Dostup == 3) {Nwr2=1; Dostup=2;}} // ���� ������ �������,
     else if (Nwr2==1) {Dostup = 3; Nwr2=0;}  //GLCONFIG.StartDostup; Nwr2=0;}
      // ��  ������ ������� �� ����� ���� ������ 3
     switch(BufInKadr1[3]) {
     case 0x01: InQueue(0,AnswId);       break; // ���p�� ������������p�
     case 0x05:  InQueue(0,RdInstparam);        break; // ���������� ��p����p�
//     case 0x58: InQueue(0,fPassword);  break;        // ������ ������
//     case 0x59: InQueue(0,fendSeans);  break;       // ����� ������
//     case 0x54: InQueue(0,RdNames);      break; // ������ ������������
     default:{ if (Dostup == 0) sel++; ex=0; } // ��� �������
   }
   if ((sel==0)&&(ex==1)) goto L1;
   if (Dostup == 0){ InQueue(0,NoDostup1); sel=0; goto L1;} // ��� ����㯠
   	  if (Dostup == 4)
           switch(BufInKadr1[3]) {
//           case 0x02: if (NumCal2 == INTPAR.RS485Com5Speed) InQueue(0,RdStat);
//                      else sel++; break; // �⥭�� ����᪨� ��p����p��
//           case 0x03: if (NumCal2 == INTPAR.RS485Com5Speed) InQueue(0,WrStat);
//                      else sel++; break; // ������ ����᪨� ��p����p��
           case 0x05:  InQueue(0,RdInstparam);        break; // ��������� ��p����p�
 //          case 0x06: if (NumCal2 == INTPAR.RS485Com5Speed) InQueue(0,AnswChim);
//                      else sel++; break; //�⥭�� 娬��᪮�� ��⠢�
//           case 0x0A: if (NumCal2 == INTPAR.RS485Com5Speed) InQueue(0,RdDiaph);
//                      else sel++; break; // ������ ����᪨� ��p����p��
           case 0x43:  InQueue(0,WrRecfg); break; // ������ ��p����䨣�p�樨
           default:{InQueue(0,NoDostup1); sel=0; goto L1; } // ��� ����㯠
          }
/*	  else if (Dostup == 7)
	   switch(BufInKadr1[3])
	   { // ������ ᯨ᪠ ���� � ��஫��
	   case 0x56: if (Nwr != 0) InQueue(0,fCreateUser);  break;
	   case 0x54: InQueue(0,RdNames);      break; // �⥭�� ������������
	   case 0x55: if (Nwr != 0) InQueue(0,WrNames);  break; // ������ ������������
	   case 0x60: InQueue(0,fUserRd);  break; //�⥭�� ���짮��⥫��
           case 0x61: InQueue(0,RdSecur);      break;  // �⥭�� ��娢� ������᭮��
           case 0x67: InQueue(0,RdEdizm);      break; // �⥭�� ������ ����७��
           case 0x68: InQueue(0,WrEdizm);      break; // ������ ������ ����७��
	   case 0x59:  break;
	   default:  sel++;
	   }

   else if ((Dostup > 0)&& (!ex))
   {    switch(BufInKadr1[3])
     {  // �⥭��
      case 0x02: InQueue(0,RdStat);       break; // �⥭�� ����᪨� ��p����p��
      case 0x04: InQueue(0,RdCalcparam);  break; // ����� ��p����p�
      case 0x05: InQueue(0,RdInstparam);  break; // ��������� ��p����p�
      case 0x08: InQueue(0,RdConst);      break; // �⥭�� ����⠭�
      case 0x0A: InQueue(0,RdDiaph);      break; // �� ��ࠬ��஢ �����.   - ���
      case 0x0C: InQueue(0,RdValarm);     break; // �⥭�� ���਩��� ��ꥬ��
      case 0x11: // ��ࠬ���� 娬��⠢�
      case 0x1B: // � V�� tot ��� ���稪��
      case 0x14:   //if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0)) ��� ������ � ������ � ���⭮����
		 InQueue(0,RdDay);// else InQueue(0,RdDayDen);
		  break; // �⥭�� ���筮�� ����
      case 0x15:// if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
		 InQueue(0,RdPeriod);// else InQueue(0,RdPeriodDen);
		 break; // �⥭�� ��p�����᪮�� ����
      case 0x16: InQueue(0,RdAudit);      break; // �⥭�� ��p���� ����⥫���
      case 0x17: InQueue(0,RdAlarm);      break; // �⥭�� ��p���� ���p��
      case 0x18:// if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
		 InQueue(0,Comm_RdCykl);// else InQueue(0,RdCyclDen);
		 break; // �⥭�� 横���᪨� ������
      case 0x13:
      case 0x19:// if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
		 InQueue(0,RdHour);// else InQueue(0,RdHourDen);
		 break; // �⥭�� �ᮢ��� ����
//      case 0x1A: InQueue(0,RdPeriodDen);  break; // �� �����. ��ࠬ��஢  - ���⭮���
//      case 0x1B:// if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
//		 InQueue(0,RdDay);// else InQueue(0,RdDayDen);
//		  break; // �⥭�� ���筮�� ���� � ��騬 ��ꥬ�� � ��
//      case 0x1C: InQueue(0,RdCyclDen);    break; // �� 横����� ��ࠬ��஢ - ���⭮���
//      case 0x1D: InQueue(0,RdHourDen);    break; // �� �ᮢ�� ��ࠬ��஢  - ���⭮���
      case 0x22: InQueue(0,AnswSys);      break; // �⥭�� ��⥬��� ��p����p��
      case 0x44:  // �⥭�� ��p����p�� ���稪�� ��� �����
      case 0x46: InQueue(0,RdParamdat);   break;// �⥭�� ��p����p�� ���稪�� ��� ��-1
      case 0x43: InQueue(0,WrRecfg);      break;// ������ ��p����䨣�p�樨
      case 0x67: InQueue(0,RdEdizm);      break; // �⥭�� ������ ����७��


      default: if (Dostup == 1) sel++; ex1=0;
     }
*/
ex1 = 0;
if ((Dostup > 1)&&(!ex1))
      {
	switch(BufInKadr1[3])
	{ // ������ x����⠢�  �� Megas
//	 case 0x03: if ((NumCal2 == INTPAR.RS485Com5Speed)&&(Dostup == 2))
//		     InQueue(0,WrStat);  else   ex2=0;
//		     break; // ������ ����᪨� ��p����p��
	 case 0x06: InQueue(0,AnswChim);     break; // �⥭�� 娬��᪮�� ��⠢�
	 case 0x07: InQueue(0,WrChim);       break; // ������ 娬��᪮�� ��⠢�
	 case 0x18: InQueue(0,Comm_RdCykl);  break;
     case 0x42: InQueue(0,SetTime);      break; // ��⠭���� �p�����
     case 0x04: InQueue(0,RdCalcparam);  break; // ����� ��p����p�
     case 0x08: InQueue(0,RdConst);      break; // �⥭�� ����⠭�
     case 0x09: InQueue(0,WrConst);      break; // ������ ����⠭�
      case 0x44:  // �⥭�� ��p����p�� ���稪�� ��� �����
      case 0x46: InQueue(0,RdParamdat);   break;// �⥭�� ��p����p�� ���稪�� ��� ��-1
      case 0x45: InQueue(0,WrParamdat);   break; // ������ ��p����p�� ���稪��
      case 0x43: InQueue(0,WrRecfg);      break;// ������ ��p����䨣�p�樨

        //      case 0x11: // ��ࠬ���� 娬��⠢�
//      case 0x1B: // � V�� tot ��� ���稪��
//      case 0x14:   //if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0)) ��� ������ � ������ � ���⭮����
//		 InQueue(0,RdDay);// else InQueue(0,RdDayDen);
//		  break; // �⥭�� ���筮�� ����
      case 0x15:InQueue(0,RdPeriod);
		 break; // �⥭�� ��p�����᪮�� ����
      case 0x13:
      case 0x19: InQueue(0,RdHour);
		 break; // �⥭�� �ᮢ��� ����
      case 0x11: // ��ࠬ���� 娬��⠢�
      case 0x1B: // � V�� tot ��� ���稪��
      case 0x14:   InQueue(0,RdDay);
		  break; // �⥭�� ���筮�� ����
      case 0x16: InQueue(0,RdAudit);      break; // �⥭�� ��p���� ����⥫���
      case 0x17: InQueue(0,RdAlarm);      break; // �⥭�� ��p���� ���p��

        //	 case 0x61: InQueue(0,RdSecur);      break;  // �⥭�� ��娢� ������᭮��
	 
//	 case 0x20: InQueue(0,SD_RdCykl); sel=1;TimeReq=0;   break; // �� ���������� �� SD-����
//	 case 0x21: InQueue(0,SD_RdPeriod); sel=1;TimeReq=0; break; // �� ��ਮ���᪨� �� SD-����
      case 0x22: InQueue(0,AnswSys); break; 
      case 0x23: InQueue(0,WrSys);        break; // ������ ��⥬��� ��p����p��
      case 0x41: InQueue(0,WrStCfg);      break; // ������ ��砫쭮� ���䨣�p�樨
      case 0x4E: InQueue(0,AnswerHComY);   break; // ���室 � ०�� HART<->COM
      case 0x4F: InQueue(0,AnswerHCom);   break;
      /*	 case 0x24: InQueue(0,RdNameSD);   break; // �� ���� �� SD-�����
	 case 0x66: InQueue(0,RdVolume);     break; // �⥭�� ��ꥬ�� ���稪�
	 case 0x49: InQueue(0,AnswClbrTable);break; // �⢥� �� ����� �⥭�� ⠡���� �����஢��
	 case 0x4B: InQueue(0,AnswCode);     break; // �⢥� �� ����� �⥭�� ���� ���
	 case 0x52: InQueue(0,RdIntPar);     break; // �⥭�� ����७��� ��ࠬ��஢
         case 0x53: InQueue(0,WrIntPar);     break; // ������ ����७��� ��ࠬ��஢
	 case 0x62: InQueue(0,RdIntPar2);     break; // �⥭�� ����७��� ��ࠬ��஢ 2
         case 0x63: InQueue(0,WrIntPar2);     break; // ������ ����७��� ��ࠬ��஢ 2
	 case 0x64: InQueue(0,RdAddConf);     break; // �⥭�� �������⥫��� ��ࠬ. ���䨣��樨
*/
     default: if (Dostup == 2) sel++; ex2=0;
	}
/*    
	if ((Dostup > 2)&&(!ex2))
	{  // ������ ��ࠬ��஢
	 switch(BufInKadr1[3]) {
	  case 0x03: InQueue(0,WrStat);       break; // ������ ����᪨� ��p����p��
	  case 0x09: InQueue(0,WrConst);      break; // ������ ����⠭�
	  case 0x0B: InQueue(0,WrDiaph);      break; // �� ��ࠬ��஢ �����.   - ���
          case 0x25: InQueue(0,InitSD);   sel=1;TimeReq=0;  break; // ���樠������ SD-�����
      case 0x41: InQueue(0,WrStCfg);      break; // ������ ��砫쭮� ���䨣�p�樨
      case 0x45: InQueue(0,WrParamdat);   break; // ������ ��p����p�� ���稪��
      case 0x4A: InQueue(0,WrClbrTable);  break; // ������ ⠡���� �����஢��
      case 0x4E: InQueue(0,AnswerHComY);   break; // ���室 � ०�� HART<->COM
      case 0x4F: InQueue(0,AnswerHCom);   break; // ���室 � ०�� HART<->COM
//      case 0x50: InQueue(0,RdNoAlarmConstFlag);   break; // �⥭�� 䫠�� ����� ���਩
//      case 0x51: InQueue(0,WrNoAlarmConstFlag);   break; // ������ 䫠�� ����� ���਩
      case 0x40: InQueue(0,AnswStCfg);    break; // ����室������ ��砫쭮� ���䨣�p�樨
      case 0x65: InQueue(0,WrAddConf);     break; // ������ �������⥫��� ��ࠬ. ���䨣��樨
      case 0x68: InQueue(0,WrEdizm);      break; // ������ ������ ����७��

          default:   // Ini_Reciv1(); //AnswError;  // �訡��� ����p �㭪樨
          if (Dostup == 3) sel++; ex3=0;
          }
        } // if (Dostup > 2)
*/
      } // if (Dostup > 1)
//   } //if (Dostup > 0)

L1:
  if (sel == 0)
  {  

   TimeReq=0;// ����� ������� ������ TimeReq=0;
  }
   else Ini_Reciv1(); //AnswError;  // ��������� ����p �������
}
//---------------------------------------------------------------------------
void RdInstparamp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{                    // ���������� ��p����p�
   struct calcdata   *pcalc;
   struct bintime    *ptime;            // �������� ��p����p� �p�����
   struct instparam  *pip;              // ���������� ��p����p�
   struct IdRun      *run;              // ��p����p� �����
   unsigned char num;
   int i,err =  0;
   union send  SEND;

   if (BufInKadr[2] != 7)
      err |= 1;                         // ����p��� ����� ���p���

   num = 3 & BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� �����
   if (err)
      SaveMessagePrefix(6,0xFF,BufTrans);        // ������ ��p����p�� ���p���
   else {
      SetCopyCalcData(num-1);
      pip   = &SEND.INSTPARAM;          // ���������� ��p����p�
      ptime = &BINTIME;
      pcalc = &COPY_CALCDATA[num-1];
      run   = &CONFIG_IDRUN[num-1];
                                        // p����� �p� p.�., �3/��� ��� ��p����
      float Qru = ((errMODBUS[num-1] > CErrMB)&&(run->constdata & 0x08))
       ? 0.0 : GetQw3(num-1);
      
      float KDP = (GLCONFIG.EDIZM_T.EdIzmdP==0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];     
      pip->dP = ((run->TypeRun == 4) || (run->TypeRun == 5)
       || (run->TypeRun == 10)) ? Qru : IndDP[num-1]*KDP;

      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       pip->P = pcalc->Pa;
      else
      pip->P    = pcalc->Pa*KoefP[GLCONFIG.EDIZM_T.EdIzP];            // ��������;
      pip->t    = pcalc->t;             // �����p���p�;
//printf("dP=%f P=%f t=%f Q=%f\n",IndDP[num-1],pcalc->Pa,pcalc->t,IndQ[num-1]);
      ConstCorr(((run->TypeRun) == 4 || (run->TypeRun) == 5
      || (run->TypeRun) == 10) ? QW : DP,&pip->dP, run->constdata);
//      if (run->Vlag == 1) pip->dP = IndDP[num-1];     
      ConstCorr(P, &pip->P, run->constdata);
      ConstCorr(T, &pip->t, run->constdata);
                                        // p�����
//printf("num=%d GetQ=%f GetQRaw=%f \n",num,GetQ(num-1),GetQRaw(num-1));
     pip->Q = ((run->TypeRun & 0x0F) == 4 || (run->TypeRun & 0x0F) == 5 ||
     (run->TypeRun & 0x0F ) == 10) ? GetQ(num-1) : IndQ[num-1];   //GetQRaw
    //  if ((run->TypeRun & 0x0F) == 10) pip->Q = GetQ(num-1);
//    pip->Q =  GetQ(num-1);
      pip->Vs = Mem_GetDayQ(num-1);      // ����� �� �����

                                        // ����� �� �p����� ����� �� ��������� ������ ��������� ������
      pip->Va = (Mem_SecDayRdEnd(DAY,num-1)==0) ? MEMDATA.q : 0.0;

                                        // ����� �����, ���. �3
      pip->Vtot   = Persist_GetQTotal(num - 1) / 1000.0;
      pip->Dens   = pcalc->ROnom;       // ��������� ����
      pip->Uheat  = pcalc->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];      // �������� ������� ��������
      ConstCorr(RO, &pip->Dens, run->constdata);
      ConstCorr(Heat, &pip->Uheat, run->constdata);
        // ������ ������� (�������� ��������)
      pip->WEnergy = GetQ(num - 1)*(pip->Uheat/1000.0);
      pip->EnBegDay = Mem_GetDayE(num - 1)*KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1000.0;// ������� � ������ �����
      pip->EnLastDay = (Mem_SecDayRdEnd(DAY,num-1) == 0)
       ?  MEMDATA.FullHeat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1000.0 : 0.0;

      pip->TotalEnergy = Persist_GetETotal(num - 1)*KoefEn[GLCONFIG.EDIZM_T.EdIzmE] / 1000.0;   // ����� ������� � ������ ������������ ���.

      pip->Month   = ptime->month;      // �����
      pip->Day     = ptime->date;       // ����

      if ((CONFIG_IDRUN[num-1].TypeRun) == 4 || (CONFIG_IDRUN[num-1].TypeRun) == 5
      || (run->TypeRun) == 10)
         pip->Day |= 0x80;              // ������ ����p���� �� �����

      pip->Year    = ptime->year;       // ���
      pip->Hours   = ptime->hours;      // ���
      pip->Minutes = ptime->minutes;    // ������
      pip->Seconds = ptime->seconds;    // �������

      SaveMessagePrefix_2(BufTrans,sizeof(struct instparam)+5,0x85); // ������ �p������
      BufTrans[4] = num;                 // ����p �����
      memmove(&BufTrans[5],&SEND,sizeof(struct instparam));  // ������ ����p�
      memcpy(&BufTrans[39],&BufTrans[41],24);
      //sprintf(str,"BufTrans=%02X %02X %02X %02X\n\r",BufTrans[0],BufTrans[1],BufTrans[2],BufTrans[3]);
//ComPrint_2(str);

//      for (int i=0; i<40; i++) printf("%X ",BufTrans[i]);//printf("\n");
//      printf("Outindex=%d,Sendcount=%d\n",outindex1,SendCount);
   }
//      CheckBufTrans(BufTrans); //�������� ��
//      Ini_Send1();
}

void RdInstparam() {                    // ���������� ��p����p�
 RdInstparamp(BufInKadr1,BufTrans1);                     // ���������� ��p����p�
      CheckBufTrans(BufTrans1); //�������� ��
      Ini_Send1();
}
//---------------------------------------------------------------------------
//void CheckBufTransIniSend()
//{ CheckBufTrans(); //�஢�ઠ ��
//  Ini_Send1();
//}
//---------------------------------------------------------------------------
void AnswId() //���p�� �����䨪��p�
{
   AnswIdp(BufTrans1);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();    
}
void AnswIdp(unsigned char* BufTrans)
{
  struct Id         *pid,ID;
  struct IdRunCom   *prun;
  struct IdRun      *pglrun;
  struct bintime    *ptime;
  uint8_t *bf;
  int i;
  unsigned char KC,TR1,TR;

  ptime = &BINTIME;                    //����筠� ��p���p� �p�����
  pid   = &ID;  //SEND.ID;
  if ((NumCal2 == INTPAR.RS485Com5Speed)&&(NumCal2 !=GLCONFIG.NumCal))
  {
  bf = &BufTrans[5];
  pglrun = &CONFIG_IDRUN[0];
  memset(&BufTrans[0],0x0,165);
  BufTrans[4] = GLCONFIG.NumRun;

  for(i=0;i<3;i++)
    { memmove(bf+i*17,pglrun,16);            //��� ��⪨
      *(bf+i*17+16) = pglrun->TypeP;    //⨯ ���稪� ��������
      pglrun++;
    }
  bf = &BufTrans[56];
  *bf     = ptime->month;    //�����
  *(bf+1) = ptime->date | 0x40;     //���� + ���� ������ * 0x20
  *(bf+2) = ptime->year;     //���
  *(bf+3) = ptime->hours;    //��
  *(bf+4) = ptime->minutes;  //������
  *(bf+5) = ptime->seconds;  //ᥪ㭤�
  bf = &BufTrans[63];
  *bf = 37;  //����� ���ᨨ
  *(bf+6) = 12;
  *(bf+7) = 8; //70
  *(bf+8) = 0x20; //�����筮�
  *(bf+9) = 0;
  *(bf+10) = 0; //73
//  bf = &BufTrans[74];

   SaveMessagePrefix_2(BufTrans,165,0x81);
//  memset(&BufTrans[132],0x0,31);
  BufTrans[1] = NumCal2;
 }
 else
 {
   prun = &ID.IDRUN[0];//SEND.ID.IDRUN[0];
  pglrun = &CONFIG_IDRUN[0];
  pid->NumRun = GLCONFIG.NumRun;      //���-�� ��⮪
  for(int i=0;i<3;i++)
    { memmove(prun,pglrun,16);            //��� ��⪨
      prun->TypeP    = pglrun->TypeP;    //⨯ ���稪� ��������
      if (i==0)
      { prun->TypeP = prun->TypeP | (GLCONFIG.EDIZM_T.EdIzmdP << 5);
        prun->TypeP = prun->TypeP | (GLCONFIG.EDIZM_T.EdIzP << 2);
      }
      else if (i==1)
       prun->TypeP = prun->TypeP | (GLCONFIG.EDIZM_T.EdIzmE << 2);
      prun->TypeRun  = pglrun->TypeRun;  //ᯮᮡ ����p���� �� ��⪥
      if (CONFIG_IDRUN[i].Dens == 1)
         prun->TypeRun = prun->TypeRun | 0x20;   //���⭮��� �� ��⪥ i
      KC = (pglrun->nKcompress << 6);
//printf("TypeRun=%d %d", prun->TypeRun,pglrun->TypeRun);
      prun->TypeRun = (prun->TypeRun | KC);
      KC = (pglrun->GOST_ISO << 4);
      prun->TypeRun = (prun->TypeRun | KC);
//printf("TypeRun+nK=%02X KC=%02X\n",prun->TypeRun,KC);

//printf("r=%d TypeRun=%02X\n",i,prun->TypeRun);
//0 - 3095, 2 - 3095+3051CD, 1 - P+T+dP, 3 - P+T+dP+dP, 4 - ���稪
      prun->NumTurbo = pglrun->NumTurbo; //���-�� �3 �� ������
      prun->Qmin     = pglrun->Qmin;     //��������� p��室
      prun->Qmax     = pglrun->Qmax;     //���ᨬ���� p��室
      prun->Pmax     = pglrun->Pmax*KoefP[GLCONFIG.EDIZM_T.EdIzP];     //��p孨� �p���� ���稪� ��������
      prun->dPmax   = pglrun->dPhmax*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];    //��p�. �p���� ���稪�� ��p�����
      prun++;
      pglrun++;
    }

  pid->TypeCount = GLCONFIG.TypeCount | GLCONFIG.EDIZM_T.T_SU | (GLCONFIG.EDIZM_T.T_VVP << 2); //䫠�� ���. ���䨣�p�樨 - ⨯ p����
  pid->ContrHour = GLCONFIG.ContrHour; //����p���� ��
  pid->Period    = GLCONFIG.Period;    //���p��� ���������� � ������
//printf(" Type=%02X GLType=%02X T_SU=%d T_VVP=%d\n",pid->TypeCount,GLCONFIG.TypeCount, GLCONFIG.EDIZM_T.T_SU, GLCONFIG.EDIZM_T.T_VVP);
  // ��p�� �p��p������� ���ᯥ祭��
pid->Ver       = 8;
pid->VerDen    =  0x91;
pid->VerMes    =  0x8C;
pid->VerGod    =  0x13;

//  GetVersion(pid);

  pid->Month     = ptime->month;    //�����
  pid->Day       = ptime->date;     //����
  pid->Year      = ptime->year;     //���
  pid->Hours     = ptime->hours;    //��
  pid->Minutes   = ptime->minutes;  //������
  pid->Seconds   = ptime->seconds;  //ᥪ㭤�
  SaveMessagePrefix_2(BufTrans,sizeof(struct Id)-4,0x81);
  memmove(&BufTrans[4],&ID,sizeof(struct Id));
  memcpy(&BufTrans[5],&BufTrans[8],18);
  memcpy(&BufTrans[23],&BufTrans[28],38);
  memcpy(&BufTrans[61],&BufTrans[68],38);
  memcpy(&BufTrans[99],&BufTrans[108],38);
  uint16_t KsROM = 0x277C;
  memmove(&BufTrans[132],&KsROM,2);

//  unsigned int KsROM;  // ������ ����������� ����� ���
//  outportb(0x1D0,RESERV_CFG_PAGE);
//  KsROM = peek(SEGMENT,0x3FC6); //�⥭�� ��
 
 }
}
//---------------------------------------------------------------------------
/*
void RdStat() {                         // �⥭�� ����᪨� ��p����p��
 RdStatp(BufInKadr1,BufTrans,NumCal2);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}
void RdStatp(unsigned char* BufInKadr,unsigned char* BufTrans,unsigned char NumCal2)
{   struct rdstat   *p,RDSTAT;
   struct IdRun    *run;                // ��p����p� ��⪨
   struct bintime  *ptime;              // ����筠� ��p���p� �p�����
   unsigned char num;
   int           err = 0;
   unsigned char *bf;

   if (BufInKadr[2] != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4];                  // ����p ��⪨
   if (num > GLCONFIG.NumRun) err |= 2;                         // ����p��� ����p� ��⪨
   ptime = &BINTIME;

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   else
   if ((NumCal2 == INTPAR.RS485Com5Speed)&&(NumCal2 !=GLCONFIG.NumCal))
  {
   run = &CONFIG_IDRUN[num-1];
   memset(&BufTrans[0],0x0,130);
   BufTrans[4] = num;
  bf = &BufTrans[5];
  memmove(bf,run,16);            //��� ��⪨
  bf = &BufTrans[122];
  *bf     = ptime->month;    //�����
  *(bf+1) = ptime->date;     //���� + ���� ������ * 0x20
  *(bf+2) = ptime->year;     //���
  *(bf+3) = ptime->hours;    //��
  *(bf+4) = ptime->minutes;  //������
  *(bf+5) = ptime->seconds;  //ᥪ㭤�
  bf = &BufTrans[21];
//  *(float*)bf = run->ROnom;
  *(float*)bf = run->RoVV;
  *(float*)(bf+4) = run->NCO2;
  *(float*)(bf+8) = run->NN2;
//  Swap4(bf);
//  Swap4(bf+4);
//  Swap4(bf+8);
  bf = &BufTrans[41];
  *(float*)bf = run->Pb;
//  Swap4(bf);
  bf = &BufTrans[65];
//  *(float*)bf = run->Heat;
  *(float*)bf = run->HsVV;

//  Swap4(bf);
  SaveMessagePrefix_2(BufTrans,130,0x82); //������ �p�䨪�
  BufTrans[1] = NumCal2;
  }
  else
   {
      p   = &RDSTAT;               // 娬��᪨� ��⠢
      run = &CONFIG_IDRUN[num-1];

      p->TypeCount = GLCONFIG.TypeCount;// ��� 0,1: ��50\GERG\NX19 [0\1\2]
                                        // ��� 7: ��p-const\��p-p���. [0\1]
      p->TypeRun  = run->TypeRun;       // ᯮᮡ ����p���� �� ��⪥
                                        // 0 - 3095, 2 - 3095+3051CD, 1 - P+T+dP, 3 - P+T+dP+dP, 4 - ���稪
      p->TypeP    = run->TypeP;         // ⨯ ���稪� �������� �� ��⪥
//      p->ROnom = run->ROnom;            // ���⭮��� ���� �� �.�., ��/�^3
      p->ROnom = run->RoVV;            // ���⭮��� ���� �� �.�., ��/�^3

      p->Pb    = run->Pb;               // ��஬����᪮� ��������, �� p�. ��.
      p->NCO2  = run->NCO2;             // ����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
      p->NN2   = run->NN2;              // ����p��� ���� ���� � ������� ᬥ�, %
                                        // ��� ��⪨
      movmem((run->NameRun),(p->NameRun),16);
      p->D20    = run->D20;             // ����p����� ������p �p��, ��
      p->d20    = run->d20;             // ����p����� ������p ����p����, ��
      if ((run->TypeRun == 4) || (run->TypeRun == 5) || (run->TypeRun == 10))
       p->dPots  = run->dPots;           // �p����� Qst ��� ���稪�, �3/�
      else
//       p->dPots = Round_fromKg(run->dPots,KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]);
      { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
         p->dPots  = run->dPots;
        else
         p->dPots  = run->dPots*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]; //�p����� ���窨, ���/�2
      }                                  //  ��� Qst ��� ���稪�
      p->Bd     = run->Bd ;             //  ����-�� ���-�� ���-� ���ਠ�� ����ࠣ��, ���^-1(��� A)
      p->kBd    = run->kBd;             // ����-�� B
      p->kCd    = run->kCd;             // ����-�� C
      p->Bt     = run->Bt ;             //  ����-�� ���-�� ���-� ���ਠ�� �p��, ���^-1(��� A)
      p->kBt    = run->kBt;             // ����-�� B
      p->kCt    = run->kCt;             // ����-�� C
      p->R      = run->R  ;             //  ���. ��客����� ��㡮�஢���, ��
                                        // ��p�� ��p����祭�� �� ��p�����
//       p->ValueSwitch = Round_fromKg(run->ValueSwitch,KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]);
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       p->ValueSwitch =  run->ValueSwitch;
      else
       p->ValueSwitch =  run->ValueSwitch*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];//��p�� ��p����祭�� �� ��p�����
      p->typeOtb= run->typeOtb;         // 0 - 㣫����, 1 - 䫠�楢�

      ptime = &BINTIME;
      p->Month  = ptime->month;         // �����
      p->Day    = ptime->date;          // ����
      p->Year   = ptime->year;          // ���
      p->Hours  = ptime->hours;         // ��
      p->Minutes= ptime->minutes;       // �����
      p->Seconds= ptime->seconds;       // ᥪ㭤�
                                        // ������ �p�䨪�
      SaveMessagePrefix_2(BufTrans,sizeof(struct rdstat)+7,0x82);
      BufTrans[4] = num;                // ����p ��⪨
                                        // ������ ���p�
      movmem(&RDSTAT,&BufTrans[5],sizeof(struct rdstat));
   }
}
*/
//---------------------------------------------------------------------------
/*
void WrStat() {                         // ������ ����᪨� ��p����p��
 WrStatp(BufInKadr,BufTrans,NumCal2,2);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}
void WrStatp(unsigned char* BufInKadr,unsigned char* BufTrans, unsigned char NumCal2, char nPort)
{
  struct wrstat   *p;
  struct IdRun    *run; //��p����p� ��⪨
  unsigned char num;
  int           err = 0, nMeth;
  unsigned char PasFU[15];
  unsigned char *pc;
  float RoFU,N2FU,CO2FU,TeplFU,PbFU;
  float dPotsw,Ep,UHeat;

  num = BufInKadr[4]; //����p ��⪨
  if ((NumCal2 == INTPAR.RS485Com5Speed)&&(NumCal2 !=GLCONFIG.NumCal))
  {
    if (BufInKadr[2] != 140 ) err |= 1;//����p��� ����� ���p��
    if (num > GLCONFIG.NumRun)  err |= 2;//����p��� ����p� ��⪨
//   Mem_LoginRd();
//   memcpy(PasFU,LogPassw[1].login,4);
//   memcpy(&PasFU[4],LogPassw[1].Password,10);
//PasFU[14] = 0x0;
//printf("PasFU=%s\n",PasFU);
//    if (memcmp(&BufInKadr[5],PasFU,14))  err |= 4;
    pc = &BufInKadr[37];
    RoFU = *(float *)pc;
    if (RoFU < 0.55)         // ������ �।�� ���������
             err |= 8;
    if (RoFU > 1.13)           // ��� ��� ��⮤��
      err |= 16;
    if(err)
    { SaveMessagePrefix_2(BufTrans,6,0xFF);  //����᪨� ��p����p� �� ����ᠭ�
      BufTrans[1] = NumCal2;
    }
  else
    {
     pc = &BufInKadr[41];
     CO2FU = *(float *)pc;
     pc = &BufInKadr[45];
     N2FU = *(float *)pc;
     pc = &BufInKadr[81];
     TeplFU = *(float *)pc;
     pc = &BufInKadr[57];
     PbFU = *(float *)pc;

//printf("RoFU=%f CO2FU=%f N2FU=%f TeplFU=%f\n");
     run = &CONFIG_IDRUN[num-1];
      if(run->RoVV  != RoFU)
       { MEMAUDIT.type     = 1; //���⭮��� ���� �� �.�., ��/�^3
         MEMAUDIT.altvalue = run->RoVV;
          run->ROnom      = RoFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];
          run->RoVV      = RoFU;
         MEMAUDIT.newvalue = run->RoVV;
         CalcZc[num-1] = 1;
          Mem_AuditWr(num-1,MEMAUDIT);//������ ���� ����⥫���
        }
      UHeat =  TeplFU;    // � ���*�
//      TeplFU = Round_toKg(UHeat,KoefEn[GLCONFIG.EDIZM_T.EdIzmE],&Ep); // �� MEGAS� � ���
//printf("UHeat=%f Heat=%f HsVV=%f dh=%f Ep=%f\n",UHeat,p->UHeat,run->HsVV, run->Heat  - p->UHeat,Ep);
      Ep = 1e-3;
     if ((UHeat > 19.0)&&(UHeat < 48.1))
     { if (fabs(run->HsVV  - UHeat)>Ep)
      { MEMAUDIT.type     = 37;
        MEMAUDIT.altvalue = run->HsVV; // * KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
        run->Heat  = TeplFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
        MEMAUDIT.newvalue = UHeat;
        run->HsVV = UHeat;
        CalcZc[num-1]=1;
                                    // ������ ���� ����⥫���
        Mem_AuditWr(num-1,MEMAUDIT);
      }
      else
       { run->HsVV = UHeat;
         run->Heat  = TeplFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
       }
      }
      if(run->NCO2  != CO2FU)
        { MEMAUDIT.type     = 2;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          MEMAUDIT.altvalue = run->NCO2;
          run->NCO2   = CO2FU;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          MEMAUDIT.newvalue = run->NCO2;
          CalcZc[num-1] = 1;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->NN2  != N2FU)
        { MEMAUDIT.type     = 3;
          MEMAUDIT.altvalue = run->NN2;
          run->NN2    = N2FU; //����p��� ���� ���� � ������� ᬥ�, %
          MEMAUDIT.newvalue = run->NN2;
          CalcZc[num-1] = 1;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->Pb  != PbFU)
        { MEMAUDIT.type     = 6;  //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT.altvalue = run->Pb;
          run->Pb     = PbFU;    //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT.newvalue = run->Pb;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���

      if( Mem_ConfigWr() == 0 )         // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
        SaveMessagePrefix_2(BufTrans,6,0x83);    // ����᪨� ��p����p� ����ᠭ�
      else
        SaveMessagePrefix_2(BufTrans,6,0xFF);    // ����᪨� ��p����p� �� ����ᠭ�
      BufTrans[1] = NumCal2;
    }
   }
   else
   {
    if( BufInKadr[2]-23 != sizeof(struct wrstat) ) err |= 1;//����p��� ����� ���p��
// 23 = 4+1+16+2 - �p����㫠, ����p ��⪨, ��p���, ����p. �㬬�
    num = BufInKadr[4]; //����p ��⪨
    if(num > GLCONFIG.NumRun)             err |= 2;//����p��� ����p� ��⪨
//    if(memcmp(&BufInKadr[5],GLCONFIG.PASSWORD,16)) //�p���p�� ��p���
   if (nPort==2)
   { if (memcmp(&BufInKadr[5],PASSWR2,14))  err |= 4;}
   else
   { if (memcmp(&BufInKadr[5],PASSWR1,14))  err |= 4;}
//printf("S err=%02X p=%s %X %X %X %X\n",err,PASSWR2,BufInKadr[5], BufInKadr[6],BufInKadr[7],BufInKadr[8]);
    p   = &RESIV.WRSTAT;    //����᪨� ��p����p�
    movmem(&BufInKadr[4+17],&RESIV,sizeof(struct wrstat));//��p������� ��p����p��
  //nMeth = GLCONFIG.TypeCount & 3;     //��� 0,1: ��50\GERG\NX19 [0\1\2]
      run = &CONFIG_IDRUN[num-1];
    if (run->typeDens == 0)
    {
    if (p->ROnom < 0.66) err |= 8;        // ������ �।�� ���������
    if (p->ROnom > 1.13) err |= 16;           // �� ��⮤�
    }
    else
    {
    if (p->ROnom < 0.55) err |= 8;        // ������ �।�� ���������
    if (p->ROnom > 0.9) err |= 16;           // �� ��⮤�
    }
    if(err)
     SaveMessagePrefix_2(BufTrans,6,0xFF);  //����᪨� ��p����p� �� ����ᠭ�
    else  //�p��� �믮������ - 8 ��
    {
//TestPeriodRes();
      run = &CONFIG_IDRUN[num-1];
      if(memcmp((p->NameRun),(run->NameRun),16))
        { MEMAUDIT.type     = 0;
          movmem((run->NameRun),&MEMAUDIT.altvalue,4);
          movmem((p->NameRun),(run->NameRun),16);   //��� ��⪨
          movmem((run->NameRun),&MEMAUDIT.newvalue,4);
          Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->D20  != p->D20)
        { MEMAUDIT.type     = 4;
          MEMAUDIT.altvalue = run->D20;
          run->D20    = p->D20;   //����p����� ������p �p��, ��
          MEMAUDIT.newvalue = run->D20;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->d20  != p->d20)
        { MEMAUDIT.type     = 5;
          MEMAUDIT.altvalue = run->d20;
          run->d20    = p->d20;   //����p����� ������p ����p����, ��
          MEMAUDIT.newvalue = run->d20;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      dPotsw =  p->dPots;    // � ���
      if ((run->TypeRun == 4) || (run->TypeRun == 5) || (run->TypeRun == 10))
       p->dPots = dPotsw;
      else
      { double KDP = (GLCONFIG.EDIZM_T.EdIzmdP == 0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
        p->dPots = Round_toKg(dPotsw,KDP,&Ep);
      }
//       p->dPots = floor(dPotsw/KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]*1000.0+0.5)/1000.0; // � ���/�2
      if(fabs(run->dPots - p->dPots)>Ep)
        { MEMAUDIT.type     = 7;
          if ((run->TypeRun == 4) || (run->TypeRun == 5) || (run->TypeRun == 10))
            MEMAUDIT.altvalue = run->dPots;
          else
          { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
             MEMAUDIT.altvalue = run->dPots;
            else
            MEMAUDIT.altvalue = run->dPots*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
          }
          run->dPots  = p->dPots; //�p����� ���窨, ���/�2
          MEMAUDIT.newvalue = dPotsw;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
//     if(GLCONFIG.TypeCount & 0x80) //��� 7: ��p-const\��p-p���. [0\1]
    {      // ��� - ����
      if(run->Bd != p->Bd)
      { MEMAUDIT.type     = 20;
        MEMAUDIT.altvalue = run->Bd;
        run->Bd     = p->Bd;    //����. A ����p����
        MEMAUDIT.newvalue = run->Bd;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kBd != p->kBd)
      { MEMAUDIT.type     = 21;
        MEMAUDIT.altvalue = run->kBd;
        run->kBd    = p->kBd;   //����-�� B ����p����
        MEMAUDIT.newvalue = run->kBd;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kCd != p->kCd)
      { MEMAUDIT.type     = 23;
        MEMAUDIT.altvalue = run->kCd;
        run->kCd    = p->kCd;   //����-�� C ����p����
        MEMAUDIT.newvalue = run->kCd;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->Bt != p->Bt)
      { MEMAUDIT.type     = 33;
        MEMAUDIT.altvalue = run->Bt;
        run->Bt     = p->Bt;    //����-�� � �p��
        MEMAUDIT.newvalue = run->Bt;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kBt != p->kBt)
      { MEMAUDIT.type     = 34;
        MEMAUDIT.altvalue = run->kBt;
        run->kBt    = p->kBt;   //����-�� B �p��
        MEMAUDIT.newvalue = run->kBt;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kCt != p->kCt)
      { MEMAUDIT.type     = 35;
        MEMAUDIT.altvalue = run->kCt;
        run->kCt    = p->kCt;   //����-�� C �p��
        MEMAUDIT.newvalue = run->kCt;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
    }
      if(run->R != p->R)
      { MEMAUDIT.type     = 19;
        MEMAUDIT.altvalue = run->R;
        run->R      = p->R;     // ���. ��客����� ��㡮�஢���, ��
        MEMAUDIT.newvalue = run->R;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }

      dPotsw =  p->ValueSwitch;    // � ���
      double KDP = (GLCONFIG.EDIZM_T.EdIzmdP == 0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
      p->ValueSwitch = Round_toKg(dPotsw,KDP,&Ep);
//      p->ValueSwitch = floor(dPotsw/KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]*1000.0+0.5)/1000.0;
      if(fabs(run->ValueSwitch - p->ValueSwitch)>Ep)     // � ���/�2
      { MEMAUDIT.type     = 11;
        if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
          MEMAUDIT.altvalue = run->ValueSwitch;
        else
          MEMAUDIT.altvalue = run->ValueSwitch*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
        run->ValueSwitch  = p->ValueSwitch; //��p�� ��p����祭�� �� ��p�����
        MEMAUDIT.newvalue = dPotsw;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->typeOtb != p->typeOtb)//0 - 㣫����, 1 - 䫠�楢�
      { MEMAUDIT.type     = 12;//⨯ �⡮p� ��������
        *(unsigned long*)&MEMAUDIT.altvalue = (unsigned long)run->typeOtb;
        run->typeOtb      = p->typeOtb;
        *(unsigned long*)&MEMAUDIT.newvalue = (unsigned long)run->typeOtb;
        Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        Calc_SetNoErrTypeOtb();
      }

      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���

      if( Mem_ConfigWr() == 0 ) //������ ��p���p� ���䨣�p�樨 ���᫨⥫�
        SaveMessagePrefix_2(BufTrans,6,0x83);  //����᪨� ��p����p� ����ᠭ�
      else
        SaveMessagePrefix_2(BufTrans,6,0xFF);  //����᪨� ��p����p� �� ����ᠭ�
    }
    }
}
*/
//---------------------------------------------------------------------------

void RdCalcparam() {                    // �⥭�� ������ ���������� ��p����p��
 RdCalcparamp(BufInKadr1,BufTrans1);
 CheckBufTrans(BufTrans1); //�������� ��
 Ini_Send1();
}
void RdCalcparamp(unsigned char* BufInKadr,unsigned char* BufTrans)
{
   struct calcparam  *pcp,CALCPARAM;
   struct calcdata   *pcc;
   struct bintime    *ptime;            // ����筠� ��p���p� �p�����
   int num;
   int err =  0;
   union vm {
      unsigned char Bt[8];
      double Val;
   } Vm;
   float KofE;

   if (BufInKadr[2] != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SetCopyCalcData(num-1);
      pcc = &COPY_CALCDATA[num-1];      // ����� ��p����p�� p���� ��⪨
      pcp = &CALCPARAM;            // ����� ��������� ��p����p�
      ptime = &BINTIME;

      pcp->TypeCount = pcc->TypeCount;  // ��� 0,1: ��50\GERG\NX19 [0\1\2]; ��� 7: ��p-const\��p-p���. [0\1]
                                        // ᯮᮡ ����p���� �� ��⪥
      pcp->TypeRun = CONFIG_IDRUN[num-1].TypeRun;
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       pcp->Pin = pcc->Pin;
      else
       pcp->Pin = pcc->Pin*KoefP[GLCONFIG.EDIZM_T.EdIzP];              // ���. ��� �����. �������� �� �室� �p��p����, ���/�2
      pcp->t   = pcc->t;

      ConstCorr(P, &pcp->Pin, CONFIG_IDRUN[num-1].constdata);
      ConstCorr(T, &pcp->t, CONFIG_IDRUN[num-1].constdata);

      if ((pcp->TypeRun) == 4 || (pcp->TypeRun) == 5   // ���稪
          || (pcp->TypeRun) == 10)
        { pcp->sqrtQ  = pcc->kQind;      // �-� �ਢ������
          pcp->dP  = GetQwRaw(num-1); //GetQw
          pcp->Q = GetQ(num-1);
        }
      else
       {  pcp->sqrtQ  = pcc->sqrtQ;      // ��p��� ����p���
          if (CONFIG_IDRUN[num-1].Vlag == 1) pcp->dP = IndDP[num-1];
          else
        { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
           pcp->dP  = IndDP[num-1];
          else
           pcp->dP  = IndDP[num-1]*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
        }
          pcp->Q = IndQ[num-1];
       }
      if (CONFIG_IDRUN[num-1].Vlag == 0)
      ConstCorr(DP, &pcp->dP, CONFIG_IDRUN[num-1].constdata);
      pcp->Vs = Mem_GetDayQ(num-1);     // ��ꥬ �� ��⪨
                                        // ��ꥬ �� �p��� ��⪨ �� ��᫥���� ����� ���筮�� ����
      pcp->Va = (Mem_SecDayRdEnd(DAY,num-1)==0) ? MEMDATA.q : 0;
                                     //⥯��� �� ���� ����
      if (GLCONFIG.EDIZM_T.EdIzmE == 0)  // ����
       KofE=KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1000.0;
      else
       if (GLCONFIG.EDIZM_T.EdIzmE == 1)
        KofE=1.0;
       else
        KofE=KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
      pcp->PrevDayFHeat = (Mem_SecDayRdEnd(DAY,num-1)==0)  ? MEMDATA.FullHeat*KofE/1000.0 : 0;
                                        // ��騩 ��ꥬ, ���. �3
      pcp->Vtot = Persist_GetQTotal(num - 1) / 1000.0;

                                        // ���ࠢ��� �����⥫�� �� ���㯫���� ��. �஬�� �-�� ��� ��९���
                                        // ��� ��騩 ��ꥬ �� �� ��� ���稪�
      pcp->Kp = ((pcp->TypeRun) == 4 || (pcp->TypeRun) == 5
      || (pcp->TypeRun) == 10) ? Persist_GetQwCountTotal(num-1) : pcc->Kp;

      pcp->Ksh    = IndKsh[num-1];      // ���ࠢ-� ����-�� �� ��-��� ��-��

         pcp->Alpha_Eps  = pcc->Eps;    // ����-�� ���७��

      pcp->ROru   = pcc->ROmk1;         // ���⭮��� � ࠡ��� �᫮����
      pcp->E      = pcc->E;             // �-� ᪮��� �室�
      pcp->K      = pcc->K;             // �����樥�� ᦨ�������
      pcp->Dd     = pcc->Dd;            // ������� ����ࠣ�� �� �.�., ��
      pcp->Dt     = pcc->Dt;            // ������� ��㡮�஢��� �� �.�., ��

         pcp->mod_beta = pcc->Beta;     // �⭮�⥫쭮� �⢥��⨥

      pcp->RO     = pcc->ROnom;         // ���⭮���
      pcp->N2     = pcc->NN2;           // ����
      pcp->CO2    = pcc->NCO2;          // 㣫���᫮�

//    ����� ���� ������ ��ꥬ �� ���� �����

      ReadMonthSumm(num-1);             // ��ꥬ �� ���� �����
      pcp->Vpm           = (float)MonthSumm[num-1].PrevMonthV;
                                        // ⥯��� �� ���� �����
      pcp->PrevMonthFHeat = (float)MonthSumm[num-1].PrevMonthFHeat*KofE/1000.0;

      if ((pcp->TypeRun) == 4 || (pcp->TypeRun) == 5
      || (pcp->TypeRun) == 10) { // ���稪
         pcp->B2_C = IndDP[num-1];
         ConstCorr(QW, &pcp->B2_C, CONFIG_IDRUN[num-1].constdata);
      }
      else {                            // ����ࠣ��
            pcp->B2_C = IndC_[num-1];
      }
      pcp->Kapa   = pcc->capa;          // �������
      pcp->Re     = IndRe[num-1];       // ����⢨�-� �᫮ ��������
      pcp->Mu     = pcc->Mu;            // �������᪠� �離����, ���*c/�2

//      pcp->Vm  = GetVm(num-1);          // ����� Q1_C1 - ��ꥬ � ��砫� �����
//      if (GLCONFIG.EDIZM_T.EdIzmE == 1)
       pcp->Heat   = pcc->Heat;
//      else
//       pcp->Heat   = pcc->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];      // 㤥�쭠� ⥯��� ᣮp���� ���/�3
//      pcp->Heat = CONFIG_IDRUN[num-1].HsVV;
         pcp->B0_Z   = pcc->Z;          // 䠪�p ᦨ������� �p� p����� �᫮����
         pcp->F_Zc   = pcc->Zc;         // 䠪�p ᦨ������� �p� �⠭��p��� �᫮����
      ConstCorr(RO, &pcp->RO, CONFIG_IDRUN[num-1].constdata);
      ConstCorr(N2, &pcp->N2, CONFIG_IDRUN[num-1].constdata);
      ConstCorr(CO2, &pcp->CO2, CONFIG_IDRUN[num-1].constdata);
      ConstCorr(Heat, &pcp->Heat, CONFIG_IDRUN[num-1].constdata);
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       pcp->Pa = pcc->Pa;
      else
       pcp->Pa     = pcc->Pa*KoefP[GLCONFIG.EDIZM_T.EdIzP];     // ���. ��������, ���/�2
//        pcp->Pa  =   N2_G88;

      pcp->Month = ptime->month; // �����
      pcp->Day   = ptime->date;  // ����

      if ((CONFIG_IDRUN[num-1].TypeRun) == 4 || (CONFIG_IDRUN[num-1].TypeRun) == 5)
//      || (CONFIG_IDRUN[num-1].TypeRun) == 10)
         pcp->Day |= 0x80;              // ᯮᮡ ����p���� �� ��⪥

      pcp->Year  = ptime->year;  // ���
      pcp->Hours = ptime->hours; // ��
                                        // �����
      pcp->Minutes = ptime->minutes;
                                        // ᥪ㭤�
      pcp->Seconds = ptime->seconds;
      memmove(&BufTrans[5],pcp,sizeof(struct calcparam));
                                        // ������ �p�䨪�
      memcpy(&BufTrans[7],&BufTrans[9],134);
      SaveMessagePrefix_2(BufTrans,sizeof(struct calcparam)+7,0x84);
      BufTrans[4] = num;              // ����p ��⪨
   }
}

//---------------------------------------------------------------------------

void AnswChim() {                       // �⥭�� 娬��᪮�� ��⠢�
 AnswChimp(BufInKadr1,BufTrans1,NumCal2);                     // ��������� ��p����p�
      CheckBufTrans(BufTrans1); //�������� ��
      Ini_Send1();
}
void AnswChimp(unsigned char* BufInKadr,unsigned char*  BufTrans, unsigned char NumCal2)
{                    // ��������� ��p����p�
   extern struct bintime BINTIME;       // ����筠� ��p���p� �p�����
   struct calcdata   *pcalc;
   struct rdchim   *p,RDCHIM;
   struct IdRun    *run;                // ��p����p� ��⪨
   struct bintime  *ptime;              // ����筠� ��p���p� �p�����
   unsigned char num;
   int i,i1,l1,err = 0;
   struct instparam  *pip,INSTPARAM;              // ��������� ��p����p�
//   float Ep;

   if (BufInKadr[2] != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
//   CopyKoef();
// if ((INTPAR.HartReqDelay & 0x01) == 1) // ᮢ���⨬���� � ������� �� �.6
  if ((NumCal2 == INTPAR.RS485Com5Speed)&&(NumCal2 !=GLCONFIG.NumCal))
    {
      SetCopyCalcData(num-1);
      pip   = &INSTPARAM;        // ��������� ��p����p�
      pcalc = &COPY_CALCDATA[num-1];
      run   = &CONFIG_IDRUN[num-1];
      pip->Q = ((run->TypeRun) == 4 || (run->TypeRun) == 5) ? GetQ(num-1) : pcalc->Q; //GetQRaw
      if ((run->TypeRun) == 10) pip->Q = GetQ(num-1);

      pip->Vs = Mem_GetDayQ(num-1);//p��室 �� ��⪨
                                        // ��ꥬ �� �p��� ��⪨ �� ��᫥���� ����� ���筮�� ����
      pip->Va = (Mem_SecDayRdEnd(DAY,num-1)==0)  ? MEMDATA.q : 0;
                                        // ��騩 ��ꥬ, ���. �3
      pip->Vtot   = Persist_GetQTotal(num - 1) / 1000;
//      BufTrans[4] = num;              // ����p ��⪨jutport
      if ((INTPAR.CntBufLen) != 0) // ����� �⢥� 27 ��� 23
      { i1 = 9; l1 = 27;
      memset(&BufTrans[5],0x0,4);
      }
      else {i1 = 5; l1 = 23;}
      memmove(&BufTrans[i1],&INSTPARAM.Q,sizeof(struct instparam));
//      if ((INTPAR.HartReqDelay & 0x60) != 0) // swap4-ᮢ���⨬���� � ������� �� �.4
//      {
//       for (i=i1; i<(i1+16); i+=4)
//       Swap4(&BufTrans[i]);  //��ॢ����� �����
//      }
        SaveMessagePrefix_2(BufTrans,l1,0x86);
        BufTrans[1] = NumCal2;
     }// �����  if ((INTPAR.HartReqDelay & 0x01) == 1) // ᮢ���⨬���� � ������� �� �.4
     else
     {
      int n=num-1;
      p   = &RDCHIM;               // 娬��᪨� ��⠢
      run = &CONFIG_IDRUN[num-1];
      ptime = &BINTIME;
      p->TypeP = run->TypeP;            // ⨯ ���稪� ��������
      p->ROnom = run->RoVV;            // ���⭮��� ���� �� �.�., ��/�^3
      p->Pb    = run->Pb;               // !��஬����᪮� ��������, �� p�. ��.
    if (run->nKcompress == 3)
    {
      p->NN2   = XJ[n][1]*100.0;
      p->NCO2  = XJ[n][2]*100.0;
      p->Metan = XJ[n][0]*100.0;   //Metan;
      p->Etan  = XJ[n][3]*100.0; //run->Etan;
      p->Propan = XJ[n][4]*100.0; //run->Propan;
      p->isoButan = XJ[n][10]*100.0; //run->isoButan;
      p->nButan   = XJ[n][11]*100.0; //run->nButan;
      p->iso_Pentan = XJ[n][12]*100.0; //run->iso_Pentan;
      p->n_Pentan = XJ[n][13]*100.0; //run->n_Pentan;
      p->nGeksan   = XJ[n][14]*100.0; //nGeksan;
      p->nGeptan   = XJ[n][15]*100.0; //nGeptan
      p->Oktan  = XJ[n][16]*100.0;  //Oktan;
      p->HydroGen = XJ[n][7]*100.0; //run->HydroGen;
      p->MonoCO  = XJ[n][8]*100.0;  //MonoCO
      p->NomKanalH = NumChrom[n];  // ����� ������ �஬�⮣��
    }
    else
    {  p->NCO2  = run->NCO2;             // ����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
       p->NN2   = run->NN2;        // ����p��� ���� ���� � ������� ᬥ�, %
    }
      p->tDens     = run->typeDens; // ⨯ ���⭮�� ���/�⭮�
//printf("H2=%8.6f %8.6f CO=%8.6f %8.6f\n",p->HydroGen,XJ[n][7],p->MonoCO,XJ[n][8]);

//      p->UHeat  = run->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];    // 㤥�쭠� ⥯��� ᣮ࠭�� � ���
      p->UHeat  = run->HsVV; //*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];    // 㤥�쭠� ⥯��� ᣮ࠭�� � ���
// printf("UHeat=%f Heat=%f\n",p->UHeat,run->Heat);
      p->Month   = ptime->month;        // �����
      p->Day     = ptime->date;         // ����
      p->Year    = ptime->year;         // ���
      p->Hours   = ptime->hours;        // ��
      p->Minutes = ptime->minutes;      // �����
      p->Seconds = ptime->seconds;      // ᥪ㭤�
                                        // ������ �p�䨪�
      if (run->nKcompress == 3)
      {  SaveMessagePrefix_2(BufTrans,sizeof(struct rdchim),0x86); //79 = 4F
         memmove(&BufTrans[5],&RDCHIM,sizeof(struct rdchim));
         memcpy(&BufTrans[6],&BufTrans[9],71);
         memcpy(&BufTrans[77],&BufTrans[81],5);
      }
      else                                  // ������ ���p�
      {  SaveMessagePrefix_2(BufTrans,35,0x86);
         memmove(&BufTrans[5],&RDCHIM,20);
         memcpy(&BufTrans[6],&BufTrans[9],16);
         memmove(&BufTrans[22],&RDCHIM.Month,6);
         memmove(&BufTrans[28],&RDCHIM.UHeat,5);
      }
   }
    BufTrans[4] = num;              // ����p ��⪨
   }
}

//---------------------------------------------------------------------------

void WrChim() {                         // ������ 娬��᪮�� ��⠢�
 WrChimp(BufInKadr1,BufTrans1,2);                     // ��������� ��p����p�
 CheckBufTrans(BufTrans1); //�������� ��
 Ini_Send1();
}
void WrChimp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{                     // ������ ����������� ������� (���)
   struct wrchim *p;
   struct IdRun  *run;                 // ��p����p� �����
   unsigned char num;
   int err = 0;
   float UHeat,Ep;
   int K,n,j;
   float XJR[21];
//   char *pcXI,*pcZc;
   double Koef;

    num = BufInKadr[4];
    run = &CONFIG_IDRUN[num-1];
    if (run->nKcompress != 3) n=49;   else n=0;
uint16_t sz = sizeof(struct wrchim);
   if ((BufInKadr[2]-21) != (sizeof(struct wrchim)-n))
      err |= 1;                          // ����p��� ����� ���p��


   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨
                                        // �p���p�� ��p���
//   if (nPort==2)
//   { if (memcmp(&BufInKadr[5],PASSWR2,14))  err |= 4;}
//   else
//   { if (memcmp(&BufInKadr[5],PASSWR1,14))  err |= 4;}

   p   = &RESIV.WRCHIM;              // 娬��᪨� ��⠢

   memmove(&RESIV,&BufInKadr[5+16],sizeof(struct wrchim)); // ��p������� ��p����p��
   if (run->nKcompress == 3)
      p->tDens = BufInKadr[90];
   else
     p->tDens = BufInKadr[41];

//printf("err=%X ROnom=%f\n",err,p->ROnom);
  
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // 娬��᪨� ��⠢ �� ����ᠭ
   else {
      if ((p->tDens==0) || (p->tDens==1))
      if (p->tDens != run->typeDens)
      {
         MEMAUDIT.type     = 9;
         MEMAUDIT.altvalue = run->typeDens;
         run->typeDens = p->tDens;
         MEMAUDIT.newvalue = p->tDens;
                                        // ������ ���� ����⥫���
         Mem_AuditWr(num-1,MEMAUDIT);
      }
      if  (run->nKcompress == 3)
      if ((p->NomKanalH != NumChrom[num-1]) && (p->NomKanalH >= 0)&&(p->NomKanalH < 4))
      {
         MEMAUDIT.type     = 10;
         MEMAUDIT.altvalue = NumChrom[num-1];
         NumChrom[num-1] = p->NomKanalH;
         MEMAUDIT.newvalue = p->NomKanalH;
                                        // ������ ���� ����⥫���
         Mem_AuditWr(num-1,MEMAUDIT);
      }

      if (((run->typeDens == 0) && ((p->ROnom < 1.175) && (p->ROnom > 0.65)))        // ��� ��᮫�⭮� ����
       || ((run->typeDens == 1)&&((p->ROnom < 0.91) && (p->ROnom > 0.54)))) // ��� �⭮��.����
      {
       if ((run->RoVV  != p->ROnom)&&(((run->constdata & 0x10)==0) || (run->nKcompress != 3)))
       {
         MEMAUDIT.type     = 1;
         MEMAUDIT.altvalue = run->RoVV;
          if (run->typeDens == 1) // ��ॢ�� �� �⭮�⥫쭮� � ���.
          {
            run->ROnom = p->ROnom * RLair[GLCONFIG.EDIZM_T.T_VVP]; // ���⭮��� ������ �� ⥬������ �����
            run->ROnom *= KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];//��� t�ਢ. SU
          }
          else       // �ਢ������ ��᮫. ���⭮�� � ��. �����. SU
            run->ROnom = p->ROnom*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];  // ���⭮��� ���� �� �.�., ��/�^3

         MEMAUDIT.newvalue = p->ROnom;
         run->RoVV = p->ROnom;
         CalcZc[num-1]=1;
         Mem_AuditWr(num-1,MEMAUDIT); // ������ ���� ����⥫���
       }
      }
//      else
//        run->ROnom = run->RoVV*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];  // ���⭮��� ���� �� �.�., ��/�^3
      if (run->Pb  != p->Pb) {
         MEMAUDIT.type     = 6;
         MEMAUDIT.altvalue = run->Pb;
         run->Pb           = p->Pb;     // ��஬����᪮� ��������, �� p�. ��.
         MEMAUDIT.newvalue = run->Pb;
                                        // ������ ���� ����⥫���
         Mem_AuditWr(num-1,MEMAUDIT);
      }
   if (run->nKcompress != 3)
   {   if ((run->NCO2  != p->NCO2)&&(p->NCO2>=0)&&(p->NCO2<15.0))
      {
         MEMAUDIT.type     = 2;
         MEMAUDIT.altvalue = run->NCO2;
         run->NCO2         = p->NCO2;   // ����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
         MEMAUDIT.newvalue = run->NCO2;
         CalcZc[num-1]=1;
                                        // ������ ���� ����⥫���
         Mem_AuditWr(num-1,MEMAUDIT);
      }

      if ((run->NN2  != p->NN2)&&(p->NN2>=0)&&(p->NN2<15.0))
      {
         MEMAUDIT.type     = 3;
         MEMAUDIT.altvalue = run->NN2;
         run->NN2          = p->NN2;    // ����p��� ���� ���� � ������� ᬥ�, %
         MEMAUDIT.newvalue = run->NN2;

                                        // ������ ���� ����⥫���
         Mem_AuditWr(num-1,MEMAUDIT);
      }
    }
// printf("constH=%02X ",run->constdata);
   if (((run->constdata & 0x20)==0) || (run->nKcompress != 3))
   {   UHeat =  p->UHeat;    // � ���*�
      if (GLCONFIG.EDIZM_T.EdIzmE == 1)
       Koef = 1.0;
      else
       Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
       p->UHeat = Round_toKg(UHeat,Koef,&Ep);  // ��ॢ�� � ���
//printf("UHeat=%f Heat=%f HsVV=%f dh=%f Ep=%f\n",UHeat,p->UHeat,run->HsVV, run->Heat  - p->UHeat,Ep);

     if ((p->UHeat > 18.5)&&(p->UHeat < 51.5))
     { if (fabs(run->HsVV  - UHeat)>Ep)
      { MEMAUDIT.type     = 37;
        MEMAUDIT.altvalue = run->HsVV; //*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
        run->Heat  = p->UHeat*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
        MEMAUDIT.newvalue = UHeat;
        run->HsVV = UHeat;
        CalcZc[num-1]=1; //run->nGerg88_AGA8;
                                    // ������ ���� ����⥫���
        Mem_AuditWr(num-1,MEMAUDIT);
      }
      else
       { run->HsVV = UHeat;
         run->Heat  = p->UHeat*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
       }
     }
    }  // ����� if (run->constdata & 0x20==0)
     StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���
//printf("Wr Heat=%f tDens=%d Buf=%d %d typeDens=%d\n",p->UHeat,p->tDens,BufInKadr[41],BufInKadr[90],run->typeDens);

    if (run->nKcompress == 3)
    {
// KUh= UHeat;   // �⫠���� ���� ����.ᦨ�
     for (n=0; n<21; n++) XJR[n] =  XJ[num-1][n];
        if ((p->NN2>=0.0)&&(p->NN2<20.01))
     { XJR[1] = p->NN2/100.0;
     }
     if ((p->NCO2>=0.0)&&(p->NCO2<20.01))
     { XJR[2] = p->NCO2/100.0;
     }
     if ((p->Metan >= 60.0)&&(p->Metan < 100.001))
      XJR[0] = p->Metan/100.0;   //Metan;
      if ((p->Etan >= 0)&&(p->Etan < 10.001))
      XJR[3] = p->Etan/100.0; //run->Etan;
      if ((p->Propan >= 0)&&(p->Propan < 3.5001))
      XJR[4] = p->Propan/100.0; //run->Propan;

      if ((p->isoButan >= 0)&&(p->isoButan < 2.5001))
      XJR[10] = p->isoButan/100.0; //run->isoButan;
      if ((p->nButan >= 0)&&(p->nButan < 2.0001))
      XJR[11] = p->nButan/100.0; //run->nButan;
      if ((p->iso_Pentan >= 0)&&(p->iso_Pentan < 2.0001))
      XJR[12] = p->iso_Pentan/100.0; //run->iso_Pentan;
      if ((p->n_Pentan >= 0)&&(p->n_Pentan < 2.0001))
      XJR[13] = p->n_Pentan/100.0; //run->n_Pentan;
      if ((p->Geksan >= 0)&&(p->Geksan < 2.0001))
      XJR[14] = p->Geksan/100.0; //run->Geksan;
      if ((p->nGeptan >= 0)&&(p->nGeptan < 1.0001))  // CO
      XJR[15] = p->nGeptan/100.0; //run->nGeptan
      if ((p->Oktan >= 0)&&(p->Oktan < 0.5001))
      XJR[16] = p->Oktan/100.0;   //run->SulfidH;
      if ((p->HydroGen >= 0)&&(p->HydroGen < 10.0001))
      XJR[7] = p->HydroGen/100.0; //run->HydroGen;
      if ((p->MonoCO >= 0)&&(p->MonoCO < 3.0001))
      XJR[8] = p->MonoCO/100.0;   //

char bNoEqvel=0;
   if (NumChrom[num-1] > 0)
   {       K = (num-1);
           for (n=0; n<21; n++)
           {
             if (fabs(XJR[n]-XJ[K][n]) > 0.0000009)
             { bNoEqvel = n+1;
               MEMAUDIT.type     = 162+n;
               MEMAUDIT.altvalue = XJ[K][n];
               MEMAUDIT.newvalue = XJR[n];
               XJ[K][n] = XJR[n];
               if (n==1) run->NN2  = XJR[n]*100.0;
               if (n==2) run->NCO2 = XJR[n]*100.0;
                                    // ������ ���� ����⥫���
               Mem_AuditWr(num-1,MEMAUDIT);
             }
           }
     if (bNoEqvel)  /// ���� ������� ��������� � ������������ ���
     {
         for (j=0; j<GLCONFIG.NumRun; j++)
           if  ((j!=(num-1)) && (NumChrom[num-1] == NumChrom[j]))
             Mem_AuditWr(num-1,MEMAUDIT);

         CalcBR[K] = 1;   //��ࠡ�⪠ XJ, XI
         Dmin=0.000001;   //����ୠ� ���⭮��� ���� �����
         Dmax=40.0;    //����ୠ� ���⭮��� ���� �����

         for (j=0; j<GLCONFIG.NumRun; j++)
         {
           if ((j != K) && (CalcBR[K] == 1)
              && (NumChrom[K] == NumChrom[j]) && (CONFIG_IDRUN[j].nKcompress == 3))
            {  CalcBR[j] = 0;    //  ��� j-�� ������ ���㦭� ��ࠡ�⪠
               memcpy(&XJ[j][0],&XJ[K][0],sizeof(XJR));   //������ XJR � XJ[k]
               CONFIG_IDRUN[j].NN2 = XJ[j][1]*100.0;
               CONFIG_IDRUN[j].NCO2 = XJ[j][2]*100.0;

               MEMAUDIT.type     = 8;
               MEMAUDIT.altvalue = XJ[j][bNoEqvel-1];
               MEMAUDIT.newvalue = XJR[bNoEqvel-1];
                        // ������ ���� ����⥫��� � ���� j
              Mem_AuditWr(num-1,MEMAUDIT);
            }
         }  // end for (j=0; j<
//printf("CalcBR0=%d CalcBR1=%d  S=%d c=%d\n",CalcBR[0],CalcBR[1],BINTIME.seconds,counttime);
        }  //����� if (bNoEqvel)
        else  //if XJR == XJ[k]    ���� ⠪��-�� ���⮣ࠬ��
            CalcBR[K] = 0;   //��� ��ࠡ�⪨ XJ, XI
       } //  �����  (NumChrom[num-1] > 0)
      } // end if (run->nKcompess == 3)
//      CONFIG_IDRUN[num-1].crc = Mem_Crc16((uint8_t*)&CONFIG_IDRUN[num-1],SizeIdR-4);
//      pAdr = &AdrIdRun[num-1][0];
//      P23K256_Write_Data(&hspi3,pAdr,(uint8_t*)&CONFIG_IDRUN[num-1],SizeIdR); 
////     ������ ��������� ������������    
//      T24_I2C_256_Write_Data(&hi2c1, AdrIdRun[num-1][0]*256+AdrIdRun[num-1][1],(uint8_t*)&CONFIG_IDRUN[num-1], SizeIdR);      
            Mem_ConfigRunWr(num-1);
      if (run->nKcompress == 3)
       P23K256_Write_Data(&hspi3,AdrFHP,(uint8_t*)XJ,SizeFHP);   
      SaveMessagePrefix_2(BufTrans,6,0x87);     // 娬��᪨� ��⠢ ����ᠭ
   }
}

//---------------------------------------------------------------------------
void RdConst() {                        // �⥭�� ����⠭�
 RdConstp(BufInKadr1,BufTrans1);                     // ��������� ��p����p�
    CheckBufTrans(BufTrans1); //�������� ��
    Ini_Send1();  // �������� BufTrans
}
void RdConstp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct rdflconst  *p,RDFLCONST;
   struct IdRun      *prun;             // ��p����p� ��⪨
   unsigned char     num;
   int           err = 0;

   if (BufInKadr[2] != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = 3 & BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      p    = &RDFLCONST;           // ����⠭��
      prun = &CONFIG_IDRUN[num-1];    // 㪠��⥫� �� ��p����p� ��⪨
                                        // ᯮᮡ ����p���� �� ��⪥
      p->TypeRun = prun->TypeRun  | (prun->Dens<<7);
      p->flag    = prun->constdata;     // ���筨� ����⠭�\���稪 0\1
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       p->constdP  = prun->constdP;
      else
       p->constdP  = prun->constdP*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]; //�p����� ���窨, ���/�2
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       p->constP  = prun->constP;
      else
       p->constP  = prun->constP*KoefP[GLCONFIG.EDIZM_T.EdIzP]; //�p����� ���窨, ���/�2
      p->constt  = prun->constt;        // ����⠭� ⥬��p���p�
      p->constqw = prun->constqw;       // ����⠭� p��室� �p� p.�., �3/��
      p->constRo   = prun->RoVV;       // ���⭮���
      p->constHeat  = prun->HsVV;
      p->constCO2  = prun->NCO2;        // 㣫���᫮�
      p->constN2   = prun->NN2;         // ����

      SaveMessagePrefix_2(BufTrans,sizeof(struct rdflconst)+7,0x88);  //������ �p�䨪�
      BufTrans[4] = num;                            //����p ��⪨
      memmove(&BufTrans[5],&RDFLCONST,2);  //������ ���p�
      memmove(&BufTrans[7],&RDFLCONST.constdP,sizeof(struct rdflconst)-4);  //������ ���p�
   }

}
//---------------------------------------------------------------------------
void WrConst() {                        // ������ ����⠭�
 WrConstp(BufInKadr1,BufTrans1,2);                     // ��������� ��p����p�
    CheckBufTrans(BufTrans1); //�������� ��
    Ini_Send1();  // �������� BufTrans
}
void WrConstp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{
   struct wrflconst  *p;
   struct IdRun      *prun;             // ��p����p� ��⪨
   unsigned char     num;
   int err = 0;
   int i;
   float Ep, Cp;
   unsigned char OldFlags,NewFlags,
                 OldFlags1,NewFlags1,OneFlag;

//if (Nwr2 == 0) err |= 0x80;           // 䫠� ����� �������
   if (BufInKadr[2]-23 != (sizeof(struct wrflconst)-3))
      err |= 1;                         // ����p��� ����� ���p��

   num = 3 & BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨
                                        // �p���p�� ��p���
//   if (nPort==2)
//   { if (memcmp(&BufInKadr[5],PASSWR2,14))  err |= 4;}
//   else
//   { if (memcmp(&BufInKadr[5],PASSWR1,14))  err |= 4;}

   p   = (struct wrflconst *)&RESIV.WRFLCONST;    // ����⠭��
   RESIV.WRFLCONST.flag = BufInKadr[4+17];
   memmove(&RESIV.WRFLCONST.constdP,&BufInKadr[4+18],BufInKadr[2]-22);

   if ((p->constRo > 1.2) || (p->constRo < 0.54))
      err |= 16;

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // ����⠭�� �� ����ᠭ�
   else {
      prun = &CONFIG_IDRUN[num-1];    // 㪠��⥫� �� ��p����p� ��⪨
                                        // ��p������� ��p����p��
//      movmem(&BufInKadr[4+17],&RESIV,sizeof(wrflconst));
                                        // �⮡ࠦ��� ����⠭�� �� ����⥫��⢠� �� ���室� �� ����⠭��
      unsigned char fConst = prun->constdata & ~(p->flag);

      OldFlags = prun->constdata;
      NewFlags = p->flag;

      for (i=0; i<4; i++) {             // 䨪��� ���਩
         OneFlag = 1 << i;              // ����ன�� �� ��।��� ��ࠬ���
         OldFlags1 = OldFlags & OneFlag;
         NewFlags1 = NewFlags & OneFlag;

                                        // � ��� ����� �� ������ ���਩
         if (OldFlags1 != NewFlags1) {  // �ந��諠 ᬥ�� �ਧ����
            switch(i) {
               case 0:                  // �������
                                        // �� ���稪
//                if (CONFIG_IDRUN[num-1].TypeRun != 4) {
                  if (((CONFIG_IDRUN[num-1].TypeRun) != 4) && ((CONFIG_IDRUN[num-1].TypeRun) != 5)
                  && ((CONFIG_IDRUN[num-1].TypeRun) != 10)) {
                                        // ���⠭���� �� ����⠭��
                     if (NewFlags1 == 0) {
                           Mem_AlarmWrCodRun(num-1,0x97,Mem_GetDayQ(num-1));
                     }
                     else {             // ��⨥ � ����⠭��
                                        // ��� �����
                           Mem_AlarmWrCodRun(num-1,0x17,Mem_GetDayQ(num-1));
                     }
                  }
                  break;
               case 1:                  // ��������
                  if (NewFlags1 == 0) { // ���⠭���� �� ����⠭��
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0xA0,Mem_GetDayQ(num-1));
                  }
                  else {                // ��⨥ � ����⠭��
                                        // ��� �����
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0x20,Mem_GetDayQ(num-1));
                  }
                  break;
               case 2:                  // �����������
                  if (NewFlags1 == 0) { // ���⠭���� �� ����⠭��
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0xA1,Mem_GetDayQ(num-1));
                  }
                  else {                // ��⨥ � ����⠭��
                                        // ��� �����
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0x21,Mem_GetDayQ(num-1));
                  }
                  break;
               case 3:                  // �������
                  if ((CONFIG_IDRUN[num-1].TypeRun & 0x0F) == 4 || (CONFIG_IDRUN[num-1].TypeRun & 0x0F)==5
                  || (CONFIG_IDRUN[num-1].TypeRun & 0x0F)  == 10) {
                                        // ���⠭���� �� ����⠭��
                     if (NewFlags1 == 0) {
//                        if (prun->NoAlarmConstFlag == 0)
                           Mem_AlarmWrCodRun(num-1,0xA2,Mem_GetDayQ(num-1));
                     }
                     else {             // ��⨥ � ����⠭��
                                        // ��� �����
//                        if (prun->NoAlarmConstFlag == 0)
                           Mem_AlarmWrCodRun(num-1,0x22,Mem_GetDayQ(num-1));
                     }
                  }
                  break;
            }
         }
      }

      if (prun->constdata != p->flag) {
         MEMAUDIT.type     = 40;
         *(unsigned long*)&MEMAUDIT.altvalue = (unsigned long)prun->constdata;
         prun->constdata = p->flag;      // ���筨� ����⠭�\���稪 0\1
         *(unsigned long*)&MEMAUDIT.newvalue = (unsigned long)prun->constdata;
//         Mem_AuditWr(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
//       CopyKoef();
       Cp= p->constdP;
       double KDP = (GLCONFIG.EDIZM_T.EdIzmdP == 0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
       p->constdP = Round_toKg(Cp,KDP,&Ep);
       if (fabs(prun->constdP - p->constdP)>Ep || (fConst & 0x01)) {
         MEMAUDIT.type     = 41;
         if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
           MEMAUDIT.altvalue = prun->constdP;
         else
           MEMAUDIT.altvalue = prun->constdP*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
         prun->constdP   = p->constdP;  //����⠭� ��p�����
         MEMAUDIT.newvalue = Cp;
         Mem_AuditWr(num-1,MEMAUDIT);
         //������ ���� ����⥫���
      }
       Cp= p->constP;
       double KP = (GLCONFIG.EDIZM_T.EdIzP == 0) ? 1.0 : KoefP[GLCONFIG.EDIZM_T.EdIzP];
       p->constP = Round_toKg(Cp,KP,&Ep);
      if (fabs(prun->constP - p->constP)>Ep || (fConst & 0x02)) {
         MEMAUDIT.type     = 42;
        if (GLCONFIG.EDIZM_T.EdIzP==0)
         MEMAUDIT.altvalue = prun->constP;
        else
         MEMAUDIT.altvalue = prun->constP*KoefP[GLCONFIG.EDIZM_T.EdIzP];
         prun->constP    = p->constP;   //����⠭� ��������
         MEMAUDIT.newvalue = Cp;
         Mem_AuditWr(num-1,MEMAUDIT);//������ ���� ����⥫���
      }

      if ((prun->constt != p->constt) || (fConst & 0x04)) {
         MEMAUDIT.type     = 43;
         MEMAUDIT.altvalue = prun->constt;
         prun->constt    = p->constt;   //����⠭� ⥬��p���p�
         MEMAUDIT.newvalue = prun->constt;
         Mem_AuditWr(num-1,MEMAUDIT);//������ ���� ����⥫���
      }

      if ((prun->constqw != p->constqw) || (fConst & 0x08)) {
         MEMAUDIT.type     = 44;
         MEMAUDIT.altvalue = prun->constqw;
         prun->constqw   = p->constqw;   //����⠭� p��室� �p� p.�., �3/��
         MEMAUDIT.newvalue = prun->constqw;
         Mem_AuditWr(num-1,MEMAUDIT);//������ ���� ����⥫���
      }
      if ((prun->RoVV != p->constRo) || (fConst & 0x10)) {
         MEMAUDIT.type     = 46;
         MEMAUDIT.altvalue = prun->RoVV;
         prun->RoVV       = p->constRo;       // ����⠭� ���⭮��
         prun->ROnom       = p->constRo*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];       // ����⠭� ���⭮��
         CalcZc[num-1] = 1;  //����� ����, ᦨ������� �� �� �� GERG88
         MEMAUDIT.newvalue = prun->RoVV;
         Mem_AuditWr(num-1,MEMAUDIT); // ������ ���� ����⥫���
      }
      else if ((fConst & 0x10) == 0)
      {
         CalcBR[num-1] = 1;   //��ࠡ�⪠ XJ, XI �� AGA8, ���� Heat
     }
       Cp= p->constHeat;
        double Koef = (GLCONFIG.EDIZM_T.EdIzmE == 1) ? 1.0 : KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
       p->constHeat = Round_toKg(Cp,Koef,&Ep);  // ��ॢ�� � ���
     if (p->constHeat > 5.0)
     { if (fabs(prun->HsVV - p->constHeat)>Ep || (fConst & 0x20)) {
         MEMAUDIT.type     = 37;
         MEMAUDIT.altvalue = prun->HsVV; //prun->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
         prun->Heat        = p->constHeat*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];     // ����⠭� 㤥�쭮� ⥯����
         prun->HsVV        = Cp;  //p->constHeat;     // ����⠭� 㤥�쭮� ⥯����
         MEMAUDIT.newvalue = Cp;
         CalcZc[num-1] =1;  //  ����� ����, ᦨ������� �� �� �� GERG88
         Mem_AuditWr(num-1,MEMAUDIT); // ������ ���� ����⥫���
      }
      else if ((fConst & 0x20) == 0)
      {
         CalcBR[num-1] = 1;   //��ࠡ�⪠ XJ, XI �� AGA8, ���� Heat
      }
     }
      if ((prun->NCO2 != p->constCO2) || (fConst & 0x40)) {
         MEMAUDIT.type     = 38;
         MEMAUDIT.altvalue = prun->NCO2;
         prun->NCO2        = p->constCO2;          // ����⠭� ��2
         CalcZc[num-1] = 1;
         MEMAUDIT.newvalue = prun->NCO2;
         Mem_AuditWr(num-1,MEMAUDIT); // ������ ���� ����⥫���
      }

      if ((prun->NN2 != p->constN2) || (fConst & 0x80)) {
         MEMAUDIT.type     = 39;
         MEMAUDIT.altvalue = prun->NN2;
         prun->NN2         = p->constN2;         // ����⠭� N2
         CalcZc[num-1] = 1;
         MEMAUDIT.newvalue = prun->NN2;
         Mem_AuditWr(num-1,MEMAUDIT); // ������ ���� ����⥫���
      }

      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���
      SaveMessagePrefix_2(BufTrans,6,0x89);     // ����⠭�� ����ᠭ�
      Mem_ConfigRunWr(num-1); 
//      pAdr = &AdrIdRun[num-1][0];
////    ������ ������������ �� ����� num-1      
//      P23K256_Write_Data(&hspi3,pAdr,(uint8_t*)&CONFIG_IDRUN[num-1],SizeIdR); 
////     ������ ��������� ������������    
//      T24_I2C_256_Write_Data(&hi2c1, AdrIdRun[num-1][0]*256+AdrIdRun[num-1][1],(uint8_t*)&CONFIG_IDRUN[num-1], SizeIdR);      
   }
}

//-----------------------------------------------------------
void SetBinTime4(int pg,int stpoint,struct bintime *BinT1,struct bintime *BinT2) {
   int tInd,tPoint,nP,RecLen;

   RecLen = sizeof(struct memdata);
   nP = 0;

   if (stpoint < ARC1)
      tInd = stpoint*RecLen+4;
   else
   if (stpoint < ARC2) {
      tInd = (stpoint-ARC1)*RecLen+4;
      nP += 1;
   }
   else
   if (stpoint < ARC3) {
      tInd = (stpoint-ARC2)*RecLen+4;
      nP += 2;
   }
   else {
      tInd = (stpoint-ARC3)*RecLen+4;
      nP += 3;
   }

//   outportb(0x1D0,pg+nP);
//   BinT1->year    = peekb(SEGMENT,tInd+5);
//   BinT1->month   = peekb(SEGMENT,tInd+4);
//   BinT1->date    = peekb(SEGMENT,tInd+3);
//   BinT1->hours   = peekb(SEGMENT,tInd+2);
//   BinT1->minutes = peekb(SEGMENT,tInd+1);
//   BinT1->seconds = peekb(SEGMENT,tInd);

//   outportb(0x1D0,pg);

   nP = 0;
//   tPoint = peek(SEGMENT,0);

   if (tPoint < ARC1)
      tInd = tPoint*RecLen+4;
   else
   if (tPoint < ARC2) {
      tInd = (tPoint-ARC1)*RecLen+4;
      nP += 1;
   }
   else
   if (tPoint < ARC3) {
      tInd = (tPoint-ARC2)*RecLen+4;
      nP += 2;
   }
   else {
      tInd = (tPoint-ARC3)*RecLen+4;
      nP += 3;
   }

//   outportb(0x1D0,pg+nP);
//   BinT2->year    = peekb(SEGMENT,tInd+5);
//   BinT2->month   = peekb(SEGMENT,tInd+4);
//   BinT2->date    = peekb(SEGMENT,tInd+3);
//   BinT2->hours   = peekb(SEGMENT,tInd+2);
//   BinT2->minutes = peekb(SEGMENT,tInd+1);
//   BinT2->seconds = peekb(SEGMENT,tInd);
}
//-----------------------------------------------------------
void SetBinTime2(int pg,int stpoint,struct bintime *BinT1,struct bintime *BinT2) {
   int tInd,tPoint,nP,RecLen;

   RecLen = sizeof(struct memdata);
   nP = 0;

   if (stpoint < ARC1)
      tInd = stpoint*RecLen+4;
   else {
      tInd = (stpoint-ARC1)*RecLen+4;
      nP += 1;
   }

//   outportb(0x1D0,pg+nP);
//   BinT1->year    = peekb(SEGMENT,tInd+5);
//   BinT1->month   = peekb(SEGMENT,tInd+4);
//   BinT1->date    = peekb(SEGMENT,tInd+3);
//   BinT1->hours   = peekb(SEGMENT,tInd+2);
//   BinT1->minutes = peekb(SEGMENT,tInd+1);
//   BinT1->seconds = peekb(SEGMENT,tInd);

//   outportb(0x1D0,pg);

   nP = 0;
//   tPoint = peek(SEGMENT,0);

   if (tPoint < ARC1)
      tInd = tPoint*RecLen+4;
   else {
      tInd = (tPoint-ARC1)*RecLen+4;
      nP += 1;
   }

//   outportb(0x1D0,pg+nP);
//   BinT2->year    = peekb(SEGMENT,tInd+5);
//   BinT2->month   = peekb(SEGMENT,tInd+4);
//   BinT2->date    = peekb(SEGMENT,tInd+3);
//   BinT2->hours   = peekb(SEGMENT,tInd+2);
//   BinT2->minutes = peekb(SEGMENT,tInd+1);
//   BinT2->seconds = peekb(SEGMENT,tInd);
}
//-----------------------------------------------------------
void SetBinTimeV1(int pg,int stpoint,struct bintime *BinT1,struct bintime *BinT2) {
   int tInd,tPoint,RecLen;

//   RecLen = sizeof(struct valarm_data);

   tInd = stpoint*RecLen+4;

//   outportb(0x1D0,pg);
//   BinT1->year    = peekb(SEGMENT,tInd+5);
//   BinT1->month   = peekb(SEGMENT,tInd+4);
//   BinT1->date    = peekb(SEGMENT,tInd+3);
//   BinT1->hours   = peekb(SEGMENT,tInd+2);
//   BinT1->minutes = peekb(SEGMENT,tInd+1);
//   BinT1->seconds = peekb(SEGMENT,tInd);

//   tPoint = peek(SEGMENT,0);

   tInd = tPoint*RecLen+4;

//   outportb(0x1D0,pg);
//   BinT2->year    = peekb(SEGMENT,tInd+5);
//   BinT2->month   = peekb(SEGMENT,tInd+4);
//   BinT2->date    = peekb(SEGMENT,tInd+3);
//   BinT2->hours   = peekb(SEGMENT,tInd+2);
//   BinT2->minutes = peekb(SEGMENT,tInd+1);
//   BinT2->seconds = peekb(SEGMENT,tInd);
   if (BinT2->month == 0) BinT2->month = 1;
   if (BinT2->date == 0) BinT2->date =1;
}
//---------------------------------------------------------------------------

void RdPeriod() {                       // �⥭�� ���p�⨢��� (��p�����᪨�) ������
portC=2;
RdPeriodp(BufInKadr1,BufTrans1,&STARTENDTIME);
CheckBufTrans(BufTrans1); //�������� ��
Ini_Send1();
    
}
void RdPeriodp(unsigned char* BufInKadr,unsigned char*  BufTrans,  struct startendtime *STARTENDTIM) {    // �⥭�� ��ਮ���᪨� ������
   static  unsigned int   startpoint2,endpoint2;
   static  unsigned int   startpoint1,endpoint1;
   static unsigned int  startpoint,endpoint;
   struct bintime      *pbtime;
   struct ReqPeriod    *prp;//��p���p� ���p��
   unsigned char       num,run;
   unsigned char       page;
   struct periodrecord *pprec;//��p���p� ���p�⨢��� ������ ��� ��p����
   struct periodrecordden *pprecd;
   unsigned int        point;
   int                 count, sizepp,sizeppd, Nrec;
   int                 err,nfc;
   float absQ;
   uint16_t i;
   union  resiv  RESIV;
   struct responseperiodden   RESPONSEPERIODDEN;
   struct responseperiod   RESPONSEPERIOD;  // �⥭�� 横���᪨� ������
   char RH, bmQ, bmT,bmRo, w, bCount;

   RH= (INTPAR.Com2Mode==0);// && (INTPAR.HeatReport==0));
   if (RH) // ��� ���������
           Nrec = 9;
   else    Nrec = 7;
   err = 0;
   count = 0;
   point = 0;
   sizepp = sizeof(struct periodrecord);
   sizeppd = sizeof(struct periodrecordden);
//printf("sizepp=%d sizepd=%d\n",sizepp,sizeof(struct periodrecordden));
   if (BufInKadr[2]-sizeof(struct ReqPeriod) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4];
   nfc = BufInKadr[3];
   run = num-1;
   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pprec = &RESPONSEPERIOD.PERIODRECORD[0];
   pprecd = &RESPONSEPERIODDEN.PERIODRECORDDEN[0];
   prp = &RESIV.REQPERIOD;
                              // ��p������� ��p����p�� ���p���
   memmove(prp,&BufInKadr[5],sizeof(struct ReqPeriod));

   if (prp->RequestNum != 0)
   {
                    // ��������� ������� ����� �� ��������� ��������� ������
      if (STARTENDTIM->RequestNum == prp->RequestNum)
         return;
      if (STARTENDTIM->RequestNum +1 != prp->RequestNum)
         err |= 0x04;               // ����p��� ������������������ ���p����
   }
                                      // ����� �������
   STARTENDTIM->RequestNum = prp->RequestNum;
   STARTENDTIM->num = 3 & (num-1);   // ����� �����

                                   // ��p������ 1-�� ���p���
   if (STARTENDTIM->RequestNum == 0) {
      pbtime = &STARTENDTIM->STARTTIME;

      pbtime->seconds = 0;
      pbtime->minutes = prp->StartMinutes;
      pbtime->hours   = prp->StartHours;
      pbtime->date    = prp->StartDay;
      pbtime->month   = prp->StartMonth;
      pbtime->year    = prp->StartYear;

      if (Mem_TestReqTime(pbtime))
         err |= 8;                   // ����p��� �p����� ���p��

  //    STARTENDTIME.RequestNum = prp->RequestNum;
      pbtime = &STARTENDTIM->ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = prp->EndMinutes+GLCONFIG.Period-1;

      if (pbtime->minutes > 59)
         pbtime->minutes= 59;

      pbtime->hours   = prp->EndHours;
      pbtime->date    = prp->EndDay;
      pbtime->month   = prp->EndMonth;
      pbtime->year    = prp->EndYear;

      if (Mem_TestReqTime(pbtime))
         err |= 0x10;           // ����p��� �p����� ���p���

      if (Mem_TestReqTimeSEp(STARTENDTIM))
         err |= 0x20; // ����p��� ����������� �p���� ���p���
                 // �������� ������ �� 1 �� 0x24 - ������ ���p���
// STARTENDTIM - ������������ 
       startpoint = pArchiv.di1p[run];
      for (uint16_t is=0; is<20; is=is+1)
   {    ReadDataW25_IT(PER, run, is  ,(uint8_t*)&MEMDATA);
       uint8_t sec = MEMDATA.STARTTIME.hours;
       sec = 0;
   }
      
 uint8_t b_st = 0, b_en = 0;     
    if (startpoint < KolZapArcAll[PER]-1) 
    { if (ReadDataW25_IT(PER, run, startpoint  ,(uint8_t*)&MEMDATA)==0)        
     {  if (Mem_TestReqTimeStart(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)) 
       {
         STARTENDTIM->STARTTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->STARTTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->STARTTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->STARTTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->STARTTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->STARTTIME.year = MEMDATA.STARTTIME.year;
         b_st = 1;  
       } 
     }
    }     
      else
     { SaveMessagePrefix_2(BufTrans,9,0x96);  //������ �p�䨪�
       BufTrans[4] = 3 & num;      //����p ��⪨
       BufTrans[5] = 0;            //���-�� ����ᥩ � �⢥�
       BufTrans[6] = 0;            //�p��������� ���
       goto CT;
     }
 
    endpoint = pArchiv.di2p[run];
    if (ReadDataW25_IT(PER, run, endpoint  ,(uint8_t*)&MEMDATA)==0)        
    {   if (Mem_TestReqTimeEndp(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)==0) 
       {
         STARTENDTIM->ENDTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->ENDTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->ENDTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->ENDTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->ENDTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->ENDTIME.year = MEMDATA.STARTTIME.year;
         b_en = 1;  
       }
   }       
// �������� ����� ������������� ������� STARTENDTIM
        if (Mem_TestReqTimeSEp(STARTENDTIM)) //����� ������ ������ ������� �����
         err |= 0x80;                   // ����p��� ᮮ⭮襭�� �p���� ���p��

       if (b_st==0)
       startpoint = Mem_SearchStart(PER, run, STARTENDTIM);      
        if (startpoint != 0xFFFF)
        { if (Mem_TestReqTimeEqp(STARTENDTIM) == 0)
            endpoint = startpoint;
          else
          { if (b_en==0)
             endpoint = Mem_SearchEnd(PER,run, STARTENDTIM);
          }   
        } 
        else err = 0x22;

      if (err == 0) {      // ��p���������� �����

         if (portC==2) { startpoint2 = startpoint;
                         endpoint2 = endpoint;
                       }
         else          { startpoint1 = startpoint;
                         endpoint1 = endpoint;
                       }
         if (pArchiv.di1p[run] < pArchiv.di2p[run])
         {  if (((startpoint < pArchiv.di1p[run]) || (endpoint > pArchiv.di2p[run]))) 
             err |= 0x40;
         }
         else 
         { if (((startpoint < pArchiv.di1p[run]) && (startpoint > pArchiv.di2p[run]))
             || ((endpoint > pArchiv.di2p[run]) && (endpoint < pArchiv.di1p[run]))
             || ((startpoint==0) && (endpoint == KolZapArcAll[PER]-1)))
             err |= 0x40;
         }
         if ((startpoint - endpoint) == 1)
             err |= 0x40;
      }
   }               //         ����� ��p������ ��p���� ���p���
   if (err) {
      if (err & 0xC0) {       // ��� ������� � ���p��������� �p������ - �� 1-�� ���p��
         SaveMessagePrefix_2(BufTrans,9,nfc+128);      //0x95 ������ ��p����p�� ���p���
         BufTrans[4] = 3 & num;      
         BufTrans[5] = 0;             // ���-�� ������� � ������
         BufTrans[6] = 0;                // �p��������� ���
      }
      else
         SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   }
   else {                            // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
         if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
         else         { startpoint = startpoint1; endpoint = endpoint1;}
                                     // ��p�����᪨� ����
      point = Mem_NextIndex(PER, startpoint,Nrec*STARTENDTIM->RequestNum,startpoint ,endpoint);
      count = 0;
      if (point != 0xFFFF) {             // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<Nrec;i++) {
            count++;                 // �p������� ��p�������
            if (i) {
               point = Mem_NextIndex(PER, point,1,startpoint ,endpoint);
               if (point == 0xFFFF)
                  break;             // ����� ���᪠
            }                        // �⥭�� ����� ��p�����᪮�� ����
            ReadDataW25_IT(PER, run, point  ,(uint8_t*)&MEMDATA);
            pbtime = &MEMDATA.STARTTIME;

            pprec->StartMonth   = pbtime->month;     //�����
            pprec->StartDay     = pbtime->date;      //����
            pprec->StartYear    = pbtime->year;      //���
            pprec->StartHours   = pbtime->hours;     //��
            pprec->StartMinutes = pbtime->minutes;   //�����
//printf("point=%d h=%d m=%d\n", point,pbtime->hours,pbtime->minutes);
union {
      unsigned char Bt[4];
      float Val;
   } Vl;

       bCount = (CONFIG_IDRUN[num-1].TypeRun == 4 || CONFIG_IDRUN[num-1].TypeRun == 5
            || CONFIG_IDRUN[num-1].TypeRun == 10);
       Vl.Val=MEMDATA.dp_Qr;
       w=Vl.Bt[0]&0x1;
            if  (bCount)    // ᯮᮡ ����p���� �� ��⪥ ���稪

             {  pprec->StartDay |= 0x80;
                pprec->DP_V = MEMDATA.dp_Qr;  // ��ꥬ �� ࠡ��� �᫮����, �3 (��� ���稪�)
             }
             else
             { if (CONFIG_IDRUN[num-1].Vlag == 1)
               {   pprec->DP_V = MEMDATA.dp_Qr;
                  Vl.Val=pprec->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pprec->DP_V =  Vl.Val;
               }
               else
               { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
                   pprec->DP_V = MEMDATA.dp_Qr;
                 else
                  pprec->DP_V = MEMDATA.dp_Qr*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];// �।��� ��९�� ��������, ���/�2 ���     |
                  Vl.Val=pprec->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  pprec->DP_V =  Vl.Val;
               }
            }                         // ��p��� ������� � ������
            pprec->Period = MEMDATA.count;
            pprec->V    = MEMDATA.q; // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
               Koef = 1.0;
              else
               Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
            pprec->En   = MEMDATA.FullHeat*Koef/1.0e3;
            Vl.Val=MEMDATA.p;
            w=Vl.Bt[0]&0x1;
           if (GLCONFIG.EDIZM_T.EdIzP == 0)
            pprec->P    = MEMDATA.p;
           else
            pprec->P    = MEMDATA.p*KoefP[GLCONFIG.EDIZM_T.EdIzP]; // �।��� ��������, ���/�2
            Vl.Val=pprec->P;
            Vl.Bt[0]=Vl.Bt[0]&0xFE;
            Vl.Bt[0]=Vl.Bt[0] | w;
            pprec->P =  Vl.Val;
            pprec->t    = MEMDATA.t; // �।��� ⥬������, �ࠤ. ������

          if (RH==0)
          { memmove(pprecd,pprec,sizepp);
           pprecd->Ro   = MEMDATA.RO;     // �।��� ���⭮���
            Vl.Val=MEMDATA.Heat;
            w=Vl.Bt[0]&0x1;
            pprecd->UTS  = MEMDATA.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];   // 㤥�쭠� ⥯��� ᣮ࠭�� ���/�3
                Vl.Val=pprecd->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pprecd->UTS =  Vl.Val;
          }
            pprec++;
            pprecd++;
            if (point == endpoint)
               break;                // ����� ���᪠
         }
      }
                                     // ���-�� ����ᥩ
    if (count > Nrec)  count = 0;  
    RESPONSEPERIOD.NumRecord = count;
      RESPONSEPERIODDEN.NumRecord = count;

      if ((point == endpoint) || (count==0) || (point > KolZapArcAll[PER]-1))
       {                              // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         RESPONSEPERIOD.ResponseStatus = 0;
         RESPONSEPERIODDEN.ResponseStatus = 0;
      }
      else                           // ����� �⢥� (0= �����襭�, 1= ���� �� �����)0
       {  RESPONSEPERIOD.ResponseStatus = 1;
          RESPONSEPERIODDEN.ResponseStatus = 1;
       }
      if (RH) sizeppd=sizepp;
                                     // ������ �p�䨪�
         SaveMessagePrefix_2(BufTrans,count*(sizeppd-2)  + 9,nfc+128);        // �訡�� ��p����p�� ���p��
uint8_t k = 7;                                     // ������ ���p�
      if (RH)
      { memmove(&BufTrans[5],&RESPONSEPERIOD,count*sizepp+9);
          for (i=9; i< count*sizepp+9+2; i++)
          { 
             switch (i)
             {    case 15: case  16: case 43: case 44 : case 71:case 72: 
                  case 99: case 100:  case 127: case 128: case 155: case 156:
                  case 183: case 184: case 211: case 212: case 239: case 240: 
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
         }         
          BufTrans[4] = 3 & num;            // ����p ��⪨
      }
     else
      {
        memmove(&BufTrans[5],&RESPONSEPERIODDEN,count*sizeppd+9);
          for (i=9; i< count*sizeppd+9+2; i++)
          { 
             switch (i)
             {    case 15: case  16: case 43: case 44 : case 71:case 72: 
                  case 99: case 100:  case 127: case 128: case 155: case 156:
                  case 183: case 184: case 211: case 212: case 239: case 240: 
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
          }   
          BufTrans[4] = (3 & num) | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
      }
   }
CT:  i=0;
}
//---------------------------------------------------------------------------
void RdAudit()  //�⥭�� ��p���� ����⥫���
{
portC=2;
   RdAuditp(BufInKadr1,BufTrans1,&STARTENDTIME);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}
void RdAuditp(unsigned char* BufInKadr,unsigned char*  BufTrans,  struct startendtime *STARTENDTIM)
{
   static  unsigned int   startpoint2,endpoint2;
   static  unsigned int   startpoint1,endpoint1;
  static unsigned int  startpoint,endpoint;
  struct bintime      *pbtime;
  struct reqaudit     *preq;  //��p���p� ���p��
  unsigned char       num,run;
  //unsigned char       page;
  struct auditrecord  *prec;  //��p���p� ������ ��� ��p����
  unsigned int        point,Nrec;
  int                 i,count,sizepau,sizepaua;
  struct responseaudit   RESPONSEAUDIT;
  int tInd,tPoint;

  int err = 0;
   count = 0;
   point = 0;
  Nrec=15;
  sizepau = sizeof(struct reqaudit);
  sizepaua = sizeof(struct auditrecord);
  if(BufInKadr[2] - sizepau != 7) err |= 1;//����p��� ����� ���p��

  num = (3 & BufInKadr[4]);
  if (num > GLCONFIG.NumRun) err |= 2;//����p��� ����p� ��⪨
  run = num-1;
  preq = &RESIV.REQAUDIT;
  prec = &RESPONSEAUDIT.AUDITRECORD[0];
  memmove(preq,&BufInKadr[5],sizepau);//��p������� ��p����p�� ���p��
  if (preq->RequestNum != 0) // && (STARTENDTIME.RequestNum != 0xFF))
  { // ��������� ������� ����� �� ��������� ��������� ������
    if( STARTENDTIME.RequestNum == preq->RequestNum)  // {;}
     return;
    else
     if( STARTENDTIM->RequestNum +1 != preq->RequestNum)  //����� �����
    err |= 0x04;   // ����p��� ������������������ ���p����
  }
  STARTENDTIM->RequestNum = preq->RequestNum;  // ����� �������
  STARTENDTIM->num = run;              // ����� �����

  if(STARTENDTIM->RequestNum == 0)  // ��p������ 1-�� ���p���
  { pbtime = &(STARTENDTIM->STARTTIME);
    pbtime->seconds = preq->StartSeconds;
    pbtime->minutes = preq->StartMinutes;
    pbtime->hours   = preq->StartHours;
    pbtime->date    = preq->StartDay;
    pbtime->month   = preq->StartMonth;
    pbtime->year    = preq->StartYear;
    if( Mem_TestReqTime(pbtime) ) err |= 8;//����p��� �p����� ���p��
    pbtime = &(STARTENDTIM->ENDTIME);
    pbtime->seconds = preq->EndSeconds;
    pbtime->minutes = preq->EndMinutes;
    pbtime->hours   = preq->EndHours;
    pbtime->date    = preq->EndDay;
    pbtime->month   = preq->EndMonth;
    pbtime->year    = preq->EndYear;
    if( Mem_TestReqTime(pbtime) ) err |= 0x10; // ����p��� �p����� ���p���
    if( Mem_TestReqTimeSEp(STARTENDTIM) ) err |= 0x20;// ����p��� ����������� �p���� ���p���
  // �������� ������ �� 1 �� 0x24 - ������ ���p���
// STARTENDTIM - ������������ 
       startpoint = pArchiv.di1Au[run];
//      for (uint16_t is=0; is<20; is=is+1)
//   {    ReadDataW25_IT(AUD, run, is  ,(uint8_t*)&MEMAUDIT);
//       uint8_t sec = MEMAUDIT.TIME.hours;
//       sec = 0;
//}
uint8_t b_st = 0, b_en = 0;     
      if (startpoint < KolZapArcAll[AUD]-1) 
      { if (Mem_AuditRd (run, startpoint, (uint8_t*)&MEMAUDIT) == 0)
        {  if (Mem_TestReqTimeStart(STARTENDTIM,(uint8_t*)&MEMAUDIT.TIME)) 
         {
           STARTENDTIM->STARTTIME.date = MEMAUDIT.TIME.date;
           STARTENDTIM->STARTTIME.hours = MEMAUDIT.TIME.hours;
           STARTENDTIM->STARTTIME.minutes = MEMAUDIT.TIME.minutes;
           STARTENDTIM->STARTTIME.month = MEMAUDIT.TIME.month;         
           STARTENDTIM->STARTTIME.seconds = MEMAUDIT.TIME.seconds;
           STARTENDTIM->STARTTIME.year = MEMAUDIT.TIME.year;
           b_st = 1;  
         }
        }         
      }  
      else
     { SaveMessagePrefix_2(BufTrans,9,0x96);  //������ �p�䨪�
       BufTrans[4] = 3 & num;      //����p ��⪨
       BufTrans[5] = 0;            //���-�� ����ᥩ � �⢥�
       BufTrans[6] = 0;            //�p��������� ���
       goto CT;
     }
    
       endpoint = pArchiv.di2Au[run];
    if (Mem_AuditRd (run, endpoint  ,(uint8_t*)&MEMAUDIT)==0)        
    {   if (Mem_TestReqTimeEndp(STARTENDTIM,(uint8_t*)&MEMAUDIT.TIME)==0) 
       {
         STARTENDTIM->ENDTIME.date = MEMAUDIT.TIME.date;
         STARTENDTIM->ENDTIME.hours = MEMAUDIT.TIME.hours;
         STARTENDTIM->ENDTIME.minutes = MEMAUDIT.TIME.minutes;
         STARTENDTIM->ENDTIME.month = MEMAUDIT.TIME.month;         
         STARTENDTIM->ENDTIME.seconds = MEMAUDIT.TIME.seconds;
         STARTENDTIM->ENDTIME.year = MEMAUDIT.TIME.year;
         b_en = 1;  
       }
   }       
// �������� ����� ������������� ������� STARTENDTIM
        if (Mem_TestReqTimeSEp(STARTENDTIM)) //����� ������ ������ ������� �����
         err |= 0x80;                   // ����p��� ᮮ⭮襭�� �p���� ���p��

       if (b_st==0)
       startpoint = Mem_SearchStart(AUD, run, STARTENDTIM);      
        if (startpoint != 0xFFFF)
//        {// if (Mem_TestReqTimeEqp(STARTENDTIM) == 0) && (
         //   endpoint = startpoint;
         // else
          { if (b_en==0)
             endpoint = Mem_SearchEnd(AUD,run, STARTENDTIM);
          }   
//        } 
        else err = 0x22;
      if (err == 0) {     

         if (portC==2) { startpoint2 = startpoint;
                         endpoint2 = endpoint;
                       }
         else          { startpoint1 = startpoint;
                         endpoint1 = endpoint;
                       }
         if (pArchiv.di1Au[run] < pArchiv.di2Au[run])
         {  if (((startpoint < pArchiv.di1Au[run]) || (endpoint > pArchiv.di2Au[run]))) 
             err |= 0x40;
         }
         else 
         { if (((startpoint < pArchiv.di1Au[run]) && (startpoint > pArchiv.di2Au[run]))
             || ((endpoint > pArchiv.di2Au[run]) && (endpoint < pArchiv.di1Au[run]))
             || ((startpoint==0) && (endpoint == KolZapArcAll[AUD]-1)))
             err |= 0x40;
         }
         if ((startpoint - endpoint) == 1)
             err |= 0x40;
      }
   }               //         ����� ��p������ ��p���� ���p���

  if ( err )
  {
  if( err & 0xC0 ) //��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
    { SaveMessagePrefix_2(BufTrans,9,0x96);  //������ �p�䨪�
      BufTrans[4] = 3 & num;      //����p ��⪨
      BufTrans[5] = 0;            //���-�� ����ᥩ � �⢥�
      BufTrans[6] = 0;            //�p��������� ���
    }
    else  SaveMessagePrefix_2(BufTrans,6,0xFF);  //�訡�� ��p����p�� ���p��
  }
  else  //�訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
  {
   if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
   else         { startpoint = startpoint1; endpoint = endpoint1;}
                                     // ��p�����᪨� ����
      point = Mem_NextIndex(AUD, startpoint,Nrec*STARTENDTIM->RequestNum,startpoint ,endpoint);
    count = 0;
      if (point != 0xFFFF) {             // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<Nrec;i++) {
            count++;                 // �p������� ��p�������
            if (i) {
               point = Mem_NextIndex(AUD, point,1,startpoint ,endpoint);
               if (point == 0xFFFF)
                  break;             // ����� ���᪠
            }                        // �⥭�� ����� ��p�����᪮�� ����
        Mem_AuditRd (run, point  ,(uint8_t*)&MEMAUDIT);
//            ReadDataW25_IT(AUD, run, point  ,(uint8_t*)&MEMAUDIT);
        pbtime = &MEMAUDIT.TIME;
        prec->StartMonth   = pbtime->month;     //�����
        prec->StartDay     = pbtime->date;      //����
        prec->StartYear    = pbtime->year;      //���
        prec->StartHours   = pbtime->hours;     //��
        prec->StartMinutes = pbtime->minutes;   //�����
        prec->StartSeconds = pbtime->seconds;   //ᥪ㭤�
        prec->ParemeterNum = MEMAUDIT.type;     //����p ��p����p�
        prec->RunNum       = STARTENDTIM->num+1;//����p ��⪨
        prec->OldValue     = MEMAUDIT.altvalue; //�p����饥 ���祭��
        prec->NewValue     = MEMAUDIT.newvalue; //����� ���祭��
        prec++;
        if(point == endpoint) break;  //����� ���᪠
      }
    }
        if (count > Nrec) count = 0;
        RESPONSEAUDIT.NumRecord = count;   //���-�� ����ᥩ
        if((point==endpoint)||(count==0)||(point>(KolZapArcAll[AUD]-1)))
         RESPONSEAUDIT.ResponseStatus = 0;//�����襭�
        else RESPONSEAUDIT.ResponseStatus = 1;  //���� �� �����
//uint8_t k = 7;                                     // ������ ���p�
      memmove(&BufTrans[5],&RESPONSEAUDIT,2);
      memmove(&BufTrans[7],&RESPONSEAUDIT.AUDITRECORD,count*sizepaua); //������ ���p�
      SaveMessagePrefix_2(BufTrans,count*sizepaua+9,0x96);//������ �p�䨪�
      BufTrans[4] = 3 & num;                //����p ��⪨
  }
  CT: i=0;  
}
//---------------------------------------------------------------------------

void RdAlarm()  //�⥭�� ��p���� ���p��
{
  portC=2;
  RdAlarmp(BufInKadr1,BufTrans1,&STARTENDTIME);
  CheckBufTrans(BufTrans1); //�������� ��
  Ini_Send1();  // �������� BufTrans
}
void RdAlarmp(unsigned char* BufInKadr,unsigned char*  BufTrans,  struct startendtime *STARTENDTIM)
{
  static  unsigned int   startpoint2,endpoint2;
  static  unsigned int   startpoint1,endpoint1;
  static unsigned int startpoint,endpoint;
  struct bintime      *pbtime;
  struct reqalarm     *preq;  //��p����p� ���p���
  unsigned char       num, run;
  unsigned char       page;
  struct alarmrecord  *prec;  //��p����p� ������ ��� ��p�����
  unsigned int        point,Nrec;
  int                 count,sizepal,sizepala;
  struct responsealarm   RESPONSEALARM;
  uint16_t err = 0;
   count = 0;
   point = 0;

  Nrec=18;
  sizepal = sizeof(struct reqalarm);
  sizepala = sizeof(struct alarmrecord);

  if(BufInKadr[2] - sizepal != 7) err |= 1;//����p��� ����� ���p���
  num = 3 & BufInKadr[4];
 if (num > GLCONFIG.NumRun) err |= 2;//����p��� ����p� �����
  run = num-1;
  preq = &RESIV.REQALARM;
  prec = &RESPONSEALARM.ALARMRECORD[0];
  memmove(preq,&BufInKadr[5],sizeof(struct reqalarm));//��p������� ��p����p�� ���p���
  if (preq->RequestNum != 0) //  && (STARTENDTIME.RequestNum != 0xFF))
  {// �������� ������� ����� �� ������v� �������� ������
    if( STARTENDTIME.RequestNum == preq->RequestNum)  // {;}
     return;
    else
    if( STARTENDTIM->RequestNum +1 != preq->RequestNum)  //����� �������
    err |= 0x04;   // ����p��� ����������������� ���p����
  }
  STARTENDTIM->RequestNum = preq->RequestNum;  // ����� �������
  STARTENDTIM->num = run;              // ����� �����

  if(STARTENDTIM->RequestNum == 0)  // ��p������ 1-�� ���p���
  { pbtime = &(STARTENDTIM->STARTTIME);
    pbtime->seconds = preq->StartSeconds;
    pbtime->minutes = preq->StartMinutes;
    pbtime->hours   = preq->StartHours;
    pbtime->date    = preq->StartDay;
    pbtime->month   = preq->StartMonth;
    pbtime->year    = preq->StartYear;
    if( Mem_TestReqTime(pbtime) ) err |= 8;//����p��� �p����� ���p���
    pbtime = &(STARTENDTIM->ENDTIME);
    pbtime->seconds = preq->EndSeconds;
    pbtime->minutes = preq->EndMinutes;
    pbtime->hours   = preq->EndHours;
    pbtime->date    = preq->EndDay;
    pbtime->month   = preq->EndMonth;
    pbtime->year    = preq->EndYear;
    if( Mem_TestReqTime(pbtime) ) err |= 0x10; // ����p��� �p����� ���p���
    if( Mem_TestReqTimeSEp(STARTENDTIM) ) err |= 0x20;// ����p��� ����������� �p���� ���p����
  // �������� ������ �� 1 �� 0x24 - ������ ���p���
// STARTENDTIM - ���������� 
       startpoint = pArchiv.di1Al[run];
      for (uint16_t is=0; is<20; is=is+1)
   {    ReadDataW25_IT(ALR, run, is  ,(uint8_t*)&MEMALARM);
       uint8_t sec = MEMALARM.TIME.hours;
       sec = 0;
   }
uint8_t b_st = 0, b_en = 0;     
   
      if (startpoint < KolZapArcAll[ALR]-1) 
      { if (Mem_AlarmRd (run, startpoint, (uint8_t*)&MEMALARM) == 0)
        {  if (Mem_TestReqTimeStart(STARTENDTIM,(uint8_t *)&MEMALARM.TIME)) 
         {
           STARTENDTIM->STARTTIME.date = MEMALARM.TIME.date;
           STARTENDTIM->STARTTIME.hours = MEMALARM.TIME.hours;
           STARTENDTIM->STARTTIME.minutes = MEMALARM.TIME.minutes;
           STARTENDTIM->STARTTIME.month = MEMALARM.TIME.month;         
           STARTENDTIM->STARTTIME.seconds = MEMALARM.TIME.seconds;
           STARTENDTIM->STARTTIME.year = MEMALARM.TIME.year;
           b_st = 1;  
         }
        }         
      }  
      else
     { SaveMessagePrefix_2(BufTrans,9,0x97);  //������ �p������
       BufTrans[4] = 3 & num;      //����p �����
       BufTrans[5] = 0;            //���-�� ������� � ������
       BufTrans[6] = 0;            //�p��������� ���
       goto CT;
     }
    
       endpoint = pArchiv.di2Al[run];
    if (Mem_AlarmRd (run, endpoint  ,(uint8_t*)&MEMALARM)==0)        
    {   if (Mem_TestReqTimeEndp(STARTENDTIM,(uint8_t*)&MEMALARM.TIME)==0) 
       {
         STARTENDTIM->ENDTIME.date = MEMALARM.TIME.date;
         STARTENDTIM->ENDTIME.hours = MEMALARM.TIME.hours;
         STARTENDTIM->ENDTIME.minutes = MEMALARM.TIME.minutes;
         STARTENDTIM->ENDTIME.month = MEMALARM.TIME.month;         
         STARTENDTIM->ENDTIME.seconds = MEMALARM.TIME.seconds;
         STARTENDTIM->ENDTIME.year = MEMALARM.TIME.year;
         b_en = 1;  
       }
   }       
// �������� ����� ������������� ������� STARTENDTIM
        if (Mem_TestReqTimeSEp(STARTENDTIM)) //T���� ����� ������ ������ �����
         err |= 0x80;                   // ����p��� ����������� �p���� ���p���

       if (b_st==0)
       startpoint = Mem_SearchStart(ALR, run, STARTENDTIM);      
        if (startpoint != 0xFFFF)
//        {// if (Mem_TestReqTimeEqp(STARTENDTIM) == 0) && (
         //   endpoint = startpoint;
         // else
          { if (b_en==0)
             endpoint = Mem_SearchEnd(ALR,run, STARTENDTIM);
          }   
//        } 
        else err = 0x22;
      if (err == 0) {     

         if (portC==2) { startpoint2 = startpoint;
                         endpoint2 = endpoint;
                       }
         else          { startpoint1 = startpoint;
                         endpoint1 = endpoint;
                       }
         if (pArchiv.di1Al[run] < pArchiv.di2Al[run])
         {  if (((startpoint < pArchiv.di1Al[run]) || (endpoint > pArchiv.di2Al[run]))) 
             err |= 0x40;
         }
         else 
         { if (((startpoint < pArchiv.di1Al[run]) && (startpoint > pArchiv.di2Al[run]))
             || ((endpoint > pArchiv.di2Al[run]) && (endpoint < pArchiv.di1Al[run]))
             || ((startpoint==0) && (endpoint == KolZapArcAll[ALR]-1)))
             err |= 0x40;
         }
         if ((startpoint - endpoint) == 1)
             err |= 0x40;
      }
   }               //         ����� ��p������ ��p���� ���p���

  if ( err )
  {
  if( err & 0xC0 ) //��� ������� � ���p��������� �p������ - �� 1-�� ���p���
    { SaveMessagePrefix_2(BufTrans,9,0x97);  //������ �p������
      BufTrans[4] = 3 & num;      //����p �����
      BufTrans[5] = 0;            //���-�� ������� � ������
      BufTrans[6] = 0;            //�p��������� ���
    }
    else  SaveMessagePrefix_2(BufTrans,6,0xFF);  //������ ��p����p�� ���p���
  }
  else  //������ � ���p���� ��� - ��p������ ���� ���p����
  {
    if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
    else         { startpoint = startpoint1; endpoint = endpoint1;}
                                     // ��p���������� �����
      point = Mem_NextIndex(ALR, startpoint,Nrec*STARTENDTIM->RequestNum,startpoint ,endpoint);
    count = 0;
      if (point != 0xFFFF) {             // ���� ������ � ����� ���������
         for (int i=0;i<Nrec;i++) {
            count++;                 // �p������� ��p�������
            if (i) {
               point = Mem_NextIndex(ALR, point,1,startpoint ,endpoint);
               if (point == 0xFFFF)
                  break;             // ����� ������
            }                        // ������ ������ ��p����������� ������
        Mem_AlarmRd (run, point  ,(uint8_t*)&MEMALARM);
//            ReadDataW25_IT(ALR, run, point  ,(uint8_t*)&MEMALARM);
        pbtime = &MEMALARM.TIME;
        prec->StartMonth   = pbtime->month;     //�����
        prec->StartDay     = pbtime->date;      //����
        prec->StartYear    = pbtime->year;      //���
        prec->StartHours   = pbtime->hours;     //���
        prec->StartMinutes = pbtime->minutes;   //������
        prec->StartSeconds = pbtime->seconds;   //�������

        prec->alarmcode = (unsigned char)MEMALARM.type; //��� ���p��
        prec->pointnum  = (unsigned char)(MEMALARM.type/256);//�������p���� ��������

        prec->runnum    = STARTENDTIM->num+1;//����p �����
        prec->value     = MEMALARM.value;    //�p�������� ��������
        prec++;
        if(point == endpoint) break;  //����� ������
      }
    }
      uint16_t n,k=15;
      if (count > Nrec) count = 0;
      RESPONSEALARM.NumRecord = count; //���-�� �������
      if((point == endpoint)||(count==0)||(point > (ARCAL-1)))
       RESPONSEALARM.ResponseStatus = 0;  //������ ������ (0= ���������, 1= ���� ��� ������)
      else RESPONSEALARM.ResponseStatus = 1;  //������ ������ (0= ���������, 1= ���� ��� ������)
       memmove(&BufTrans[5],&RESPONSEALARM,2);
      memmove(&BufTrans[7],&RESPONSEALARM.ALARMRECORD,count*(sizepala)); //������ ����p� 
      k=16;
      uint8_t n2=16,n3=17,n4=18; 
          for (n=n2; n< count*(sizepala)+7; n++)
          {               
           if (
               (n == n2+((n-n2) / 16)*16) || 
               (n == n3+((n-n3) / 16)*16)||
               (n == n4+((n-n4) / 16)*16))   
                continue;
           else
               { BufTrans[k] = BufTrans[n]; k++;}
          }   

      SaveMessagePrefix_2(BufTrans,count*(sizepala-3)+9,0x97);//������ �p������
      BufTrans[4] = 3 & num;                //����p �����
  }
  CT:  count = 0;
}

//---------------------------------------------------------------------------
void Comm_RdCykl() {                    // �⥭�� 横���᪨� ������
  portC=2;
  Comm_RdCyklp(BufInKadr1,BufTrans1,&STARTENDTIME);
  CheckBufTrans(BufTrans1); //�������� ��
  Ini_Send1();  // �������� BufTrans
}
//-------------------------------------------------------
void Comm_RdCyklp(unsigned char* BufInKadr,unsigned char*  BufTrans,  struct startendtime *STARTENDTIM) {                    // �⥭�� 横���᪨� ������
   static  unsigned int   startpoint2,endpoint2;
   static  unsigned int   startpoint1,endpoint1;
   static unsigned int   startpoint,endpoint;
   struct bintime        *pbintime;
   unsigned char         num,run;
//   unsigned char         page;
   unsigned int          point;
   int                   err;
   struct reqcykl        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������
   struct cyklrecord  *pcr;//��p���p� ����� ������ ��� ��p����
   struct cyklrecordden  *pcrd;//��p���p� ����� ������ ��� ��p����
   int                   count,  nfc,Nrec;
   int lstruct_cykl,sizepc;
   union  resiv  RESIV;
   struct responsecyklden   RESPONSECYKLDEN;
   struct responsecykl   RESPONSECYKL;  // �⥭�� 横���᪨� ������
   char RH;
   uint16_t i,ir;

   RH= (INTPAR.Com2Mode==0);// && (INTPAR.HeatReport==0));
   if (RH)
           Nrec = 9;
   else    Nrec = 7;
   err = 0;
   count = 0;
   point = 0;

   sizepc = sizeof(struct cyklrecord);
   if (BufInKadr[2]-sizeof(struct reqcykl) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4];
   nfc = BufInKadr[3];
   run = num-1;
   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨
   pcr = &RESPONSECYKL.CYKLRECORD[0];
   pcrd = &RESPONSECYKLDEN.CYKLRECORDDEN[0];
   prc = &RESIV.REQCYKL;
                                        // ��p������� ��p����p�� ���p��
   memmove(prc,&BufInKadr[5],sizeof(struct reqcykl));
   if (prc->RequestNum != 0)
   {
                                           // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIM->RequestNum == prc->RequestNum)
         return;
      if (STARTENDTIM->RequestNum +1 != prc->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIM->RequestNum = prc->RequestNum;
   STARTENDTIM->num = 3 & run; // ����� ��⪨

   if (STARTENDTIM->RequestNum == 0)    // ��p���⪠ 1-�� ���p��
   {   // ��p���⪠ 1-�� ���p��
//printf("1-� RecS=%2d Tmp=%2d: %2d: %2d Start=%2d: %2d: %2d\n",prc->RequestNum,prc->StartHours,prc->StartMinutes,prc->StartSeconds,STARTENDTIME.STARTTIME.hours,STARTENDTIME.STARTTIME.minutes,STARTENDTIME.STARTTIME.seconds);
//      CopyKoef();
      pbintime = &STARTENDTIM->STARTTIME;
      pbintime->seconds = prc->StartSeconds;
      pbintime->minutes = prc->StartMinutes;
      pbintime->hours   = prc->StartHours;
      pbintime->date    = prc->StartDay;
      pbintime->month   = prc->StartMonth;
      pbintime->year    = prc->StartYear;

      if (Mem_TestReqTime(pbintime))
         err |= 8;                       // ����p��� �p����� ���p��

 //     STARTENDTIM->RequestNum = prc->RequestNum;
      pbintime = &STARTENDTIM->ENDTIME;

      pbintime->seconds = prc->EndSeconds;
      pbintime->minutes = prc->EndMinutes;
      pbintime->hours   = prc->EndHours;
      pbintime->date    = prc->EndDay;
      pbintime->month   = prc->EndMonth;
      pbintime->year    = prc->EndYear;

      if (Mem_TestReqTime(pbintime))
          err |= 0x10;                   // ����: ������� �����
      if (Mem_TestReqTimeSEp(STARTENDTIM)) //����� ������ ������ ������� �����
         err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��
                                        // ��p���� 横���᪮�� ����
// STARTENDTIM - ������������ 
       startpoint = pArchiv.di1s[run];

//      for (uint16_t is=0; is<1500; is=is+10)
//   {    Mem_SecDayRd (SEC,run, is); 
//       uint8_t sec = MEMDATA.STARTTIME.hours;
//       sec = 0;
//   }
 uint8_t b_st = 0, b_en = 0;     
    if (startpoint < KolZapArcAll[SEC]-1)        
    { 
      Mem_SecDayRd (SEC,run, startpoint); 
       if (Mem_TestReqTimeStart(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)) 
       {
         STARTENDTIM->STARTTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->STARTTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->STARTTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->STARTTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->STARTTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->STARTTIME.year = MEMDATA.STARTTIME.year;
         b_st = 1;  
       } 
    }     
      else
     { SaveMessagePrefix_2(BufTrans,9,0x96);  //������ �p�䨪�
       BufTrans[4] = 3 & num;      //����p ��⪨
       BufTrans[5] = 0;            //���-�� ����ᥩ � �⢥�
       BufTrans[6] = 0;            //�p��������� ���
       goto CT;
     }
       
       endpoint = pArchiv.di2s[run];
       Mem_SecDayRd (SEC,run, endpoint); //endpoint  
       if (Mem_TestReqTimeEndp(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)==0) 
       {
         STARTENDTIM->ENDTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->ENDTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->ENDTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->ENDTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->ENDTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->ENDTIME.year = MEMDATA.STARTTIME.year;
         b_en = 1;  
       }   
// �������� ����� ������������� ������� STARTENDTIM
        if (Mem_TestReqTimeSEp(STARTENDTIM)) //����� ������ ������ ������� �����
         err |= 0x80;                   // ����p��� ᮮ⭮襭�� �p���� ���p��

       if (b_st==0)
       startpoint = Mem_SearchStart(SEC,run, STARTENDTIM);
        if (startpoint != 0xFFFF)
        { if (Mem_TestReqTimeEqp(STARTENDTIM) == 0)
            endpoint = startpoint;
          else
          { if (b_en==0)
             endpoint = Mem_SearchEnd(SEC,run, STARTENDTIM);
          }   
        } 
        else err = 0x22;
        // ��砫�� ������
        if (err == 0) {                   // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
                                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (portC==2) { startpoint2 = startpoint;
                         endpoint2 = endpoint;
                       }
         else          { startpoint1 = startpoint;
                         endpoint1 = endpoint;
                       }
         if (pArchiv.di1s[run] < pArchiv.di2s[run])
         {  if (((startpoint < pArchiv.di1s[run]) || (endpoint > pArchiv.di2s[run]))) 
             err |= 0x40;
         }
         else 
         { if (((startpoint < pArchiv.di1s[run]) && (startpoint > pArchiv.di2s[run]))
             || ((endpoint > pArchiv.di2s[run]) && (endpoint < pArchiv.di1s[run]))
             || ((startpoint==0) && (endpoint == KolZapArcAll[SEC]-1)))
             err |= 0x40;
         }
         if ((startpoint - endpoint) == 1)
             err |= 0x40;
         
      }   // ����� if (err==0)
   }                                  // ����� ��p���⪨ 1-�� ���p��
 //  STARTENDTIM->RequestNum = prc->RequestNum;
   if (err) {
      if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_2(BufTrans,9,nfc+128);        //0x98 �訡�� ��p����p�� ���p��
         BufTrans[4] = num;         // ����p ��⪨
         BufTrans[5] = 0;               // ���-�� ����ᥩ � �⢥�
         BufTrans[6] = 0;               // �p��������� ���
      }
      else
      {// printf("err=%02X ",err);
         SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
      }
   }
   else {  // ��� ������             // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
         if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
         else         { startpoint = startpoint1; endpoint = endpoint1;}
                                        // ��p���� 横���᪮�� ����

      point = Mem_NextIndex(SEC,startpoint,Nrec*STARTENDTIM->RequestNum,startpoint ,endpoint);
      count = 0;
      if (point != 0xFFFF) {
       for (int ir=0; ir<Nrec; ir++) 
       {
            count++;                    // �p������� ��p�������
            if (ir) {
               point = Mem_NextIndex(SEC,point,1,startpoint ,endpoint);
               if (point == 0xFFFF)
                  break;                // ����� ���᪠
            }
         // �⥭�� ����� 横���᪮�� ����
            Mem_SecDayRd (SEC,run, point);  
            pbintime = &MEMDATA.STARTTIME;
                                        // ���
            pcr->StartMonth   = pbintime->month;
            pcr->StartDay     = pbintime->date;
            pcr->StartYear    = pbintime->year;
            pcr->StartHours   = pbintime->hours;
            pcr->StartMinutes = pbintime->minutes;
            pcr->StartSeconds = pbintime->seconds;
      union {
      unsigned char Bt[4];
      float Val;
      }  Vl;
      char w;
       Vl.Val=MEMDATA.dp_Qr;
       w=Vl.Bt[0]&0x1;
            if ((CONFIG_IDRUN[run].TypeRun) == 4 || (CONFIG_IDRUN[run].TypeRun) == 5
             || (CONFIG_IDRUN[run].TypeRun) == 10)
            { pcr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
              pcr->DP_V   = MEMDATA.dp_Qr;   // ��ꥬ �3
            }
            else
            { if (CONFIG_IDRUN[run].Vlag == 1)
               {   pcr->DP_V = MEMDATA.dp_Qr;
                  Vl.Val=pcr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pcr->DP_V =  Vl.Val;
               }
               else
               { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
                   pcr->DP_V = MEMDATA.dp_Qr;
                 else
                  pcr->DP_V   = MEMDATA.dp_Qr*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   //  ��ꥬ �� �3
                 Vl.Val=pcr->DP_V;
                 Vl.Bt[0]=Vl.Bt[0]&0xFE;
                 Vl.Bt[0]=Vl.Bt[0] | w;
                 pcr->DP_V =  Vl.Val;
               }
            }
            pcr->Period = MEMDATA.count;// ��p��� ������� � ᥪ㭤��
            pcr->V      = MEMDATA.q;    // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
                Koef = 1.0;
              else
                Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
            pcr->En   = MEMDATA.FullHeat*Koef/1.0e3;
           Vl.Val=MEMDATA.p;
           w=Vl.Bt[0]&0x1;
           if (GLCONFIG.EDIZM_T.EdIzP == 0)
            pcr->P    = MEMDATA.p;
           else
            pcr->P      = MEMDATA.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
           Vl.Val=pcr->P;
           Vl.Bt[0]=Vl.Bt[0]&0xFE;
           Vl.Bt[0]=Vl.Bt[0] | w;
           pcr->P =  Vl.Val;
            pcr->t      = MEMDATA.t;    // �।��� ⥬������, �ࠤ. ������

          if (RH==0)
          { memmove(pcrd,pcr,sizepc);
           pcrd->Ro   = MEMDATA.RO;     // �।��� ���⭮���
            Vl.Val=MEMDATA.Heat;
            w=Vl.Bt[0]&0x1;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 1)
             pcrd->UTS  = MEMDATA.Heat;
            else
             pcrd->UTS  = MEMDATA.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];   // 㤥�쭠� ⥯��� ᣮ࠭�� ���/�3
                Vl.Val=pcrd->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pcrd->UTS =  Vl.Val;
          }
            pcr++;
            pcrd++;
            if (point == endpoint)
               break;                   // ����� ���᪠
         } //for (int i=0; i<Nrec;
      }  // if (point != 0xFFFF)
                                        // ���-�� ����ᥩ

      if (count > Nrec) count = 0;
      RESPONSECYKL.NumCyklRecord = count;

      RESPONSECYKLDEN.NumCyklRecord = count;

      if ((count==0) ||  (point == endpoint) || (point >  (KolZapArcAll[SEC]-1)))
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         { RESPONSECYKL.ResponseStatus = 0;
           RESPONSECYKLDEN.ResponseStatus = 0;
//           STARTENDTIM->RequestNum = 0xFF;
//fclose(fp);
         }
        else
        { RESPONSECYKL.ResponseStatus = 1;
          RESPONSECYKLDEN.ResponseStatus = 1;
        }
      lstruct_cykl = (RH) ? sizepc : sizeof(struct cyklrecordden);

                                        // ������ �p�䨪�
         SaveMessagePrefix_2(BufTrans,count*(lstruct_cykl-1)  + 9,nfc+128);        // �訡�� ��p����p�� ���p��
                                        // ������ ���p�
      uint8_t k = 7;
//      unsigned char  pc;  
      if (RH)  // �⢥� �� ᮤ�ন� ���⭮��� � �. ⥯����
      {    
         memmove(&BufTrans[5],&RESPONSECYKL,count*lstruct_cykl+4);
          for (i=9; i< count*lstruct_cykl+9; i++)
          { 
//             pc = (unsigned char *)(&RESPONSECYKL+i);
              
             switch (i)
             {    case  16: case 44: case 72: case 100: case 128: case 156:
                  case 184: case 212: case 240: 
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
         }   
      }
       else     // �⢥�  ᮤ�ন� ���⭮��� � �. ⥯����
      {
       memmove(&BufTrans[5],&RESPONSECYKLDEN,count*lstruct_cykl+2);
      }
      if (RH)
      BufTrans[4] = num;            // ����p ��⪨
      else
      BufTrans[4] = (num) | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
   }  // ����� else ��� ������
   CT:    count = 0;
}
//---------------------------------------------------------------------------

void RdHour() {                         // �⥭�� �ᮢ��� ����
  portC=2;
  RdHourp(BufInKadr1,BufTrans1,&STARTENDTIME);
  CheckBufTrans(BufTrans1); //�������� ��
  Ini_Send1();  // �������� BufTrans     
}
void RdHourp(unsigned char* BufInKadr,unsigned char*  BufTrans,  struct startendtime *STARTENDTIM) {    // �⥭�� �ᮢ�� ������
   static  unsigned int   startpoint2,endpoint2;
   static  unsigned int   startpoint1,endpoint1;
   static unsigned int  startpoint,endpoint;
   struct bintime      *pbtime;
   struct reqhour      *prh;            // ��p���p� ���p�� - �⥭�� �ᮢ�� ������
   unsigned char       num,run;
   unsigned char       page;
   struct hourrecord   *phr;            // ��p���p� ����� ������ ��� ��p����
   struct hourrecordden *phrd;
   struct hourrecordchr *phrch;           // ��p���p� ����� ������ ��� ��p����
   uint16_t i,ir;
   unsigned int        point;
   int                 err,sizephd,sizephch,nfc;
   int                 count,sizeph, Nrec;
    union  resiv  RESIV;
   struct responsehourden   RESPONSEHOURDEN;
   struct responsehour   RESPONSEHOUR;  // �⥭�� 横���᪨� ������
   struct responsehourchr   RESPONSEHOURCHR;
   char RH;
   union {
      unsigned char Bt[4];
      float Val;
   } Vl;
   char w;

   nfc = BufInKadr[3];
   RH= (INTPAR.Com2Mode==0);// && (INTPAR.HeatReport==0));
   if (nfc==0x13) Nrec = 9;
   else
    if (RH)
           Nrec = 9;
    else    Nrec = 7;
   err = 0;
   count = 0;
   point = 0;
   sizeph = sizeof(struct hourrecord);
   sizephd = sizeof(struct hourrecordden);
   sizephch = sizeof(struct hourrecordchr);
   if (BufInKadr[2]-sizeof(struct reqhour) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4];
   run = num-1;

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   phr = &RESPONSEHOUR.HOURRECORD[0];
   phrd = &RESPONSEHOURDEN.HOURRECORDDEN[0];
   phrch = &RESPONSEHOURCHR.HOURRECORDCHR[0];

   prh = &RESIV.REQHOUR;
                                        // ��p������� ��p����p�� ���p��
   memmove(prh,&BufInKadr[5],sizeof(struct reqhour));
   if ((prh->RequestNum != 0) &&  (STARTENDTIM->RequestNum != 0xFF))
   {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
     if (STARTENDTIM->RequestNum == prh->RequestNum)
        return;
     if (STARTENDTIM->RequestNum +1 != prh->RequestNum)
        err |= 0x04;                    // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIM->RequestNum = prh->RequestNum;
   STARTENDTIM->num = 3 & (num-1);      // ����� ��⪨
   if (STARTENDTIM->RequestNum == 0) {  // ��p���⪠ 1-�� ���p��
      pbtime = &STARTENDTIM->STARTTIME;
//     CopyKoef();
      pbtime->seconds = 0;
      pbtime->minutes = 0;
      pbtime->hours   = prh->StartHours;
      pbtime->date    = prh->StartDay;
      pbtime->month   = prh->StartMonth;
      pbtime->year    = prh->StartYear;

      if (Mem_TestReqTime(pbtime))
         err |= 8;                      // ����p��� �p����� ���p��

      pbtime = &STARTENDTIM->ENDTIME;

      pbtime->seconds = 59;
      pbtime->minutes = 59;
      pbtime->hours   = prh->EndHours;
      pbtime->date    = prh->EndDay;
      pbtime->month   = prh->EndMonth;
      pbtime->year    = prh->EndYear;

      if (Mem_TestReqTime(pbtime))
         err |= 0x10;                   // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSEp(STARTENDTIM))
         err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��
                                        // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
//      for (uint16_t is=50; is<65; is=is+1)
//   {    ReadDataW25_IT(HOU, run, is  ,(uint8_t*)&MEMDATA);
//       uint8_t sec = MEMDATA.STARTTIME.hours;
//       sec = 0;
//   }

      // STARTENDTIM - ������������ 
       startpoint = pArchiv.di1h[run];
uint8_t b_st = 0, b_en = 0;     
    if (startpoint < KolZapArcAll[HOU]-1)        
    { if (ReadDataW25_IT(HOU, run, startpoint,(uint8_t*)&MEMDATA)==0) 
     {  if (Mem_TestReqTimeStart(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)) 
       {
         STARTENDTIM->STARTTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->STARTTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->STARTTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->STARTTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->STARTTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->STARTTIME.year = MEMDATA.STARTTIME.year;
         b_st = 1;  
       } 
    }
   } 
      else
     { SaveMessagePrefix_2(BufTrans,9,0x96);  //������ �p�䨪�
       BufTrans[4] = 3 & num;      //����p ��⪨
       BufTrans[5] = 0;            //���-�� ����ᥩ � �⢥�
       BufTrans[6] = 0;            //�p��������� ���
       goto CT;
     }
     
       endpoint = pArchiv.di2h[run];
    if (ReadDataW25_IT(HOU, run, endpoint,(uint8_t*)&MEMDATA)==0)       
    {
       if (Mem_TestReqTimeEndp(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)==0) 
       {
         STARTENDTIM->ENDTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->ENDTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->ENDTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->ENDTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->ENDTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->ENDTIME.year = MEMDATA.STARTTIME.year;
         b_en = 1;  
       }
    }       
// �������� ����� ������������� ������� STARTENDTIM
        if (Mem_TestReqTimeSEp(STARTENDTIM)) //����� ������ ������ ������� �����
         err |= 0x80;                   // ����p��� ᮮ⭮襭�� �p���� ���p��

       if (b_st==0)
       startpoint = Mem_SearchStart(HOU,run, STARTENDTIM);
        if (startpoint != 0xFFFF)
        { if (Mem_TestReqTimeEqp(STARTENDTIM) == 0)
            endpoint = startpoint;
          else
          { if (b_en==0)
             endpoint = Mem_SearchEnd(HOU,run, STARTENDTIM);
          }   
        } 
        else err = 0x22;
        // ��砫�� ������
        if (err == 0) {                   // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
                                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (portC==2) { startpoint2 = startpoint;
                         endpoint2 = endpoint;
                       }
         else          { startpoint1 = startpoint;
                         endpoint1 = endpoint;
                       }
         if (pArchiv.di1h[run] < pArchiv.di2h[run])
         {  if (((startpoint < pArchiv.di1h[run]) || (endpoint > pArchiv.di2h[run]))) 
             err |= 0x40;
         }
         else 
         { if (((startpoint < pArchiv.di1h[run]) && (startpoint > pArchiv.di2h[run]))
             || ((endpoint > pArchiv.di2h[run]) && (endpoint < pArchiv.di1h[run]))
             || ((startpoint==0) && (endpoint == KolZapArcAll[HOU]-1)))
             err |= 0x40;
         }
         if ((startpoint - endpoint) == 1)
             err |= 0x40;
         
      }   // ����� if (err==0)
   }                                  // ����� ��p���⪨ 1-�� ���p��
      
   if (err) {
       if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_2(BufTrans,9,nfc+128);     // ������ �p�䨪�
         BufTrans[4] = 3 & num;         // ����p ��⪨
         BufTrans[5] = 0;               // ���-�� ����ᥩ � �⢥�
         BufTrans[6] = 0;               // �p��������� ���
      }
      else
         SaveMessagePrefix_2(BufTrans,6,0xFF);     // �訡�� ��p����p�� ���p��
   }
   else
   {   //err==0   �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
         if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
         else         { startpoint = startpoint1; endpoint = endpoint1;}
                                        // �ᮢ�� ����
      point = Mem_NextIndex(HOU, startpoint,Nrec*STARTENDTIM->RequestNum,startpoint ,endpoint);      
         // �p���p��� point; ���� �� ���p����쭮�� ����p� ���p��
      count = 0;
      if (point != 0xFFFF) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int ir=0;ir<Nrec;ir++) {
            count++;                    // ���-�� ����ᥩ
//            if (sizeph*count > 245){ count--;break;}
            if (ir) {
               point = Mem_NextIndex(HOU, point,1,startpoint ,endpoint);
               if (point == 0xFFFF)
                  break;                // ����� ���᪠
            }
                                        // �⥭�� ����� �ᮢ��� ����          
            ReadDataW25_IT(HOU, run, point  ,(uint8_t*)&MEMDATA);
            pbtime = &MEMDATA.STARTTIME;
                                        // �����
            phr->StartMonth   = pbtime->month;
            phr->StartDay     = pbtime->date;
            phr->StartYear    = pbtime->year;
            phr->StartHours   = pbtime->hours;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
       Vl.Val=(float)MEMDATA.dp_Qr;
       w=Vl.Bt[0]&0x1;
            if ((CONFIG_IDRUN[num-1].TypeRun) == 4 || (CONFIG_IDRUN[num-1].TypeRun) == 5
             || (CONFIG_IDRUN[num-1].TypeRun) == 10)
              { phr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                phr->DP_V   = MEMDATA.dp_Qr;   //  ��ꥬ �� �3
              }
              else
              { if (CONFIG_IDRUN[num-1].Vlag == 1)
                { phr->DP_V = MEMDATA.dp_Qr;
                  Vl.Val=phr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  phr->DP_V =  Vl.Val;
                }
                else
               { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
                   phr->DP_V = MEMDATA.dp_Qr;
                 else
                   phr->DP_V   = MEMDATA.dp_Qr*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   // �।��� ��९�� ��������, ���/�2 ���     |
                  Vl.Val=phr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  phr->DP_V =  Vl.Val;
                }
              }                            // ��p��� ������� � ������
            phr->Period = MEMDATA.count;
            phr->V      = MEMDATA.q;    // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
                Koef = 1.0;
              else
                Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
             }
      if (nfc== 0x13)
      {     // ��� ���ࣨ�, ���⭮��,CO2,N2
            if ((CONFIG_IDRUN[num-1].TypeRun) == 4 || (CONFIG_IDRUN[num-1].TypeRun) == 5
             || (CONFIG_IDRUN[num-1].TypeRun) == 10)
               phr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
           memmove(phrch,phr,sizephch-20);
           phrch->FullHeat   = MEMDATA.FullHeat*Koef/1.0e3;
           phrch->Ro   = MEMDATA.RO;     // �।��� ���⭮���
            Vl.Val=MEMDATA.Heat;
            w=Vl.Bt[0]&0x1;
            phrch->Heat  = MEMDATA.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];   // 㤥�쭠� ⥯��� ᣮ࠭�� ���/�3
                Vl.Val=phrch->Heat;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                phrch->Heat =  Vl.Val;
            phrch->N2   = MEMDATA.N2;     // �।��� ᮤ�ঠ��� ����
            phrch->CO2  = MEMDATA.CO2;    // �।��� ᮤ�ঠ��� 㣫���᫮��
            phrch++;
       }
       else    // nfc != 0x13
       {     // ��� �ᮢ�� � �ᮢ�� � ���⭮����
        phr->En   = MEMDATA.FullHeat*Koef/1.0e3;
        Vl.Val=MEMDATA.dp_Qr;
        w=Vl.Bt[0]&0x1;
            if ((CONFIG_IDRUN[num-1].TypeRun) == 4 || (CONFIG_IDRUN[num-1].TypeRun) == 5
             || (CONFIG_IDRUN[num-1].TypeRun) == 10)
              { phr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                phr->DP_V   = MEMDATA.dp_Qr;   //  ��ꥬ �� �3
              }
              else
              { if (CONFIG_IDRUN[num-1].Vlag == 1)
                { phr->DP_V = MEMDATA.dp_Qr;
                  Vl.Val=phr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  phr->DP_V =  Vl.Val;
                }
                else
                { phr->DP_V   = MEMDATA.dp_Qr*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   // �।��� ��९�� ��������, ���/�2 ���     |
                  Vl.Val=phr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  phr->DP_V =  Vl.Val;
                }
              }                            // ��p��� ������� � ������
            phr->V      = MEMDATA.q;    // ��ꥬ, �3

            phr->En   = MEMDATA.FullHeat*Koef/1.0e3;
           Vl.Val=MEMDATA.p;
           w=Vl.Bt[0]&0x1;
           if (GLCONFIG.EDIZM_T.EdIzP == 0)
            phr->P = MEMDATA.p;
           else
            phr->P = MEMDATA.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
           Vl.Val=phr->P;
           Vl.Bt[0]=Vl.Bt[0]&0xFE;
           Vl.Bt[0]=Vl.Bt[0] | w;
           phr->P =  Vl.Val;
            phr->t      = MEMDATA.t;    // �।��� ⥬������, �ࠤ. ������

          if (RH==0)
          { memmove(phrd,phr,sizeph);
           phrd->Ro   = MEMDATA.RO;     // �।��� ���⭮���
            Vl.Val=MEMDATA.Heat;
            w=Vl.Bt[0]&0x1;
            phrd->UTS  = MEMDATA.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];   // 㤥�쭠� ⥯��� ᣮ࠭�� ���/�3
                Vl.Val=phrd->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                phrd->UTS =  Vl.Val;
            phrd++;
          }
         }
            phr++;
            if (point == endpoint)
               break;                   // ����� ���᪠
         }
        }
//      }
//printf("2-count=%d point=%d sizeph=%d\n",count,point,sizeph);
        // ���-�� ����ᥩ
      if (count > Nrec) count = 0;
/*      if (nfc== 0x13)
      {                                  // ���-�� ����ᥩ
       RESPONSEHOURCHR.NumHourRecord = count;
       if ((point == endpoint) || (count==0) || (point > (KolZapArcAll[HOU]-1)))
         RESPONSEHOURCHR.ResponseStatus = 0;
       else                              // ����� �⢥� = 1; ���� �� �����
         RESPONSEHOURCHR.ResponseStatus = 1;
       SaveMessagePrefix_2(BufTrans,count*sizephch  + 9,nfc+128);        // �訡�� ��p����p�� ���p��
       memmove(&BufTrans[5],&RESPONSEHOURCHR,count*sizephch+2);
          for (i=9; i< count*sizepp+9; i++)
          { 
             switch (i)
             {    case 15: case  16: case 43: case 44 : case 71:case 72: 
                  case 99: case 100:  case 127: case 128: case 155: case 156:
                  case 183: case 184: case 211: case 212: case 239: case 240: 
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
         }         

       BufTrans[4] = (3 & num) | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
      }
      else   //nfc != 0x13
*/      
      {
       RESPONSEHOUR.NumHourRecord = count;
       RESPONSEHOURDEN.NumHourRecord = count;
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
       if ((point == endpoint) || (count==0) || (point > KolZapArcAll[HOU]-1) )
        { RESPONSEHOUR.ResponseStatus = 0;
          RESPONSEHOURDEN.ResponseStatus = 0;
        }
       else                              // ����� �⢥� = 1; ���� �� �����
        {  RESPONSEHOUR.ResponseStatus = 1;
          RESPONSEHOURDEN.ResponseStatus = 1;
        }
       if (RH) sizephd=sizeph;
                                        // ������ �p�䨪�
         SaveMessagePrefix_2(BufTrans,count*(sizephd-3)  + 9,nfc+128);        // �訡�� ��p����p�� ���p��
                                        // ������ ���p�
uint8_t k = 7;    
        if (RH)
       { memmove(&BufTrans[5],&RESPONSEHOUR,count*sizeph+9);
          for (i=9; i< count*sizeph+9+2; i++)
          { 
             switch (i)
              {    case 15: case  16: case  17: case 43: case 44 : case 45 : case 71:case 72: case 73: 
                  case 99: case 100: case 101: case 127: case 128: case 129: case 155: case 156: case 157:
                  case 183: case 184: case 185: case 211: case 212: case 213: case 239: case 240: case 241: 
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
         }         
           BufTrans[4] = 3 & num;            // ����p ��⪨
       }
       else
       {
        memmove(&BufTrans[5],&RESPONSEHOURDEN,count*sizephd+9);
          for (i=9; i< count*sizephd+9; i++)
          { 
             switch (i)
              {   case 15: case  16: case  17: case 43: case 44 : case 45 : case 71:case 72: case 73: 
                  case 99: case 100: case 101: case 127: case 128: case 129: case 155: case 156: case 157:
                  case 183: case 184: case 185: case 211: case 212: case 213: case 239: case 240: case 241: 
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
         }         
           BufTrans[4] = (3 & num) | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
       }
     }
   }
   CT: i=0;
}

//---------------------------------------------------------------------------
void dayPlus(unsigned char D,unsigned char M,unsigned char Y,
unsigned char* D1,unsigned char* M1,unsigned char *Y1)
{
   const unsigned char DayInMonth[]= {
      31, 28, 31 , 30, 31, 30, 31, 31, 30, 31, 30, 31
   };
unsigned char C;

      C = ((!(Y%4)) && (M == 2)) ? 29 : DayInMonth[M-1];

      *D1 = D+1;
      *Y1 = Y;
      *M1 = M;
      if(*D1 > C)
      {
        *M1 = M+1;
        if (*M1>12)
        {
           *M1=1;
           *Y1=Y+1;
        }
        *D1=1;
      }
}
//---------------------------------------------------------------------------
/*
void dayMinus(unsigned char D,unsigned char M,unsigned char Y,
unsigned char *D1,unsigned char *M1,unsigned char *Y1)
{
   const unsigned char DayInMonth[]= {
      31, 28, 31 , 30, 31, 30, 31, 31, 30, 31, 30, 31};
unsigned char C;

      *D1 = D-1;
      *Y1 = Y;
      *M1 = M;
      if(*D1 == 0)
      {
        *M1 = M-1;
        if (*M1== 0)
        {
           *M1=12;
           *Y1=Y-1;
        }
        *D1 = ((!(*Y1%4)) && (*M1 == 2)) ? 29 : DayInMonth[*M1-1];;
      }
}
*/
//---------------------------------------------------------------------------

void RdDay() {                          // �⥭�� ���筮�� ����
  portC=2;
  RdDayp(BufInKadr1,BufTrans1,&STARTENDTIME);
  CheckBufTrans(BufTrans1); //�������� ��
  Ini_Send1();  // �������� BufTrans  
}
void RdDayp(unsigned char* BufInKadr,unsigned char*  BufTrans,  struct startendtime *STARTENDTIM) {    // �⥭�� �ᮢ�� ������
   static  unsigned int   startpoint2,endpoint2;
   static  unsigned int   startpoint1,endpoint1;
   static unsigned int  startpoint,endpoint;
   struct bintime      *pbtime;
   struct reqdaily     *prd;            // ��p���p� ���p�� - �⥭�� ������ ������
   unsigned char       num,run;
   unsigned char       page;
   unsigned int        point,Si,Die,Dib;
   int                 err,nfc,k;
   struct dailyrecord  *pdr;            // ��p���p� ����� ������ ��� ��p����
   struct dailyrecordden *pdrd;
   struct dailyrecordchr *pdrch;           // ��p���p� ����� ������ ��� ��p����
   int                 count, Nrec,lenrecord;
   int                 sizepdd,sizepdch;
   int                 sizepd;
    union  resiv  RESIV;
   struct responsedailyden   RESPONSEDAILYDEN;
   struct responsedaily   RESPONSEDAILY;  // �⥭�� 横���᪨� ������
   struct responsedailychr   RESPONSEDAILYCHR;
   char RH;
   union {
      unsigned char Bt[4];
      float Val;
   } Vl;
   char w,VrutotalIs;

   nfc = BufInKadr[3];
   RH= (INTPAR.Com2Mode==0);// && (INTPAR.HeatReport==0));
   num = BufInKadr[4] & 3;
   run = num - 1;
   VrutotalIs = 0;
   if (b_Counter[num-1])
      VrutotalIs = (nfc==0x1B);
//   if (nfc==0x11) Nrec = 10;  // ��ࠬ���� ��⠢� ����
//   else
   if ((nfc==0x1B) || (nfc==0x14))
   { if (VrutotalIs) Nrec = 9;
     else  // nfc==0x14
     { if (RH)  // ��� ���⭮��
             Nrec = 10;
       else  Nrec = 7;
     }
   }
   err = 0;
   count = 0;
   point = 0;
   sizepd = sizeof(struct dailyrecord);
   sizepdd = sizeof(struct dailyrecordden);
   sizepdch = sizeof(struct dailyrecordchr);
   if (!VrutotalIs)          // ��� V�� tot sizepd = 27
      sizepd = sizepd-4;     // ���� 23

   if (BufInKadr[2]-sizeof(struct reqdaily) != 7)
      err |= 1;                         // ����p��� ����� ���p��
//printf(" sizepd=%d\n",sizepd);
   if ((num & 3) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pdr = &RESPONSEDAILY.DAILYRECORD[0];
   pdrd = &RESPONSEDAILYDEN.DAILYRECORDDEN[0];
   pdrch = &RESPONSEDAILYCHR.DAILYRECORDCHR[0];
   prd = &RESIV.REQDAILY;
                                     // ��p������� ��p����p�� ���p��
   memmove(prd,&BufInKadr1[5],sizeof(struct reqdaily));

   if (prd->RequestNum != 0) {
                                     // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIM->RequestNum == prd->RequestNum)
         return;

      if (STARTENDTIM->RequestNum +1 != prd->RequestNum)
         err |= 0x04;                // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }                                 // ����� �����

   STARTENDTIM->RequestNum = prd->RequestNum;
   STARTENDTIM->num = 3 & (num-1);   // ����� ��⪨
                                     // ��p���⪠ 1-�� ���p��
   if (STARTENDTIM->RequestNum == 0) {
      pbtime = &STARTENDTIM->STARTTIME;

      pbtime->seconds = 0;
      pbtime->minutes = 0;           
//      pbtime->hours   = GLCONFIG.ContrHour; // ����p������ ���
      pbtime->hours   = 0;
      pbtime->date    = prd->StartDay;
      pbtime->month   = prd->StartMonth;
      pbtime->year    = prd->StartYear;

      if (Mem_TestReqTime(pbtime))
         err |= 8;                   // ����p��� �p����� ���p���
      pbtime = &STARTENDTIM->ENDTIME;

      pbtime->seconds = 59;
      pbtime->minutes = 59;         
      pbtime->hours   = (GLCONFIG.ContrHour) ? GLCONFIG.ContrHour-1 : 23;

      if (GLCONFIG.ContrHour)
         dayPlus(prd->EndDay,prd->EndMonth,prd->EndYear ,&pbtime->date,&pbtime->month,&pbtime->year);
      else 
          {
         pbtime->date    = prd->EndDay;
         pbtime->month   = prd->EndMonth;
         pbtime->year    = prd->EndYear;
      }
      pbtime->seconds = 0;
      pbtime->minutes = 0;         
      pbtime->hours   = 0;

      if (Mem_TestReqTime(pbtime))
         err |= 0x10;           // ����p��� �p����� ���p���

      if (Mem_TestReqTimeSEp(STARTENDTIM))
         err |= 0x20; // ����p��� ����������� �p���� ���p���
                 // �������� ������ �� 1 �� 0x24 - ������ ���p���
// STARTENDTIM - ������������ 
//      for (uint16_t is=0; is<16; is=is+1)
//   {    ReadDataW25_IT(DAY, run, is  ,(uint8_t*)&MEMDATA);
//       uint8_t sec = MEMDATA.STARTTIME.hours;
//       sec = 0;
//   }
     startpoint = pArchiv.di1d[run];     
      
     uint8_t b_st = 0, b_en = 0;     
    if (startpoint < KolZapArcAll[DAY]-1) 
    {
     if (ReadDataW25_IT(DAY, run, startpoint  ,(uint8_t*)&MEMDATA)==0)        
     {  if (Mem_TestReqTimeStart(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)) 
       {
         STARTENDTIM->STARTTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->STARTTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->STARTTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->STARTTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->STARTTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->STARTTIME.year = MEMDATA.STARTTIME.year;
         b_st = 1;  
       } 
    }
   }     
   else
   {   SaveMessagePrefix_2(BufTrans,9,0x96);  //������ �p�䨪�
       BufTrans[4] = 3 & num;      //����p ��⪨
       BufTrans[5] = 0;            //���-�� ����ᥩ � �⢥�
       BufTrans[6] = 0;            //�p��������� ���
       goto CT;
   }
      
     
       endpoint = pArchiv.di2d[run];
    if (ReadDataW25_IT(DAY, run, endpoint  ,(uint8_t*)&MEMDATA)==0)        
    {   if (Mem_TestReqTimeEndp(STARTENDTIM,(uint8_t*)&MEMDATA.STARTTIME)==0) 
       {
         STARTENDTIM->ENDTIME.date = MEMDATA.STARTTIME.date;
         STARTENDTIM->ENDTIME.hours = MEMDATA.STARTTIME.hours;
         STARTENDTIM->ENDTIME.minutes = MEMDATA.STARTTIME.minutes;
         STARTENDTIM->ENDTIME.month = MEMDATA.STARTTIME.month;         
         STARTENDTIM->ENDTIME.seconds = MEMDATA.STARTTIME.seconds;
         STARTENDTIM->ENDTIME.year = MEMDATA.STARTTIME.year;
         b_en = 1;  
       }
   }       
// �������� ����� ������������� ������� STARTENDTIM
        if (Mem_TestReqTimeSEp(STARTENDTIM)) //����� ������ ������ ������� �����
         err |= 0x80;                   // ����p��� ᮮ⭮襭�� �p���� ���p��

       if (b_st==0)
       startpoint = Mem_SearchStart(DAY, run, STARTENDTIM);      
        if (startpoint != 0xFFFF)
        { if (Mem_TestReqTimeEqp(STARTENDTIM) == 0)
            endpoint = startpoint;
          else
          { if (b_en==0)
             endpoint = Mem_SearchEnd(DAY,run, STARTENDTIM);
          }   
        } 
        else err = 0x22;

      if (err == 0) {      // ��p���������� �����

         if (portC==2) { startpoint2 = startpoint;
                         endpoint2 = endpoint;
                       }
         else          { startpoint1 = startpoint;
                         endpoint1 = endpoint;
                       }
         if (pArchiv.di1d[run] < pArchiv.di2d[run])
         {  if (((startpoint < pArchiv.di1d[run]) || (endpoint > pArchiv.di2d[run]))) 
             err |= 0x40;
         }
         else 
         { if (((startpoint < pArchiv.di1d[run]) && (startpoint > pArchiv.di2d[run]))
             || ((endpoint > pArchiv.di2d[run]) && (endpoint < pArchiv.di1d[run]))
             || ((startpoint==0) && (endpoint == KolZapArcAll[DAY]-1)))
             err |= 0x40;
         }
         if ((startpoint - endpoint) == 1)
             err |= 0x40;
      }
   }               //         ����� ��p������ ��p���� ���p���

if (err) {
      if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_2(BufTrans,9,nfc+128);     // ������ �p�䨪�
         BufTrans[4] = 3 & num;      // ����p ��⪨
         BufTrans[5] = 0;            // ���-�� ����ᥩ � �⢥�
         BufTrans[6] = 0;            // �p��������� ���
      }
      else
         SaveMessagePrefix_2(BufTrans,6,0xFF);     // �訡�� ��p����p�� ���p��
   }  // if (err!=0)
   else {   //  (err==0)  �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
         if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
         else         { startpoint = startpoint1; endpoint = endpoint1;}
                                     // ����� ����
      point = Mem_NextIndex(DAY, startpoint,Nrec*STARTENDTIM->RequestNum,startpoint ,endpoint);
// �p���p��� point; ���� �� ���p����쭮�� ����p� ���p��
      count = 0;
      if (point != 0xFFFF) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int ir=0;ir<Nrec;ir++) {
            count++;                    // ���-�� ����ᥩ
            if (ir) {
               point = Mem_NextIndex(DAY, point,1,startpoint ,endpoint);
               if (point == 0xFFFF)
                  break;                // ����� ���᪠
            }
                                        // �⥭�� ����� �ᮢ��� ����          
            ReadDataW25_IT(DAY, run, point  ,(uint8_t*)&MEMDATA);

            pbtime = &MEMDATA.STARTTIME;
                                     // �����
            pdr->StartMonth = pbtime->month;
                                     // ����
            pdr->StartDay   = pbtime->date;
   union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
Vl.Val=MEMDATA.dp_Qr;
w=Vl.Bt[0]&0x1;
            if (b_Counter[run])
             {  pdr->StartDay |= 0x80;// ᯮᮡ ����p���� �� ��⪥
                pdr->DP_V = MEMDATA.dp_Qr;  //��ꥬ �� �3
             }
             else
             { if (CONFIG_IDRUN[num-1].Vlag == 1)
                { pdr->DP_V = MEMDATA.dp_Qr;
                  Vl.Val=pdr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pdr->DP_V =  Vl.Val;
                }
                else
                { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
                   pdr->DP_V = MEMDATA.dp_Qr;
                  else
                   pdr->DP_V = MEMDATA.dp_Qr*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];  // �।��� ��९�� ��������, ���/�2 ���     |
                  Vl.Val=pdr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  pdr->DP_V =  Vl.Val;
                }
             }
                                     // ���
            pdr->StartYear    = pbtime->year;

            pdr->V    = MEMDATA.q;   // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
              Koef = 1.0;
             else
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
      if (nfc== 0x11)  //��� ��ࠬ��஢ 娬��⠢�
      {     // ��� ���ࣨ�, ���⭮��,CO2,N2
            if (b_Counter[run])
               pdr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
           memmove(pdrch,pdr,sizepdch-20);
//           pdrch->Period = MEMDATA.count;
           pdrch->FullHeat   = MEMDATA.FullHeat*Koef/1.0e3;
           pdrch->Ro   = MEMDATA.RO;     // �।��� ���⭮���
            Vl.Val=MEMDATA.Heat;
            w=Vl.Bt[0]&0x1;
            pdrch->Heat  = MEMDATA.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];   // 㤥�쭠� ⥯��� ᣮ࠭�� ���/�3
                Vl.Val=pdrch->Heat;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pdrch->Heat =  Vl.Val;
            pdrch->N2   = MEMDATA.N2;     // �।��� ᮤ�ঠ��� ����
            pdrch->CO2  = MEMDATA.CO2;    // �।��� ᮤ�ঠ��� 㣫���᫮��
            pdrch++;
       }
       else  // (nfc!= 0x11)  nfc=0x14, sizepd=23 ��� sizepd=31 �  nfc=0x1B, sizepd=27
       {     // ��� ������ � ������ � ���⭮����
//        pdr->En   = MEMDATA.FullHeat*Koef/1.0e3;
        Vl.Val=MEMDATA.dp_Qr;
        w=Vl.Bt[0]&0x1;
            if (b_Counter[run])
             {  pdr->StartDay |= 0x80;// ᯮᮡ ����p���� �� ��⪥
                pdr->DP_V = MEMDATA.dp_Qr;  //��ꥬ �� �3
             }
             else
             { if (CONFIG_IDRUN[num-1].Vlag == 1)
                { pdr->DP_V = MEMDATA.dp_Qr;
                  Vl.Val=pdr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pdr->DP_V =  Vl.Val;
                }
                else
                { pdr->DP_V = MEMDATA.dp_Qr*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];  // �।��� ��९�� ��������, ���/�2 ���     |
                  Vl.Val=pdr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  pdr->DP_V =  Vl.Val;
                }
             }
             pdr->V    = MEMDATA.q;   // ��ꥬ, �3

            pdr->En   = MEMDATA.FullHeat*Koef/1.0e3;
            Vl.Val=MEMDATA.p;
            w=Vl.Bt[0]&0x1;
           if (GLCONFIG.EDIZM_T.EdIzP == 0)
            pdr->P = MEMDATA.p;
           else
            pdr->P    = MEMDATA.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];   // �।��� ��������, ���/�2
            Vl.Val=pdr->P;
            Vl.Bt[0]=Vl.Bt[0]&0xFE;
            Vl.Bt[0]=Vl.Bt[0] | w;
            pdr->P =  Vl.Val;
            pdr->t    = MEMDATA.t;   // �।��� ⥬������, �ࠤ. ������
           if (VrutotalIs)
            pdr->Vrtot= MEMDATA.CO2; // ��騩 ��� � ��, ���.�3;
           else   pdr->Vrtot=0;
          if (RH==0)
          { memmove(pdrd,pdr,sizepd);
            pdrd->Ro   = MEMDATA.RO;     // �।��� ���⭮���
            Vl.Val=MEMDATA.Heat;
            w=Vl.Bt[0]&0x1;
            pdrd->UTS  = MEMDATA.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];   // 㤥�쭠� ⥯��� ᣮ࠭�� ���/�3
                Vl.Val=pdrd->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pdrd->UTS =  Vl.Val;
            pdrd++;
          } // if (RH==0)
        }  // else if (nfc==0x11)

           pdr++;
            if (point == endpoint)
               break;                // ����� ���᪠
         }
      } //  for (i=0; i<Nreq
      if (count > Nrec) count = 0;
uint16_t i;
/*      
      if (nfc== 0x11)
      {                                  // ���-�� ����ᥩ
       RESPONSEDAILYCHR.NumRecord = count;
       if ((point == endpoint) || (count==0) || (point > ARC4-1))
         RESPONSEDAILYCHR.ResponseStatus = 0;
       else                              // ����� �⢥� = 1; ���� �� �����
         RESPONSEDAILYCHR.ResponseStatus = 1;
         SaveMessagePrefix_2(BufTrans,count*sizepdch + 9, nfc+128);        // �訡�� ��p����p�� ���p��
         memmove(&BufTrans[5],&RESPONSEDAILYCHR,count*sizepdch+2);
         BufTrans[4] = 3 & num ;
      }
      else   // nfc=0x14 ��� 0x1B
*/
      if (nfc == 0x14)
      {
       RESPONSEDAILY.NumRecord = count;
       RESPONSEDAILYDEN.NumRecord = count;
                                  // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
       if ((point == endpoint) || (count==0) || (point > ARC4-1))
       {
         RESPONSEDAILY.ResponseStatus = 0;
         RESPONSEDAILYDEN.ResponseStatus = 0;
       }
       else                              // ����� �⢥� = 1; ���� �� �����
       {  RESPONSEDAILY.ResponseStatus = 1;
          RESPONSEDAILYDEN.ResponseStatus = 1;
       }
//printf("pd=%d pdd=%d RH=%d count=%d\n",sizepd, sizepdd, RH,count);
       if (VrutotalIs)   //sizepd = 27
       {
         memmove(&BufTrans[5],&RESPONSEDAILY,count*sizepd+2);
         SaveMessagePrefix_2(BufTrans,count*(sizepd-1)  + 9, nfc+128);        // �訡�� ��p����p�� ���p��
         BufTrans[4] = 3 & num;            // ����p ��⪨
       }
       else  //(nfc==0x14)
       { if (RH)   //sizepd=23
         {
           for (k=0; k<count; k++)
           memmove(&BufTrans[7]+k*sizepd,&RESPONSEDAILY.DAILYRECORD[k],sizepd);
           BufTrans[5] = RESPONSEDAILY.NumRecord;
           BufTrans[6] = RESPONSEDAILY.ResponseStatus;
           BufTrans[4] = 3 & num;            // ����p ��⪨
           k = 7; 
             for (i=7; i< count*sizepd+9; i++)
          { 
             switch (i)
              {   case 10: case  34: case 58:
                  case 82: case 104: case 128: case 152:
                  case 174: case 198: case 222: case 248:  
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
         }         

             SaveMessagePrefix_2(BufTrans,count*(sizepd-1)  + 9,nfc+128);        // �訡�� ��p����p�� ���p��
         }
         else  // sizepdd==31,
         {
           memmove(&BufTrans[5],&RESPONSEDAILYDEN,count*sizepdd+2);
           BufTrans[4] = (3 & num) | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
           k = 7; 
             for (i=7; i< count*sizepd+9; i++)
          { 
             switch (i)
              {   case 10: case  38: case 66:
                  case 94: case 122: case 150: case 178:
                  case 206: case 234:  
                  break;
                  default: { BufTrans[k] = BufTrans[i]; k++;}
             }   
         }         
           SaveMessagePrefix_2(BufTrans,count*(sizepdd-1)  + 9,nfc+128);        // �訡�� ��p����p�� ���p��
         }
       }                                 // ������ ���p�
     } //  else if (nfc== 0x11) ����� (nfc=0x14 ��� 0x1B)
   }  // ����� if (err)
   CT: k = 0;
}

/*
//------------------------------------------------------------------------
int NeedVSumm(struct reqvalarm *prd) {
   long t1,t2,tc;
   struct bintime      bTmp;

   bTmp.seconds = 0;
   bTmp.minutes = 0;
   bTmp.hours   = GLCONFIG.ContrHour;

   bTmp.date    = prd->StartDay;
   bTmp.month   = prd->StartMonth;
   bTmp.year    = prd->StartYear;

//   t1 = GetContrHSec(bTmp);

//   tc = GetContrHSec(BINTIME);          // ⥪�騥 ��⪨

   if (t1 == tc)                        // �᫨ ��⪨ ⥪�騥
      return 1;                         // �㬬��� �뤠��
   else
      return 0;                         // �㬬��� �� �뤠��
}
*/
//------------------------------------------------------------------------
                                        // �⥭�� �㬬��� ���਩��� ��ꥬ��
/*
int RdValarmSumm(unsigned char* BufInKadr,unsigned char*  BufTrans) {
   unsigned char       num;
   int count;
   struct responsevalarm   RESPONSEVALARM;
   struct valarmrecord *pdr;

   num = BufInKadr[4] & 3;
   pdr = &RESPONSEVALARM.VALARMRECORD[0];


//      outportb(0x1D0,BASE_CFG_PAGE);                // ����� ��࠭��� ���䨣��樨
//      FP_SEG(ValarmSumm) = SEGMENT;         // 㪠��⥫� �� �㬬���� ����. ��ꥬ��
//      FP_OFF(ValarmSumm) = 576;
//      FP_SEG(ValarmSumm2) = SEGMENT;         // 㪠��⥫� �� �㬬���� ����. ��ꥬ��
//      FP_OFF(ValarmSumm2) = 768;
                                        // �����
      pdr->StartMonth = (ValarmSumm+num-1)->STARTTIME.month;
                                        // ����
      pdr->StartDay   = (ValarmSumm+num-1)->STARTTIME.date;

      if ((CONFIG_IDRUN[num-1].TypeRun) == 4 || (CONFIG_IDRUN[num-1].TypeRun) == 5
       || (CONFIG_IDRUN[num-1].TypeRun) == 10)
         pdr->StartDay |= 0x80;         // ᯮᮡ ����p���� �� ��⪥
                                        // ���
      pdr->StartYear  = (ValarmSumm+num-1)->STARTTIME.year;

                                        // �ਢ������ ��ꥬ ��� ���਩
      pdr->VTotSt = (ValarmSumm+num-1)->VTotSt + (ValarmSumm2+num-1)->VTotSt;
                                        // ࠡ�稩 ��ꥬ ��� ���਩
      pdr->VTotWc = (ValarmSumm+num-1)->VTotWc + (ValarmSumm2+num-1)->VTotWc;
                                        // �ਢ������ ��ꥬ �����⥫��� ���਩
      pdr->VAddSt = (ValarmSumm+num-1)->VAddSt + (ValarmSumm2+num-1)->VAddSt;

                                        // ���⥫쭮��� ��� ���਩
      pdr->TTot   = (ValarmSumm+num-1)->TTot + (ValarmSumm2+num-1)->TTot;
                                        // ���⥫쭮��� �⪫�祭�� ��⠭��
      pdr->Toff   = (ValarmSumm+num-1)->Toff + (ValarmSumm2+num-1)->Toff;
                                        // ���⥫쭮��� ࠡ��� �� Qmin
      pdr->Tqmin  = (ValarmSumm+num-1)->Tqmin + (ValarmSumm2+num-1)->Tqmin;

      count = 1;
                                        // ���-�� ����ᥩ
      RESPONSEVALARM.NumRecord = 1;

      RESPONSEVALARM.ResponseStatus = 0;
                                        // ������ �p�䨪�
      SaveMessagePrefix_2(BufTrans,count*sizeof(struct valarmrecord)+9,0x8C);
                                        // ������ ���p�
      movmem(&RESPONSEVALARM,&BufTrans[5],count*sizeof(struct valarmrecord)+2);

      BufTrans[4] = 3 & num;            // ����p ��⪨

      return 1;
}

//---------------------------------------------------------------------------

void RdValarmp(unsigned char* BufInKadr,unsigned char*  BufTrans,
 struct startendtime *STARTENDTIM, struct valarm_data  *vdata)
{
   static  unsigned int   startpoint2,endpoint2;
   static  unsigned int   startpoint1,endpoint1;
   static unsigned int startpoint,endpoint;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point,Nrec;
   int                 err,i;
   int                 count,sizepval,sizepvalr;
   struct reqvalarm    *prd;            // ��p���p� ���p�� - �⥭�� ���਩��� ��ꥬ��
   struct valarmrecord *pdr;            // ��p���p� ����� ������ ��� ��p����
   struct responsevalarm   RESPONSEVALARM;

   err = 0;
  Nrec=9;
  sizepval = sizeof(struct reqvalarm);
  sizepvalr = sizeof(struct valarmrecord);

   if (BufInKadr[2]-sizepval != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4] & 3;

   if ((num & 3) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨


   prd = &RESIV.REQVALARM;
   pdr = &RESPONSEVALARM.VALARMRECORD[0];
                                        // ��p������� ��p����p�� ���p��
   memmove(prd,&BufInKadr[5],sizepval);
   if (NeedVSumm(prd) == 1) {              // ��砫쭠� ��� ⥪���
      RdValarmSumm(BufInKadr,BufTrans);    //pdr
   }
   else {                               //
      if (prd->RequestNum != 0) {
                                           // ������� �०��� �⢥� �� ������ ���㫥�� �����
         if (STARTENDTIM->RequestNum == prd->RequestNum)
            return;

         if (STARTENDTIM->RequestNum +1 != prd->RequestNum)
            err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
      }
                                           // ����� �����
      STARTENDTIM->RequestNum = prd->RequestNum;
      STARTENDTIM->num = 3 & (num-1);      // ����� ��⪨

      if (STARTENDTIM->RequestNum == 0) {  // ��p���⪠ 1-�� ���p��
         pbtime = &STARTENDTIM->STARTTIME;
         pbtime->seconds = 0;
         pbtime->minutes = 0;
                                           // ����p���� ��;
         pbtime->hours   = GLCONFIG.ContrHour;
         pbtime->date    = prd->StartDay;
         pbtime->month   = prd->StartMonth;
         pbtime->year    = prd->StartYear;

         if (Mem_TestReqTime(pbtime))
            err |= 8;                      // ����p��� �p����� ���p��

         pbtime = &STARTENDTIM->ENDTIME;
         pbtime->seconds = 59;
         pbtime->minutes = 59;
                                           // ����p���� ��;
         pbtime->hours   = (GLCONFIG.ContrHour) ? GLCONFIG.ContrHour-1 : 23;

         if (GLCONFIG.ContrHour)
            dayPlus(prd->EndDay,prd->EndMonth,prd->EndYear,
                   &pbtime->date,&pbtime->month,*pbtime->year);
         else {
            pbtime->date    = prd->EndDay;
            pbtime->month   = prd->EndMonth;
            pbtime->year    = prd->EndYear;
         }

         if (Mem_TestReqTime(pbtime))
            err |= 0x10;                   // ����p��� �p����� ���p��

         if (Mem_TestReqTimeSEp(STARTENDTIM))
            err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��
                                           // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
         if (err == 0) {                   // ����� ����
            page = GetValarmPage(num-1);
                                           // ��砫�� ������
            startpoint = ValarmDataSearchStart2(page,STARTENDTIM);
                                           // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
            if (startpoint < V_ALARM) {    // ����� ����
               SetBinTimeV1(page,startpoint,&BinTmp,&BinTmp2);
            }
//printf("stp=%d BinTmp=%d BinTmp2=%d\n",startpoint, BinTmp.date, BinTmp2.date);
                                           // ��������� < ��砫� ��娢�
            if ((startpoint > V_ALARM) || (BinToSec(BinTmp) < BinToSec(BinTmp2))) {
               startpoint = ValarmDataSearchStart(page,STARTENDTIM); // ��砫�� ������
            }
         if (portC==2) startpoint2 = startpoint;
         else          startpoint1 = startpoint;

            if (Mem_TestReqTimeEqp(STARTENDTIM) == 0) {
               endpoint = startpoint;
            }
            else {                          // ������ ������
               endpoint = ValarmDataSearchEnd(page,STARTENDTIM);
            }
        if (portC==2) endpoint2 = endpoint;
        else          endpoint1 = endpoint;
            if ((startpoint > V_ALARM-1) || (endpoint > V_ALARM-1))
               err |= 0x40;
         }
         if (err == 0) {
            ValarmDataRd(startpoint,page,vdata); // �⥭�� 1-� �����

           if (Mem_VTestReqTimeEnd(STARTENDTIM))
              err |= 0x80;
         }
      }                                 // ����� ��p���⪨ ��p���� ���p��
//printf("stpoint=%d endpoint=%d err=%02X page=%02X StData=%d endt=%d\n",startpoint,endpoint,err,page,ValarmData.STARTTIME.date,STARTENDTIME.ENDTIME.month);

      if (err) {
         if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
               SaveMessagePrefix_2(BufTrans,9,0x8C);     // ������ �p�䨪�
               BufTrans[4] = 3 & num;   // ����p ��⪨
               BufTrans[5] = 0;         // ���-�� ����ᥩ � �⢥�
               BufTrans[6] = 0;         // �p��������� ���
//            }
         }
         else
            SaveMessagePrefix_2(BufTrans,6,0xFF);     // �訡�� ��p����p�� ���p��
      }
      else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
       if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
       else         { startpoint = startpoint1; endpoint = endpoint1;}
                                           // ����� ����
         page  = GetValarmPage(num-1);
         point = NextIndexValarm(startpoint,Nrec*STARTENDTIM->RequestNum,page);
         count = 0;

         if (point < V_ALARM) {            // ���� ����� � ⠪�� ᬥ饭���
            for (int i=0;i<Nrec;i++) {
               count++;                    // ���-�� ����ᥩ
               if (i) {
                  point = NextIndexValarm(point,1,page);
                  if (point > V_ALARM-1)
                     break;                // ����� ���᪠
               }
//printf("point=%d count=%d\n",point,count);
                                           // �⥭�� ����� ���਩��� ��ꥬ��
               ValarmDataRd(point,page,vdata);
//               pbtime = &ValarmData.STARTTIME;
               pbtime = &vdata->STARTTIME;
                                           // �����
               pdr->StartMonth   = pbtime->month;
                                           // ����
               pdr->StartDay     = pbtime->date;

               if ((CONFIG_IDRUN[num-1].TypeRun) == 4 || (CONFIG_IDRUN[num-1].TypeRun) == 5
                || (CONFIG_IDRUN[num-1].TypeRun) == 10)
                  pdr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                                           // ���
               pdr->StartYear     = pbtime->year;

                                           // �ਢ������ ��ꥬ ��� ���਩
               pdr->VTotSt = vdata->VTotSt;
                                           // ࠡ�稩 ��ꥬ ��� ���਩
               pdr->VTotWc = vdata->VTotWc;
                                           // �ਢ������ ��ꥬ ����������
               pdr->VAddSt = vdata->VAddSt;

                                           // ���⥫쭮��� ��� ���਩
               pdr->TTot   = vdata->TTot;
                                           // ���⥫쭮��� �⪫�祭�� ��⠭��
               pdr->Toff   = vdata->Toff;
                                           // ���⥫쭮��� ࠡ��� �� ���.���祭���
               pdr->Tqmin  = vdata->Tqmin;

               pdr++;
//printf("VTotSt=%f VTotWc=%f  TTot=%ld StartDay=%d\n",ValarmData.VTotSt,ValarmData.VTotWc,ValarmData.TTot,pbtime->date);

               if (point == endpoint)
                  break;                // ����� ���᪠
            }
         }
         if (count > Nrec) count = 0;   // ���-�� ����ᥩ
         RESPONSEVALARM.NumRecord = count;

         if ((point == endpoint) || (count==0) || (point > V_ALARM-1)) {
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
               RESPONSEVALARM.ResponseStatus = 0;
         }
         else                           // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
            RESPONSEVALARM.ResponseStatus = 1;
                                        // ������ �p�䨪�
         SaveMessagePrefix_2(BufTrans,count*sizepvalr+9,0x8C);
                                           // ������ ���p�
         movmem(&RESPONSEVALARM,&BufTrans[5],count*sizepvalr+2);

         BufTrans[4] = 3 & num;            // ����p ��⪨
      }
   }
}
//-------------------
void RdValarm() {                       // �⥭�� ���਩��� ��ꥬ��
   portC=2;
   RdValarmp(BufInKadr1,BufTrans,&STARTENDTIME,&ValarmData);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}
*/
//---------------------------------------------------------------------------
void AnswSys() {                        // �⥭�� ��⥬��� ��p����p��
 AnswSysp(BufInKadr1,BufTrans1);
  CheckBufTrans(BufTrans1); //�������� ��
  Ini_Send1();  // �������� BufTrans
}
void AnswSysp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct sys        *psys,SYS;

   psys = &SYS;


   psys->Period      = GLCONFIG.Period; // ���p��� ���������� � ������
                                        // ����p���� ��
   psys->ContrHour   = GLCONFIG.ContrHour;
                                        // �����
   psys->SummerMonth = GLCONFIG.SummerMonth;
                                        // ����
   psys->SummerDay   = GLCONFIG.SummerDay;
                                        // �� ��p�室� �� ��⭥� �p���
   psys->SummerHours = GLCONFIG.SummerHours;
                                        // �����
   psys->WinterMonth = GLCONFIG.WinterMonth;
                                        // ����
   psys->WinterDay   = GLCONFIG.WinterDay;
                                        // �� ��p�室� �� ������ �p���
   psys->WinterHours = GLCONFIG.WinterHours;
                                        // ��ꥬ ��� ���ਧ���, �3/���
   psys->vol_odor    = GLCONFIG.vol_odor;
                                        // ���ᨬ���� ��ꥬ ��� �����ॢ�, �3/��
   psys->termQmax    = GLCONFIG.termQmax;
                                        // �᫮ �ॠ��� ���2
   psys->PreambNum   = GLCONFIG.PreambNum;
                                        // �᫮ �ॠ��� ���1
   psys->PreambNum_1 = GLCONFIG.PreambNum_1;
                                        // ������ �p�䨪�
   SaveMessagePrefix_2(BufTrans,sizeof(struct sys)+6,0xA2);
                                        // ������ ���p�
   memmove(&BufTrans[4],&SYS,sizeof(struct sys));
}
//---------------------------------------------------------------------------
/*
void fPassword() {    // ���� ����� � ��஫� �㭪�� 0x58
 fPasswordp(BufInKadr1,BufTrans1,Tlogin,2,&Dostup,attempt);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}
void fPasswordp(unsigned char* BufInKadr,unsigned char*  BufTrans, unsigned char* Tlogin, char nPort, unsigned int* Dostup, unsigned char* attemp)
{    // ���� ����� � ��஫� �㭪�� 0x58
   int    err = 0,i,dst,k;
   struct npasword *pas;
   char *buf1="    \0";
   char nlog[5];
   char u1,u2,u3,u4,u5,u6;
   char  *pENP;
   union  resiv  RESIV;
   unsigned int StartDst;

//sprintf(str,"nPort=%d Dostup=%d \n\r",nPort,*Dostup);
//ComPrint_2(str);

   if (BufInKadr[2] != 20)
      err |= 1;                         // ����p��� ����� ���p��
    StartDst = (nPort==1) ? GLCONFIG.StartDostup1 : GLCONFIG.StartDostup;
    pas = &RESIV.NPASWORD;
    movmem(&BufInKadr[4],pas,sizeof(struct npasword));
    movmem(pas->nlogin,nlog,4); nlog[4]='\0';    // �室��� ���
    u4 =(strcmp((char*)Tlogin,nlog)==0);  // �室 � ⥬-�� ������ Tlogin == nlog
//printf("1-pw2=%s\n",PASSWR2);
   if (nPort==2)
   { if (memcmp(&BufInKadr[4],PASSWR2,14)==0) {dst=*Dostup; goto D2;}}
   else
   { if (memcmp(&BufInKadr[4],PASSWR1,14)==0) {dst=*Dostup; goto D2;}}
//printf("2-pw1=%s\n",PASSWR1);

    Mem_LoginRd();   // �� ��� � LogPassw
    i=0;
    do
     { if (memcmp(pas->nlogin, &LogPassw[i].login,4)==0)
       break;
       i++;
     }
     while (i<6);
     k=i;
     dst = 0;
    attemp[k]++;
    if ((attemp[k] < LATT) && (k < 6))
  {
   //�஢�ઠ ��஫�
    u1 =(strcmp((char*)Tlogin,buf1)==0);//Tlogin ����, ��� ���짮��⥫�
    u2 =(memcmp(pas->nlogin,buf1,4)!=0); //nlogin �� ����, ���� ��� ���짮��⥫�
    u3 = (strlen(nlog)==4); // ����� ����� = 4
//    if (((*Dostup == StartDst) || (strcmp(nlog,"Admn\0")==0)) // ��室��� ���ﭨ�
//         && u1 && u2 && u3  || u4 || (!u1) && (!u4) && u2 && u3)
//         && u1 && u2 && u3  ||  (!u1) && u2 && u3)
     {
      for (i=0; i<6; i++)
      {  u5 = (memcmp(pas->nlogin, LogPassw[i].login,4)==0);
         u6 = (memcmp(pas->npassw, LogPassw[i].Password,10)==0);
      if (u5)
       if (((i==0)&&(strlen((char*)LogPassw[i].Password)==0)) || (u6))
       { if (i == 0)
           {*Dostup = 7;
//            if (strlen(LogPassw[i].Password)==0)
//            { outportb(0x1d0,RESERV_CFG_PAGE);       // ��⠭����� ��p����� �����
//              pENP = (char far *)MK_FP(SEGMENT,4100);
//              memcpy(LogPassw[i].Password,"          ",10);
//              _fmemcpy(pENP,(char far *)LogPassw[i].Password,10);
//            }
              attemp[i] = 0;
           }
          else  *Dostup = LogPassw[i].kodostup;    // if i>0
//printf("u1=%d u2=%d u3=%d u4=%d\n",!u1, u2, u3, !u4);
//          if ((!u1) && (!u4) && u2 && u3)
          if ((!u1) && u2 && u3)
              Mem_WrEndSeans_2(Tlogin,nPort);

             strncpy((char*)Tlogin ,(char*)pas->nlogin,4);
             Tlogin[4]='\0';
             if (nPort==2)
             logTimeb = BINTIME;//�६� ��砫� ᥠ�� = ⥪�饥 �६�
             else
             logTimeb1 = BINTIME;
//printf("i>0 port=%d h1=%d m1=%d h=%d m=%d\n",nPort,logTimeb1.hours,logTimeb1.minutes,logTimeb.hours,logTimeb.minutes);
             dst = *Dostup;
             attemp[i] = 0;
             break;
          } // ����� if (u5 && U6)- ���-��஫� ᮢ����
       } //for (i=0; i<6; i++)
     }  //if ((Dostup == GLCONFIG.StartDostup)
//     else    // ������ �室 � ⥬ �� ������ - ��祣� �� �������
       if (!u4 && u1)     // �室 �� (��� != tlogin � Tlogin - ����)
       {  dst = *Dostup;
          if (nPort==2)
           logTimeb = BINTIME;//�६� ��砫� ᥠ�� = ⥪�饥 �६�
          else
           logTimeb1 = BINTIME;
       }
   }  //   if (attemp[k] < LATT)
  else
   {
     if (attemp[k] >= LATT) err |= 2; //if (attemp[k] >= LATT)
    // � ��娢 ������᭮�� - ����⪠ ����㯠 � ������ pas->nlogin
//printf("attemp[%d]=%d %X %s\n",k,attemp[k],err,Tlogin);
     if ((attemp[k] == LATT) && (StartDst !=3)) //�� ����⪨
    {
      strncpy((char*)MEMSECUR.login, (char*)pas->nlogin,4);
      MEMSECUR.TIMEBEG = BINTIME;
      memset(&MEMSECUR.TIMEEND,0,6);
      MEMSECUR.Port = nPort;
      MEMSECUR.mode  = 2;       // ��ᠭ�樮��஢���� �����
//      i = Mem_SecurWr_2(Mem_SecurGetPage(),0,nPort);
     }
     if (attemp[6] > LATT) attemp[6] = 0;
     if (attemp[0] >= LATT) attemp[0] = 0;
    } // else    if (attemp[k] < LATT)
//printf("dst=%d Dostup=%d err=%d attemp[%d]=%d\n",dst,Dostup,err,k,attemp[k]);

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
//printf("1-dst=%d\n",dst);
    if (dst > 0)                         // �஢��� ����㯠 > 0
    { strncpy((char*)Tlogin ,(char*)pas->nlogin,4);
      strncpy((char*)MEMSECUR.login, (char*)Tlogin,4);
      if (nPort == 2)
      MEMSECUR.TIMEBEG = logTimeb;
      else
      MEMSECUR.TIMEBEG = logTimeb1;
//printf("S-port=%d h1=%d m1=%d h=%d m=%d\n",nPort,logTimeb1.hours,logTimeb1.minutes,logTimeb.hours,logTimeb.minutes);
      memset(&MEMSECUR.TIMEEND,0,6);
      MEMSECUR.Port = nPort;
      MEMSECUR.mode  = 0;  // ��砫� ᥠ��
      i = Mem_SecurWr_2(Mem_SecurGetPage(),1,nPort);
      outportb(0x1D0,BASE_CFG_PAGE);

      if (nPort == 2)
      { PASSWR2[0]='\0';
        strcat(PASSWR2,Tlogin);
        strcat(PASSWR2,pas->npassw);
        PASSWR2[14]='\0'; PASSWR2[15]=0x0A;
//printf("pw2=%s\n",PASSWR2);
//        outportb(0x1D0,BASE_CFG_PAGE);
//        poke(SEGMENT,3624,di1p[1][0]); // �������
//        poke(SEGMENT,3626,di2p[1][0]); // �������
//        pENP = (char far *)MK_FP(SEGMENT,3614);  //������
//        _fmemcpy(pENP,&logTimeb,6);
      } else
      { PASSWR1[0]='\0';
        strcat(PASSWR1,(char*)Tlogin1);
        strcat(PASSWR1,(char*)pas->npassw);
        PASSWR1[14]='\0'; PASSWR1[15]=0x0A;
//printf("pw1=%s\n",PASSWR1);
//        outportb(0x1D0,BASE_CFG_PAGE);
//        poke(SEGMENT,3620,di1p[0][0]); // �������
//        poke(SEGMENT,3622,di2p[0][0]); // �������
//        pENP = (char far *)MK_FP(SEGMENT,3608);  //������
//        _fmemcpy(pENP,&logTimeb1,6);
      }
        for (i=0; i<6; i++)
        attemp[i] = 0;
//printf(PASSWR2);
     }
//printf("3-Dostup=%d dst=%d Tl=%s\0 nl=%s\n",*Dostup, dst, Tlogin, nlog);
// �室 �� ����⮬ ᥠ�� � ��㣨� ������, ⥪�騩 ᥠ�� ����뢠����
D2:      BufTrans[4] = dst;
      SaveMessagePrefix_2(BufTrans,7,0xD8);        // �뤠� �⢥� D8 dst
//printf("dst=%d pw2=%s\n",dst,PASSWR2);
//printf("pw1=%s\n",PASSWR1);

// ������ Tlogin � Dostup � ���
//      outportb(0x1D0,BASE_CFG_PAGE); //  ����� ��࠭���
//					// � ������� �� ���
//      pENP = (char far *)MK_FP(SEGMENT,16368);
//      if (nPort == 1)
//      { _fmemcpy(pENP,Tlogin,4);  //������
//        poke(SEGMENT,16372,*Dostup); // �������
//      }
//      else
//      { _fmemcpy(pENP+6,Tlogin,4);  //������
//        poke(SEGMENT,16378,*Dostup); // �������
//      }
    } // err ==0
}
*/
//-------------------------------------------------------------------------

void NoDostup1() {    // �⢥� "��� ����㯠"

      BufTrans1[4] = 8;
//      SaveMessagePrefix_2(BufTrans,7,0xD8);        // �訡�� ��p����p�� ���p��
      SaveMessagePrefix(7,0xD8,BufTrans1);
    CheckBufTrans(BufTrans1); //�������� ��
    Ini_Send1();  // �������� BufTrans
}

//---------------------------------------------------------------------------
/*
void GornalD_Wr(int numl)   //���������� ��ୠ�� ����㯠
{ int i,k,n;
  struct GornalDostup *pad,*pad1;

  n = numl;
  k=Mem_ArhivDRd(&numl); //�⥭�� ��娢� �� ���

   if (k < 10)
    { strncpy(ArhivDostup[k].login, Tlogin,4);
      ArhivDostup[k].TIMEBEG = logTimeb;
      ArhivDostup[k].TIMEEND = BINTIME;
      ArhivDostup[k].Port = 2;
    } else if (k==10)
    { for (i=0; i<9; i++)
      { pad = &ArhivDostup[i];
        pad1 = &ArhivDostup[i+1];
        movmem(pad1,pad,sizeof(struct GornalDostup));
      }
      k=9;
      strncpy(ArhivDostup[k].login, Tlogin,4);
      ArhivDostup[k].TIMEBEG = logTimeb;
      ArhivDostup[k].TIMEEND = BINTIME;
      ArhivDostup[k].Port = 2;
    }
     for (i=k+1; i<10; i++)
     {
      ArhivDostup[i].login[0]='\0';
      ArhivDostup[i].Port = 0;
     }
      Mem_ArhivDWr(n); //������ � ���
}
*/
//---------------------------------------------------------------------------
/*
void fendSeans() {    // ����� ᥠ�� �裡 �㭪�� 0x59
  fendSeansp(BufInKadr,BufTrans,Tlogin,2,&Dostup);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}

void fendSeansp(unsigned char* BufInKadr,unsigned char*  BufTrans, unsigned char* Tlogin, char nPort, unsigned int* Dostup)
{
   int    err = 0,num;
//   struct SendSeans *es;
   char *buf1="    \0";
   char Tlog[4];
   char *pENP;
   union resiv RESIV;

   if (BufInKadr[2] != 10)
      err |= 1;                         // ����p��� ����� ���p��
//    es = &RESIV.ENDSEANS;
    memcpy(Tlog,Tlogin,4);
//sprintf(str,"port=%d Tl=%s\n\r",nPort,Tlogin);
//ComPrint_2(str);
    if (memcmp(buf1,Tlogin,4))  // �᫨ ⥪��� ��� = ������
       {
         Mem_WrEndSeans_2(Tlogin,nPort);
         strncpy((char*)Tlogin ,"    \0",5);
//         outportb(0x1D0,BASE_CFG_PAGE); //  ����� ��࠭���  � ������� �� ���
//         pENP = (char far *)MK_FP(SEGMENT,16368);
         if (nPort == 1)
         { *Dostup=GLCONFIG.StartDostup1;
           memcpy(PASSWR1,GLCONFIG.PASSWORD,14);
//          _fmemcpy(pENP,Tlogin,4);  //������
////          _fmemcpy(pENP+4,&Dostup,2);
//           poke(SEGMENT,16372,*Dostup); // �������
         }
         else
         { *Dostup=GLCONFIG.StartDostup;
           memcpy(PASSWR2,GLCONFIG.PASSWORD,14);
//          _fmemcpy(pENP+6,Tlogin,4);  //������
//          _fmemcpy(pENP+10,&Dostup,2);
//           poke(SEGMENT,16378,*Dostup); // �������
         }
// ������ Tlogin � Dostup � ���
       }
    else err |= 2;
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
//      movmem(es->nlogin,&BufTrans[4],sizeof(struct SendSeans));
      movmem(Tlog,&BufTrans[4],4);
      SaveMessagePrefix_2(BufTrans,10,0xD9);
        }
}
//---------------------------------------------------------------------------
void fCreateUser() {    // ᮧ����� ���짮��⥫�� �㭪�� 0x56
  fCreateUserp(BufInKadr1,BufTrans,Tlogin,2,&Dostup);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}
void fCreateUserp(unsigned char* BufInKadr,unsigned char*  BufTrans, unsigned char* Tlogin, char nPort, unsigned int* Dostup)
{   int   k,i, err = 0;
   struct logpass *lg;

   if (BufInKadr[2] != 88)
      err |= 1;                         // ����p��� ����� ���p��
    lg = &RESIV.LOGPASS[0];
    movmem(&BufInKadr[4],&RESIV,5*sizeof(struct logpass));
    GLCONFIG.StartDostup1 = BufInKadr[84];
    GLCONFIG.StartDostup = BufInKadr[85];
    if (Mem_ConfigWr() != 0)
         err |= 2;
    Mem_LoginRd();
    for (i=0; i<5; i++)
    { lg = &RESIV.LOGPASS[i];
      if (lg->write > 0)
        {
	       strncpy((char*)MEMSECUR.login,(char*)lg->nlogin,4);
               MEMSECUR.mode  = 22;
	       MEMSECUR.TIMEBEG = BINTIME;
	       memset(&MEMSECUR.TIMEEND,0,6);
               MEMSECUR.Port = 2; // ����� ����
               k = Mem_SecurWr_2(Mem_SecurGetPage(),0,2);
          movmem(lg,&LogPassw[i+1],15);
        }
    }
   Mem_LoginWr(); //������ � ��� �� LogPassw
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SaveMessagePrefix_2(BufTrans,6,0xD6);
        }
}
*/
//---------------------------------------------------------------------------
/*
void fRdArchDost() {    // �⥭�� ��娢� �����᭮�� �㭪�� 0x57
   int   j,k,i, err = 0;
   struct  GornalDostup *ad;
   if (BufInKadr[2] != 8)
      err |= 1;                         // ����p��� ����� ���p��
      ad = &ArhivDostup[0];
      i = BufInKadr[5];     // ����� ���짮��⥫�
      k=Mem_ArhivDRd(&i);  // ��⠥� k ����ᥩ i-�� ���짮��⥫�
//printf("k=%d %s\n",k,ArhivDostup[0].login,ArhivDostup[0].Port);
      BufTrans[4] = BufInKadr[4];
      if ((k==0) || (k > 10))
      { BufTrans[5] = 0;
        BufTrans[6] = 0;
        SaveMessagePrefix(9,0xD7);
        return;
      }
      else
     {
      BufTrans[5] = k;
      if (i < 5)
       BufTrans[6] = 1;
      else BufTrans[6] = 0;
      j = sizeof(struct  GornalDostup);  // = 17
      movmem(ad,&BufTrans[7],k*j);
     }
         if (err)
      SaveMessagePrefix(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SaveMessagePrefix(j*k+9,0xD7);
        }
}
*/
//---------------------------------------------------------------------------
/*
void fUserRd() {    // �⥭�� ���짮��⥫�� �㭪�� 0x60
   int   i, err = 0;
   struct logpass *lg;
   char kD;

   if (BufInKadr[2] != 6)
      err |= 1;
                      // ����p��� ����� ���p��
     Mem_LoginRd(); //�⥭��� �� ��� � LogPassw
    movmem(&LogPassw[1],&BufTrans[4],5*sizeof(struct logpass));
    BufTrans[79] = GLCONFIG.StartDostup1;
    BufTrans[80] = GLCONFIG.StartDostup;
   if (err)
      SaveMessagePrefix(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SaveMessagePrefix(83,0xE0);
        }
}
//---------------------------------------------------------------------------
void RdSecur()  //�⥭�� ��p���� ������᭮��
{
portC=2;
RdSecurp(BufInKadr1,BufTrans,&STARTENDTIME);
   CheckBufTrans(BufTrans1); //�������� ��
   Ini_Send1();
}
void RdSecurp(unsigned char* BufInKadr,unsigned char*  BufTrans,  struct startendtime *STARTENDTIM)
{
  static  unsigned int   startpoint2,endpoint2;
  static  unsigned int   startpoint1,endpoint1;
  static unsigned int startpoint,endpoint;
  struct bintime      *pbtime;
  struct reqsecur     *preq;  //��p���p� ���p��
  unsigned char       num;
  unsigned char       page;
  struct securrecord  *prec;  //��p���p� ������ ��� ��p����
  unsigned int        point,Nrec;
  int                 count,sizepse,sizepser;
  struct responsesecur RESPONSESECUR;

  int err = 0;
  count = 0;
  point = 0;
  Nrec=13;
  sizepse = sizeof(struct reqsecur);
  sizepser = sizeof(struct securrecord);

  if(BufInKadr[2] - sizepse != 7) err |= 1;//����p��� ����� ���p��
  num = BufInKadr[4];
  if( (3 & num) > GLCONFIG.NumRun) err |= 2;//����p��� ����p� ��⪨
  preq = &RESIV.REQSECUR;
  prec = &RESPONSESECUR.SECURRECORD[0];
  movmem(&BufInKadr[5],preq,sizeof(struct reqsecur));//��p������� ��p����p�� ���p��
  if(preq->RequestNum != 0)
  { if( STARTENDTIM->RequestNum == preq->RequestNum)
    return;
    if( STARTENDTIM->RequestNum +1 != preq->RequestNum)  //����� �����
    err |= 0x04;  //����p��� ��᫥����⥫쭮�� ���p�ᮢ
  }
  STARTENDTIM->RequestNum = preq->RequestNum; //����� �����
  STARTENDTIM->num = 3 & (num-1);             //����� ��⪨

  if(STARTENDTIM->RequestNum == 0)  //��p���⪠ 1-�� ���p��
  { pbtime = &STARTENDTIM->STARTTIME;
    pbtime->seconds = preq->StartSeconds;
    pbtime->minutes = preq->StartMinutes;
    pbtime->hours   = preq->StartHours;
    pbtime->date    = preq->StartDay;
    pbtime->month   = preq->StartMonth;
    pbtime->year    = preq->StartYear;
    if( Mem_TestReqTime(pbtime) ) err |= 8;//����p��� �p����� ���p��
    pbtime = &STARTENDTIM->ENDTIME;
    pbtime->seconds = preq->EndSeconds;
    pbtime->minutes = preq->EndMinutes;
    pbtime->hours   = preq->EndHours;
    pbtime->date    = preq->EndDay;
    pbtime->month   = preq->EndMonth;
    pbtime->year    = preq->EndYear;
    if( Mem_TestReqTime(pbtime) ) err |= 0x10;//����p��� �p����� ���p��
    if( Mem_TestReqTimeSEp(STARTENDTIM)) err |= 0x20;//����p��� ᮮ⭮襭�� �p���� ���p��
//�������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
    if ( err == 0 )
    { page = Mem_SecurGetPage();//��p���� ��p���� ���p��
      startpoint  = Mem_SecurSearchStart2(page,STARTENDTIM);   // ��砫�� ������
//printf("page=%X startpoint=%d\n",page, startpoint);
                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
       outportb(0x1D0,page);
//       if (startpoint < ARCH_SECURITY) {  // ����� ����
//          int tInd,tPoint;

//          tInd = startpoint*LEN_SECURITY+BEGADDR_SECURITY;
//          BinTmp.year    = peekb(SEGMENT+0x10,tInd+9);
//          BinTmp.month   = peekb(SEGMENT+0x10,tInd+8);
//          BinTmp.date    = peekb(SEGMENT+0x10,tInd+7);
//          BinTmp.hours   = peekb(SEGMENT+0x10,tInd+6);
//          BinTmp.minutes = peekb(SEGMENT+0x10,tInd+5);
//          BinTmp.seconds = peekb(SEGMENT+0x10,tInd+4);

//          tPoint = peek(SEGMENT,BEGADDR_SECURITY);
//          tInd = tPoint*LEN_SECURITY+BEGADDR_SECURITY;
//          BinTmp2.year    = peekb(SEGMENT+0x10,tInd+9);
//          BinTmp2.month   = peekb(SEGMENT+0x10,tInd+8);
//          BinTmp2.date    = peekb(SEGMENT+0x10,tInd+7);
//          BinTmp2.hours   = peekb(SEGMENT+0x10,tInd+6);
//          BinTmp2.minutes = peekb(SEGMENT+0x10,tInd+5);
//          BinTmp2.seconds = peekb(SEGMENT+0x10,tInd+4);
//       }
//printf("T=%lu T2=%lu\n",BinToSec(BinTmp),BinToSec(BinTmp2));
                                       // ��������� < ��砫� ��娢�
//       if ((startpoint > ARCH_SECURITY) || (BinToSec(BinTmp) < BinToSec(BinTmp2)))
//          startpoint = Mem_SecurSearchStart(page,STARTENDTIM); // ��砫�� ������
//       else {
//          startpoint++;                // �த������ ��������� 㪠��⥫� �� 1
//          if (startpoint == ARCH_SECURITY)
//             startpoint = 0;
//       }
         if (portC==2) startpoint2 = startpoint;
         else          startpoint1 = startpoint;

        if( Mem_TestReqTimeEqp(STARTENDTIM) == 0) endpoint = startpoint;
        else endpoint = Mem_SecurSearchEnd(page,STARTENDTIM); //������ ������
        if (portC==2) endpoint2 = endpoint;
        else          endpoint1 = endpoint;

//printf("startpoint=%d endpoint=%d\n",startpoint, endpoint);
      if((startpoint > (ARCH_SECURITY-1)) || (endpoint > (ARCH_SECURITY-1))) err |= 0x40;
    }
    if ( err == 0 )
    { Mem_SecurRd(startpoint,page); //�⥭�� 1-� ����� ���� ���p��
//����p��� ᮮ⭮襭�� �p���� ���p�� � �p娢�
      if( Mem_SecurTestReqTimeSAER(STARTENDTIM) ) err |= 0x80;
      else
      if (Mem_SecurTestReqTimeSABR(STARTENDTIM))  err |= 0x80;
    }
  }//����� ��p���⪨ ��p���� ���p��

  if(err)
  { if( err & 0xC0 ) //��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
    { SaveMessagePrefix_2(BufTrans,9,0xE1);  //������ �p�䨪�
      BufTrans[4] = 3 & num;      //����p ��⪨
      BufTrans[5] = 0;            //���-�� ����ᥩ � �⢥�
      BufTrans[6] = 0;            //�p��������� ���
    }
    else  SaveMessagePrefix_2(BufTrans,6,0xFF);  //�訡�� ��p����p�� ���p��
  }
  else  //�訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
  {
    if (portC==2) {startpoint = startpoint2; endpoint = endpoint2;}
    else         { startpoint = startpoint1; endpoint = endpoint1;}
    page  = Mem_SecurGetPage();//��p���� ��p����
    point = Mem_SecurNextIndex(startpoint, Nrec*STARTENDTIM->RequestNum, page);
    count = 0;
    if( point < ARCH_SECURITY )  //���� ����� � ⠪�� ᬥ饭���
    { for(int i=0;i<Nrec;i++)
      { count++;  //�p������� ��p�������
        if(i)
        { point = Mem_SecurNextIndex(point,1,page);
          if(point > (ARCH_SECURITY-1)) break;     //����� ���᪠
        }
        Mem_SecurRd(point,page);    //�⥭�� ����� ����
        pbtime = &MEMSECUR.TIMEBEG;
        prec->StartMonth   = pbtime->month;     //�����
        prec->StartDay     = pbtime->date;      //����
        prec->StartYear    = pbtime->year;      //���
        prec->StartHours   = pbtime->hours;     //��
        prec->StartMinutes = pbtime->minutes;   //�����
        prec->StartSeconds = pbtime->seconds;   //ᥪ㭤�

        pbtime = &MEMSECUR.TIMEEND;
        prec->EndMonth   =  pbtime->month;
        prec->EndDay   =  pbtime->date;
        prec->EndYear    = pbtime->year;      //���
        prec->EndHours   = pbtime->hours;     //��
        prec->EndMinutes = pbtime->minutes;   //�����
        prec->EndSeconds = pbtime->seconds;   //ᥪ㭤�
        prec->ncomport      =  MEMSECUR.Port;
        prec->mode      =  MEMSECUR.mode;
        memcpy(prec->nlogin, MEMSECUR.login,4);
        prec++;
        if(point == endpoint) break;  //����� ���᪠
      }
    }
//printf("poin=%d count=%d\n",point,count);
      if (count > Nrec) count = 0;
      RESPONSESECUR.NumRecord = count; //���-�� ����ᥩ
      if((point == endpoint)||(count==0)||(point > (ARCH_SECURITY-1))) RESPONSESECUR.ResponseStatus = 0;  //����� �⢥� (0= �����襭�, 1= ���� �� �����)
      else RESPONSESECUR.ResponseStatus = 1;  //����� �⢥� (0= �����襭�, 1= ���� �� �����)
      SaveMessagePrefix_2(BufTrans,count*sizepser+9,0xE1);//������ �p�䨪�
      movmem(&RESPONSESECUR,&BufTrans[5],count*sizepser+2); //������ ���p�
      BufTrans[4] = 3 & num;                //����p ��⪨
  }
}
*/
//--------------------------------------------------------------
/*
void SD_MgnSend() {   // �뤠� ���������� ������ �� SD-�����
   long                  nbl;
   char                  count;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������

   prc = &RESIV.REQCYKLSD;
   if (!BufInKadr[5])   // ���� �����, ��砫�� ����
    { count = 0;
      nbl = Search_SD_blokmg(nRunSD, prc->StartYear,prc->StartMonth,
   prc->StartDay,prc->StartHours,prc->StartMinutes,prc->StartSeconds,
   prc->EndYear,prc->EndMonth,
   prc->EndDay,prc->EndHours,prc->EndMinutes,prc->EndSeconds);
//      nbl ������ ���� ࠢ�� -1 (ARCMBL), �᫨ �� ���� �� ������
      if ((Ble < 0) || (Ble >= ARCMBL)) Ble = nbl;
//printf("mgn-nbl=%ld Ble=%ld\n",nbl,Ble);
      if ((nbl >= 0)&&(nbl < ARCMBL))
       count = Read_SD_blokmg(nRunSD,nbl);
     }
   else
   { nbl = prc->RequestNum;      //�� ���� �����
     if (nbl <  (ARCMBL-1)) nbl++; else nbl=0;
     nbl = BlankBlok(nbl,nRunSD);
//      nbl = NextBlokmg(nbl);
     count = 0;
     if ((nbl >= 0)&&(nbl < ARCMBL))
      count = Read_SD_blokmg(nRunSD,nbl);  //count==0, �᫨ ��᫥���� ����, ���� count==1
   }
//printf("nbl=%ld c=%d\n",nbl,count);

   if ((nbl >= 0)&&(nbl < ARCMBL))
    {
     BufTrans[5] = count; *(long*)(BufTrans+6) = nbl;
     movmem(mdatar,&BufTrans[12],512);
     BufTrans[4] = 3 & (nRunSD+1);                //����p ��⪨

     SaveMessagePrefix(12,0xA0);     // ������ �p�䨪�
    }
   else
   {
         SaveMessagePrefix(9,0xA0);     // ������ �p�䨪�
         BufTrans[4] = 3 & (nRunSD+1);         // ����p ��⪨
         BufTrans[5] = 0;               // ���-�� ����ᥩ � �⢥�
         BufTrans[6] = 0;               // �p��������� ���
   }
   SDMgn2 = 0;
//   CheckBufTransIniSend();
    CheckBufTrans(BufTrans); //�������� ��
    Ini_Send1();  // �������� BufTrans

   TimeReq=0;// ��� ⠩��� ���� TimeReq=0;

//printf("%d ",counttime);
}
//---------------------------------------------------------------------------
void SD_RdCykl() {                    // �⥭�� 横���᪨� ������
   struct bintime        *pbintime;
   unsigned char         num; //,nRun;
   unsigned char         page,bSD;
   unsigned int          point;
   int                   err,nR,nRun;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������
   int                    sizepc, Nrec;

   err = 0;
//   sizepc = sizeof(struct cyklrecordSD);
 //  if (BufInKadr[2]-sizeof(struct reqcyklSD) != 7)
 //     err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

//   pcr = &SEND.RESPONSECYKLSD.CYKLRECORD[0];

   prc = &RESIV.REQCYKLSD;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr[6],prc,sizeof(struct reqcyklSD));
   if (err)
   {  SaveMessagePrefix(6,0xFF);     // �訡�� ��p����p�� ���p��
      return;
   }                                  // ����� �����
     nRunSD = 3 & (num-1);      // ����� ��⪨
     nRun = Time_GetNumrun();
   switch(GLCONFIG.NumRun)
   {
   case 1: bSD=((LongQueue[3]) ||  (counttime > 37));
        break;
   case 2: bSD=((LongQueue[3]) ||  (counttime > (20*nRun+17)));
        break;
   case 3: nR = (nRun==2) ? 1:nRun;
           bSD=((LongQueue[3]) ||  (counttime > (20*nR+17)));
        break;
   }
//printf("Mgn2=%d %d %d\n",counttime,nRun,bSD);
   if (bSD)
      SDMgn2 = 1;
    else
      InQueue(3,SD_MgnSend);
}
//------------------------------------------------------------
void SD_PerSend() {   // �⥭�� ��ਮ���᪨� ������ �� SD-�����
   long                  nbl;
   char                  count;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������


   prc = &RESIV.REQCYKLSD;
   if (!BufInKadr[5])   // ���� �����, ��砫�� ����
    { count = 0;
      nbl = Search_SD_blokP(nRunSD, prc->StartYear,prc->StartMonth,
   prc->StartDay,prc->StartHours,prc->StartMinutes,prc->StartSeconds,
   prc->EndYear,prc->EndMonth,
   prc->EndDay,prc->EndHours,prc->EndMinutes,prc->EndSeconds);
//      nbl ������ ���� ࠢ�� -1 (ARCPBL), �᫨ �� ���� �� ������
      if ((Ble < 0) || (Ble >= ARCPBL)) Ble = nbl;
//printf("per-nbl=%ld Ble=%ld\n",nbl,Ble);
      if ((nbl >= 0)&&(nbl < ARCPBL))
       count = Read_SD_blokP(nRunSD,nbl);
     }
   else
   { nbl = prc->RequestNum;      //�� ���� �����
     if (nbl <  (ARCPBL-1)) nbl++; else nbl=0;
     nbl = BlankBlokP(nbl,nRunSD);
     count = 0;
     if ((nbl >= 0)&&(nbl < ARCPBL))
      count = Read_SD_blokP(nRunSD,nbl);  //count==0, �᫨ ��᫥���� ����, ���� count==1
   }
//printf("nbl=%ld c=%d\n",nbl,count);

   if ((nbl >= 0)&&(nbl < ARCPBL))
    {
     BufTrans[5] = count; *(long*)(BufTrans+6) = nbl;
     movmem(mdatar,&BufTrans[12],512);
     BufTrans[4] = nRunSD+1;                //����p ��⪨
     SaveMessagePrefix(12,0xA1);     // ������ �p�䨪�
    }
   else
   {
         SaveMessagePrefix(9,0xA1);     // ������ �p�䨪�
         BufTrans[4] = nRunSD+1;         // ����p ��⪨
         BufTrans[5] = 0;               // ���-�� ����ᥩ � �⢥�
         BufTrans[6] = 0;               // �p��������� ���
   }
   SDPer2 = 0;
//   CheckBufTransIniSend();
   CheckBufTrans(BufTrans); //�������� ��
   Ini_Send1();  // �������� BufTrans

   TimeReq=0;// ��� ⠩��� ���� TimeReq=0;
}
//--------------------------------------------------------------
void SD_RdPeriod() {   // �⥭�� ��ਮ���᪨� ������ �� SD-�����
   struct bintime        *pbintime;
   unsigned char         num,nR,nRun;
   unsigned char         page,bSD;
   unsigned int          point;
   int                   err;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� ��ਮ���᪨� � ����� ������
//   struct periodrecordSD   *pcr;          // ��p���p� ����� ������ ��� ��p���� ��ਮ���᪨�

   int                    sizepc, Nrec;
   long                  nbl;
   char                  count;

   err = 0;

   num = BufInKadr[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

//   pcr = &SEND.RESPONSEPERSD.PER_RECORD[0];

   prc = &RESIV.REQCYKLSD;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr[6],prc,sizeof(struct reqcyklSD));
   if (err)
   {  SaveMessagePrefix(6,0xFF);     // �訡�� ��p����p�� ���p��
      return;
   }                                  // ����� �����
     nRunSD = 3 & (num-1);      // ����� ��⪨
     nRun = Time_GetNumrun();
   switch(GLCONFIG.NumRun)
   {
   case 1: bSD=((LongQueue[3]) ||  (counttime > 35));
        break;
   case 2: bSD=((LongQueue[3]) ||  (counttime > (20*nRun+15)));
        break;
   case 3: nR = (nRun==2) ? 1:nR;
           bSD=((LongQueue[3]) ||  (counttime > (20*nR+15)));
        break;
   }
//printf("Per2=%d %d %d\n",counttime,nRun,bSD);
   if (bSD)
      SDPer2 = 1;
    else
      InQueue(3,SD_PerSend);
}
//--------------------------------------------------------------
*/
void SetTime()  //��⠭���� �p�����
{
    SetTimep(BufInKadr1,BufTrans1,2);
    CheckBufTrans(BufTrans1); //�������� ��
    Ini_Send1();
}
void SetTimep(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{ struct settime    *psettime;
  struct bintime    *pbintime;
  unsigned char     *pchar;
  extern struct bintime TIME_SETTIME;  //�ᯮ������ �p� ��⠭���� �p�����
  int i;
  int   err = 0;

//if (Nwr1 == 1) err |= 0x80;           // 䫠� ����� �������

  if(BufInKadr[2]-22 != sizeof(struct settime)) err |= 1;  //����p��� ����� ���p��
/*   if (nPort==2)
   { if (memcmp(&BufInKadr[4],PASSWR2,14))  err |= 4;}
   else
   { if (memcmp(&BufInKadr[4],PASSWR1,14))  err |= 4;}
*/
  if(err) SaveMessagePrefix_2(BufTrans,6,0xFF);            //�訡�� �室��� ��p����p��
  else
  {
   memmove(&RESIV,&BufInKadr[20],sizeof(struct settime)); //������ ��p����p��
    psettime = &RESIV.SETTIME;
    pbintime = &TIME_SETTIME;
    pbintime->month   = psettime->Month;          //�����
    pbintime->date    = psettime->Day;            //����
    pbintime->year    = psettime->Year;           //���
    pbintime->hours   = psettime->Hours;          //��
    pbintime->minutes = psettime->Minutes;        //�����
    pbintime->seconds = psettime->Seconds;        //ᥪ㭤�
    Time_SetFlagTime();     // ��⠭���� 䫠�� ��⠭���� �p�����
          MEMAUDIT.type = 128;  // �p��� ���᫨⥫�
          pchar = (unsigned char*)&MEMAUDIT.altvalue;
          pchar[0] = BINTIME.hours;
          pchar[1] = BINTIME.minutes;
          pchar[2] = BINTIME.seconds;
          pchar[3] = 0;
          pchar[4] = pbintime->hours;
          pchar[5] = pbintime->minutes;
          pchar[6] = pbintime->seconds;
          pchar[7] = 0;
 for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
    Mem_AuditWr(i,MEMAUDIT);

    if((BINTIME.month!=pbintime->month)||(BINTIME.date!=pbintime->date)||(BINTIME.year!=pbintime->year))
    {     MEMAUDIT.type = 129;  //��� ���᫨⥫�
          pchar = (unsigned char*)&MEMAUDIT.altvalue;
          pchar[0] = BINTIME.month;
          pchar[1] = BINTIME.date;
          pchar[2] = BINTIME.year;
          pchar[3] = 0;
          pchar[4] = pbintime->month;
          pchar[5] = pbintime->date;
          pchar[6] = pbintime->year;
          pchar[7] = 0;
 for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
    Mem_AuditWr(i,MEMAUDIT);
    }
    SaveMessagePrefix_2(BufTrans,6,0xC2);  //���䨣�p��� ����ᠭ�
  }
}
//---------------------------------------------------------------------------
void Time_SetFlagTime() {               // ��������� ����� ��������� �p�����

   FlagTime     = 0xAAAA;
   FlagTimeCopy = 0x5555;
}
//---------------------------------------------------------------------------
void Time_ResFlagTime() {               // ��p�� ����� ��������� �p�����

   FlagTime     = 0;
   FlagTimeCopy = 0;
}
//---------------------------------------------------------------------------
uint8_t Time_GetFlagTime() {
   uint16_t FlagCmp;

   FlagCmp = ~FlagTime;

   if (FlagCmp == FlagTimeCopy)
      return 1;                         // ��������� �p����� ���������
   else
      return 0;                         // ��������� �p����� ���������
}
              // ��� �㬬�p������ ������ �ᮢ��

//---------------------------------------------------------------------------
