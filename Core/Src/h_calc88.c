//////////////////////////////////////////////////////////////////
// ������ ������� ����������� �� ������ GERG-88
//////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <mem.h>
//#include <dos.h>
//#include <time.h>
#include "h_main.h"
#include "h_calc.h"
#include "h_analog.h"
#include "h_mem.h"
#include "h_smooth.h"

extern struct bintime   BINTIME;        // �������� ��p����p� �p�����
extern struct glconfig  GLCONFIG;
extern struct AddCfg    ADCONFIG;
extern struct IdRun CONFIG_IDRUN[3]; // ���䨣���� �� ��⪠�
extern struct AdRun CONFIG_ADRUN[3];               //120*3=360

extern unsigned int  counttime;         // ���-�� ������ ��� ���������� �p�����
//---------------------------------------------
extern struct calcdata     CALCDATA[3];
extern char Zc1[],CalcZc[];
extern char Z_in[];
extern unsigned int GergErr;
//extern char Calc59Mgn, Calc1Per,Calc5Conf;
extern int calcerr;
extern char ReErr[3], HeatErr[3];              // ��������� ������ �� ������� � ����������
extern float KoRo[3][3];
//extern float RLair;
//-----------------------------------------------------------------
float T_st8;  //,P_st;    T_st = 293.15;                       // 20 ����. �������

int K,KK,CZc;
float N2_G88;
//char u1,u2;
struct calcdata* uk;
float BR11H0[3]={-0.425468, 0.2865e-2, -0.462073e-5};
float BR11H1[3]={0.877118e-3, -.556281e-5, 0.88151e-8};
float BR11H2[3]={-.824747e-6, 0.431436e-8, -.608319e-11};
float BR22[3]={-.1446 , 0.740910e-3, -.91195e-6};
float BR23[3]={-.339693 , 0.161176e-2, -.204429e-5};
float BR33[3]={-.86834 , 0.40376e-2, -.51657e-5};
float BR15[3]={-.52128e-1, 0.27157e-3, -.25e-6};
float BR17[3]={-.68729e-1, -.239381e-5, 0.518195e-6};
float BR55[3]={-.110596e-2, 0.813385e-4, -.98722e-7};
float BR77[3]={-.13082 , 0.60254e-3, -.6443e-6};
float B25 = 0.012;
float CR111H0[3]={-.302488 , 0.195861e-2, -.316302e-5};
float CR111H1[3]={0.646422e-3, -.422876e-5, 0.688157e-8};
float CR111H2[3]={-.332805e-6, 0.22316e-8, -.367713e-11};
float CR222[3]={0.78498e-2, -.39895e-4, 0.61187e-7};
float CR223[3]={0.552066e-2, -.168609e-4, 0.157169e-7};
float CR233[3]={0.358783e-2, 0.806674e-5, -.325798e-7};
float CR333[3]={0.20513e-2, 0.34888e-4, -.83703e-7};
float CR555[3]={0.104711e-2, -.364887e-5, .467095e-8};
float CR117[3]={0.736748e-2, -.276578e-4, .343051e-7};
float Z12 = 0.72, Z13 = -.865;
float Y12 = 0.92, Y13 = 0.92, Y123 =1.1, Y115 = 1.2;
float GM1R0= -2.709328, GM1R1= .021062199;
float GM2 = 28.0135, GM3 = 44.01;
float GM5 = 2.0159, GM7 = 28.01;
float FA = 22.414097;  //, FB = 22.710811;
float RL = 1.292923, TO; // = 273.15;   //RL=1.292923 ��� 0 �� �, =1.20445 ��� 20
float H5 = 285.83, H7 = 282.98;
float R = 0.0831451;
float AMOL,HS,RM,H;
float X1,X2,X3,X5,X7,X11,X12,X13,X22,X23,X33,X15,X17,X25,X55,X77;
float Z, D, V, P8, TC;
float SM,BEFF,SMT1,SMT2,DH,B11;
int sch;
unsigned int    *pbuferr;
/*
// X1  - ���������� CH ����������� �� ���������
// X2  - ���������� N2  ����������� �� ���������
// X3  - ���������� CO2  ��������
// X5  - ���������� H2   ��������
*/

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
void Task_SGerg_L1(void);
void Task_SGerg_L2(void);
void Task_SGerg_L3(void);
void Task_SGerg_L4(void);
//-------------------------------------------------------
float SMBER(float H)
{
  float GM1,SM1;
  GM1= GM1R0+GM1R1*H;
  X1 = (HS-(X5*H5+X7*H7)*AMOL)/H/AMOL;
  X2 = 1.0f-X1-X3-X5-X7;
  SM1 = (X1 * GM1+X2 * GM2 +X3 * GM3 +X5 * GM5 +X7 *GM7)*AMOL ;
//printf("HS=%f X5=%f X7=%f SM1=%f\n",HS,X5,X7,AMOL);
  return SM1;
}
//-----------------------------------------
float B11BER(float T,float H)
{
//COMMON /BBLOK/ BR11H0(3), BR11H1(3), BR11H2(3), BR22(3), BR23(3)
//  BR33(3), BR15(3), BR17(3), BR55(3), BR77(3), B25
float T2,B11a;

  T2=T*T;
  B11a=BR11H0[0] + BR11H0[1] *T + BR11H0[2]*T2
  +(BR11H1[0] + BR11H1[1]*T + BR11H1[2]*T2)*H
  +(BR11H2[0] + BR11H2[1]*T + BR11H2[2]*T2)*H*H;
  return  B11a;
}
//------------------------------------------------
float BBER(float T1,float B11a)
{
//COMMON /BBLOK/ BR11H0 (3.) , BR11H1(3), BR11H2(3), BR22(3),BR23(3),
// BR33(3), BR15(3), BR17(3), BR55(3), BR77(3), B25
//COMMON/ZETA/ Z12, Z13,Y12,Y13,Y123,Y115
//COMMON /XBLOK/ X1,X2,X3,X11,X12,X13,X22,X23,X33
//X5,X7,X15,X17,X25,X55,X77
  float B22,B23,B33,B15,B55,B17,B77,BA13,ZZZ;

  float T2=T1*T1;
  B22=BR22[0] + BR22[1]*T1 + BR22[2]*T2;
  B23=BR23[0] + BR23[1]*T1 + BR23[2]*T2;
  B33=BR33[0] + BR33[1]*T1 + BR33[2]*T2;
  B15=BR15[0] + BR15[1]*T1 + BR15[2]*T2;
  B55=BR55[0] + BR55[1]*T1 + BR55[2]*T2;
  B17=BR17[0] + BR17[1]*T1 + BR17[2]*T2;
  B77=BR77[0] + BR77[1]*T1 + BR77[2]*T2;
  BA13= B11a*B33;
  if (BA13 < 0) calcerr = 5; //printf("STOP 1 NO SOLUTION'\n");
  ZZZ = Z12+(320-T1)*(320-T1)*1.875e-5;
  float BEFF1 = X11*B11a + X12*ZZZ*(B11a+B22) + 2.0*X13*Z13*sqrt(BA13)
  + X22*B22 + 2.0*X23*B23 + X33*B33 + X55*B55
  + 2.0*X15*B15 + 2.0*X25*B25 + 2.0*X17*B17 + X77*B77;
//  printf("X25=%f B25=%f X17=%f B17=%f X77=%f B77=%f\n",Z,X25,B25,X17,B17,X77,B77);

  return BEFF1;
}
// *************************************************************************************************
float CBER(float T,float H)
{
//COMMON /CBLOK/ CR111H0(3), CR111H1(3), CR111H2(3), CR222(3),
// CR223(3) , CR233(3), CR333(3), CR555{3), CR117{3)
//COMMON /ZETA/ Z12,Z13,Y12,Y13,Y123,Y115
//COMMON /XBLOK/ X1,X2,��,Xll,X12,X13,X22,X23,X33
//  ,X5,X7,X15,X17,X25,X55,X77
float C111,C222,C223,C233,C333, C555,C117,CA112,C113,CA122,CA123;
float CA133,CA115, CA113;

  float T2=T*T;
  C111=CR111H0[0] + CR111H0[1]*T + CR111H0[2]*T2
 +(CR111H1[0] + CR111H1[1]*T + CR111H1[2]*T2)*H
 +(CR111H2[0] + CR111H2[1]*T + CR111H2[2]*T2)*H*H;
  C222 = CR222[0] + CR222[1]*T + CR222[2]*T2;
  C223 = CR223[0] + CR223[1]*T + CR223[2]*T2;
  C233 = CR233[0] + CR233[1]*T + CR233[2]*T2;
  C333 = CR333[0] + CR333[1]*T + CR333[2]*T2;
  C555 = CR555[0] + CR555[1]*T + CR555[2]*T2;
  C117 = CR117[0] + CR117[1]*T + CR117[2]*T2;
  CA112=C111*C111*C222;
  CA113=C111*C111*C333;
  CA122=C111*C222 *C222;
  CA123=C111*C222*C333;
  CA133=C111*C333*C333;
  CA115=C111*C111*C555;
  if ((CA112 < 0.0) || (CA113<0.0) || (CA122<0.0) ||
    (CA123<0.0) || (CA133<0.0) || (CA115 < 0.0))
    calcerr= 4; //printf(" STOP ' NO SOLUTION'\n");
  float D3REP=1.0/3.0;
  float CEFF1=X1*X11*C111 +3.0*X11*X2*pow(CA112,D3REP)*(Y12+(T-270.0)*0.0013)
  + 3.0*X11*X3 *pow(CA113,D3REP) *Y13
  + 3.0*X1*X15 *pow(CA115,D3REP) *Y115
  + 3.0*X1*X22 *pow(CA122,D3REP) *(Y12+(T-270.0)*0.0013)
  + 6.0*X1*X2*X3*pow(CA123,D3REP) *Y123
  + 3.0*X1*X33 *pow(CA133,D3REP) *Y13
  + X22*X2*C222 + 3.0*X22*X3*C223 + 3.0*X2*X33*C233
  + X3*X33*C333 + X5*X55*C555 + 3.0*X11*X7*C117;
//  printf("X5=%f X55=%f C555=%f X11=%f X7=%f C117=%f\n",Z,X5,X55,C555, X11, X7, C117);
  return CEFF1;
}
//---------------------------------
void ITER(float P, float T, float B, float C)
{
// float GM1R0, GM1R1, G�2, GM3, GM5, GM7, FA, FB, RL, H5, H7, R;
  float PA,RT,RTP;
//printf("R=%f T=%f P=%f B=%f\n",R,T,P,B);
   RT = R*T;
   RTP= RT/P;
   V = RTP+B;
//printf("RTP=%f V=%f\n",RTP,V);
  int KK = 0;
 L5: V = RTP*(1.0+B/V+C/(V*V));
//printf("V=%f ",V);
  KK = KK+1;
  if (KK > 20) {calcerr= 3; goto L6;} //{printf("STOP  NO CONVERGENCY #3\n"); goto L6;}

  Z = 1.0+B/V+C/(V*V);
  PA = RT/V*Z;
//printf("PA=%f Z=%f P=%f\n",PA,Z,P);
  if (fabs(PA-P) >= 1.0e-5) goto L5;
 L6: //printf("KI=%d\n",KK);
	return;
}
//-------------------------------------------
//void SGERG1(float P,float TC)
void Task_SGERG1(void)
{

//  HS = QM;
//  X3 = Q3;
//  X5 = Q5;
//  if ((RM<0.55)||(RM>0.9)) printf("����� �� �������� �� ���������\n");
//  if ((X3< 0.0)||(X3> 0.30)) printf("����� �� �������� �� CO2\n");
//  if ((HS< 20.0) || (HS>48.0)) printf("����� �� �������� �� ��.�������\n");
//  if ((0.55+0.97*X3-0.45*X5)>RM) printf("�������� ����������� ������\n");
//  printf("CO2=%f H2=%f RoM=%f\n",X3,X5,RM);
  X7 = X5*0.0964;
  X33 = X3*X3;
  X55 = X5*X5;
  X77 = X7*X7;
  BEFF= -0.065;
  H = 1000.0;
  AMOL= 1.0/(FA+BEFF);
  K = 0;
  KK = 0;
//     Task_SGerg_L1(void);
}
//-------------------------------------------
void Task_SGerg_L1(void)
{int nRun;

 //  nRun = Time_GetNumrun(void);
//printf("%d H=%.3f DH=%.6f\n",nRun,H,DH);                       // X7 = X5*0.0964;
 L1: SMT1=SMBER(H);   //X1 = (HS-(X5(%H2)*H5+X7*H7)*AMOL)/H/AMOL;  X2 = 1.0-X1-X3-X5-X7
  if (fabs(SM-SMT1)>1.0e-6)
  { SMT2=SMBER(H+1.0);  //��������� X2 = 1.0-X1-X3-X5-X7;
    DH= (SM-SMT1)/(SMT2-SMT1);
    H = H+DH;
//printf("SMT1=%f SMT2=%f DH=%f H=%f\n",SMT1,SMT2, DH, H);
    KK = KK+1;
    if (KK>20) {calcerr = 2; goto L2;}//printf("���������� ������� ����. ����������-1\n");
    goto L1;
  }
//printf("KK=%d\n",KK);
L2:
//printf("SM=%f SMT1=%f SMT2=%f calcerr=%d\n",SM, SMT1, SMT2,calcerr);
/*     if (calcerr == 0)
     { if (sch == 1)
         Task_SGerg_L2(void);
       else
         Task_SGerg_L2(void);
     }
    else  // ���� ������
    { if (sch == 1)
         Task_Calc_CorrEnd(void);
      else
         TaskCalcEnd(void);
//printf("L2 err=%d TO=%f\n",calcerr,TO);
//      nRun = Time_GetNumrun(void);
//      pbuferr = Calc_GetPointBuferr(nRun); // �������� ��p�� ����p�
//      int err = (calcerr) ? 0x93 : 0x13;       // ������ p������ ����. �����������
//      if (err != pbuferr[26]) {
//       Mem_AlarmWrCodRun(nRun,err,Mem_GetDeyQ(nRun));
//      pbuferr[26] = err; //�p������� ����� ��������
//     }
    }
*/
return;
}
//---------------------------
void Task_SGerg_L2(void)
{ int nRun;

  X11 = X1*X1 ;
  X12 = X1*X2;
  X13 = X1*X3;
  X22 = X2*X2;
  X23 = X2*X3;
  X25 = X2*X5;
  X15 = X1*X5;
  X17 = X1*X7;
  B11 = B11BER(TO,H);
  BEFF = BBER(TO,B11);
  if   ((FA+BEFF)>0)
  AMOL = 1.0/(FA+BEFF);
  else  AMOL=1.0;
  float HSBER = X1*H*AMOL+(X5*H5+X7*H7)*AMOL;
  if (fabs(HS-HSBER)>1.0e-5)
  {
   K = K+1;
   if (K>20) {calcerr=1; goto L3;}//printf("���������� ������� ����. ����������-2\n");
//   goto L1;
     Task_SGerg_L1(); return;
  }
//printf("K=%d N2=%f\n",K,X2);
//  if ((X2<-0.01)||(X2>0.5)) printf("����� �� �������� �� N2\n");
//  if ((X2+X3)>0.5) printf("����� �� �������� �� N2+CO2\n");
L3:
//     printf("�������� �� ���������� �������������� �� ������\n");
//printf("L3 err=%d\n",calcerr);
//     if (calcerr == 0)
//     { if (sch == 1)
//         Task_SGerg_L3(void);
//       else
//         Task_SGerg_L3(void);
//     }
//    else  // ���� ������
//    { if (sch == 1)
//         Task_Calc_CorrEnd(void);
//      else
//         TaskCalcEnd(void);
//printf("L3 err=%d TO=%f\n",calcerr,TO);
//      nRun = Time_GetNumrun(void);
//      pbuferr = Calc_GetPointBuferr(nRun); // �������� ��p�� ����p�
//      int err = (calcerr) ? 0x93 : 0x13;       // ������ p������ ����. �����������
//      if (err != pbuferr[26]) {
//       Mem_AlarmWrCodRun(nRun,err,Mem_GetDeyQ(nRun));
//       pbuferr[26] = err; //�p������� ����� ��������
//      }

//    }
return;
}
//--------------------------------------------
void Task_SGerg_L3(void)
{ int nRun, err;

  float T = TC+273.15;  // �K, TO=273.15
  if ((0.55 + 0.4*X2 + 0.97*X3-0.45*X5)>RM) calcerr = 7;
//printf("X2=%f X3=%f X5=%f RM=%f\n",X2,X3, X5, RM);
   B11 = B11BER(T,H);
   BEFF = BBER(T,B11);
   float CEFF = CBER(T,H);
//printf("B=%f C=%f T=%f count=%d\n",BEFF,CEFF,T,counttime);

   ITER(P8,T,BEFF,CEFF);
  D = 1.0/V;
//  N2_G88 = X2;
//printf("ITER L3 err=%d\n",calcerr);
//printf("Z=%f\n",Z);
//     if ((calcerr == 0) || (calcerr == 7))
//     {
//         Task_SGerg_L4(void);
//     }
//    else  // ���� ������
//    { if (sch == 1)
//         Task_Calc_CorrEnd(void);
//      else
//         TaskCalcEnd(void);
//printf("ITER err=%d TO=%f\n",calcerr,TO);
/*      nRun = Time_GetNumrun(void);
      pbuferr = Calc_GetPointBuferr(nRun); // �������� ��p�� ����p�
      err =  (calcerr) ? 0x93 : 0x13;       // ������ p������ ����. �����������
      if (err != pbuferr[26]) {
       Mem_AlarmWrCodRun(nRun,err,Mem_GetDeyQ(nRun));
       pbuferr[26] = err; //�p������� ����� ��������
      }
*/
//    }
}
//------------------------------------
int TaskCalc_SGERG88(void)
{
   struct IdRun    *prun;               // ��������� �� ��p����p� �����
   int nRun,ret;

   nRun = Time_GetNumrun();
   uk = &CALCDATA[nRun];                // ������ �������� ����p� �����
   prun  = &CONFIG_IDRUN[nRun];       // ������� ����p �����
   sch = ((prun->TypeRun == 4) || (prun->TypeRun == 5)
         || (prun->TypeRun == 10));
 T_st8 = 273.15;
 TO= T_st8;
 calcerr = 0;
  ret = 0;
	X3=  0.01*uk->NCO2;// ���������� CO2
 HS=  prun->Heat*KoRo[GLCONFIG.EDIZM_T.T_SU][1];// ��. ������� �������� ������
//printf("nRun=%d Heat=%f HS=%f\n",nRun ,prun->Heat,HS);
/*
 if ((HS<20.0) || (HS > 48.0))
 {
      if (HeatErr[nRun] == 0) {         // ���� ������ ������,
         Mem_AlarmWrCodRun(nRun,0x8B,Mem_GetDeyQ(nRun)); //�� ������� ��������
         HeatErr[nRun] = 1;
      }
 }
 else
 {
      if (HeatErr[nRun] == 1) {         // ���� ������ ����� ����� �������,
         Mem_AlarmWrCodRun(nRun,0x0B,Mem_GetDeyQ(nRun));
         HeatErr[nRun] = 0;
      }
 }
*/
 if (HS<20.0)       // ������ �� ������������� �����
  HS=20.0;
 else if (HS > 48.0)
  HS= 48.0;
/* if (fabs(TO - 273.15)<1e-4) RL=1.292923;   //t=0 ��.
 else if (fabs(TO - 288.15)<1e-4) RL=1.225410;  // 15 ��
 else  RL=1.204713; // RL=1.20445;   //    20 ��
*/
 RL=1.292923;
  SM =  uk->ROnom*KoRo[GLCONFIG.EDIZM_T.T_SU][1];  //������� � 0 �� �
// if (prun->typeDens == 0)
   RM=  SM/RL; // RL=1.292923 ��� 0 ��.  1.20468;   // ������������� ���������
// else
//   RM = SM;
 X5=  0.01*uk->NN2;   // ���������� H2
//printf("Zc1=%d CalcZc=%d\n",Zc1[nRun],CalcZc[nRun]);
 if ((CalcZc[nRun]) || (Zc1[nRun]))
 {
  TC=T_st8-273.15;
  P8=1.01325;
  CZc=1;
//  Z_in[nRun] = 0;
  CalcZc[nRun] = 0;
  }
  else
  {
   P8 =   uk->Pa*0.9806652;   // ������� �������� ���
   TC =  uk->t;   // ������� ����������� ��
   CZc=0;
  }
//printf("nRun=%d P=%6.3f T=%7.3f  RM=%7.5f HS=%7.3f CO2=%6.4f\n",nRun,P,TC,RM,HS,X3);
//printf("c1=%d\n",counttime);
//   if ((P<1.0)||(P>120.1)) calcerr=1; //printf("����� �� �������� �� P\n");
//   if ((TC<-23.0)||(TC>65.0)) calcerr=2; //printf("����� �� �������� �� T\n");
//printf("CalcZc[%d]=%d Zc1=%d\n",nRun,CalcZc[nRun],Zc1[nRun]);
       if (uk->Pa < 0.1)
          goto L4;
//           Task_Calc_CorrEnd(void);
       else
         Task_SGERG1();
       Task_SGerg_L1();
     if (calcerr == 0)
     {
         Task_SGerg_L2();
     }
     else  // ���� ������
     {
         goto L4;
//printf("L2 err=%d TO=%f\n",calcerr,TO);
     }
     if (calcerr == 0)
     {
         Task_SGerg_L3();
     }
    else  // ���� ������
    { goto L4;
//printf("L3 err=%d TO=%f\n",calcerr,TO);
    }
     if ((calcerr == 0) || (calcerr == 7))
     {
         Task_SGerg_L4();
     }
    else  // ���� ������
    {  goto L4;
    }
//printf("L4 err=%d TO=%f CZc=%d\n",calcerr,TO,CZc);
//����������� ������� ������� � ������
//printf("P8=%f\n",P8);
   
		if ((P8 - 1.01325) < 1e-5)  goto L8;   //��� ������� Zc
    
/*      if (sch == 1)
         Task_Calc_CorrQnom();
       else
         TaskCalc_RO();
*/
goto L8;
// ����� �� �������
L4://  printf("End calcerr=%d\n",calcerr);
 ret = 1;
/* if (sch==0)
        TaskCalcEnd();
     else
        Task_Calc_CorrEnd();
*/
L8:

return ret;
}
//------------------------------_____________
void Task_SGerg_L4(void)
{  int nRun,err;
   float Ks;

   nRun = Time_GetNumrun();
/*      pbuferr = Calc_GetPointBuferr(nRun); // �������� ��p�� ����p�
      err = (calcerr==7) ? 0x293 : 0x13;       // ������ p������ ����. �����������
      if (err != pbuferr[26]) {
       Mem_AlarmWrCodRun(nRun,err,Mem_GetDeyQ(nRun));
       pbuferr[26] = err; //�p������� ����� ��������
      }
*/
   Ks = uk->K;
//printf("Z=%f CZc=%d Zc1=%d\n",Z,CZc,Zc1[nRun]);
   if (CZc)
   {
     uk->Zc = Z;
     if (Zc1[nRun])
       Zc1[nRun] = 0;
/*     { Ks=1.0;
       uk->K = Ks;
       uk->Z = Z;
     }
*/
     if (CalcZc[nRun])
       CalcZc[nRun] = 0;
     CZc = 0;
//printf(" Zc=%f Ks=%f\n",Z,Ks);
   }
   else
   { uk->Z = Z;
     if (uk->Zc > 0.1)
      Ks = uk->Z / uk->Zc;
     else Ks=1;
     uk->Kgerg = Ks;
     uk->K = Ks;
//printf("G88 Zc=%f Ks=%f  ukZ=%f\n",uk->Zc, uk->K, uk->Z);
   }
//printf("G88  Ks=%f  ukZ=%f nRun=%d c=%d\n",Ks, uk->Z,nRun,counttime);
//     { if (sch == 1)
//         Task_Calc_CorrQnom(void);
//       else
//         TaskCalc_RO(void);
//     }
}
