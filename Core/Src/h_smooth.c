/////////////////////////////////////////////////////////////////
// ����������� ������� �� ��������.
/////////////////////////////////////////////////////////////////
//#include <malloc.h>
#include "h_analog.h"
#include "h_mem.h"
#include "h_smooth.h"
#include "h_calc.h"

extern struct glconfig  GLCONFIG;
extern struct IdRun CONFIG_IDRUN[3];

extern struct calcdata  CALCDATA[];
extern int SysTakt; //���� ���������� 1 ��� 2 ���
//extern unsigned int  counttime;         // ���-�� ������ ��� ���������� �p�����
extern long QCurrSumm[];                // ������� ����� ������
extern long CurrSumm[];                 // ������� ����� ������ ��� �����������
extern unsigned char BufLen;            // ����� ������ ��������� �� ��� (�� ���������-20)
//extern unsigned char BufShift;          // c���� ����������� ������ ��������� �� ��� (�� ���������-20)
extern unsigned char CountUse;          // ������ ������������� ���������
extern int PulseTakt;                   // ����� ����� ������ ��������� (0-255)
extern unsigned int Interval[];
extern unsigned  int val2;
extern float  Kki;
extern double IndDP[],IndQ[],IndQs[];
extern char  RS485HF[];
extern int counttime;
//---------------------------------------------------------------------
struct new_count Cnt[3];                // ������ ������ ��������

//---------------------------------------------------------------------
void Disp_count_0(unsigned char Run) {  // ��������� ��������� �������� �����
   struct IdRun *prun;
   int i;

   prun = &CONFIG_IDRUN[Run];

   Cnt[Run].tcas = 3600;

   if (prun->Qmin != 0.0)
      Cnt[Run].interval_min =           // �������� ��� ������������ �������
         (int)(prun->NumTurbo / prun->Qmin * (float)Cnt[Run].tcas);
   else
      Cnt[Run].interval_min = 600;

   Cnt[Run].interval_max = 200;         // �������� ��� ������������� �������


   Cnt[Run].dem = 0.0;                  // �-� �������������

   Cnt[Run].Kimp_n = 0;                 // �-�� ��������� �� ������� �����
   Cnt[Run].Kimp_n1 = 0;                // ���������� ��������� �-�� ���������
   Cnt[Run].Tn = 0;                     // ����, ��������������� �imp_n
   Cnt[Run].Tn1 = 0;                    // ����, ��������������� �imp_n1

   for (i=0; i<QBUFLEN; i++) {
      Cnt[Run].AKimp_n[i] = 0;          // �-�� ��������� �� ������� �����
      Cnt[Run].ATn[i] = 0;              // ����, ��������������� �imp_n
   }

   Cnt[Run].RBLen = 0;
   Cnt[Run].ImpSumm = 0;
   Cnt[Run].TimeSumm = 0;

   Cnt[Run].pr1ob = 1;                  // ������� ������� ���������
   Cnt[Run].pr1ob_0 = 1;                // ������� ������� ���������

                                        // �������� ����� ������� � ��������� �-��� ���������
   Cnt[Run].interval = Cnt[Run].interval_min;
   Cnt[Run].interval_1 = 0.0;           // ����������� ��������

   Cnt[Run].j = 0;                      // ������� �������� ���������
   Cnt[Run].jj = 0;                     // ������� ���������� �������� ���������
}

//-------------------------------------------------------------------------------
void Disp_count_1(unsigned char Run) {  // ����������� �������� ����� ��� ���������� �� ��������
   struct IdRun *prun;

   prun = &CONFIG_IDRUN[Run];

   if (prun->Qmin != 0.0)
      Cnt[Run].interval_min =
         (int)(prun->NumTurbo / prun->Qmin * (float)Cnt[Run].tcas);
   else
      Cnt[Run].interval_min = 600;
}

//----------------------------------------------------------------------
void RashCalc(unsigned char Run, float Kimp_r) {      // ������ ������� �������� �����
   struct IdRun *prun;
   int i,Buflen; //,kn,km;
   float Kpr,Q;
//   float Qmin,Qmax;
   int  Blen;

   prun = &CONFIG_IDRUN[Run];
//printf("CountUse = %d\n",CountUse);
//  printf("Kimp_n = %d\n",Cnt[Run].Kimp_n);  // 2. ===== ������� �� �������� ��-������� =====
    Buflen= QBUFLEN;
/*   if (((prun->TypeRun) == 5)&&(CountUse == STEREO)) {            // ======== �������� CountUse==2
//    if (CountUse == STEREO) {            // ======== �������� CountUse==2
//printf("STEREO Kimp_n=%d\n",Cnt[Run].Kimp_n);
      if (Cnt[Run].Kimp_n != 0) {       // ����� ��������� �� ����� �� ����� 0
         if (Cnt[Run].pr1ob == 1) {     //  ������ ���������
         Cnt[Run].Tn1 = Cnt[Run].Tt;     // ��������� �����
         Cnt[Run].pr1ob = 2;
         }
         if (Cnt[Run].pr1ob == 2) {     // �� ������ ���������
                                        // �������� ����� ��������� ����������
          Cnt[Run].Tn = Cnt[Run].Tt;     // ��������� �����
          Cnt[Run].interval = (float)(Cnt[Run].Tn - Cnt[Run].Tn1);

//printf(" interval = %f PulseTakt=%d Tn=%ld\n",Cnt[Run].interval,PulseTakt,Cnt[Run].Tn);
           if (Cnt[Run].interval > 0)                                      // �������� ��������
            {
//             if  ( PulseTakt<(BufLen))
               if (Cnt[Run].interval > 1)
              { Cnt[Run].Q = ((Kimp_r * prun->NumTurbo)
                         / Cnt[Run].interval)
                         * (float)Cnt[Run].tcas;
                Cnt[Run].QRaw = Cnt[Run].Q;
//printf("LF   Qw= %f\n",Cnt[Run].QRaw);
              }
                else {  // ������� �������
                                     // ������� �� � ����� �� �������
                     Cnt[Run].Q = (float)QCurrSumm[0] * prun->NumTurbo
                         / (float)(BufLen)
                         * (float)Cnt[Run].tcas / SysTakt ;
//                     Cnt[Run].QRaw = (float)CurrSumm[0] * prun->NumTurbo
//                         / (float)(BufLen+BufShift)
//                         * (float)Cnt[Run].tcas / SysTakt ;
                     Cnt[Run].QRaw = Cnt[Run].Q;
//printf("HF   Qw= %f QCurrSumm[0]=%lu\n",Cnt[Run].QRaw,QCurrSumm[0]);
                  }
            }  // ����� if (Cnt[Run].interval > 0)

            Cnt[Run].Q1 = Cnt[Run].Q;   // ��������� ������
                                        // ��������� ������
            Cnt[Run].Q1Raw = Cnt[Run].QRaw;
         } // �����  if (Cnt[Run].pr1ob == 2) �� ������

         Cnt[Run].Tn1 = Cnt[Run].Tn;    // ��������� ����������� �������

         Cnt[Run].j = 0;                // ��������� �������� ���������

         Cnt[Run].pr1ob = 2;            // ������� ���������� 2-� �����
      } // ����� if (Cnt[Run].Kimp_n != 0)
                                        // ������ ���������
      if ((float)Cnt[Run].j < Cnt[Run].interval) {
         Cnt[Run].dem = 0.0;            // �������� �-� �������������
         Cnt[Run].jj = 0;               // �������� ������� ���������� ���������
      }
      else { //((float)Cnt[Run].j >= Cnt[Run].interval)
                           // �� ������ ���������
           // ����������� �� ������������ ����� �����
         if (Cnt[Run].j > (long)Cnt[Run].interval_min
            || Cnt[Run].j > (long)Cnt[Run].interval_max)
                                        // ����������� �-� �������������
            Cnt[Run].dem = (Cnt[Run].dem + 1.0*SysTakt)*2.0;

         if (Cnt[Run].dem>10.0e30)      // ���������� �-�
            Cnt[Run].dem = 10.0e30;

           Cnt[Run].jj += SysTakt;

         Cnt[Run].interval_1 = Cnt[Run].interval + (float)Cnt[Run].jj + Cnt[Run].dem;

         if (Cnt[Run].interval_1 != 0) {
            Cnt[Run].Q = (Cnt[Run].Q1 * Cnt[Run].interval)
                / Cnt[Run].interval_1;  // ��������� ������
            Cnt[Run].QRaw = (Cnt[Run].Q1Raw * Cnt[Run].interval)
                / Cnt[Run].interval_1;  // ��������� ������
//printf("interval_1=%f QRaw=%f\n",Cnt[Run].interval_1, Cnt[Run].QRaw);
         }

        if (Cnt[Run].QRaw < prun->dPots) // ���� ������ Qst(������ �������)
         {   Cnt[Run].QRaw = 0.0;
             Cnt[Run].Q = 0.0; }          // �� ��������

      }  //����� ((float)Cnt[Run].j >= Cnt[Run].interval)

      Cnt[Run].j += SysTakt;    // ��������� ������� ���������
   }  // ����� STEREO

   else
*/
//    {                               // ������� ��������
//---------------------------------------------------------------------------
//    printf("Kimp_r=%f\n",Kimp_r);
      if (Cnt[Run].Kimp_n != 0) {       // �������� �� ����� ����
         if (Cnt[Run].pr1ob == 1) {     // ���� ������ ����������
                                        // �� ������ ��������� �����
//            Cnt[Run].Tn1 = Cnt[Run].Tt;
//     printf(" pr1ob=%d Q=%f",Cnt[Run].pr1ob,Q);
            Cnt[Run].pr1ob = 2;
                                        // � ���
         }
         else {                         // ���� ������ ���������� � ������
                                        // 1 - ����� �� ��������
          Cnt[Run].interval = (float)(Cnt[Run].Tt - Cnt[Run].Tn1);
//printf("Tt=%ld Tn1=%ld\n",Cnt[Run].Tt, Cnt[Run].Tn1);
        if (Cnt[Run].interval <= 0) Cnt[Run].interval=1.0;
        if ((Cnt[Run].Kimp_n > 9) || (Cnt[Run].interval > 6.0))
           Buflen=5;
        else Buflen=10;
//printf("Cnt[Run].interval=%f Buflen=%d\n",Cnt[Run].interval,Buflen);
/*          Kpr = (float)Interval[Run]/Kki;
//printf(" Kpr =%f interval=%f Int=%d kki=%f\n",Kpr,Cnt[Run].interval,Interval[Run], Kki);
      if (Kpr < 2.01) {Buflen=QBUFLEN; Kpr=Cnt[Run].Kimp_n*Kpr;} else Buflen=2;
//printf(" Kpr =%f\n",Kpr);
       if ((Cnt[Run].interval < 300.0)&&(Interval[Run] > Kki/1.05)) // ������������  Interval[Run] < 60000
           Cnt[Run].interval = Kpr;
//printf("3-Cnt[%d].interval=%f,Interval=%u \n",Run,Cnt[Run].interval,Interval[Run]);
*/
           if (Cnt[Run].RBLen < Buflen)
           {   Cnt[Run].AKimp_n[Cnt[Run].RBLen] = Kimp_r;
               Cnt[Run].ATn[Cnt[Run].RBLen] = Cnt[Run].interval;
             Cnt[Run].ImpSumm += Kimp_r;
//printf("Kimp_r=%f\n",Kimp_r);
               Cnt[Run].TimeSumm += Cnt[Run].ATn[Cnt[Run].RBLen];
               Cnt[Run].RBLen++;        // ���������� ������
               Blen=Cnt[Run].RBLen;
            }
            else {                      // 2- ����� ��������
                                        // ������� ������� ����� ������
               Cnt[Run].ImpSumm = 0.0;
               Cnt[Run].TimeSumm = 0.0;
//      printf("2-IS=%f TS=%f\n",Cnt[Run].ImpSumm,Cnt[Run].TimeSumm);
                                        // ����� ��������
               for (i=0; i<(Buflen-1); i++) {
                  Cnt[Run].AKimp_n[i] = Cnt[Run].AKimp_n[i+1];
                  Cnt[Run].ATn[i] = Cnt[Run].ATn[i+1];
               }
                                        // ������� ��������� ��������
                 Cnt[Run].AKimp_n[Buflen-1] = Kimp_r;
                 Cnt[Run].ATn[Buflen-1] = Cnt[Run].interval;
//printf("ATn[Buflen-1]=%f\n",Cnt[Run].ATn[Buflen-1]);
                                        // �������� �� � ������
                for (i=0; i<Buflen; i++) {
                 Cnt[Run].ImpSumm += Cnt[Run].AKimp_n[i];
                 Cnt[Run].TimeSumm += Cnt[Run].ATn[i];
                }
//    printf("3-IS=%f TS=%f\n",Cnt[Run].ImpSumm,Cnt[Run].TimeSumm);
            Cnt[Run].RBLen = Buflen;
            Blen =  Buflen;
      }  //����� else ����� ��������
            Cnt[Run].ImpSummAverage = Cnt[Run].ImpSumm / Cnt[Run].TimeSumm;
                                        // ������ �� ��������
            Cnt[Run].Q = Cnt[Run].ImpSummAverage * prun->NumTurbo * (float)Cnt[Run].tcas;
            Cnt[Run].QRaw = Cnt[Run].Q;
            Cnt[Run].Tn1 = Cnt[Run].Tt; // ��������� �����
            Cnt[Run].Q1 = Cnt[Run].Q;   // ��������� ������
                                        // ��������� ������
            Cnt[Run].Q1Raw = Cnt[Run].QRaw;
//printf("ImpSumm=%f,TimeSumm=%f Q=%f\n",Cnt[Run].ImpSumm,Cnt[Run].TimeSumm,Cnt[Run].Q);
         }  //����� Kimp_n !=0

//         Cnt[Run].Tn1 = Cnt[Run].Tt;    // ��������� �����

         Cnt[Run].j = 0;                // ��������� �������� ���������
         Cnt[Run].pr1ob_0 = 1;
      }
      else {                            // ������ ������ ����������

         if (Cnt[Run].pr1ob_0 == 1) {   // ������ ���������
            Cnt[Run].dem = 0.0;         // �������� �-� �������������
            Cnt[Run].jj = 0;            // �������� ������� ���������� ���������
            Cnt[Run].j = 0;             // ��������� �������� ���������
            Cnt[Run].pr1ob_0 = 2;       // ������ ���������
         }
         else {                         // �� ������ ������ ����������
                                        // ����������� �� ������������ ����� �����

                                        // ������ ��� ���������� ��������
            if (Cnt[Run].RBLen != 0) {  // ����� �� ������
//             printf("j=%d\n",Cnt[Run].j);
                                        // ����������� �-� �������������
                                 // ����  ������ ��� ���������� ��������
               if (Cnt[Run].j > Cnt[Run].ATn[Cnt[Run].RBLen-1]) {
                  Cnt[Run].dem = (Cnt[Run].dem + 1.0*SysTakt)*2.0;
                                        // ���������� �-�
                  if (Cnt[Run].dem>10.0e30)
                     Cnt[Run].dem = 10.0e30;
//                printf("dem=%f\n",Cnt[Run].dem);

                  Cnt[Run].jj += SysTakt;

//                printf("jj=%d\n",Cnt[Run].jj);
                  Cnt[Run].interval_1 = Cnt[Run].ATn[Cnt[Run].RBLen-1] + (float)Cnt[Run].jj + Cnt[Run].dem;
//                printf("intv1=%f\n",Cnt[Run].interval_1);

                  if (Cnt[Run].interval_1 != 0) {
                                        // ��������� ������
                     Cnt[Run].Q = (Cnt[Run].Q1 * Cnt[Run].ATn[Cnt[Run].RBLen-1])
                        / Cnt[Run].interval_1;
                                        // ��������� ������
                     Cnt[Run].QRaw = (Cnt[Run].Q1Raw * Cnt[Run].ATn[Cnt[Run].RBLen-1])
                        / Cnt[Run].interval_1;
//             printf("Q=%f QRaw=%f c=%d\n",Cnt[Run].Q,Cnt[Run].QRaw,counttime);

                if (Cnt[Run].QRaw < prun->dPots) // ���� ������ Qst(������ �������)
                {   Cnt[Run].QRaw = 0.0;
                    Cnt[Run].Q = 0.0; }          // �� ��������

                  }
               }
               else {                   // ��������� ������� ���������
                  Cnt[Run].j += SysTakt;
//                printf("j=%d\n",Cnt[Run].j);
               }
            }
            else {                      // ��������� ������
               Cnt[Run].Q = 0;
               Cnt[Run].QRaw = 0;
            }
         }
      }
//   }
//printf("Stack=%u\n",stackavail());
}

//----------------------------------------------------------------------------
float GetQw4(int nRun) {                 // ������ � �� �� 4-� ������
                 // ������ � �� �� 4-� ������
//printf("4-IndQs=%f IndDP=%f Qw=%f\n", IndQs[nRun],IndDP[nRun],CONFIG_IDRUN[nRun].Qw);
   if ((RS485HF[nRun]==1) && ((CONFIG_IDRUN[nRun].TypeRun) != 10))
    return IndQs[nRun];
   else
//    return IndDP[nRun];
      return CONFIG_IDRUN[nRun].Qw;
}
//----------------------------------------------------------------------------
float GetQw3(int nRun) {                 // ������ � �� �� 3-� ������
//printf("3-run=%d IndQs=%f Cnt.QRaw=%f const=%f\n", nRun,IndQs[nRun],Cnt[nRun].QRaw,
//  CONFIG_IDRUN[nRun].constqw);
      if  ((CONFIG_IDRUN[nRun].TypeRun) == 10)  // ���
      {  if (!(CONFIG_IDRUN[nRun].constdata & 0x08))
           return CONFIG_IDRUN[nRun].constqw;
        else
         {
           return IndQs[nRun];
         }
      }
      else  // �������
        if  (((CONFIG_IDRUN[nRun].TypeRun) == 4)
            ||  ((CONFIG_IDRUN[nRun].TypeRun) == 5))
         { if (!(CONFIG_IDRUN[nRun].constdata & 0x08))
            return CONFIG_IDRUN[nRun].constqw;
          else
            return Cnt[nRun].QRaw;
         }
       else // ��
         return IndDP[nRun];
}
//----------------------------------------------------------------------------
float GetRealQw(int nRun) {              // �������� ������ ��� �������� ������
                                         // ��� ����� ������ �� ���������
   return Cnt[nRun].Q;
}

//---------------------------------------------------------------------------
double GetQ(int nRun) {                  // �������� ������ ��� ������������ ������
   struct calcdata   *uk;

   uk = &CALCDATA[nRun];                // ������� ����p �����
//printf("IndQ=%f IndDP=%f Cnt.QRaw=%f\n",IndQ[nRun],IndDP[nRun],Cnt[nRun].QRaw);
      if  ((CONFIG_IDRUN[nRun].TypeRun) == 10)  // ���
       if (!(CONFIG_IDRUN[nRun].constdata & 0x08))
       return  IndDP[nRun]  * uk->kQind;
       else
       return  IndQs[nRun]  * uk->kQind;
      else  // �������
        if  (((CONFIG_IDRUN[nRun].TypeRun) == 4)
        ||  ((CONFIG_IDRUN[nRun].TypeRun) == 5))
          if (!(CONFIG_IDRUN[nRun].constdata & 0x08))
            return  IndDP[nRun] * uk->kQind;//CONFIG_IDRUN[nRun].constqw*uk->kQind;
          else
            return Cnt[nRun].QRaw*uk->kQind;
       else // ��
         return IndQ[nRun];
}
//--------------------------------------------------------------------------
double GetQwRaw(int nRun) {              // �������� ������ ��� �������� ������
                                        // ��� �����������
   if (!(CONFIG_IDRUN[nRun].constdata & 0x08)) {
      return  CONFIG_IDRUN[nRun].constqw ;
   }
   else
      return Cnt[nRun].QRaw;
}

//---------------------------------------------------------------------------
float GetRealQwRaw(int nRun) {          // �������� ������ ��� �������� ������
 if (CONFIG_IDRUN[nRun].TypeCounter > 0 && CONFIG_IDRUN[nRun].AddrMODBUS > 0)
  {  return IndQs[nRun];}
   else
  {  return Cnt[nRun].QRaw;}
}

//--------------------------------------------------------------------------
float GetQRaw(int nRun) {               // �������� ������ ��� ������������ ������
   struct calcdata   *uk;               // ��� �����������

   uk = &CALCDATA[nRun];                // ������� ����p �����

   return (GetQwRaw(nRun)*uk->kQind);   // (4) ��-50-213-80
}
