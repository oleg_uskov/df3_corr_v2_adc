//////////////////////////////////////////////////////////////////
// ������ ������� ����.
//////////////////////////////////////////////////////////////////
#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <mem.h>
//#include <dos.h>
//#include <time.h>
#include "h_main.h"
#include "h_calc.h"
#include "h_analog.h"
#include "h_mem.h"
//#include "h_disp.h"
//#include "h_hart.h"
#include "h_smooth.h"
//#include "h_valarm.h"
//#include "h_msd.h"
//#include "h_main.h"

#define   comport1  0x3F8             // COM1
#define  comport    0x2F8
#define   RIRQ3             0xFF3E
#define PER  1
#define HOU  2
#define DAY  3
#define ALR 4
#define AUD 5
#define SCR 6
#define AVOL 7
#define SEC  8
//-----------------------------------------------------------------------
void TaskCalcRe_(void);
void ReadSetup(void);
void K_Gerg91_2(void);
void K_Gerg91_22(struct calcdata* uk);
void TaskCalc_K_sh3_32(void);
unsigned int K_Gerg91_3(struct calcdata* uk,int rN);
unsigned int Calc_K_sh_3(struct calcdata *uk);
unsigned int Calc_K_sh_32(struct calcdata *uk);
void ComPrint(char *Txt);
void ComPrintF(float Nmb);
void ComPrintL(int N);
void RS485_Set485Data(int k);            // ���������� ������ �� ������ �� ��p����p� 485-��������
void TaskCalcVlago(void);
void TaskSetMicroData(void);               // ������ �� MicroMotion
void RS485IniHF(void);
//void Hart_SetHartData(int);
//void Com1_Watch();
//void printAT();
void ZapuskUZS(int nRn);
//void SetamountPT(int nRun);
void SD_MgnSend(void);
void SD_MgnSend_1(void);
void SD_PerSend(void);
void SD_PerSend_1(void);
//void InitSD_HC();
void SDsend(void);
int TaskCalc_SGERG88(void);
//void RS485_Set485FBData();
//void RS485FBIni(int,char);
void RS485Ini(void);
//void DensomTrans(void);  // ������ ����� ��p��� ����������
int TaskCalc_AGA8(void);
int GetEdErr(int nRun);
int ChkReAlarm(void);
void Task_Start();
void Task_CalcQ(void);
void WriteDataW25(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *wrData);
void ReadDataW25(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData);
uint8_t WriteDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *wrData);
uint8_t ReadDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData);
struct SPI_HandleTypeDef;
void P23K256_Write_Data(struct SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
//void WriteCycleData();
uint8_t Mem_AuditWr(uint8_t,struct memaudit audit);
uint16_t Mem_Crc16Int(uint8_t *buf, uint16_t count);
void Setperiodtdata();
float  Mem_GetDayQ(uint8_t run);
void GlPrint();
//-----------------------------------------------------------------------
/*
//extern unsigned char BufTrans_1[];
extern unsigned int SendCount_1;
extern unsigned int outindex_1;
extern unsigned int inindex,inindex_1;
//extern unsigned char BufTrans[];
extern unsigned int SendCount;
extern unsigned int outindex;
//-------------------------------
*/                                        // ������� ������
//extern struct valarm_count AllAlarmCount[3];
					// ������� �������� Qmin
//extern struct min_count MinAlarmCount[3];
unsigned int errQw[3]; // ������ ����� ��� �� MODBUS
unsigned char SetShelf[3];
unsigned char SetShelfP[3];
unsigned char SetShelfC[3];

extern unsigned char CalcStartFlag;     // ������� ������� �������
extern struct intpar    INTPAR;
extern struct new_count Cnt[3];         // ������ ������ ��������
//extern char CalcBRZ;
extern char Z_Out[],Z_in[];
extern double  BMIX[];
extern uint16_t NumZap[3];
extern uint8_t wData[ArchSize];
extern uint8_t rData[ArchSize];
extern uint8_t str90[90];
//extern float XJ[3][21];

//extern struct hartdata  HARTDATA[];   // ����� ������ � ��������
//extern unsigned char FixBuf[];
//extern struct hartdata *ph;             // ����� ���� ��������

extern unsigned char CalcReadyFlag[];   // ������� ���������� ������ �������
extern unsigned char FreqAlarm;         // ������� ������ �� ����������
extern unsigned char pr_first;          // ������� ���������� ��������� ����������
//extern unsigned char KbdScr;
extern int ValarmRun;
extern unsigned long Cntn;
extern int glnumrun;
struct calcdata COPY_CALCDATA[3];
struct calcdata COPY_CALCDATA_1[3];

struct bintime   BINTIME;        // �������� ��p����p� �p�����
extern struct glconfig  GLCONFIG;
extern struct AddCfg    ADCONFIG;
extern struct IdRun CONFIG_IDRUN[3];
extern struct AdRun CONFIG_ADRUN[3];               //120*3=360
extern struct memaudit    MEMAUDIT;
extern uint16_t  counttime;         // ���-�� ������ ��� ���������� �p�����
float Qadwc[3],Qadsc[3];         // ����������� ����� �� ����� - ������� � �����������
//extern struct alarm MinMatr[8][3];      // �������� ��� ���� ����� � �����
//extern unsigned char NeedCom1;                 // ����� ���1 ��� �����
//extern int Nsp[3], Nsp1[3];
//extern unsigned char Err485;         // ������� ������ �� ����������
extern unsigned int hart2; // ������� HART2
extern unsigned int Obs_ss[]; // ������ "��������� ������������ �/� ��������"
extern char  RS485HF[];
//extern int fMB; // ���������� ������� ������ �� MODBUS (1-�����c, 2-�����, 3-������)
extern int SysTakt;
extern int sms_send;
extern char startT;
extern unsigned int      rs485num;       // ���-�� �������� ��� RS485
extern char RS485MB;
extern unsigned int Interval[3];
//extern unsigned char errMODBUS[];
extern unsigned int Obs_err[];
extern float Kki;
extern char SDMgn2, SDPer2;  // ���� ������� �������
extern char SDMgn1, SDPer1;  // ���� ������� �������
extern int nWatch;
extern int NzBlk[],NzBlkp[];
extern char utr[3],utr1[3];
extern char Zc1[],CalcZc[];
extern double KoefP[];  // = {1.0,0.0980665,98.0655,0.980665};
extern double KoefdP[];  // = {1.0, 0.00980655, 0.0980665};
extern double KoefEn[]; // = {238.846, 1.0, 0.277778};
extern float KoRo[3][3];
extern float TAU_flt,Ros_flt;       // ��������� ��������
extern struct memdata      MEMDATA;   // ��p����p� ������ ������ - 26 ����
extern char FirstC[];
extern char NoWriteCycle[];
long int ts2;
extern unsigned int rs485num; //, rs485FBnum;
extern unsigned char AnCom5; // ������� ������� COM2
//extern char Densomer;
extern  float MValue[3][4];  // ������ ������ - ����� ����� 0,1,2
extern char BR;
extern unsigned int ExtChroma;
extern float XI[];
extern int sch;
extern char CalcBR[];
extern int IniSD;
float  ZcK[3][2];
extern  float  ZcK1[3][2];
extern int kT;
//extern double  Vwsr[];
//extern long Tin;
//extern uint16_t di1s[],di2s[];
extern uint16_t  KolElS[],KolElM[],KolElH[],KolElD[];
extern struct SPI_HandleTypeDef hspi3;
extern uint8_t AdrCALC[3][2];
extern uint16_t SizeCALC;
//struct pointARch;
extern struct pointARch pArchiv;
extern uint16_t Zap_in_Block;
extern uint16_t SectorSize;
extern  char str[80];
extern  uint32_t t_17;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
//extern union
//{    
// uint8_t MASS_QS[3][ARCS_B + BLARC_B];
// struct memdata As[3][ARCS+BLARC]; 
//} dataArc;
//extern union
//{
//    uint8_t Bl_s[BLARC_B];
//    struct memdata As[BLARC];
//} BlockAs;
extern uint16_t ARCS_1;  // ARCS+BLARC = 60+4
struct memdata mdataR;
//extern RTC_TimeTypeDef sTime;
//extern RTC_DateTypeDef sDate;

//struct pointARch
//{    uint16_t di1p[3];  //dip[3][2]
//     uint16_t di2p[3];
//     uint16_t di1h[3];
//     uint16_t di2h[3];
//     uint16_t di1d[3];
//     uint16_t di2d[3];
//     uint16_t di1Al[3];
//     uint16_t di2Al[3];
//     uint16_t di1Au[3];
//     uint16_t di2Au[3];
//     uint16_t di1Scr[3];
//     uint16_t di2Scr[3];
//     uint16_t di1AV[3];
//     uint16_t di2AV[3];
//     uint16_t di1s[3];    
//     uint16_t di2s[3];
//};

//------------------------------------------------------------------------
struct calcdata     CALCDATA[3];
struct calcdata     *pcalcdata;
unsigned int GergErr;
//int   Maxq[4];         // ������������ ����� �������
float IndRe[3],IndC_[3],IndKsh[3],IndQs[3],Qsn[3];
double IndQ[3],IndDP[3];
char ReErr[3], HeatErr[3];              // ��������� ������ �� ������� � ����������
unsigned char run_SMS;
//char Calc59Mgn, Calc1Per,Calc5Conf;
//char u1,u2;
unsigned int        error;
unsigned int        calcerr;
unsigned int        calc_buferrrun0[30];
unsigned int        calc_buferrrun1[30];
unsigned int        calc_buferrrun2[30];
unsigned int        membuferrrun0[20]; // �訡�� �� �p����饬 ⠪� p�����
unsigned int        membuferrrun1[20]; // �訡�� �� �p����饬 ⠪� p�����
unsigned int        membuferrrun2[20]; // �訡�� �� �p����饬 ⠪� p�����
//unsigned int coderred[]={0xA7,0x27,0xA8,0x28,0xA9,0x29,0xAA,0x2A,0x8D,0x0D};
//unsigned int coderrgr[]= {0xAB,0x2B,0xAC,0x2C,0xAD,0x2D,0xAE,0x2E,\
//                          0xAF,0x2F,0xB0,0x30,0xB1,0x31,0xB2,0x32,\
//                          0xB3,0x33,0xB4,0x34,0xB5,0x35};
//unsigned char QstErrFlag[3] = {0,0,0};

int t1;
float C_11,C_12,C_13,C_130,C_14,C_15,C_16,C_17;
double Q_1;
double Re_1;
//unsigned int CalcTakt;
float StartRe[3];      // �������� Re ����� ������
double PrevQ[3];                         // ������ �� ����.���� ��������

//char ErrMODBUSQw[3],Err1MODBUSQw[3];  // ������� ������ �� MODBUS ��� ���������
float  AmountQru[3][6];
int Amk[3];
float T_st;  //,P_st;    T_st = 293.15;                       // 20 ����. �������
float Tsr[3],Psr[3];
//float RLair[3] = {1.204449,1.292923,1.225410};  //20,0,15
char FBCom;
int FBIni;
char FirstCalc;
char FReverse[3];


/****************************************************************************
calc_buferr[0] - �������� ������ �������� (0 ���\��2)
calc_buferr[1] - �������� ������ �������� (�����)
calc_buferr[2] - �����p���p� ������ ������� (�����)
calc_buferr[3] - �����p���p� ������ ������� (�����)
calc_buferr[4] - ��������� ������ ���������� (�����)
calc_buferr[5] - ��������� ������ ���������� (�����)

calc_buferr[6] - �������� ������ 0 ���\��2, p����� �� �p����������, Q=�p��
calc_buferr[7] - �������� ������ 120 ���\��2, p����� �� �p����������, Q=�p��
calc_buferr[8] - �����p���p� ������ -40 �p��. �, p����� �� �p����������, Q=�p��
calc_buferr[9] - �����p���p� ������ 90 �p��. �, p����� �� �p����������, Q=�p��
calc_buferr[10] - ��������� ������ 0.55 ��/�3, p����� �� �p����������, Q=�p��
calc_buferr[11] - ��������� ������ 1.0 ��/�3, p����� �� �p����������, Q=�p��

calc_buferr[12] - ��p���� �������� ������ �p���� �������, �������� ������
calc_buferr[13] - ��p���������� ��p����, p����� �� �p����������, Q=0

calc_buferr[20] - ������p ����p���� ������ 12.5 ��, p����� �� �p����������, Q=0
calc_buferr[21] - ������p �p����p����� > 760.0 ��� > 1000 ��, p����� �� �p����������, Q=0
calc_buferr[22] - ������p �p����p����� < 50 ��, p����� �� �p����������, Q=0
calc_buferr[23] - ������ > 0.56 ��� > 0.64, p����� �� �p����������, Q=0
calc_buferr[24] - ������ < 0.04 ��� < 0.05, p����� �� �p����������, Q=0
calc_buferr[25] - ������� ������� �� ���� , p����� �� �p����������, Q=�p��
calc_buferr[26] - ������ p������ ����������� �����������
calc_buferr[27] - ������ ����������� �������� � ��p�����
calc_buferr[28] - ������ p������ ���� ����������
calc_buferr[29] - �������� Pa > 3 MPa, ����������� ������ NX-19

//������ p������ ������������ ��������

 // 1 ��� = 101,9716 ���/�2
****************************************************************************/
//---------------------------------------------------------------------------
  float  valGERG91[] = {1.02, 122.37, -23.15, 66.85, 0.54,  1.2, 30.59}; // GERG91-���
  static float  valNX19[]   = {1.02, 122.37, -23.15, 66.85, 0.54,  1.2, 30.59}; // NX19
         float  valALARM[]  = {1.02, 122.37, -23.15, 66.85, 0.54,  1.2, 30.59}; // ���p���� - 306 - ��� �����

  unsigned int errGERG91[] = { 0x3A0,0x320,0x199,0x119,0x198,0x118,0x190,0x110,0x1A6,0x126,0x1A5,0x125,0x299,0x219};
//  static unsigned int errNX19[]   = { 0x3A0,0x320,0x199,0x119,0x198,0x118,0x190,0x110,0x1A6,0x126,0x1A5,0x125,0x299,0x219};

//---------------------------------------------------------------------------
float DPrevers = 6.0;
//---------------------------------------------------------------------------
int GetDpMinR() {                        // 13.09.06
   return 6300;
}
int GetDpMinShelf() {                   // 13.09.06
   int iTmp;
   return 0; //return INTPAR.QminFlag;
}
//-----------------------------------------------------------
                                        // ����p��� �室��� ������ �� ��⮤�� p����
unsigned int Calc_CorrTestInData(int nRun) {
   float           *index;
   unsigned int    *perr;
   unsigned int    *pbuferr;
   unsigned int    err = 0;
   unsigned int    ret;
   struct calcdata* uk;
   float fTmp;
   struct IdRun    *prun;               // ��������� �� ��p����p� �����
   uk = &CALCDATA[nRun];                // ������ �������� ����p� �����
   prun  = &CONFIG_IDRUN[nRun];       // ������� ����p �����
         ret = 0;
         index = valGERG91;
         perr = errGERG91;
   switch (nRun) {
      case 0: pbuferr = calc_buferrrun0; break;
      case 1: pbuferr = calc_buferrrun1; break;
      case 2: pbuferr = calc_buferrrun2; break;
   }
 					// < 1 ��p���������� ��������
   err = (uk->Pa < (index[0]-0.05)) ? perr[0] : perr[1];
//printf("Data P=%f Min=%f\n",uk->Pa,index[0]);
   if (err != pbuferr[0]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[0] = err;                 // �p������� ����� ��������
      ret = 1;
   }

     if ((CONFIG_IDRUN[nRun].TypeRun != 8)
     &&  (CONFIG_IDRUN[nRun].TypeRun != 9))
   {
   err = (uk->Pa > index[1]) ? perr[2] : perr[3];
   if (err != pbuferr[1]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[1] = err;
      }
  }                                     // �����p���p� ������ ������
   if (prun->Dens == 0)  // ��������� �� ���������
  {                                        // ��������� ���� �����������
                                        // ��������� ���� �����������
    float a;
    a=ceil(100.0*index[4]*KoRo[0][GLCONFIG.EDIZM_T.T_VVP]-0.5)/100.0;
//printf("a=%f idex=%f KoRo=%f\n",a,index[4],KoRo[0][GLCONFIG.EDIZM_T.T_VVP]);
    err = (prun->RoVV < a) ? perr[8] : perr[9];
   if (err != pbuferr[4]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[4] = err;                 // �p������� ����� ��������
      ret = 1;
   }
 // �p���p�� ��������� ���������
   a = ceil(100.0*index[5]*KoRo[0][GLCONFIG.EDIZM_T.T_VVP]-0.5)/100.0;
//printf("b=%f idex=%f KoRo=%f\n",a,index[5],KoRo[0][GLCONFIG.EDIZM_T.T_VVP]);
   err = (prun->RoVV > a) ? perr[10] : perr[11];
   if (err != pbuferr[5]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[5] = err;                 // �p������� ����� ��������
      ret = 1;
   }
  }
   else  // �᫨ ���⭮��� ������祭
   {
    err = (prun->ROnom < prun->R0min) ? perr[8] : perr[9];
    if (err == perr[8])
     { uk->ROnom = prun->ROnom ;} // ������� ��������� �� ���������
    if (err != pbuferr[4]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[4] = err;                // �p������� ����� ��������
      ret = 1;
    }
    err = (prun->ROnom > prun->R0max) ? perr[10] : perr[11];
    if (err == perr[10]) uk->ROnom = prun->ROnom; // ������� ��������� �� ���������
    if (err != pbuferr[5]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[5] = err;                // �p������� ����� ��������
      ret = 1;
    }
   }

   return ret;
}

/*
//---------------------------------------------------------------------------
time_t BinToSec(struct bintime Bin) {
   struct tm tmTime;
   time_t t_ret;

   tmTime.tm_sec   = Bin.seconds;
   tmTime.tm_min   = Bin.minutes;
   tmTime.tm_hour  = Bin.hours;
   tmTime.tm_mday  = Bin.date;
   tmTime.tm_mon   = Bin.month-1;
   tmTime.tm_year  = Bin.year+100;

   if (tmTime.tm_year>137)
      tmTime.tm_year  = 137;

   tmTime.tm_wday  = 0;
   tmTime.tm_yday  = 0;
   tmTime.tm_isdst = 0;

   t_ret = mktime(&tmTime);

   return t_ret;
}
*/
//---------------------------------------------------------------------------
//��������� ����� ������ �p���� �p�������� ����� p������ � "������������" ���������
void Calc_CorSetNoErr(int nR) {
   unsigned int *ukerr;
   unsigned int Code;
   int i;

//........................................................................
   for (i=0; i<6; i++) {
       Code =  errGERG91[i*2+1];
      if (nR==0)
      calc_buferrrun0[i] = Code;
      else if (nR==1)
      calc_buferrrun1[i] = Code;
      else if (nR==2)
      calc_buferrrun2[i] = Code;
   }
}
//........................................................................
void Calc_SetNoErr() {
//   unsigned int *ukerr;
   unsigned int Code;
   int i;
   //........................................................................
   calc_buferrrun0[12] = 2;             // �p����� �������, ���/�2 ��� �����������
   calc_buferrrun1[12] = 2;
   calc_buferrrun2[12] = 2;

   calc_buferrrun0[13] = 0x40;          // ��p���������� ��p����
   calc_buferrrun1[13] = 0x40;
   calc_buferrrun2[13] = 0x40;

   calc_buferrrun0[20] = 0x36;          // ������p ����p���� ������ 12.5 ��
   calc_buferrrun1[20] = 0x36;
   calc_buferrrun2[20] = 0x36;
					// 1 - ���������
   calc_buferrrun0[21] = (CONFIG_IDRUN[0].typeOtb) ? 0x37 : 0x38;
   calc_buferrrun1[21] = (CONFIG_IDRUN[1].typeOtb) ? 0x37 : 0x38;
   calc_buferrrun2[21] = (CONFIG_IDRUN[2].typeOtb) ? 0x37 : 0x38;

   calc_buferrrun0[22] = 0x39;          // ������p �p����p����� < 50.0 ��
   calc_buferrrun1[22] = 0x39;
   calc_buferrrun2[22] = 0x39;
					// 1 - ��.
   calc_buferrrun0[23] = (CONFIG_IDRUN[0].typeOtb) ? 0x3A : 0x3C;
   calc_buferrrun1[23] = (CONFIG_IDRUN[1].typeOtb) ? 0x3A : 0x3C;
   calc_buferrrun2[23] = (CONFIG_IDRUN[2].typeOtb) ? 0x3A : 0x3C;

					// 1 - ��. ������ < 0.04
   calc_buferrrun0[24] = (CONFIG_IDRUN[0].typeOtb) ? 0x3B : 0x3D;
   calc_buferrrun1[24] = (CONFIG_IDRUN[1].typeOtb) ? 0x3B : 0x3D;
   calc_buferrrun2[24] = (CONFIG_IDRUN[2].typeOtb) ? 0x3B : 0x3D;

   calc_buferrrun0[25] = 0x3E;          // ������� ������� �� ����
   calc_buferrrun1[25] = 0x3E;
   calc_buferrrun2[25] = 0x3E;

   calc_buferrrun0[26] = 0x13;          // ������ p������ ����. �����������
   calc_buferrrun1[26] = 0x13;
   calc_buferrrun2[26] = 0x13;

   calc_buferrrun0[27] = 0x3F;          // ����������� ��������, ��. ����p���� DP-[���/�2], Pa-[���/��2]
   calc_buferrrun1[27] = 0x3F;
   calc_buferrrun2[27] = 0x3F;

   calc_buferrrun0[28] = 0x0C;          // ����� ����������
   calc_buferrrun1[28] = 0x0C;
   calc_buferrrun2[28] = 0x0C;

   calc_buferrrun0[29] = 0x219;          // NX-19 Pa > 3 MPa
   calc_buferrrun1[29] = 0x219;
   calc_buferrrun2[29] = 0x219;
}
//-----------------------------------------------------------------------
// ��������� ����� ������ ��� ���� ������ "������������" ���������
void Calc_SetNoErrTypeOtb() {
                                        // 1 - ���������
   calc_buferrrun0[21] = (CONFIG_IDRUN[0].typeOtb) ? 0x37 : 0x38;
   calc_buferrrun1[21] = (CONFIG_IDRUN[1].typeOtb) ? 0x37 : 0x38;
   calc_buferrrun2[21] = (CONFIG_IDRUN[2].typeOtb) ? 0x37 : 0x38;

                                        // 1 - ��.
   calc_buferrrun0[23] = (CONFIG_IDRUN[0].typeOtb) ? 0x3A : 0x3C;
   calc_buferrrun1[23] = (CONFIG_IDRUN[1].typeOtb) ? 0x3A : 0x3C;
   calc_buferrrun2[23] = (CONFIG_IDRUN[2].typeOtb) ? 0x3A : 0x3C;

                                        // 1 - ��. ������ < 0.04
   calc_buferrrun0[24] = (CONFIG_IDRUN[0].typeOtb) ? 0x3B : 0x3D;
   calc_buferrrun1[24] = (CONFIG_IDRUN[1].typeOtb) ? 0x3B : 0x3D;
   calc_buferrrun2[24] = (CONFIG_IDRUN[2].typeOtb) ? 0x3B : 0x3D;
}
//---------------------------------------------------------------------------
unsigned int* Calc_GetPointBuferr(int nRun) {
   unsigned int    *pbuferr;

   switch (nRun) {
      case 0:
         pbuferr = calc_buferrrun0;
         break;
      case 1:
         pbuferr = calc_buferrrun1;
         break;
      case 2:
         pbuferr = calc_buferrrun2;
         break;
   }
   return pbuferr;
}
//---------------------------------------------------------------------------
                                        // ����p��� ������p������� ��p����p�� � ����������� ��������
unsigned int Testcalcdata(struct calcdata* uk){// ����p��� ������� ��p����p��
   unsigned int    err,erdP;
   unsigned int    *pbuferr;            // ��������� �� ����p ������ ������� �����
   struct IdRun    *prun;               // ��������� �� ��p����p� �����
   int nRun;
   float fTmp;

   nRun = Time_GetNumrun();

   prun = &CONFIG_IDRUN[nRun];        // ��������� �� ������� �����

   switch (nRun) {
      case 0: pbuferr = calc_buferrrun0; break;
      case 1: pbuferr = calc_buferrrun1; break;
      case 2: pbuferr = calc_buferrrun2; break;
   }

   {                               // ���� - 8.586
                                        // ������p ����p���� ������ 12.5 ��
      err = ( uk->Dd < 12.5 ) ? 0xB6 : 0x36;

      if (err != pbuferr[20]) {
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[20] = err;             // �p������� ����� ��������
      }

      if (err == 0xB6)
         return 1;                      // Q = Qpred
                                        // ������p �p����p����� > 1000.0 ��
      if (uk->typeOtb)                  // 1 - ���������
         err = ( uk->Dt > 1000.54 ) ? 0xB7 : 0x37;
      else
         err = ( uk->Dt > 1000.54 ) ? 0xB8 : 0x38;
      if (err != pbuferr[21]) {
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[21] = err;             // �p������� ����� ��������
      }
      if  (err == 0xB8)
         return 1;                      // Q = Qpred
                                        // ������p �p����p����� < 50.0 ��
      err = ( uk->Dt < 50.0 ) ? 0xB9 : 0x39;

      if (err != pbuferr[22]) {
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[22] = err;             // �p������� ����� ��������
      }

      if (err == 0xB9)
         return 1;                      // Q = Qpred

      if (uk->typeOtb)                  // 1 - ���������
         err = ( uk->Beta > 0.75 ) ? 0xBA : 0x3A;
      else
         err = ( uk->Beta > 0.75 ) ? 0xBC : 0x3C;

      if (err != pbuferr[23]) {
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[23] = err;             // �p������� ����� ��������
      }

      if (err == 0xBA || err == 0xBC)
         return 1;                      // Q = Qpred

                                                 //Beta < 0.1
      if (uk->typeOtb)                  // 1 - ���������
         err = ( uk->Beta < 0.1 ) ? 0xBB : 0x3B;
      else
         err = ( uk->Beta < 0.1 ) ? 0xBD : 0x3D;
      if (err != pbuferr[24] ) {
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[24] = err;             // �p������� ����� ��������
      }
      if (err == 0xBB || err == 0xBD)
         return 1;                      // Q = Qpred

   }
                                        // ������� ������� �� ����
   err = ( uk->Pa == 0.0 ) ? 0xBE : 0x3E;
   if (err != pbuferr[25]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[25] = err;                // �p������� ����� ��������
   }
   if (err == 0xBE)
      return 1;                         // ����p����� � ������� Q=�p��
//printf("DP=%f\n",uk->DP);
   if (uk->Pa > 0.95) {                 // ����������� ��������, ��. ����p���� DP-[���/�2], Pa-[���/��2]
      err = ( uk->DP/uk->Pa > 2500 ) ? 0xBF : 0x3F;

      if ( err != pbuferr[27]) {
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[27] = err;             // �p������� ����� ��������
      }
   }
   if  (FReverse[nRun] == 0)// || (FReverse[nRun] == 3))
   {
     switch(erdP=ChkSensMin(6,uk->DP,prun->dPots)) {
      case 0:  err = 0x02; break;
      case 1:  err = 0x82; break;
      default: err = pbuferr[12];
     }
     if ((err != pbuferr[12]) && (prun->dPots != 0.0)) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[12] = err;                // �p������� ����� ��������
     }
//printf("DP=%f Ots=%f\n",uk->DP, prun->dPots);
    }
     if (uk->DP < prun->dPots)
     {
                                        // ��������� 17.10.03
      uk->DP = 0;                       // �������� ������� ��� ����� ��� ���� ��������
      COPY_CALCDATA[nRun].DP = 0;
      IndDP[nRun] = 0;
       return 2;
     }
     else
      IndDP[nRun] = uk->DP;
   return 0;
}
//---------------------------------------------------------------------------
                                        // p����� p������ �p� ��p������� ��������
unsigned int Calc_CorrQnom(struct calcdata* uk) {
   float value,kQw,Qwm;
   int nRun;
   struct IdRun    *prun;               // ��������� �� ��p����p� �����
   struct AdRun            *aprun;      // ��p����p� �����
   unsigned  int err;

   nRun = Time_GetNumrun();
   prun = &CONFIG_IDRUN[nRun];        // ��������� �� ������� �����
   aprun  = &CONFIG_ADRUN[nRun];      // ������� ����p �����

   value = uk->T * uk->K;
//printf("Qnom P=%f T=%f val=%f \n",uk->Pa,uk->T,value);
   if (value == 0.0)
      return 1;                      // ������� ������� �� ����
      uk->kQind = T_st/1.033227 * uk->Pa / value; // ��������� ��� ����������� � ���������

     uk->Q =  uk->Qw_s*3600.0 * uk->kQind;         // 4) ��-50-213-80
  if (CONFIG_IDRUN[nRun].TypeRun  != 10)
   IndQs[nRun] = uk->Qw;                // �������� ������
//  else
//  {   uk->Qw_s = IndQs[nRun]/3600.0;
//      uk->Q =  uk->Qw_s*3600.0 * uk->kQind;
//  }
float Qwk;
//if (Interval[nRun] > 1)
//{
//   Qwk = uk->Qw/((float)Interval[nRun]);
//}
//else
 Qwk = IndQs[nRun];
                                        // ������� �� Qmin
   if (INTPAR.QminFlag != 0) {          // ���� ������� ���� "������� �� Qmin"
                                        // ���� ������ Qst - Tmin �� �������
					// Qst < Qw < Qmin  � P > Pmin
     if ((prun->dPots < Qwk) && (Qwk < prun->Qmin))  // && uk->Pa >= prun->Pmin)
     {
                                        // ����������� �����  �.�.
	 Qadwc[nRun] = prun->Qmin - Qwk;
                                        // �-� ���������� �������
         kQw = (double)prun->Qmin / (double)Qwk;

         uk->Qw = prun->Qmin;

         value = uk->Q * kQw;           // ����������� ������ c.�.

	 Qadsc[nRun] = value - uk->Q;   // ����������� ������ c.�.

         uk->Q = value;                 // ���������������� c.�.
      }
         else {  //if (prun->dPots < uk->Qw && uk->Qw < prun->Qmin
         Qadsc[nRun] = 0.0;             // ����������� ����� ��
	 Qadwc[nRun] = 0.0;             // ����������� ����� ��
      }
   }
   else { //if (INTPAR.QminFlag != 0)
      Qadsc[nRun] = 0.0;                // ����������� �����
      Qadwc[nRun] = 0.0;                // ����������� ����� ��
   }

   IndQ[nRun] = uk->Q;
   IndDP[nRun] = uk->Qw;
   if ((CONFIG_IDRUN[nRun].TypeRun) == 4 || (CONFIG_IDRUN[nRun].TypeRun) == 5)
   if ((RS485HF[nRun] == 1)&&(startT >= 7*SysTakt))
   {
    if ((aprun->dPh_s > prun->dPots) && (Cnt[nRun].Q < prun->dPots))
    {
      err=0x193;     // �������� ���������� ����
      if (err != errQw[nRun]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      errQw[nRun] = err;                 // �p������� ����� ��������
      }
    }
    else if (Cnt[nRun].Q > prun->dPots)
     {
      err=0x113;
      if (err != errQw[nRun]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      errQw[nRun] = err;                 // �p������� ����� ��������
      }
    }
   }
   uk->FullHeat = uk->Heat * uk->Q;     // ������ ������� ��������

   return 0;
}
//---------------------------------------------------------------------------
//����p������ �������� ������ ��� p������ �� ������ GERG-91
void CopyDataGerg91(struct calcdata* uk) {

   if (GLCONFIG.EDIZM_T.T_SU==1) T_st= 273.15;     //0 ��.� ���  GERG88
   else if (GLCONFIG.EDIZM_T.T_SU==2)  T_st= 288.15;  // 15 ��. �
   else  T_st= 293.15;           // 20 ��. �
   uk->ROc = uk->ROnom*KoRo[GLCONFIG.EDIZM_T.T_SU][0]; // ������ � 20 ��. �
   uk->Xa  = 0.01*uk->NN2;
   uk->Xy  = 0.01*uk->NCO2;
   uk->T  = uk->t + 273.15;
   uk->p_T = uk->Pa / uk->T;

}
//---------------------------------------------------------------------------
static float   Xa[3],Xy[3],Xe[3];
static float   Xe2[3],Xa2[3],Xy2[3];                 // ������� ����������, ������������� > 1 ����
static float   C1[3],C2[3],C3[3],C223[3],C233[3],C[3],Cm[3];
static float   step = 1.0/3.0;
static float   p,b,B0,C0,A1,A0,A2,Z,K;
static float   Zc;                          // �����p ����������� �p� ������p���� ��������
static float   A02,A13;   // ������� ����������, ������������� > 1 ����
static float   T,Bm[3],B1[3],B2[3],B23[3],B3[3];
//---------------------------------------------------------------------------
              // p����� ������������ ����������� ������ �p�p����� ����� �
             // ���������� 0.55 - 1.0 ��/�3
unsigned int CalcCoeffK_GERG_91(struct calcdata* uk) {
   float   a;
   float   ROc;
   float   Me;                          // ����p��� ����� ��������p���
   float   H;                           // �������� �������� ������� ���p���� ���/�3
   float   B;     //B1,B23,B3,B2;
   float   coeff_B1[3];
   float   coeff_C1[3];
   float   T2,H2;   // ������� ����������, ������������� > 1 ����
   int nR;
   if (uk->Pa <= 1.0) {                  // ���� Pabs < 1.0
      uk->K = 1.0;                      // �� �� ������� �, � ������� ��� �� 1.0
      uk->Kgerg = 1.0;
      return 1;
   }
   nR = Time_GetNumrun();
//printf("1-CalcZc[%d]=%d ",nR,CalcZc[nR]);
   ROc = uk->ROc;

   Xa[nR] = uk->Xa;
   Xy[nR] = uk->Xy;
   { a = 0.0741*ROc - 0.006 - 0.063*Xa[nR] - 0.0575*Xy[nR];
     Zc = 1.0 - a*a;                        // (36) ���� 30319.2-96
   }
     uk->Zc = Zc;

   Xe[nR] = 1.0 - Xa[nR] - Xy[nR];                    // (22) ���� 30319.2-96
   Xe2[nR] = Xe[nR]*Xe[nR];
   Xa2[nR] = Xa[nR]*Xa[nR];
   Xy2[nR] = Xy[nR]*Xy[nR];

   if (Xe[nR] == 0.0)
      return 1;
                                        // (35) ���� 30319.2-96
   Me = (24.05525*Zc*ROc - 28.0135*Xa[nR] - 44.01*Xy[nR]) / Xe[nR];
   H = 128.64 + 47.479*Me;
   H2 = H*H;
   uk->H = H;
//printf("H=%f\n",H);
   T = uk->T;
   T2 = T*T;
   coeff_B1[0] = -0.425468 + 2.865e-3*T - 4.62073e-6*T2;
   coeff_B1[1] = 8.77118e-4 - 5.56281e-6*T + 8.815114e-9*T2;
   coeff_B1[2] = -8.24747e-7 + 4.31436e-9*T - 6.08319e-12*T2;
                                        // (23) ���� 30319.2-96
   B1[nR]  = coeff_B1[0] + coeff_B1[1]*H + coeff_B1[2]*H2;
                                        // (24) ���� 30319.2-96
   B2[nR]  = -0.1446 + 7.4091e-4*T - 9.1195e-7*T2;
                                        // (25) ���� 30319.2-96
   B23[nR] = -0.339693 + 1.61176e-3*T - 2.04429e-6*T2;
                                        // (26) ���� 30319.2-96
   B3[nR]  = -0.86834 + 4.0376e-3*T - 5.1657e-6*T2;

   a = 320.0 - T;
   B  = 0.72 + 1.875e-5*a*a;            // (32) ���� 30319.2-96

   if (B1[nR]*B3[nR]<0.0)
      return 1;
                                        // (20) ���� 30319.2-96
   Bm[nR] = Xe2[nR]*B1[nR] + Xe[nR]*Xa[nR]*B*(B1[nR]+B2[nR]);
   return 0;
}
//--------------------------------------------

void K_Gerg91_1(struct calcdata* uk)
{
int nR;float T2, H2, H;
float   coeff_C1[3];
nR = Time_GetNumrun();

   Bm[nR] = Bm[nR] + Xa2[nR]*B2[nR] + 2*Xa[nR]*Xy[nR]*B23[nR] + Xy2[nR]*B3[nR]
             - 1.73*Xe[nR]*Xy[nR]*(float)sqrt(B1[nR]*B3[nR]);

   uk->Bm = Bm[nR];
   T = uk->T;
   T2 = T*T;
   H=uk->H; H2=H*H;
   coeff_C1[0] = -0.302488 + 1.95861e-3*T - 3.16302e-6*T2;
   coeff_C1[1] = 6.46422e-4 - 4.22876e-6*T + 6.88157e-9*T2;
   coeff_C1[2] = -3.32805e-7 + 2.2316e-9*T - 3.67713e-12*T2;
                                        // (27) ���� 30319.2-96
   C1[nR] = coeff_C1[0] + coeff_C1[1]*H + coeff_C1[2]*H2;
//   uk->C1 = C1[nR];
                                        // (28) ���� 30319.2-96
   C2[nR] = 7.8498e-3 - 3.9895e-5*T + 6.1187e-8*T2;
                                        // (29) ���� 30319.2-96
   C3[nR] = 2.0513e-3 + 3.4888e-5*T - 8.3703e-8*T2;
                                        // (30) ���� 30319.2-96
   C223[nR] = 5.52066e-3 - 1.68609e-5*T + 1.57169e-8*T2;
                                        // (31) ���� 30319.2-96
   C233[nR] = 3.58783e-3 + 8.06674e-6*T - 3.25798e-8*T2;

   C[nR]    = 0.92 + 0.0013*(T - 270.0);      // (33) ���� 30319.2-96
if (C1[nR] < 0) C1[nR] = 0.00005;
}
//-------------------------------------------------------------

void K_Gerg91_2() {
int nRun;
   nRun = Time_GetNumrun();
   Cm[nRun]   = Xe2[nRun]*Xe[nRun]*C1[nRun]
           + 3*Xe2[nRun]*Xa[nRun]*C[nRun]*(float)pow(C1[nRun]*C1[nRun]*C2[nRun],step)
             + 2.76*Xe2[nRun]*Xy[nRun]*(float)pow(C1[nRun]*C1[nRun]*C3[nRun],step)
               + 3*Xe[nRun]*Xa2[nRun]*C[nRun]*(float)pow(C1[nRun]*C2[nRun]*C2[nRun],step);
}

//-------------------------------------------------------------

void K_Gerg91_22(struct calcdata* uk) {
int nRun;
   nRun = Time_GetNumrun();
   Cm[nRun]   = Cm[nRun]
                 + 6.6*Xe[nRun]*Xa[nRun]*Xy[nRun]*(float)pow(C1[nRun]*C2[nRun]*C3[nRun],step)
                   + 2.76*Xe[nRun]*Xy2[nRun]*(float)pow(C1[nRun]*C3[nRun]*C3[nRun],step)
                     + Xa2[nRun]*Xa[nRun]*C2[nRun]
                       + 3*Xa2[nRun]*Xy[nRun]*C223[nRun]
                         + 3*Xa[nRun]*Xy2[nRun]*C233[nRun]
         + Xy2[nRun]*Xy[nRun]*C3[nRun];                   // (21) ���� 30319.2-96

   uk->Cm = Cm[nRun];
//printf("Cm=%f\n",Cm[nRun]);
}

//-------------------------------------------------------------
unsigned int K_Gerg91_3(struct calcdata* uk, int nR) {
float a;
{
   p = 98.0665e-3 * uk->Pa;             // ��p���� �� ���/��2 � ���
   b = 1000.0*p/(2.7715*uk->T);             // ���(43) ���� 30319.2-96
//printf("Pa=%f MPa T=%f K\n",p,uk->T);
}
   B0 = b *uk->Bm;                         // (41) ���� 30319.2-96
   C0 = b * b * uk->Cm;                     // (42) ���� 30319.2-96

   A1 = 1.0 + B0;                         // (40) ���� 30319.2-96
   A0 = 1.0 + 1.5*(B0 + C0);              // (39) ���� 30319.2-96
   A02 = A0*A0;
   A13 = A1*A1*A1;

   if ((A02 - A13)<0.0)
     { return 1;}
   a= A0 - sqrt(A02 - A13);                                  // (38) ���� 30319.2-96
   if (fabs(a) < 1.0e-7)
    { if (fabs(A1)<1.0e-7) A2 = A1+1.0e-7;
      else A2 = A1*0.9;
    }
   else
    if (a>0.0)
     A2 = pow(a,step);
    else
     A2 = -pow(-a,step);
   Z  = (1.0 + A2 + A1/A2)/3.0;         // (37) ���� 30319.2-96 �����p ����������� �p� p.�.
   {
     if (uk->Zc < 0.01)  return 1;
//if (Time_GetNumrun()==2)
     uk->Z = Z;
     K = Z/uk->Zc;                            // (44) ���� 30319.2-96 ����. �����������
     uk->Kgerg = K;
     uk->K = K;
    }
   return 0;
}
//---------------------------------------------------------------------------
//p����� ������������ ����������� ������ �p�p����� ����� � ���������� 0.67 - 0.7 ��/�3
unsigned int CalcCoeffK_NX19(struct calcdata* uk) {

   float   pressure,ROc,Xa,Xy,ppk,Tpk,pp,Tp,Ta,pa,dTa,dTa2,F,T;
   float   a0,a1,B0,B1,B2,Z,Zc,K,teta0,teta1;
   int nR;

   if (uk->Pa < 1.0) {                  // ���� Pabs < 1.0
      uk->K = 1.0;                      // �� �� ������� �, � ������� ��� �� 1.0
      return 0;
   }
   nR = Time_GetNumrun();
   { T  = uk->T;
     pp = uk->Pa;
   }
   pressure = pp * 98.0665e-3;      // ��p���� �� ���/��2 � ���
   ROc = uk->ROc;
   Xa  = 0.01*uk->NN2;                  // ����p��� ���� �����
   Xy  = 0.01*uk->NCO2;                 // ����p��� ���� �������� ����p���

                                        // �������p��������� ��������
   ppk = 2.9585 * (1.608 - 0.05994*ROc + Xy - 0.392*Xa); //(17) ���� 30319.2-96
                                        // �������p��������� �����p���p�
   Tpk = 88.25*(0.9915 + 1.759*ROc - Xy - 1.681*Xa);     //(18) ���� 30319.2-96
                                        // �p��������� ��������
   pp = pressure / ppk;                                  //(46) ���� 30319.1-96
                                        // �p��������� �����p���p�
   Tp = T / Tpk;                                     //(47) ���� 30319.1-96

                                        // ��p����p Ta
   Ta = 0.71892 * Tp + 0.0007;                           //(16) ���� 30319.2-96
                                        // ��p����p pa
   pa = 0.6714 * pp + 0.0147;                            //(15) ���� 30319.2-96
   dTa = Ta - 1.09;
   dTa2 = dTa*dTa;

   F = 75.0e-5*(float)pow(pa,2.3);

   if (pa >= 0.0 && pa <= 2.0 && dTa >= 0.0 && dTa <= 0.3) {
      F /= (float)exp(20.0*dTa);
      a0 = sqrt(dTa);
      a1 = pa * (2.17 - pa + 1.4 * a0);
      F += 11.0e-4 * a0 * a1 * a1;                       //(12) ���� 30319.2-96
   }

   if (pa >= 0.0 && pa < 1.3 && (dTa >= -0.25) && dTa < 0.0) {
      F *= 2.0 - (float)exp(20.0*dTa);
      a0 = dTa2 * dTa2;             //dTa ** 4
      F += 1.317 * pa * (1.69 - pa * pa) * a0;           //(13) ���� 30319.2-96
   }

   if (pa >= 1.3 && pa < 2.0 && (dTa >= -0.21 ) && dTa < 0.0) {
      F *= 2.0 - (float)exp(20.0*dTa);
      a0 = 0.455 * ( 1.3 - pa );
      a1 = 1.69 * (float)pow(2,1.25) - pa * pa;
      B0 = dTa * (0.03249 + 18.028 * dTa2);
      B1 = dTa2*( 2.0167 + dTa2*(42.844 + 200.0*dTa2));
      F += a0 * a1 * (B0 + B1);                            //(14) ���� 30319.2-96
   }

   a0 = Ta * Ta;
   teta1 = a0*a0*Ta / (a0 *(6.60756*Ta-4.42646)+3.22706);  //(11) ���� 30319.2-96
   teta0 = (a0*(1.77218-0.8879*Ta)+0.305131)*teta1/(a0*a0);//(10) ���� 30319.2-96

   B1 = 2.0 * teta1 / 3.0 - teta0 * teta0;                 //(9) ���� 30319.2-96
   B0 = teta0*(teta1-teta0*teta0) + 0.1*teta1*pa*(F-1.0);  //(8) ���� 30319.2-96
   B2 = sqrt(B0*B0+B1*B1*B1);
   B2 = (float)pow(B0+B2,1.0/3.0);                         //(7) ���� 30319.2-96

   Z  = 1.0 + 0.00132/pow(Ta,3.25);
   Z *= Z;
   Z *= 0.1*pa;
                                        /* �����p ����������� */
   Z /= B1/B2 - B2 + teta0;             //(6) ���� 30319.2-96
    {
      uk->Z = Z;
                    // �����p ����������� �p� ������p���� ��������
     { a0 = 0.0741*ROc - 0.006 - 0.063*Xa - 0.0575*Xy;
       Zc = 1.0 - a0*a0;                      //(36) ���� 30319.2-96
       uk->Zc = Zc;
//printf("1-20�� NX Zc=%f Z=%f P=%f T=%f\n",uk->Zc,Z,pressure,T);
     }
      K = Z / uk->Zc;                          //(44) ���� 30319.2-96
      uk->K = K;                           // ����������� ����������� �p�p������ ����
    }
//printf("NX Zc=%f Z=%f K=%f P=%f T=%f\n",uk->Zc,Z,K,pressure,T);

   return 0;
}
//---------------------------------------------------------------------------
// ������������ ��� ��������� �����.����������, ����� �� ������������ ������
void SetCopyCalcData(unsigned char nRun) {

     COPY_CALCDATA[nRun] = CALCDATA[nRun];
}
//---------------------------------------------------------------------------
// ������������ ��� ��������� �����.����������, ����� �� ������������ ������
void SetCopyCalcData_1(unsigned char nRun) {

     COPY_CALCDATA_1[nRun] = CALCDATA[nRun];
}
//---------------------------------------------------------------------------
void SetDensChromData(int nRun) {
   struct IdRun            *prun;       // ��p����p� �����
   struct AdRun            *aprun;      // ��p����p� �����
   struct calcdata         *pcalc;
   float                   var;
   int i;

   prun  = &CONFIG_IDRUN[nRun];       // ������� ����p �����
   aprun  = &CONFIG_ADRUN[nRun];      // ������� ����p �����
   pcalc = &CALCDATA[nRun];
/*   if (prun->Dens == 1) {               // ��������� ����������
      pcalc->ROnom = prun->ROnom;    // ��������� - ����.��������
      if (prun->constdata& 0x10)        // c ����������
        { if (!FreqAlarm && !Err485) // ��� ������ ����������
          if (aprun->ROdens*KoRo[GLCONFIG.EDIZM_T.T_SU][0] > 0.66)   //   ROdens ��� T_SU �� � 20 ��
          pcalc->ROnom = aprun->ROdens;  // c  ���������� � T_SU
        }
      pcalc->NCO2 = prun->NCO2;         // ��������� - ����.��������
      pcalc->NN2  = prun->NN2;          // ��������� - ����.��������
      var = CalcHeat(nRun);  //  � ���  ��� T_SU
      if ((prun->nKcompress == 0)||(prun->nKcompress == 1)) // Gerg91 ��� NX19
      { if (prun->constdata & 0x20)   // ��������� �� ��.������� ���
         pcalc->Heat = var;     // ������ ������� �� ����������
       else
         pcalc->Heat = prun->Heat;      // ��������� - ����.��������
      }
      else  pcalc->Heat = prun->Heat;   // GERG88, AGA8
   }
   else
*/
   {                       // ��� �� ����������, �� ������
      pcalc->ROnom = prun->ROnom;       // ��������� - ����.��������
      pcalc->NCO2 = prun->NCO2;         // ��������� - ����.��������
      pcalc->NN2  = prun->NN2;          // ��������� - ����.��������
     pcalc->Heat = prun->Heat;   //GERG88, AGA8
//printf("NN2=%f\n",pcalc->NN2);
   }
}
//---------------------------------------------------------------------------
void SetCalcData() {                    // ���������� ������� ����p �����
   struct IdRun    *prun;               // ��p����p� �����
   struct AdRun    *aprun;              // ��p����p� �����
   struct calcdata *pcalc;
   float           var;
   unsigned char   nRun,i;
   struct hartdata   *phartdata;

   nRun = Time_GetNumrun();
   prun  = &CONFIG_IDRUN[nRun];       // ������� ����p �����
   aprun  = &CONFIG_ADRUN[nRun];      // ������� ����p �����
   pcalc = &CALCDATA[nRun];
   pcalc->run = nRun;
   SetDensChromData(nRun);    
   if ((prun->TypeRun) == 8 || (prun->TypeRun) == 9)
    pcalc->ROnom = prun->ROnom;
   else
    CopyDataGerg91(pcalc);
//  AnalogToCalc(nRun);
                                        // �������� ���������\������ [0\1] - ��p����
   if (prun->constdata & 0x01) {        // ������ ��p�����
      switch(prun->TypeRun) {           // ������ ����p���� �� �����
         case 0:                        // 0 - 3095
         case 1:                        // 1 - P+T+dP
         case 6:                        // 6 - (P,T)+dP
         case 7:                        // 7 - (P,dP)+T
                                        // ������� ������ ��������� �������� �������� � ���/�2
                                        // ��������� �� ���-WinSys
            if ((prun->sensData[2].nInterface != tiAN_WS) &&
               (prun->sensData[2].nInterface !=tiAN_RS485))
            {
               if (SetShelf[nRun] == 1)
                  pcalc->DP = prun->dPhmin;
               else
                                        // ������������ dPh
                  pcalc->DP = GetCalibrValue(nRun,2,aprun->dPh);
//printf("dPh=%f DP=%f nRun=%d\n",aprun->dPh,pcalc->DP, nRun);
            }
            else
               if (SetShelf[nRun] == 1)
                  pcalc->DP = prun->dPhmin;      //11.01.08 ������� dPmin
               else
               pcalc->DP = aprun->dPh;  // ���������� ��� ������.

            break;

         case 3:                        // 3 - P+T+dP+dp
         case 2:                        // ������� ������ ��������� �������� �������� � ���/�2
         case 11:                      // ��������� �� ���-WinSys
            if ((prun->sensData[2].nInterface != tiAN_WS) &&
               (prun->sensData[2].nInterface !=tiAN_RS485))
             {
                                        // ������������ dPh
               if (SetShelf[nRun] == 1)
                  var = prun->dPhmin;
               else
                  var = GetCalibrValue(nRun,2,aprun->dPh);
            }
            else
               if (SetShelf[nRun] == 1)
                  var = prun->dPhmin;   //11.01.08 ������� dPmin
               else
               var = aprun->dPh;        // ���������� ��� ������.

               pcalc->DP = var;         // ��p���� ��p����
//printf("dPh=%f dPl=%f statdPl=%X var=%f\n",aprun->dPh,aprun->dPl,aprun->statdPl, var);
            if (var < prun->ValueSwitch)// ������ ������ ������������
              if (aprun->statdPl  == 0)  // ��� ������ �� HART dPl
            {
                                        // ������� ������ ��������� ������� �������� � ���/�2
                                        // ��������� �� ���-WinSys
               if ((prun->sensData[3].nInterface != tiAN_WS) &&
                  (prun->sensData[3].nInterface !=tiAN_RS485))
                {
                  if (SetShelf[nRun] == 1)
                     pcalc->DP = prun->dPhmin;
                  else                  // ������������ dPl
                     pcalc->DP = GetCalibrValue(nRun,3,aprun->dPl);
               }
               else                     // ���������� ��� ������.
                  if (SetShelf[nRun] == 1)
                     pcalc->DP = prun->dPhmin;   //11.01.08 ������� dPmin
                  else                  // ������������ dPl
                     pcalc->DP = aprun->dPl;
            }

            break;

         case 4:                        // 4 - �������+P+T
         case 5:                        // 5 - ������� ��
         case 10:                        // 10 - ������� ���
            break;
      }
   }
   else {  //if (prun->constdata & 0x01) {
      pcalc->DP = prun->constdP + NumZap[nRun];        // ��������� ��p����� ���/�2
    if (((prun->TypeRun) != 8) && ((prun->TypeRun) != 9))
    {
      if (GetDpMinShelf() != 0) {       // ++++++++++++++++++
                                        // 16.11.05 ��� ��������
         if (((prun->TypeRun) != 3)&&((prun->TypeRun) != 2))
          {         // ������ ����p���� �� �����
            if (prun->constdP > prun->dPots && prun->constdP < prun->dPhmin)
             {
               pcalc->DP = prun->dPhmin;
            }
         }
         else {
            if (prun->constdP > prun->dPots && prun->constdP < prun->dPhmin)
            {
               pcalc->DP = prun->dPhmin;
            }
         }
      }
   }
  }
   if (prun->constdata & 0x02) {        // ������ ��������
      if ((prun->TypeRun == 8) || (prun->TypeRun == 9))
     {         // ������ ����p���� �� �����
         pcalc->Pin = aprun->P;
//printf("pcalc->Pin =%f\n",pcalc->Pin);
      }                                 // ������� ������ ��������� �������� � ���/��2
      else
                                        // ��������� �� ���-WinSys
      if ((prun->sensData[1].nInterface != tiAN_WS) &&
         (prun->sensData[1].nInterface !=tiAN_RS485))
       {
         if (SetShelfP[nRun] == 1)
            pcalc->Pin = prun->Pmin;
         else                               // ������������ P
            pcalc->Pin = GetCalibrValue(nRun,1,aprun->P);
//printf("ap-P=%f Pin=%f nRun=%d edp=%02X\n",aprun->P,pcalc->Pin, nRun,aprun->edp);
      }
      else
         if (SetShelfP[nRun] == 1)
            pcalc->Pin = prun->Pmin;  //11.01.08 ������� Pmin
         else
            pcalc->Pin = aprun->P;
//printf("nRun= %d Pin= %f\n",nRun,pcalc->Pin);
   }
   else {
      pcalc->Pin = prun->constP;        // ���������
    if ((prun->TypeRun) != 8 && (prun->TypeRun) != 9)
    {  if (GetDpMinShelf() != 0) {       // ++++++++++++++++++
         if (prun->constP > 1.04 && prun->constP < prun->Pmin) {
            pcalc->Pin = prun->Pmin;
         }
      }
   }
   }
   if (prun->constdata & 0x04) {        // ������ �����p���p�
                                        // ��������� �� ���-WinSys
      if ((prun->TypeRun == 8) || (prun->TypeRun == 9))
      {         // ������ ����p���� �� �����
         pcalc->t = aprun->t;           // Micromotion & Rotamass
      }
      else                              // ��������� �� ���-WinSys
      if ((prun->sensData[0].nInterface != tiAN_WS) &&
      (prun->sensData[0].nInterface !=tiAN_RS485))
                                        // ������������ �
         pcalc->t = GetCalibrValue(nRun,0,aprun->t);
      else
         pcalc->t = aprun->t;
   }
   else
      pcalc->t = prun->constt;          // ���������
//pcalc->t = Th;
//////////////////////////////////////////////////////////
                                        // ����� ���. �������p���� - ��� p������
   pcalc->TypeCount = GLCONFIG.TypeCount;
   pcalc->typeP     = prun->TypeP;      // ��� ������� �������� - 0\1 ���.\�����.
   pcalc->typeOtb   = prun->typeOtb;    // 0 - �������, 1 - ���������

   pcalc->Pb = prun->Pb * 1.359509e-3;  // �����. ��������, �� "�� p�. ��." � "���/c�2"
   pcalc->Pa = pcalc->Pin;              // ���. ��������

   if (pcalc->typeP)
      pcalc->Pa += pcalc->Pb;           // ���� ���������� ��������

   pcalc->Dt20   = prun->D20;           // ����p����� ������p �p���, ��
   pcalc->Dd20   = prun->d20;           // ����p����� ������p ����p����, ��

      pcalc->R      = prun->R/3.14159265;

   pcalc->Bd  = prun->Bd;  // ����-�� ���-�� ����-� ��������� ���������, ���^-1(��� A)
   pcalc->kAd = prun->Bd;
   pcalc->kBd = prun->kBd;              // ����-�� B
   pcalc->kCd = prun->kCd;              // ����-�� C

   pcalc->Bt  = prun->Bt;  // ����-�� ���-�� ����-� ��������� �p���, ���^-1(��� A)
   pcalc->kAt = prun->Bt;
   pcalc->kBt = prun->kBt;              // ����-�� B
   pcalc->kCt = prun->kCt;              // ����-�� C

                                        // ��� ������ ����
   pcalc->taupp  = prun->TauP;          // ������������� �������� � ���������-�� C
   pcalc->Rn     = prun->Rn;            // ������ ���������� ������� ������
//printf("const[%d]=%X\n",nRun,prun->constdata);
}
//---------------------------------------------------------------------------
void Calc_CorrSetCalcData() {           // ���������� ������� ����p �����
   struct IdRun    *prun;               // ��p����p� �����
   struct AdRun    *aprun;              // ��p����p� �����
   struct calcdata *pcalc;
   float           var;
   unsigned char nRun;
   unsigned long int t3,ts,tls;
   struct memdata *pm;

   nRun = Time_GetNumrun();
   pm = &MEMDATA;
//printf("Calc_CorrSetCalcData=%d\n",counttime);
   prun  = &CONFIG_IDRUN[nRun];       // ��������� �� ��p����p� �����
   aprun  = &CONFIG_ADRUN[nRun];      // ��������� �� ��p����p� �����
   pcalc = &CALCDATA[nRun];
//ComPrint("C1");
//   AnalogToCalc(nRun);
   if (prun->constdata & 0x02) {        // ������ ��������
					// ������� ������ ��������� �������� � ���/��2
					// ��������� �� ���-WinSys
      if ((prun->sensData[1].nInterface != tiAN_WS) &&
	    (prun->sensData[1].nInterface !=tiAN_RS485))
					// ������������ P
	 pcalc->Pin = GetCalibrValue(nRun,1,aprun->P);
      else
	 pcalc->Pin = aprun->P;
   }
   else
      pcalc->Pin = prun->constP;        // ���������
//printf("Pin=%f\n",pcalc->Pin);
   if (prun->constdata & 0x04) {        // ������ �����p���p�
					// ��������� �� ���-WinSys
      if ((prun->sensData[0].nInterface != tiAN_WS) &&
	    (prun->sensData[0].nInterface !=tiAN_RS485))
					// ������������ �
	 pcalc->t = GetCalibrValue(nRun,0,aprun->t);
      else
	 pcalc->t = aprun->t;
   }
   else
      pcalc->t = prun->constt;          // ���������
//printf("t=%f\n",pcalc->t);
					// ������ ������
   if (prun->constdata & 0x08)
    {                   // ���������
      pcalc->Qw = prun->Qw;
      pcalc->Qw_s = aprun->Qw_s;
    }
   else     // ���������
    {// printf("FirstC[%d]=%d\n",nRun,FirstC[nRun]);
     if (FirstC[nRun])              //������ ���� ��� ����� run  ��
      { if (CONFIG_IDRUN[nRun].TypeRun != 10)   // �������
         {
//            if (ts2 <= 30)
//             pcalc->Qw = prun->constqw*ts2;
//            else
             pcalc->Qw = prun->constqw;
         }
				 else pcalc->Qw = prun->constqw;
/*        else   // ���
        {
             unsigned char page = Mem_CyklGetPage(nRun);
             if (Mem_DayRdEnd(page))
             { tls = BinToSec(pm->STARTTIME);
               t3 =(BinToSec(BINTIME)-tls);
              if (t3 <= 30)
                pcalc->Qw = prun->constqw*t3/SysTakt;
              else
                pcalc->Qw = prun->constqw;
             }
             else // ������ ������  Mem_DayRdEnd_1
                pcalc->Qw = prun->constqw;
        }
*/
     FirstC[nRun]=0;
    }
    else    // �� ������ ����
        pcalc->Qw = prun->constqw;
    pcalc->Qw_s = pcalc->Qw/3600.0;
    }					// ����� ���. �������p���� - ��� p������
//   SetamountPT(nRun);

   pcalc->TypeCount = GLCONFIG.TypeCount;
   pcalc->typeP = prun->TypeP;          // ��� ������� �������� - 0\1 ���.\�����.

   pcalc->Pb = prun->Pb * 1.359509e-3;  // �����. ��������, �� "�� p�. ��." � "���/c�2"
   pcalc->Pa = pcalc->Pin;              // ���. ��������

   if (pcalc->typeP)
      pcalc->Pa += pcalc->Pb;           // ���� ���������� ��������

//////////////////////////////////////////////////////////////////

   SetDensChromData(nRun);

//////////////////////////////////////////////////////////////////

                                        // ��� ������ ����
   pcalc->taupp  = prun->TauP;          // ������������� �������� � ���������-�� C
   pcalc->Rn     = prun->Rn;            // ������ ���������� ������� ������
}
//---------------------------------------------------------------------------
void Task_Calc_CorrSetCalcData() {      // ������ p������ �p� ��������
   struct IdRun            *prun;       // ��������� �� ��p����p� �����
   struct AdRun            *aprun;
   int nRun,n,Al_sms0,Al_sms1;
   unsigned int err,ce;
   struct calcdata *pcalc;
   char s,s1,s2;
   char u1,u2;

 nRun = Time_GetNumrun();
//printf("CorSet nRun=%d Z_in=%d c = %d \n",nRun,Z_in, counttime);
   pcalc = &CALCDATA[nRun];
   prun  = &CONFIG_IDRUN[nRun];
   aprun  = &CONFIG_ADRUN[nRun];
   pcalc->run = nRun;

//   Hart_SetHartData(nRun);            // ���������� ������ �� ������ �� ��p����p� HART-��������
/*   if (rs485num>0)  //&&(RS485HF[nRun] == 0))
    RS485_Set485Data(nRun);            // ���������� ������ �� ������ �� ��p����p� 485-��������
*/
  if ((CONFIG_IDRUN[nRun].TypeRun) != 10)
   {
//      Time_SetCountData(nRun);        // ����p���� ������ �� ��������� ��������
   }
    else
    {
     if (RS485HF[nRun] == 0) {prun->Qw=0.0; IndQs[nRun]=0.0;
     aprun->Qw_s = 0; pcalc->Qw_s=0;}
     Cnt[nRun].QRaw= prun->Qw;
     Cnt[nRun].Q = IndQs[nRun];
    }
   calcerr = GetEdErr(nRun);
//printf("calcerr_Ed=%d nRun=%d\n",calcerr,nRun);
   calcerr = Mem_AlarmSetValErr(nRun);  // ����p��� ��. ��p��. �������� - ��. ����p���� � ��������

 // ������ ������ ����p���� ���
if (nRun == 0)
    ZapuskUZS(nRun); //������ ������ ��� �� MODBUS
/*   struct memdata *pm;
   unsigned char page;

   pm = &MEMDATA;
      if ((FirstC[nRun] == 0x0F) || (FirstC[nRun] == 2))
    {  
       Mem_RdEnd(SEC,nRun); // �� ������������ ���
       Psr[nRun]=pm->p;
       Tsr[nRun]=pm->t;
    }

    if ((FirstC[nRun] == 2) && (CONFIG_IDRUN[nRun].TypeRun == 10) ||
      (FirstC[nRun]==0x0F) && (CONFIG_IDRUN[nRun].TypeRun != 10))
    {
//printf("2-P1=%f T1=%f Pin=%f t=%f\n",Psr[nRun],Tsr[nRun],aprun->P,aprun->t);
       if (aprun->P > 0.0)
       aprun->P = (Psr[nRun] + aprun->P)/2;
       if (aprun->t != 0.0)
       aprun->t = (Tsr[nRun] + aprun->t)/2;
   if (prun->constdata & 0x08)
       FirstC[nRun] = 0;
    }
    else
    if ((FirstC[nRun] == 0x0F) && (CONFIG_IDRUN[nRun].TypeRun == 10))
     { // ������ ����� ��� ����� ������, ������� ��� �� ����
         GlPrint();                // ����� �� ��� ��������p
       NoWriteCycle[nRun]=1;
     }
*/
      Calc_CorrSetCalcData();              // ���������� ��室��� ������ ��� p����
                                        // ����p�頥� ���� �訡��, �����뢠�� � ��p��� ���p���� ᮮ�饭��
      calcerr = Calc_CorrTestInData(nRun);
      if (calcerr) goto COR;
//printf("TestInData nRun=%d c=%d\n",nRun,counttime);

      calcerr = Task_Calc_CorrCoeffK();  // �த������� ����
      if (calcerr)  goto COR;

//      Task_Calc_CorrQnom();
      calcerr = Calc_CorrQnom(pcalc);      // 1-����⪠ ���. �� ����
//memset(str90,0,40);
//sprintf((char*)str90,"CorrEND nRun=%d t_17=%d\n\r",kT,t_10);
//HAL_GPIO_WritePin(GPIOD, U2_RTS_Pin, GPIO_PIN_SET);   //RTS USART1 set  GPIO_PIN_4
//HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),1000);

COR:   Task_Calc_CorrEnd();
  }
//---------------------------------------------------------------------------
/*
void Task_Calc_CorrTestInData() {
   int nRun;
//printf("InData CalcTakt = %d \n",CalcTakt);
   nRun = Time_GetNumrun();
//ComPrint("C2");
   Calc_CorrSetCalcData();              // ���������� �������� ������ ��� p������

                                        // ����p����� ���� ������, ���������� � ��p��� ���p����� ���������
      calcerr = Calc_CorrTestInData(nRun);
//printf("TestInData nRun=%d c=%d\n",nRun,counttime);
 // �� ����� ������� ��� �� AGA8 �� �������� Task_Calc_CorrCoeffK
     if (Z_in[nRun] != 2)
     {//  printf("CounZ Z_Out=%d\n",Z_Out[nRun]);
      Task_Calc_CorrCoeffK();  // ����������� �������
     }
   else                                // ����� �� �p������� ��p����p�
      Task_Calc_CorrQnom();
}*/
//---------------------------------------------------------------------------
// ����������� ����������� ��� ��������
int Task_Calc_CorrCoeffK() {
   unsigned int *pbuferr;
   int err,nRun,k;
//   char far *pcXI,*pcZc;
   struct calcdata* uk;

//printf("Coeffk CalcTakt = %d \n",CalcTakt);
   nRun = Time_GetNumrun();
//memset(str90,0,40);
//sprintf((char*)str90,"BEG Corr nRun=%d t_10=%d\n\r",kT,t_17);
//HAL_GPIO_WritePin(GPIOD, U2_RTS_Pin, GPIO_PIN_SET);   //RTS USART1 set  GPIO_PIN_4
//HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),1000);

   sch = (((CONFIG_IDRUN[nRun].TypeRun) == 4) || ((CONFIG_IDRUN[nRun].TypeRun) == 5)
         || ((CONFIG_IDRUN[nRun].TypeRun) == 10));
   uk = &CALCDATA[nRun];                // ������ �������� ����p� �����

    calcerr = 0;
   if (FirstCalc < GLCONFIG.NumRun)       // 1-� ᥪ㭤�
    FirstCalc++;

    if (CONFIG_IDRUN[nRun].Dens)         // ���� ���⭮���
        CalcZc[nRun] = 1;
      CopyDataGerg91(uk);            // ����p������ �������� ������ ��� p������ �� ������ GERG-91
   if (CONFIG_IDRUN[nRun].nKcompress == 2)
    { // calcerr = 0;
       if ((Zc1[nRun] == 1) || (CalcBR[nRun] == 1))
          TaskCalc_SGERG88();        // ���� �� GERG88
       calcerr = TaskCalc_SGERG88();
    }
   else
   if (CONFIG_IDRUN[nRun].nKcompress == 1)
   {      // ��� GERG91 p����� ������������ ����������� ������ �p�p����� ����� �� ������ GERG-91
      calcerr = 0;
      calcerr = TaskCalc_K_8_586G();
   }  // ��� GERG-91
   else                     // NX-19
    if (CONFIG_IDRUN[nRun].nKcompress == 0)  // NX-19
       calcerr = TaskCalc_K_8_586N();
    else
  {//printf("Count nRun=%d nKcompress=%d FirstCalc=%d\n",nRun, GLCONFIG.IDRUN[nRun].nKcompress, FirstCalc);
   if (CONFIG_IDRUN[nRun].nKcompress == 3)  // AGA8
   {
         calcerr =  TaskCalc_AGA8();
     }   //����� if (CONFIG_IDRUN[nRun].nKcompress == 3)
     else  // ���� nKcompress > 3 ��� < 0
     return 1;
//       Task_Calc_CorrEnd();
  }
      if (calcerr)
       return 1;
      else
      {
//sprintf((char*)str90,"END Corr nRun=%d t_17=%d\n\r",kT,t_17);
//HAL_GPIO_WritePin(GPIOD, U2_RTS_Pin, GPIO_PIN_SET);   //RTS USART2 set  GPIO_PIN_4
//HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),1000);
      return 0;
  }
}
//---------------------------------------------------------------------------
/*void Task_Calc_CorrQnom() {
   struct calcdata *pcalc;
   int nRun;
   nRun = Time_GetNumrun();
   pcalc = &CALCDATA[nRun];             // ������ �������� ����p� �����
//printf("CorrQnom nRun=%d Z=%f K=%f Pa=%f c=%d\n",nRun,pcalc->Z,pcalc->K,pcalc->Pa);
   calcerr = Calc_CorrQnom(pcalc);      // 1-������� ���. �� ����
//if (Z_in[nRun]==2)
//  {// printf("Qnom Z_in=%d\n",counttime);
//      Task_Calc_CorrEnd();
//  }
//else
 //     Task_Calc_CorrEnd();
}
*/
//---------------------------------------------------------------------------
void Task_Calc_CorrEnd() {
   struct calcdata *pcalc;
   extern struct calcdata COPY_CALCDATA[];
   int nRun,k;
   struct memdata *pm;

   nRun = Time_GetNumrun();
   pcalc = &CALCDATA[nRun];             // ������ �������� ����p� �����
    pm = &MEMDATA;
    calcerr = 0;
    k = counttime;
   if (calcerr == 1) {
      if (GetQ(nRun) == 0) {            // ���� ��������� ���
	 pcalc->Q = 0.0;                // ������ ��������
	 IndQ[nRun] = pcalc->Q;
      }
   }
if (calcerr == 2) {
      pcalc->Q  = 0.0;
      IndQ[nRun] = pcalc->Q;
   }
//unsigned char *s1,*s2,i2;
//unsigned char st[6],en[6];
   P23K256_Write_Data(&hspi3,&AdrCALC[nRun][0], (uint8_t*)&CALCDATA[nRun], SizeCALC);   
   TaskWriteArcData();
//   uint16_t poi = pArchiv.di2s[nRun]-30;
//   Mem_SecDayRd(SEC,nRun, poi);
//   uint8_t m1 = MEMDATA.STARTTIME.minutes;
//   uint8_t s = MEMDATA.STARTTIME.seconds;
//   sprintf(str,"t_17=%d di2=%d poi=%d ,m1=%d s=%d\n\r",t_17,pArchiv.di2s[nRun],poi,m1,s);
//   HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,sizeof(str));
   
//s1 = (unsigned char *)&MEMDATA.STARTTIME; 
//for (i2=0; i2<6; i2++) 
//{  st[i2] = *(s1+i2);
//}
//  Mem_SecDayRd(SEC,nRun, pArchiv.di2s[nRun]);  
//// ReadDataW25_IT(SEC, nRun, pArchiv.di2s[nRun],(uint8_t*)&MEMDATA); 
//s2 = (unsigned char *)&MEMDATA.STARTTIME;   
//for (i2=0; i2<6; i2++) 
//{ 
//   en[i2] = *(s2+i2);
//}
//if (memcmp(st,en,6) !=0)
//{         
//sprintf(str,"NoWrite t_17=%d di2=%d\n\r",t_17,pArchiv.di2s[nRun]);
////   HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,sizeof(str));
//    strcat(str,"\0");
//   HAL_UART_Transmit_IT(&huart2,(uint8_t*)str,sizeof(str));
//}
//else
//{ sprintf(str,"t_17=%d di2=%d\n\r",t_17,pArchiv.di2s[nRun]);
//    strcat(str,"\0");
//   HAL_UART_Transmit_IT(&huart2,(uint8_t*)str,sizeof(str));
//}   
    
kT++;  //������� �� ��������� �����
   if (kT < GLCONFIG.NumRun)
   { 
     InQueue(2, Task_Start);
   } 
   else  kT = 0;
    glnumrun = kT;
}
//---------------------------------------------------------------------------
void TaskSetCalcData() {                // ������ p������ �p� ����p����
   struct IdRun    *prun;               // ��������� �� ��p����p� �����
   struct AdRun    *aprun;              // ��������� �� ��p����p� �����
   int i,nRun,kR,nR;
   int err;
   char s,s1,s2;
   char u1,u2;
   struct calcdata         *pcalc;

   nRun = Time_GetNumrun();
   CalcReadyFlag[nRun] = 0;
   prun = &CONFIG_IDRUN[nRun];        // ��������� �� ������� �����
   aprun = &CONFIG_ADRUN[nRun];       // ��������� �� ������� �����
   pcalc = &CALCDATA[nRun];
//printf("TaskSetCalcData nRun=%d\n",nRun);
//  Hart_SetHartData(nRun);            // ���������� ������ �� ��⪠� �� ��p���p� HART-���稪��
//sprintf((char*)str90,"BEGIN D nRun=%d t_17=%d\n\r",kT,t_17);
//HAL_GPIO_WritePin(GPIOD, U2_RTS_Pin, GPIO_PIN_SET);   //RTS USART1 set  GPIO_PIN_4
//HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),1000);
   
    if (rs485num>0)  //&&(RS485HF[nRun] == 0))
    RS485_Set485Data(nRun);            // ���������� ������ �� ������ �� ��p����p� 485-��������
    SetCalcData();                    // ���������� �������� ������ ��� p������
//  if (Z_in[nRun] == 2) goto CN;
  // printf("SU Z_Out=%d c=%d\n",Z_Out[nRun],counttime);
    calcerr = 0;
//    TaskCalcDiam();
      pcalc->Ksh = 1.0;
                                  // ���� - �� ������ �����
      calcerr = Calc_dDBetaE(pcalc);
    if (calcerr) goto CNK;

//-------TaskTestcalcdata586();
    err = Calc_CorrTestInData(nRun);
//      calcerr = 0;   err = 0;
    calcerr = Testcalcdata(pcalc);
      if (calcerr || err)
         goto CNK;      
      calcerr = TaskCalc_Kn();        // �� ���� 586 ����
      if (calcerr) goto CNK;
      InQueue(2,Task_CalcQ);
      goto CNEnd;   
 CNK:       InQueue(2,TaskCalcEnd);
     
 CNEnd:  calcerr = 0; 
}
void Task_CalcQ(void)
{ struct calcdata         *pcalc;
  int nRun;

    nRun = Time_GetNumrun();
//   prun = &CONFIG_IDRUN[nRun];        // ��������� �� ������� �����
//   aprun = &CONFIG_ADRUN[nRun];       // ��������� �� ������� �����
   pcalc = &CALCDATA[nRun];

    //      TaskCalc_RO();
       calcerr = Calc_RO(pcalc);
      if (calcerr) goto CN;
//printf("Calc_RO=%d K=%f\n",calcerr,pcalc->K);
//        TaskCalc_Miu();
      calcerr = Calc_Miu(pcalc);
      if (calcerr) goto CN;
//      TaskCalc_Kapa();
//printf("Calc_Kapa=%d\n",calcerr);
      calcerr = Calc_Kapa(pcalc);
      if (calcerr) goto CN;
//        TaskCalc_Epsilon();
      calcerr = Calc_Epsilon(pcalc);
//printf("Epsilon=%d\n",calcerr);
      if (calcerr) goto CN;
//        TaskCalc_Eps2();
      calcerr = Calc_Eps2(pcalc);
//printf("Eps2=%d\n",calcerr);
      if (calcerr) goto CN;
//        TaskCalc_C1();
      pcalc->Re = StartRe[nRun];              // �� ��ࢮ� 蠣� - ���⮢�� Re
      calcerr = Calc_C(pcalc);
//printf("Calc_C1=%d Re=%f\n",calcerr,pcalc->Re);
      if (calcerr) goto CN;
//          TaskCalc_Q1();
      calcerr = Calc_Q1(pcalc);
//   printf("Calc_Q1 %d",calcerr);
      if (calcerr) goto CN;
      PrevQ[nRun] = pcalc->Q;
//        TaskCalc_Re1();
      Calc_Re1(pcalc);
//printf("Calc_Re1\n");
      calcerr = ChkReAlarm();                  // �஢�ઠ ���ਨ �� Re
//        TaskCalc_C2();
     calcerr = Calc_C(pcalc);
     if (calcerr) goto CN;
     IndC_[nRun] = pcalc->C_;
//         TaskCalc_K_sh2_1();
     calcerr = Calc_K_sh_1(pcalc);
     if (calcerr) goto CN;
//        TaskCalc_K_sh2_2();
     calcerr = Calc_K_sh_2(pcalc);
     if (calcerr) goto CN;
//        TaskCalc_K_sh2_3();
KSH3:     calcerr = Calc_K_sh_3(pcalc);
     IndKsh[nRun] = pcalc->Ksh;
     if (calcerr) goto Q2;
//         TaskCalc_K_sh2_32();
     calcerr = Calc_K_sh_32(pcalc);
     if (calcerr) goto CN;
Q2:     IndKsh[nRun] = pcalc->Ksh ;
//        TaskCalc_Q2();
//     {
       calcerr = Calc_Q(pcalc);
       if (calcerr == 0)
       {                                 // �㦭� �� �த������ ����
        if (pcalc->Q != 0)
        {
          if (fabs( (PrevQ[nRun]-pcalc->Q)/pcalc->Q ) > 1e-6 )
          {  goto RE2;
          }
          else  {
            IndQ[nRun] = pcalc->Q;
//printf("IndQ= %f IndC_=%f\n",IndQ[i],IndC_[i]);
            goto CN;  //TaskCalcEnd();   //����� ����
          }
        }
        else
           goto CN; //TaskCalcEnd();  ����� �� Q=0
       }
       else
         goto CN; //TaskCalcEnd();  ����� �᫨ caller=0
//     }
RE2:
//    TaskCalc_Re2();    // � ᫥�.蠣� ���樨
//printf("pcalc->Q= %f c=%d\n",pcalc->Q,counttime);
     Calc_Re(pcalc);
//printf(" pcalc->Re=%f pQ=%f Q=%f\n",pcalc->Re,PrevQ[i],pcalc->Q);
     IndRe[nRun] = pcalc->Re;
     PrevQ[nRun] = pcalc->Q;
     StartRe[nRun] = pcalc->Re;
//        TaskCalc_C3();
     calcerr = Calc_C(pcalc);
     PrevQ[nRun] = pcalc->Q;
     IndC_[nRun] = pcalc->C_;
     IndKsh[nRun] = pcalc->Ksh;
     if (calcerr) goto CN;
     goto KSH3;
//        TaskCalc_K_sh3_3();
/*     calcerr = Calc_K_sh_3(pcalc);
     if (calcerr) goto CN;
//        TaskCalc_K_sh3_32();
     calcerr = Calc_K_sh_32(pcalc);
     IndKsh[nRun] = pcalc->Ksh;
     if (calcerr) goto CN;
        goto Q2; //TaskCalc_Q3();
*/
CN:
//sprintf((char*)str90,"END D nRun=%d t_17=%d\n\r",kT,t_17);
//HAL_GPIO_WritePin(GPIOD, U2_RTS_Pin, GPIO_PIN_SET);   //RTS USART1 set  GPIO_PIN_4
//HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),1000);

        InQueue(2,TaskCalcEnd);

}
//---------------------------------------------------------------------------
void TaskCalcEnd() {
   struct calcdata         *pcalc;
   extern struct calcdata  COPY_CALCDATA[];
   int i,nRun;
   struct memdata *pm;
   unsigned long int t3,ts,tls;

   pm = &MEMDATA;
      nRun = Time_GetNumrun();          // ��-50
//      nRunG = nRun;  // ��������� ����� ����� � ���������� ����������

   pcalc = &CALCDATA[nRun];             // ������ �������� ����p� �����
   if ( calcerr == 1) {
      if (pcalc->DP == 0) {             // ���� ������� = 0
         pcalc->Q = 0.0;                // ������ ��������
         IndQ[nRun] = pcalc->Q;
      }
   }
//printf("calcerr=%d Q=%f\n",calcerr,pcalc->Q);
   if ( calcerr == 2) {
      pcalc->Q = 0.0;
      IndQ[nRun] = pcalc->Q;
   }
//-------���������� ������������ ������ ��� �����������
/*  if (FirstC[nRun])
                 //������ ���� ��� ����� run  ���������
   {
    unsigned char page = Mem_CyklGetPage(nRun);
    if (Mem_DayRdEnd(page))
    { tls = BinToSec(pm->STARTTIME);
      t3 =(BinToSec(BINTIME)-tls);
      ts = t3 - 2 + ((SysTakt==2) ? (t3 % 2) : 1);
//printf("Calc t3=%ld ts=%ld  IndQ[%d]=%f Q=%f\n",t3,ts, nRun,IndQ[nRun],pcalc->Q);
     if (ts <= 30)
     {
      float q1=pcalc->Q/3600;
      pcalc->Q = ((q1 + pm->q /pm->count)/2 * (ts+SysTakt))* 3600/SysTakt;
//printf("Q=%f q=%f count=%d ts=%ld\n",pcalc->Q,pm->q,pm->count,ts);
     }
    }
    FirstC[nRun] = 0;
   }
*/
   IndQ[nRun] = pcalc->Q;
   
// =========== ������� ==================
    if (NumZap[nRun] < 5000)
       NumZap[nRun]++;
    else NumZap[nRun] = 0;   
    pcalc->H = NumZap[nRun];
// =========== ����� ������� ==================
// ������ CALCDATA
    P23K256_Write_Data(&hspi3,&AdrCALC[nRun][0], (uint8_t*)&CALCDATA[nRun], SizeCALC);   
    
   i=counttime;                       // ���������� ������� ����������
   CalcReadyFlag[nRun] = 1;
   pcalc->FullHeat = pcalc->Heat * pcalc->Q;

//unsigned char *s1,*s2,i2;
//uint16_t di;
   TaskWriteArcData();      // ������ � ����� � ���������
//   uint16_t poi = pArchiv.di2s[nRun]-30;
//   Mem_SecDayRd(SEC,nRun, poi);
//   uint8_t m1 = MEMDATA.STARTTIME.minutes;
//   uint8_t s = MEMDATA.STARTTIME.seconds;
//   char str[60];
//   sprintf(str,"t_17=%d di2=%d poi=%d ,m1=%d s=%d\n\r",t_17,pArchiv.di2s[nRun],poi,m1,s);
//    uint16_t Lstr = strlen(str);
//   HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,sizeof(str));
//   HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,Lstr);
// di = pArchiv.di1s[nRun];
//    Mem_SecDayRd(SEC,nRun,di );  
// s1= (uint8_t*)&MEMDATA.STARTTIME;
//sprintf(str,"di=%d %d/%d/%d-%d:%d:%d         \n\r",di,*(s1+5),*(s1+4),*(s1+3),*(s1+2),*(s1+1),*(s1));
//HAL_UART_Transmit_IT(&huart2,(uint8_t*)str,sizeof(str));

    kT++;
if (kT < GLCONFIG.NumRun)
   { 
     InQueue(2, Task_Start);
   }
   else kT = 0;   
   glnumrun = kT;
}

//---------------------------------------------------
/*
float CalcHeat(int nRun) {              // ������ �������� ������� ����.
   float fTmp,                          // ������� ������� � ��������
         fTmp2;                         // � ���
   int iTmp;

   if ((CONFIG_IDRUN[nRun].nKcompress == 0)||(CONFIG_IDRUN[nRun].nKcompress == 1)) // Gerg91 ��� NX19  // ����������
   {   fTmp = 85.453 * (0.52190 * CONFIG_IDRUN[nRun].ROnom * KoRo[CONFIG_EDIZM_T.T_SU][0]
       + 0.0424 - 0.65197 * (CONFIG_IDRUN[nRun].NN2 / 100.0)
           - CONFIG_IDRUN[nRun].NCO2 / 100.0);
      fTmp *= KoRo[0][GLCONFIG.EDIZM_T.T_SU];  // ���������� � T_SU
   }
   else fTmp = CONFIG_IDRUN[nRun].Heat;
     fTmp2 = fTmp; //KoefEn[GLCONFIG.EDIZM_T.EdIzmE]; //�� ���� �� ���  � ���*�
   if ((CONFIG_IDRUN[nRun].TypeRun) == 8 || (CONFIG_IDRUN[nRun].TypeRun) == 9)
    iTmp = 0;
   else                                     // ����� �� �������
     iTmp = (CONFIG_IDRUN[nRun].nKcompress==0) ? 2 : 1;      // 0  -  NX-19
                                        // ����� �� �������
   if (((iTmp == 1) && (fTmp < 20.0 || fTmp> 48.0))     //GERG91,AGA8 � SGERG88
       || ((iTmp == 2) && (fTmp < 30.0 || fTmp> 40.0))) {      // NX-19
      if (HeatErr[nRun] == 0) {         // ���� ������ ������,
       Mem_AlarmWrCodRun(nRun,0x8B,Mem_GetDayQ(nRun)); //�� ������� ��������
         HeatErr[nRun] = 1;
//printf("Heat=%f\n",fTmp);
      }
      if ((iTmp == 1) && (fTmp < 20.0))
        fTmp2=20.0;
      else  if ((iTmp == 1) && (fTmp > 48.0))
        fTmp2=48.0;
      else  if ((iTmp == 2) && (fTmp < 30.0))
        fTmp2=30.0;
      else  if ((iTmp == 2) && (fTmp > 40.0))
        fTmp2=40.0;
   }
   else                                 // �������� � �����
   if ((iTmp == 1) && (fTmp >= 20.0) && (fTmp<= 48.0)
       || (iTmp == 2) && (fTmp >= 30.0) && (fTmp<= 40.0)) {
      if (HeatErr[nRun] == 1) {         // ���� ������ ����� ����� �������,
         Mem_AlarmWrCodRun(nRun,0x0B,Mem_GetDayQ(nRun));
         HeatErr[nRun] = 0;
      }
   }

   return fTmp2;                        // ������� � (���) ������������ (���*�)
}
*/
//-------------------------------------------------
void ReHeatErrIni() {                   // ��������� ��������� - ������ ���
   int i;

   for (i=0; i<3; i++) {
      ReErr[i] = 0;
      HeatErr[i] = 0;
   }
}
//---------------------------------------------------------------------------
// ������� ����� ��� ���������� ������� �� ���� 8.586
//---------------------------------------------------------------------------
int TaskCalc_Kn() {
   struct calcdata   *pcalc;
   int nRun;
   float  ZcKk[3][3];
//printf("TaskCalc_Kn ");
    nRun= Time_GetNumrun();
            // ������ �������� ����p� �����
   pcalc = &CALCDATA[nRun];
   if (FirstCalc < GLCONFIG.NumRun)       // 1-� ᥪ㭤�
    FirstCalc++;

   if  (CONFIG_IDRUN[nRun].Dens)         // ���� ���⭮���
        CalcZc[nRun] = 1;
//printf("Gerg[%d]=%d %d\n",nRun,CONFIG_IDRUN[nRun].nGerg91_NX,CONFIG_IDRUN[nRun].nGerg88_AGA8);

   calcerr = Calc_KnPT(pcalc);
//printf("KnPT=%d Pa=%f nK=%d\n",calcerr,pcalc->Pa,CONFIG_IDRUN[nRun].nKcompress);
   if ((calcerr == 0)&&(pcalc->Pa>0.85))
   {
        if (CONFIG_IDRUN[nRun].nKcompress  == 2) //��� ���  2-GERG88 3-AGA-8
        {// printf("TaskCalc_SGERG88\n");
             if ((Zc1[nRun] == 1) || (CalcBR[nRun] == 1))
               TaskCalc_SGERG88();
             calcerr = TaskCalc_SGERG88();
        }
        else if (CONFIG_IDRUN[nRun].nKcompress  == 1) //��� ���  1-GERG, 0-NX19
             calcerr = TaskCalc_K_8_586G();
        else  if (CONFIG_IDRUN[nRun].nKcompress  == 0)
             calcerr = TaskCalc_K_8_586N();
        else if (CONFIG_IDRUN[nRun].nKcompress  == 3)
        {
             calcerr = TaskCalc_AGA8();
        } // ����� if (CONFIG_IDRUN[nRun].nKcompress  == 3)
        else  // �᫨ nKcompress > 3 ��� < 0
           goto KN; // TaskCalcEnd();
        if (calcerr )
          goto KN;
        else
           return 0;
     }  //    if ((calcerr == 0)&&(pcalc->Pa>0.6))
KN:  return 1;
//TaskCalcEnd();
// printf("TaskCalc_Kn)\n");
}

//---------------------------------------------------------------------------
int TaskCalc_K_8_586N() {
   struct calcdata   *pcalc;
   unsigned int      *pbuferr;
   int err,nRun,sch;

//   ComPrint("06");
   nRun = Time_GetNumrun();
   CalcZc[nRun] = 0;
   sch = (((CONFIG_IDRUN[nRun].TypeRun) == 4) || ((CONFIG_IDRUN[nRun].TypeRun) == 5)
         || ((CONFIG_IDRUN[nRun].TypeRun) == 10));

   pcalc = &CALCDATA[nRun];             // ������ �������� ����p� �����
//printf("nRun=%d KNXst c=%d\n",nRun,counttime);
   CopyDataGerg91(pcalc);
   calcerr = CalcCoeffK_NX19(pcalc);
//printf("nRun=%d KNXend=%d err=%d\n",nRun,counttime,calcerr );
   pbuferr = Calc_GetPointBuferr(nRun); // �������� ��p�� ����p�
   err = (calcerr) ? 0x93 : 0x13;       // ������ p������ ����. �����������

   if (err != pbuferr[26]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[26] = err; //�p������� ����� ��������
   }

   if (calcerr == 0)
    return 0;
   else
    return 1;
// printf("TaskCalc_K_8_586N)\n");
}

//---------------------------------------------------------------------------
//  ������ ����� ����. ����� �������� ��� ��������
int TaskCalc_K_8_586G() {
   struct calcdata   *pcalc;
   unsigned int      *pbuferr;
   int err,nRun,sch;

   GergErr = 0;
   nRun = Time_GetNumrun();
   CalcZc[nRun] = 0;
   pcalc = &CALCDATA[nRun];             // ������ �������� ����p� �����
   sch = (((CONFIG_IDRUN[nRun].TypeRun) == 4) || ((CONFIG_IDRUN[nRun].TypeRun) == 5)
         || ((CONFIG_IDRUN[nRun].TypeRun) == 10));
   CopyDataGerg91(pcalc);               // ����p������ ��� GERG-91
                                        // p����� � �� GERG-91
   calcerr = CalcCoeffK_GERG_91(pcalc);
   GergErr = calcerr;
     if  (calcerr == 0)
      K_Gerg91_1(pcalc);
     else
      return 1;
   K_Gerg91_2();    //    ;
   K_Gerg91_22(pcalc);
   calcerr = K_Gerg91_3(pcalc,nRun);
   GergErr += calcerr;
   pbuferr = Calc_GetPointBuferr(nRun); // �������� ��p�� ����p�
   err = (GergErr) ? 0x93 : 0x13;       // ������ p������ ����. �����������

   if (err != pbuferr[26]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[26] = err; //�p������� ����� ��������
   }
//printf("K_Gerg91_3=%d sch=%d nRun=%d\n",calcerr,sch,nRun);

     if (GergErr == 0)
     { return 0;
     }
    else
    return 1;

}

// ������ ����. ����������� � ��������� �� 4 ����� ��� ���������
//---------------------------------------------------------------------------
int ChkReAlarm() {
   struct calcdata *pcalc;
   unsigned int *pbuferr;
   int i,err;
   float Re_min,fTmp;

   i = Time_GetNumrun();

   switch (i) {
      case 0:
         pbuferr = calc_buferrrun0;
         break;
      case 1:
         pbuferr = calc_buferrrun1;
         break;
      case 2:
         pbuferr = calc_buferrrun2;
         break;
   }

   pcalc = &CALCDATA[i];

   if (pcalc->typeOtb == 0) {           // �������
      if (pcalc->Beta <= 0.56)
         Re_min = 5000.0;
      else
         Re_min = 16000*pcalc->Beta2;
   }
   else {                               // ���������
      fTmp = 1.7e5*pcalc->Beta2*pcalc->Dt/1000.0;
      if ( fTmp > 5000.0)
         Re_min = fTmp;
      else
         Re_min = 5000.0;
   }

                                        // Re < Re_min
   err = ( pcalc->Re < Re_min ) ? 0x8C : 0x0C;
   if (err != pbuferr[28]) {            // �������� ��������� ????????
    Mem_AlarmWrCodRun(i,err,Mem_GetDayQ(i));
    pbuferr[28] = err;                // �p������� ����� ��������
   }
if (pcalc->Re < (Re_min - 1000.0))
   return 1;                         // Q = Qpred

   return 0;
}
//---------------------------------------------------------------------------
unsigned int    Calc_dDBetaE(struct calcdata * uk) {
   float Alpha_t_d;                     // �-� ���������� ��� ����������� t
   float Alpha_t_D;                     // �-� ���������� ��� ����������� t
   float t1,t2,t20;                     // ������������� ���������� ��� �����������
   float fTmp;

   t1 = uk->t;
   t2 = t1 * t1;
   t20 = uk->t - 20.0;                  // ��������� �-��� ���������� �� t

   Alpha_t_d = uk->kAd + uk->kBd*t1 + uk->kCd*t2;
   Alpha_t_D = uk->kAt + uk->kBt*t1 + uk->kCt*t2;

   uk->Ktd = 1.0+Alpha_t_d*t20;           // �������� �� t � �������� ���������
   uk->Ktt = 1.0+Alpha_t_D*t20;           // �������� �� t � �������� �����

   uk->Dd = uk->Dd20*uk->Ktd;           // �������� ��� ������� �����������
   uk->Dt = uk->Dt20*uk->Ktt;

   fTmp = uk->Dt;

   if (fTmp == 0)                       //???
      return 1;

   uk->Beta = uk->Dd/uk->Dt;            // ������������� ���������
   uk->Beta2 = uk->Beta*uk->Beta;
   uk->mod = uk->Beta2;
   uk->Beta4 = uk->Beta2*uk->Beta2;
   uk->Beta8 = uk->Beta4*uk->Beta4;

   fTmp = 1.0-uk->Beta4;

   if (fTmp <= 0)                       //???
      return 1;

   uk->E = 1.0/sqrt(fTmp);              // �-� �������� �����
//printf("E=%f\n",uk->E);
   return 0;
}

//---------------------------------------------------------------------------
unsigned int    Calc_KnPT(struct calcdata * uk) {
   float Test;
   float r_k;
   float fTmp,fTmp2,tau, a, pp;
   char d,m,g;
   int d1,m1;
   long tPov, tTek;
   struct bintime TimePoverka;

  if ((uk->Rn > 0.0) && (uk->taupp>0))
  {
 //  tTek  = BinToSec(BINTIME);
   a = 0.195;
   Test = uk->Dd*0.0004;
/*   if  (uk->taupp>5.0)
   { pp= uk->taupp/100.0;
     m1=pp;
     g = (pp-(float)m1)*100.0+0.5;
     pp=(float)m1/100.0;
     d1=pp;
     m= (pp-(float)d1)*100+0.5;
     d=d1;
//     printf("d=%d m=%d g=%d",d,m,g);
     TimePoverka.date=d;   TimePoverka.hours=0;
     TimePoverka.month=m;  TimePoverka.minutes=0;
     TimePoverka.year=g;   TimePoverka.seconds=0;
     tPov  = BinToSec(TimePoverka);
     tau = (float)(tTek-tPov)/31536000.0;
//     printf("tPov=%ld tTek=%ld tau=%f \n",tPov,tTek,tau);
     fTmp2 = tau / 3.0;
   }
   else
*/
fTmp2 = uk->taupp / 3.0;

   if (fTmp2 == 0)
   { uk->Kp = 1.0;
      return 1;
   }
   if (uk->taupp>5.0)
   {  r_k = a - (a - uk->Rn) *  exp(-fTmp2);
   }
   else
   r_k = a - (a - uk->Rn) * (1.0 - exp(-fTmp2)) / fTmp2;

   if (r_k >= Test) {                   //  ���������� �� ������������� ��������

      fTmp2 = uk->Dd;

      if (fTmp2 == 0)                   //???
      { uk->Kp = 1.0;
        return 1;
      }

      fTmp2 = r_k/uk->Dd+0.0007773;

      if (fTmp2 < 0)                    //???
       {  uk->Kp = 1.0;
          return 1;
       }
      fTmp = pow(fTmp2,0.6);
      uk->Kp = 0.9826 + fTmp;
   }
   else                                 // �� �������
      uk->Kp = 1.0;
  }
  else  uk->Kp = 1.0;
// printf("Test=%f Rn=%f r_�=%f Kp=%f\n", Test, uk->Rn, r_k, uk->Kp);

   uk->T = 273.15 + uk->t;

   return 0;
}
//---------------------------------------------------------------------------
unsigned int    Calc_PT(struct calcdata *uk) {

   uk->T = 273.15 + uk->t;

   return 0;
}

//---------------------------------------------------------------------------
unsigned int    Calc_RO(struct calcdata *uk ) {
   float TdivPst = T_st/1.033227;   //293.15/1.03323;
   float fTmp;
   fTmp = uk->T;

   if (fTmp <= 0)                       //???
      return 1;

   uk->p_T = uk->Pa/uk->T;

   fTmp = uk->K;
   if (fTmp == 0)                       //???
      return 1;

   uk->ROmk1 = uk->ROnom * TdivPst * uk->p_T / uk->K;   //��������� � ��
   return 0;
}

//---------------------------------------------------------------------------
unsigned int    Calc_Miu(struct calcdata *uk) {
   float P_pc,T_pc;
   float P_p,T_p;
   float C_m,Miu_T;
   float fTmp;
                                        // ����������������� ��������
   if ((INTPAR.FullNormVolume==0)||(CONFIG_IDRUN[uk->run].GOST_ISO==0))  // GOST 30319
   {
                                        // (48) ���� 30319.1-96
    P_pc = 2.9585*(1.608 - 0.05994*uk->ROnom + uk->Xy - 0.392*uk->Xa);
    T_pc = 88.25*(0.9915 + 1.759*uk->ROnom - uk->Xy - 1.681*uk->Xa);

    fTmp = P_pc;

    if (fTmp == 0)                       //???
      return 1;

    P_p = uk->Pa / P_pc;                      // ����������� ��������
    P_p = P_p * 0.098067;
    fTmp = T_pc;
    if (fTmp == 0)                       //???
      return 1;

    T_p = uk->T / T_pc;
    fTmp = T_p -1.0;
    if (fTmp == 0)                       //???
      return 1;
    C_m = 1.0+(P_p*P_p/(30.0*fTmp));     // ����������� ���������

    if (uk->T < 0)                       //???
      return 1;
    if (uk->ROnom < 0)                   //???
      return 1;
//printf("ROnom=%f\n",uk->ROnom);
    fTmp = sqrt(uk->ROnom) + 2.08 - 1.5*(uk->Xy + uk->Xa);
    if (fTmp == 0)                       //???
      return 1;
    Miu_T = (sqrt(uk->T) + 1.37 - 9.09*pow(uk->ROnom,0.125)) / fTmp;
    uk->Mu = 3.24*Miu_T*C_m;
   }
   else   //   ISO ���������
     uk->Mu = 10.99*sqrt(uk->T/293.15)*(1+164.0/293.15)/(1+164.0/uk->T)*
         (0.99625+0.003753*uk->Pa);

    uk->Mu = uk->Mu * 1.0e-6;
   return 0;
}
//---------------------------------------------------------------------------
unsigned int    Calc_Kapa(struct calcdata *uk) {
   float buf,ppp;

   buf = uk->p_T*9.80665e-2;

   if (buf < 0)                         //???
      return 1;
   if ((INTPAR.FullNormVolume == 0)||(CONFIG_IDRUN[uk->run].GOST_ISO==0))  // GOST 30319
   { uk->capa = (1.556 + 0.115144 * uk->Xa) - uk->T *
       (3.9e-4 - 2.652e-4 * uk->Xa) - 0.208 * uk->ROnom +
       pow(buf,1.43)*(384.0*(1.0 - uk->Xa)*pow(buf,0.8) +
       26.4 * uk->Xa);
    }
    else  //ISO ���������
      uk->capa = 1.29+7.18e-7*(2575+(346.23-uk->T)*(346.23-uk->T))*uk->Pa*KoefP[3]; // � ����
   return 0;
}

//---------------------------------------------------------------------------
unsigned int Calc_Epsilon(struct calcdata *uk) {
   float buf,fTmp;

   buf = uk->DP * 1e-4;                 // �� �2 � ��2

   if (uk->capa == 0)                   //???
      return 1;

   if (uk->Pa == 0)                     //???
      return 1;

   fTmp = 1.0 - buf/uk->Pa;

   if (fTmp < 0)                        //???
      return 1;
 //  uk->capa = 1.36;
     uk->Eps = 1.0 - (0.351 + 0.256*uk->Beta4 + 0.93*uk->Beta8)*(1.0 - pow(fTmp,1.0/uk->capa));
//printf("Capa=%f Eps=%f\n",uk->capa,uk->Eps);
                                        // ���������� �� ������� ������������� �����
   if (uk->Beta < 0)                    //???
      return 1;

   C_11 = 0.5961+0.0261*uk->Beta2-0.216*uk->Beta8;
   C_12 = 0.000521*pow(1e6*uk->Beta,0.7);
   C_130 = pow(uk->Beta,3.5);

   return 0;
}
//---------------------------------------------------------------------------
unsigned int Calc_Eps2(struct calcdata *uk) {
   float buf,fTmp;

   C_13 = C_130*pow(1e6,0.3);
   fTmp = 1.0 - uk->Beta4;
   if (fTmp == 0)                       //???
      return 1;

   C_14 = uk->Beta4/(1.0-uk->Beta4);
   C_15 = pow(uk->Beta,1.3);
   C_16 = pow(19000.0*uk->Beta,0.8);
   return 0;
}

//---------------------------------------------------------------------------
unsigned int    Calc_C(struct calcdata *uk) {
   float A,M1,M2,L1,L2;
   float fTmp;
//printf("Re=%f\n", uk->Re);
   C_17 = 1.0/uk->Re;

   if (C_17 < 0)                        //???
      return 1;

   A = C_16*pow(C_17,0.8);

   if (uk->typeOtb == 0)                // ������� �����
      L1 = L2 = 0;
   else {
      if (uk->typeOtb == 1)//���������
      {  if (uk->Dt == 0)                  //???
         return 1;

       L1 = L2 = 25.4/uk->Dt;}
      else                        // 2-Radius
      {L1=1; L2=0.47;}       // ����������
   }
//printf("Beta=%f uk->typeOtb=%d ",uk->Beta,uk->typeOtb);
   fTmp = 1.0 - uk->Beta;
   if (fTmp == 0)                       //???
      return 1;

   M1 = 2 * L2 / fTmp;
   if (M1 < 0)                          //???
      return 1;
   if ((uk->Dt >= 71.12) || ((INTPAR.FullNormVolume) && (CONFIG_IDRUN[uk->run].GOST_ISO))) //ISO ���������
      M2 = 0;
   else
      M2 = 0.011*(0.75-uk->Beta)*(2.8-uk->Dt/25.4);
   uk->C_ = C_11 + C_12*pow(C_17,0.7) + (0.0188+0.0063*A)*C_13*pow(C_17,0.3)
            + (0.043 + 0.08*exp(-10*L1) - 0.123*exp(-7*L1))*(1.0 - 0.11*A)*C_14
            - 0.031*(M1 - 0.8*pow(M1,1.1))*C_15 + M2;
// printf("C_=%f\n",uk->C_);
   return 0;
}

//-----------------------------------------------------
float KtB_Re10_5[4][3] = {
   {  8.87,         6.7307,     -10.244    },
   { -3.7114,      -5.5844,       5.7094   },
   {  0.41841,      0.732485,    -0.76477  },
   {  0,            0,            0        }
};
float KtB_Re310_6[4][3] = {
   {  27.23,      -25.928,        1.7622   },
   { -11.458,      12.426,       -3.8765   },
   {   1.6117,     -2.09397,      1.05567  },
   {  -0.07567,     0.106143,    -0.076764 }
};
float KtB_Re10_8[4][3] = {
   {  16.5416,     322.594,     -92.029    },
   {  -6.60709,   -132.2,        37.935    },
   {   0.88147,     17.795,      -5.1885   },
   {  -0.039226,    -0.799765,    0.23583  }
};

float Ra_max,Ra_min,Max,Min;
float A0_ksh,A1_ksh,A2_ksh;
float Lambda,Lambda_zv;
float pLambda,pLambda_zv;
float Ash,Ash_zv;
float k_D,k_D_zv;
float k_R,k_R_zv;
float ra,dd,lgRe[4];

//---------------------------------------------------------------------------
unsigned int Calc_K_sh_1(struct calcdata *uk) {
   int i,iTmp;
   long lTmp;
   float fTmp,fTmp1,fTmp2,fTmp3;
//printf("K_sh1 Re=%f\n",uk->Re);
   lgRe[0] = 1;
   lgRe[1] = log10f(uk->Re);
   lgRe[2] = lgRe[1]*lgRe[1];
   lgRe[3] = lgRe[1]*lgRe[2];

   A0_ksh = 0.0;                            // ���������� ������������
   A1_ksh = 0.0;
   A2_ksh = 0.0;


   for (i=0; i<4; i++) {                // ������ �-��� � �� ������� 2
      if (uk->Re>1e4 && uk->Re<=1e5) {
         A0_ksh = A0_ksh + KtB_Re10_5[i][0]*lgRe[i];
         A1_ksh = A1_ksh + KtB_Re10_5[i][1]*lgRe[i];
         A2_ksh = A2_ksh + KtB_Re10_5[i][2]*lgRe[i];
      }
      else
      if (uk->Re>1e5 && uk->Re<=3e6) {
         A0_ksh = A0_ksh + KtB_Re310_6[i][0]*lgRe[i];
         A1_ksh = A1_ksh + KtB_Re310_6[i][1]*lgRe[i];
         A2_ksh = A2_ksh + KtB_Re310_6[i][2]*lgRe[i];
      }
      if (uk->Re>3e6 && uk->Re<=1e8) {
         A0_ksh = A0_ksh + KtB_Re10_8[i][0]*lgRe[i];
         A1_ksh = A1_ksh + KtB_Re10_8[i][1]*lgRe[i];
         A2_ksh = A2_ksh + KtB_Re10_8[i][2]*lgRe[i];
      }
   }
    return 0;
}
//----------------------------------------------------------------
unsigned int Calc_K_sh_3(struct calcdata *uk) {
   int i,iTmp;
   long lTmp;
   float fTmp,fTmp1,fTmp2,fTmp3, p1,p2,p3,p4,p5,p6,s1;
   
    p1= 3.14159265;
    s1=1.0;
    uk->Ksh = s1;                           // Ksh ������� ������
   if (uk->R <= Ra_max && uk->R >= Ra_min)
     { return 1;}
   else {                               // ������ �����

      Ash =p1 * uk->R;             // ������� 3;

      if (uk->Dt == 0.0)                  //???
         return 1;
      p2= 0.26954;
      k_D =p2 * Ash/uk->Dt;
      p3= 5.035;
      k_R = p3 * C_17;
      k_R_zv = k_R;

      if (uk->R > Ra_max)
         Ash_zv = p1*Ra_max;
      else
      if (uk->R < Ra_min)
         Ash_zv = p1*Ra_min;
      else
         Ash_zv = p1*uk->R;

      k_D_zv = p2*Ash_zv/uk->Dt;
      p3=3.3333;
                                        // 5.12
      fTmp  = k_D+p3*k_R;
      if (fTmp <= 0.0)
         return 1;

      fTmp1 = k_D-k_R*log10f(fTmp);
      if (fTmp1 <= 0.0)
         return 1;
      p4=2.0;
      p5=37.36;
      p6=1.74;

      fTmp2 = p4*Ash/uk->Dt - p5*log10f(fTmp1)*C_17;
      if (fTmp2 <= 0.0)
         return 1;
      pLambda = p6 - p4 * log10f(fTmp2);
   }
      return 0;
 }
//------------------------------------------------
unsigned int Calc_K_sh_32(struct calcdata *uk)
{
   float fTmp,fTmp1,fTmp2,p1, p3,p4,p5,p6,s1;

      s1=1.0;
      p3= 5.035;
      fTmp  = k_D_zv+p3*k_R_zv;

      if (fTmp <= 0.0)
         return 1;

      fTmp1 = k_D_zv-k_R_zv*log10f(fTmp);
//printf("fTmp1= %f \n", fTmp1);
      if (fTmp1 <= 0.0)
         return 1;
      p4=2.0;
      p5=37.36;
      p6=1.74;
      fTmp2 = p4*Ash_zv/uk->Dt - p5*log10f(fTmp1)*C_17;
      if (fTmp2 <= 0.0)
         return 1;

      pLambda_zv = p6 - p4 * log10f(fTmp2);

      if (pLambda !=0.0)
         Lambda = s1/(pLambda*pLambda);
      else
         Lambda = 0.0;

      if (pLambda_zv !=0.0)
         Lambda_zv = s1/(pLambda_zv*pLambda_zv);
      else
         Lambda_zv = 0.0;
// printf("C_130=%f Lam=%f Lamz=%f\n",C_130,Lambda,Lambda_zv);
      p1 = 5.22;                                  // 5.11
      uk->Ksh = s1 + p1 * C_130*(Lambda-Lambda_zv);
//   printf("%f ",uk->Ksh);
   return 0;
}

//----------------------------------------------------------------

unsigned int Calc_K_sh_2(struct calcdata *uk)
{
   int i,iTmp;
   long lTmp;
   float fTmp,fTmp1,fTmp2,fTmp3;

   if (uk->Re <= 1e4)                   // ����������� Ra_max
      fTmp = 0.718866*pow(uk->Beta,-3.887) + 0.364;
   else
   if (uk->Beta < 0.65)
      fTmp = A0_ksh*pow(uk->Beta,A1_ksh) + A2_ksh;
   else
      fTmp = A0_ksh*pow(0.65,A1_ksh) + A2_ksh;

   if ( fTmp >= 15.0)                   // ����������� ������
      fTmp = 15.0;
                                        // ��������� �� 27.03.07
   if (fTmp >= 10.0) {                  // �� 2-� �������� ����
      lTmp = floor(fTmp+0.5);           // ����� �� 15 �� 10 (����������)
      fTmp = lTmp;
   }
   else
   if (fTmp < 10.0 && fTmp >= 1.0) {    // ����� ����������� � ������ 1
      lTmp = floor((fTmp+0.05)*10.0);
      fTmp = (float)lTmp/10.0;
   }
   else
   if (fTmp < 1 && fTmp >= 0.1) {       // ������� ����
      lTmp = floor((fTmp+0.005)*100.0);
      fTmp = (float)lTmp/100.0;
   }
   else
   if (fTmp < 0.1 && fTmp >= 0.01) {    // ����� ����
      lTmp = floor((fTmp+0.0005)*1000.0);
      fTmp = (float)lTmp/1000.0;
   }
   else {                               // �������� ����
      lTmp = floor((fTmp+0.00005)*10000.0);
      fTmp = (float)lTmp/10000.0;
   }

   Ra_max = fTmp*uk->Dt*1e-4;           // ���������� Ra_max



   if (uk->Beta < 0.65)                 // ����������� Ra_min
      fTmp = 7.1592 - 12.387*uk->Beta-(2.0118-3.469*uk->Beta)*lgRe[1]
               + (0.1382-0.23762*uk->Beta)*lgRe[2];
   else
      fTmp = -0.892353+0.24308*lgRe[1]-0.0162562*lgRe[2];

   lTmp = floor((fTmp+0.0005)*1000.0);  // �� 3-� ������
   fTmp = (float)lTmp/1000.0;           // ����� �������

   if (fTmp <= 0 || uk->Re < 3e6)       // ����������� �����
      Ra_min = 0;
   else
      Ra_min = fTmp*uk->Dt*1e-4;        // ���������� Ra_min
    return 0;
}
//----------------------------------------------------------------
unsigned int    Calc_Q(struct calcdata *uk) {
   double qq,q1;
   q1 = 3600.0;
   qq = Q_1 * uk->C_ * uk->Ksh;
   uk->Q_ = qq;                         // ��������� ������

   uk->Q  = q1*qq;                    // ������� ������
//printf("Q_1=%f C_=%f Ksh=%f Q=%f\n",Q_1,uk->C_,uk->Ksh, uk->Q);
   return 0;
}

//---------------------------------------------------------------------------
unsigned int    Calc_Q1(struct calcdata *uk) {
   double dd,p1,p2,p3;
   float P_st,Dp,pp;
   double p4;
   P_st = 101325;                       // 1 ���������
   p1 = 9.80665;
   p2 = 9.80665e4;

   Dp = uk->DP * p1;               // � �������
//printf("K=%f T=%f ROnom=%f DP=%f\n",uk->K,uk->T,uk->ROnom,uk->DP);
   pp = uk->Pa * p2;             //
   dd = uk->Dd*1e-3;
   if (uk->K == 0)
      return 1;

   if (uk->T == 0)
      return 1;

   if (uk->ROnom == 0)
      return 1;

   if (Dp <= 0 || pp == 0)
      return 1;
//printf("RO=%f T_st=%f K=%f\n",uk->ROnom,T_st, uk->K);
   uk->sqrtQ = sqrt(2 * Dp/uk->ROnom * (pp/P_st) * (T_st/uk->T )/ uk->K);
//printf("sqrtQ=%f \n",uk->sqrtQ);
       p4 = 0.78539816;   // pi/4=0.7853982
       p1 = (float)(dd * dd);
       p2 = (float)(uk->E * uk->Kp);
       p3 = (double)( p4 * uk->Eps * uk->sqrtQ);
       Q_1 =(float)(p1 * p2 * p3);
//printf("p1= %f p2= %f p3= %f O1=%f\n",p1,p2,p3,Q_1);
     Calc_Q(uk);
   return 0;
}

//---------------------------------------------------------------------------
unsigned int    Calc_Re(struct calcdata *uk) {
   int i;

   i = Time_GetNumrun();
     uk->Re = (float)(Re_1 * uk->Q_);
   if (uk->Re <= 0) {
      if (StartRe[i] <= 0) {
         uk->Re =(float)1e6;
      }
      else
        uk->Re = StartRe[i];
   }
   IndRe[i] = uk->Re;
//printf("Re=%f\n",uk->Re);
   return 0;
}

//---------------------------------------------------------------------------
unsigned int    Calc_Re1(struct calcdata * uk) {
   float dd;
   double t1,t2;

   t2 = 1.273239544;
   dd = uk->Dt * 1e-3;

   t1=uk->ROnom /(dd * uk->Mu);

   Re_1=(float)(t2 * t1);   //  (t2=4.0 / 3.14159265=1.273239544)
   Calc_Re(uk);

   return 0;
}
//---------------------------------------------------------------------------
/*
void TaskWriteArcData() {               // ������ � �����
   int n,nRun_s;
   struct calcdata *pcalc;
   struct AdRun    *aprun;
   char far *pENP;
   double *v;

     aprun = &CONFIG_ADRUN[nRun];       // ������� ����p �����
     nRun_s =nRun;
     nWatch = 0;
//printf("nRun=%d c=%d\n",nRun,counttime);
   pcalc = &CALCDATA[nRun_s];
   movmem(pcalc,&COPY_CALCDATA[nRun_s],sizeof(struct calcdata));
//   if ((CONFIG_NumRun == 3) && (nRun_s == 2) || (CONFIG_NumRun == 1))
//   {//������  � ��������� ������ ������������
//    DensomTrans();
//   }
   n=1;
//   Nsp[nRun_s]++;
     Setperiodtdata();           // ��� �����p������ ��p���������� ������ �� �����
     Sethourtdata();             // ��� �����p������ ������� ������ �� �����
     Setdaytdata();              // ��� �����p������ �������� ������ �� �����
     Setmonthtdata();            // ������ ��������� ������
//printf("1-run=%d C_Qw_s=%f,Qw_s=%f P=%f t=%f\n",nRun_s,pcalc->Qw_s,aprun->Qw_s,aprun->P,aprun->t);
   Mem_AddCfgWr(nRun_s);
}

//-----------------------------------------------------------
void WriteArcData()
{//  printf("run=%d IndQ=%f kQ=%f\n",nRun,IndQ[nRun],CALCDATA[nRun].kQind);
   Setperiodtdata();
   Sethourtdata();
   Setdaytdata();
   Setmonthtdata();
}
*/
//-----------------------------------------------------------

void TaskCalcVlago() {                   // p����� ���������
   struct calcdata   *pcalc;
   struct IdRun    *prun;               // ��p����p� �����
   int nRun, k, k1, k2;
   float A, B, W, Trsr, Trs40, P;
   float kInerA1[5] = {1.94967e-6, 0.0002240584, 0.01182407, 0.3493798, 4.7024755};
   float kInerA2[5] = {-2.836136e-6, 0.0003211258, 0.01080033, 0.3368264, 4.67221069};
   float kInerB1[5] = {3.4886007e-8, 2.978039e-6, 0.00011218278, 0.0028468919, 0.0437214};
   float kInerB2[5] = {-2.0566815e-8, 1.160753e-6, 5.0787148e-5, 0.00218634, 0.04183289};
   float Wls[] = {0.03, 0.031, 0.032, 0.033, 0.034, 0.035, 0.036, 0.037, 0.038, 0.039, 0.040,
    0.041, 0.042, 0.043, 0.044, 0.045, 0.046, 0.047, 0.048, 0.049, 0.05, 0.052, 0.054, 0.056,
    0.058, 0.06, 0.062, 0.064, 0.066, 0.068, 0.07, 0.072, 0.074, 0.076, 0.078, 0.08, 0.082,
    0.084, 0.086, 0.088, 0.09, 0.092, 0.094, 0.096, 0.098, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15,
    0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3,
    0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.42, 0.44, 0.46, 0.48, 0.5,
    0.52, 0.54, 0.56, 0.58, 0.6, 0.62, 0.64, 0.66, 0.68, 0.7, 0.72, 0.74, 0.76, 0.78, 0.8,
    0.82, 0.84, 0.86, 0.88, 0.9, 0.92, 0.94, 0.96, 0.98, 1.0};
   float ArTrs40[] = {-23.1, -22.7, -22.2, -21.8, -21.5, -21.1, -20.8, -20.4, -20.1, -19.8,
   -19.4, -19.1, -18.8, -18.5, -18.2, -17.9, -17.6, -17.4, -17.1, -16.8, -16.5, -16.0, -15.5,
   -15.0, -14.6, -14.1, -13.6, -13.2, -12.8, -12.4, -12.0, -11.5, -11.1, -10.7, -10.3, -9.9,
   -9.6, -9.3, -9.0, -8.7, -8.4, -8.1, -7.9, -7.6, -7.3, -7.0, -5.7, -4.5, -3.3, -2.2, -1.2, -0.2,
    0.6, 1.5, 2.3, 3.1, 3.8, 4.5, 5.2, 5.9, 6.5, 7.1, 7.7, 8.3, 8.8, 9.3, 9.9, 10.4, 10.9,
    11.3, 11.8, 12.2, 12.6, 13.0, 13.4, 13.8, 14.6, 15.4, 16.2, 17.0, 17.7, 18.3, 19.0, 19.6,
    20.2, 20.8, 21.3, 21.9, 22.4, 22.9, 23.4, 23.9, 24.4, 24.9, 25.4, 25.9, 26.3, 26.7, 27.1,
    27.6, 28.0, 28.4, 28.7, 29.1, 29.5, 29.8};

   nRun = Time_GetNumrun();
                                        // ������ �������� ����p� �����
   pcalc = &CALCDATA[nRun];
   Trsr = pcalc->t;
   if  (Trsr < -30.0) {A = 0.391; B =  0.0071;}
   else if (Trsr > 20.0) {A = 17.87; B = 0.112;}
   else if (Trsr < -4.1)
   {
    A = ((((kInerA1[0]*Trsr + kInerA1[1])*Trsr + kInerA1[2])*Trsr + kInerA1[3])*Trsr + kInerA1[4]);
    B = ((((kInerB1[0]*Trsr + kInerB1[1])*Trsr + kInerB1[2])*Trsr + kInerB1[3])*Trsr + kInerB1[4]);
   }
   else
   {
    A = ((((kInerA2[0]*Trsr + kInerA2[1])*Trsr + kInerA2[2])*Trsr + kInerA2[3])*Trsr + kInerA2[4]);
    B = ((((kInerB2[0]*Trsr + kInerB2[1])*Trsr + kInerB2[2])*Trsr + kInerB2[3])*Trsr + kInerB2[4]);
   }
//printf("A= %f B= %f Pin= %f Pa= %f Trsr= %f ", A,B,pcalc->Pin,pcalc->Pa, Trsr);
//   W = A*101.325/(pcalc->Pin*98.0655)+B;
      P=pcalc->Pa;
      W = A*101.325/(P*98.0655)+B;
   if (W < 0.03) Trs40 = -24.0;
   else if (W > 1.0) Trs40 = 30.0;
       else
      {
       for (k=0; k<106; k++)
       {
        if (W == 1.0) {k1=104; k2 = 105;break;}
         else
         if (W < Wls[k])  {k1=k-1; k2=k; break;}
       }
       Trs40 = ArTrs40[k1]+(ArTrs40[k2]-
             ArTrs40[k1])*(W-Wls[k1])/(Wls[k2]-Wls[k1]);
      }
//printf("W= %f Trs40= %f\n",W,Trs40);
     pcalc->DP = Trs40;
     IndDP[nRun] = Trs40;
     pcalc->Q = IndQ[nRun] = 0.0;
//     nRunG = nRun;  // ��������� ����� ����� � ���������� ����������
       TaskWriteArcData();      // ������ � ����� � ���������
}

//-----------------------------------------------------------
void TaskSetMicroData() {               // ������ �� MicroMotion
   struct AdRun    *aprun;              // ��p����p� �����
   struct calcdata *pcalc;
   unsigned char   nRun;
   struct IdRun    *prun;               // ��p����p� �����
   float           *index;
   unsigned int    *perr;
   unsigned int    *pbuferr;
   unsigned int    err = 0;

      nRun = Time_GetNumrun();
//   nRunG = nRun;  // ��������� ����� ����� � ���������� ����������
   prun  = &CONFIG_IDRUN[nRun];       // ������� ����p �����
   aprun = &CONFIG_ADRUN[nRun];       // ������� ����p �����
   pcalc = &CALCDATA[nRun];
        index = valGERG91;
        perr = errGERG91;
   switch (nRun) {
      case 0: pbuferr = calc_buferrrun0; break;
      case 1: pbuferr = calc_buferrrun1; break;
      case 2: pbuferr = calc_buferrrun2; break;
   }

 if (pcalc->t < prun->R0min) err=perr[8];    //�஢�ઠ ���⭮��
 else
 if (pcalc->t > (prun->R0min+0.01)) err=perr[9];  else  err = pbuferr[4];
   if (err != pbuferr[4]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[4] = err;                 // �p������� ����� ��������
   }

 if (pcalc->t > prun->R0max) err=perr[10];
 else
 if (pcalc->t < (prun->R0max-0.01)) err=perr[11]; else  err = pbuferr[5];
   if (err != pbuferr[5]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pbuferr[5] = err;                 // �p������� ����� ��������
   }
   if  (pcalc->t > prun->R0min)  // >Romin
    pcalc->Q  = pcalc->DP/pcalc->t;  // �����
   else  pcalc->Q  = pcalc->DP/prun->R0min;

   if (prun->constdata & 0x01) //� �������
   {
    pcalc->DP = aprun->dPh;  //�������� ������
    IndDP[nRun] = pcalc->DP;
   }  else                      // ���������
   { pcalc->DP = prun->constdP;
     IndDP[nRun] = prun->constdP;
   }
    if (pcalc->DP <= -prun->dPots)   // �������� �����
    {    err = 0xC0;
         if (err != pbuferr[13]) {          // ������ ��������
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[13] = err;              // �p������� ����� ��������
      }
    }
    else                         // ����� ���������
    {  err = 0x40;
        if (err != pbuferr[13]) {          // ������ ��������
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[13] = err;              // �p������� ����� ��������
      }
    }

    if (prun->constdata & 0x02)         // ������ ��������
     pcalc->Pa = aprun->P;   // ����� �����
    else   pcalc->Pa = prun->constP;
      IndQ[nRun] = pcalc->Q;
        TaskWriteArcData();      // ������ � ����� � ���������
}
//-----------------------------------------------------------
/*
void SetamountPT(int nRun)
{  struct AdRun            *aprun;      // ��p����p� �����
   struct IdRun    *prun;               // ��������� �� ��p����p� �����
   struct calcdata *pcalc;

   prun = &CONFIG_IDRUN[nRun];        // ��������� �� ������� �����
   aprun  = &CONFIG_ADRUN[nRun];      // ������� ����p �����
   pcalc = &CALCDATA[nRun];
//   Err1MODBUSQw[nRun] = ErrMODBUSQw[nRun];
  if ((CONFIG_IDRUN[nRun].TypeRun) == 10)
  {
     if (errMODBUS[nRun]>0)
       ErrMODBUSQw[nRun]=1;
     else ErrMODBUSQw[nRun]=0;
  }
  else if ((RS485HF[nRun] == 1)&&(errQw[nRun]==0x193) ||
          (errMODBUS[nRun]>0)&&(errQw[nRun]==0x113))
         ErrMODBUSQw[nRun]=1;
       else ErrMODBUSQw[nRun]=0;
   if ((Err1MODBUSQw[nRun]==1)&&(ErrMODBUSQw[nRun]==1)) // �����
     { aprun->Ksq = aprun->Ksq+1.0;
      // ������� �������� P,t �� ������ ���������� �����
      aprun->t1 = (aprun->t1*(aprun->Ksq-1.0) + pcalc->t)/aprun->Ksq;  //aprun->
      aprun->P1 = (aprun->P1*(aprun->Ksq-1.0) + pcalc->Pin)/aprun->Ksq;  //aprun->
     }
//   } // ����� (!((aprun->Qw1 > 0.0) && (pcalc->Qw == 0.0)))
   else   // ��� pcalc->Qw == 0 � aprun->Qw1 > 0  // ����� (������) ���� � �������
   if ((Err1MODBUSQw[nRun]==0)&&(ErrMODBUSQw[nRun]==1))
       { aprun->Ksq = 1.0;
         aprun->t1 = pcalc->t;  //��������� ������ ������
         aprun->P1 = pcalc->Pin;
         aprun->Qw1 = pcalc->Qw;
     }
   else  // ����� ���������
   if ((Err1MODBUSQw[nRun]==1)&&(ErrMODBUSQw[nRun]==0))
   {//   printf("Qw1=%f Qw=%f P1=%f\n", aprun->Qw1,pcalc->Qw,aprun->P1);
        aprun->Ksq = aprun->Ksq+1.0;
        // ������� �������� P,t �� ������ ���������� �����
        aprun->t1 = (aprun->t1*(aprun->Ksq-1.0) + pcalc->t)/aprun->Ksq; //aprun->
        aprun->P1 = (aprun->P1*(aprun->Ksq-1.0) + pcalc->Pin)/aprun->Ksq;// aprun->
        pcalc->t = aprun->t1;  //aprun->
        pcalc->Pin = aprun->P1;  //aprun->
        aprun->Ksq = 1.0;
        aprun->Qw1 = pcalc->Qw;
   }
//printf("Ksq=%f t1=%f P1=%f Qw1=%f t=%f P=%f Qw=%f\n",aprun->Ksq, aprun->t1, aprun->P1,aprun->Qw1,pcalc->t,pcalc->Pin,pcalc->Qw);
}
*/
//---------------------------------------------------------------
float Round_fromKg(float P,double Koef)
{ float r,Round;
  double R1;

 R1 =(double)P*Koef;
 if (R1<10.0) r=10000.0;
 else if (R1 <1000.0) r=1000.0;
 else  r=100.0;
// ������� �� ���/��2 � ��� (���)
 Round  = floor(R1*r+0.5)/r; //���������� �� 2,3 ��� 4 ������ ����� �������
 return Round;
 }
//--------------------------------------------------------------
float Round_toKg(float P,double Koef,float *Eps)
{ float r,Round;
  double R1;

 R1 = (double)P/Koef;
 if (R1<1.0) r=100000.0;
 else if (R1 <100.0) r=10000.0;
 else if (R1 <1000.0) r=1000.0;
 else  r=100.0;
 Round = 1.0/r;
 *Eps=Round - 0.1*Round;
// ������� ��  ��� (���) � ���/��2
 Round  = floor(R1*r+0.5)/r; //���������� �� 2,3 ��� 4 ������ ����� �������
 return Round;
 }
//---------------------------------------------------------------------------
int GetEdErr(int nRun) {         // ������ ������� ���� �� ����� ������ ������� ���������
   struct IdRun *prun;                 // ��������� �� ������� �����
   struct AdRun *aprun;                

   prun = &CONFIG_IDRUN[nRun];
   aprun = &CONFIG_ADRUN[nRun];

   switch(prun->TypeRun) {
      case 0:   // 3095
      case 1:   // P+T+dP
      case 6:
      case 7:
         if ((aprun->statt & 0x04) == 4 || (aprun->statP & 0x04) == 4 ||
             (aprun->statdPh & 0x04) == 4)
            return 1;    // ���� ������
         else
            return 0;    // ��� ������
      case 2:   // 3095+3051CD
      case 3:   // P+T+dP+dP
         if ((aprun->statt & 0x04) == 4 || (aprun->statP & 0x04) == 4 ||
             (aprun->statdPh & 0x04) == 4 || (aprun->statdPl & 0x04) == 4)
            return 1;    // ���� ������
         else
            return 0;    // ��� ������
      case 4:   // �������
      case 5:
         if ((aprun->statt & 0x04) == 4 || (aprun->statP & 0x04) == 4)
            return 1;      // ���� ������
         else
            return 0;   // ��� ������
   }
   return 0;
}
//---------------------------------
void  TaskWriteArcData()
{
  Setperiodtdata();
  Sethourtdata();
  Setdaytdata();
  Setmonthtdata();            // ������ ��������� ������
} 
 
