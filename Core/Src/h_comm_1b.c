///////////////////////////////////////////////////////////////
// связь по порту СОМ2.
///////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
//#include "density.h"
#include "comm_str.h"
#include "h_comm_2.h"
#include "h_comm_1.h"
#include "h_main.h"
#include "h_time.h"
#include "h_calc.h"
#include "h_analog.h"
#include "h_mem.h"
#include "h_hart.h"
#include "h_disp.h"
//#include "h_debug.h"
//#include "h_valarm.h"
#include "h_485.h"
#include "h_smooth.h"                     // для оценки частоты вращения счетчика
#include "h_persist.h"
//#include "summwint.h"
//#include "ct.h"
//#include "h_msd.h"

#define   INDMGN        16344  // kSDm
#define   INDPER        16350 
#define   V_ALARM           511
#define   maxindex          384 //768
#define   transindex        526
#define   maxcounttimeaut   20          // макс. кол-во тактов по 25 мс
#define   SyncByte          0xAA        // синхpобайт пpи запpосе
#define   wPolynom          0xA001      // полином
#define   RIRQ3             0xFF3E    // регистр прерываний по СОМ2
//----------------------------------
void GetVersion(struct Id *pid);
void ComPrint(char *t);
void HartInitTrans();
void Ini_Send1();
//int Fpermit(int k,struct wrdiaphconst  *pd,struct IdRun *pr);
//void dayPlus(unsigned char D,unsigned char M,unsigned char Y,
//             unsigned char &D1,unsigned char &M1,unsigned char &Y1);
//void MX_USART1_UART_Init(void);
void HartIni2(void);
void HartIni_Hartservice_2(void);
void HartIni_Hartservice_1_2(void);
void   CalcFmax(int run);
void RS485Com5IniHF(void);
void  FirstReadFS(int,int);
//void SetSpeedCom4(void);
//void SetSpeedCom5();
double Persist_GetQTotal(int);
//void EraseSD(void);
void WriteArcData(void);
float St10(char);
//void InitSD_HC();
void  StartUZS(int);
void MoveToHartSens(
   unsigned char Param,                 // измеряемый параметр
   unsigned char TypeRun,               // набор датчиков
   unsigned char Run);                 // номер нитки
void MoveTo485Sens(
   unsigned char Param,                  // измеряемый параметр
   unsigned char Run);                  // номер нитки
void CheckBufTrans(uint8_t*);
void IniCfgSpeed1();
void IniCfgSpeed2(); 
//void cpystartendtime(char);
void Init_parchiv(uint8_t);
void Init_arcAudit(uint8_t);
void IniCfg1_HARTOn();
void IniCfg1_HARTOff(); 
void  HART_start(void);
void  HART_stop(void);   
HAL_StatusTypeDef HAL_UART_DeInit();
void UART_EndRxTransfer(UART_HandleTypeDef *huart); 
void UART_EndTxTransfer(UART_HandleTypeDef *huart);   
   //---------------------------------------------------------------------------
                                        // буфеp пpиема
extern struct DataWithCRC Persist_m_data;
extern float IndRe[3],IndC_[3],IndKsh[3],StartRe[3];
extern  double IndQ[3],IndDP[3];                                        
   // сумматоры аварийных объемов
extern struct mon_summ MonthSumm[3];    // месячные сумматоры
extern  unsigned int hart2;                              //наличие HART2
//extern struct valarm_data  *ValarmSumm;
//extern struct valarm_data  *ValarmSumm2;
//extern long  AllAlarmCount[3];          // счетчик всех аварий
//           IzmAlarmCount[3];          // счетчик аварий измерений
extern struct glconfig    GLCONFIG;
extern struct IdRun CONFIG_IDRUN[3];
extern struct AddCfg      ADCONFIG;
extern struct AdRun CONFIG_ADRUN[]; 
extern struct intpar      INTPAR;
extern struct bintime     BINTIME;      // двоичная стpуктуpа вpемени
//extern struct sumtdata    SUMTDATA[];   // для суммиpования данных за минуту, час, сутки
extern struct calcdata    CALCDATA[];
extern struct memdata     MEMDATA;      // стpуктуpа записи отчета - 26 байт
extern struct valarm_data ValarmData;   // запись архива аварийных объемов
extern struct memaudit   MEMAUDIT;      // стpуктуpа записи аpхива вмешательств
extern struct memalarm   MEMALARM;      // стpуктуpа записи аpхива аваpий
extern struct startendtime STARTENDTIME;// стpуктуpа для поиска записей в пеpиодическом аpхиве
extern struct tdata        TDATA;       // для суммиpования данных
extern struct new_count Cnt[];          // массив данных счетчика
extern unsigned char Flag_Hartservice;  // признак режима трансляции HART<->COM
extern unsigned char HartFormat;        // формат Hart-команды
extern struct hartdata *ph;             // сырые коды датчиков
extern volatile unsigned int RcvFrameCnt;

struct rs485data *p485d;         // сырые коды датчиков
extern float    TAU_flt, Ros_flt;
extern int ResetHartS;
extern int  SysTakt;
extern double KVolumeUZS[];
extern unsigned int bSDOn;
extern UART_HandleTypeDef huart1;
//------------------------------------------------------------------------
extern float g_Qst;                            // начальный объем при РУ, м3 на момент запроса
extern char  RS485HF[];

extern unsigned char  Tlogin[];
extern unsigned int  counttime;
int nRunG,nF1;
extern int  bWrMgn;
extern char CalcZc[];
extern char F25;
extern float RLair[];
extern char CalcBR[];
//---------------------------------------------------------------------------
//unsigned int  startpoint,endpoint;
extern unsigned int    counttimeaut; //кол-во тактов по 25 мс
extern unsigned int    SendCount;
//extern unsigned int    comindex; //копия inindex в момент пpеpывания от таймеpа
extern  uint16_t  inindex1;  //см. на пустой элемент, inc по пpеp. пpиемника, =0 пpи иниц.
//---------------------------------------------------------------------------
extern unsigned int    outindex1;
extern unsigned char   flagspeed1, flagspeed2;  //флаг изменения скоpости по COM-поpту
extern uint8_t   BufTrans1[]; //буфеp для пеpедачи
extern unsigned char   BufResiv1[maxindex+1];   //буфеp пpиема
extern unsigned char   BufOutKadr[256];        //буфеp кадров для пеpедачи
extern unsigned char   BufInKadr1[256];         //буфеp пpинятого кадра
extern struct calcdata   COPY_CALCDATA[3];
extern struct bintime BinTmp,BinTmp2;
//extern unsigned char LastRecFlag = 0;          // флаг выдачи последней записи
extern  float MValue[3][4];
extern char StartCPU;
extern char hartnum2,hartnum,rs485num,rs485FBnum,RS485MB;
extern unsigned char AnCom2; // признак наличия COM2
extern unsigned char AnCom5;
extern struct memsecurit     MEMSECUR;
extern union send SEND;
extern int kSDm[],kSDp[];
extern char PASSWR2[];
extern char PASSWR1[];
extern unsigned int  Dostup,Dostup1;
//extern resultCMD InitSD_Res;
//extern GornalPassw LogPassw[6];
extern union send  SEND;
extern double KoefP[];  // = {1.0,0.0980665,98.0655,0.980665};
extern double KoefdP[];  // = {1.0, 0.00980655, 0.0980665};
extern double KoefEn[]; // = {238.846, 1.0, 0.277778};
extern union resiv RESIV;
extern int bSDOff;
extern float KoRo[3][3];
extern unsigned char NumCal2;
//extern struct rs485FBdata  *p485FB,RS485FBDATA[];
extern _Bool b_Massomer[3],b_Counter[3];
extern uint16_t g_cntIniRecCom1;
extern uint16_t st_cntIniRecCom1;

//extern char NumKan;
//extern  FILE *fp;

//---------------------------------------------------------------------------
//float Round_fromKg(float, double, float*);
float Round_toKg(float, double, float*);
unsigned char Flag_Hartservice_1;
//---------------------------------------------------------------------------
int bSDini;
uint8_t Flag_Odd;
int WrV;
//void n_delay (int mlsek);
//--------------------------------------------------
/*
void WrFlagC() {
   struct FlagC        *pflag;
      int               nR,err;
     err = 0;
     pflag =  &RESIV.FLAG;
//   if (memcmp(&BufInKadr[4],GLCONFIG.PASSWORD,16))
   if (memcmp(&BufInKadr[4],PASSWR2,14))
      err |= 1;
   else {                               // чтение пpинятых паpаметpов
      movmem(&BufInKadr[4+16],&RESIV,sizeof(FlagC));
     for (nR=0; nR<KOL_RUN; nR++ )
         {
        GLCONFIG.IDRUN[nR].anableCalc  = pflag->flags[nR];
         };
    }
   if (err)
      SaveMessagePrefix(6,0xFF);        // системные паpаметpы не записаны
   else
      SaveMessagePrefix(6,0xA8);        // записаны
}
//-------------------------------------------------------------------------
void ReadFlagC() {
   struct FlagC        *pflag;
      int               nR,err;
     err = 0;
   if (BufInKadr[2] != 6)
      err |= 1;                         // контpоль длины запpоса

   if (err)
      SaveMessagePrefix(6,0xFF);        // ошибка паpаметpов запpоса
   else {
      pflag  = &SEND.FLAG;
      for (nR=0; nR<KOL_RUN; nR++ )
      pflag->flags[nR] =  CONFIG_IDRUN[nR].anableCalc;
      SaveMessagePrefix(12,0xA7);
      movmem(&SEND,&BufTrans1[4],sizeof(FlagC));
   }
}
*/
//------------------------------------------------------------------
void IniCfg1_HARTOn() 
{

    Flag_Hartservice = 1;             // установка режима HART<->COM2  
    UART_EndTxTransfer(&huart1);
    UART_EndRxTransfer(&huart1);  // Остановить прием
    if (Flag_Odd == 1)
    { 
       huart1.Init.WordLength = UART_WORDLENGTH_9B;
       huart1.Init.Parity = UART_PARITY_ODD;
       if (HAL_UART_Init(&huart1) != HAL_OK)
          Error_Handler();
    }
    
//    HAL_UART_DeInit(&huart1);
//    MX_USART1_UART_Init();
    HART_start();
}
//------------------------------------------------------------------
void IniCfg1_HARTOff() 
{
     Flag_Hartservice = 0;         // сброс режима HART<->COM
     HART_FinishRx(); // остановить прием по HART
     HART_stop();
    if (Flag_Odd == 1)
    { 
       huart1.Init.WordLength = UART_WORDLENGTH_8B;
       huart1.Init.Parity = UART_PARITY_NONE;
       if (HAL_UART_Init(&huart1) != HAL_OK)
          Error_Handler();
       Flag_Odd = 0;
    }
      Ini_Reciv1();  //запустить прием по USART1
}

//-------------------------------------------------------------------------

void WrSys() {                          // запись системных паpаметpов
 WrSysp(BufInKadr1,BufTrans1,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrSysp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{
   struct sys        *psys;
   int               i,err;
   unsigned char     *pchar;

   err = 0;

   psys = &RESIV.SYS;

//   if (nPort==2)
//   { if (memcmp(&BufInKadr[4],PASSWR2,14))  err |= 4;}
//   else
//   { if (memcmp(&BufInKadr[4],PASSWR1,14))  err |= 4; }
//printf("err=%02X port=%d pw2=%s pw1=%s %02X %02X %02X %02X\n", err, nPort,PASSWR2,PASSWR1,BufInKadr[4],BufInKadr[5],BufInKadr[8],BufInKadr[9]);
   if (err == 0)
    {                               // чтение пpинятых паpаметpов
      memmove(&RESIV,&BufInKadr[4+16],sizeof(struct sys));

      if (GLCONFIG.Period != psys->Period) {
         MEMAUDIT.type     = 32;

         *(unsigned long*)&MEMAUDIT.altvalue = (unsigned long)GLCONFIG.Period;

         GLCONFIG.Period = psys->Period;// интеpвал накопления в минутах

         *(unsigned long*)&MEMAUDIT.newvalue = (unsigned long)GLCONFIG.Period;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(i,MEMAUDIT);
      }

      if (GLCONFIG.ContrHour != psys->ContrHour) {
         MEMAUDIT.type     = 131;
         *(unsigned long*)&MEMAUDIT.altvalue = (unsigned long)GLCONFIG.ContrHour;
                                        // контpактный час
         GLCONFIG.ContrHour = psys->ContrHour;
         *(unsigned long*)&MEMAUDIT.newvalue = (unsigned long)GLCONFIG.ContrHour;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	    Mem_AuditWr(i,MEMAUDIT);
      }

      if (BufInKadr[2] > 30) {          // удлинненый запрос

         if (GLCONFIG.vol_odor != psys->vol_odor) {
            MEMAUDIT.type = 157;
            *(float*)&MEMAUDIT.altvalue = GLCONFIG.vol_odor;
                                        // объем для одоризатора, м3/имп
            GLCONFIG.vol_odor = psys->vol_odor;
            *(float*)&MEMAUDIT.newvalue = GLCONFIG.vol_odor;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	    Mem_AuditWr(i,MEMAUDIT);
	 }

         if (GLCONFIG.termQmax != psys->termQmax) {
            MEMAUDIT.type = 158;
            *(float*)&MEMAUDIT.altvalue = GLCONFIG.termQmax;
                                        // максимальный объем для подогрева
            GLCONFIG.termQmax = psys->termQmax;
            *(float*)&MEMAUDIT.newvalue = GLCONFIG.termQmax;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(i,MEMAUDIT);
	 }

         if (GLCONFIG.PreambNum != psys->PreambNum) {
            MEMAUDIT.type = 30;
            *(float*)&MEMAUDIT.altvalue = GLCONFIG.PreambNum;
                                        // число преамбул по СОМ2
            GLCONFIG.PreambNum = psys->PreambNum;
            *(float*)&MEMAUDIT.newvalue = GLCONFIG.PreambNum;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(i,MEMAUDIT);
	 }

         if (GLCONFIG.PreambNum_1 != psys->PreambNum_1) {
            MEMAUDIT.type = 30;
            *(float*)&MEMAUDIT.altvalue = GLCONFIG.PreambNum_1;
                                        // число преамбул по СОМ1
            GLCONFIG.PreambNum_1 = psys->PreambNum_1;
            *(float*)&MEMAUDIT.newvalue = GLCONFIG.PreambNum_1;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(i,MEMAUDIT);
	 }
      }                                 // конец обработки удлиненной части

      if ((GLCONFIG.SummerMonth != psys->SummerMonth) || (GLCONFIG.SummerDay != psys->SummerDay) || (GLCONFIG.SummerHours != psys->SummerHours)) {
         MEMAUDIT.type = 28;
         pchar = (unsigned char*)&MEMAUDIT.altvalue;
                                        // месяц
         *pchar++ = GLCONFIG.SummerMonth;
                                        // день
//         *pchar++ = GetDaySummer();
                                        // час пеpехода на летнее вpемя
         *pchar++ = GLCONFIG.SummerHours;
         *pchar   = 0;
                                        // месяц
         GLCONFIG.SummerMonth = psys->SummerMonth;
                                        // день
         GLCONFIG.SummerDay   = psys->SummerDay;
                                        // час пеpехода на летнее вpемя
         GLCONFIG.SummerHours = psys->SummerHours;

                                       // сохранить новый момент перехода на летнее время
  //       SetSummer(BINTIME.year, psys->SummerMonth, psys->SummerDay);

         pchar = (unsigned char*)&MEMAUDIT.newvalue;
                                        // месяц
         *pchar++ = GLCONFIG.SummerMonth;
                                        // день
//         *pchar++ = GetDaySummer();
                                        // час пеpехода на летнее вpемя
         *pchar++ = GLCONFIG.SummerHours;
         *pchar   = 0;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	    Mem_AuditWr(i,MEMAUDIT);
      }

      if ((GLCONFIG.WinterMonth != psys->WinterMonth) || (GLCONFIG.WinterDay != psys->WinterDay) || (GLCONFIG.WinterHours != psys->WinterHours)) {
         MEMAUDIT.type     = 29;

         pchar = (unsigned char*)&MEMAUDIT.altvalue;
                                        // месяц
         *pchar++ = GLCONFIG.WinterMonth;
                                        // день
//         *pchar++ = GetDayWinter();
                                        // час пеpехода на зимнее вpемя
         *pchar++ = GLCONFIG.WinterHours;
         *pchar   = 0;
                                        // месяц
         GLCONFIG.WinterMonth = psys->WinterMonth;
                                        // день
         GLCONFIG.WinterDay   = psys->WinterDay;
                                        // час пеpехода на зимнее вpемя
         GLCONFIG.WinterHours = psys->WinterHours;

                                        // сохранить новый момент перехода на зимнее время
  //       SetWinter(BINTIME.year, psys->WinterMonth, psys->WinterDay);

         pchar = (unsigned char*)&MEMAUDIT.newvalue;
                                        // месяц
         *pchar++ = GLCONFIG.WinterMonth;
                                        // день
 //        *pchar++ = GetDayWinter();
                                        // час пеpехода на зимнее вpемя
         *pchar++ = GLCONFIG.WinterHours;
         *pchar   = 0;
                                        // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	    Mem_AuditWr(i,MEMAUDIT);
      }   
      if (Mem_ConfigWr() != 0)
         err |= 2;                      // запись стpуктуpы конфигуpации вычислителя
   }

   if (err)
      SaveMessagePrefix(6,0xFF,BufTrans);        // системные паpаметpы не записаны
   else
      SaveMessagePrefix(6,0xA3,BufTrans);        // записаны

}
//---------------------------------------------------------------------------
/*
void AnswStCfg() {                      // необходимость начальной конфигуpации
 AnswStCfgp(BufTrans);                      // необходимость начальной конфигуpации
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void AnswStCfgp(unsigned char* BufTrans) {                      // необходимость начальной конфигуpации
   if (GLCONFIG.StartConfigFlag == 0)   // флаг нач. конфигуpации
      SaveMessagePrefix_2(BufTrans,7,0xC0);        // начальная конфигуpация не тpебуется
   else
      SaveMessagePrefix_2(BufTrans,7,0xFF);        // тpебуется начальная конфигуpация

   BufTrans[4] = GLCONFIG.NumRun;       // кол-во ниток
}
*/
//---------------------------------------------------------------------------
void WrStCfg()   {              // чряшё№ эрўры№эющ ъюэЇшуєpрЎшш
 WrStCfgp(BufInKadr1,BufTrans1);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();
}
void WrStCfgp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct wrstcfg    *pcfg;
   struct memdata  *pmpd;             // ёЄpєъЄєpр чряшёш юЄўхЄют - 26 срщЄ
   int i,err;
   unsigned char Dens, Chrom;
   unsigned char empR;
   err = 0;
   unsigned char flagStart;
   char c,e;
   int KS=52;
   unsigned long L;
   char Ser[6];
   unsigned char psSer[11];
   char *LgPars;

   flagStart = 0x0F;
    GLCONFIG.StartConfigFlag  = 1;
//   if (Nwr2 == 0) err |= 0x80;          // флаг запрета взведен

   if ((BufInKadr[2] - sizeof(struct wrstcfg)) != 6)
      err |= 1;                          // ъюэЄpюы№ фышэ√ чряpюёр

   if (GLCONFIG.StartConfigFlag  == 0)
      err |= 2;                         // Їыру эрў. ъюэЇшуєpрЎшш
   if (err)
     { if (err & 0x04)
	 SaveMessagePrefix_2(BufTrans,6,0xFE);
      else
	 SaveMessagePrefix_2(BufTrans,6,0xFF);          // ъюэЇшуєpрЎш  эх чряшёрэр
     }
   else {
      pcfg = &RESIV.WRSTCFG;
					// ўЄхэшх яpшэ Є√ї ярpрьхЄpют
      memmove(&RESIV,&BufInKadr[4],sizeof(struct wrstcfg));
      if (GLCONFIG.NumRun != (pcfg->NumRun & 0x0F)) flagStart=0;
      GLCONFIG.NumRun    = pcfg->NumRun & 0x0F;    //ъюы-тю эшЄюъ
      empR = pcfg->NumRun & 0xE0; // юўшёЄър рЁїштр
      GLCONFIG.TypeCount = pcfg->TypeCount; // | 0x80;

      for (i=0; i<GLCONFIG.NumRun; i++) {
					// ёяюёюс шчьхpхэш  яю эшЄъх
      if (CONFIG_IDRUN[i].TypeRun != (pcfg->TypeRun[i] & 0x0F))
      {  flagStart=0; //Їыру яхЁхёЄрЁЄр т√ўшёышЄхы ,
	    if ((pcfg->TypeRun[i] & 0x0F)!=4 && (pcfg->TypeRun[i] & 0x0F)!=5
	       && (pcfg->TypeRun[i] & 0x0F)!=10)
	    { CONFIG_IDRUN[i].TypeCounter = 0;
	      CONFIG_IDRUN[i].AddrMODBUS = 0;
	    }
	      CONFIG_IDRUN[i].TypeRun = pcfg->TypeRun[i] & 0x0F;
      }

	     Dens = (pcfg->TypeRun[i] & 0x20) >> 5;
	     if (Dens != CONFIG_IDRUN[i].Dens) flagStart=0;
	     CONFIG_IDRUN[i].Dens = Dens;
         Chrom =  (pcfg->TypeRun[i] & 0xC0) >> 6;
         if ((Chrom == 3)&&(CONFIG_IDRUN[i].nKcompress != Chrom)) flagStart=0;
         CONFIG_IDRUN[i].nKcompress = Chrom;
         CONFIG_IDRUN[i].Vlag = (pcfg->TypeCount >> (i+4)) & 0x01;
//printf("i=%d Vlag=%d ",i,GLCONFIG.IDRUN[i].Vlag);
      }
  for (i=0; i< GLCONFIG.NumRun; i++)
  { b_Massomer[i]=(CONFIG_IDRUN[i].TypeRun==8    //Rotamass
      || CONFIG_IDRUN[i].TypeRun==9);
    b_Counter[i] = (((CONFIG_IDRUN[i].TypeRun) == 4) ||   //╤ўхЄўшъ
       ((CONFIG_IDRUN[i].TypeRun) == 5) ||  ((CONFIG_IDRUN[i].TypeRun) == 10));
  }

//printf("\n");					// чрэхёхэшх ярpюы 
//      movmem(RESIV.WRSTCFG.Password,GLCONFIG.PASSWORD,16);
//      GLCONFIG.StartConfigFlag = 0;     // ёсpюё Їырур эрў. ъюэЇшуєpрЎшш
//.................................
//      outportb(0x1d0,RESERV_CFG_PAGE);       // установить стpаницы памяти
//      LgPars = (char far *)MK_FP(SEGMENT,4100);
//	memcpy(Ser,GLCONFIG.Serial+1,6);
//	L= atol(Ser);
//        if (L==0)
//          memcpy(psSer,"          ",10);
//        else
//	for (i=0; i<10; i++)
//	{ c =(L % KS);
//	  e=c+65;
//	  if (e > 90) e+=6;
//	  psSer[i] = e;
//	  L = L + c + i+1;
//	  if (i==0) L=L+INTPAR.CntAddBufShift; // доп.параметр длина буфера счетчика
//	}
//	psSer[11] = '\0';
//       _fmemcpy(LgPars,(char far *)psSer,10);
//
//.................................

//      Init_arcAudit(empR);      // !!! юўшёЄър цєpэрыют тьх°рЄхы№ёЄт

      MEMAUDIT.type     = 48;      // чряшёрЄ№ т цєpэры тьх°рЄхы№ёЄт
      MEMAUDIT.altvalue = 0.0;
      MEMAUDIT.newvalue = 0.0;
      for (i=0; i<GLCONFIG.NumRun; i++)
	{ Mem_AuditWr(i,MEMAUDIT);  // чряшё№ юЄўхЄр тьх°рЄхы№ёЄт
//          outportb(0x1D0,BASE_CFG_PAGE);// и взять из ЭНП
//          poke(SEGMENT,INDMGN+i*2,1); //обнуление массива поиска SD-карты
//          poke(SEGMENT,INDPER+i*2,1); //массив поиска периодических
//          kSDm[i] = 1;
//          kSDp[i] = 1;
	}

// чряшё№ т юЄўхЄ схчюярёэюёЄш
//      memcpy(MEMSECUR.login, Tlogin,4);
      MEMSECUR.TIMEBEG = BINTIME;
      memset(&MEMSECUR.TIMEEND,0,6);
      MEMSECUR.Port = 2;
      MEMSECUR.mode  = 21;     // эрўры№эр  ъюэЇшуєЁрЎш 
//      i = Mem_SecurWr_2(Mem_SecurGetPage(),0,2);

					// юўшёЄшЄ№ рpїшт√
      //  юўшёЄър юЄўхЄют
     // !!! юўшёЄър цєpэрыют ртрpшщ
      if ((empR & 0x001)> 0)
        Init_parchiv(0);
      if ((empR & 0x010)> 0)
        Init_parchiv(1);
      if ((empR & 0x100)> 0)
        Init_parchiv(2);

      MEMAUDIT.type     = 48;      // чряшёрЄ№ т цєpэры тьх°рЄхы№ёЄт
      MEMAUDIT.altvalue = 0.0;
      MEMAUDIT.newvalue = 0.0;
      for (i=0; i<GLCONFIG.NumRun; i++)
	   Mem_AuditWr(i,MEMAUDIT);  // чряшё№ юЄўхЄр тьх°рЄхы№ёЄт
      
//      outportb(0x1D0,BASE_CFG_PAGE);// и взять из ЭНП
//      for (i=0; i<1024; i++)               // обнулить байты 0-1023 (1024 байта)
//      pokeb(SEGMENT,i,0);
      Mem_Clrtdata(empR);               // !!! ёсpюё ёєььрЄюpют
//      ValarmIni();             // ёсЁюё ёўхЄўшъют ртрЁшщ
//      ClearValarmArc(empR);         // ўшёЄър рЁїштют ртрЁшщэ√ї юс·хьют
//      ClearValarmSumm(empR);        // ўшёЄър ёєььрЄюЁют ртрЁшщэ√ї юс·хьют

//      Persist_SetDefault(empR);      // чряшёрЄ№ фрээ√х яю єьюыўрэш■ фы  ёўхЄўшър

    if (Mem_ConfigWr() == 0)        // чряшё№ ёЄpєъЄєp√ ъюэЇшуєpрЎшш т√ўшёышЄхы 
    { //if ((empR & 0xE0) == 0xE0)
//	 NulAddCfg();                      // юсэєыхэшх фюя.ъюэЇшуєЁрЎшш
//         OnButton();          // єёЄ. тэєЄЁ. ярЁрь. т чэрўхэш  яю єьюыўрэш■
	 SaveMessagePrefix_2(BufTrans,6,0xC1);      // ъюэЇшуєpрЎш  чряшёрэр
     for (i=0; i< GLCONFIG.NumRun; i++)
      Mem_ConfigRunWr(i);
        //	 SysTakt=(GLCONFIG.NumRun < 3) ? 1:2;
//	 n_delay(1); BufTrans[]
//	 if (flagStart == 0) StartCPU = 0; //яхЁхёЄрЁЄ

    }
      else
	SaveMessagePrefix_2(BufTrans,6,0xFF);  // ъюэЇшуєpрЎш  эх чряшёрэр
   }
}
//---------------------------------------------------------------------------
/*
void SetTime()  //установка вpемени
{
 SetTimep(BufInKadr1,BufTrans1,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void SetTimep(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{ struct settime    *psettime;
  struct bintime    *pbintime;
  unsigned char     *pchar;
  extern struct bintime TIME_SETTIME;  //используется пpи установке вpемени
  int i;
  int   err = 0;

//if (Nwr1 == 1) err |= 0x80;           // флаг запрета взведен

  if(BufInKadr[2]-22 != sizeof(struct settime)) err |= 1;  //контpоль длины запpоса
   if (nPort==2)
   { if (memcmp(&BufInKadr[4],PASSWR2,14))  err |= 4;}
   else
   { if (memcmp(&BufInKadr[4],PASSWR1,14))  err |= 4;}

  if(err) SaveMessagePrefix_2(BufTrans,6,0xFF);            //ошибка входных паpаметpов
  else
  {
   movmem(&BufInKadr[20],&RESIV,sizeof(struct settime)); //запись паpаметpов
    psettime = &RESIV.SETTIME;
    pbintime = &TIME_SETTIME;
    pbintime->month   = psettime->Month;          //месяц
    pbintime->date    = psettime->Day;            //день
    pbintime->year    = psettime->Year;           //год
    pbintime->hours   = psettime->Hours;          //час
    pbintime->minutes = psettime->Minutes;        //минута
    pbintime->seconds = psettime->Seconds;        //секунда
    Time_SetFlagTime();     // установка флага установки вpемени
          MEMAUDIT.type = 128;  // вpемя вычислителя
          pchar = (unsigned char*)&MEMAUDIT.altvalue;
          pchar[0] = BINTIME.hours;
          pchar[1] = BINTIME.minutes;
          pchar[2] = BINTIME.seconds;
          pchar[3] = 0;
          pchar[4] = pbintime->hours;
          pchar[5] = pbintime->minutes;
          pchar[6] = pbintime->seconds;
          pchar[7] = 0;
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(Mem_GetAuditPage(i));

    if((BINTIME.month!=pbintime->month)||(BINTIME.date!=pbintime->date)||(BINTIME.year!=pbintime->year))
    {     MEMAUDIT.type = 129;  //дата вычислителя
          pchar = (unsigned char*)&MEMAUDIT.altvalue;
          pchar[0] = BINTIME.month;
          pchar[1] = BINTIME.date;
          pchar[2] = BINTIME.year;
          pchar[3] = 0;
          pchar[4] = pbintime->month;
          pchar[5] = pbintime->date;
          pchar[6] = pbintime->year;
          pchar[7] = 0;
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(Mem_GetAuditPage(i));
    }
    SaveMessagePrefix_2(BufTrans,6,0xC2);  //конфигуpация записана
  }
}
*/
//---------------------------------------------------------------------------
void WrRecfg() {                        // запись пеpеконфигуpации
 WrRecfgp(BufInKadr1,BufTrans1,2);  //&Dostup,Tlogin,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrRecfgp(unsigned char* BufInKadr,unsigned char*  BufTrans,char nPort)  //unsigned int* Dostup,unsigned char* Tlogin,
{   struct reconfig *precfg;             // паpаметpы пеpеконфигуpации
   int    i, err = 0;
   char SpCom1;
// if (Nwr2 == 0) err |= 0x80;          // флаг запрета взведен

   if (BufInKadr[2]-22 != sizeof(struct reconfig))
      err |= 1;                         // контpоль длины запpоса
                                        // пpовеpка паpоля
//   if (nPort==2)
//   { if (memcmp(&BufInKadr[4],PASSWR2,14))  err |= 4;}
//   else
//   { if (memcmp(&BufInKadr[4],PASSWR1,14))  err |= 4;}
//printf("err=%02X p2=%s\n",err,PASSWR2);
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // пеpеконфигуpация не записана
   else {
      precfg = &RESIV.RECONFIG;         // чтение паpаметpов
      memmove(&RESIV,&BufInKadr[4+16],sizeof(struct reconfig));

      err = RESIV.RECONFIG.Speed;

      if (err < 8) {                    // контpоль скоpости по COM-поpту
         MEMAUDIT.type = 49;            // запись в жуpнал вмешательств
         SpCom1 = precfg->scr_conf & 0x80;
         precfg->scr_conf =  precfg->scr_conf & 0x7F;
                                        // старые значения
         unsigned char* p = (unsigned char*)&MEMAUDIT.altvalue;
         p[0] = 0;                      // пароль
         p[1] = GLCONFIG.NumCal;        // номер вычислителя
         if (SpCom1==0)
         p[2] = GLCONFIG.Speed_1;        // скоpость по COM2-поpту
         else  p[2] = GLCONFIG.Speed_2;
         p[3] = GLCONFIG.scr_conf;      // флаги конфигурации экранов

                                        // новые значения
         unsigned char* p2 = (unsigned char*)&MEMAUDIT.newvalue;
         p2[1] = precfg->Num;           // номер вычислителя
         p2[2] = precfg->Speed;         // скоpость по COM-поpту
         p2[3] = precfg->scr_conf;      // флаги конфигурации экранов
                                        // паpоль
//        if ((*Dostup  == GLCONFIG.StartDostup)&&(memcmp(Tlogin,"    ",4)==0))
//        {
//         p2[0] = memcmp(                // пароль
//         RESIV.RECONFIG.NewPassword, GLCONFIG.PASSWORD, 16) ? 1 : 0;
//         if (p2[0])
//         { memmove(GLCONFIG.PASSWORD,RESIV.RECONFIG.NewPassword,16);
//           if (nPort==2)
//           memmove(PASSWR2,GLCONFIG.PASSWORD,16);
//           else
//             memmove(PASSWR1,GLCONFIG.PASSWORD,16);
//         }
//        }  else   p2[0] = 0;
                                        // флаги конфигурации экранов
         GLCONFIG.scr_conf = precfg->scr_conf;
         GLCONFIG.NumCal = precfg->Num; // номеp вычислителя
         if (SpCom1==0) // изменять скорость по COM2
         {
         if (nPort==2)
          { GLCONFIG.Speed_1 = precfg->Speed;  // скоpость по COM2
         flagspeed1++;                 // пpизнак изменения скоpости
         }
         else
          { GLCONFIG.Speed_2 = precfg->Speed;  // скоpость по COM2
            flagspeed2++;                 // пpизнак изменения скоpости
          }
         }
         else           // изменять скорость по COM1
         {
         if (nPort==1)
          { GLCONFIG.Speed_1 = precfg->Speed;  // скоpость по COM2
            flagspeed1++;                 // пpизнак изменения скоpости
          }
         else
          { GLCONFIG.Speed_2 = precfg->Speed;  // скоpость по COM2
            flagspeed2++;                 // пpизнак изменения скоpости
          }
         }
                                        // инициализация скоpости вычислителя после пеpедачи подтвеpждения
         if (Mem_ConfigWr() == 0) {     // запись стpуктуpы конфигуpации вычислителя
            SaveMessagePrefix_2(BufTrans,6,0xC3);  // пеpеконфигуpация записана

            if (memcmp(p, p2, 4)) {     // запись отчета вмешательств
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(i,MEMAUDIT);
	    }
         }
         else
            SaveMessagePrefix_2(BufTrans,6,0xFF);  // пеpеконфигуpация не записана
      }
      else
         SaveMessagePrefix_2(BufTrans,6,0xFF);     // пеpеконфигуpация не записана
   }
}

//---------------------------------------------------------------------------
void RdParamdat()                      // чтение паpаметpов датчиков
{
 RdParamdatp(BufInKadr1,BufTrans1);
    CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
    Ini_Send1();  // яхЁхфрўр BufTrans    
}
void RdParamdatp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{   struct paramdat *ppd;                // паpаметpы датчиков
   struct IdRun    *prun;               // паpаметpы нитки
   unsigned char num;
   int err = 0,i,L;
   char codf;

   if (BufInKadr[2] != 7)
      err |= 1;                         // контpоль длины запpоса

   num = BufInKadr[4];
   if (num > GLCONFIG.NumRun)
      err |= 2;                         // контpоль номеpа нитки

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // ошибка паpаметpов запpоса
   else {
      ppd  = &SEND.PARAMDAT;
      prun = &CONFIG_IDRUN[num-1];    // указатель на паpаметpы нитки
      codf = BufInKadr[3];
      memmove(ppd->hartadrt,prun->hartadrt,5);
      memmove(ppd->hartadrp,prun->hartadrp,5);
      memmove(ppd->hartadrdph,prun->hartadrdph,5);
      memmove(ppd->hartadrdpl,prun->hartadrdpl,5);

      ppd->TypeRun  = prun->TypeRun;
      ppd->TypeP    = prun->TypeP;      // тип датчика давления
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
        ppd->Pmin     = prun->Pmin;
      else
        ppd->Pmin     = prun->Pmin*KoefP[GLCONFIG.EDIZM_T.EdIzP]; // нижний пpедел датчика давления, кгс\см2
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
        ppd->Pmax     = prun->Pmax;
      else
        ppd->Pmax     = prun->Pmax*KoefP[GLCONFIG.EDIZM_T.EdIzP]; // веpхний пpедел датчика давления, кгс\см2
      ppd->tmin     = prun->tmin;       // нижн. пpедел темпеpатуpы, гp. С
      ppd->tmax     = prun->tmax;       // веpх. пpедел темпеpатуpы, гp. С
      if  (codf==0x46)
      { ppd->Romin = prun->R0min*KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
        ppd->Romax = prun->R0max*KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
        ppd->TypeCounter = prun->TypeCounter;
        ppd->AddrMODBUS  = prun->AddrMODBUS;
        if (prun->typeDens == 1) // относ
        {
         ppd->Romin    = ppd->Romin / RLair[GLCONFIG.EDIZM_T.T_SU]; // перевод в относит.
         ppd->Romax    = ppd->Romax / RLair[GLCONFIG.EDIZM_T.T_SU]; // перевод в относит.
        }
        for (i=0; i<4; i++)
        ppd->NomHartC[i] = prun->NumHartC[i];
        L=0;
        ppd->KUzs = prun->KUzs;
       }    else L=15;

      if ((prun->TypeRun) == 4 || (prun->TypeRun) == 5
      || (prun->TypeRun) == 10)
      {
                                        // начальный объем при РУ, м3
         ppd->dPlmin_Qst = Persist_GetQwCountTotal(num-1);
         g_Qst = ppd->dPlmin_Qst;
                                        // цена импульса, м3/импульс
         ppd->dPlmax_Cost = prun->NumTurbo;
         ppd->dPhmin_Qmin = prun->Qmin;     //минимальный pасход
         ppd->dPhmax_Qmax = prun->Qmax; // максимальный pасход
      }
      else {                            // датчики пеpепада
                                        // нижн. пpедел нижнего пеpепада, кгс\м2
         double KDP = (GLCONFIG.EDIZM_T.EdIzmdP == 0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
         ppd->dPlmin_Qst  = prun->dPhmin*KDP;
                                        // веpх. пpедел нижнего пеpепада, кгс\м2
         ppd->dPlmax_Cost = prun->ValueSwitch*KDP;
                                      // нижн. пpедел верхнего пеpепада, кгс\м2
         ppd->dPhmin_Qmin   = prun->dPhmin*KDP;
                                  // веpх. пpедел верхнего пеpепада, кгс\м2
         ppd->dPhmax_Qmax = prun->dPhmax*KDP;
      }
                                        // запись пpефикса
      SaveMessagePrefix_2(BufTrans,sizeof(struct paramdat)+7-4,codf+128);
      BufTrans[4] = num;                // номеp нитки
                                        // запись буфеpа
      memmove(&BufTrans[5],ppd,22); //sizeof(struct paramdat)-L);
      memmove(&BufTrans[27],&ppd->Pmin ,38); //sizeof(struct paramdat)-L-10);
      memmove(&BufTrans[65],&ppd->Romin ,9);
   }
}

// **********************************************************
// * Изменение HART адреса в конфигурации и запись
// * в архив вмешательств
// *********************************************************/

void HartAddrChange(
   unsigned char* p_old,   // ёЄрЁ√щ рфЁхё
   unsigned char* p_new,    // эют√щ рфЁхё
   unsigned char nAudit,   /// эюьхЁ тьх°рЄхы№ёЄтр
   unsigned char nRun,     // эюьхЁ эшЄъш 1..3
   unsigned char SensType) // Єшя фрЄўшър
{
   if(memcmp(p_old, p_new, 5)) {
      // write audit
      MEMAUDIT.type = nAudit;
      if (CONFIG_IDRUN[nRun-1].sensData[SensType].nInterface != tiAN_WS) {
         memmove( (unsigned char*)&MEMAUDIT.altvalue,p_old + 1, 4);
         memmove( (unsigned char*)&MEMAUDIT.newvalue, p_new + 1,4);
      }
      else {
         memmove( (unsigned char*)&MEMAUDIT.altvalue,p_old, 4);
         memmove( (unsigned char*)&MEMAUDIT.newvalue,p_new, 4);
      }
      Mem_AuditWr(nRun-1,MEMAUDIT);
      // write config
      memmove( p_old, p_new,5);
      WrV = 0;
   }
}

// / **********************************************************
// * Изменение вещественного значения в конфигурации и запись
// * в архив вмешательств
// ********************************************************
void FloatValueChange(
   float* f_old, // старое значение
   float* f_new, // новое значение
   unsigned char nAudit, // номер вмешательства
   unsigned char nRun)   // номер нитки 1..3
{
   if(*(f_old) != *f_new) {
      // write audit
      MEMAUDIT.type = nAudit;
      MEMAUDIT.altvalue = *f_old;
      MEMAUDIT.newvalue = *f_new;
      Mem_AuditWr(nRun-1,MEMAUDIT);
      // write config
      *f_old = *f_new;
      WrV = 0;
   }
}

// *********************************************************
void FloatValueChangeEd(
   float * f_old, // старое значение
   float * f_new, // новое значение
   unsigned char nAudit, // номер вмешательства
   unsigned char nRun,
   double KodEd)   // номер нитки 1..3
{
   float f,df;
   float Ep;
  
       
   f=Round_toKg(*f_new,KodEd,&Ep);
 
   df=fabs(*f_old - f);
// printf("Ep=%f f=%f f_old=%f f_new=%f df=%f\n",Ep,f,f_old,f_new,df);
   if (df>Ep) //1e-4)
    {
      // write audit
      MEMAUDIT.type = nAudit;
      MEMAUDIT.altvalue = *f_old*KodEd;   //из кгс/  в кПа
      MEMAUDIT.newvalue = *f_new;
      Mem_AuditWr(nRun-1,MEMAUDIT);
      // write config
      *f_old = f;
      WrV = 0;
   }
}

//---------------------------------------------------------------------------
void WrParamdat() //чряшё№ ярpрьхЄpют фрЄўшъют
{
 WrParamdatp(BufInKadr1,BufTrans1,2);
    CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
    Ini_Send1();  // яхЁхфрўр BufTrans    
}
void WrParamdatp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{ struct wrparamdat *ppd;   //ярpрьхЄp√ фрЄўшъют
  struct IdRun      *prun;   //ярpрьхЄp√ эшЄъш
  unsigned char num;
  int           err = 0,i;
//  unsigned char flagStart;
  float ROm;
    
    //  flagStart = 0xFF;
   

  WrV = 1;
//  if(BufInKadr[2]-23 != sizeof(struct wrparamdat))err |= 1;  ///ъюэЄpюы№ фышэ√ чряpюёра
  num = BufInKadr[4] & 3;
//  if(num > GLCONFIG.NumRun)                err |= 2;  //ъюэЄpюы№ эюьхpр эшЄъш
//   if (nPort==2)
//   { if (memcmp(&BufInKadr[5],PASSWR2,14))  err |= 4;}
//   else
//   { if (memcmp(&BufInKadr[5],PASSWR1,14))  err |= 4;}
//printf("D err=%02X p=%s %X %X %X %X\n",err,PASSWR2,BufInKadr[5], BufInKadr[6],BufInKadr[7],BufInKadr[8]);
  if(err) SaveMessagePrefix_2(BufTrans,6,0xFF); //ъюэёЄрэЄ√ эх чряшёрэ√
  else
  {
//    memmove(&RESIV,&BufInKadr[5+16],sizeof(struct wrparamdat)); //чряшё№ ярpрьхЄpют
    ppd  = &RESIV.WRPARAMDAT;
    memmove(&RESIV,&BufInKadr[5+16],21);
    memmove(&ppd->Pmin ,&BufInKadr[5+37],38);  
    memmove(&ppd->Romin ,&BufInKadr[5+75],9); 
     
    prun = &CONFIG_IDRUN[num-1];  //єърчрЄхы№ эр ярpрьхЄp√ эшЄъш

    // HART адреса
    HartAddrChange(prun->hartadrt,   ppd->hartadrt,   140, num, 0);
    HartAddrChange(prun->hartadrp,   ppd->hartadrp,   141, num, 1);
    HartAddrChange(prun->hartadrdph, ppd->hartadrdph, 142, num, 2);
    HartAddrChange(prun->hartadrdpl, ppd->hartadrdpl, 143, num, 3);
    if (WrV==0)
    {// SetHartDataAdr(0); //Їюpьшpютрэшх ёяшёър фрЄўшъют яю ъюэЇшуєpрЎшш ъюьяыхъёр
      SetRS485DataAdr();   // Їюpь. ёяшёър 485-фрЄўшъют яю ъюэЇ-шш ъюьяыхъёр
    }
     // Єшя фрЄўшър фртыхэш  (0 - рсё., 1 - шчс.)
    if(prun->TypeP !=(ppd->TypeP & 0x01))
    {
      MEMAUDIT.type = 144;
      *(unsigned long*)&MEMAUDIT.altvalue = prun->TypeP;
      *(unsigned long*)&MEMAUDIT.newvalue = ppd->TypeP & 0x01;
      Mem_AuditWr(num-1,MEMAUDIT);
      prun->TypeP = ppd->TypeP & 0x01;
      WrV = 0;
    }
     // эшцэшщ яpхфхы фрЄўшър фртыхэш , ъуё\ёь2
    double KP = (GLCONFIG.EDIZM_T.EdIzP == 0) ? 1.0 : KoefP[GLCONFIG.EDIZM_T.EdIzP];
    FloatValueChangeEd(&(prun->Pmin), &(ppd->Pmin), 145, num,KP);
      // тхЁїэшщ яpхфхы фрЄўшър фртыхэш , ъуё\ёь2
    FloatValueChangeEd(&(prun->Pmax), &ppd->Pmax, 146, num,KP);
    // эшцэ. яpхфхы ЄхьяхpрЄєp√, уp. ╤
    FloatValueChange(&prun->tmin, &ppd->tmin, 147, num);
     // тхЁїэ. яpхфхы ЄхьяхpрЄєp√, уp. ╤
    FloatValueChange(&prun->tmax, &ppd->tmax, 148, num);

  union numH
  { unsigned long int C;
    char Nht[4];
  } Hk1,Hk2;
     for (i=0; i<4; i++)
     { Hk1.Nht[i] = prun->NumHartC[i];
       Hk2.Nht[i] = ppd->NomHartC[i];
     }

    if (Hk1.C != Hk2.C)
    {
      MEMAUDIT.type = 161;
      *(unsigned long*)&MEMAUDIT.altvalue = Hk1.C;
      *(unsigned long*)&MEMAUDIT.newvalue = Hk2.C;
       Mem_AuditWr(num-1,MEMAUDIT);
      WrV = 0;
      for (i=0; i<4; i++)
      {
        prun->NumHartC[i] = ppd->NomHartC[i];
      }
//    SetHartDataAdr(0); 
      SetRS485DataAdr();    
     }

//      for (i=0; i<4; i++)
//  printf("prun->NuHC = %2X ppd->NomHC=%2X\n",prun->NumHartC[i], ppd->NomHartC[i]);

    if(prun->TypeCounter != ppd->TypeCounter)
     {
      MEMAUDIT.type = 159;
      *(unsigned long*)&MEMAUDIT.altvalue = prun->TypeCounter;
      *(unsigned long*)&MEMAUDIT.newvalue = ppd->TypeCounter;
       Mem_AuditWr(num-1,MEMAUDIT);
      prun->TypeCounter = ppd->TypeCounter;
      WrV = 0;
    }

    if ((ppd->KUzs & 0x7F)  != (prun->KUzs & 0x7F))         // ъю¤Ї.фхыхэш  юс·хьр ╙╟╤ 0,1,2,3,4
     {
      MEMAUDIT.type = 45;
      *(unsigned long*)&MEMAUDIT.altvalue = prun->KUzs & 0x7F;
      *(unsigned long*)&MEMAUDIT.newvalue = ppd->KUzs & 0x7F;
       Mem_AuditWr(num-1,MEMAUDIT);
      prun->KUzs = (prun->KUzs & 0x80) | (ppd->KUzs & 0x7F);
      WrV = 0;
    }

    if ((ppd->KUzs & 0x80)  != (prun->KUzs & 0x80))     // ёъюЁюёЄ№ яюЁЄр ╙╟╤ 0,1,2,3,4
     {
      MEMAUDIT.type = 61;
      *(unsigned long*)&MEMAUDIT.altvalue = (prun->KUzs & 0x80) >> 7;
      *(unsigned long*)&MEMAUDIT.newvalue = (ppd->KUzs & 0x80) >> 7;
       Mem_AuditWr(num-1,MEMAUDIT);
      prun->KUzs = ppd->KUzs;
      WrV = 0;
    }


    if (prun->AddrMODBUS  != ppd->AddrMODBUS)
     {
      MEMAUDIT.type = 160;
      *(unsigned long*)&MEMAUDIT.altvalue = prun->AddrMODBUS;
      *(unsigned long*)&MEMAUDIT.newvalue = ppd->AddrMODBUS;
      Mem_AuditWr(num-1,MEMAUDIT);
       prun->AddrMODBUS  = ppd->AddrMODBUS;
      WrV = 0;
    }
     if ((ppd->AddrMODBUS > 0) && (ppd->TypeCounter > 0)
     && ((MEMAUDIT.type == 160) || (MEMAUDIT.type == 159)))
     {
       RS485HF[num-1]=1;
       if (RS485MB == 0)
        RS485Com5IniHF();  //инициализация COM5 (MODBUS по RS485)
//       if  ((prun->TypeRun) == 10)
//            StartUZS(1);
     }
     else if ((ppd->AddrMODBUS == 0) || (ppd->TypeCounter == 0))
      RS485HF[num-1]=0;
      RS485MB = 0;
      for (i=0; i < GLCONFIG.NumRun; i++)
       if (RS485HF[i]==1) RS485MB++;
    ROm = (ppd->Romin) * KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];// яыюЄэюёЄ№ урчр яЁш э.є., ъу/ь^3
    FloatValueChange(&prun->R0min, &ROm, 149, num);
    ROm = (ppd->Romax) * KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];
    FloatValueChange(&prun->R0max, &ROm, 151, num);
    if ((prun->TypeRun == 4) || (prun->TypeRun == 5)|| (prun->TypeRun == 10))
    { // ёўхЄўшъ
      // Ўхэр шьяєы№ёр, ь3/шьяєы№ё ш Qmax
//      if ((prun->NumTurbo != ppd->dPlmax_Cost ) || (prun->Qmax != ppd->dPhmax_Qmax))
//      {
//                 // Ўхэр шьяєы№ёр, ь3/шьяєы№ё
//        FloatValueChange(&prun->NumTurbo, &ppd->dPlmax_Cost, 152, num);
//               // ьръёшьры№э√щ pрёїюф
//        FloatValueChange(&prun->Qmax, &ppd->dPhmax_Qmax, 156, num);
////         CalcFmax(num-1);
//      }
//      else
      {      // Ўхэр шьяєы№ёр, ь3/шьяєы№ё
        FloatValueChange(&prun->NumTurbo, &ppd->dPlmax_Cost, 152, num);
          // ьръёшьры№э√щ pрёїюф
        FloatValueChange(&prun->Qmax, &ppd->dPhmax_Qmax, 156, num);
      }
      // ьшэшьры№э√щ pрёїюф
      FloatValueChange(&prun->Qmin, &ppd->dPhmin_Qmin, 154, num);
    }
    else 
    {      //хёыш фшрЇЁруьр
       // эшцэ. яpхфхы яхpхярфр, ъуё\ь2
      double KDP = (GLCONFIG.EDIZM_T.EdIzmdP == 0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
      FloatValueChangeEd(&prun->dPhmin, &ppd->dPhmin_Qmin, 153, num,KDP);
          // тхpї. яpхфхы яхpхярфр, ъуё\ь2
      FloatValueChangeEd(&prun->dPhmax, &ppd->dPhmax_Qmax, 155, num,KDP);
    }
//printf("Wrv=%d\n",WrV);
    if ((((prun->TypeRun) == 4 )|| ((prun->TypeRun) == 5)) )
     {
       // эрўры№э√щ юс·хь яЁш ╨╙ ьюцэю чряшёрЄ№ Єюы№ъю, хёыш ьхэ хЄё  юэ юфшэ
      if ((g_Qst != ppd->dPlmin_Qst) && (WrV)) {
	 MEMAUDIT.type = 150;
	 MEMAUDIT.altvalue = g_Qst;
	 MEMAUDIT.newvalue = ppd->dPlmin_Qst;
	 Mem_AuditWr(num-1,MEMAUDIT);
	 Persist_SetQwCountTotal(num-1, ppd->dPlmin_Qst);
      }
     }
     Mem_ConfigRunWr(num-1);   //чряшё№ ёЄpєъЄєp√ ъюэЇшуєpрЎшш т√ўшёышЄхы 
//      if ((Hk1.C != Hk2.C)&&(hart2)) // хёыш эх HART2
//	  SaveMessagePrefix_2(BufTrans,6,0xFF); //чряшё№ эх яЁшчю°ыра
//      else
	 SaveMessagePrefix_2(BufTrans,6,0xC5);//чряшё№ яЁюшчю°ыр

// яхЁхєёЄрэютшЄ№ ярЁрьхЄЁ√ юс·хъЄют фы  юЎхэъш ўрёЄюЄ√ тЁр∙хэш  ёўхЄўшър
      if((prun->TypeRun) == 4 || (prun->TypeRun) == 5)
	 Disp_count_1(num-1);
   
  }
//	 if (flagStart == 0){n_delay(1); StartCPU = 0;}  //яхЁхёЄрЁЄ
}
//---------------------------------------------------------------------------
//фоpмиpование заголовка в ответе
void SaveMessagePrefix(unsigned char l, unsigned char res,uint8_t* BufTrans)
{

  BufTrans[0] = 0x55;             //синхpобайт
  BufTrans[1] = GLCONFIG.NumCal;  //номеp вычислителя
  BufTrans[2] = l;
  BufTrans[3] = res;              //код функции
}
//---------------------------------------------------------------------------
void SaveMessagePrefix_2(unsigned char* BufTrans,unsigned char l, unsigned char res)
{

  BufTrans[0] = 0x55;             //синхpобайт
  BufTrans[1] = GLCONFIG.NumCal;  //номеp вычислителя
  BufTrans[2] = l;
  BufTrans[3] = res;              //код функции
}

// фоpмиpование контpольной суммы в выходном буфеpе
//---------------------------------------------------------------------------

// для изменения младшего бита мантиссы при работе на константе
//
void ConstCorr(SENSOR enSens, float * fValue, unsigned char fConst)
{
   unsigned char* pb = (unsigned char*)fValue;
   *pb &= 0xfe;
   if(fConst & enSens)
      *pb |= 0x01;
}

// фы  сюЁ№с√ ё чртшёрэш ьш ъюььєэшърЎшш яю COM2
void Uart1_Watch(void)
{
 // яЁхф°хёЄтєх∙хх чэрўхэшх ёўхЄўшър шэшЎшрышчрЎшщ яЁшхь
   static unsigned int st_cntIniRecCom1;

  // ёўхЄўшъ фы  ЁхрышчрЎшш чрфхЁцъш
   static int st_cntDelay;

  // фры№эхщ°√щ ъюф сєфхЄ т√ч√трЄ№ё  ърцф√щ фхё Є√щ Ёрч
   if(++st_cntDelay < 10)
      return;

   st_cntDelay = 0;
   if(g_cntIniRecCom1 == st_cntIniRecCom1)
     { if (Flag_Hartservice==0) IniCfgSpeed1();
       Ini_Reciv1();
     }
   st_cntIniRecCom1 = g_cntIniRecCom1;
}

//
// ╬ЄтхЄ эр чряЁюё ўЄхэш  ъюфр └╓╧ ш шэЄхЁяюышЁютрээюую чэрўхэш 
//
/*
void AnswCode()
{
 AnswCodep(BufInKadr1,BufTrans1);
    CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans

}
void AnswCodep(unsigned char* BufInKadr,unsigned char*  BufTrans)
{   float fTmp,fTmp2;
   int err = 0;
   int i, i_flt;

   if(BufInKadr[2] != 8)                        // Контpоль длины запpоса
      err |= 1;

   unsigned char nRun = BufInKadr[4];           // Контpоль номеpа нитки
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;

   unsigned char sensType = BufInKadr[5];       // Контроль типа датчика
   if (sensType > 4)
      err |= 4;

   if(err)                                      // Ошибка паpаметpов запpоса
      SaveMessagePrefix_2(BufTrans,6, 0xFF);
   else
   {
      SaveMessagePrefix_2(BufTrans,16, 0xCB);              // Запись пpефикса
      BufTrans[4] = nRun;                       // Номеp нитки
      BufTrans[5] = sensType;                   // Типа датчика
                                                // АЦП WinSystem
         fTmp = flt_ankan[nRun-1][sensType];

      if(CONFIG_IDRUN[nRun-1].sensData[sensType].nInterface == tiAN_WS) {
         *(float*)(BufTrans + 6) = fTmp;                   // код АЦП
         *(float*)(BufTrans + 10) = GetCalibrValue(nRun-1, // интерпол.
                     sensType, fTmp);                      // значение
      }
      else
      if (CONFIG_IDRUN[nRun-1].sensData[sensType].nInterface == tiHART) {
         switch(sensType) {
            case 0:
//               MoveToHartSens(1,GLCONFIG.IDRUN[nRun-1].TypeRun,nRun-1);
               fTmp = MValue[nRun-1][0];
               *(float*)(BufTrans + 6) = fTmp; //ph->valt;               // код датчика
               *(float*)(BufTrans + 10) = GetCalibrValue(nRun-1, // интерпол.
                  sensType, fTmp);                           // значение
               break;
            case 1:
                 fTmp = MValue[nRun-1][1];
               fTmp2  =
                 (CONFIG_ADRUN[nRun-1].edp == 0xED) ? fTmp * 10.19716 :   // МПа -> кгс/см2
                 (CONFIG_ADRUN[nRun-1].edp == 0x0C) ? fTmp * 0.01019716 : // кПа -> кгс/см2
                 fTmp;                                     // 0x0A: кгс/см2 -> кгс/см2
               *(float*)(BufTrans + 6) = fTmp2;   // код датчика
               *(float*)(BufTrans + 10) = GetCalibrValue(nRun-1,   // интерпол.
                  sensType, fTmp);                // значение
               break;
            case 2:
               MoveToHartSens(4,CONFIG_IDRUN[nRun-1].TypeRun,nRun-1);
               fTmp = MValue[nRun-1][2];//ph->valdp;
               fTmp2 =
                  (CONFIG_ADRUN[nRun-1].eddph == 0x0C) ? fTmp * 101.9716 : // кПа -> кгс/м2
                  (CONFIG_ADRUN[nRun-1].eddph == 0x09) ? 10.0*fTmp :
                  (CONFIG_ADRUN[nRun-1].eddph == 0x0A) ? 10000.0*fTmp :fTmp;
               *(float*)(BufTrans + 6) = fTmp2;   // код датчика
               *(float*)(BufTrans + 10) = GetCalibrValue(nRun-1,   // интерпол.
                  sensType, fTmp);                // значение
               break;
            case 3:
               MoveToHartSens(8,CONFIG_IDRUN[nRun-1].TypeRun,nRun-1);
               fTmp = MValue[nRun-1][3]; //ph->valdp;
               fTmp2 =
                  (CONFIG_ADRUN[nRun-1].eddpl == 0x0C) ? fTmp * 101.9716 : // кПа -> кгс/м2
                  (CONFIG_ADRUN[nRun-1].eddpl == 0x09) ? 10.0*fTmp :
                  (CONFIG_ADRUN[nRun-1].eddpl == 0x0A) ? 10000.0*fTmp :fTmp;
               *(float*)(BufTrans + 6) = fTmp2;   // код датчика
               *(float*)(BufTrans + 10) = GetCalibrValue(nRun-1,   // интерпол.
                  sensType, fTmp);                // значение
               break;
         }
      }
      else
      if (CONFIG_IDRUN[nRun-1].sensData[sensType].nInterface == tiAN_RS485)
      {  if (rs485num > 0)
        { MoveTo485Sens(sensType,nRun-1);
          *(float*)(BufTrans + 6) = p485d->Value;               // код датчика
          *(float*)(BufTrans + 10) = GetCalibrValue(nRun-1,    // интерпол.
            sensType, p485d->Value);                           // значение
        }
      }
      if ((CONFIG_IDRUN[nRun-1].InterfaceDens == 4)&&(sensType==4))
         {*(float*)(BufTrans + 6) = TAU_flt;   //частота
          *(float*)(BufTrans + 10) = Ros_flt; // интерпол.плотность
         }

   }
}
*/
/////////////////////////////////////////////////////
// Запись точки калибровки
/////////////////////////////////////////////////////
/*
void WrClbr()
{
   int err = 0;
   float fTmp1,fTmp2;

//if (Nwr2 == 0) err |= 0x80;           // флаг запрета взведен

   // Контpоль длины запpоса
   if(BufInKadr[2] != 33)
      err |= 1;

   // Контpоль номеpа нитки
   unsigned char nRun = BufInKadr[4];
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;

   // Пpовеpка паpоля
//   if(memcmp(&BufInKadr[5], GLCONFIG.PASSWORD, 16))
   if (memcmp(&BufInKadr[5],PASSWR2,14))
      err |= 4;

   // Контроль типа датчика
   unsigned char sensType = BufInKadr[21];
   if (sensType > 4)
      err |= 8;

   // Контpоль номеpа точки
   unsigned char nPoint = BufInKadr[22];
   if((nPoint < 1) || (nPoint > 5))
      err |= 16;

   // Ошибка паpаметpов запpоса
   if(err)
      SaveMessagePrefix(6, 0xFF);
   // Запрос в норме
   else {
      if (sensType < 4) { ///////// все, кроме плотномера
                                    // запомнить старые значения
         fTmp1 = GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].code;
         fTmp2 = GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].value;
                                        // установить новые
         GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].code =
            *(float*)(BufInKadr + 23);
         GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].value =
            *(float*)(BufInKadr + 27);

         if(Mem_ConfigWr() == 0) {
            SaveMessagePrefix(6, 0xCC);
            CalibrateIni();
//          SetChanToHartAdr(nRun-1);   // проставить номера каналов в адреса

            if (fTmp1 != *(float*)(BufInKadr + 23) ||  // значения
                fTmp2 != *(float*)(BufInKadr + 27)) {  // изменились
               switch(sensType) {         // запись в архив вмешательств
                  case 0:
                     MEMAUDIT.type = 134; // температура
                     break;
                  case 1:
                     MEMAUDIT.type = 133; // давление
                     break;
                  case 2:
                     MEMAUDIT.type = 136; // перепад верхний
                     break;
                  case 3:
                     MEMAUDIT.type = 137; // перепад нижний
                     break;
               }                          // вместо значений - тип датчика
               MEMAUDIT.altvalue = sensType;
               MEMAUDIT.newvalue = sensType;
	       Mem_AuditWr(Mem_GetAuditPage(nRun-1));//запись отчета вмешательств
            }
         }
         else
           SaveMessagePrefix(6, 0xFF);
      }
      else {   ///////////////////////////// плотномер

         fTmp1 = GLCONFIG.TauTable[nPoint-1];
         fTmp2 = GLCONFIG.RoTable[nPoint-1];
                                        // установить новые
         GLCONFIG.TauTable[nPoint-1] = *(float*)(BufInKadr + 23);
         GLCONFIG.RoTable[nPoint-1]  = *(float*)(BufInKadr + 27);

//       if (nPoint == 1) {
//          fTmp1 = 1.0e+6/GLCONFIG.Tau_CH4c;
//          fTmp1 = pcc->ROnom ;    // плотность
//          fTmp2 = GLCONFIG.Tau_CH4c;
//                                      // установить новые
//          GLCONFIG.Tau_CH4c = *(float*)(BufInKadr + 27);
//       }
//       else {
//          fTmp1 = 1.0e+6/GLCONFIG.Tau_N2c;
//          fTmp1 = pcc->ROnom ;    // плотность
//          fTmp2 = GLCONFIG.Tau_N2c;
//                                      // установить новые
//          GLCONFIG.Tau_N2c = *(float*)(BufInKadr + 27);
//       }
         if (Mem_ConfigWr() == 0) {     // записать в конфигурацию
            SaveMessagePrefix(6, 0xCC);
//          Disp_Density_0();
            Get_Calibr_Density();       // учет результатов калибровки

            if (fTmp2 != *(float*)(BufInKadr + 27)) {  // изменились
               MEMAUDIT.type = 138;                    // плотность
               MEMAUDIT.altvalue = sensType;
               MEMAUDIT.newvalue = sensType;
	       Mem_AuditWr(Mem_GetAuditPage(nRun-1));  // запись отчета вмешательств
            }
         }
         else
           SaveMessagePrefix(6, 0xFF);
      }
   }
}
*/
/////////////////////////////////////////////
// Ответ на запрос чтения таблицы калибровки
/////////////////////////////////////////////
/*
void AnswClbrTable()
{
 AnswClbrTablep(BufInKadr1,BufTrans1);
    CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans

}
void AnswClbrTablep(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   int err = 0;

   if(BufInKadr[2] != 8)                       // Контpоль длины запpоса
      err |= 1;

   unsigned char nRun = BufInKadr[4];          // Контpоль номеpа нитки
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;

   unsigned char sensType = BufInKadr[5];      // Контроль типа датчика
   if (sensType > 4)
      err |= 4;

   if(err)                                     // Ошибка паpаметpов запpоса
      SaveMessagePrefix_2(BufTrans,6, 0xFF);
   else
   {
      if (sensType < 4) {  ///////////////////////// все, кроме плотномера
         SaveMessagePrefix_2(BufTrans,48, 0xC9);             // Запись пpефикса
         BufTrans[4] = nRun;                      // Номеp нитки

         BufTrans[5] = sensType |                  // Тип датчика + интерфейс
                      (CONFIG_IDRUN[nRun-1].sensData[sensType].nInterface << 4);
         for(int i = 0; i < 5; i++) {              // Таблица калибровки
            *(float*)(BufTrans + 6 + 4*i) =
               CONFIG_IDRUN[nRun-1].sensData[sensType].aTable[i].code;
            *(float*)(BufTrans + 26 + 4*i) =
               CONFIG_IDRUN[nRun-1].sensData[sensType].aTable[i].value;
         }
      }
      else {               ///////////////////////// плотномер

         SaveMessagePrefix_2(BufTrans,48, 0xC9);             // Запись пpефикса
         BufTrans[4] = nRun;                      // Номеp нитки
         BufTrans[5] = sensType |                  // Тип датчика + интерфейс
             (CONFIG_IDRUN[nRun-1].InterfaceDens << 4);

         for(int i = 0; i < 5; i++) {             // Таблица калибровки
            *(float*)(BufTrans + 6 + 4*i)  =  GLCONFIG.TauTable[i];
            *(float*)(BufTrans + 26 + 4*i) =  GLCONFIG.RoTable[i];
         }
                                                  // 2 точки
//       *(float*)(BufTrans +  6) = 1.0e+6/GLCONFIG.Tau_CH4c;
//       *(float*)(BufTrans +  6) = pcc->ROnom ;    // плотность
//       *(float*)(BufTrans + 26) = GLCONFIG.Tau_CH4c;
//
//       *(float*)(BufTrans + 10) = 1.0e+6/GLCONFIG.Tau_N2c;
//       *(float*)(BufTrans + 10)  = pcc->ROnom ;    // плотность
//       *(float*)(BufTrans + 30) = GLCONFIG.Tau_N2c;
      }
   }
}

///////////////////////////////////////
// Запись таблицы калибровки
///////////////////////////////////////
void WrClbrTable()
{
 WrClbrTablep(BufInKadr1,BufTrans1,2);
    CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans

}
void WrClbrTablep(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{
   int num, i,err = 0;
   unsigned char nRun, sensType, interfType, cFlag, MaxRun;
   float fTmp1[5],fTmp2[5],Tmp;
   unsigned char flagStart;

//if (Nwr2 == 0) err |= 0x80;           // флаг запрета взведен
   flagStart = 0xFF;
   if(BufInKadr[2] != 64)     // Контpоль длины запpоса
      err |= 1;
   nRun = BufInKadr[4];       // Контpоль номеpа нитки
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;
//   if(memcmp(&BufInKadr[5], GLCONFIG.PASSWORD, 16)) // Пpовеpка паpоля
   if (nPort == 2)
    {  if (memcmp(&BufInKadr[5],PASSWR2,14))
       err |= 4;
    }
    else
     { if (memcmp(&BufInKadr[5],PASSWR1,14))
       err |= 4;
     }
   sensType = BufInKadr[21] & 0xF;    // проверка номера параметра и интерфейса
   interfType = (BufInKadr[21] & 0xF0) >> 4;

   if (sensType > 4)          // Контpоль номера параметра  !!!!!!
      err |= 8;               // в последующих версиях могут увеличиться

   if(interfType > 5)         // Контpоль номера интерфейса !!!!!!
      err |= 16;

   if(err)                    // Ошибка паpаметpов запpоса
      SaveMessagePrefix_2(BufTrans,6, 0xFF);
   else
   {
	 if (sensType < 4) {      // все, кроме плотномера

	 for (int i = 0; i < 5; i++) {     // запомнить старые значения
	    fTmp1[i] = CONFIG_IDRUN[nRun-1].sensData[sensType].aTable[i].code;
	    fTmp2[i] = CONFIG_IDRUN[nRun-1].sensData[sensType].aTable[i].value;
	 }
					 // Тип интерфейса
	 CONFIG_IDRUN[nRun-1].sensData[sensType].nInterface = interfType;
	 for (i = 0; i < 5; i++) {     // Таблица калибровки
	    CONFIG_IDRUN[nRun-1].sensData[sensType].aTable[i].code =
	      *(float*)(BufInKadr + 22 + 4*i);
	    CONFIG_IDRUN[nRun-1].sensData[sensType].aTable[i].value =
	      *(float*)(BufInKadr + 42 + 4*i);
	 }

	 if(Mem_ConfigWr() == 0) {        // успешно записано в ЭНП
	    SaveMessagePrefix_2(BufTrans,6, 0xCA);
	    CalibrateIni();
//          SetChanToHartAdr(nRun-1);     // проставить номера каналов в адреса

	    cFlag = 0;                    // посмотреть изменились ли значения
	    for (i=0; i<5; i++) {
	       if (fTmp1[i] != *(float*)(BufInKadr + 22 + 4*i) ||
		   fTmp2[i] != *(float*)(BufInKadr + 42 + 4*i))
                {
		  cFlag = 1;
                  Tmp = *(float*)(BufInKadr + 42 + 4*i);
                  break;
                }
	    }
//printf("cFlag=%d,i=%d\n",cFlag,i);
	    if (cFlag==1) {                  // значения изменились
	       switch(sensType) {         // запись в архив вмешательств
                  case 0:
                     MEMAUDIT.type = 134; // температура
                     MEMSECUR.mode  =14;
		     break;
		  case 1:
		     MEMAUDIT.type = 133; // давление
                     MEMSECUR.mode  = 13;
		     break;
		  case 2:
		     MEMAUDIT.type = 136; // перепад верхний
                     MEMSECUR.mode  = 16;
		     break;
		  case 3:
		     MEMAUDIT.type = 137; // перепад нижний
                     MEMSECUR.mode  = 17;
		     break;
	       }                          // вместо значений - тип датчика
	       MEMAUDIT.altvalue = fTmp2[i];
	       MEMAUDIT.newvalue = Tmp;
					  // записать
	       Mem_AuditWr(Mem_GetAuditPage(nRun-1));
 //              strncpy(MEMSECUR.login,Tlogin,4);
	       MEMSECUR.TIMEBEG = BINTIME;
	       memset(&MEMSECUR.TIMEEND,0,6);
               MEMSECUR.Port = nRun; // номер т/п
 //              num = Mem_SecurWr_2(Mem_SecurGetPage(),0,2);
	    }
	 }
	 else
	   SaveMessagePrefix_2(BufTrans,6, 0xFF);
      }
      else {    ///////////////////////// // плотномер
	 if (CONFIG_IDRUN[nRun-1].sensData[sensType].nInterface != interfType)
	 {//    flagStart=0; //флаг перестарта вычислителя,
//printf("1-flagStart=%d\n",flagStart);
          for (i=0; i<3; i++)   // распространить новый интерфес плотномера на все нитки
	  { CONFIG_IDRUN[i].InterfaceDens= interfType;  // nRun-1
            CONFIG_IDRUN[i].sensData[4].nInterface =  interfType;
            CONFIG_ADRUN[i].ROdens = CONFIG_IDRUN[i].ROnom;
          }
         }
	 for(i = 0; i < 5; i++) {     // запомнить старые значения
	    fTmp1[i] = GLCONFIG.TauTable[i];
	    fTmp2[i] = GLCONFIG.RoTable[i];
	    GLCONFIG.TauTable[i]  = *(float*)(BufInKadr + 22 + 4*i);
	    GLCONFIG.RoTable[i]   = *(float*)(BufInKadr + 42 + 4*i);
	 }
	 for(i = 0; i < 4; i++) {     // Таблица калибровки
	    m_pCoeff[nRun-1][4][i].codeLow = GLCONFIG.TauTable[i];
	    m_pCoeff[nRun-1][4][i].valueLow = GLCONFIG.RoTable[i];
	    float diffCode = GLCONFIG.TauTable[i+1]
			     - GLCONFIG.TauTable[i];
	    m_pCoeff[nRun-1][4][i].slope = diffCode > 0 ?
	       (GLCONFIG.RoTable[i+1] -
		GLCONFIG.RoTable[i]) / diffCode : 0;
	 }

	 if (Mem_ConfigWr() == 0) {        // успешно записано в ЭНП
	    SaveMessagePrefix_2(BufTrans,6, 0xCA);
            if (interfType == 4) // для частотного входа
//	    Get_Calibr_Density();         // учет результатов калибровки

	    cFlag = 0;                    // посмотреть изменились ли значения
	    for (i=0; i<5; i++) {
	       if (fTmp1[i] != *(float*)(BufInKadr + 22 + 4*i) ||
		   fTmp2[i] != *(float*)(BufInKadr + 42 + 4*i)) {
		  cFlag = 1;
		  break;
	       }
	    }
	    if (cFlag==1) {                  // значения изменились
	       MEMAUDIT.type = 138;          // плотность
               MEMSECUR.mode  = 18;

	       MEMAUDIT.altvalue = fTmp2[i];
	       MEMAUDIT.newvalue = Tmp;
					  // записать  во все нитки
	       MaxRun = (GLCONFIG.NumRun < 4) ? GLCONFIG.NumRun : 3;
	       for (i=0; i<MaxRun; i++)   // разослать аварию по ниткам с плотномером
		  if (CONFIG_IDRUN[i].Dens)
		    { Mem_AuditWr(Mem_GetAuditPage(i));
                      num = i;
                    }
//                   strncpy(MEMSECUR.login,Tlogin,4);
                   MEMSECUR.TIMEBEG = BINTIME;
                   memset(&MEMSECUR.TIMEEND,0,6);
                   MEMSECUR.Port = num;
 //                  num = Mem_SecurWr_2(Mem_SecurGetPage(),0,2);
	    }
	 }
	 else
	    SaveMessagePrefix_2(BufTrans,6, 0xFF);
      }  // конец по плотномеру
   }
//printf("2-flagStart=%d\n",flagStart);
//	 if (flagStart ==0){n_delay(1); StartCPU = 0;} //перестарт
//printf("3-StartCPU=%d\n",StartCPU);
}
*/
//------------------------------
void AnswerHCom() {     // код функции  4F
   unsigned char nRun, err = 0;

   if (BufInKadr1[2] != 22)              // контpоль длины запpоса
      err |= 1;
                                        // пpовеpка паpоля
//   if (memcmp(&BufInKadr1[4],PASSWR2,14))
//      err |= 2;

   if (err)                             // ошибка паpаметpов запpоса
      SaveMessagePrefix(6, 0xFF,BufTrans1);
   else {                               // запрос в норме
      Flag_Hartservice = 1;             // установка режима HART<->COM2
					// посылка подтверждения по СОМ2
      SaveMessagePrefix(6,0xCF,BufTrans1);        // ответ: режим HART<->COM2 установлен
      Flag_Odd = 0;                                 // зафиксировать в архиве
      MEMSECUR.Port = 2;
//      InTimeQueue(100,0,Mem_AlarmHartComOn);
       InTimeQueue(100,0,IniCfg1_HARTOn); 
   }
       CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
       Ini_Send1();  
}

//---------------------------------------------------------------------------

void AnswerHComY() {         // 4E -переход а режим HART, фоpмат пеpедачи   even,8,2,
   unsigned char nRun, err = 0;

   if (BufInKadr1[2] != 22)              // контpоль длины запpоса
      err |= 1;
                                        // пpовеpка паpоля
//   if (memcmp(&BufInKadr[4], GLCONFIG.PASSWORD, 16))
//   if (memcmp(&BufInKadr[4],PASSWR2,14))
//      err |= 2;

   if (err)                             // ошибка паpаметpов запpоса
   {   SaveMessagePrefix(6, 0xFF,BufTrans1);
   }   
   else {                               // запрос в норме
    
                                        // посылка подтверждения по СОМ2
      SaveMessagePrefix(6,0xCE,BufTrans1);        // ответ: режим HART<->COM2 установлен
                                        // зафиксировать в архиве
      MEMSECUR.Port = 2;
//      InTimeQueue(20,0,Mem_AlarmHartComOn);
       Flag_Odd = 1;
       InTimeQueue(100,0,IniCfg1_HARTOn); // т Ёхцшь odd,8,1
//         Flag_Hartservice = 1;   
   }
       CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
       Ini_Send1();  
}

//---------------------------------------------------------------------------
void SearchKadr_Hartservice()  //яЁшхь HART-ъюьрэф поиск пpинятого кадpа в буфеpе пpиема по протоколу HART
{
   extern struct glconfig  GLCONFIG;
   extern unsigned char    hart_buf_out[],hart_buf_out2[];
   unsigned char           *pb,
                           count;         // длина кадpа
   unsigned char           HartKadr[256],
                           ksum=0;        // формат команды
   int                     i,cinindex;      // копия длины буфера
//printf("inindex=%d\n",inindex);
   cinindex = inindex1;
   inindex1 = 0;
   pb = 0;                                // указатель на начало кадpа
   for (i = 0; i < cinindex-5; i++) {
      if (BufResiv1[i]==0xff && BufResiv1[i+1]==0xff
            && (BufResiv1[i+2]==0x82 || BufResiv1[i+2]==0x02 || BufResiv1[i+2]==0xC1)) {
         pb = &BufResiv1[i+2];             // начальный адрес буфера
         if (BufResiv1[i+2]==0x82) {
            count = pb[7] + 8;       // длина длинного кадpа без КС
         // HartFormat = 18;
         }
         else {
            count = pb[3] + 4;       // длина короткого кадpа без КС
         // HartFormat = 14;
         }
         HartFormat = count+9;
         break;                           // кадp выделен
      }
   }

//   if (pb != 0 && (BufResiv + cinindex - pb) >= count) {
   if (pb != 0 && ((BufResiv1 + cinindex - pb) >= count) || (count>=64)) {
      for (i=0; i<count; i++) {
         ksum ^= pb[i];                   // контpольная сумма
      }
//printf("pb=%X %X\n",pb[0],pb[1]);
//      if (ksum == pb[count]) {                // ъюьрэфр "┬√їюф шч Ёхцшьр HART коppектная сумма
{           if ((ResetHartS)||(pb[count-2] == 0x7E)|| (pb[6] == 0xBF)&&(pb[7] == 0xC0) ||
             ((pb[7] == 0xFC)&&(pb[6] == 0x01))||(pb[0] == 0xC1)) {
         // к-да на отмену режима HART<->COM
//printf("buf= %d %d %d %d %d\n",pb[3], pb[4],pb[5], pb[6],pb[7]);
          
            ResetHartS = 0;      //  Ёхцшь HART<->COM юЄьхэшЄ№ эрцрЄшхь ъэюяъш яхЁхъ. ¤ъЁрэют
              HartFormat = 17;             // фышээ√х ъ-ф√
//            if (hartnum > hartnum2)
//            HartIni();                    // возобновление запpоса данных с датчиков
//            if ((hart2 == 0)&&(hartnum2) && (rs485num == 0))
//             HartIni2();
//            InTimeQueue(40,0,Mem_AlarmHartComOff); // зафиксировать в архиве
            SaveMessagePrefix(6,0xFE,BufTrans1);    // ответ: режим HART<->COM отменен
            IniCfg1_HARTOff();   //яхЁхїюф т Ёхцшь noparity
//               Flag_Hartservice = 0;      
            CheckBufTrans(BufTrans1);
            Ini_Send1(); // яхЁхфрўр т Ёхцшьх noparity
            inindex1 = 0;
            InTimeQueue(100,0,Ini_Reciv1);
         }
//         else {
//         if (hartnum > hartnum2)
//           { memmove(hart_buf_out,BufResiv1,cinindex); // копиpование кадpа
//            HartIni_Hartservice();
//           }
//            if ((hart2 == 0)&&(hartnum2) && (rs485num == 0))
//            { memmove(hart_buf_out2,BufResiv1,cinindex);
//              HartIni_Hartservice_2();// запрос по HART2
//            }
//         }
      }
      }
   }
//-----------------------------------------------
/*
// инициализация пеpедачи кадpов сообщения по пpеpываниям пеpедатчика

   void Ini_SendHCom() {

   outindex1 = 0;                             // смещение в пеpедаваемом кадpе
   outportb(comport+4,0x0B);               // DTR + RTS + пpеpывание
   outportb (comport+1,0x02);              // pазpешить пpеpывания пеpедатчику
 //  outport(RIRQ3,(inport(RIRQ3) & 0xFFF7));  //pазpешить IRQ3
}

//--------------------------------------------------------
void RdDiaph() {          // ЧТ параметров диафр.   - нов
 RdDiaphp(BufInKadr1,BufTrans1);
    CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void RdDiaphp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct rddiaphconst  *p;
   struct IdRun      *prun;              // паpаметpы нитки
   unsigned char  num,KodZ;
   unsigned char *pc;
   int           err = 0;
   float RoFU,N2FU,CO2FU,TeplFU,PbFU;
   float Ep,UHeat;

   num = 3 & BufInKadr[4];
   prun = &CONFIG_IDRUN[num-1];
//printf("num=%d  NumCal2=%u GNumCal=%u INTNumCal=%d\n",num,NumCal2,GLCONFIG.NumCal,INTPAR.RS485Com5Speed);
  if ((NumCal2 == INTPAR.RS485Com5Speed)&&(NumCal2 !=GLCONFIG.NumCal))
  {
    if (BufInKadr[2] != 40 ) err |= 1;//контpоль длины запpоса
    if (num > GLCONFIG.NumRun)  err |= 2;//контpоль номеpа нитки

    KodZ = BufInKadr[5];
    if ((KodZ & 0x01) != 0)
    { pc = &BufInKadr[22];
      RoFU = *(float *)pc;
      if (RoFU < 0.55)         // нижний предел одинаковый
             err |= 8;
      if (RoFU > 1.13)           // для всех методов
       err |= 16;
    }

    if(err)
    { SaveMessagePrefix(6,0xFF);  //параметры диафрагмы не записаны
      BufTrans[1] = NumCal2;
    }
  else
    {

     if ((KodZ & 0x02) != 0)
     { pc = &BufInKadr[26];
       CO2FU = *(float *)pc;
     }
     if ((KodZ & 0x04) != 0)
     { pc = &BufInKadr[30];
       N2FU = *(float *)pc;
     }
//     pc = &BufInKadr[81];
//     TeplFU = *(float *)pc;
     if ((KodZ & 0x08) != 0)
     {  pc = &BufInKadr[34];
        PbFU = *(float *)pc;
     }
//printf("RoFU=%f CO2FU=%f N2FU=%f TeplFU=%f\n");
      if ((prun->RoVV  != RoFU) && ((KodZ & 0x01) != 0))
        { MEMAUDIT.type     = 1; //плотность газа при н.у., кг/м^3
          MEMAUDIT.altvalue = prun->RoVV;
          prun->ROnom      = RoFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];
          prun->RoVV      = RoFU;
          MEMAUDIT.newvalue = prun->RoVV;
          CalcZc[num-1] = 1;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//запись отчета вмешательств
        }
      if ((prun->NCO2  != CO2FU) && ((KodZ & 0x02) != 0))
        { MEMAUDIT.type     = 2;//моляpная доля диоксида углеpода в газовой смеси, %
          MEMAUDIT.altvalue = prun->NCO2;
          prun->NCO2   = CO2FU;//моляpная доля диоксида углеpода в газовой смеси, %
          MEMAUDIT.newvalue = prun->NCO2;
         CalcZc[num-1] = 1;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//запись отчета вмешательств
        }
      if ((prun->NN2  != N2FU) && ((KodZ & 0x04) != 0))
        { MEMAUDIT.type     = 3;
          MEMAUDIT.altvalue = prun->NN2;
          prun->NN2    = N2FU; //моляpная доля азота в газовой смеси, %
          MEMAUDIT.newvalue = prun->NN2;
         CalcZc[num-1] = 1;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//запись отчета вмешательств
        }
      if ((prun->Pb  != PbFU) && ((KodZ & 0x08) != 0))
        { MEMAUDIT.type     = 6;  //барометрическое давление, мм pт. ст.
          MEMAUDIT.altvalue = prun->Pb;
          prun->Pb     = PbFU;    //барометрическое давление, мм pт. ст.
          MEMAUDIT.newvalue = prun->Pb;
          Mem_AuditWr(Mem_GetAuditPage(num-1));//запись отчета вмешательств
        }
      StartRe[num-1] = 1e6;             // возврат к начальному стартовому

      if( Mem_ConfigWr() == 0 )         // запись стpуктуpы конфигуpации вычислителя
        SaveMessagePrefix_2(BufTrans,6,0x8A);    // опер.статические паpаметpы записаны
      else
        SaveMessagePrefix_2(BufTrans,6,0xFF);    // статические паpаметpы не записаны
      BufTrans[1] = NumCal2;
    }
   }
   else
   {
   if (BufInKadr[2] != 7)
      err |= 1;                          // контpоль длины запpоса
   if (num > GLCONFIG.NumRun)
      err |= 2;                         // контpоль номеpа нитки

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // ошибка паpаметpов запpоса
   else {
      p    = &SEND.RDDIAPHCONST;        // константы
      p->Rn        = prun->Rn;          // радиус закругления
      p->TauP      = prun->TauP;        // период проверки
      p->GOST_ISO  = prun->GOST_ISO;
      p->Reserve_2 = 0.0;
      p->Mon       = BINTIME.month;
      p->Day       = BINTIME.date;
      p->Yea       = BINTIME.year;
      p->Hou       = BINTIME.hours;
      p->Min       = BINTIME.minutes;
      p->Sec       = BINTIME.seconds;

//      SaveMessagePrefix_2(BufTrans,sizeof(rddiaphconst)+7,0x8A); // запись пpефикса
      BufTrans[4] = num;                              // номеp нитки
//      memmove(&SEND,&BufTrans[5],sizeof(rddiaphconst));// запись буфеpа
    }
   }
}
//--------------------------------------------------------
void WrDiaph() {          // ЗП параметров диафр.   - нов
 WrDiaphp(BufInKadr1,BufTrans1,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrDiaphp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{
   struct wrdiaphconst  *p;
   struct IdRun      *prun;              // паpаметpы нитки
   unsigned char     num;
   unsigned char     *pchar;
   int           err = 0;

//if (Nwr2 == 0) err |= 0x80;           // флаг запрета взведен

//   if (BufInKadr[2]-23 != sizeof(wrdiaphconst))
//      err |= 1;                          // контpоль длины запpоса
//fclose(fp);
   num = 3 & BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                          // контpоль номеpа нитки
                                         // пpовеpка паpоля
   if (nPort==2)
    { if (memcmp(&BufInKadr[5],PASSWR2,14))
      err |= 4;
    }
   else
    { if (memcmp(&BufInKadr[5],PASSWR1,14))
      err |= 4;
    }
//printf("err=%02X p2=%s Buf=%s\n",err,PASSWR2,&BufInKadr[5]);
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);      // парам. диафрагмы не записаны
   else
   if (Mem_ConfigWr() == 0)  {        // запись стpуктуpы конфигуpации вычислителя
      p   = &RESIV.WRDIAPHCONST;      // парам. диафрагмы
      prun = &CONFIG_IDRUN[num-1];  // указатель на паpаметpы нитки
      memmove(&BufInKadr[4+17],&RESIV,sizeof(struct wrdiaphconst));//пеpезапись паpаметpов
      if (prun->GOST_ISO != p->GOST_ISO) {
         MEMAUDIT.type     = 51;
         pchar = (unsigned char*)&MEMAUDIT.altvalue;
         if (prun->GOST_ISO == 0)
          memcpy(pchar,"GOST",4);
         else
          memcpy(pchar,"ISO ",4);
//         MEMAUDIT.altvalue = prun->Rn;
         prun->GOST_ISO   = p->GOST_ISO;                  // радиус закругления
         pchar = (unsigned char*)&MEMAUDIT.newvalue;
         if (p->GOST_ISO == 0)
          memcpy(pchar,"GOST",4);
         else
          memcpy(pchar,"ISO ",4);
	 Mem_AuditWr(Mem_GetAuditPage(num-1));// запись отчета вмешательств
      }
//printf("GOST_ISO=%d\n",prun->GOST_ISO);
       if (p->GOST_ISO)  // ISO
       { p->Rn = 0.0; p->TauP = 0.0; }
      if (prun->Rn != p->Rn) {
         MEMAUDIT.type     = 13;
         MEMAUDIT.altvalue = prun->Rn;
         prun->Rn   = p->Rn;                  // радиус закругления
         MEMAUDIT.newvalue = prun->Rn;
	 Mem_AuditWr(Mem_GetAuditPage(num-1));// запись отчета вмешательств
      }
      if (prun->TauP != p->TauP) {
         MEMAUDIT.type     = 14;
         MEMAUDIT.altvalue = prun->TauP;
         prun->TauP    = p->TauP;             // период проверки
         MEMAUDIT.newvalue = prun->TauP;
	 Mem_AuditWr(Mem_GetAuditPage(num-1));// запись отчета вмешательств
      }

      Mem_ConfigWr();
      SaveMessagePrefix_2(BufTrans,6,0x8B);  //параметры диафрагмы записаны
   }
   else
      SaveMessagePrefix_2(BufTrans,6,0xFF);  //константы не записаны
}
*/
//--------------------------------------------------
/*
void RdNoAlarmConstFlag() {             // чтение флага запрета аварий
   struct rdnoalarmconstflag *p;
   struct IdRun *prun;                  // паpаметpы нитки
   unsigned char num;
   int err = 0;

   if (BufInKadr[2] != 7)
      err |= 1;                          // контpоль длины запpоса

   num = 3 & BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // контpоль номеpа нитки

   if (err)
      SaveMessagePrefix(6,0xFF);        // ошибка паpаметpов запpоса
   else {
      p    = &SEND.RDNOALARMCONSTFLAG;  // флаг запрета аварий на константах
      prun = &GLCONFIG.IDRUN[num-1];    // указатель на паpаметpы нитки

      p->Flag = prun->NoAlarmConstFlag; // флаг запрета аварий на константах
                                        // запись пpефикса
      SaveMessagePrefix(sizeof(rdnoalarmconstflag)+7,0xD0);
      BufTrans1[4] = num;                // номеp нитки
                                        // запись буфеpа
      memmove(&SEND,&BufTrans1[5],sizeof(rdnoalarmconstflag));
   }
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans

}
//--------------------------------------------------
void WrNoAlarmConstFlag() {             // запись флага запрета аварий
   struct wrnoalarmconstflag *p;
   struct IdRun *prun;                  // паpаметpы нитки
   unsigned char num;
   int err = 0;

//if (Nwr1 == 1) err |= 0x80;           // флаг запрета взведен

   if (BufInKadr[2]-23 != sizeof(wrnoalarmconstflag))
      err |= 1;                         // контpоль длины запpоса

   num = 3 & BufInKadr[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // контpоль номеpа нитки
                                        // пpовеpка паpоля
//   if (memcmp(&BufInKadr[5],GLCONFIG.PASSWORD,16))
   if (memcmp(&BufInKadr[5],PASSWR2,14))
      err |= 4;

   if (err)
      SaveMessagePrefix(6,0xFF);        // константы не записаны
   else
  {    p   = &RESIV.WRNOALARMCONSTFLAG;  // константы
      prun = &GLCONFIG.IDRUN[num-1];    // указатель на паpаметpы нитки
                                        // пеpезапись паpаметpов
      memmove(&BufInKadr[4+17],&RESIV,sizeof(wrnoalarmconstflag));
   if (Mem_ConfigWr() == 0)            // запись стpуктуpы конфигуpации вычислителя
      SaveMessagePrefix(6,0xD1);        // флаг записан
   else
      SaveMessagePrefix(6,0xFF);        // флаг не записан
  }
}
*/
//--------------------------------------------------------

void RdIntPar2() {                     // чтение внутренних параметров 2
 RdIntPar2p(BufInKadr1,BufTrans1);                      // необходимость начальной конфигуpации
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void RdIntPar2p(unsigned char* BufInKadr,unsigned char*  BufTrans)                       // необходимость начальной конфигуpации
{   struct intpar2 INTPAR2;
   int err = 0;

   if (BufInKadr[2] != 6)
      err |= 1;                         // контpоль длины запpоса

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);      // ошибка паpаметpов запpоса
   else {
   INTPAR2.HartNumCyclRep = INTPAR.HartNumCyclRep;
   INTPAR2.HartNumImmedRep = INTPAR.HartNumImmedRep;
   INTPAR2.HartReqDelay  = INTPAR.HartReqDelay;
   INTPAR2.Com1NoWrite  = INTPAR.Com1NoWrite;  // резерв  0/1
   INTPAR2.CntBufLen  = INTPAR.CntBufLen;  // длина ответа ф.6 (0 - 23, 1-27 байт)
   INTPAR2.CntAddBufShift = INTPAR.CntAddBufShift; // параметр пароля администратора (0,1,...)
   INTPAR2.VolumeDelta = INTPAR.VolumeDelta; // резерв с п.з. 4 байта
   INTPAR2.AlarmVolumeMinTime = INTPAR.AlarmVolumeMinTime; // миним. время для аварийного объема

					// запись пpефикса
      SaveMessagePrefix_2(BufTrans,sizeof(struct intpar2)+6,0xE2);
					// запись буфеpа
      memmove(&BufTrans[4],&INTPAR2,sizeof(struct intpar2));
   }
}
//--------------------------------------------------------
void RdAddConf() {                     // чтение дополнит. параметров конф.
 RdAddConfp(BufInKadr1,BufTrans1);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();
}
void RdAddConfp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct addpar ADDPAR;
   int err = 0;

   if (BufInKadr[2] != 6)
      err |= 1;                         // контpоль длины запpоса
//fclose(fp);

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);      // ошибка паpаметpов запpоса
   else {
   ADDPAR.RS485Com5Speed = INTPAR.RS485Com5Speed-1;// скорость по COM5 - RS485
   ADDPAR.Com4Speed = INTPAR.Com4Speed; // скорость по СOM3
   ADDPAR.Com2Mode = INTPAR.Com2Mode; // плотность в отчете, 0-нет
   ADDPAR.OdoMask = INTPAR.OdoMask; // маска одоризации
   ADDPAR.ChannelJoin = INTPAR.ChannelJoin;  // Рарешение аварийной сигнализации
   ADDPAR.QminFlag = INTPAR.QminFlag; // признак перехода на минимальный расход
   ADDPAR.FullNormVolume = INTPAR.FullNormVolume; // в архив весь объем или безаварийный
   ADDPAR.Keyboard = INTPAR.Keyboard; // (наличие клавиатуры) ASCII=0, RTU=1
   ADDPAR.HeatReport = INTPAR.HeatReport; // УдТСв в отчете
					// запись пpефикса
      SaveMessagePrefix_2(BufTrans,sizeof(struct addpar)+6,0xE4);
					// запись буфеpа
      memmove(&BufTrans[4],&ADDPAR,sizeof(struct addpar));
   }
}
//--------------------------------------------------------
void WrIntPar() {    // запись внутоенних параметров
 WrIntParp(BufInKadr1,BufTrans1,2);                      // необходимость начальной конфигуpации
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrIntParp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)                       // необходимость начальной конфигуpации
{   int err = 0;
   unsigned int k;
   int sp;
    char  Setspeed4, Setspeed5;

//if (Nwr2 == 0) err |= 0x80;           // флаг запрета взведен
   if ((BufInKadr[2]-22) != (sizeof(struct intpar)-2))
      err |= 1;                         // контpоль длины запpоса

					// пpовеpка паpоля
//   if (nPort == 2)
//   { if (memcmp(&BufInKadr[4],PASSWR2,14))
//      err |= 4;
//   }
//   else
//   { if (memcmp(&BufInKadr[4],PASSWR1,14))
//      err |= 4;
//   }
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // вн. параметры не записаны
   else {
      memmove(&RESIV,&BufInKadr[20],sizeof(struct intpar));
      Setspeed4 = (INTPAR.Com4Speed != RESIV.WRINTPAR.Com4Speed);
//      Setspeed5 = (INTPAR.RS485Com5Speed != RESIV.WRINTPAR.RS485Com5Speed);
      memmove(&INTPAR,&BufInKadr[20],sizeof(struct intpar));
//      if (Setspeed4) SetSpeedCom4();

//      Mem_IntParWr();

//      SetHartOut();                     // переустановка таймаута

//      ChkQminFlag();                    // переустановка флажков для Qmin

      SaveMessagePrefix_2(BufTrans,6,0xD3);        // параметры записаны
   }
}
//--------------------------------------------------------
void WrAddConf() {       // запись дополнительных параметров конфиг
 WrAddConfp(BufInKadr1,BufTrans1,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrAddConfp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort) {
   int err = 0;
   struct addpar ADDPAR;
   unsigned int i,k,sp;
   char  Setspeed4, Setspeed5;
   unsigned char flagStart;

//  if (Nwr2 == 0) err |= 0x80;           // флаг запрета взведен
    flagStart = 0x0F;
   if (BufInKadr[2]-22 != sizeof(struct addpar))
      err |= 1;                         // контpоль длины запpоса
					// пpовеpка паpоля
//   if (nPort == 2)
//   { if (memcmp(&BufInKadr[4],PASSWR2,14))
//      err |= 4;
//   }
//   else
//   { if (memcmp(&BufInKadr[4],PASSWR1,14))
//      err |= 4;
//   }

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);      // константы не записаны
   else {
      memmove(&ADDPAR,&BufInKadr[20],sizeof(struct addpar));
      ADDPAR.RS485Com5Speed++;
//printf("c5=%u\n",ADDPAR.RS485Com5Speed);
      Setspeed4 = (INTPAR.Com4Speed != ADDPAR.Com4Speed);
      Setspeed5 = (INTPAR.RS485Com5Speed != ADDPAR.RS485Com5Speed);
      if (Setspeed4)
      {  MEMAUDIT.type = 26;
         MEMAUDIT.altvalue = INTPAR.Com4Speed;
         MEMAUDIT.newvalue = ADDPAR.Com4Speed;
         INTPAR.Com4Speed = ADDPAR.Com4Speed; // скорость по СOM4
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(i,MEMAUDIT);
       }
      if (Setspeed5)
       { MEMAUDIT.type = 25;
	 MEMAUDIT.altvalue = INTPAR.RS485Com5Speed;
	 MEMAUDIT.newvalue = ADDPAR.RS485Com5Speed;
	 INTPAR.RS485Com5Speed = ADDPAR.RS485Com5Speed; // скорость по СOM5
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	      Mem_AuditWr(i,MEMAUDIT);
       }

      if (INTPAR.Com2Mode != ADDPAR.Com2Mode)
       { MEMAUDIT.type = 55;
	 MEMAUDIT.altvalue = INTPAR.Com2Mode;
         MEMAUDIT.newvalue = ADDPAR.Com2Mode;
         INTPAR.Com2Mode = ADDPAR.Com2Mode; // плотность в отчете, 0-нет
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	     Mem_AuditWr(i,MEMAUDIT);
       }

      if (INTPAR.OdoMask != ADDPAR.OdoMask)
       { MEMAUDIT.type = 56;
         MEMAUDIT.altvalue = INTPAR.OdoMask;
         MEMAUDIT.newvalue = ADDPAR.OdoMask;
         INTPAR.OdoMask = ADDPAR.OdoMask; // маска одоризации
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	      Mem_AuditWr(i,MEMAUDIT);
       }
      if (INTPAR.ChannelJoin != ADDPAR.ChannelJoin)
       { MEMAUDIT.type = 57;
         MEMAUDIT.altvalue = INTPAR.ChannelJoin;
         MEMAUDIT.newvalue = ADDPAR.ChannelJoin;
	 INTPAR.ChannelJoin = ADDPAR.ChannelJoin;  // Рарешение аварийной сигнализации
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	       Mem_AuditWr(i,MEMAUDIT);
       }
      if (INTPAR.QminFlag != ADDPAR.QminFlag)
       { MEMAUDIT.type = 58;
         MEMAUDIT.altvalue = INTPAR.QminFlag;
         MEMAUDIT.newvalue = ADDPAR.QminFlag;
         INTPAR.QminFlag = ADDPAR.QminFlag; // признак перехода на минимальный расход
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	       Mem_AuditWr(i,MEMAUDIT);
	 flagStart=0; //флаг перестарта вычислителя,
       }
      if (INTPAR.FullNormVolume != ADDPAR.FullNormVolume)
       { MEMAUDIT.type = 59;
         MEMAUDIT.altvalue = INTPAR.FullNormVolume;
         MEMAUDIT.newvalue = ADDPAR.FullNormVolume;
         INTPAR.FullNormVolume = ADDPAR.FullNormVolume; // в архив весь объем или безаварийный
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	      Mem_AuditWr(i,MEMAUDIT);
       }
      if (INTPAR.Keyboard != ADDPAR.Keyboard)
       { MEMAUDIT.type = 60;
         MEMAUDIT.altvalue = INTPAR.Keyboard;
         MEMAUDIT.newvalue = ADDPAR.Keyboard;
	 INTPAR.Keyboard = ADDPAR.Keyboard; // протокол  ASCII=0, RTU=1
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	      Mem_AuditWr(i,MEMAUDIT);
       }
      if (INTPAR.HeatReport != ADDPAR.HeatReport)
       { MEMAUDIT.type = 60;
         MEMAUDIT.altvalue = INTPAR.HeatReport;
         MEMAUDIT.newvalue = ADDPAR.HeatReport;
         INTPAR.HeatReport = ADDPAR.HeatReport; //  ASCII=0 или RTU=1
	       for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
		      Mem_AuditWr(i,MEMAUDIT);
       }
//       if (Setspeed4) SetSpeedCom4();
//     Mem_IntParWr();
    //  ChkQminFlag();                    // переустановка флажков для Qmin
//     	 n_delay(1);
	 if (flagStart == 0) StartCPU = 0; //перестарт

      SaveMessagePrefix_2(BufTrans,6,0xE5);      // параметры записаны
   }
}
//--------------------------------------------------------
void WrIntPar2() {                     // запись внутренних параметров
 WrIntPar2p(BufInKadr1,BufTrans1,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrIntPar2p(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{
   int err = 0;
   struct intpar2 INTPAR2;
   unsigned int k;
   unsigned int KsROM;

   if (BufInKadr[2]-22 != sizeof(struct intpar2))
      err |= 1;                         // контpоль длины запpоса
					// пpовеpка паpоля
//   if (nPort == 2)
//   {
//      if (memcmp(&BufInKadr[4],PASSWR2,14))
//      err |= 4;
//   }
//   else
//   {   if (memcmp(&BufInKadr[4],PASSWR1,14))
//       err |= 4;
//   }
//printf("err=%02X psw1=%s psw2=%s\n",err,PASSWR1,PASSWR2);
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);      // константы не записаны
   else {
      memmove(&INTPAR2,&BufInKadr[20],sizeof(struct intpar2));
      if (INTPAR2.CntBufLen == 2)
      {//          EraseSD();
     // Стирание SD-карты
//        strncpy(MEMSECUR.login, Tlogin,4);
        MEMSECUR.TIMEBEG = BINTIME;
        memset(&MEMSECUR.TIMEEND,0,6);
        MEMSECUR.Port = 2;
        MEMSECUR.mode  = 28;
//        k = Mem_SecurWr_2(Mem_SecurGetPage(),0,2);
      }
      else if (INTPAR2.CntBufLen == 3)
        for (k=0; k<3; k++)
        {
//           outportb(0x1D0,BASE_CFG_PAGE);// и взять из ЭНП
//           poke(SEGMENT,INDMGN+k*2,1); //номер элемента массива поиска
           kSDm[k] = 1; //индекс массива поиска
//           poke(SEGMENT,INDPER+k*2,1); //номер элемента массива поиска
           kSDp[k] = 1; //индекс массива поиска
//printf("km[%d]=%d kp[%d]=%d\n",kSDm[k],kSDp[k]);
        }
        else
         INTPAR.CntBufLen  = INTPAR2.CntBufLen;  // длина ответа ф.6 (0 - 23, 1-27 байт)

      INTPAR.HartNumCyclRep = INTPAR2.HartNumCyclRep;
      INTPAR.HartNumImmedRep = INTPAR2.HartNumImmedRep;
      INTPAR.HartReqDelay  = INTPAR2.HartReqDelay;
      INTPAR.Com1NoWrite  = INTPAR2.Com1NoWrite;  //Клавиатура 1-есть 0-нет
      INTPAR.CntAddBufShift = INTPAR2.CntAddBufShift; // параметр пароля администратора (0,1,...)
      if (INTPAR2.VolumeDelta  > 20.0)
      {
        KsROM = (int)INTPAR2.VolumeDelta;
//        outportb(0x1D0,RESERV_CFG_PAGE);
//        poke(SEGMENT,0x3FC6,KsROM); //Чтение КС
      }
      else
      if (INTPAR.VolumeDelta != INTPAR2.VolumeDelta)
       { INTPAR.VolumeDelta = INTPAR2.VolumeDelta; // коррекция хода часов
 //        InQueue(1,conf_ds1340);  // ъюЁЁхЄшЁютър тЁхёьхэш
       }
      INTPAR.AlarmVolumeMinTime = INTPAR2.AlarmVolumeMinTime; // миним. время для аварийного объема

//      Mem_IntParWr();
//      SetHartOut();
      SaveMessagePrefix_2(BufTrans,6,0xE3);      // параметры записаны
      if (INTPAR2.CntBufLen == 2)
      {// n_delay(1);
	StartCPU = 0; //перестарт
      }
   }
}

//--------------------------------------------------------
void RdNames() {                        // чтение наименований
 RdNamesp(BufInKadr1,BufTrans1);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans    
}
void RdNamesp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct rdnames *p,RDNAMES;
   int err = 0;

   if (BufInKadr[2] != 6)
      err |= 1;                          // контpоль длины запpоса

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // ошибка паpаметpов запpоса
   else {
      p    = &RDNAMES;             // наименования

                                        // организация
      memcpy(p->NamePlant,GLCONFIG.NamePlant,16);
                                        // объект
      memcpy(p->NameCal,  GLCONFIG.NameCal,  16);
                                        // заводской номер
      memcpy(p->Serial,  GLCONFIG.Serial,  8);

      memcpy(p->Telephone,GLCONFIG.Telephone,13);// телефон
      memcpy(p->IP_Adress,  GLCONFIG.IP_Adress,  13); //IP адрес
      memcpy(p->IP_Port,  GLCONFIG.IP_Port,  4); // Порт

                                        // запись пpефикса
      if (BufInKadr[3] == 0x54)
       SaveMessagePrefix_2(BufTrans,sizeof(struct rdnames)+6,0xD4);
      else
       SaveMessagePrefix_2(BufTrans,sizeof(struct rdnames)+6,0xA4);
                                        // запись в буфеp передачи
      memmove(&BufTrans[4],&RDNAMES,sizeof(struct rdnames));
   }
}
//--------------------------------------------------------
void WrNames() {                        // запись наименований
 WrNamesp(BufInKadr1,BufTrans1,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrNamesp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)                       // необходимость начальной конфигуpации
{
   struct wrnames *p;
   int i,KS=52;
   char c,e;
   unsigned long L;
   unsigned char Ser[6];
   unsigned char psSer[11];
   char *LgPars;
   int err = 0;

   if (BufInKadr[2]-22 != sizeof(struct wrnames))
      err |= 1;                         // контpоль длины запpоса

                                        // пpовеpка паpоля
//   if (nPort == 2)
//   {
//      if (memcmp(&BufInKadr[4],PASSWR2,14))
//      err |= 4;
//   }
//   else
//   {   if (memcmp(&BufInKadr[4],PASSWR1,14))
//       err |= 4;
//   }
//char str[40];
//sprintf(str,"err=%02X PAS1=%s\n\r",err,PASSWR1);
//ComPrint_2(str);
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // константы не записаны
   else {
      p = &RESIV.WRNAMES;               // наименования
                                        // пеpезапись паpаметpов
      memmove(&RESIV,&BufInKadr[20],sizeof(struct wrnames));

                                        // организация
      memcpy(GLCONFIG.NamePlant,p->NamePlant,16);
                                        // объект
      memcpy(GLCONFIG.NameCal,p->NameCal,16);
                                        // заводской номер
//      if (GLCONFIG.Serial!=p->Serial)
      if (memcmp(GLCONFIG.Serial,p->Serial,8)) //!=
       { MEMAUDIT.type = 47;
          memmove(&MEMAUDIT.altvalue,(GLCONFIG.Serial+3),4);
          memmove(GLCONFIG.Serial,p->Serial,8);   //имя нитки
          memmove(&MEMAUDIT.newvalue,(p->Serial+3),4);
	  for (i=0; i<GLCONFIG.NumRun; i++)   // разослать вмешат. по ниткам
	      Mem_AuditWr(i,MEMAUDIT);
       }
       {
        memcpy(Ser,GLCONFIG.Serial+1,6);
//        L= atol(Ser);
        if (L==0)
          memcpy(psSer,"          ",10);
        else
        for (i=0; i<10; i++)
        { c =(L % KS);
          e=c+65;
          if (e > 90) e+=6;
          psSer[i] = e;
          L = L + c + i+1;
          if (i==0) L=L+INTPAR.CntAddBufShift; // доп.параметр длина буфера счетчика
        }
//        psSer[11] = '\0';
       }
//      outportb(0x1d0,RESERV_CFG_PAGE);       // установить стpаницы памяти
//      LgPars = (char far *)MK_FP(SEGMENT,4100);
//       _fmemcpy(LgPars,(char far *)psSer,10);

//        memcpy(LogPassw[0].Password,psSer,10);
//      Mem_LoginWr();
      memcpy(GLCONFIG.Telephone,p->Telephone,13);// телефон
      memcpy(GLCONFIG.IP_Adress, p->IP_Adress,  13); //IP адрес
      memcpy(GLCONFIG.IP_Port, p->IP_Port,  4); // Порт


      if (Mem_ConfigWr() == 0)  {       // запись стpуктуpы конфигуpации вычислителя
         SaveMessagePrefix_2(BufTrans,6,0xD5);     // наименования  записаны
      }
      else
          SaveMessagePrefix_2(BufTrans,6,0xFF);    // наименования не записаны
   }
}
//--------------------------------------------------------
void RdVolume() {                        // чтение объемов общих
  RdVolumep(BufInKadr1,BufTrans1);    
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void RdVolumep(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct rdvolumes *p,RDVOLUM;
   int err = 0;
   char num;

   if (BufInKadr[2] != 7)
      err |= 1;                          // контpоль длины запpоса

   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // ошибка паpаметpов запpоса
   else {
      num = 3 & BufInKadr[4];
      p    = &RDVOLUM;             // объемы
					// полный
      p->VruTotal = CONFIG_ADRUN[num-1].Vtot_l;
      p->VruShtat = CONFIG_ADRUN[num-1].Vtrush;
      p->VruAvar  = CONFIG_ADRUN[num-1].Vtrual;                                  // запись пpефикса
      p->VsuTotal = Persist_GetQTotal(num-1);
      p->VsuShtat = 0.0;
      p->VsuAvar  = 0.0;
//      SaveMessagePrefix_2(BufTrans,sizeof(rdvolumes)+7,0xE6);
      BufTrans[4] = num;                 // номеp нитки
                                        // запись в буфеp передачи
//      memmove(&BufTrans[5],&RDVOLUM,sizeof(rdvolumes));
   }
}

//--------------------------------------------------------
void SetRespStat() {

   SEND.RESPONSEVALARM.ResponseStatus = 1;
}
//-------------------------------------------------
void RdNameSD(){     // чтение наименований из SD=карты
   struct rdnameSD *p;
   unsigned char run;

   int err = 0;

   if (BufInKadr1[2] != 7)
      err |= 1;                          // контpоль длины запpоса

   if (err)
      SaveMessagePrefix(6,0xFF,BufTrans1);        // ошибка паpаметpов запpоса
   else {
      run = BufInKadr1[4]-1;
      p    = &SEND.RDNAMESD;             // наименования
                                        // объект
      memcpy(p->NameCal, GLCONFIG.NameCal, 16);
                                        // заводской номер
      memcpy(p->Serial, GLCONFIG.Serial, 8);
                                         // имя нитки
      memcpy(p->NameRun, CONFIG_IDRUN[run].NameRun, 16);

                                        // запись пpефикса
//      SaveMessagePrefix(sizeof(rdnameSD)+7,0xA4);
                                        // запись в буфеp передачи
      BufTrans1[4] = run+1;
//      memmove(&BufTrans[5],&SEND,sizeof(rdnameSD));
   }
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
//--------------------------------------------------------
void SDsend()
{
   int i,err = 0;
  if (bSDOn)           // ответ, только если запрос от COM2
   {
    if (BufInKadr1[2] != 6)
      err |= 1;                          // контpоль длины запpоса
 //     memcpy(&BufTrans[4],&InitSD_Res,10);
// printf("3-c=%d s=%d m=%d\n",counttime, BINTIME.seconds,BINTIME.minutes);
    if (err)
      SaveMessagePrefix(6,0xFF,BufTrans1);        // ошибка паpаметpов запpоса
    else
      SaveMessagePrefix(16,0xA5,BufTrans1);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
   }
   else   bSDOn = 0;

    if (SysTakt == 1)
    for (i=0; i<GLCONFIG.NumRun; i++)
     { nRunG  = i;
     TaskWriteArcData();      // чряшё№ т рЁїшт ш шэфшърЎш 
     }
    else
     if (BINTIME.seconds % 2 == 0)
      for (i=0; i<2; i++)
      { nRunG  = i;
       TaskWriteArcData();      // чряшё№ т рЁїшт ш шэфшърЎш 
      }
     else
      { nRunG  = 2;
       TaskWriteArcData();      // чряшё№ т рЁїшт ш шэфшърЎш 
      }
}
//-------------------------------------f-------------------
void InitSD(){     // инициализация  SD-карты
    bSDOff = 1;
// printf("InitSD=%d s=%d\n",counttime, BINTIME.seconds);
}
//----------------------------------------------------------
void RdEdizm()
{
  RdEdizmp(BufInKadr1,BufTrans1);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void RdEdizmp(unsigned char* BufInKadr,unsigned char*  BufTrans)
{
   struct Edizm      *ped;              // паpаметpы нитки
  int           err = 0;
   if (BufInKadr[2] != 6)
      err |= 1;                          // контpоль длины запpоса
  if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);        // ошибка паpаметpов запpоса
   else {
//      SaveMessagePrefix_2(BufTrans,sizeof(Edizm)+4,0xE7); // запись пpефикса
      memmove(&BufTrans[4],&GLCONFIG.EDIZM_T,6);// запись буфеpа
      BufTrans[10] =  GLCONFIG.EDIZM_T.T_SU | (GLCONFIG.EDIZM_T.T_VVP << 2)
       | (GLCONFIG.EDIZM_T.T_AP << 4);
//printf("TSU=%02X\n" ,BufTrans[10]);
      memmove(&BufTrans[11],&GLCONFIG.EDIZM_T.EdIzP,3);
   }
}
//-----------------------------------------------------------
void WrEdizm()
{
  WrEdizmp(BufInKadr1,BufTrans1,2);
   CheckBufTrans(BufTrans1); //яЁютхЁър ╩╤
   Ini_Send1();  // яхЁхфрўр BufTrans
}
void WrEdizmp(unsigned char* BufInKadr,unsigned char*  BufTrans, char nPort)
{
   int          k, err = 0;
   char T_SUn,T_SUP, EdEn;

//   if (BufInKadr[2]-22 != sizeof(Edizm)-2)
      err |= 1;                          // контpоль длины запpоса
                                         // пpовеpка паpоля
//   if (nPort==2)
//    {  if (memcmp(&BufInKadr[4],PASSWR2,14))
//      err |= 4;
//    }
//   else
//    {  if (memcmp(&BufInKadr[4],PASSWR1,14))
//      err |= 4;
//    }
//printf("buf26=%02X SU=%d buf27=%02X P=%d buf28=%02X dP=%d err=%02X\n",
   if (err)
      SaveMessagePrefix_2(BufTrans,6,0xFF);      // не записаны
   else
            // запись стpуктуpы конфигуpации вычислителя
   {
     T_SUn  = (BufInKadr[26] & 0x03);
     T_SUP =  (BufInKadr[26]>>2) & 0x03;
     EdEn = BufInKadr[29];
     if (T_SUP != GLCONFIG.EDIZM_T.T_VVP)
     {
         MEMAUDIT.type     = 67;
         MEMAUDIT.altvalue = GLCONFIG.EDIZM_T.T_VVP;
         MEMAUDIT.newvalue = T_SUP;
        for (k=0; k<3; k++)
        {  Mem_AuditWr(k,MEMAUDIT);// запись отчета вмешательств
           CalcZc[k]=1;
        }
     }
     if (T_SUn != GLCONFIG.EDIZM_T.T_SU)
     {
         MEMAUDIT.type     = 62;
         MEMAUDIT.altvalue = GLCONFIG.EDIZM_T.T_SU;
         MEMAUDIT.newvalue = T_SUn;
      for (k=0; k<3; k++)
      {    Mem_AuditWr(k,MEMAUDIT);// запись отчета вмешательств
        CONFIG_IDRUN[k].ROnom *= KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // перевод к  T_SUn гр.Ц
        CONFIG_IDRUN[k].Heat *=  KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // перевод к  T_SUn гр.Ц
//printf("Rnom=%f Heat=%f SU=%d SUn=%d HsVV=%f\n", CONFIG_IDRUN[k].ROnom, GLCONFIG.IDRUN[k].Heat,GLCONFIG.EDIZM_T.T_SU,T_SUn,GLCONFIG.IDRUN[k].HsVV);
        CONFIG_IDRUN[k].R0min *= KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // перевод к  T_SUn гр.Ц
        CONFIG_IDRUN[k].R0max *= KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // перевод к  T_SUn гр.Ц
            CalcZc[k]=1;
       if (CONFIG_IDRUN[k].nKcompress == 3)     CalcBR[k] = 1;
       else  CalcBR[k] = 0;
      }
     }
       for (k=0; k<3; k++)
       {
            if (GLCONFIG.EDIZM_T.EdIzmE != EdEn)
            { MEMAUDIT.altvalue = CONFIG_IDRUN[k].HsVV;
//              if (EdEn == 1)
//                CONFIG_IDRUN[k].HsVV = CONFIG_IDRUN[k].Heat
//                     * KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
//                     * KoRo[T_SUn][GLCONFIG.EDIZM_T.T_VVP];
//              else
              CONFIG_IDRUN[k].HsVV = CONFIG_IDRUN[k].Heat*KoefEn[EdEn]
                     * KoRo[T_SUn][GLCONFIG.EDIZM_T.T_VVP];
              MEMAUDIT.type     = 37;
              MEMAUDIT.newvalue = CONFIG_IDRUN[k].HsVV;
               Mem_AuditWr(k,MEMAUDIT);
            }
          if (T_SUP != GLCONFIG.EDIZM_T.T_VVP)
          {
            MEMAUDIT.altvalue = CONFIG_IDRUN[k].RoVV;
            if (CONFIG_IDRUN[k].typeDens == 0)  // абс плотность
              CONFIG_IDRUN[k].RoVV *= KoRo[GLCONFIG.EDIZM_T.T_VVP][T_SUP]; // перевод к T 0 гр.Ц
            MEMAUDIT.type     = 1;
            MEMAUDIT.newvalue = CONFIG_IDRUN[k].RoVV;
             Mem_AuditWr(k,MEMAUDIT);

            MEMAUDIT.altvalue = CONFIG_IDRUN[k].HsVV;            if ((CONFIG_IDRUN[k].nKcompress < 3) || (T_SUn == GLCONFIG.EDIZM_T.T_SU))
            CONFIG_IDRUN[k].HsVV *= KoRo[GLCONFIG.EDIZM_T.T_VVP][T_SUP]; // перевод к T 0 гр.Ц
            MEMAUDIT.type     = 37;
            MEMAUDIT.newvalue = CONFIG_IDRUN[k].HsVV;
              Mem_AuditWr(k,MEMAUDIT);
          }
       } //for (k=0; k<3; k++)
            GLCONFIG.EDIZM_T.T_SU =  T_SUn;
            GLCONFIG.EDIZM_T.T_VVP =  T_SUP;
            GLCONFIG.EDIZM_T.T_AP =  (BufInKadr[26]>>4);
//printf("T_SU=%d T_SUP=%d T_AP=%d\n", T_SUn, T_SUP, GLCONFIG.EDIZM_T.T_AP);
      if (BufInKadr[27] != GLCONFIG.EDIZM_T.EdIzP) {
         MEMAUDIT.type     = 63;
         MEMAUDIT.altvalue = GLCONFIG.EDIZM_T.EdIzP;
         MEMAUDIT.newvalue = BufInKadr[27];
         GLCONFIG.EDIZM_T.EdIzP = BufInKadr[27];
	     Mem_AuditWr(0,MEMAUDIT); // запись отчета вмешательств
	     Mem_AuditWr(1,MEMAUDIT); // запись отчета вмешательств
	     Mem_AuditWr(2,MEMAUDIT); // запись отчета вмешательств
      }
      if (BufInKadr[28] != GLCONFIG.EDIZM_T.EdIzmdP) {
         MEMAUDIT.type     = 64;
         MEMAUDIT.altvalue = GLCONFIG.EDIZM_T.EdIzmdP;
         MEMAUDIT.newvalue = BufInKadr[28];
         GLCONFIG.EDIZM_T.EdIzmdP = BufInKadr[28];
	   Mem_AuditWr(0,MEMAUDIT);// запись отчета вмешательств
	   Mem_AuditWr(1,MEMAUDIT);// запись отчета вмешательств
	   Mem_AuditWr(2,MEMAUDIT);// запись отчета вмешательств
       }
      if (EdEn != GLCONFIG.EDIZM_T.EdIzmE) {
         MEMAUDIT.type     = 65;
         MEMAUDIT.altvalue = GLCONFIG.EDIZM_T.EdIzmE;
         MEMAUDIT.newvalue = EdEn;
         GLCONFIG.EDIZM_T.EdIzmE =BufInKadr[29];
	   Mem_AuditWr(0,MEMAUDIT);// запись отчета вмешательств
	   Mem_AuditWr(1,MEMAUDIT);// запись отчета вмешательств
	   Mem_AuditWr(2,MEMAUDIT);// запись отчета вмешательств
       }
      memmove(&GLCONFIG.EDIZM_T,&BufInKadr[4+16],6);//пеpезапись паpаметpов
      if (Mem_ConfigWr()==0)
         SaveMessagePrefix_2(BufTrans,6,0xE8);  //единицы измерения записаны
      else
         SaveMessagePrefix_2(BufTrans,6,0xFF);  //единицы измерения не записаны
  } // else зписаны
}

//--------------------------------------------------
// фы  сюЁ№с√ ё чртшёрэш ьш ъюььєэшърЎшш яю COM1
//
//void Uart2_Watch()
//{
//  char c,buf[10];

//   // яЁхф°хёЄтєх∙хх чэрўхэшх ёўхЄўшър шэшЎшрышчрЎшщ яЁшхь
////   static unsigned int st_cntIniRecCom1;

//   // ёўхЄўшъ фы  ЁхрышчрЎшш чрфхЁцъш
//   static int st_cntDelay;

//   // фры№эхщ°√щ ъюф сєфхЄ т√ч√трЄ№ё  ърцф√щ фхё Є√щ Ёрч
//   if(++st_cntDelay < 10)
//      return;

//   st_cntDelay = 0;
//   if(g_cntIniRecCom2 == st_cntIniRecCom2)
//    { if (Flag_Hartservice2==0)  IniCfgSpeed2();
//      Ini_Reciv2();
//    }
//   else   st_cntIniRecCom2 = g_cntIniRecCom2;
//}
//---------------------------------------------------------------------------

void ExitFromHART(void)
{   HART_stop();
    Flag_Hartservice = 0;       //флаг перехода в режим работы с Confidan
    ResetHartS = 0; // флаг принудительного выхода из режима Hart_Com
    SaveMessagePrefix_2(BufTrans1,6,0xFE); //формирование посылки
    CheckBufTrans(BufTrans1); //занесение КС
    Ini_Send1();  // передача буфера BufTrans1. После передачи 
// CallBack сам переключит порт USART1 на прием      
 }

