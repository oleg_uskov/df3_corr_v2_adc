/* User Code Begin Header */
/**
  ******************************************************************************
  * @File           : Main.C
  * @Brief          : Main Program Body
  ******************************************************************************
  * @Attention
  *
  * <H2><Center>&Copy; Copyright (C) 2020 Stmicroelectronics.
  * All Rights Reserved.</Center></H2>
  *
  * This Software Component Is Licensed By St Under Ultimate Liberty License
  * Sla0044, The "License"; You May Not Use This File Except In Compliance With
  * The License. You May Obtain A Copy Of The License At:
  *                             Www.St.Com/Sla0044
  *
  ******************************************************************************
  */
/* User Code End Header */
/* Includes ------------------------------------------------------------------*/
#Include "Main.H"
#Include "Fatfs.H"

/* Private Includes ----------------------------------------------------------*/
/* User Code Begin Includes */
#Include "Stdio.H"
#Include "Stdlib.H"
#Include "String.H"
#Include <Math.H>
#Include "H_calc.H"
#Include "H_mem.H"
#Include "H_main.H"
#Include "H_time.H"
#Include "H_disp.H"
#Include "Hart_driver.H"

//#Include "Liquidcrystal.H"

#Include "W25.H"
#Include "Stm32l4xx_hal_rtc.H"
/* User Code End Includes */

/* Private Typedef -----------------------------------------------------------*/
/* User Code Begin Ptd */
#Define Dwt_control *(Volatile Unsigned Long *)0xe0001000
#Define Scb_demcr   *(Volatile Unsigned Long *)0xe000edfc
#Define  Count_dma2 40
#Define  Count_urt3 40
#Define  Com5_buf_len     80
#Define Mode8  (0x00000700u)
#Define Mode16 (0x00000f00u)
//#Define Uart_txfifo_threshold_1_2   Usart_cr3_txftcfg_1        
#Define  Modbus_send_len_fb 8
#Define  Modbus_send_len_ascii  17
#Define  Modbus_send_len_rtu 8
#Define  Count_modbus 80
#Define  Maxindex          550
Void Dwt_init(Void) {
	Scb_demcr |= Coredebug_demcr_trcena_msk; //
	Dwt_control |= Dwt_ctrl_cyccntena_msk;  // 
}

/* User Code End Ptd */

/* Private Define ------------------------------------------------------------*/
/* User Code Begin Pd */
#Define Testaddr  0x20000//0x7f//0x007f0000
#Define Testsize  520 // ä�� ì������

#Define Testsizew  64 // ä�� ç�����
#Define Testsizer  64 // ä�� ÷�����
#Define Maxq    30   //ì��� ä���� î������
#Define Kqu      3  // Ú��������� î�������
#Define Syncbyte    0xaa        // ñ���?p����� ÿp� ç��p���
#Define Ltimereq 600    // ñ�� ÿ���� ÿ��������� î�����

/* User Code End Pd */

/* Private Macro -------------------------------------------------------------*/
/* User Code Begin Pm */

/* User Code End Pm */

/* Private Variables ---------------------------------------------------------*/
Crc_handletypedef Hcrc;

Qspi_handletypedef Hqspi;

Rtc_handletypedef Hrtc;

Spi_handletypedef Hspi1;
Spi_handletypedef Hspi2;
Spi_handletypedef Hspi3;
//Dma_handletypedef Hdma_spi1_rx;
//Dma_handletypedef Hdma_spi1_tx;

Tim_handletypedef Htim16;
Tim_handletypedef Htim17;

Uart_handletypedef Huart1;
Uart_handletypedef Huart2;
Uart_handletypedef Huart3;
//Dma_handletypedef Hdma_usart1_rx;
//Dma_handletypedef Hdma_usart1_tx;
Dma_handletypedef Hdma_usart2_rx;
Dma_handletypedef Hdma_usart2_tx;
Dma_handletypedef Hdma_usart3_rx;
Dma_handletypedef Hdma_usart3_tx;

/* User Code Begin Pv */
/*  Ð��?���: ß������������ Per = 1, ×������ Hou = 2. Þ�� à��?��� î���������� ð��-
ì���, ÿ� 100 Ú� í� í����.Þ��� ç����� 64 â����. Ý� î��� í���� 100 Ú /64 = 1600
ç������.
  Ñ������� à��?�� Day = 3, 28 Ú í� í���,  28 Ú/64 = 448 ç������.
  Ð�������� à��?�� Alr = 4, 24 K í� í����, 24� / 16 = 1536 ç������ ÿ� 16 â���
  Ð��?�� â����������� Aud = 5, 40 K í� í����, 44 Ú / 32 = 1408 ç������
  Ð��?�� â����������� Scr = 6, 32 K í� í����, 32 Ú / 32 = 1024 þ�����
  Ð��?�� à��������? î������ Avol = 16 K í� í����, 16 Ú / 32 = 512 ç�����.
  Ñ�������� ÿ������������? - ÿ� 74 â���� í� í���� = 74*3=222 â,
  Ñ�������� ÷������? - 74*3=222 â,
  Ñ�������� ñ�������? - 74*3 = 222 â.
*/
Uint32_t Alt1, Alt2, Alt3, Count1, Count2, Count3;
Uint32_t Ctim1, Ctim2, Ctim3;
Uint8_t  Bflag_c1,Bflag_c2;
Uint32_t Intr[3]; //, Intr2, Intr3; [2]
Uint32_t Interval[3] = {0,0,0};  //ø������� ì���� ø���������


Double Dqw_1, Dqw_3, Dqw_2;  // ñ�������� ð���?�� ñ�������� ü���
 Double Dqw, Qw;
//Uint16_t Pim = 0;
Uint32_t K,In1,In2,K1;
Uint32_t Ct1, Ct2, Ct3;
Float Kd = 0.75;
Uint32_t T16; 

 Uint8_t St_send=1, Busy=1;
 Hal_statustypedef St=1;
 Char _print[20];
 Uint32_t Ad;
// Ä�� W25
 __io Uint8_t Cmdcplt, Rxcplt, Txcplt, Statusmatch, Timeout,Rx23cplt;
 __io Uint8_t Rx23cplt,Tx23cplt,Rxcpltu3,Txcpltu3;
  Uint16_t Bsectorerase[9] = {0,4,79,154,175,193,226,250,262}; // ä� 427
  Uint16_t Kolsectorarch[9] = {0,25,25,7,6,11,8,4,55};// 55};  
  Uint16_t Recsizew[9] = {0, 64,64,64,16,32,32,32,64};// 0, Rec,Hor,Day,Alr,Aud,Scr,Avol
  Uint16_t Recsizer[9] = {0, 64,64,64,16,18,18,32,64};
  Uint16_t Kolzaparcall[9] = {0,1600,1600,448,1536,1408,1024,512, 3520};//3520 191};
  Uint16_t Kolzaparc[9] = {0,1536,1536,384,1280,1280,896,384,3456}; //  3456 128};
  Uint16_t Sectorsize=4096;  
  Uint16_t Timeoutw = 3000;
  Uint8_t Bpoweroff;  // ÿ������ �?�������� ø�� ü������� ñ�����
  Uint16_t Zap_in_block;
  Uint16_t St_cntinireccom1;
  Uint16_t G_cntinireccom1;
  Uint16_t St_cntinireccom2;
  Uint16_t G_cntinireccom2;
  
  Uint16_t Erasebl[9][3]; //={0}; //,0,0,0,0,0,0,0,0}; // 54 â����
    Hal_statustypedef Er_i2c;
// Uint32_t Baudrates_huart12[]={1200,2400,4800,9600,19200,38400,57600,115200};
// Uint32_t Baudrates_huart2[]={1200,2400,4800,9600,19200,38400,57600,115200};

 Char  Comnd; 
Int Scrbuthit(Void);                        // î���������� í������ ú����� þ������
//Void N_delay (Int Mlsek);
Unsigned Int Crc16(Unsigned Char* Buf, Int Isize);
Void Config(Void);
Void Braket1(Float* Xi);
Int Time_getnumrun(Void);                  // ÷����� ó������� í���p� í����
Void Initialdata(Void);
Void Dzofpt_b(Void);
Void Task_calc_corrsetcalcdata();
Void Chekbufresiv1();
Void Ini_reciv1();
Void Uart_endtxtransfer(Uart_handletypedef *Huart);
Void Mem_alarmsetnoalarm();
Static Void Uart_dmatransmitcplt(Dma_handletypedef *Hdma);
Static Void Uart_dmareceivecplt(Dma_handletypedef *Hdma);
Void Setrs485dataadr(Void); 
Void Rs485_setrs485err(Void);
Void Rs485com4inihf(Void);  
Void Rs485ini(Void);
Void  Startuzs(Int);
Void  Firstreadfs(Int,Int,Uint16_t); // ¿����� ç����� ®����� ¸� �␢␘ Flowsic
Void Result_modbus(Void);
Void Time_getcount(Void); 
Uint32_t  Setcomspeed(Uint8_t);
Void Setspeeduzs(Uint8_t Nrun);
Void Persist_addqwcounttotal(Int Irun, Double Qwcounttotal);
Double Persist_getqwcounttotal(Int Irun);
Void Setcursor(Uint8_t Col, Uint8_t Row);
Void Printform0(Void);
Void Setwindows1();
Void Setwindows2();
Void Glprint();
Void Uart1_watch(Void);
Void Inicfgspeed1();
Void Inicfgspeed2();
Void Setspeed_usart3(Uint32_t);
Void Zapuskuzs();
Void Lcdprint(Uint8_t Stroka, Uint8_t Colonka, Char * Text);
Void Lcdresligt(Void);

Typedef Void (*Pf)(Void);
Struct Tq                               // ñ�p����p� þ������� î��p��� ú ó����p�
{ Unsigned Int  Time;                   // ú��������� ó�����
  Int           Prioritet;              // í���p ÿp��p����� ô������
  Pf            Task;                   // ó�������� í� ô������
};
Int Inqueue(Int N,Pf Func);






/* User Code End Pv */

/* Private Function Prototypes -----------------------------------------------*/
Void Systemclock_config(Void);
Static Void Mx_gpio_init(Void);
Static Void Mx_dma_init(Void);
Static Void Mx_rtc_init(Void);
Static Void Mx_tim17_init(Void);
 Void Mx_usart1_uart_init(Void);
Static Void Mx_usart2_uart_init(Void);
Static Void Mx_usart3_uart_init(Void);
Static Void Mx_quadspi_init(Void);
Static Void Mx_spi1_init(Void);
Static Void Mx_spi2_init(Void);
Static Void Mx_spi3_init(Void);
Static Void Mx_crc_init(Void);
Static Void Mx_tim16_init(Void);
/* User Code Begin Pfp */
Static Void Qspi_autopollingmemready_it(Qspi_handletypedef *Hqspi);
Static Void Qspi_autopollingmemready(Qspi_handletypedef *Hqspi,Uint32_t Timeout);
Uint8_t Qspi_send_cmd_it(Uint32_t Instruction, Uint32_t Address,Uint32_t Dummycycles, 
                    Uint32_t Addressmode, Uint32_t Datamode, Uint32_t Datasize);
Uint8_t Writedataw25_it(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Wrdata);
Uint8_t Readdataw25_it(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Rddata);
Void Writedataw25(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Wrdata);
Void Readdataw25(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Rddata);

Hal_statustypedef Hal_qspi_autopolling_it(Qspi_handletypedef *Hqspi, Qspi_commandtypedef *Cmd, Qspi_autopollingtypedef *Cfg);
Void Liquidcrystal(Gpio_typedef *Gpioport,Uint16_t D0, Uint16_t D1,
    Uint16_t D2, Uint16_t D3,Uint16_t D4, Uint16_t D5, Uint16_t D6, Uint16_t D7);
Size_t Print(Const Char Str[]);
Void Hal_rtc_mspinit(Rtc_handletypedef* Hrtc);

Unsigned Char Flag_hartservice;         // ¿������ à����� â��������� Hart<->Com2
//Extern Int Calcnumrun;                  // í���p í���� ä�� ð������
Uint16_t  Counttime;         // ú��-â� ó����� ä�� ñ��������� âp�����
Extern Int Nrep;//Rnrep;                  // ì���. ø ð������� ÷���� í�����. ÿ�������
Extern Unsigned Int Inindex1,Inindex2;  // ñ�. í� ÿ����� þ������, Inc ÿ� ÿp�p. ÿp�������, =0 ÿp� ø���.
Extern Unsigned Int Comindex;           // ñ�. í� ÿ����� þ������, Inc ÿ� ÿp�p. ÿp�������, =0 ÿp� ø���.

Extern Uint8_t Bufresiv1[Maxindex];
Extern Uint8_t Bufresiv2[Maxindex];
Extern Int   Flagsomwi;                 // ô��� ä�� ÿ�p��?��� ñ ë������ í� ç����� âp���
Extern Struct Tdata Tdata;              // ä�� ñ����p������ ä�����?
Extern Struct Bintime Power_off_time;   // â���� â���������
Extern Int   Starttime;                 // âp��� "P���p����" ñ������
Extern Unsigned Char Flag_hartservice_1;
Extern Int Cwatchdog;
Extern Struct Clock_image Time_wrtime;  // ø����������� ÿp� ó�������� âp�����

Extern Struct Memaudit    Memaudit;     // ñ�p����p� ç����� àp�?��� â�����������
Extern Struct Memdata     Memdata;      // ñ�p����p� ç����� î����� - 26 â���
Extern Struct Bintime     Bintime;      // ä������� ñ�p����p� âp�����
Struct Bintime     Copy_bintime;      // ä������� ñ�p����p� âp�����

Extern Struct Intpar      Intpar;
Extern Struct Calcdata    Calcdata[3];
Extern Struct Memalarm    Memalarm;     // ñ�p����p� ç����� àp�?��� à��p��
Extern Struct Clock_image Wtime;
Extern Long Qcntt;                      // ñ������ ñ����� ä�� ð���?��� ÿ� ñ�������
Extern Char Txrx1,Txrx2,Txrx485; // ÿ����/ÿ������� 0/1
Extern Unsigned Int Base_com3;
Uint8_t   Glnumrun;                         // í���p í���� ä�� à��?���
Extern Double Indq[3];
Extern Unsigned Int Altcount0,Altcount1;
Extern Double Kvolumeuzs[];
Extern Unsigned Long Int Vpred[5];
Extern Unsigned Int Di1,Di2,Di1p[2][4],Di2p[2][4];
Extern Int Nzblk[],Nzblkp[];
Extern Int Bsdini;
Extern Int Bsdini_1;
Extern Int Bwrmgn;
Extern Unsigned Char Spr;
Extern Struct Bintime Logtimeb,Logtimeb1;
Extern Unsigned Long Tval2,Tval22;
Extern Unsigned  Int Tval12;
Extern Unsigned Long Timekp1[],Timekp2[];
Extern Char Mv[3][4];
//Extern Int Aga_f[];
Char Calc59mgn1, Calc1per1,Calc5conf1;
Extern Char Numchrom[];
Extern Char Firstcalc,Timecalc;
Extern Float T_st;
Char Numchrom1[3]={1,2,3};
//Struct Idrun Idrun[3]; // 520*3=1560+168=1728 // ÿ�p����p� í����
Extern Uint8_t Flagspeed1;
Extern Uint8_t Modbus_sendbuf[32];// Com5sendbuf[32];              // ¢���� ¿������
Extern Uint8_t Modbus_recvbuf[Com5_buf_len+1]; //Com5recvbuf[Com5_buf_len+1]; // ¢���� ¿����
Extern Uint8_t Resmodbus;
Extern Struct Printform  Printform;
//---------------------------------------------
Struct Bintime     Time_settime;        // ø����������� ÿp� ó�������� âp�����

Long Ta1,Ta2,Tb1,Tb2;           // ä�� ç������ ç�������
Int Sms_send;
Float Q2;
Extern Int   Od2;
Extern Char Bterm,Bq;
//Extern Char Hart_buf_out[],Hart_buf_out2[];     // â���p ÿ�p�����
//Extern Int  Inhartbufcount,Inhartbufcount2;         // ñ������� â ú��p� ä�� Hart-ì����� (ÿp���)
//Extern Char Hartnum2,Hartnums,Hartnum;
Extern Unsigned Int      Rs485num;       // ú��-â� ä������� ä�� Rs485
//Extern Int Noask;
Extern Unsigned Int  Codefsn[3][16];
Extern Unsigned Int  Codealarmfs[],Codealarmfsk[],Codenoalarmfsk[];
Extern Unsigned Int Codenoalarmfs[];
Extern Char Erstatus[3][16], Noerstatus[3][16];
Extern Unsigned Int Sendcount;
Extern Uint8_t Buftrans1[];
Extern Unsigned Int Outindex1;
Extern Unsigned Char Errmodbus[];
Extern Unsigned Int Errmb[],Errqw[];
Extern Int Tal1[3],Tal2[3],Twr1[3],Twr2[3];
Extern Unsigned Char Com5sendbuf[];              // â���� ÿ������
Extern Char Comh[];
//Extern Int Nv;
Extern Unsigned Long Int Vr1[],Vws1[],Vrls[];
Extern Double   Vpo[3];// î���� ç� â���� â��������� â����������
Extern Unsigned Char Run_sms;
Extern Int    Hartcount3,Hartcount2,Hartcount1;             // ó������ í���p ä������ â ñ����� ä�� ñ����p������
Extern Uint8_t Mrun, Nuzs; // í���� í���� ø ÿ��������� í���� Ó��
Extern Unsigned Int Modbuslen;
Extern Double  Vwsr[]; //Vthr,
Extern Unsigned Int  Dostup,Dostup1;
//Extern Char Numlogin,Numlogin1,Km;
Extern Unsigned Char  Tlogin[];
Extern Unsigned Char Tlogin1[];
Extern Double Vrts[];
Extern Int Cwatchdog,Nwatch;
Extern Struct Memsecurit   Memsecur;
/*--------------- í����� î������� ñ�p����p� ú ô������ 0x56 ----------------*/
//Extern Gornalpassw Logpassw[6];   //6*15=90 â��� C 4096 â���� ñ��. 81
Extern Struct Sint Is;
Extern Char Attempt[],Attempt1[];
Extern Int Fmb; // î��������� ô������ ÷����� ÿ� Modbus (1-ñ����c, 2-î����, 3-ð���?��)
Extern Unsigned Char Mcmd0[6];   // 48 Bit
Extern Unsigned Char Mdatar[514];
Extern Unsigned Char Mdataw[514];
Extern Long Nb1;
Extern Unsigned Char   Buftrans2[];
Extern Unsigned Int    Outindex2;
Extern Unsigned Int    Sendcount2;
Extern Char Bnew[];
Extern Int Wrmgnal[],Wrperal[];
Extern Int Ksd;
Extern Struct Conf Confblock;
Extern Long Tlastal[];
Extern Unsigned Int Buf_err[2][3];
Extern Float  Amountqru[3][6];
Extern Int Amk[3];
Extern Char Passwr2[];
Extern Char Passwr1[];
Extern Char Sdmgn2,Sdmgn1,Sdper1,Sdper2;
Extern Unsigned Long Offsetmgn,Offsetper;
Extern Int Nowritemgn;
Extern Float Vru1[3],Vs1[3];
Extern Unsigned Long Int Stfl;
Extern Char Mgnalarm;
Extern Char Errmodbusqw[3];
Extern Unsigned  Char Lcom4inthf;

Uint8_t Str90[90];
Extern  Float Sumheat[2],Sumro[2],Sumco2[2],Sumn2[2];
Extern  Int Kheat[2],Kro[2],Kco2[2],Kn2[2];
Extern  Char Fbcom;
Extern Double  Bmix[];
Extern Float Told[];
Extern Int Inisd;
Extern Float Dmin;//0.000001;   //ì������� ÿ�������� ü��� í�����
Extern Float Dmax;//40.0;    //ì������� ÿ�������� ü��� â�����
Extern Unsigned Char Runed_1_3;
//Extern Unsigned Char Errrv[];
Extern Float Startre[];
Extern Float Xjm[3][21];
Extern Unsigned Char Qsterrflag;
Extern Int M_ndaysummer;                // ä��� ÿ����?��� í� ë����� â����, 0 - ô��� ÿ����?�� ç�������
Extern Int M_ndaywinter;                // ä��� ÿ����?��� í� ç����� â����, 0 - ô��� ÿ����?�� ç�������
Extern Struct Datawithcrc Persist_m_data;
Extern Unsigned Char Hartsensready;     // ü��������� Hart-ä�������
Uint8_t Buf_inmb;
Int Sp1;
Char Fc;
Struct Glconfig    Glconfig;
Extern Struct Glconfig    Res_glconfig;
Extern Struct Tdata        Tdata;              // ¤�� â����p������ ¤����� ¿������������
Extern Struct Tdata        Hdata;              // ¤�� â����p������ ¤����� ç������


Struct Idrun Config_idrun[3];
Struct Addcfg       Adconfig;           // ä������������� ú�����������
Struct Adrun Config_adrun[3]; 
Struct Glconfig *Maincfg;
Struct Idrun  *Idrn;
Unsigned Char Ancom2,Ancom5; // ÿ������ í������ Com2
Unsigned Char Ancom1; // ÿ������ í������ Com1
Unsigned Char Latnrun[3][20];
Unsigned Char Needcom1;                 // â���� Ñ��1 ÿ�� ñ����
Unsigned Char Calcstartflag = 0;        // ÿ������ ç������ ð������ ä��������
Unsigned Char Corrstartflag = 0;        // ÿ������ ç������ ð������ ñ�������
Unsigned Char Calcreadyflag[3]={0,0,0}; // ÿ������ ü��������� ä�����? ð������
Char Tplat=3;                           // ó�� ÿ���� =0(S), =1(N), = 3(K)
Unsigned Char Avsig;
Unsigned Char Sh[4]={0x20,0x20,0x20,0x20};
Unsigned Char Evenflagrma[3];
Float  Kki; // ú���������� ú�������� ø�������� ì�� ø��
Char Bdisp;
Unsigned Int Obs_ss[4]; // ñ����� "Ò�������� î����������� ó/ç ñ�������"
Unsigned Int Obs_err[5];
Char Startcpu;
Int   Starttime;   
Unsigned Int Flash_rom;
Unsigned Char Firstmb[3], Firster[3], Firstrun;
Float Dval1[3],Dvful1[3],Dval2[3],Dvful2[3];
Double Vrual1[3],Vful1[3]; // ÿ��������� ç������� ÿ��������� î����� ð� Ó��
Struct Bintime Optimeb;
Int Port2 = 0x2f8;
Unsigned Long Int Starttim;
Unsigned Long Int T3;
Char Nowritecycle[3];
Unsigned Int Extkoef = 3132; //ä� 3248  ä�� ç����� ú������������ Koefp,Koefdp,Koefe,Koro
//Unsigned Int Extchroma = 2766;// ä�� ç����� �?���������� Xi 3 ì������ ÿ� 21*4 â��� (252 â����)
Double Koefp[4]   = {1.0,0.0980665,98.0655,0.980665}; //ø� ú�/ñ�2 â Ü��, ú�� ø�� Ã��
Double Koefdp[3]  = {1.0, 0.00980655, 0.0980665};  // ø� ú�/ì2 â ú�� ø�� ì���
Double Koefen[3]  = {238.846, 1.0, 0.277778};  // ø� Ü�� â ú��� ø�� ú���
//Char Densomer;
Unsigned Int Respassw,Respassw1;
//File *Fp;
//Char Far *Pcxi;
Unsigned Int Extchroma = 2766;// ä�� ç����� �?���������� Xi 3 ì������ ÿ� 21*4 â��� (252 â����)
Uint8_t  Modbustcp,Modbustcp_1,Modbusrtu,Modbusrtu_1;
//  = {{ 0.9070, 0.0310, 0.0050, 0.0450, 0.0084, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0010, 0.0015, 0.0003, 0.0004, 0.0004, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
//    { 0.9650, 0.0030, 0.0060, 0.0180, 0.0045, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0010, 0.0010, 0.0005, 0.0003, 0.0007, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
//    { 0.8590, 0.0100, 0.0150, 0.0850, 0.0230, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0035, 0.0035, 0.0005, 0.0005, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 }};
Float Xj[3][21];
Extern Double Z[];
Extern Float Xi[];
Extern Int Cid[];
Char Calcbr[3],Z_out[3],Z_in[3];
Char Ic;
Int Ncc;
Uint16_t Di1s[3],Di2s[3];
Uint16_t  Kolels[3],Kolelm[3],Kolelh[3],Koleld[3];
Uint16_t Numzap[3];
Uint16_t Sizecfg, Sizeidr,Sizeadcfg,Sizeadr,Sizeintpar,Sizecalc;
Uint16_t Sizefhp;
_bool B_massomer[3],B_counter[3];
Uint8_t   Bbegincalc;
Uint8_t   Txcpltdma4,  Rxcpltdma4,Txcpltdma5,  Rxcpltdma5,Txcpltusr3,  Rxcpltusr3; 
Uint8_t  Txcpltmodbus,Rxcpltmodbus;
Int   Startflag;
Gpio_pinstate But1, But2;
Uint8_t In_but1,In_but2;
Uint8_t  Setw1,Setw2;
Uint8_t  Flagw1, Flagw2;
Uint16_t Delayw1, Delayw2;	

//Union
//{    
// Uint8_t Mass_qs[3][Arcs_b + Blarc_b];
// Struct Memdata As[3][Arcs+Blarc]; 
//} Dataarc;
//Union
//{
//    Uint8_t Bl_s[Blarc_b];  // â��� 4096 â
//    Struct Memdata As[Blarc]; //64 ç����
//} Blockas;
Uint16_t Arcs_1 = Arcs + Blarc; // ÷���� ñ���� â����� ñ ç�������� (64)


Uint8_t Adrcfg[2] = {0,32}; //ñ 32-ü� â���� 224 â���� ä�� Glconfig
Uint8_t Adridrun[3][2] = {1,0,2,0xc0,4,0x80};  // ä� (6,0x40) ÿ� 448 â��� ,
Uint8_t Adraddcfg[2] = {6,0x40}; // Ð���� ä������������� ú����������� 32 â����
Uint8_t Adradrun[3][2] = {6,0x60,7,0x00,7,0xa0}; // ä� 8,0x40 //Ð����� Ä��. ú����������� ÿ� í��7��� ÿ� 160 â���
Uint8_t Adrintp[2]={8,0x40}; //32 ä� 8,0x60
Uint8_t Adrcalc[3][2]={8,0x60,9,0xc0,0xb,0x20}; //352*3 ä� 0xc,0x80
Uint8_t Adrpointarch[9][2] = {0,0,0x0c,0x80,0x0c,0x8c,0x0c,0x98,0x0c,0xa4,0x0c,0xb0,
    0x0c,0xbc,0x0c,0xc8,0x0c,0xd4}; // ä� (0xc,0xe0) 3�232
Uint8_t Adrerasebl[2]= {0xc,0xe0};  // ä� (0xd, 0x16)            54    
Uint8_t Adrsumm[3][3][2]= {0xd,0x16,0xd,0xf4,0xe,0xd2,0xf,0xb0,0x10,0x8e,
  0x11,0x6c,0x12,0x4a,0x13,0x28,0x14,0x06};//ä� (0x14,0xe4) //222*3*3=1998=7ce â���   222=De
Uint8_t Adrfhp[2] = {0x14,0xe4}; // 252 â��� ä� (15e0)  {14e4,1538,158c} ä� {15e0}
Struct Addcfg     *Adcfg; // = &Adconfig;
Struct Pointarch Parchiv;
Struct Addrtemper Addrtemper1 ={{0x00,0xa0},{0x20,0xa0}};

//Rtc_handletypedef Hrtc;
//Tim_handletypedef Htim1;
//Tim_handletypedef Htim17;
    
Uint32_t Timereq;       // â���� î� ÿ��������� î�����, ñ

    // ø����������� ÿ�� ñ����� ñ������
Int Systakt, Nrn;
Unsigned Char Hartformat = 17;          // ô����� Hart-ú������
//Unsigned Char Flag_hartservice;         // ÿ������ ð����� ó��������� Hart<->Com2
                  
Char  Rs485hf[3],Rs485mb;
Int             Value;                  // ä�� ñ��p��
//Unsigned Int    Calc_buferr[30];
Struct Memdata    *Pmpd;
//Extern Int Nsp[3], Nsp1[3];
Unsigned Int Hart2;                              //í������ Hart2
Int Isum;
Long Sumdens;
Char Startt,Startt1,Startt2;
Char Power;
Unsigned Char Ntmp;
//Unsigned Char Hartc;
Unsigned Char Wminute,Whours;
Uint32_t Timereq,Timereq1;       // â���� î� ÿ��������� î�����, ñ
Int Nwr,Nwra,Nwr1,Nwr2; // î������� ä����� â���������� ø ç����� ç�����
//#Define   Rirq3             0xff3e
Unsigned Int Kn,Kr,Ns,Bsdoff,Bsdon,Bsdoff_p,Bsdoff1;
Float Koro[3][3]={1.0, 1.0737565, 1.0174792,0.9313097, 1.0, 0.9475883,0.9828210,1.0553105,1.0};
//Float Koro[3][3];
Int W;
Long Tin;
Unsigned Int Pz;
//Struct    Watch_mem Watchmem[5];
//Unsigned Long Int Tls;
Union
{
Char Msc[8];
Long Msl[2];
} Stms;
Int Kolaga8;
//----------------------- ä�� ä��������� ç���� -----------------------------------
Struct Tq  Timequeue[Maxq];                    // î��p��� þ�������� ú ó����p�
Int Longtimequeue=0;                    // ä���� î��p��� þ�������� ú ó����p�

Pf  Worktask;                           // ó�������� í� â���������� ô������
Pf  Queue[3][Maxq];                     // ì����� î��p���� ô������
Uint16_t Longqueue[3];                       // ì����� ä��� î��p���� ô������
Pf  *Ukq;
Int A;
//__io Uint8_t Cmdcplt, Rxcplt, Txcplt, Statusmatch, Timeout;
Uint8_t Startr;
Uint8_t *Padr;

Struct {
   Struct Bintime Lasttime;
   Char B[2];
} Offtime;
Struct Bintime Lasttime;
Char Eraseconf;
Int Bsdyes;
Char Utr[3],Utr1[3];
//Int Nf1;  //ÿ����?�� ú Ñ� 0 ü�.Ö
Uint8_t Calczc[3],Zc1[3];
Char Firstc[3],Firstt[3];
Char Brz[3];
Int Nrnz,Nrnzc,Nrnb;
Struct Compress Zcks[3];
Float  Zck1[3][2]={0.8,0.9,0.8,0.9,0.8,0.9};
Uint8_t Kt;
Uint32_t _sectakt;
Void Setcursor(Uint8_t Col, Uint8_t Row);
/* User Code End Pfp */

/* Private User Code ---------------------------------------------------------*/
/* User Code Begin 0 */
Float    Ododelta,                      // à�������
         Odoprev,                       // ¿��������� §������� ®�����
         Odorest;                       // ®������ ®� ¿���������� à������
Int      Odoimp,                        // ç���� ¸�������� º ¢�����
         Fod;                           // à��������� ¬���� ¸���������
Char Odoenable;                         // à��������� ¢����� ¸��������
Int Resetharts;
Odostruc Odotab[2];                     // â������ ¤�� ¢����� ¸��������
Int      Odowrind,                      // ¸����� ¤�� §����� ¢ â������
         Odordind;                      // ¸����� ¤�� ç����� ¸� â������
Unsigned Char Odonumrun;                // ­���� à������ ®���������
Unsigned Char Prevodomask;              // ¬���� ®��������� ­� ¿��������� â����
Unsigned Char Nchan;
Float Qodo[3];                          // à������ ¤�� ®��������� ¿� ­�����
Int   Iqodo;                            // ç���� ­���� ¤�� à������ ®�����
                                        // ¸����������� ¿�� â����� â������

  Uint32_t _i;
  Uint32_t Tmpreg = 0;
  Uint8_t Card;
	
  Char Buffer[512];
//  Static Fatfs G_sfatfs;
//  Fresult Fresult,Fresult1,Fresult2,Fresult3;
//  Fil File;
  Int Len;
//  Uint Bytes_written;
  Uint16_t Sizefile;

  Uint32_t T_17, T_16;
  Uint16_t Tm1,Tm2;
  Rtc_timetypedef Stime;  //= {0};
  Rtc_datetypedef Sdate; //= {0};
  Rtc_alarmtypedef Salarm;// = {0};

Void P23k256_write_data(Spi_handletypedef *Hspi, Uint8_t *Adress, Uint8_t *Txdata, Uint16_t Txsize) 
{
	Uint8_t Instruction = 2;

	Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_reset);
	Hal_spi_transmit(Hspi, &Instruction, 1, Hal_max_delay);
	Hal_spi_transmit(Hspi, Adress, 2, Hal_max_delay);  //Adress[1]
	Hal_spi_transmit(Hspi, Txdata, Txsize, Hal_max_delay); 
	Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_set);
}
Uint8_t P23k256_read_data(Spi_handletypedef *Hspi, Uint8_t *Adress, Uint8_t *Rxdata, Uint16_t Rxsize) 
    {
	Uint8_t Write_cmd[]={0x01,0x41};
	Uint8_t Instruction = 3;

//	Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_reset);
//	Hal_spi_transmit(Hspi, Write_cmd, 2, Hal_max_delay);
//	Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_set);
    
    Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_reset);
	Hal_spi_transmit(Hspi, &Instruction, 1, Hal_max_delay);
	Hal_spi_transmit(Hspi, Adress, 2, Hal_max_delay);
	Hal_statustypedef Sr = Hal_spi_receive(Hspi, Rxdata, Rxsize, Hal_max_delay); 
//        If (Sr != Hal_ok) While(1);
	Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_set);
    Return Sr;
}

//---------------------------------------------------------------------------
  Void Init_parchiv(Uint8_t K)
  { Uint8_t N;
    Uint8_t Adr[2];
 //   For (K=0; K<3; K++)
    {                // ç������  
     Parchiv.Di1p[K] = 1599; // 1536 + 64 ÿ� 64 â = 100 Ú
     Parchiv.Di2p[K] = 0;
     Parchiv.Di1h[K] = 1599; // 1536 + 64 ÿ� 64 â = 100 Ú
     Parchiv.Di2h[K] = 0;
     Parchiv.Di1d[K] = 447; // 384 + 64 ÿ� 64 â = 28 Ú
     Parchiv.Di2d[K] = 0;
     Parchiv.Di1al[K] = 1535;  // 1280 + 256 ÿ� 16 â = 24 Ú
     Parchiv.Di2al[K] = 0;
     Parchiv.Di1au[K] = 1407; //1280 + 128 ÿ� 32 â = 44 Ú
     Parchiv.Di2au[K] = 0;
     Parchiv.Di1scr[K] = 1023; //896 + 128 ÿ� 32 â = 32 Ú
     Parchiv.Di2scr[K] = 0 ;
     Parchiv.Di1av[K] = 511; //384 + 128 ÿ� 32 â   = 16 Ú
     Parchiv.Di2av[K]= 0;
     Parchiv.Di1s[K] = 3519; //3520;  //  3456 + 64  ÿ� 64 â = 220 Ú 55 ñ�������
     Parchiv.Di2s[K] = 0;
    }   
// Ç����� í������ ñ������? ñ�������
    For (N=1; N<10; N++)
      Erasebl[N][K] = 0; // ñ������ â���� â���? à��?���� = 0
 };

//----------------------------------------------------------------  
  Void Init_arcper(Uint8_t M) //ì���� 001,010,011,100,101,110,111 - í���� í����
{ Uint8_t K;
  Uint8_t Mask=1;
    
    For (K=0; K<3; K++)
    { 
        If ((Mask & M) > 0)
      {  Parchiv.Di1p[K] = 1599; // 1536 + 64 ÿ� 64 â = 100 Ú
         Parchiv.Di2p[K] = 0;
         Erasebl[Per][K] = 0; // í���� ñ������ â����
      }
      Mask = Mask << 1;
    }      
//î�������� à��?��� Per 
     P23k256_write_data(&Hspi3,&Adrpointarch[Per][0], (Uint8_t*)&Parchiv.Di1p[0],12);   
// Ç����� ñ������? ñ������� à��?��� Per
    P23k256_write_data(&Hspi3,Adrerasebl,(Uint8_t*)Erasebl, Sizeof(Erasebl));
}
//---------------------------------------------
//î�������� à��?��� Hour
Void Init_archou(Uint8_t M) // M=0..7 (000 - í�� î��������, 001 -î������ 1-þ í����
{  Uint8_t K;
   Uint8_t Mask=1;
    
    For (K=0; K<3; K++)
    { 
        If ((Mask & M) > 0)
      {  Parchiv.Di1h[K] = 1599; // 1536 + 64 ÿ� 64 â = 100 Ú
         Parchiv.Di2h[K] = 0;
         Erasebl[Hou][K] = 0; // í���� ñ������ â����
      }
      Mask = Mask << 1;
    }      
    //î�������� à��?��� ÷������?
     P23k256_write_data(&Hspi3,&Adrpointarch[Hou][0], (Uint8_t*)&Parchiv.Di1h[K],12);   
// Ç����� ñ������? ñ������� à��?��� 
    P23k256_write_data(&Hspi3,Adrerasebl,(Uint8_t*)Erasebl, Sizeof(Erasebl));
}
///--------------------------------------------------------
//î�������� à��?��� Day
Void Init_arcday(Uint8_t M)
{  Uint8_t K;
   Uint8_t Mask=1;
    
    For (K=0; K<3; K++)
    { 
        If ((Mask & M) > 0)
      {  Parchiv.Di1d[K] = 447; // 447 = 384 + 64 ÿ� 64 â = 28 Ú
         Parchiv.Di2d[K] = 0;
         Erasebl[Day][K] = 0; // í���� ñ������ â����
      }
      Mask = Mask << 1;
    }      
//î�������� à��?��� Day
     P23k256_write_data(&Hspi3,&Adrpointarch[Day][0], (Uint8_t*)&Parchiv.Di1d[0],12);   
// Ç����� ñ������? ñ������� à��?��� 
     P23k256_write_data(&Hspi3,Adrerasebl,(Uint8_t*)Erasebl, Sizeof(Erasebl));
}
//----------------------------------------------------
//î�������� à��?��� à�����
Void Init_arcalarm(Uint8_t M)
{  Uint8_t K;
   Uint8_t Mask=1;
    
    For (K=0; K<3; K++)
    { 
        If ((Mask & M) > 0)
      {  Parchiv.Di1al[K] = 1535;  // 1280 + 256 ÿ� 16 â = 24 Ú
         Parchiv.Di2al[K] = 0;
         Erasebl[Alr][K] = 0; // í���� ñ������ â����
      }
      Mask = Mask << 1;
    }      
//î�������� à��?��� Alarm
     P23k256_write_data(&Hspi3,&Adrpointarch[Alr][0], (Uint8_t*)&Parchiv.Di1al[0],12);   
// Ç����� ñ������? ñ������� à��?��� 
     P23k256_write_data(&Hspi3,Adrerasebl,(Uint8_t*)Erasebl, Sizeof(Erasebl));
}
//--------------------------------------------------------
//î�������� à��?��� â�����������
Void Init_arcaudit(Uint8_t M)
{  Uint8_t K;
   Uint8_t Mask=1;
    
    For (K=0; K<3; K++)
    { 
        If ((Mask & M) > 0)
      {  Parchiv.Di1au[K] = 1407;  // 1280 + 256 ÿ� 16 â = 24 Ú
         Parchiv.Di2au[K] = 0;
         Erasebl[Aud][K] = 0; // í���� ñ������ â����
      }
      Mask = Mask << 1;
    }      
//î�������� à��?��� Audit
     P23k256_write_data(&Hspi3,&Adrpointarch[Aud][0], (Uint8_t*)&Parchiv.Di1au[0],12);   
// Ç����� ñ������? ñ������� à��?��� Audit
    P23k256_write_data(&Hspi3,Adrerasebl,(Uint8_t*)Erasebl, Sizeof(Erasebl));
}
//-----------------------------------------------------------      
Void Hal_tim_periodelapsedcallback(Tim_handletypedef *Htim)
{// ÿ��������� ÿ� ó������ 5 ì�
Struct Tq   *Ukt;
Int T;
    
   If (*(&Htim->Instance)  == Tim17)
   {If (T_17==0) //(T_17==200)
	{_sectakt++;
//	 T_17=0;
     If (_sectakt == 60) 
         {_sectakt = 0; } //Counttime = 0; }
//    Hal_rtc_gettime(&Hrtc, &Stime, Rtc_format_bin);
//    Hal_rtc_getdate(&Hrtc, &Sdate, Rtc_format_bin);
//    Hal_rtc_getalarm(&Hrtc, &Salarm, Rtc_alarm_a, Rtc_format_bin);  
//    Bintime.Seconds = Stime.Seconds; 
//    Bintime.Minutes = Stime.Minutes;
//    Bintime.Hours = Stime.Hours;
//    Bintime.Date = Sdate.Date;
//    Bintime.Month = Sdate.Month;
//    Bintime.Year = Sdate.Year;
	}
	T_17++;
    Counttime++; // = T_17;
   If (Flag_Hartservice==0)
   { If ((Counttime % 4) == 0)
       Chekbufresiv1();  //¿������� ¿�������� ¢����� Com2
   }   
//    If ((Counttime+2 % 4) == 0)   
//       Chekbufresiv2();  //¿������� ¿�������� ¢����� Com2
    
 //      Counttime = T_17;
    If(Longtimequeue)           //?P�??P?  ¤�?­? ®??P?¤? ? ? ��?P?
    { Ukt = Timequeue;
      Tm1=0;  //?®�-?® ?«?¬?­?®?, ?®��?¦�??? ?¤��?­?? ?§ ®??P?¤? ? ? ��?P?
      For (T=0; T<Longtimequeue; T++)
      { Ukt->Time--;  //¤??P. ?P?¬?­? ­�?®��?­?? ? ®??P?¤? ???? ?«?¬?­?®?
	    If (Ukt->Time == 0)
	    { Inqueue(Ukt->Prioritet,Ukt->Task); //­��?P ?P?®p????  §���??
	      Tm1++;
	    }
	    Ukt++;
	  }
      If (Tm1)//?¤��?­?? ? ??P?§�???? ?«?¬?­?®? ®??P?¤?
	  { If (Tm1 == Longtimequeue) Longtimequeue=0;
	    Else
	    { Ukt = Timequeue;
	      Tm2 = Longtimequeue;
	      For (T=0; T<Tm2; T++)  //??P?§�???? ?«?¬?­?®?
		  {If (Ukt->Time == 0)    //®?P?¤?«?­?? ?«?¬?­?  ¤�? ?¤��?­??
		    { Longtimequeue--;
		      If (Longtimequeue == 0) Break;
			   Memmove(Ukt,Ukt+1,Sizeof(Struct Tq)*(Tm2-T-1));
		      Tm1--; If (Tm1==0) Break;
		      Ukt--;  //?P�??P? , ??«? ??P?¬???­�?© ?«?¬?­? = 0
		    }
		  Ukt++;
		  }
	    }
	  }
    }
  } // End  Tim17
  Else  If (*(&Htim->Instance)  == Tim16)
   { 
		T_16++;
   }
}
//--------------------------------------------------

Void Time_getcount(Void)
{  // ÿ������ ø�������� â ñ������ î� ñ�������� ü���
 Uint32_t Val1, Val2, Val3; //, Ct1, Ct2, Ct3;
// Double Dqw;
    Long Long Tr;
    Struct Idrun  *Prun1;  
    Struct Idrun  *Prun2;
    Struct Idrun  *Prun3;
    Struct Adrun  *Aprun1;
    Struct Adrun  *Aprun2;
    Struct Adrun  *Aprun3;
    
    Prun1 = &Config_idrun[0];
    Prun2 = &Config_idrun[1];
    Prun3 = &Config_idrun[2];
    Aprun1  = &Config_adrun[0];
    Aprun2  = &Config_adrun[1];
    Aprun3  = &Config_adrun[2];
    
    // ä�� ÿ������ ñ������� Fl-1      
      If (Alt1 > Ct1) {          // â��� ÿ�p��������� ñ�������
	    Val1 = 0xffffffff - Alt1;
	    Val1 += Ct1;
	    Val1++;
      }
      Else                             // í�� ÿ�p��������� ñ�������
        Val1 = Ct1-Alt1;
      Alt1 = Ct1;

      If (Val1 > 0)
      {   Dqw    = (Double)Val1*Prun1->Numturbo; // ñ�������� î���� *Glconfig.Idrun[0].Numturbo)
       Persist_addqwcounttotal(0, Dqw); // ó��������� î����� Dqw
       Aprun1->Vtot_l =Persist_getqwcounttotal(0); // î���� î����
          
        If (Interval[0] < 96)  // Â�
          Dqw_1    = Dqw;  // ñ�������� ð���?�� 
      //Ý� ø Â�
        Aprun1->Qw_s = Dqw_1;
        Prun1->Qw = (Double)3600*Dqw_1; //÷������ ð���?�� â ð�
        If (Prun1->Qw > 1.2*Prun1->Qmax) Prun1->Qw = 1.2*Prun1->Qmax;  
        If (Prun1->Qw > 0)
         Kd = Pow(0.1/Prun1->Qw,0.1);            
        
      }
      Else 
      {    Tr =  (Long Long)(T16 - Intr[0]) - (Long Long)Interval[0];
          If (Tr > 0)  // î� ÿ������� ø������� ÿ����� â����� Interval�
          {
           If (Prun1->Qw > 0.2)
            Prun1->Qw = Kd * Prun1->Qw; // Ó��������� ð���?��� ÿ�� î�������� ø��������
           Else  Prun1->Qw = 0.0;
          }         
      }       
    // ä�� â������ ñ������� Fl-2 
      If (Alt2 > Ct2) {          // â��� ÿ�p��������� ñ�������
	    Val2 = 0xffff - Alt2;
	    Val2 += Ct2;
	    Val2++;
      }
      Else                             // í�� ÿ�p��������� ñ�������
        Val2 = Ct2-Alt2;
      Alt2 = Ct2;

      If (Val2 > 0)
      {  Dqw    = (Double)Val2*Config_idrun[1].Numturbo; // ñ�������� î���� *Glconfig.Idrun[1].Numturbo)
         Persist_addqwcounttotal(1, Dqw); // ó��������� î����� Dqw
         Aprun2->Vtot_l =Persist_getqwcounttotal(1); // î���� î����
          
        If (Interval[1] < 96)  // Â�
          Dqw_2    = Dqw;  // ñ�������� ð���?�� 
      //Ý� ø Â�
        Aprun2->Qw_s = Dqw_2;        
        Prun2->Qw = (Double)3600*Dqw_2; //÷������ ð���?�� â ð�
        If (Prun2->Qw > 1.2*Prun2->Qmax) Prun2->Qw = 1.2*Prun2->Qmax;  
        If (Prun2->Qw > 0)
         Kd = Pow(0.1/Prun2->Qw,0.1);            
        
      }
      Else If (Interval[1] > 0)
      {    Tr =  (Long Long)(T16 - Intr[1]) - (Long Long)Interval[1];
          If (Tr > 0)  // î� ÿ������� ø������� ÿ����� â����� Interval�
          {
           If (Prun2->Qw > 0.2)
            Prun2->Qw = Kd * Prun2->Qw;
           Else  Prun2->Qw = 0.0;
          }         
      }
    // ä�� ó������� ñ������� Fl-3 
//      If (Alt3 > Ct3) {          // â��� ÿ�p��������� ñ�������
//	    Val3 = 0xffff - Alt3;
//	    Val3 += Ct3;
//	    Val3++;
//      }
//      Else                             // í�� ÿ�p��������� ñ�������
//        Val3 = Ct3-Alt3;
//      Alt3 = Ct3;

//      If (Val3 > 0)
//      {    
//         Dqw    = (Double)Val3*Config_idrun[2].Numturbo; // ñ�������� î���� *Glconfig.Idrun[2].Numturbo)
//       Persist_addqwcounttotal(2, Dqw); // ó��������� î����� Dqw
//       Aprun3->Vtot_l =Persist_getqwcounttotal(2); // î���� î����
//          
//        If (Interval[2] < 96)  // Â�
//          Dqw_3    = Dqw;  // ñ�������� ð���?�� 
//  //Ý� ø Â�
//        Aprun3->Qw_s = Dqw_3;
//        Prun3->Qw = (Double)3600*Dqw_3; //÷������ ð���?�� â ð�
//        If (Prun3->Qw > 1.2*Prun2->Qmax) Prun2->Qw = 1.2*Prun2->Qmax;  
//        If (Prun3->Qw > 0)
//         Kd = Pow(0.1/Prun3->Qw,0.1);            
//      }
//      Else 
//      {    Tr =  (Long Long)(T16 - Intr[2]) - (Long Long)Interval[2];
//          If (Tr > 0)  // î� ÿ������� ø������� ÿ����� â����� Interval�
//          {
//           If (Prun3->Qw > 0.2)
//            Prun3->Qw = Kd * Prun3->Qw;
//           Else  Prun3->Qw = 0.0;
//          } 
//      }          
}
//-------------------------------------------------------
Void Hal_rtc_alarmaeventcallback(Rtc_handletypedef *Hrtc)
{
Struct Bintime    *Pbintime;
    
    Ct1 = Count1; Ct2 = Count2; Ct3 = Count3;
     T16 = T_16;  // ç�������� ñ������ ó����� â í����� ñ������
    Pbintime = &Time_settime;
    Counttime = 0;
    T_17 = 0;
    Startr = 0xff;
   If (Time_getflagtime() == 1)
   { //  Startt=0;
    Sdate.Month =   Pbintime->Month;           //¬����
    Sdate.Date  =   Pbintime->Date;            //¤���
    Sdate.Year    = Pbintime->Year;           //¼��
    Stime.Hours   = Pbintime->Hours;          //ç��
    Stime.Minutes = Pbintime->Minutes;        //¬�����
    Stime.Seconds = Pbintime->Seconds;        //â������
//  Stime.Daylightsaving = Rtc_daylightsaving_none;
//  Stime.Storeoperation = Rtc_storeoperation_reset;
  If (Hal_rtc_settime(Hrtc, &Stime, Rtc_format_bin) != Hal_ok)
  {
    Error_handler();
  }
//  Sdate.Weekday = Rtc_weekday_friday;
  If (Hal_rtc_setdate(Hrtc, &Sdate, Rtc_format_bin) != Hal_ok)
  {
    Error_handler();
  }
   Copy_bintime = Time_settime;
   Bintime = Time_settime;
   Time_resflagtime();               // â�p�� ä���� ü�������� ¢p�����
 } //If (Time_getflagtime() == 1)
    Hal_rtc_gettime(Hrtc, &Stime, Rtc_format_bin);
    Hal_rtc_getdate(Hrtc, &Sdate, Rtc_format_bin);
//    Hal_rtc_getalarm(Hrtc, &Salarm, Rtc_alarm_a, Rtc_format_bin);  
    Bintime.Seconds = Stime.Seconds; 
    Bintime.Minutes = Stime.Minutes;
    Bintime.Hours = Stime.Hours;
    Bintime.Date = Sdate.Date;
    Bintime.Month = Sdate.Month;
    Bintime.Year = Sdate.Year;
 // ÿ������ ø�������� î� ñ�������� ü���
    Time_getcount();
    Uart1_watch();
}

//-------------------------------------------
Void Hal_tim_oc_delayelapsedcallback(Tim_handletypedef *Htim)
{
        
}
//-----------------------------------------------
Void Conf_p23k256(Spi_handletypedef *Hspi)
{	Uint8_t Write_cmd[]={0x01,0x41};  // Ç����� 01000001 â ð������
	Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_reset);
	Hal_spi_transmit(Hspi, Write_cmd, 2, Hal_max_delay);
	Hal_gpio_writepin(Gpiob, Gpio_pin_8, Gpio_pin_set);
}

//----------------------------------------------------------
Int Inqueue(Int N,Pf Func) 
{
Int K;
   If (N > Kqu-1)
      Return(-1);
   K= Longqueue[N]; 
//   Asm Cli
   If (Longqueue[N] == Maxq) {
//      Asm Sti
      Return(-2);
   }
   Queue[N][K] = Func;       // ®??P?¤? ??­???©
   Longqueue[N]++;
//   Asm Sti
   Return(0);  
}
Int Intimequeue(Unsigned Int Time,Int N,Pf Func) {
   Struct Tq  *Ukq;                             // ?? ��??«? ­� ?«?¬?­? ®??P?¤? ? ? ��?P?
   If (N > Kqu-1)
      Return(-1);
   If (Longtimequeue == Maxq)
      Return(-2);
   If (Time == 0)
      Return(-3);
//   Asm Cli
   Ukq = &Timequeue[Longtimequeue];
   Ukq->Time      = Time;                // ?P?¬?
   Ukq->Prioritet = N;                   // ?P?®p???? §���??
   Ukq->Task      = Func;                // ?? ��??«? ­� §���??
   Longtimequeue++;
//   Asm Sti
   Return(0);
}
//---------------------------------------------------------------
Int Inqueueint(Int N,Pf Func) {
   If (N > Kqu-1)
      Return(-1);
   If (Longqueue[N] == Maxq)
      Return(-2);                       // ??«? ®??P?¤? §�?®��?­�
   Queue[N][Longqueue[N]] = Func;       // ¬�???? ®??P?¤?© §���?
   Longqueue[N]++;
   Return(0);
}
//---------------------------------------------------------------
Void Timecorrection()  //ÿ�������� �?������ ÿ�� â��������
{ Char N_run;

   For(N_run = 0; N_run < Glconfig.Numrun; N_run++)
   {
         Struct Bintime _bintime = Bintime;    
                                        // ç������� â Tdata ÿ������������ ñ�������;
         Mem_rdtdata(Per,N_run, &Tdata);
//ñ�������������� ñ�������� â���� ä� ÿ�������������� ø������� ì������
         If (Glconfig.Period == 0)
            Glconfig.Period = 60;
         _bintime.Seconds = 0;
         _bintime.Minutes /= Glconfig.Period;
         _bintime.Minutes *= Glconfig.Period;
 //ÿ�� í����?��������, ÿ��������� ú�������� ÿ�������������� ø ÷������� ñ��������� 
 // ø ñ������ ç����� â ÿ������������ î����
         If (Memcmp(&Tdata.Starttime, &_bintime, Sizeof(Struct Bintime)))
         {
             Mem_startperiodhour(N_run);      // ú�������� ñ��������� ÿ�� ñ�����
          }
                                        // ç������� â Tdata ÷������ ñ�������
         Mem_rdtdata(Hou,N_run, &Tdata);
//ñ�������������� ñ�������� â���� ä� ÿ�������������� ø������� ì������
         _bintime.Seconds = 0;
         _bintime.Minutes = 0;
 //ÿ�� í����?��������, ÿ��������� ú�������� ÷������� ø ñ�������� ñ��������� 
 // ø ñ������ ç����� â ÷������ î����
         If (Memcmp(&Tdata.Starttime, &_bintime, Sizeof(Struct Bintime))) 
         {
         Mem_starthourday(N_run); }        // ú�������� ñ��������� ÿ�� ñ�����
                                        //  ç������� â Tdata ñ������� ñ�������
         Mem_rdtdata(Day, N_run, &Tdata);
//ñ�������������� ñ�������� â���� ä� ÿ�������������� ø������� ì������
// í���������  ñ�������? ä�����?       
//         Timecorrectionforday(&_bintime, Glconfig.Contrhour);
 //ÿ�� í����?��������, ÿ��������� ú�������� ñ�������� ø ì������������������ 
 // ø ñ������ ç����� â ñ������� î���� 
         If (Memcmp(&Tdata.Starttime, &_bintime, Sizeof(Struct Bintime)))
            {
					// ú�������� ñ��������� ÿ�� ñ�����
            Mem_startdaymonth(Power_off_time, N_run);
         }
      }
}
//-----------------------------------------------
Void Task_start_calc(Void)
{
      If ((Config_idrun[Kt].Typerun == 4)
          || (Config_idrun[Kt].Typerun == 5)
          || (Config_idrun[Kt].Typerun == 10))
      {// ÿ�������� ð������ î����� ü��� â Ñ�  ä�� ñ������� ü���
            Inqueue(2, Task_calc_corrsetcalcdata);
      } // If ((Config_idrun[K/].Typerun) == 4 || ...
      Else      // ä�������� ø ú����������� ð���?�������, â�������
      {
  // ÿ�������� ð������ ð���?��� ä�� ä��������      
         Inqueue(2, Tasksetcalcdata);
      }
// ç����� ø������� à��?���� â Ý��
   P23k256_write_data(&Hspi3,&Adrpointarch[1][0], (Uint8_t*)&Parchiv,
         Sizeof(Struct Pointarch));  
// Ç����� ñ������? ñ������� à��?���� â Ý��
    P23k256_write_data(&Hspi3,Adrerasebl,(Uint8_t*)Erasebl, Sizeof(Erasebl));
      Memcpy(&Lasttime,&Bintime,6);       // ó������ â���� -> â â���� â���������
}  // ú���� ö���� ÿ� út   
//------------------------------------------------------------
Void Calc_braket(Void)
{
    Nrnz = Time_getnumrun();
    Braket1(Xi);
//    Braket(Xi);
    Brz[Nrnz]++;    
    Inqueue(2,Dzofpt_b);
}
/*Void Calc_braket2(Void)
{
    Inqueue(2,Dzofpt_b);  
    
}
*/
//------------------------------------------------------------
Void Calcfhp(Void)
{    Int Nc,N;
    
     Nrnz = Time_getnumrun();
//   Ð������� î�������� í����? Ô��
     If ((Config_idrun[Nrnz].Nkcompress==3)&&((Brz[Nrnz]==0)|| (Calcbr[Nrnz]))&&(Nrnz < Glconfig.Numrun))   //Aga8
     { // ÿ������� ú���������� ø� Xj â Xi
        Nc=0;
        For (N=0; N<21; N++)
        {  Xi[N] = 0.0;
// â�?����� ì����� ñ��������� ú����������
//Printf("Xj=%F ",Xj[Nrnz][N]);
           If (Xj[Nrnz][N] != 0.0)
           { Nc++;
             Cid[Nc-1] = N;
             Xi[Nc-1]  = Xj[Nrnz][N];
//Printf("Xi=%F ",Xi[Nc-1]);
           }
        }
//Printf("Braket\N");
// â���� ô������ ÿ�������������� î�������� Ô��
//Printf("Nrnz=%D\N",Nrnz);
	   Z_in[Nrnz] = 1;
           If (Calcbr[Nrnz]) Z_in[Nrnz] = 2;
           Calcbr[Nrnz] = 0;
           Nrnb=Nrnz;
           Nrnzc = Nrnz;
           Ncc = Nc;
           Inqueue(2,Calc_braket);     // Xi â���� ø� ú�����������
     }    //If ((Config_idrun[Nrnz].Nkcompress==3)
     Else Inqueue(2,Task_start_calc);
}
//---------------------------------------------------------------------------
Void Task_start(Void)
{
    If (Kt == Glconfig.Numrun)  Kt = 0;
    Glnumrun = Kt;
//  Printf("Zapusk Calc Kt=%D Z_in=%D\N",Kt,Z_in[Kt]);
     Nrnz = Kt;
     If ((Config_idrun[Kt].Nkcompress==3) && ((Z_in[Kt] == 1) || (Z_in[Kt] == 2)
          || (Calcbr[Kt]==1)))
      Inqueue(2,Calcfhp);
     Else
      Inqueue(2,Task_start_calc);
}
//---------------------------------------------------------------------------
Void Settasktakt(Void) {                    // ç���������� â í����� ñ������
//***************************************************
     Nrnz = 0;
    Kt=0;
Sprintf((Char*)Str90,"Nrun=%D T_17=%D\N\R",Kt,T_17);
Hal_gpio_writepin(Gpiod, Gpio_pin_4, Gpio_pin_set);   //Rts Usart1 Set
Hal_uart_transmit(&Huart2,(Uint8_t*)Str90,Sizeof(Str90),1000);
   Memcpy(&Adcfg->Lasttime,&Bintime,6); // ç��������� ó������ â���� (â���������)
  If (Bbegincalc)  // 1-é ð�� ÿ��������� 
  { //í� 2-é ñ������ ÿ����
    If (Bbegincalc == 1)
    { Bbegincalc = 2;   
      Memcpy(&Adcfg->Lasttime,&Power_off_time,6); // ç��������� â���� â���������
      P23k256_write_data(&Hspi3,Adraddcfg,(Uint8_t*)Adcfg,Sizeadcfg); // ç����� ä�� ÿ��������� 
    }    
  }   
  Else Bbegincalc = 1; 
   If (Printform.Ligt) {
      Printform.Ligt--;
      If (Printform.Ligt == 0)       // âp��� ñ������� ø�������p�
         Lcdresligt();
   }
     If ((In_but1 < Butmax)&&(In_but2 < Butmax))
        Glprint();    
   
     Uart1_watch();     
//    Uart2_watch()
// Ð����� ð���?��� ø î����� ü���
    Inqueue(2,Task_start); 
}
//---------------------------------------------------------------------------
Int Time_getnumrun(Void) {                  // ÷����� ó������� í���p� í����
/*    If (Glconfig.Numrun == 1) Glnumrun = 0;
   Else If (Glconfig.Numrun == 2) Glnumrun = Counttime / 20;
   If (Glconfig.Numrun == 3)
  { If (Bintime.Seconds % 2 == 0)
   { Glnumrun = (Counttime-1) / 20;
   }
   Else  Glnumrun = 2;
   }
*/ Glnumrun = Kt;
   If (Glnumrun == Glconfig.Numrun) Glnumrun = 0;
   Return Glnumrun;
}
//---------------------------------------------------------------------------

/* User Code End 0 */

/**
  * @Brief  The Application Entry Point.
  * @Retval Int
  */
Int Main(Void)
{
  /* User Code Begin 1 */
   Uint32_t _i = 0;
//   Uint8_t Streg1 = 0x00, Streg2 = 0x02;  // Qe = 1;
  Uint16_t I,J,K,N, N_run,Nruns,Cr[3];
   Uint16_t Crccfg[2],Crcint; 

    Idrn = Config_idrun;
    T_16 = 0;
    T_17= 0;
    Counttime = 0;  
   Startr = 0;
    Bpoweroff = 0;
    Bbegincalc = 0;
    Bdisp = 1;
       In_but1 = 0;
       In_but2 = 0;
       Setw1 = 0;
       Setw2 = 0;
      Flagw1 = 0; Flagw2 = 0;
      Delayw1 = 0;
      Delayw2 = 0;

    For (I=0; I<3; I++)
   {
    Interval[I]=0;
    Calcdata[I].Re = 1e6;
    Startre[I] = 1.0e6;
      Brz[I]=0;
      Told[I] = 0.0;
      Z_out[I] = 0;
      Calczc[I] = 0;
      Zc1[I] = 1;
      Calcbr[I] = 0;
      Z_in[I] = 1;
//      Aga_f[I] = 1;
      Firstc[I] = 0x0f;
      Firstt[I] = 0x0f;
      Nowritecycle[I] =0x0;
      Calc_corsetnoerr(I);
      Kolels[I]=0;
      Kolelm[I]=0;
      Kolelh[I]=0;
      Koleld[I]=0;
//      Numzap[I] = 0;
   }
       Txcpltdma4 = 0;
       Rxcpltdma4 = 0;
       Startflag = 0;
       Rxcpltusr3 = 0;
       Txcpltusr3 = 0;
       Resmodbus = 80;
      St_cntinireccom1 = 0;
      G_cntinireccom1 = 0;
      St_cntinireccom2 = 0;
      G_cntinireccom2 = 0;
      Nuzs = 0;
      Printform.Ligt = 15;
      Flag_hartservice = 0;
  /* User Code End 1 */

  /* Mcu Configuration--------------------------------------------------------*/

  /* Reset Of All Peripherals, Initializes The Flash Interface And The Systick. */
  Hal_init();

  /* User Code Begin Init */
	Dwt_init();
   Kolaga8=0;
   Systakt = 1;
   Sizecfg = Sizeof(Struct Glconfig);  //224 (224)
   Sizeidr = Sizeof(Struct Idrun); //448(448)  
   Sizeadcfg = Sizeof(Struct Addcfg);  //6 (32)
   Sizeadr = Sizeof(Struct Adrun); //160  (160)
   Sizeintpar = Sizeof(Struct Intpar);  //28  (32)
   Sizecalc = Sizeof(Struct Calcdata);  //344  (352)
   Sizefhp = Sizeof(Xj);  // 252;
   Adcfg = &Adconfig;
   Maincfg = &Glconfig;
//   Mx_gpio_init();
//  Mx_spi1_init();
//  Mx_spi2_init();
//  Mx_spi3_init();
 // Mx_crc_init();
 
  /* User Code End Init */

  /* Configure The System Clock */
  Systemclock_config();

  /* User Code Begin Sysinit */
  Hqspi.Instance = Quadspi;
  Hal_qspi_deinit(&Hqspi);
  Mx_quadspi_init();
  Hal_qspi_mspinit(&Hqspi);
  /* User Code End Sysinit */

  /* Initialize All Configured Peripherals */
  Mx_gpio_init();
  Mx_dma_init();
  Mx_rtc_init();
  Mx_tim17_init();
  Mx_usart1_uart_init();
  //Hart_start();
  Mx_usart2_uart_init();
  Mx_usart3_uart_init();
  Mx_quadspi_init();
  Mx_spi1_init();
  Mx_spi2_init();
//  Mx_fatfs_init();
  Mx_spi3_init();
  Mx_crc_init();
  Mx_tim16_init();
  /* User Code Begin 2 */


Conf_p23k256(&Hspi3);  // ó�������� ð������� ñ������ â ð���� (01)
   Config(); // ç������ í��������? ç�������
// ×����� î������� ú�����������   
   P23k256_read_data(&Hspi3,Adrcfg,(Uint8_t*)Maincfg,Sizecfg);   
   Crccfg[0] =  Mem_crc16((Uint8_t*)Maincfg,Sizecfg-4);
   Flagspeed1 = 0;
   Inicfgspeed1();  
   //Flagspeed2 = 1;
//   Inicfgspeed2();  
   If (Maincfg->Crc != Crccfg[0])
   {// Sprintf(Str90,"Ú��������� ñ���� î��. ú����������� í������ Crc=%04x Crccfg=%04x\N\R Ñ������ ð�������� ú�����������\N\R",Maincfg->Crc,Crccfg[0]);
       Sprintf((Char*)Str90,"Crc Main Config Invalid! Crc=%04x Crccfg=%04x Read Reserve Main Config\N\R",Maincfg->Crc,Crccfg[0]);
       Hal_uart_transmit(&Huart2,(Uint8_t*)Str90,Strlen((Char*)Str90),1000);
   }
   For (N=0; N<3; N++)
   {
     Padr = &Adridrun[N][0];
     P23k256_read_data(&Hspi3,Padr,(Uint8_t*)&Config_idrun[N],Sizeidr);  
     Cr[N] = Mem_crc16((Uint8_t*)&Config_idrun[N],Sizeidr-2);
     If (Cr[N] != Config_idrun[N].Crc)
     { Sprintf((Char*)Str90,"Crc Run %D Config Invalid! Crc=%04x Cr[%D]=%04x Read Reserve Config\N\R",N+1,(Idrn+N)->Crc, N+1, Cr[N]);
       Hal_uart_transmit_it(&Huart2,Str90,Strlen((Char*)Str90));
     }
   }
//   Uint32_t Ks = Setcomspeed((Config_idrun[1].Kuzs & 0x7f) + 3);
//   Setspeeduzs(1);
  
   Mx_usart3_uart_init(); // Rs-485 Ó��
  
// ×����� ä������������� ú����������� Adconfig î���� ÷����
   P23k256_read_data(&Hspi3,Adraddcfg,(Uint8_t*)Adcfg,Sizeadcfg);  
   Memcpy(&Lasttime,&Adcfg->Lasttime,6);
   Memcpy(&Power_off_time,&Lasttime,6);
   Crccfg[1] = Mem_crc16((Uint8_t*)Adcfg,Sizeof(Struct Addcfg)-2);
// ×����� ä������������� ú����������� ÿ� í����� Config_adrun[N]
   For (N=0; N<3; N++)
   {
     Padr = &Adradrun[N][0];
     P23k256_read_data(&Hspi3,Padr,(Uint8_t*)&Config_adrun[N],Sizeadr);  
     Cr[N] = Mem_crc16((Uint8_t*)&Config_adrun[N],Sizeof(Struct Adrun)-2);       
   }



// ×����� â���������? ÿ���������
   P23k256_read_data(&Hspi3,Adrintp, (Uint8_t*)&Intpar, Sizeintpar);   
   Crcint =  Mem_crc16(&Intpar.Hartnumcyclrep,Sizeintpar-4);
   If (Intpar.Crc != Crcint)
   { Sprintf((Char*)Str90,"Crc Intparam Invalid! Crc=%04x Crcint=%04x Read Reserve Intparam\N\R",Intpar.Crc,Crcint);
     Hal_uart_transmit_it(&Huart2,(Uint8_t*)Str90,Strlen((Char*)Str90)); 
   }

// ×����� ð��������? ä�����? Calcdata   
//   For (N=0; N<3; N++)
//   {
//     P23k256_read_data(&Hspi3,&Adrcalc[N][0], (Uint8_t*)&Calcdata[N], Sizecalc);   
//   }
// ------------ î������ ----------------
   Numzap[2] = Calcdata[2].H;
   /* Initialize All Configured Peripherals */

    Liquidcrystal(Gpiod, D0_pin, D1_pin, D2_pin, D3_pin, D4_pin, D5_pin, D6_pin,
	D7_pin);

//Hal_gpio_writepin(Gpioc, Card_cs_pin, Gpio_pin_set);


   
   _sectakt = 0;
    Lcom4inthf = 0;  // Modbus ô���
    Setrs485dataadr();  
     Hal_gpio_writepin(Gpiod, Led_pin, Gpio_pin_set); // ÿ��������� ä������ Â��
//     Printform.Ligt = 60; 
    Rs485num = 0;
//  Setrs485fbdataadr();
    If (Rs485num > 0) //ô��� ä������ ÿ� Rs485 ÿ� ÿ���� Hart3 (Usart3)
    {
//Printf("Rs485com4ini\N");
      Sp1=0x00e3;
      Rs485com4inihf(); //ø������������ î����� ä�������
      Rs485_setrs485err();                 // ü�������� ¢ â�������� ®�����
      Rs485ini();    //î���� Ð�� ÿ� Rs-485�
    }  
//**************************   
//   For (N=0; N<3; N++) Init_parchiv(N);
//       //ç����� ø������� à��?���� ÿ���� ø������������
//    P23k256_write_data(&Hspi3,&Adrpointarch[1][0], (Uint8_t*)&Parchiv,
//         Sizeof(Struct Pointarch));   
//    P23k256_write_data(&Hspi3,Adrerasebl,(Uint8_t*)Erasebl, Sizeof(Erasebl));
//*****************************************

// ×����� �?����������� Ô��   
   For (N=0; N<3; N++)
   {
    If (Config_idrun[N].Nkcompress==3)
     Kolaga8++;
   }
   If (Kolaga8 > 0)
   { Initialdata(); // ç��������� ì������� ì����� Aga8
// ×����� Ô�� 
    P23k256_read_data(&Hspi3,Adrfhp,(Uint8_t*)&Xj,Sizefhp);   
   }   

       For (J=0; J<3; J++)
       { Zcks[J].K = Zck1[J][0];    Zcks[J].Z = Zck1[J][1];
         Calcdata[J].K=Zcks[J].K; Calcdata[J].Z=Zcks[J].Z;
         Numchrom[J] = Numchrom1[J];
       }
   Calibrateini();                      // Ç����� ø��������������� ó������
    Inindex1 = 0; 
    Ini_reciv1();
//    Ini_reciv2();
      
       // ñ�p�� î����� üp���� ì������ P������
   Calc_setnoerr();
   Calc_setnoerrtypeotb();                                      // Ð�������� Þ�����
			// î�������� ñ�������� à����� ø ÷����� ì��. ø��������
   If (Glconfig.Edizm_t.T_su==1) T_st= 273.15;     //0 ü�.Ö ø��  Gerg88
   Else If (Glconfig.Edizm_t.T_su==2)  T_st= 288.15;  // 15 ü�. Ö
   Else  T_st= 293.15;           // 20 ü�. Ö
     Nruns=Glconfig.Numrun; // ÷���� í����
     Rs485mb = 0;
  For (I=0; I < Glconfig.Numrun; I++)
 {    Rs485hf[I] = 0;
     If ((Config_idrun[I].Typerun == 4)
     || (Config_idrun[I].Typerun == 5)
     || (Config_idrun[I].Typerun == 10))
      If ((Config_idrun[I].Typecounter > 0) && (Config_idrun[I].Addrmodbus > 0))
       { Rs485hf[I] = 1; // ???? ? ??®�-???????? ?® Rs485
	     Rs485mb++; // = 1;
       }
 }
 If (Rs485mb) { Mrun=0; Zapuskuzs();}
 
    For (I=0; I< Kqu; I++)
     Longqueue[I] = 0;
   I = 0;
   Fc = 0;

  Uint8_t Takt;
  Takt=0;
  Uint32_t K2 = 0;
  For (I=0; I< Glconfig.Numrun; I++)
  { B_massomer[I]=(Config_idrun[I].Typerun==8    //Rotamass
      || Config_idrun[I].Typerun==9);
    B_counter[I] = (((Config_idrun[I].Typerun) == 4) ||   //Ñ������
       ((Config_idrun[I].Typerun) == 5) ||  ((Config_idrun[I].Typerun) == 10));
  }
  Hal_tim_base_start_it(&Htim17);
  Hal_tim_base_start_it(&Htim16);
//  Hal_rtc_setalarm_it(&Hrtc, &Salarm, Rtc_format_bin);
  //While (Startr == 0); // î������� 1-é ñ������ ñ����������� Rtc

Uint32_t T1,T2,Tpause;
Uint8_t D;

  Intr[0] = T_16; Intr[1] = T_16; Intr[2] = T_16;
  If   (Bintime.Date < Lasttime.Date) 
        D = Bintime.Date + 31 ;
  Else  D = Bintime.Date;
  T2= (Long)Lasttime.Date*86400l + (Long)Lasttime.Hours*3600l+(Long)(Lasttime.Minutes*60)+(Long)Lasttime.Seconds;
  T1= (Long)D*86400l +(Long)Bintime.Hours*3600l+(Long)(Bintime.Minutes*60)+(Long)Bintime.Seconds;
  Tpause=T1-T2;
  If (Tpause > 30)
  { // â��������� ÿ������   
      Bpoweroff = 1;
  } 
  Else    // ü������ ñ����
  {
      Bpoweroff = 0;
  }
      If (Bpoweroff==1)
      { // â��������� ÿ������   
       Mem_alarmsetpoweroff();
       Mem_alarmsetpoweron();        
      } 
      Else    // ü������ ñ����
      {
       Mem_alarmsetstop();
       Mem_alarmsetstart();
      }
   If (Rs485mb > 0)
     { Startuzs(Bpoweroff);
     }  
  // Ú�������� ñ��������� ÿ�� â�������� (ÿ�������� �?������)
 //   Timecorrection();
   Initsensor();
  /* User Code End 2 */

  /* Infinite Loop */
  /* User Code Begin While */
  While (1)
  {
		Hal_gpio_writepin(Gpiod, Led_pin, Gpio_pin_set); // п������?��� д�����?� В��
     A = Longqueue[I];
      If (A)
      {
         Ukq = &Queue[I][0]; 
         Worktask = *Ukq;
         A--;
	     Longqueue[I] = A; 
         If (A)
            Memmove(Ukq,Ukq+1,Sizeof(Pf) * A);
         Queue[I][A] = 0; 
	     Worktask();     // ???®��?­?? ??­???? 
	     I = 0;
      }
      Else
         I = (I < (Kqu-1))? (I + 1) : 0 ;

      If (Counttime > 2) Fc = 0;
      If  ((Counttime < 3) && (Fc == 0))
    {      Inqueue(2,Settasktakt);       
        Fc = 1;                      
    }   

    /* User Code End While */

    /* User Code Begin 3 */
	  Runsensor();
	  //Hal_delay(500);
  }  // End While(1)
 
  /* User Code End 3 */
}

/**
  * @Brief System Clock Configuration
  * @Retval None
  */
Void Systemclock_config(Void)
{
  Rcc_oscinittypedef Rcc_oscinitstruct = {0};
  Rcc_clkinittypedef Rcc_clkinitstruct = {0};
  Rcc_periphclkinittypedef Periphclkinit = {0};

  /** Configure Lse Drive Capability 
  */
  Hal_pwr_enablebkupaccess();
  __hal_rcc_lsedrive_config(Rcc_lsedrive_low);
  /** Initializes The Cpu, Ahb And Apb Busses Clocks 
  */
  Rcc_oscinitstruct.Oscillatortype = Rcc_oscillatortype_hse|Rcc_oscillatortype_lse;
  Rcc_oscinitstruct.Hsestate = Rcc_hse_on;
  Rcc_oscinitstruct.Lsestate = Rcc_lse_on;
  Rcc_oscinitstruct.Pll.Pllstate = Rcc_pll_on;
  Rcc_oscinitstruct.Pll.Pllsource = Rcc_pllsource_hse;
  Rcc_oscinitstruct.Pll.Pllm = 6;
  Rcc_oscinitstruct.Pll.Plln = 40;
  Rcc_oscinitstruct.Pll.Pllp = Rcc_pllp_div7;
  Rcc_oscinitstruct.Pll.Pllq = Rcc_pllq_div2;
  Rcc_oscinitstruct.Pll.Pllr = Rcc_pllr_div8;
  If (Hal_rcc_oscconfig(&Rcc_oscinitstruct) != Hal_ok)
  {
    Error_handler();
  }
  /** Initializes The Cpu, Ahb And Apb Busses Clocks 
  */
  Rcc_clkinitstruct.Clocktype = Rcc_clocktype_hclk|Rcc_clocktype_sysclk
                              |Rcc_clocktype_pclk1|Rcc_clocktype_pclk2;
  Rcc_clkinitstruct.Sysclksource = Rcc_sysclksource_pllclk;
  Rcc_clkinitstruct.Ahbclkdivider = Rcc_sysclk_div1;
  Rcc_clkinitstruct.Apb1clkdivider = Rcc_hclk_div1;
  Rcc_clkinitstruct.Apb2clkdivider = Rcc_hclk_div1;

  If (Hal_rcc_clockconfig(&Rcc_clkinitstruct, Flash_latency_1) != Hal_ok)
  {
    Error_handler();
  }
  Periphclkinit.Periphclockselection = Rcc_periphclk_rtc|Rcc_periphclk_usart1
                              |Rcc_periphclk_usart2|Rcc_periphclk_usart3;
  Periphclkinit.Usart1clockselection = Rcc_usart1clksource_pclk2;
  Periphclkinit.Usart2clockselection = Rcc_usart2clksource_pclk1;
  Periphclkinit.Usart3clockselection = Rcc_usart3clksource_pclk1;
  Periphclkinit.Rtcclockselection = Rcc_rtcclksource_lse;
  If (Hal_rccex_periphclkconfig(&Periphclkinit) != Hal_ok)
  {
    Error_handler();
  }
  /** Configure The Main Internal Regulator Output Voltage 
  */
  If (Hal_pwrex_controlvoltagescaling(Pwr_regulator_voltage_scale1) != Hal_ok)
  {
    Error_handler();
  }
}

/**
  * @Brief Crc Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_crc_init(Void)
{

  /* User Code Begin Crc_init 0 */

  /* User Code End Crc_init 0 */

  /* User Code Begin Crc_init 1 */

  /* User Code End Crc_init 1 */
  Hcrc.Instance = Crc;
  Hcrc.Init.Defaultpolynomialuse = Default_polynomial_enable;
  Hcrc.Init.Defaultinitvalueuse = Default_init_value_enable;
  Hcrc.Init.Inputdatainversionmode = Crc_inputdata_inversion_none;
  Hcrc.Init.Outputdatainversionmode = Crc_outputdata_inversion_disable;
  Hcrc.Inputdataformat = Crc_inputdata_format_bytes;
  If (Hal_crc_init(&Hcrc) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Crc_init 2 */

  /* User Code End Crc_init 2 */

}

/**
//---------------------------------
Void Setspeed_usart3(Uint32_t Ks)
{
  Huart3.Init.Baudrate = Ks; //1200;
  If (Hal_uart_init(&Huart3) != Hal_ok)
  {
    Error_handler();
  }    
}    

  * @Brief Quadspi Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_quadspi_init(Void)
{

  /* User Code Begin Quadspi_init 0 */

  /* User Code End Quadspi_init 0 */

  /* User Code Begin Quadspi_init 1 */

  /* User Code End Quadspi_init 1 */
  /* Quadspi Parameter Configuration*/
  Hqspi.Instance = Quadspi;
  Hqspi.Init.Clockprescaler = 0;
  Hqspi.Init.Fifothreshold = 4;
  Hqspi.Init.Sampleshifting = Qspi_sample_shifting_none;
  Hqspi.Init.Flashsize = 22;
  Hqspi.Init.Chipselecthightime = Qspi_cs_high_time_2_cycle;
  Hqspi.Init.Clockmode = Qspi_clock_mode_0;
  If (Hal_qspi_init(&Hqspi) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Quadspi_init 2 */

  /* User Code End Quadspi_init 2 */

}

/**
  * @Brief Rtc Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_rtc_init(Void)
{

  /* User Code Begin Rtc_init 0 */

  /* User Code End Rtc_init 0 */

//  Rtc_timetypedef Stime = {0};
//  Rtc_datetypedef Sdate = {0};
//  Rtc_alarmtypedef Salarm = {0};

  /* User Code Begin Rtc_init 1 */

  /* User Code End Rtc_init 1 */
  /** Initialize Rtc Only 
  */
  Hrtc.Instance = Rtc;
  Hrtc.Init.Hourformat = Rtc_hourformat_24;
  Hrtc.Init.Asynchprediv = 0;
  Hrtc.Init.Synchprediv = 32767;
  Hrtc.Init.Output = Rtc_output_disable;
  Hrtc.Init.Outputremap = Rtc_output_remap_none;
  Hrtc.Init.Outputpolarity = Rtc_output_polarity_high;
  Hrtc.Init.Outputtype = Rtc_output_type_opendrain;
  If (Hal_rtc_init(&Hrtc) != Hal_ok)
  {
    Error_handler();
  }

  /* User Code Begin Check_rtc_bkup */
    
  /* User Code End Check_rtc_bkup */

  /** Initialize Rtc And Set The Time And Date */
 /*
  Stime.Hours = 0;
  Stime.Minutes = 0;
  Stime.Seconds = 0;
  Stime.Daylightsaving = Rtc_daylightsaving_none;
  Stime.Storeoperation = Rtc_storeoperation_reset;
  If (Hal_rtc_settime(&Hrtc, &Stime, Rtc_format_bin) != Hal_ok)
  {
    Error_handler();
  }
  Sdate.Weekday = Rtc_weekday_wednesday;
  Sdate.Month = Rtc_month_january;
  Sdate.Date = 1;
  Sdate.Year = 20;

  If (Hal_rtc_setdate(&Hrtc, &Sdate, Rtc_format_bin) != Hal_ok)
  {
    Error_handler();
  }
*/  
  /** Enable The Alarm A */
  Salarm.Alarmtime.Hours = 0;
  Salarm.Alarmtime.Minutes = 0;
  Salarm.Alarmtime.Seconds = 1;
  Salarm.Alarmtime.Subseconds = 0;
  Salarm.Alarmtime.Daylightsaving = Rtc_daylightsaving_none;
  Salarm.Alarmtime.Storeoperation = Rtc_storeoperation_reset;
  Salarm.Alarmmask = Rtc_alarmmask_all;
  Salarm.Alarmsubsecondmask = Rtc_alarmsubsecondmask_all;
  Salarm.Alarmdateweekdaysel = Rtc_alarmdateweekdaysel_date;
  Salarm.Alarmdateweekday = 1;
  Salarm.Alarm = Rtc_alarm_a;
  If (Hal_rtc_setalarm_it(&Hrtc, &Salarm, Rtc_format_bin) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Rtc_init 2 */

  /* User Code End Rtc_init 2 */

}

/**
  * @Brief Spi1 Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_spi1_init(Void)
{

  /* User Code Begin Spi1_init 0 */

  /* User Code End Spi1_init 0 */

  /* User Code Begin Spi1_init 1 */

  /* User Code End Spi1_init 1 */
  /* Spi1 Parameter Configuration*/
  Hspi1.Instance = Spi1;
  Hspi1.Init.Mode = Spi_mode_master;
  Hspi1.Init.Direction = Spi_direction_2lines;
  Hspi1.Init.Datasize = Spi_datasize_8bit;
  Hspi1.Init.Clkpolarity = Spi_polarity_high;
  Hspi1.Init.Clkphase = Spi_phase_2edge;
  Hspi1.Init.Nss = Spi_nss_soft;
  Hspi1.Init.Baudrateprescaler = Spi_baudrateprescaler_16;
  Hspi1.Init.Firstbit = Spi_firstbit_msb;
  Hspi1.Init.Timode = Spi_timode_disable;
  Hspi1.Init.Crccalculation = Spi_crccalculation_disable;
  Hspi1.Init.Crcpolynomial = 7;
  Hspi1.Init.Crclength = Spi_crc_length_datasize;
  Hspi1.Init.Nsspmode = Spi_nss_pulse_disable;
  If (Hal_spi_init(&Hspi1) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Spi1_init 2 */

  /* User Code End Spi1_init 2 */

}

/**
  * @Brief Spi2 Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_spi2_init(Void)
{

  /* User Code Begin Spi2_init 0 */

  /* User Code End Spi2_init 0 */

  /* User Code Begin Spi2_init 1 */

  /* User Code End Spi2_init 1 */
  /* Spi2 Parameter Configuration*/
  Hspi2.Instance = Spi2;
  Hspi2.Init.Mode = Spi_mode_master;
  Hspi2.Init.Direction = Spi_direction_2lines;
  Hspi2.Init.Datasize = Spi_datasize_8bit;
  Hspi2.Init.Clkpolarity = Spi_polarity_low;
  Hspi2.Init.Clkphase = Spi_phase_1edge;
  Hspi2.Init.Nss = Spi_nss_hard_output;
  Hspi2.Init.Baudrateprescaler = Spi_baudrateprescaler_2;
  Hspi2.Init.Firstbit = Spi_firstbit_msb;
  Hspi2.Init.Timode = Spi_timode_disable;
  Hspi2.Init.Crccalculation = Spi_crccalculation_disable;
  Hspi2.Init.Crcpolynomial = 7;
  Hspi2.Init.Crclength = Spi_crc_length_datasize;
  Hspi2.Init.Nsspmode = Spi_nss_pulse_disable;
  If (Hal_spi_init(&Hspi2) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Spi2_init 2 */

  /* User Code End Spi2_init 2 */

}

/**
  * @Brief Spi3 Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_spi3_init(Void)
{

  /* User Code Begin Spi3_init 0 */

  /* User Code End Spi3_init 0 */

  /* User Code Begin Spi3_init 1 */

  /* User Code End Spi3_init 1 */
  /* Spi3 Parameter Configuration*/
  Hspi3.Instance = Spi3;
  Hspi3.Init.Mode = Spi_mode_master;
  Hspi3.Init.Direction = Spi_direction_2lines;
  Hspi3.Init.Datasize = Spi_datasize_8bit;
  Hspi3.Init.Clkpolarity = Spi_polarity_low;
  Hspi3.Init.Clkphase = Spi_phase_1edge;
  Hspi3.Init.Nss = Spi_nss_soft;
  Hspi3.Init.Baudrateprescaler = Spi_baudrateprescaler_8;
  Hspi3.Init.Firstbit = Spi_firstbit_msb;
  Hspi3.Init.Timode = Spi_timode_disable;
  Hspi3.Init.Crccalculation = Spi_crccalculation_disable;
  Hspi3.Init.Crcpolynomial = 7;
  Hspi3.Init.Crclength = Spi_crc_length_datasize;
  Hspi3.Init.Nsspmode = Spi_nss_pulse_enable;
  If (Hal_spi_init(&Hspi3) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Spi3_init 2 */

  /* User Code End Spi3_init 2 */

}

/**
  * @Brief Tim16 Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_tim16_init(Void)
{

  /* User Code Begin Tim16_init 0 */

  /* User Code End Tim16_init 0 */

  /* User Code Begin Tim16_init 1 */

  /* User Code End Tim16_init 1 */
  Htim16.Instance = Tim16;
  Htim16.Init.Prescaler = 20;
  Htim16.Init.Countermode = Tim_countermode_up;
  Htim16.Init.Period = 952;
  Htim16.Init.Clockdivision = Tim_clockdivision_div1;
  Htim16.Init.Repetitioncounter = 0;
  Htim16.Init.Autoreloadpreload = Tim_autoreload_preload_disable;
  If (Hal_tim_base_init(&Htim16) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Tim16_init 2 */

  /* User Code End Tim16_init 2 */

}

/**
  * @Brief Tim17 Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_tim17_init(Void)
{

  /* User Code Begin Tim17_init 0 */

  /* User Code End Tim17_init 0 */

  /* User Code Begin Tim17_init 1 */

  /* User Code End Tim17_init 1 */
  Htim17.Instance = Tim17;
  Htim17.Init.Prescaler = 20;
  Htim17.Init.Countermode = Tim_countermode_up;
  Htim17.Init.Period = 4760;
  Htim17.Init.Clockdivision = Tim_clockdivision_div1;
  Htim17.Init.Repetitioncounter = 0;
  Htim17.Init.Autoreloadpreload = Tim_autoreload_preload_disable;
  If (Hal_tim_base_init(&Htim17) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Tim17_init 2 */

  /* User Code End Tim17_init 2 */

}

/**
  * @Brief Usart1 Initialization Function
  * @Param None
  * @Retval None
  */
 Void Mx_usart1_uart_init(Void)
{

  /* User Code Begin Usart1_init 0 */
  //If (Glconfig.Speed_1 > 7) Glconfig.Speed_1 = 5;
  /* User Code End Usart1_init 0 */

  /* User Code Begin Usart1_init 1 */

  /* User Code End Usart1_init 1 */
  Huart1.Instance = Usart1;

  Huart1.Init.Baudrate = 38400;//115200;//1200
If (Flag_hartservice == 1)    
{  Huart1.Init.Wordlength = Uart_wordlength_9b;
  Huart1.Init.Parity = Uart_parity_odd;   
}
Else
{ Huart1.Init.Wordlength = Uart_wordlength_8b;//9b
  Huart1.Init.Parity = Uart_parity_none; //Odd;
}  
  Huart1.Init.Stopbits = Uart_stopbits_1;
  
  Huart1.Init.Mode = Uart_mode_tx_rx;
  Huart1.Init.Hwflowctl = Uart_hwcontrol_none;
  Huart1.Init.Oversampling = Uart_oversampling_16;
  Huart1.Init.Onebitsampling = Uart_one_bit_sample_disable;
  Huart1.Advancedinit.Advfeatureinit = Uart_advfeature_no_init;
  If (Hal_uart_init(&Huart1) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Usart1_init 2 */

  /* User Code End Usart1_init 2 */

}

/**
  * @Brief Usart2 Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_usart2_uart_init(Void)
{

  /* User Code Begin Usart2_init 0 */

  /* User Code End Usart2_init 0 */

  /* User Code Begin Usart2_init 1 */

  /* User Code End Usart2_init 1 */
  Huart2.Instance = Usart2;
  Huart2.Init.Baudrate = 115200;
  Huart2.Init.Wordlength = Uart_wordlength_8b;
  Huart2.Init.Stopbits = Uart_stopbits_1;
  Huart2.Init.Parity = Uart_parity_none;
  Huart2.Init.Mode = Uart_mode_tx_rx;
  Huart2.Init.Hwflowctl = Uart_hwcontrol_none;
  Huart2.Init.Oversampling = Uart_oversampling_16;
  Huart2.Init.Onebitsampling = Uart_one_bit_sample_disable;
  Huart2.Advancedinit.Advfeatureinit = Uart_advfeature_no_init;
  If (Hal_uart_init(&Huart2) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Usart2_init 2 */

  /* User Code End Usart2_init 2 */

}

/**
  * @Brief Usart3 Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_usart3_uart_init(Void)
{

  /* User Code Begin Usart3_init 0 */

  /* User Code End Usart3_init 0 */

  /* User Code Begin Usart3_init 1 */

  /* User Code End Usart3_init 1 */
  Huart3.Instance = Usart3;
  Huart3.Init.Baudrate = 115200;
  Huart3.Init.Wordlength = Uart_wordlength_8b;
  Huart3.Init.Stopbits = Uart_stopbits_1;
  Huart3.Init.Parity = Uart_parity_none;
  Huart3.Init.Mode = Uart_mode_tx_rx;
  Huart3.Init.Hwflowctl = Uart_hwcontrol_none;
  Huart3.Init.Oversampling = Uart_oversampling_16;
  Huart3.Init.Onebitsampling = Uart_one_bit_sample_disable;
  Huart3.Advancedinit.Advfeatureinit = Uart_advfeature_no_init;
  If (Hal_uart_init(&Huart3) != Hal_ok)
  {
    Error_handler();
  }
  /* User Code Begin Usart3_init 2 */

  /* User Code End Usart3_init 2 */

}

/** 
  * Enable Dma Controller Clock
  */
Static Void Mx_dma_init(Void) 
{

  /* Dma Controller Clock Enable */
  __hal_rcc_dma1_clk_enable();

  /* Dma Interrupt Init */
  /* Dma1_channel2_irqn Interrupt Configuration */
  Hal_nvic_setpriority(Dma1_channel2_irqn, 0, 0);
  Hal_nvic_enableirq(Dma1_channel2_irqn);
  /* Dma1_channel3_irqn Interrupt Configuration */
  Hal_nvic_setpriority(Dma1_channel3_irqn, 0, 0);
  Hal_nvic_enableirq(Dma1_channel3_irqn);
  /* Dma1_channel6_irqn Interrupt Configuration */
  Hal_nvic_setpriority(Dma1_channel6_irqn, 0, 0);
  Hal_nvic_enableirq(Dma1_channel6_irqn);
  /* Dma1_channel7_irqn Interrupt Configuration */
  Hal_nvic_setpriority(Dma1_channel7_irqn, 0, 0);
  Hal_nvic_enableirq(Dma1_channel7_irqn);

}

/**
  * @Brief Gpio Initialization Function
  * @Param None
  * @Retval None
  */
Static Void Mx_gpio_init(Void)
{
  Gpio_inittypedef Gpio_initstruct = {0};

  /* Gpio Ports Clock Enable */
  __hal_rcc_gpioe_clk_enable();
  __hal_rcc_gpioc_clk_enable();
  __hal_rcc_gpioh_clk_enable();
  __hal_rcc_gpioa_clk_enable();
  __hal_rcc_gpiob_clk_enable();
  __hal_rcc_gpiod_clk_enable();

  /*Configure Gpio Pin Output Level */
  Hal_gpio_writepin(Gpioa, Filtr_pin|Adc3_pin|Adc2_pin|Adc1_pin 
                          |E_lcd_pin|U1_rts_pin, Gpio_pin_reset);

  /*Configure Gpio Pin Output Level */
  Hal_gpio_writepin(Gpiob, K1_out_pin|U3_rts_pin|K2_out_pin|Spi3_cs_pin, Gpio_pin_reset);

  /*Configure Gpio Pin Output Level */
  Hal_gpio_writepin(Gpioe, K3_out_pin|K4_out_pin, Gpio_pin_reset);

  /*Configure Gpio Pin Output Level */
  Hal_gpio_writepin(Gpiod, D0_pin|D1_pin|D2_pin|D3_pin 
                          |D4_pin|D5_pin|D6_pin|D7_pin, Gpio_pin_set);

  /*Configure Gpio Pin Output Level */
  Hal_gpio_writepin(Gpioc, Card_cs_pin|Card_vcc_pin, Gpio_pin_reset);

  /*Configure Gpio Pin Output Level */
  Hal_gpio_writepin(Rs_lcd_gpio_port, Rs_lcd_pin, Gpio_pin_set);

  /*Configure Gpio Pin Output Level */
  Hal_gpio_writepin(Gpiod, Led_pin|U2_rts_pin, Gpio_pin_reset);

  /*Configure Gpio Pins : Fl_2_exti_4_pin Fl_1_exti_5_pin */
  Gpio_initstruct.Pin = Fl_2_exti_4_pin|Fl_1_exti_5_pin;
  Gpio_initstruct.Mode = Gpio_mode_it_rising;
  Gpio_initstruct.Pull = Gpio_nopull;
  Hal_gpio_init(Gpioe, &Gpio_initstruct);

  /*Configure Gpio Pins : Filtr_pin Adc3_pin Adc2_pin Adc1_pin 
                           U1_rts_pin */
  Gpio_initstruct.Pin = Filtr_pin|Adc3_pin|Adc2_pin|Adc1_pin 
                          |U1_rts_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_nopull;
  Gpio_initstruct.Speed = Gpio_speed_freq_low;
  Hal_gpio_init(Gpioa, &Gpio_initstruct);

  /*Configure Gpio Pins : K1_out_pin U3_rts_pin K2_out_pin */
  Gpio_initstruct.Pin = K1_out_pin|U3_rts_pin|K2_out_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_nopull;
  Gpio_initstruct.Speed = Gpio_speed_freq_low;
  Hal_gpio_init(Gpiob, &Gpio_initstruct);

  /*Configure Gpio Pins : K3_out_pin K4_out_pin */
  Gpio_initstruct.Pin = K3_out_pin|K4_out_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_nopull;
  Gpio_initstruct.Speed = Gpio_speed_freq_low;
  Hal_gpio_init(Gpioe, &Gpio_initstruct);

  /*Configure Gpio Pins : K1_in_pin Check_220_pin */
  Gpio_initstruct.Pin = K1_in_pin|Check_220_pin;
  Gpio_initstruct.Mode = Gpio_mode_input;
  Gpio_initstruct.Pull = Gpio_nopull;
  Hal_gpio_init(Gpioe, &Gpio_initstruct);

  /*Configure Gpio Pins : K2_in_pin K3_in_pin K4_in_pin */
  Gpio_initstruct.Pin = K2_in_pin|K3_in_pin|K4_in_pin;
  Gpio_initstruct.Mode = Gpio_mode_input;
  Gpio_initstruct.Pull = Gpio_nopull;
  Hal_gpio_init(Gpiob, &Gpio_initstruct);

  /*Configure Gpio Pins : D0_pin D1_pin D2_pin D3_pin 
                           D4_pin D5_pin D6_pin D7_pin */
  Gpio_initstruct.Pin = D0_pin|D1_pin|D2_pin|D3_pin 
                          |D4_pin|D5_pin|D6_pin|D7_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_pullup;
  Gpio_initstruct.Speed = Gpio_speed_freq_low;
  Hal_gpio_init(Gpiod, &Gpio_initstruct);

  /*Configure Gpio Pins : Card_cs_pin Card_vcc_pin */
  Gpio_initstruct.Pin = Card_cs_pin|Card_vcc_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_nopull;
  Gpio_initstruct.Speed = Gpio_speed_freq_low;
  Hal_gpio_init(Gpioc, &Gpio_initstruct);

  /*Configure Gpio Pin : Card_pin */
  Gpio_initstruct.Pin = Card_pin;
  Gpio_initstruct.Mode = Gpio_mode_input;
  Gpio_initstruct.Pull = Gpio_nopull;
  Hal_gpio_init(Card_gpio_port, &Gpio_initstruct);

  /*Configure Gpio Pin : Rs_lcd_pin */
  Gpio_initstruct.Pin = Rs_lcd_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_nopull;
  Gpio_initstruct.Speed = Gpio_speed_freq_high;
  Hal_gpio_init(Rs_lcd_gpio_port, &Gpio_initstruct);

  /*Configure Gpio Pin : E_lcd_pin */
  Gpio_initstruct.Pin = E_lcd_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_pullup;
  Gpio_initstruct.Speed = Gpio_speed_freq_high;
  Hal_gpio_init(E_lcd_gpio_port, &Gpio_initstruct);

  /*Configure Gpio Pins : Led_pin U2_rts_pin */
  Gpio_initstruct.Pin = Led_pin|U2_rts_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_nopull;
  Gpio_initstruct.Speed = Gpio_speed_freq_low;
  Hal_gpio_init(Gpiod, &Gpio_initstruct);

  /*Configure Gpio Pin : Gercon_pin */
  Gpio_initstruct.Pin = Gercon_pin;
  Gpio_initstruct.Mode = Gpio_mode_input;
  Gpio_initstruct.Pull = Gpio_nopull;
  Hal_gpio_init(Gercon_gpio_port, &Gpio_initstruct);

  /*Configure Gpio Pin : Spi3_cs_pin */
  Gpio_initstruct.Pin = Spi3_cs_pin;
  Gpio_initstruct.Mode = Gpio_mode_output_pp;
  Gpio_initstruct.Pull = Gpio_nopull;
  Gpio_initstruct.Speed = Gpio_speed_freq_very_high;
  Hal_gpio_init(Spi3_cs_gpio_port, &Gpio_initstruct);

  /**/
  Hal_i2cex_enablefastmodeplus(I2c_fastmodeplus_pb8);

  /* Exti Interrupt Init*/
  Hal_nvic_setpriority(Exti4_irqn, 0, 0);
  Hal_nvic_enableirq(Exti4_irqn);

  Hal_nvic_setpriority(Exti9_5_irqn, 0, 0);
  Hal_nvic_enableirq(Exti9_5_irqn);

}

/* User Code Begin 4 */
//------------------------------------------------------
Static Void Qspi_autopollingmemready(Qspi_handletypedef *Hqspi,Uint32_t Timeout)
{
  Qspi_commandtypedef     Scommand;
  Qspi_autopollingtypedef Sconfig;
  
 /* Configure Automatic Polling Mode To Wait For Memory Ready ------ */  
  Scommand.Instructionmode   = Qspi_instruction_1_line;
  Scommand.Instruction       = W25x_readstatusreg1;
  Scommand.Addressmode       = Qspi_address_none;
  Scommand.Alternatebytemode = Qspi_alternate_bytes_none;
  Scommand.Datamode          = Qspi_data_1_line;
  Scommand.Dummycycles       = 0;
  Scommand.Ddrmode           = Qspi_ddr_mode_disable;
  Scommand.Ddrholdhalfcycle  = Qspi_ddr_hhc_analog_delay;
  Scommand.Sioomode          = Qspi_sioo_inst_every_cmd;

  Sconfig.Match           = 0x00;
  Sconfig.Mask            = 0x01;
  Sconfig.Matchmode       = Qspi_match_mode_and;
  Sconfig.Statusbytessize = 1;
  Sconfig.Interval        = 0x10;
  Sconfig.Automaticstop   = Qspi_automatic_stop_enable;

  If (Hal_qspi_autopolling(Hqspi, &Scommand, &Sconfig, Timeout) != Hal_ok)
  {
    Error_handler();
  }
}
//-------------------------------
/**
  * @Brief  This Function Read The Sr Of The Memory And Wait The Eop.
  * @Param  Hqspi: Qspi Handle
  * @Retval None
  */
Static Void Qspi_autopollingmemready_it(Qspi_handletypedef *Hqspi)
{
  Qspi_commandtypedef     Scommand;
  Qspi_autopollingtypedef Sconfig;

  /* Configure Automatic Polling Mode To Wait For Memory Ready ------ */  
  Scommand.Instructionmode   = Qspi_instruction_1_line;
  Scommand.Instruction       = W25x_readstatusreg1;
  Scommand.Addressmode       = Qspi_address_none;
  Scommand.Alternatebytemode = Qspi_alternate_bytes_none;
  Scommand.Datamode          = Qspi_data_1_line;
  Scommand.Dummycycles       = 0;
  Scommand.Ddrmode           = Qspi_ddr_mode_disable;
  Scommand.Ddrholdhalfcycle  = Qspi_ddr_hhc_analog_delay;
  Scommand.Sioomode          = Qspi_sioo_inst_every_cmd;

  Sconfig.Match           = 0x00;
  Sconfig.Mask            = 0x01;
  Sconfig.Matchmode       = Qspi_match_mode_and;
  Sconfig.Statusbytessize = 1;
  Sconfig.Interval        = 0x10;
  Sconfig.Automaticstop   = Qspi_automatic_stop_enable;

  If (Hal_qspi_autopolling_it(Hqspi, &Scommand, &Sconfig) != Hal_ok)
  {
    Error_handler();
  }
}
//------------------------------------------------------

/**
  * @Brief  Command Completed Callbacks.
  * @Param  Hqspi: Qspi Handle
  * @Retval None
  */
Void Hal_qspi_cmdcpltcallback(Qspi_handletypedef *Hqspi)
{
  Cmdcplt++;
}
/**
  * @Brief  Rx Transfer Completed Callbacks.
  * @Param  Hqspi: Qspi Handle
  * @Retval None
  */
Void Hal_qspi_rxcpltcallback(Qspi_handletypedef *Hqspi)
{
  Rxcplt++;
}

Void Hal_i2c_memrxcpltcallback(I2c_handletypedef *Hi2c1)
{   Rx23cplt++;  }
    
/**
  * @Brief  Tx Transfer Completed Callbacks.
  * @Param  Hqspi: Qspi Handle
  * @Retval None
  */
Void Hal_qspi_txcpltcallback(Qspi_handletypedef *Hqspi)
{
  Txcplt++; 
}

/**
  * @Brief  Status Match Callbacks
  * @Param  Hqspi: Qspi Handle
  * @Retval None
  */
Void Hal_qspi_statusmatchcallback(Qspi_handletypedef *Hqspi)
{
  Statusmatch++;
}

/**
  * @Brief  Transfer Error Callback.
  * @Param  Hqspi: Qspi Handle
  * @Retval None
  */
Void Hal_qspi_errorcallback(Qspi_handletypedef *Hqspi)
{
  Error_handler();
}

Void Hal_uart_rxcpltcallback(Uart_handletypedef *Huart)
{
   If (Huart->Instance == Usart1)
   { 
		 //Ini_reciv1();
		 Hart_finishrx();
   } 
}
//-------------------------------------------------------  
//Þ�������� ÿ������� 
Void Hal_uart_txcpltcallback(Uart_handletypedef *Huart)
{   Hal_statustypedef Errdma2;
   If (Huart->Instance == Usart1)
   { 
       
      If (Flag_hartservice == 1)
        Hart_finishtx();
      Else
      { Uart_endtxtransfer(&Huart1);
        Ini_reciv1();
        G_cntinireccom1++;
      }   
   } 
   Else If (Huart->Instance == Usart2)
   { 
       Uart_endtxtransfer(&Huart2);
//       Ini_reciv2();
       G_cntinireccom2++;
   } 
   Else If (Huart->Instance == Usart3)
   {
    Txcpltusr3++; // ß������� î�������
    Hal_gpio_writepin(Gpiob, Gpio_pin_1, Gpio_pin_reset);  //Rts Uart3 (Hart3) ñ����  
       // â������� ÿ���� 
    If ((Errdma2 = Hal_uart_receive_it(&Huart3, Modbus_recvbuf,Count_modbus)) != Hal_ok) //Resmodbus
	{	Error_handler(); }      
    Rxcpltusr3 = 0;
   }
}
//-----------------------------------------
//Void Inicfgspeed() {
//Huart1.Init.Baudrate = Baudrates_huart1[Glconfig.Speed];   
//Mx_usart1_uart_init();
//   Flagspeed1 = 0;                       // ?Ip�? ??Aua ?Pni�oa??? ???N?N??? ?¬�p�???
//}    
//----------------------------------------
// ß��������� ÿ�� ÿ���?��� ø�������� î� ñ��������
Void Hal_gpio_exti_callback(Uint16_t Gpio_pin)
{  Long Long T; 
    Uint8_t Str20[20];
    
  If (Gpio_pin== Fl_1_exti_5_pin)   //Gpio_pin_5 
  { 
//Ñ��������� ô��� ÿ��������� Pin_5 (Fl-1)
    __nop(); __nop(); __nop();  

      Count1++;
      T = T_16 - Intr[0]; // ø������� ì���� ø��������� â 10��
      If (T < 0) Interval[0] = T + Uint32_max+1; // 4294967295.0;
      Else Interval[0] = T;
      Intr[0] = T_16;
      If (Interval[0] > 95)   // * Config_idrun[2].Numturbo
          Dqw_1    = Config_idrun[0].Numturbo/((Float)Interval[0]*0.001); // ñ�������� ð���?��
      Memset(Str20,0,20);
      Sprintf((Char*)Str20,"Dqw=%4.2f C1=%D",Dqw_1,Count1);
      Lcdprint(0, 0, (Char*)Str20);
  }    
  Else If (Gpio_pin== Fl_2_exti_4_pin)  //Gpio_pin_4 = Fi_1_exti_5_pin     
  {
  //Ñ��������� ô��� ÿ��������� Pin_4 (Fl-2)
    __nop(); __nop(); __nop();
      Count2++;
      T = T_16 - Intr[1]; // ø������� ì���� ø��������� â 10��
      If (T < 0) Interval[1] = T + Uint32_max+1; // 4294967295.0;
      Else Interval[1] = T;
      Intr[1] = T_16;
      If (Interval[1] > 95)   // * Glconfig.Idrun[2].Numturbo
          Dqw_2    = Config_idrun[1].Numturbo/((Float)Interval[1]*0.001); // ñ�������� ð���?��
  }    
  Else{
        __nop();
      }

 }
//------------------------------------------------------
//Ñ������� ñ������ â�� ÿ���������
Void Erasesectorw25(Uint16_t Typearch, Uint8_t Run, Uint16_t Numsector)
{ Uint8_t Qsend, Busy;
  Uint32_t Adrsecterase;

  Adrsecterase =  Numsector * Sectorsize;
//  Ad = Adrsecterase;
  W25qxx_write_enable(); // Wel=1
  If (Qspi_send_cmd(W25x_sectorerase, Adrsecterase, 0, Qspi_address_1_line, Qspi_data_none, 0)!=Qspi_ok)
         Error_handler();
  Qspi_autopollingmemready(&Hqspi,Timeoutw);
} 

//Ñ������� ñ������ ñ ÿ�����������
Void Erasesectorw25_it(Uint16_t Typearch,  Uint16_t Numsector)
{ Uint8_t Qsend, Busy;
  Uint32_t Adrsecterase;

  Cmdcplt = 0; 
  Adrsecterase = Numsector*Sectorsize;
//    Ad = Adrsecterase;
  W25qxx_write_enable(); // Wel=1
  If (Qspi_send_cmd_it(W25x_sectorerase, Adrsecterase, 0, Qspi_address_1_line, Qspi_data_none, 0)!=Qspi_ok)
         Error_handler();
  While (Cmdcplt == 0);// î������� ñ����������� ÿ���������
  Cmdcplt = 0;
  Qspi_autopollingmemready(&Hqspi,Timeoutw);
} 
//------------------------------------------------------------
// Ç����� ì������ Wrdata â ÿ����� W25q ñ ÿ�������������� ñ������� ñ������
Void Writedataw25(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Wrdata)
{ Uint8_t Qsend, Busy;
  Uint32_t Adrwrite;
  Uint16_t Numsector;
   
    Numsector =  Bsectorerase[Typearch]+Run*Kolsectorarch[Typearch] + Numrec /(Sectorsize / Recsizew[Typearch]);
    //Ad = Numsector;
    If (Numsector != Erasebl[Typearch][Run]) // ô��� ç������� ñ����� í� ñ����
    { Erasesectorw25(Typearch, Run, Numsector); // ó� ñ�������
      Erasebl[Typearch][Run] = Numsector;  // ô������� ô����, ÷�� ç������� ñ����� ñ����
    }   
    Adrwrite = (Bsectorerase[Typearch]+Run*Kolsectorarch[Typearch])*Sectorsize + Numrec*Recsizew[Typearch];
//    Ad = Adrwrite;
    W25qxx_write_enable();
    Qsend = Qspi_send_cmd(W25x_pageprogram, Adrwrite, 0, Qspi_address_1_line, Qspi_data_1_line, Recsizer[Typearch]);
   If ((Qsend != 0) || (Hal_qspi_transmit(&Hqspi, Wrdata,Timeoutw) != Hal_ok))
               Error_handler();
//     If  (W25qxx_wait_busy() != 0)
//         Error_handler();
   Qspi_autopollingmemready(&Hqspi,Timeoutw);  
}
//------------------------------------------------------------
// Ç����� ì������ Wrdata â ÿ����� W25q ñ ÿ�������������� ñ������� ñ������
Uint8_t Writedataw25_it(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Wrdata)
{ Uint8_t Qsend, Busy;
  Uint32_t Adrwrite;
  Uint16_t Numsector;
  Uint8_t Rddata[64];
 
    Numsector =  Bsectorerase[Typearch]+Run*Kolsectorarch[Typearch] + Numrec /(Sectorsize / Recsizew[Typearch]);
//    Ad = Numsector;
    If (Numsector != Erasebl[Typearch][Run]) // ô��� ç������� ñ����� í� ñ����
    { Erasesectorw25_it(Typearch, Numsector); // ó� ñ�������
      Erasebl[Typearch][Run] = Numsector;  // ô������� ô����, ÷�� ç������� ñ����� ñ����
    }  

     Adrwrite = (Bsectorerase[Typearch]+Run*Kolsectorarch[Typearch])*Sectorsize + Numrec*Recsizew[Typearch];
//    Ad = Adrwrite;
    Txcplt = 0;
    W25qxx_write_enable();
    Qsend = Qspi_send_cmd(W25x_pageprogram, Adrwrite, 0, Qspi_address_1_line, Qspi_data_1_line, Recsizer[Typearch]);
    If ((Qsend != 0) || (Hal_qspi_transmit_it(&Hqspi, Wrdata) != Hal_ok))
        Return 1; //       Error_handler();
    While (Txcplt == 0);
    Txcplt = 0;
    Qspi_autopollingmemready(&Hqspi,Timeoutw);
    Return 0;    
    }
//--------------------------------------------------------------------------
// ×����� ä�����? ø� Qspi â�� ÿ���������
Void Readdataw25(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Rddata)
{
  Uint8_t Qsend, Busy;
  Uint32_t Adrreadw;

   Adrreadw= (Bsectorerase[Typearch]+Run*Kolsectorarch[Typearch])* Sectorsize + Numrec*Recsizew[Typearch];
   
//    Ad = Adrreadw;
   St_send=Qspi_send_cmd(W25x_fastreaddual, Adrreadw, 8, Qspi_address_1_line, Qspi_data_2_lines, Recsizer[Typearch]);
   If ((St_send!=0) || (Hal_qspi_receive(&Hqspi, Rddata, Timeoutw) != Hal_ok))
         Error_handler();
}
//--------------------------------------------------------------------------
// ×����� ä�����? ø� Qspi ÿ� It
Uint8_t Readdataw25_it(Uint16_t Typearch, Uint8_t Run, Uint32_t Numrec,Uint8_t *Rddata)
{
  Uint8_t Qsend, Busy;
  Uint32_t Adrreadw;

   Rxcplt = 0;
   Adrreadw= (Bsectorerase[Typearch]+Run*Kolsectorarch[Typearch])* Sectorsize + Numrec*Recsizew[Typearch];
   Ad = Adrreadw;
    St_send=Qspi_send_cmd(W25x_fastreaddual, Adrreadw, 8, Qspi_address_1_line, Qspi_data_2_lines, Recsizer[Typearch]);
   If ((St_send!=0) || (Hal_qspi_receive_it(&Hqspi, Rddata) != Hal_ok))
         Return 1; //Error_handler();
    While (Rxcplt == 0); // î������� ú���� ÿ�����
   Rxcplt = 0;
   Return 0;
//   Qspi_autopollingmemready(&Hqspi,Timeoutw);   // î������� ñ����������� ô����� W25  
}
//--------------------------------------------------------------
Void  Startuzs(Int Offn)
{ Int I;

      For (I=0; I < Glconfig.Numrun; I++)
        // Offn=1 ¤����������� ¿��������� > 20 C
//       If (Config_idrun[I].Typecounter == 1)
       If  ((Intpar.Keyboard != 0) && (Config_idrun[I].Typecounter == 1))       // Moscad Ascii
         Firstreadfs(I,Offn,Modbus_send_len_ascii); //¿����� ç����� ®����� ¸ à������ ¸� �␢␘
       Else                    // Moscad Rtu
         Firstreadfs(I,Offn,Modbus_send_len_rtu);

    }
//----------------------------------------------------------
//------------------ ¿����� ç����� ®�����  ¸� �␢␘ Flowsic
Void  Firstreadfs(Int N_run, Int On,Uint16_t Mblen)
{//  Unsigned Long Int Vs;
  Int Nerr;
  Double *V;
  Double Vthr;
//  Char Far *Penp;

  If (Config_idrun[N_run].Typerun == 10)
   {        // ==10
//      Outportb(0x1d0,Base_cfg_page); // â� ¢������ â�������
//					// ¸ ¢���� ¸� ��
//      Penp = (Char Far *)Mk_fp(Segment,6156+N_run*8); // ç�����
//      V = &Vthr;
//      _fmemcpy(V,Penp,8);
//Printf("Vthr=%Lf On=%D\N",Vthr,On);
      Modbuslen = Mblen; // Modbus_send_ascii;
     If (On == 1)                 // ´��� ¤����������� ¿��������� > 20 C
     {  Firstmb[N_run] = 1;
       Vwsr[N_run] = 0.0;
     }
      Else                             //   ´��� ¿��������� < 20 C,
	 Vwsr[N_run] = Vthr;
   }
}

/* User Code End 4 */

/**
  * @Brief  This Function Is Executed In Case Of Error Occurrence.
  * @Retval None
  */
Void Error_handler(Void)
{
  /* User Code Begin Error_handler_debug */
  /* User Can Add His Own Implementation To Report The Hal Error Return State */
//While (1);
Memset(Str90,0,90);
Sprintf((Char*)Str90, "*** Error_uart\N\R");
Hal_uart_transmit(&Huart2,Str90,Sizeof(Str90),1000);
  /* User Code End Error_handler_debug */
}

#Ifdef  Use_full_assert
/**
  * @Brief  Reports The Name Of The Source File And The Source Line Number
  *         Where The Assert_param Error Has Occurred.
  * @Param  File: Pointer To The Source File Name
  * @Param  Line: Assert_param Error Line Source Number
  * @Retval None
  */
Void Assert_failed(Uint8_t *File, Uint32_t Line)
{
  /* User Code Begin 6 */
  /* User Can Add His Own Implementation To Report The File Name And Line Number,
     Tex: Printf("Wrong Parameters Value: File %S On Line %D\R\N", File, Line) */
  /* User Code End 6 */
}
#Endif /* Use_full_assert */

/************************ (C) Copyright Stmicroelectronics *****End Of File****/
