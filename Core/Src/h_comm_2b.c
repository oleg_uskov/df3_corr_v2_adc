//////////////////////////////////////////////////////////////////
// ��� �� ����� ���1.
//////////////////////////////////////////////////////////////////

//#ifdef NEW_BUTN

#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <mem.h>
#include <string.h>
#include <math.h>

#include "h_main.h"
#include "density.h"
#include "comm_str.h"
#include "h_comm_2.h"

#include "h_time.h"
#include "h_calc.h"
#include "h_analog.h"
#include "h_mem.h"
#include "h_hart.h"
#include "h_disp.h"
#include "h_debug.h"
#include "h_valarm.h"
#include "h_485.h"
#include "h_smooth.h"                     // ��� �業�� ����� ��饭�� ���稪�
#include "h_persist.h"
#include "h_summwint.h"
//#include "ct.h"
//#include "h_msd.h"

//---------------------------------------------------------------------------
#define         V_ALARM            511
#define         maxindex_1         384 //768
#define         transindex_1       526
#define         maxcounttimeaut_1   20  // ����. ���-�� ⠪⮢ �� 25 ��

#define         SyncByte          0xAA  // ᨭ�p����� �p� ���p��
#define         wPolynom        0xA001  // �������
//---------------------------------------------------------------------------
void ComPrint_2(char *Txt);
void GetVersion(struct Id *pid);
void ComPrint(char *Txt);
void HartIni();
void HartIni2();
void HartIni_Hartservice_1_2();
void n_delay (int mlsek);
void   CalcFmax(int run);
void RS485Com5IniHF();
void SetSpeedCom4();
//void SetSpeedCom5();
float St10(char);
void EraseSD();
//void InitSD_HC();
void WriteArcData();
//int Fpermit(int k,struct wrdiaphconst  *pd,struct IdRun *pr);
void  StartUZS(int);
//---------------------------------------------------------------------------
extern float    TAU_flt, Ros_flt;
extern float IndRe[3],IndC_[3],IndKsh[3],IndQ[3],StartRe[3],IndDP[3];
extern struct mon_summ MonthSumm[3];    // ������ �㬬����
                                        // �㬬���� ���਩��� ��ꥬ��
extern struct valarm_data far *ValarmSumm;
extern struct valarm_data far *ValarmSumm2;
extern struct valarm_data ValarmData_1; // ������ ��娢� ���਩��� ��ꥬ��
//extern long  AllAlarmCount[3];          // ���稪 ��� ���਩
//     IzmAlarmCount[3];                // ���稪 ���਩ ����७��

extern struct glconfig    GLCONFIG;
extern struct AddCfg      ADCONFIG;
extern struct intpar      INTPAR;
extern struct bintime     BINTIME;      // ����筠� ��p���p� �p�����
extern struct sumtdata    SUMTDATA[];   // ��� �㬬�p������ ������ �� ������, ��, ��⪨
extern struct calcdata    CALCDATA[];
extern unsigned int hart2;                              //����稥 HART2
extern struct memdata      MEMDATA_1;   // ��p���p� ����� ���� - 26 ����
extern struct memaudit     MEMAUDIT_1;  // ��p���p� ����� �p娢� ����⥫���
extern struct memalarm     MEMALARM_1;  // ��p���p� ����� �p娢� ���p��
//extern struct startendtime STARTENDTIME_1; // ��p���p� ��� ���᪠ ����ᥩ � ��p�����᪮� �p娢�
extern char F25;
extern struct new_count Cnt[];          // ���ᨢ ������ ���稪�
extern unsigned char HartFormat;        // �ଠ� Hart-�������
extern struct hartdata *ph;             // ���� ���� ���稪��
extern struct rs485data *p485d;         // ���� ���� ���稪��
extern int SysTakt;
//---------------------------------------------------------------------------
extern unsigned char Flag_Hartservice_1;       // �ਧ��� ०��� �࠭��樨 HART<->COM
extern float g_Qst_1;                          // ��砫�� ��ꥬ �� ��, �3 �� ������ �����
extern char  RS485HF[];
extern unsigned int g_cntIniRecCom1;           // ���稪 ���樠����権 �ਥ�� �� COM1
extern unsigned int st_cntIniRecCom1;         // ��� ����� � ����ᠭ�ﬨ ����㭨��樨 �� COM1

extern unsigned int    counttimeaut_1;         // ���-�� ⠪⮢ �� 25 ��
extern unsigned int    SendCount_1;
//extern unsigned int    comindex_1;             // ����� inindex � ������ �p�p뢠��� �� ⠩��p�
extern unsigned int    inindex_1;              // �. �� ���⮩ �������, inc �� �p�p. �p�������, =0 �p� ����.
extern int ResetHartS;
extern unsigned char  Tlogin1[];
extern  int WrV;
extern char CalcZc[];
//---------------------------------------------------------------------------
// extern unsigned char LastRecFlag_1 = 0;        // 䫠� �뤠� ��᫥���� �����
extern unsigned int    outindex_1;
extern unsigned char   flagspeed_1,flagspeed;            // 䫠� ��������� ᪮p��� �� COM-��p��
                                        // ���p ��� ��p����
extern unsigned char   BufTrans_1[];
                                        // ���p �p����
extern unsigned char   BufResiv_1[];
extern unsigned char   BufOutKadr_1[256];      // ���p ���஢ ��� ��p����
extern unsigned char   BufInKadr_1[256];       // ���p �p���⮣� ����
extern struct calcdata   COPY_CALCDATA_1[3];
extern struct bintime BinTmp_1,BinTmp2_1;
extern  float MValue[3][4];
extern char StartCPU;
extern double KVolumeUZS[];
extern char hartnum2,hartnum,rs485num,RS485MB;
extern unsigned char AnCom5;
extern struct memsecurit MEMSECUR, MEMSECUR_1;
extern union send SEND_1;
extern int kSDm[],kSDp[];
extern char PASSWR1[16];
extern unsigned int  Dostup,Dostup1;
extern resultCMD InitSD_Res;
extern int nRunG;
extern int  bSDOff1;
extern union resiv  RESIV_1;
extern GornalPassw LogPassw[6];
extern double KoefP[];  // = {1.0,0.0980665,98.0655,0.980665};
extern double KoefdP[];  // = {1.0, 0.00980655, 0.0980665};
extern double KoefEn[]; // = {238.846, 1.0, 0.277778};
extern float KoRo[3][3];
extern unsigned char NumCal;
//extern char NumKan1;
//extern  FILE *fp;
//---------------------------------------------------------------------------
//float Round_fromKg(float, double, float*);
float Round_toKg(float, double, float*);

//---------------------------------------------------------------------------
int bSDini_1;

void AnswStCfg_1() {                    // ����室������ ��砫쭮� ���䨣�p�樨
 AnswStCfgp(BufTrans_1);                      // ����室������ ��砫쭮� ���䨣�p�樨
}
/*
   if (GLCONFIG.StartConfigFlag == 0)   // 䫠� ���. ���䨣�p�樨
      SaveMessagePrefix_1(7,0xC0);      // ��砫쭠� ���䨣�p��� �� �p������
   else
      SaveMessagePrefix_1(7,0xFF);      // �p������ ��砫쭠� ���䨣�p���

   BufTrans_1[4] = GLCONFIG.NumRun;     // ���-�� ��⮪
}
*/
//---------------------------------------------------------------------------
void WrStCfg_1()                        // ������ ��砫쭮� ���䨣�p�樨
{
 WrStCfgp(BufInKadr_1,BufTrans_1);
}
/* --------------------
 struct wrstcfg    *pcfg;
  struct memdata  *pmpd;                // ��p���p� ����� ���⮢ - 26 ����
  int               i,err;
  unsigned char Dens, Chrom;
   char c,e;
   int KS=52;
   unsigned long L;
   unsigned char Ser[6];
   unsigned char psSer[11];
   char far *LgPars;
   char str[20];
//  unsigned char empR[3];
  unsigned char empR;
  unsigned char flagStart;
//   if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������
  flagStart = 0x0F;
  err = 0;
  if( (BufInKadr_1[2] - sizeof(struct wrstcfg)) != 6 ) err |= 1; //����p��� ����� ���p��
  if( GLCONFIG.StartConfigFlag  == 0) err |= 2; //䫠� ���. ���䨣�p�樨
//   if (memcmp(GLCONFIG.Serial,"    ",4)==0)
//      err |= 4;
   if (err)
     { if (err & 0x04)
	 SaveMessagePrefix_1(6,0xFE);
      else
	 SaveMessagePrefix_1(6,0xFF);        // ���䨣�p��� �� ����ᠭ�
     }
  else
  { pcfg = &RESIV_1.WRSTCFG;
    movmem(&BufInKadr_1[4],&RESIV_1,sizeof(wrstcfg)); //�⥭�� �p������ ��p����p��
    if (GLCONFIG.NumRun != (pcfg->NumRun & 0x0F)) flagStart=0;
    GLCONFIG.NumRun    = pcfg->NumRun & 0x0F;    //���-�� ��⮪
    empR = pcfg->NumRun & 0xE0;  // ���⪠ ��娢�
    GLCONFIG.TypeCount = pcfg->TypeCount; // | 0x80;   //��� 0: ��50\GERG [0\1]
					    //��� 1: ��p-const\��p-p���. [0\1]

//0 - 3095, 2 - 3095+3051CD, 1 - P+T+dP, 3 - P+T+dP+dP, 4 - ���稪

      for (i=0; i<GLCONFIG.NumRun; i++)
      {
       if (GLCONFIG.IDRUN[i].TypeRun != (pcfg->TypeRun[i] & 0x0F))
       {  flagStart=0; //䫠� ������� ���᫨⥫�,
	if ((pcfg->TypeRun[i] & 0x0F)!=4 && (pcfg->TypeRun[i] & 0x0F)!=5
	 && (pcfg->TypeRun[i] & 0x0F)!=10)
	  { GLCONFIG.IDRUN[i].TypeCounter = 0;
	    GLCONFIG.IDRUN[i].AddrMODBUS = 0;
	  }
       }
					// ᯮᮡ ����p���� �� ��⪥
         if (((pcfg->TypeRun[i] & 0x0F) == 4)&&(i==2))  //�� 3-� ��⪥ �� ����� ���� ���稪 ��
         GLCONFIG.IDRUN[i].TypeRun = 5;
         else
	 GLCONFIG.IDRUN[i].TypeRun = pcfg->TypeRun[i] & 0x0F;
         //���⭮���
	 Dens = (pcfg->TypeRun[i] & 0x20) >> 5;
	 if (Dens != GLCONFIG.IDRUN[i].Dens) flagStart=0;
	 GLCONFIG.IDRUN[i].Dens = Dens;
         // GERG91 ��� NX19
	 Chrom = (pcfg->TypeRun[i] & 0x40) >> 6;
	 GLCONFIG.IDRUN[i].nGerg91_NX = Chrom;
	 Dens = (pcfg->TypeRun[i] & 0x80) >> 7;
         if ((F25 == 0)&&(Dens==1)) { Dens=0; GLCONFIG.IDRUN[i].nGerg91_NX = 1;}
	 GLCONFIG.IDRUN[i].nGerg88_AGA8 = Dens;
         GLCONFIG.IDRUN[i].Vlag = (pcfg->TypeCount >> (i+4)) & 0x01;
      }

//    movmem(RESIV_1.WRSTCFG.Password,GLCONFIG.PASSWORD,16); //����ᥭ�� ��p���

    GLCONFIG.StartConfigFlag  = 0;  // �p�� 䫠�� ���. ���䨣�p�樨
//.................................
      outportb(0x1d0,RESERV_CFG_PAGE);       // ��⠭����� ��p����� �����
      LgPars = (char far *)MK_FP(SEGMENT,4100);
	memcpy(Ser,GLCONFIG.Serial+1,6);
	L= atol(Ser);
        if (L==0)
           memcpy(psSer,"          ",10);
        else
	for (i=0; i<10; i++)
	{ c =(L % KS);
	  e=c+65;
	  if (e > 90) e+=6;
	  psSer[i] = e;
	  L = L + c + i+1;
	  if (i==0) L=L+INTPAR.CntAddBufShift; // ���.��ࠬ��� ����� ���� ���稪�
	}
	psSer[11] = '\0';
       _fmemcpy(LgPars,(char far *)psSer,10);

//.................................

      Mem_AuditClr(empR); //!!! ���⪠ ��p����� ����⥫���
	  MEMAUDIT_1.type     = 48;   //������� � ��p��� ����⥫���
	  MEMAUDIT_1.altvalue = 0.0;
	  MEMAUDIT_1.newvalue = 0.0;
      for (i=0; i<GLCONFIG.NumRun; i++)
	{  Mem_AuditWr_1(Mem_GetAuditPage(i));//������ ���� ����⥫���
          outportb(0x1D0,BASE_CFG_PAGE);// � ����� �� ���
          poke(SEGMENT,INDMGN+i*2,1); //���㫥��� ���ᨢ� ���᪠ SD-�����
          poke(SEGMENT,INDPER+i*2,1); //���ᨢ ���᪠ ��ਮ���᪨�
          kSDm[i] = 1;
          kSDp[i] = 1;
        }
//      for (i=5270; i<10456; i++)        // ���㫨�� ��娢 ������᭮��
//      pokeb(SEGMENT,i,0);
//      poke(SEGMENT,5272,600);  //600 �����
      strncpy(MEMSECUR_1.login, Tlogin1,4);
      MEMSECUR_1.TIMEBEG = BINTIME;
      memset(&MEMSECUR_1.TIMEEND,0,6);
      MEMSECUR_1.Port = 1;
      MEMSECUR_1.mode  = 21;
      i = Mem_SecurWr_1(Mem_SecurGetPage(),0);

      //������ �p娢�
      ClrArc(empR);       //!!! - �⫠��� ���⪠ ���⮢
      Mem_AlarmClr(empR); //!!! ���⪠ ��p����� ���p��
      Mem_Clrtdata(empR);                   // !!! �p�� �㬬��p��
      ValarmIni();                      // ��� ���稪�� ���਩
      ClearValarmArc(empR);                 // ��⪠ ��娢�� ���਩��� ��ꥬ��
      ClearValarmSumm(empR);                // ��⪠ �㬬��஢ ���਩��� ��ꥬ��
      Persist_SetDefault(empR); //���㫥��� ���� ��ꥬ��

     if( Mem_ConfigWr() == 0 )
     {       // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
      if ((empR & 0xE0) == 0xE0)
	NulAddCfg();                    // ���㫥��� ���.���䨣��樨
//        OnButton();                     // ���. �����. ��ࠬ. � ���祭�� �� 㬮�砭��
	SaveMessagePrefix_1(6,0xC1);    // ���䨣�p��� ����ᠭ�
	SysTakt=(GLCONFIG.NumRun < 3) ? 1:2;

	 if (flagStart == 0) {n_delay(1); StartCPU = 0;} //�������
     }
     else
	SaveMessagePrefix_1(6,0xFF);//���䨣�p��� �� ����ᠭ�
  }
}
*/
//---------------------------------------------------------------------------
void SetTime_1()  //��⠭���� �p�����
{
 SetTimep(BufInKadr_1,BufTrans_1,1);
}
/*
 struct settime    *psettime;
  struct bintime    *pbintime;
  unsigned char     *pchar;
  extern struct bintime TIME_SETTIME;  //�ᯮ������ �p� ��⠭���� �p�����

  int   err = 0;
  int i;
//  if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

  if(BufInKadr_1[2]-22 != sizeof(struct settime)) err |= 1;  //����p��� ����� ���p��
//  if(memcmp(&BufInKadr_1[4],GLCONFIG.PASSWORD,16))    //�p���p�� ��p���
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
						err |= 4;
  if(err) SaveMessagePrefix_1(6,0xFF);            //�訡�� �室��� ��p����p��
  else
  {
    movmem(&BufInKadr_1[20],&RESIV_1,sizeof(struct settime)); //������ ��p����p��
    psettime = &RESIV_1.SETTIME;
    pbintime = &TIME_SETTIME;
    pbintime->month   = psettime->Month;          //�����
    pbintime->date    = psettime->Day;            //����
    pbintime->year    = psettime->Year;           //���
    pbintime->hours   = psettime->Hours;          //��
    pbintime->minutes = psettime->Minutes;        //�����
    pbintime->seconds = psettime->Seconds;        //ᥪ㭤�
    Time_SetFlagTime();     // ��⠭���� 䫠�� ��⠭���� �p�����
	  MEMAUDIT_1.type = 128;  // �p��� ���᫨⥫�
	  pchar = (unsigned char*)&MEMAUDIT_1.altvalue;
	  pchar[0] = BINTIME.hours;
	  pchar[1] = BINTIME.minutes;
	  pchar[2] = BINTIME.seconds;
	  pchar[3] = 0;
	  pchar[4] = pbintime->hours;
	  pchar[5] = pbintime->minutes;
	  pchar[6] = pbintime->seconds;
	  pchar[7] = 0;
	  for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
	     Mem_AuditWr_1(Mem_GetAuditPage(i));

    if((BINTIME.month!=pbintime->month)||(BINTIME.date!=pbintime->date)||(BINTIME.year!=pbintime->year))
    {     MEMAUDIT_1.type = 129;  //��� ���᫨⥫�
          pchar = (unsigned char*)&MEMAUDIT_1.altvalue;
          pchar[0] = BINTIME.month;
          pchar[1] = BINTIME.date;
          pchar[2] = BINTIME.year;
          pchar[3] = 0;
          pchar[4] = pbintime->month;
          pchar[5] = pbintime->date;
          pchar[6] = pbintime->year;
          pchar[7] = 0;
	  for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
	     Mem_AuditWr_1(Mem_GetAuditPage(i));
    }
    SaveMessagePrefix_1(6,0xC2);  //���䨣�p��� ����ᠭ�
  }
}
*/
//---------------------------------------------------------------------------
void WrRecfg_1() {                      // ������ ��p����䨣�p�樨
 WrRecfgp(BufInKadr_1,BufTrans_1,&Dostup1,Tlogin1,1);
}
/*
   struct reconfig   *precfg;           // ��p����p� ��p����䨣�p�樨
   int               err = 0;
   char SpCom2;
   int i;
//   if (Nwr1 == 0)  err |= 0x80;  // 䫠� ����� �������

   if (BufInKadr_1[2]-22 != sizeof(reconfig))
      err |= 1;                          // ����p��� ����� ���p��
                                         // �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[4],GLCONFIG.PASSWORD,16))
   if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 4;
   if(err) SaveMessagePrefix_1(6,0xFF);//���䨣�p��� �� ����ᠭ�
   else {
      precfg = &RESIV_1.RECONFIG;
                                        // �⥭�� ��p����p��
      movmem(&BufInKadr_1[4+16],&RESIV_1,sizeof(reconfig));
      err = RESIV_1.RECONFIG.Speed;

      if (err < 8) {                    // ����p��� ᪮p��� �� COM-��p��
         MEMAUDIT_1.type = 49;          // ������ � ��p��� ����⥫���
         SpCom2 = precfg->scr_conf & 0x80;
         precfg->scr_conf =  precfg->scr_conf & 0x3F;

                                        // ������ ��������
         unsigned char* p = (unsigned char*)&MEMAUDIT_1.altvalue;
         p[0] = 0;                      // ��஫�
         p[1] = GLCONFIG.NumCal;        // ����� ���᫨⥫�
         if (SpCom2==0)
         p[2] = GLCONFIG.Speed1;        // ᪮p���� �� COM2-��p��
         else  p[2] = GLCONFIG.Speed;
         p[3] = GLCONFIG.scr_conf;      // 䫠�� ���䨣��樨 ��࠭��

                                        // ����� ��������
         unsigned char* p2 = (unsigned char*)&MEMAUDIT_1.newvalue;
         p2[1] = precfg->Num;           // ����� ���᫨⥫�
         p2[2] = precfg->Speed;         // ᪮p���� �� COM1-��p��
         p2[3] = precfg->scr_conf;      // 䫠�� ���䨣��樨 ��࠭��
                                        // ��p���
        if ((Dostup1  == GLCONFIG.StartDostup1) && (memcmp(Tlogin1,"    ",4)==0))
        {
         p2[0] = memcmp(                // ��஫�
         RESIV_1.RECONFIG.NewPassword, GLCONFIG.PASSWORD, 16) ? 1 : 0;
         if (p2[0])
         { movmem(RESIV_1.RECONFIG.NewPassword,GLCONFIG.PASSWORD,16);
           movmem(GLCONFIG.PASSWORD,PASSWR1,16);
         }
        }  else   p2[0] = 0;

                                        // 䫠�� ���䨣��樨 ��࠭��
         GLCONFIG.scr_conf = precfg->scr_conf;
         GLCONFIG.NumCal = precfg->Num; // ����p ���᫨⥫�
                                        // ᪮p���� �� COM1-��p��
         if (SpCom2==0) // �������� ᪮���� �� COM2
         {
         GLCONFIG.Speed1 = precfg->Speed;  // ᪮p���� �� COM2
         flagspeed_1++;                 // �p����� ��������� ᪮p���
         }
         else           // �������� ᪮���� �� COM1
         {
         GLCONFIG.Speed = precfg->Speed;  // ᪮p���� �� COM1
         flagspeed++;
         }
                                        // ���樠������ ᪮p��� ���᫨⥫� ��᫥ ��p���� ���⢥p������
         if (Mem_ConfigWr() == 0) {     // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
            SaveMessagePrefix_1(6,0xC3);// ��p����䨣�p��� ����ᠭ�
            if (memcmp(p, p2, 4)) {     // ������ ���� ����⥫���
	  for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
	     Mem_AuditWr_1(Mem_GetAuditPage(i));
	    }
         }
         else
            SaveMessagePrefix_1(6,0xFF);// ��p����䨣�p��� �� ����ᠭ�
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // ��p����䨣�p��� �� ����ᠭ�
   }
}
*/
//---------------------------------------------------------------------------
void RdParamdat_1() //�⥭�� ��p����p�� ���稪��
{
 RdParamdatp(BufInKadr_1,BufTrans_1);
}
/*
{ struct paramdat *ppd;   //��p����p� ���稪��
  struct IdRun    *prun;  //��p����p� ��⪨
  unsigned char num;
  int  err = 0,i,L;
  char codf;

  if( BufInKadr_1[2] != 7 ) err |= 1;   //����p��� ����� ���p��
  num = BufInKadr_1[4];
  if(num > GLCONFIG.NumRun) err |= 2; //����p��� ����p� ��⪨
  if(err) SaveMessagePrefix_1(6,0xFF);  //�訡�� ��p����p�� ���p��
  else
  { ppd  = &SEND_1.PARAMDAT;
    prun = &GLCONFIG.IDRUN[num-1];  //㪠��⥫� �� ��p����p� ��⪨
    codf = BufInKadr_1[3];

    movmem(prun->hartadrt,ppd->hartadrt,5);
    movmem(prun->hartadrp,ppd->hartadrp,5);
    movmem(prun->hartadrdph,ppd->hartadrdph,5);
    movmem(prun->hartadrdpl,ppd->hartadrdpl,5);
    ppd->TypeRun  = prun->TypeRun;
    ppd->TypeP    = prun->TypeP;    //⨯ ���稪� ��������
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
        ppd->Pmin     = prun->Pmin;
      else
    ppd->Pmin     = prun->Pmin*KoefP[GLCONFIG.EDIZM_T.EdIzP];     //������ �p���� ���稪� ��������, ���\�2
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
        ppd->Pmax     = prun->Pmax;
      else
    ppd->Pmax     = prun->Pmax*KoefP[GLCONFIG.EDIZM_T.EdIzP];     //��p孨� �p���� ���稪� ��������, ���\�2
    ppd->tmin     = prun->tmin;     //����. �p���� ⥬��p���p�, �p. �
    ppd->tmax     = prun->tmax;     //��p�. �p���� ⥬��p���p�, �p. �
    if  (codf==0x46)
      { ppd->Romin    = prun->R0min;//*KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
        ppd->Romax    = prun->R0max;//*KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
      ppd->TypeCounter = prun->TypeCounter;
      ppd->AddrMODBUS  = prun->AddrMODBUS;
      for (i=0; i<4; i++)
      ppd->NomHartC[i] = prun->NumHartC[i];
      ppd->KUzs = prun->KUzs;
      L=0;
     }    else L=15;

    if ((prun -> TypeRun) == 4 || (prun -> TypeRun) == 5
    || (prun -> TypeRun) == 10)   //���稪
    { // ��砫�� ��ꥬ �� ��, �3
      g_Qst_1 = ppd->dPlmin_Qst = Persist_GetQwCountTotal(num-1);
      ppd->dPlmax_Cost   = prun->NumTurbo; //業� ������, �3/������
      ppd->dPhmin_Qmin = prun->Qmin;     //��������� p��室
      ppd->dPhmax_Qmax   = prun->Qmax;     //���ᨬ���� p��室
    }
    else                        //���稪� ��p�����
    {
      double KDP = (GLCONFIG.EDIZM_T.EdIzmdP == 0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
      ppd->dPlmin_Qst  = prun->dPhmin*KDP;
                                        // ��p�. �p���� ��p�����, ���\�2
      ppd->dPlmax_Cost = prun->ValueSwitch*KDP;
      ppd->dPhmin_Qmin   = prun->dPhmin*KDP;   //����. �p���� ��p�����, ���\�2
      ppd->dPhmax_Qmax   = prun->dPhmax*KDP;   //��p�. �p���� ��p�����, ���\�2
///!!!ppd->Qmin          = prun->Qmin;     // ���. ��室
    }
      SaveMessagePrefix_1(sizeof(paramdat)+7-L,codf+128);//������ �p�䨪�
      BufTrans_1[4] = num;                         //����p ��⪨
      movmem(ppd,&BufTrans_1[5],sizeof(paramdat)-L); //������ ���p�
  }
}
*/
//---------------------------------------------------------------------------
void WrParamdat_1() //������ ��p����p�� ���稪��
{
 WrParamdatp(BufInKadr_1,BufTrans_1,1);
}
/*
{ struct wrparamdat *ppd;   //��p����p� ���稪��
  struct IdRun      *prun;  //��p����p� ��⪨
  unsigned char num;
  int           err = 0,i;
//   unsigned char flagStart;
//   flagStart = 0xFF;
//  if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

  WrV = 1;  //  ࠧ�襭�� ����� ��ꥬ� � ��
  if(BufInKadr_1[2]-23 != sizeof(wrparamdat))err |= 1;  //����p��� ����� ���p��
  num = BufInKadr_1[4] & 3;
  if(num > GLCONFIG.NumRun)                err |= 2;  //����p��� ����p� ��⪨
//  if(memcmp(&BufInKadr_1[5],GLCONFIG.PASSWORD,16))      //�p���p�� ��p���
  if (memcmp(&BufInKadr_1[5],PASSWR1,14))
                                           err |= 4;
  if(err) SaveMessagePrefix_1(6,0xFF);                  //����⠭�� �� ����ᠭ�
  else
  {
    movmem(&BufInKadr_1[5+16],&RESIV_1,sizeof(wrparamdat)); //������ ��p����p��
    ppd  = &RESIV_1.WRPARAMDAT;
    prun = &GLCONFIG.IDRUN[num-1];  //㪠��⥫� �� ��p����p� ��⪨

    // HART ����
    HartAddrChange(prun->hartadrt,   ppd->hartadrt,   140, num, 0);
    HartAddrChange(prun->hartadrp,   ppd->hartadrp,   141, num, 1);
    HartAddrChange(prun->hartadrdph, ppd->hartadrdph, 142, num, 2);
    HartAddrChange(prun->hartadrdpl, ppd->hartadrdpl, 143, num, 3);
    if (WrV==0)
    { SetHartDataAdr(0); //�p��p������ ᯨ᪠ ���稪�� �� ���䨣�p�樨 ��������
      SetRS485DataAdr();    // �p�. ᯨ᪠ 485-���稪�� �� ����-�� ��������
    }
    // ⨯ ���稪� �������� (0 - ���., 1 - ���.)
    if(prun->TypeP != ppd->TypeP & 0x01) {
      MEMAUDIT_1.type = 144;
      *(unsigned long*)&MEMAUDIT_1.altvalue = prun->TypeP;
      *(unsigned long*)&MEMAUDIT_1.newvalue = ppd->TypeP & 0x01;
      Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      prun->TypeP = ppd->TypeP & 0x01;
      WrV = 0;
    }
    // ������ �p���� ���稪� ��������, ���\�2
    double KP = (GLCONFIG.EDIZM_T.EdIzP == 0) ? 1.0 : KoefP[GLCONFIG.EDIZM_T.EdIzP];
    FloatValueChangeEd(prun->Pmin, ppd->Pmin, 145, num,KP);
    // ���孨� �p���� ���稪� ��������, ���\�2
    FloatValueChangeEd(prun->Pmax, ppd->Pmax, 146, num,KP);
    // ����. �p���� ⥬��p���p�, �p. �
    FloatValueChange(prun->tmin, ppd->tmin, 147, num);
    // ����. �p���� ⥬��p���p�, �p. �
    FloatValueChange(prun->tmax, ppd->tmax, 148, num);

  union numH
  { unsigned long int C;
    char Nht[4];
  } Hk1,Hk2;

     for (i=0; i<4; i++)
     { Hk1.Nht[i] = prun->NumHartC[i];
       Hk2.Nht[i] = ppd->NomHartC[i];
     }
   if (hart2==0) // ���� 2-� HART �����
   { if (Hk1.C != Hk2.C)
     {
      MEMAUDIT_1.type = 161;
      *(unsigned long*)&MEMAUDIT_1.altvalue = Hk1.C;
      *(unsigned long*)&MEMAUDIT_1.newvalue = Hk2.C;
      Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      WrV = 0;
      for (i=0; i<4; i++)
      {
//        if ((prun->NumHartC[i]==1)&&(ppd->NomHartC[i]==2)&&(hartnum2==0))
//           flagStart = 0;
        prun->NumHartC[i] = ppd->NomHartC[i];
      }
        SetHartDataAdr(0); //�p��p������ ᯨ᪠ ���稪�� �� ���䨣�p�樨 ��������
     }
   } else
       if (Hk1.C != Hk2.C)
      {
	 for (i=0; i<4; i++)
	  prun->NumHartC[i] = 1;
         SetHartDataAdr(0); //�p��p������ ᯨ᪠ ���稪�� �� ���䨣�p�樨 ��������
      }

    if(prun->TypeCounter != ppd->TypeCounter)
     {
      MEMAUDIT_1.type = 159;
      *(unsigned long*)&MEMAUDIT_1.altvalue = prun->TypeCounter;
      *(unsigned long*)&MEMAUDIT_1.newvalue = ppd->TypeCounter;
      Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      prun->TypeCounter = ppd->TypeCounter;
      WrV = 0;
    }

    if ((ppd->KUzs & 0x7F)  != (prun->KUzs & 0x7F))       // ᪮���� ���� ��� 0,1,2,3,4
     {
      MEMAUDIT_1.type = 45;
      *(unsigned long*)&MEMAUDIT_1.altvalue = prun->KUzs & 0x7F;
      *(unsigned long*)&MEMAUDIT_1.newvalue = ppd->KUzs & 0x7F;
      Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      prun->KUzs = (prun->KUzs & 0x80) | (ppd->KUzs & 0x7F);
      WrV = 0;
    }

     if ((ppd->KUzs & 0x80)  != (prun->KUzs & 0x80))       // ᪮���� ���� ��� 0,1,2,3,4
     {
      MEMAUDIT_1.type = 61;
      *(unsigned long*)&MEMAUDIT_1.altvalue = (prun->KUzs & 0x80) >> 7;
      *(unsigned long*)&MEMAUDIT_1.newvalue = (ppd->KUzs & 0x80) >> 7;
      Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      prun->KUzs = ppd->KUzs;
      WrV = 0;
    }


    if (prun->AddrMODBUS  != ppd->AddrMODBUS)
     {
      MEMAUDIT_1.type = 160;
      *(unsigned long*)&MEMAUDIT_1.altvalue = prun->AddrMODBUS;
      *(unsigned long*)&MEMAUDIT_1.newvalue = ppd->AddrMODBUS;
      Mem_AuditWr_1(Mem_GetAuditPage(num-1));
       prun->AddrMODBUS  = ppd->AddrMODBUS;
      WrV = 0;
    }    // �᫨ ����� ���
    if ((ppd->AddrMODBUS > 0) && (ppd->TypeCounter > 0)
     && ((MEMAUDIT_1.type == 160) || (MEMAUDIT_1.type == 159)))
     {
       RS485HF[num-1]=1;
       if (RS485MB == 0)
        RS485Com5IniHF();  //���樠������ ���� 5 (MODBUS �� RS485)
       if  ((prun->TypeRun) == 10)
//        flagStart=0; //䫠� ������� ���᫨⥫�,
        StartUZS(1);
     }
     else if ((ppd->AddrMODBUS == 0) || (ppd->TypeCounter == 0))
      RS485HF[num-1]=0;
      RS485MB = 0;       // �ନ஢���� ������ ���祭�� RS485MB
      for (i=0; i < GLCONFIG.NumRun; i++)
       if (RS485HF[i]==1) RS485MB = 1;
      if (RS485MB == 0) // ������� ���� 5
       outportb(CT_I1CON, 0x0F);    // ������� ���뢠���  INT1 COM5
    FloatValueChange(prun->R0min, ppd->Romin * KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU], 149, num);
    FloatValueChange(prun->R0max, ppd->Romax * KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU], 151, num);
    if((prun->TypeRun) == 4 || (prun->TypeRun) == 5
    || (prun->TypeRun) == 10) { // ���稪
           // 業� ������, �3/������
      if ((prun->NumTurbo != ppd->dPlmax_Cost ) || (prun->Qmax != ppd->dPhmax_Qmax))
     {
        // 業� ������, �3/������
        FloatValueChange(prun->NumTurbo, ppd->dPlmax_Cost, 152, num);
        // ���ᨬ���� p��室
        FloatValueChange(prun->Qmax, ppd->dPhmax_Qmax, 156, num);
         CalcFmax(num-1);
      }
      else
      {
        FloatValueChange(prun->NumTurbo, ppd->dPlmax_Cost, 152, num);
        FloatValueChange(prun->Qmax, ppd->dPhmax_Qmax, 156, num);
      }
      // ��������� p��室
      FloatValueChange(prun->Qmin, ppd->dPhmin_Qmin, 154, num);
    }
    else {
      // ����. �p���� ��p�����, ���\�2
      double KDP = (GLCONFIG.EDIZM_T.EdIzmdP == 0) ? 1.0 : KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
      FloatValueChangeEd(prun->dPhmin, ppd->dPhmin_Qmin, 153, num,KDP);
      // ��p�. �p���� ��p�����, ���\�2
      FloatValueChangeEd(prun->dPhmax, ppd->dPhmax_Qmax, 155, num,KDP);
    }
   if (((prun->TypeRun) == 4 )|| ((prun->TypeRun) == 5))
    { // ���稪
      // ��砫�� ��ꥬ �� ��
      if ((g_Qst_1 != ppd->dPlmin_Qst) && (WrV)) {
	 MEMAUDIT_1.type = 150;
	 MEMAUDIT_1.altvalue = g_Qst_1;
	 MEMAUDIT_1.newvalue = ppd->dPlmin_Qst;
	 Mem_AuditWr_1(Mem_GetAuditPage(num-1));
	 Persist_SetQwCountTotal(num-1, ppd->dPlmin_Qst);
      }
    }

    if( Mem_ConfigWr() == 0) //������ ��p���p� ���䨣�p�樨 ���᫨⥫�
    { if ((Hk1.C != Hk2.C)&&(hart2))
	  SaveMessagePrefix_1(6,0xFF); //������ �� �ਧ�諠
      else
	 SaveMessagePrefix_1(6,0xC5);//������ �p�䨪�
//      CalibrateIni();            // ��१����� ��� ����������


   // �����⠭����� ��ࠬ���� ��ꥪ⮢ ��� �業�� ����� ��饭�� ���稪�
      if((prun->TypeRun) == 4 || (prun -> TypeRun) == 5)
	 Disp_count_1(num-1);
    }
    else
      SaveMessagePrefix_1(6,0xFF);//������ �p�䨪�
  }
//  if (flagStart == 0){n_delay(1); StartCPU = 0;} //�������
}
*/
//---------------------------------------------------------------------------
//�p��p������ ��������� � �⢥�
void SaveMessagePrefix_1(unsigned char l, unsigned char res)
{
  BufTrans_1[0] = 0x55;             //ᨭ�p�����
  BufTrans_1[1] = GLCONFIG.NumCal;  //����p ���᫨⥫�
  BufTrans_1[2] = l;
  BufTrans_1[3] = res;              //��� �㭪樨
}
//---------------------------------------------------------------------------
// �p��p������ ����p��쭮� �㬬� � ��室��� ���p�
void CheckBufTrans_1() {
   #define wPolynom 0xA001           //�������
   unsigned char   *uk;
   int             count;
   unsigned int    crc;
   int             bNeedXor;

   uk = BufTrans_1;

   count = (int)(*(uk+2) - 2);//��� 2-� ���⮢ ����p��쭮� �㬬�
   crc = Mem_Crc16(uk, count);
   uk += count;

   *((unsigned int*)uk) = crc; //������ ����p��쭮� �㬬�
}
//---------------------------------------------------------------------------

// ��� ����� � ����ᠭ�ﬨ ����㭨��樨 �� COM1
//
void Com1_Watch()
{
  char c,buf[10];

   // �।����饥 ���祭�� ���稪� ���樠����権 �ਥ�
//   static unsigned int st_cntIniRecCom1;

   // ���稪 ��� ॠ����樨 ����প�
   static int st_cntDelay;

   // ���쭥��� ��� �㤥� ��뢠���� ����� ������ ࠧ
   if(++st_cntDelay < 10)
      return;

   st_cntDelay = 0;
//ComPrint_2("1W  ");
   outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����
   outportb(comport1+4,0x09);        // ���室 �� �ਥ�
   if(g_cntIniRecCom1 == st_cntIniRecCom1)
    { if (Flag_Hartservice_1==0)  IniCfgSpeed_1();
//     Port_Ini_1();
//     Ini_Int_Com_1();                  // ��⠭���� ����p� �p�p뢠��� �� com1-IRQ4
      Ini_Reciv_1();
    }
   else   st_cntIniRecCom1 = g_cntIniRecCom1;
}

//
// �⢥� �� ����� �⥭�� ���� ��� � ���௮��஢������ ���祭��
//
void AnswCode_1()
{
 AnswCodep(BufInKadr_1,BufTrans_1);
}
/*
   float fTmp,fTmp2;
   int err = 0;
   int i, i_flt;

   if(BufInKadr_1[2] != 8)                        // ����p��� ����� ���p��
      err |= 1;

   unsigned char nRun = BufInKadr_1[4];           // ����p��� ����p� ��⪨
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;

   unsigned char sensType = BufInKadr_1[5];       // ����஫� ⨯� ���稪�
   if (sensType > 4)
      err |= 4;

   if(err)                                      // �訡�� ��p����p�� ���p��
      SaveMessagePrefix_1(6, 0xFF);
   else
   {
      SaveMessagePrefix_1(16, 0xCB);              // ������ �p�䨪�
      BufTrans_1[4] = nRun;                       // ����p ��⪨
      BufTrans_1[5] = sensType;                   // ���� ���稪�
                                                // ��� WinSystem
         fTmp = flt_ankan[nRun-1][sensType];


      if(GLCONFIG.IDRUN[nRun-1].sensData[sensType].nInterface == tiAN_WS) {
         *(float*)(BufTrans_1 + 6) = fTmp;                   // ��� ���
         *(float*)(BufTrans_1 + 10) = GetCalibrValue(nRun-1, // ���௮�.
                     sensType, fTmp);                      // ���祭��
      }
      else
      if (GLCONFIG.IDRUN[nRun-1].sensData[sensType].nInterface == tiHART) {
         switch(sensType) {
            case 0:
//               MoveToHartSens(1,GLCONFIG.IDRUN[nRun-1].TypeRun,nRun-1);
                 fTmp = MValue[nRun-1][0];
               *(float*)(BufTrans_1 + 6) = fTmp;//ph->valt;               // ��� ���稪�
               *(float*)(BufTrans_1 + 10) = GetCalibrValue(nRun-1, // ���௮�.
                  sensType,fTmp);                           // ���祭��
               break;
            case 1:
//               MoveToHartSens(2,GLCONFIG.IDRUN[nRun-1].TypeRun,nRun-1);
//               fTmp = ph->valp;
                 fTmp = MValue[nRun-1][1];
               fTmp2  =
                 (ADCONFIG.ADRUN[nRun-1].edp == 0xED) ? fTmp * 10.19716 :   // ��� -> ���/�2
                 (ADCONFIG.ADRUN[nRun-1].edp == 0x0C) ? fTmp * 0.01019716 : // ��� -> ���/�2
                 fTmp;                   // 0x0A: ���/�2 -> ���/�2
               *(float*)(BufTrans_1 + 6) = fTmp2;   // ��� ���稪�
               *(float*)(BufTrans_1 + 10) = GetCalibrValue(nRun-1,   // ���௮�.
                  sensType, fTmp);                // ���祭��
               break;
            case 2:
//               MoveToHartSens(4,GLCONFIG.IDRUN[nRun-1].TypeRun,nRun-1);
//               fTmp = ph->valdp;
                  fTmp = MValue[nRun-1][2];
               fTmp2 =
                  (ADCONFIG.ADRUN[nRun-1].eddph == 0x0C) ? fTmp * 101.9716 : // ��� -> ���/�2
                  (ADCONFIG.ADRUN[nRun-1].eddph == 0x09) ? 10.0*fTmp :
                  (ADCONFIG.ADRUN[nRun-1].eddph == 0x0A) ? 10000.0*fTmp :fTmp;
               *(float*)(BufTrans_1 + 6) = fTmp2;   // ��� ���稪�
               *(float*)(BufTrans_1 + 10) = GetCalibrValue(nRun-1,   // ���௮�.
                  sensType, fTmp);                // ���祭��
               break;
            case 3:
//               MoveToHartSens(8,GLCONFIG.IDRUN[nRun-1].TypeRun,nRun-1);
               fTmp = MValue[nRun-1][3]; //ph->valdp;
               fTmp2 =
                  (ADCONFIG.ADRUN[nRun-1].eddpl == 0x0C) ? fTmp * 101.9716 : // ��� -> ���/�2
                  (ADCONFIG.ADRUN[nRun-1].eddpl == 0x09) ? 10.0*fTmp :
                  (ADCONFIG.ADRUN[nRun-1].eddpl == 0x0A) ? 10000.0*fTmp :fTmp;
               *(float*)(BufTrans_1 + 6) = fTmp2;   // ��� ���稪�
               *(float*)(BufTrans_1 + 10) = GetCalibrValue(nRun-1,   // ���௮�.
                  sensType, fTmp);                // ���祭��
               break;
         }
      }
      else
      if (GLCONFIG.IDRUN[nRun-1].sensData[sensType].nInterface == tiAN_RS485) {
         MoveTo485Sens(sensType,nRun-1);
         *(float*)(BufTrans_1 + 6) = p485d->Value;               // ��� ���稪�
         *(float*)(BufTrans_1 + 10) = GetCalibrValue(nRun-1,    // ���௮�.
             sensType, p485d->Value);                           // ���祭��
      }
         if ((GLCONFIG.IDRUN[nRun-1].InterfaceDens == 4)&&(sensType==4))
         {*(float*)(BufTrans_1 + 6) = TAU_flt;   //����
          *(float*)(BufTrans_1 + 10) = Ros_flt; // ���௮�.���⭮���
         }

   }
}
*/
/////////////////////////////////////////////////////
// ������ �窨 �����஢��
/////////////////////////////////////////////////////
/*
void WrClbr_1()
{
   int err = 0;
   float fTmp1,fTmp2;

//   if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

   // ����p��� ����� ���p��
   if(BufInKadr_1[2] != 33)
      err |= 1;

   // ����p��� ����p� ��⪨
   unsigned char nRun = BufInKadr_1[4];
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;

   // �p���p�� ��p���
//   if(memcmp(&BufInKadr_1[5], GLCONFIG.PASSWORD, 16))
    if (memcmp(&BufInKadr_1[5],PASSWR1,14))
      err |= 4;

   // ����஫� ⨯� ���稪�
   unsigned char sensType = BufInKadr_1[21];
   if (sensType > 4)
      err |= 8;

   // ����p��� ����p� �窨
   unsigned char nPoint = BufInKadr_1[22];
   if((nPoint < 1) || (nPoint > 5))
      err |= 16;

   // �訡�� ��p����p�� ���p��
   if(err)
      SaveMessagePrefix_1(6, 0xFF);
   // ����� � ��ଥ
   else {
      if (sensType < 4) { ///////// ��, �஬� ���⭮���
                                   // ��������� ���� ���祭��
         fTmp1 = GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].code;
         fTmp2 = GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].value;
                                        // ��⠭����� ����
         GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].code =
            *(float*)(BufInKadr_1 + 23);
         GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[nPoint-1].value =
            *(float*)(BufInKadr_1 + 27);

         if(Mem_ConfigWr() == 0) {
            SaveMessagePrefix_1(6, 0xCC);
            CalibrateIni();
//          SetChanToHartAdr(nRun-1);   // ���⠢��� ����� ������� � ����

            if (fTmp1 != *(float*)(BufInKadr_1 + 23) ||  // ���祭��
                fTmp2 != *(float*)(BufInKadr_1 + 27)) {  // ����������
               switch(sensType) {         // ������ � ��娢 ����⥫���
                  case 0:
                     MEMAUDIT_1.type = 134; // ⥬������
                     break;
                  case 1:
                     MEMAUDIT_1.type = 133; // ��������
                     break;
                  case 2:
                     MEMAUDIT_1.type = 136; // ��९�� ���孨�
                     break;
                  case 3:
                     MEMAUDIT_1.type = 137; // ��९�� ������
                     break;
               }                          // ����� ���祭�� - ⨯ ���稪�
               MEMAUDIT_1.altvalue = sensType;
               MEMAUDIT_1.newvalue = sensType;
	       Mem_AuditWr_1(Mem_GetAuditPage(nRun-1));//������ ���� ����⥫���
            }
         }
         else
           SaveMessagePrefix_1(6, 0xFF);
      }
      else {   ///////////////////////////// ���⭮���

         fTmp1 = GLCONFIG.TauTable[nPoint-1];
         fTmp2 = GLCONFIG.RoTable[nPoint-1];
                                        // ��⠭����� ����
         GLCONFIG.TauTable[nPoint-1] = *(float*)(BufInKadr_1 + 23);
         GLCONFIG.RoTable[nPoint-1]  = *(float*)(BufInKadr_1 + 27);

//       if (nPoint == 1) {
//          fTmp1 = 1.0e+6/GLCONFIG.Tau_CH4c;
//          fTmp1 = pcc->ROnom ;    // ���⭮���
//          fTmp2 = GLCONFIG.Tau_CH4c;
//                                      // ��⠭����� ����
//          GLCONFIG.Tau_CH4c = *(float*)(BufInKadr_1 + 27);
//       }
//       else {
//          fTmp1 = 1.0e+6/GLCONFIG.Tau_N2c;
//          fTmp1 = pcc->ROnom ;    // ���⭮���
//          fTmp2 = GLCONFIG.Tau_N2c;
//                                      // ��⠭����� ����
//          GLCONFIG.Tau_N2c = *(float*)(BufInKadr_1 + 27);
//       }
         if (Mem_ConfigWr() == 0) {     // ������� � ���䨣����
            SaveMessagePrefix_1(6, 0xCC);
//          Disp_Density_0();
            Get_Calibr_Density();       // ��� १���⮢ �����஢��

            if (fTmp2 != *(float*)(BufInKadr_1 + 27)) {  // ����������
               MEMAUDIT_1.type = 138;                    // ���⭮���
               MEMAUDIT_1.altvalue = sensType;
               MEMAUDIT_1.newvalue = sensType;
	       Mem_AuditWr_1(Mem_GetAuditPage(nRun-1));  // ������ ���� ����⥫���
            }
         }
         else
           SaveMessagePrefix_1(6, 0xFF);
      }
   }
}
*/
/////////////////////////////////////////////
// �⢥� �� ����� �⥭�� ⠡���� �����஢��
/////////////////////////////////////////////
void AnswClbrTable_1()
{
 AnswClbrTablep(BufInKadr_1,BufTrans_1);
}
/*
   int err = 0;

   if(BufInKadr_1[2] != 8)                       // ����p��� ����� ���p��
      err |= 1;

   unsigned char nRun = BufInKadr_1[4];          // ����p��� ����p� ��⪨
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;

   unsigned char sensType = BufInKadr_1[5];      // ����஫� ⨯� ���稪�
   if (sensType > 4)
      err |= 4;

   if (err)                                     // �訡�� ��p����p�� ���p��
      SaveMessagePrefix_1(6, 0xFF);
   else
   {
      if (sensType < 4) {  ///////////////////////// ��, �஬� ���⭮���
         SaveMessagePrefix_1(48, 0xC9);             // ������ �p�䨪�
         BufTrans_1[4] = nRun;                      // ����p ��⪨

         BufTrans_1[5] = sensType |                  // ��� ���稪� + ����䥩�
                      (GLCONFIG.IDRUN[nRun-1].sensData[sensType].nInterface << 4);
         for(int i = 0; i < 5; i++) {              // ������ �����஢��
            *(float*)(BufTrans_1 + 6 + 4*i) =
               GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[i].code;
            *(float*)(BufTrans_1 + 26 + 4*i) =
               GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[i].value;
         }
      }
      else {               ///////////////////////// ���⭮���
         SaveMessagePrefix_1(48, 0xC9);             // ������ �p�䨪�
         BufTrans_1[4] = nRun;                      // ����p ��⪨

//         BufTrans_1[5] = sensType | 0x40;           // ��� ���稪� + ����䥩�
         BufTrans_1[5] = sensType |                  // ��� ���稪� + ����䥩�
             (GLCONFIG.IDRUN[nRun-1].InterfaceDens << 4);

         for(int i = 0; i < 5; i++) {             // ������ �����஢��
            *(float*)(BufTrans_1 + 6 + 4*i)  =  GLCONFIG.TauTable[i];
            *(float*)(BufTrans_1 + 26 + 4*i) =  GLCONFIG.RoTable[i];
         }
                                                  // 2 �窨
//       *(float*)(BufTrans_1 +  6) = 1.0e+6/GLCONFIG.Tau_CH4c;
//       *(float*)(BufTrans_1 +  6) = pcc->ROnom ;    // ���⭮���
//       *(float*)(BufTrans_1 + 26) = GLCONFIG.Tau_CH4c;
//
//       *(float*)(BufTrans_1 + 10) = 1.0e+6/GLCONFIG.Tau_N2c;
//       *(float*)(BufTrans_1 + 10)  = pcc->ROnom ;    // ���⭮���
//       *(float*)(BufTrans_1 + 30) = GLCONFIG.Tau_N2c;
      }
   }
}
*/
///////////////////////////////////////
// ������ ⠡���� �����஢��
///////////////////////////////////////
void WrClbrTable_1()
{
 WrClbrTablep(BufInKadr_1,BufTrans_1,1);
}
/*
   int num,i,err = 0;
   unsigned char nRun,sensType,interfType,cFlag,MaxRun;
   float fTmp1[5],fTmp2[5],Tmp;
   unsigned char flagStart;
   flagStart = 0xFF;

//  if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

   if(BufInKadr_1[2] != 64)     // ����p��� ����� ���p��
      err |= 1;
   nRun = BufInKadr_1[4];       // ����p��� ����p� ��⪨
   if((nRun < 1) || (nRun > GLCONFIG.NumRun))
      err |= 2;
//   if(memcmp(&BufInKadr_1[5], GLCONFIG.PASSWORD, 16)) // �p���p�� ��p���
    if (memcmp(&BufInKadr_1[5],PASSWR1,14))
      err |= 4;

   sensType = BufInKadr_1[21] & 0xF;    // �஢�ઠ ����� ��ࠬ��� � ����䥩�
   interfType = (BufInKadr_1[21] & 0xF0) >> 4;

   if (sensType > 4)           // ����p��� ����� ��ࠬ���  !!!!!!
      err |= 8;               // � ��᫥����� ������ ����� 㢥�������
   if(interfType > 5)         // ����p��� ����� ����䥩� !!!!!!
      err |= 16;

   if(err)                    // �訡�� ��p����p�� ���p��
      SaveMessagePrefix_1(6, 0xFF);
   else
   {                                          // ��� ����䥩�
      if (sensType < 4) {      // ��, �஬� ���⭮���
         for(int i = 0; i < 5; i++) {     // ��������� ���� ���祭��
            fTmp1[i] = GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[i].code;
            fTmp2[i] = GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[i].value;
         }
	 GLCONFIG.IDRUN[nRun-1].sensData[sensType].nInterface = interfType;
         for(i = 0; i < 5; i++) {     // ������ �����஢��
            GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[i].code =
              *(float*)(BufInKadr_1 + 22 + 4*i);
            GLCONFIG.IDRUN[nRun-1].sensData[sensType].aTable[i].value =
              *(float*)(BufInKadr_1 + 42 + 4*i);
         }

         if(Mem_ConfigWr() == 0) {        // �ᯥ譮 ����ᠭ� � ���
            SaveMessagePrefix_1(6, 0xCA);
            CalibrateIni();
//          SetChanToHartAdr(nRun-1);     // ���⠢��� ����� ������� � ����

            cFlag = 0;                    // ��ᬮ���� ���������� �� ���祭��
            for (i=0; i<5; i++) {
               if (fTmp1[i] != *(float*)(BufInKadr_1 + 22 + 4*i) ||
                   fTmp2[i] != *(float*)(BufInKadr_1 + 42 + 4*i))
               {
                  cFlag = 1;
                  Tmp = *(float*)(BufInKadr_1 + 42 + 4*i);
                  break;
               }
            }
            if (cFlag==1) {                  // ���祭�� ����������
               switch(sensType) {         // ������ � ��娢 ����⥫���
                  case 0:
                     MEMAUDIT_1.type = 134; // ⥬������
                     MEMSECUR_1.mode  =14;
                     break;
                  case 1:
                     MEMAUDIT_1.type = 133; // ��������
                     MEMSECUR_1.mode  =13;
                     break;
                  case 2:
                     MEMAUDIT_1.type = 136; // ��९�� ���孨�
                     MEMSECUR_1.mode  =16;
                     break;
                  case 3:
                     MEMAUDIT_1.type = 137; // ��९�� ������
                     MEMSECUR_1.mode  =17;
                     break;
               }                          // ����� ���祭�� - ⨯ ���稪�
               MEMAUDIT_1.altvalue = fTmp2[i];
               MEMAUDIT_1.newvalue = Tmp;
                                          // �������
	       Mem_AuditWr_1(Mem_GetAuditPage(nRun-1));
               strncpy(MEMSECUR_1.login,Tlogin1,4);
	       MEMSECUR_1.TIMEBEG = BINTIME;
	       memset(&MEMSECUR_1.TIMEEND,0,6);
               MEMSECUR_1.Port = nRun;
               num = Mem_SecurWr_1(Mem_SecurGetPage(),0);
            }
         }
         else
           SaveMessagePrefix_1(6, 0xFF);
      }
      else {    ///////////////////////// // ���⭮���
                                          // ��������� ���� ���祭��
	 if (GLCONFIG.IDRUN[nRun-1].sensData[sensType].nInterface != interfType)
	 {  //  flagStart=0; //䫠� ������� ���᫨⥫�,
          for (i=0; i<3; i++)   // �����࠭��� ���� ������ ���⭮��� �� �� ��⪨
	  { GLCONFIG.IDRUN[i].InterfaceDens= interfType;  // nRun-1
            GLCONFIG.IDRUN[i].sensData[4].nInterface =  interfType;
            ADCONFIG.ADRUN[i].ROdens = GLCONFIG.IDRUN[i].ROnom;
          }
         }
         for (int i = 0; i < 5; i++) {     // ��������� ���� ���祭��
            fTmp1[i] = GLCONFIG.TauTable[i];
            fTmp2[i] = GLCONFIG.RoTable[i];
            GLCONFIG.TauTable[i]  = *(float*)(BufInKadr_1 + 22 + 4*i);
            GLCONFIG.RoTable[i]   = *(float*)(BufInKadr_1 + 42 + 4*i);
         }
         for(i = 0; i < 4; i++) {     // ������ �����஢��
            m_pCoeff[nRun-1][4][i].codeLow = GLCONFIG.TauTable[i];
            m_pCoeff[nRun-1][4][i].valueLow = GLCONFIG.RoTable[i];
            float diffCode = GLCONFIG.TauTable[i+1]
                             - GLCONFIG.TauTable[i];
            m_pCoeff[nRun-1][4][i].slope = diffCode > 0 ?
               (GLCONFIG.RoTable[i+1] -
                GLCONFIG.RoTable[i]) / diffCode : 0;
         }

         if (Mem_ConfigWr() == 0) {        // �ᯥ譮 ����ᠭ� � ���
            SaveMessagePrefix_1(6, 0xCA);
            Get_Calibr_Density();         // ��� १���⮢ �����஢��
//          CalibrateIni();
//          SetChanToHartAdr(nRun-1);     // ���⠢��� ����� ������� � ����

            cFlag = 0;                    // ��ᬮ���� ���������� �� ���祭��
            for (i=0; i<5; i++) {
               if (fTmp1[i] != *(float*)(BufInKadr_1 + 22 + 4*i) ||
                   fTmp2[i] != *(float*)(BufInKadr_1 + 42 + 4*i)) {
                  cFlag = 1;
                  Tmp = *(float*)(BufInKadr_1 + 42 + 4*i);
                  break;
               }
            }
            if (cFlag==1) {                  // ���祭�� ����������
               MEMAUDIT_1.type = 138;          // ���⭮���
               MEMSECUR_1.mode  =18;

               MEMAUDIT_1.altvalue = fTmp2[i];
               MEMAUDIT_1.newvalue = Tmp;
                                          // �������  �� �� ��⪨
	       MaxRun = (GLCONFIG.NumRun < 4) ? GLCONFIG.NumRun : 3;
	       for (i=0; i<MaxRun; i++)   // ࠧ�᫠�� ����� �� ��⪠� � ���⭮��஬
		  if (GLCONFIG.IDRUN[i].Dens)
		    { Mem_AuditWr_1(Mem_GetAuditPage(i));
                      num = i;
                    }
	       strncpy(MEMSECUR_1.login,Tlogin1,4);
	       MEMSECUR_1.TIMEBEG = BINTIME;
	       memset(&MEMSECUR_1.TIMEEND,0,6);
               MEMSECUR_1.Port = num;
               num = Mem_SecurWr_1(Mem_SecurGetPage(),0);
            }
         }
         else
            SaveMessagePrefix_1(6, 0xFF);
//////////////////////////////////////////////////////////
      }  // ����� �� ���⭮����
   }
	 if (flagStart ==0){n_delay(1); StartCPU = 0;} //�������
}
*/
//------------------------------
void AnswerHCom_1() {
   unsigned char nRun, err = 0;

   if (BufInKadr_1[2] != 22)            // ����p��� ����� ���p��
      err |= 1;
                                        // �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[4], GLCONFIG.PASSWORD, 16))
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 2;

   if (err)                             // �訡�� ��p����p�� ���p��
      SaveMessagePrefix_1(6, 0xFF);
   else {                               // ����� � ��ଥ
      Flag_Hartservice_1 = 1;           // ��⠭���� ०��� HART<->COM1
                                        // ���뫪� ���⢥ত���� �� ���1
      SaveMessagePrefix_1(6,0xCF);      // �⢥�: ०�� HART<->COM1 ��⠭�����
       MEMSECUR.Port = 1;
      InTimeQueue(20,0,Mem_AlarmHartComOn); // ��䨪�஢��� � ��娢�
   }
}
//---------------------------------------------------------------------------
void porteven1()
{/*    outportb(comport1+3,0x80);        // ��p������� �� ���. ����⥫� �����
     _AX=96;                            // �� ᪮���� 1200
     outportb(comport1,_AL);           // ��. ���� ����⥫�
     _AX=96;
     outportb(comport1+1,_AH);         // ��. ���� ����⥫�
*/
     outportb(comport1+3,0x1F);           // �p��� ��p����   even,8,2
}
void AnswerHComY_1() {
   unsigned char nRun, err = 0;

   if (BufInKadr_1[2] != 22)            // ����p��� ����� ���p��
      err |= 1;
                                        // �p���p�� ��p���
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 2;

   if (err)                             // �訡�� ��p����p�� ���p��
      SaveMessagePrefix_1(6, 0xFF);
   else {                               // ����� � ��ଥ
      Flag_Hartservice_1 = 1;           // ��⠭���� ०��� HART<->COM1
 // ���뫪� ���⢥ত���� �� ���1
      SaveMessagePrefix_1(6,0xCE);      // �⢥�: ०�� HART<->COM1 ��⠭�����
//            CheckBufTrans_1();
//            Ini_Send_1();
       MEMSECUR.Port = 1;
      InTimeQueue(20,0,Mem_AlarmHartComOn); // ��䨪�஢��� � ��娢�
      InTimeQueue(22,0,porteven1);
//   outportb(comport1+3,0x1F);           // �p��� ��p����   even,8,2
         }
}
//---------------------------------------------------------------------------
void SearchKadr_Hartservice_1()  //���� �p���⮣� ���p� � ���p� �p����
{
   extern struct glconfig  GLCONFIG;
   extern unsigned char    hart_buf_out[],hart_buf_out2[];
   unsigned char           *pb,
                           count;         // ����� ���p�
   unsigned char           HartKadr[256],
                           ksum=0;        // �ଠ� �������
   int                    Spd, cinindex, s;      // ����� ����� ����

   cinindex = inindex_1;
   inindex_1 = 0;
//   count = 0;
   pb = 0;            // 㪠��⥫� �� ��砫� ���p�
   for (int i = 0; i < cinindex-5; i++) {
      if (BufResiv_1[i]==0xff && BufResiv_1[i+1]==0xff
            && (BufResiv_1[i+2]==0x82 || BufResiv_1[i+2]==0x02 || BufResiv_1[i+2]==0xC1)) {
         pb = &BufResiv_1[i+2];             // ��砫�� ���� ����
         if (BufResiv_1[i+2]==0x82) {
            count = pb[7] + 8;       // ����� �������� ���p� ��� ��
         // HartFormat = 18;
         }
         else {
            count = pb[3] + 4;       // ����� ���⪮�� ���p� ��� ��
         // HartFormat = 14;
         }
         HartFormat = count+9;
         break;                         // ���p �뤥���
      }
   }
    if (pb != 0 && ((BufResiv_1 + cinindex - pb) >= count) || (count>=64)){
//   if (pb != 0 && (BufResiv_1 + cinindex - pb) >= count) {
      for (int i=0; i<count; i++) {
         ksum ^= pb[i];                 // ����p��쭠� �㬬�
      }
//      if (ksum == pb[count]) {              // ��pp��⭠� �㬬�
//         if (pb[count-2] == 0x7E) {         // �-�� �� �⬥�� ०��� HART<->COM1
      {// if ((pb[count-2] == 0x7E)||(pb[6] == 0xBF)) {// �-�� �� �⬥�� ०��� HART<->COM
        if (( ResetHartS)||(pb[count-2] == 0x7E)|| (pb[6] == 0xBF)&&(pb[7] == 0xC0) ||
        ((pb[7] == 0xFC)&&(pb[6] == 0x01))||(pb[0] == 0xC1)) {
//printf("buf= %d %d %d %d %d\n",pb[3], pb[4],pb[5], pb[6],pb[7]);
            Flag_Hartservice_1 = 0;     // ��� ०��� HART<->COM1
            ResetHartS = 0;             // ��室 �� ०��� HART<->COM �⬥����
            outportb(comport1+3,0x07);           // �p��� ��p����   n,8,2, �����
            HartFormat = LHARTCOM;            // ������ �-��
            if (hartnum > hartnum2)
            HartIni();                  // ������������� ���p�� ������ � ���稪��
                                        // ��䨪�஢��� � ��娢�
            if ((hart2 == 0)&&(hartnum2) && (rs485num == 0))
             HartIni2();
            InTimeQueue(40,0,Mem_AlarmHartComOff);
            SaveMessagePrefix_1(6,0xFE);// �⢥�: ०�� HART<->COM1 �⬥���
            CheckBufTrans_1();
            Ini_Send_1();
            inindex_1 = 0;
            InTimeQueue(40,0,Ini_Reciv_1);
         }
         else {                         // ��᫠�� � HART
         if (hartnum > hartnum2)
          {   memmove(hart_buf_out,BufResiv_1,cinindex); // ����p������ ���p�
//ComPrint_2("Hartservice_1\n");
            HartIni_Hartservice_1();  // ����� �� HART1
          }
          if ((hart2 == 0)&&(hartnum2) && (rs485num == 0))
            { memmove(hart_buf_out2,BufResiv_1,cinindex);
              HartIni_Hartservice_1_2();// ����� �� HART2
            }
         }
      }
      }
   }
//-----------------------------------------------
//���樠������ ��p���� ���p�� ᮮ�饭�� �� �p�p뢠��� ��p����稪�
void Ini_SendHCom_1() {

   outindex_1 = 0;                        // ᬥ饭�� � ��p��������� ���p�
   outportb(comport1+4,0x0B);             // DTR + RTS + �p�p뢠���
   outportb (comport1+1,0x02);            // p��p���� �p�p뢠��� ��p����稪�
//   outport(0xFF40,(inport(0xFF40) & 0xFFF7));  //p��p���� IRQ4
//   outportb(0x21,(inportb(0x21) & 0xEF)); // p��p���� IRQ4
}

//--------------------------------------------------------
void RdDiaph_1() {          // �� ��ࠬ��஢ �����.   - ���
 RdDiaphp(BufInKadr_1,BufTrans_1);
}
/*
   struct rddiaphconst  *p;
   struct IdRun      *prun;              // ��p����p� ��⪨
   unsigned char     num,KodZ;
   unsigned char *pc;
   int           err = 0;
   float RoFU,N2FU,CO2FU,TeplFU,PbFU;
   float Ep,UHeat;
   char str[50];

   num = 3 & BufInKadr_1[4];
   prun = &GLCONFIG.IDRUN[num-1];    // 㪠��⥫� �� ��p����p� ��⪨
  if ((NumCal == INTPAR.RS485Com5Speed) && (NumCal != GLCONFIG.NumCal))
  {
    if (BufInKadr_1[2] != 40 ) err |= 1;//����p��� ����� ���p��
    if (num > GLCONFIG.NumRun)  err |= 2;//����p��� ����p� ��⪨
    KodZ = BufInKadr_1[5];
    if ((KodZ & 0x01) != 0)
    { pc = &BufInKadr_1[22];
      RoFU = *(float *)pc;
      if (RoFU < 0.55)         // ������ �।�� ���������
             err |= 8;
      if (RoFU > 1.13)           // ��� ��� ��⮤��
       err |= 16;
    }
//sprintf(str,"NumCal=%u err=%04X\n",NumCal,err);
//ComPrint_2(str);

    if(err)
    { SaveMessagePrefix_1(6,0xFF);  //����᪨� ��p����p� �� ����ᠭ�
      BufTrans_1[1] = NumCal;
    }
  else
    {

     if ((KodZ & 0x02) != 0)
     { pc = &BufInKadr_1[26];
       CO2FU = *(float *)pc;
     }
     if ((KodZ & 0x04) != 0)
     { pc = &BufInKadr_1[30];
       N2FU = *(float *)pc;
     }
//     pc = &BufInKadr_1[81];
//     TeplFU = *(float *)pc;
     if ((KodZ & 0x08) != 0)
     {  pc = &BufInKadr_1[34];
        PbFU = *(float *)pc;
     }
//printf("RoFU=%f CO2FU=%f N2FU=%f TeplFU=%f\n");
      if ((prun->RoVV  != RoFU) && ((KodZ & 0x01) != 0))
        { MEMAUDIT_1.type     = 1; //���⭮��� ���� �� �.�., ��/�^3
          MEMAUDIT_1.altvalue = prun->RoVV;
          prun->ROnom      = RoFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];
          prun->RoVV      = RoFU;
          MEMAUDIT_1.newvalue = prun->RoVV;
          CalcZc[num-1] = 1;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
/----------------------------------------*
     UHeat =  TeplFU;    // � ���*�
//      TeplFU = Round_toKg(UHeat,KoefEn[GLCONFIG.EDIZM_T.EdIzmE],&Ep); // �� MEGAS� � ���
//printf("UHeat=%f Heat=%f HsVV=%f dh=%f Ep=%f\n",UHeat,p->UHeat,run->HsVV, run->Heat  - p->UHeat,Ep);
      Ep = 1e-3;
      if (fabs(prun->HsVV  - UHeat)>Ep)
      { MEMAUDIT_1.type     = 37;
        MEMAUDIT_1.altvalue = prun->HsVV; //*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
        prun->Heat  = TeplFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
        MEMAUDIT_1.newvalue = UHeat;
        prun->HsVV = UHeat;
        CalcZc[num-1]=prun->nGerg88_AGA8;
                                    // ������ ���� ����⥫���
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      }
      else
       { prun->HsVV = UHeat;
         prun->Heat  = TeplFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
       }
*-------------------------------------------/
      if ((prun->NCO2  != CO2FU) && ((KodZ & 0x02) != 0))
        { MEMAUDIT_1.type     = 2;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          MEMAUDIT_1.altvalue = prun->NCO2;
          prun->NCO2   = CO2FU;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          MEMAUDIT_1.newvalue = prun->NCO2;
         CalcZc[num-1] = 1;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if ((prun->NN2  != N2FU) && ((KodZ & 0x04) != 0))
        { MEMAUDIT_1.type     = 3;
          MEMAUDIT_1.altvalue = prun->NN2;
          prun->NN2    = N2FU; //����p��� ���� ���� � ������� ᬥ�, %
          MEMAUDIT_1.newvalue = prun->NN2;
         CalcZc[num-1] = 1;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if ((prun->Pb  != PbFU) && ((KodZ & 0x08) != 0))
        { MEMAUDIT_1.type     = 6;  //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT_1.altvalue = prun->Pb;
          prun->Pb     = PbFU;    //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT_1.newvalue = prun->Pb;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���

      if( Mem_ConfigWr() == 0 )         // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
        SaveMessagePrefix_1(6,0x8A);    // ����.����᪨� ��p����p� ����ᠭ�
      else
        SaveMessagePrefix_1(6,0xFF);    // ����᪨� ��p����p� �� ����ᠭ�
      BufTrans_1[1] = NumCal;
    }
   }   //����� if ((NumCal == INTPAR.RS485Com5Speed) && (NumCal != GLCONFIG.NumCal))
   else
   {
    if (BufInKadr_1[2] != 7)
      err |= 1;                          // ����p��� ����� ���p��

    if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      p    = &SEND_1.RDDIAPHCONST;        // ����⠭��

      p->Rn        = prun->Rn;          // ࠤ��� ����㣫����
      p->TauP      = prun->TauP;        // ��ਮ� �஢�ન

      p->Reserve_2 = 0.0;
      p->Mon       = BINTIME.month;
      p->Day       = BINTIME.date;
      p->Yea       = BINTIME.year;
      p->Hou       = BINTIME.hours;
      p->Min       = BINTIME.minutes;
      p->Sec       = BINTIME.seconds;

      SaveMessagePrefix_1(sizeof(rddiaphconst)+7,0x8A); // ������ �p�䨪�
      BufTrans_1[4] = num;                              // ����p ��⪨
      movmem(&SEND_1,&BufTrans_1[5],sizeof(rddiaphconst));// ������ ���p�
   }
  }
}
*/
//--------------------------------------------------------
void WrDiaph_1() {          // �� ��ࠬ��஢ �����.   - ���
 WrDiaphp(BufInKadr_1,BufTrans_1,1);
}
/*
   struct wrdiaphconst  *p;
   struct IdRun      *prun;              // ��p����p� ��⪨
   unsigned char     num;
   int           err = 0;

//  if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

   if (BufInKadr_1[2]-23 != sizeof(wrdiaphconst))
      err |= 1;                          // ����p��� ����� ���p��
//fclose(fp);
   num = 3 & BufInKadr_1[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                          // ����p��� ����p� ��⪨
                                         // �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[5],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[5],PASSWR1,14))
      err |= 4;

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // ��ࠬ���� ����ࠣ�� �� ����ᠭ�
   else
   if (Mem_ConfigWr() == 0)  {        // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
      p   = &RESIV_1.WRDIAPHCONST;      // ��ࠬ���� ����ࠣ��
      prun = &GLCONFIG.IDRUN[num-1];  // 㪠��⥫� �� ��p����p� ��⪨
      movmem(&BufInKadr_1[4+17],&RESIV_1,sizeof(wrdiaphconst));//��p������� ��p����p��
//if (Fpermit(1,p,prun)) {}
//else
  {    if (prun->Rn != p->Rn) {
         MEMAUDIT_1.type     = 13;
         MEMAUDIT_1.altvalue = prun->Rn;
         prun->Rn   = p->Rn;                  // ࠤ��� ����㣫����
         MEMAUDIT_1.newvalue = prun->Rn;
	 Mem_AuditWr_1(Mem_GetAuditPage(num-1));// ������ ���� ����⥫���
      }
      if (prun->TauP != p->TauP) {
         MEMAUDIT_1.type     = 14;
         MEMAUDIT_1.altvalue = prun->TauP;
         prun->TauP    = p->TauP;             // ��ਮ� �஢�ન
         MEMAUDIT_1.newvalue = prun->TauP;
	 Mem_AuditWr_1(Mem_GetAuditPage(num-1));// ������ ���� ����⥫���
      }
   }
      Mem_ConfigWr();
      SaveMessagePrefix_1(6,0x8B);  // ��ࠬ���� ����ࠣ��
     }
     else
     SaveMessagePrefix_1(6,0xFF);  // ��ࠬ���� ����ࠣ��
   }
//--------------------------------------------------
/*
void RdNoAlarmConstFlag_1() {             // �⥭�� 䫠�� ����� ���਩
   struct rdnoalarmconstflag *p;
   struct IdRun *prun;                  // ��p����p� ��⪨
   unsigned char num;
   int err = 0;

   if (BufInKadr_1[2] != 7)
      err |= 1;                          // ����p��� ����� ���p��

   num = 3 & BufInKadr_1[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      p    = &SEND_1.RDNOALARMCONSTFLAG;  // 䫠� ����� ���਩ �� ����⠭��
      prun = &GLCONFIG.IDRUN[num-1];    // 㪠��⥫� �� ��p����p� ��⪨

      p->Flag = prun->NoAlarmConstFlag; // 䫠� ����� ���਩ �� ����⠭��
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(rdnoalarmconstflag)+7,0xD0);
      BufTrans_1[4] = num;                // ����p ��⪨
                                        // ������ ���p�
      movmem(&SEND_1,&BufTrans_1[5],sizeof(rdnoalarmconstflag));
   }
}

//--------------------------------------------------
void WrNoAlarmConstFlag_1() {           // ������ 䫠�� ����� ���਩
   struct wrnoalarmconstflag *p;
   struct IdRun *prun;                  // ��p����p� ��⪨
   unsigned char num;
   int err = 0;

//   if (Nwr1 == 0)
//      err |= 0x80;                      // 䫠� ����� �������

   if (BufInKadr_1[2]-23 != sizeof(wrnoalarmconstflag))
      err |= 1;                         // ����p��� ����� ���p��

   num = 3 & BufInKadr_1[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨
                                        // �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[5],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[5],PASSWR1,14))
      err |= 4;

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // ����⠭�� �� ����ᠭ�
   else
   {   p   = &RESIV_1.WRNOALARMCONSTFLAG;  // ����⠭��
      prun = &GLCONFIG.IDRUN[num-1];    // 㪠��⥫� �� ��p����p� ��⪨
                                        // ��p������� ��p����p��
      movmem(&BufInKadr_1[4+17],&RESIV_1,sizeof(wrnoalarmconstflag));
     if (Mem_ConfigWr() == 0)            // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
       SaveMessagePrefix_1(6,0xD1);        // 䫠� ����ᠭ
     else
       SaveMessagePrefix_1(6,0xFF);        // 䫠� �� ����ᠭ
   }
}
*/
//--------------------------------------------------------
/*
void RdPeriodChr_1() {                    // �� �����. ��ࠬ��஢  - ���
   static unsigned int    startpoint_1,endpoint_1;
   struct bintime         *pbtime;
   struct ReqPeriod       *prp;         // ��p���p� ���p��
   unsigned char          num;
   unsigned char          page;
   struct periodrecordchr *pprec;       // ��p���p� ���p�⨢��� ������ ��� ��p����
   unsigned int           point;
   int                    count;
   int err = 0;

   if (BufInKadr_1[2]-sizeof(ReqPeriod) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pprec = &SEND_1.RESPONSEPERIODCHR.PERIODRECORDCHR[0];

   prp = &RESIV_1.REQPERIOD;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prp,sizeof(ReqPeriod));

   if (prp->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prp->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prp->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }                                    // ����� �����
   STARTENDTIME_1.RequestNum = prp->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨
                                        // ��p���⪠ 1-�� ���p��
   if (STARTENDTIME_1.RequestNum == 0) {
      pbtime = &STARTENDTIME_1.STARTTIME;

      pbtime->seconds = 0;
      pbtime->minutes = prp->StartMinutes;
      pbtime->hours   = prp->StartHours;
      pbtime->date    = prp->StartDay;
      pbtime->month   = prp->StartMonth;
      pbtime->year    = prp->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                   // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = prp->EndMinutes+GLCONFIG.Period-1;

      if (pbtime->minutes > 59)
         pbtime->minutes = 59;

      pbtime->hours   = prp->EndHours;
      pbtime->date    = prp->EndDay;
      pbtime->month   = prp->EndMonth;
      pbtime->year    = prp->EndYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��
                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                     // ��p�����᪨� ����
         page = GetPage(STARTENDTIME_1.num,0);
                                     // ��砫�� ������
         startpoint_1 = MemPerioddataSearchStart2_1(page);

         if (startpoint_1 < ARC4)    //{    // ����� ����
            SetBinTime4(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC4-1) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = MemPerioddataSearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC4)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = MemPerioddataSearchEnd_1(page);

         if ((startpoint_1 > ARC4-1) || (endpoint_1 > ARC4-1))
            err |= 0x40;
      }
      if (err == 0) {                // �⥭�� 1-� ����� ��ਮ���᪮�� ����
         MemPerioddataRd_1(startpoint_1,page);
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1()) // �訡��, �᫨ ���⮢�� �६� � ��ࢮ� ����� ��娢�
            err |= 0x80;             // �ॢ�蠥� ����筮� �६� � �����
      }
   }                                 // ����� ��p���⪨ ��p���� ���p��


   if (err) {
      if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x95);// ������ �p�䨪�
         BufTrans_1[4] = 3 & num;      // ����p ��⪨
         BufTrans_1[5] = 0;            // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;            // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // ��p�����᪨� ����
      page  = GetPage(STARTENDTIME_1.num,0);
      point = NextIndex_1(startpoint_1,9*STARTENDTIME_1.RequestNum,page);

      count = 0;
      if (point < ARC4) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<9;i++) {
            count++;                    // �p������� ��p�������
            if (i) {
               point = NextIndex_1(point,1,page);
               if (point > ARC4-1)
                  break;                // ����� ���᪠
            }                           // �⥭�� ����� ��p�����᪮�� ����
            MemPerioddataRd_1(point,page);

            pbtime = &MEMDATA_1.STARTTIME;
                                     // �����
            pprec->StartMonth   = pbtime->month;
            pprec->StartDay     = pbtime->date;
            pprec->StartYear    = pbtime->year;
            pprec->StartHours   = pbtime->hours;
            pprec->StartMinutes = pbtime->minutes;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
       Vl.Val=MEMDATA_1.Heat;
       w=Vl.Bt[0]&0x1;

            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
                                     // ᯮᮡ ����p���� �� ��⪥
               pprec->StartDay |= 0x80;
                                     // ��p��� ������� � ������
            pprec->Period   = MEMDATA_1.count;
                                     // ���� ⥯���
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            pprec->FullHeat   = MEMDATA_1.FullHeat*Koef/1.0e3;
                                     // �।��� 㤥�쭠� ⥯���
            pprec->Heat     = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
                Vl.Val=pprec->Heat;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pprec->Heat =  Vl.Val;

                                     // �।��� ���⭮���
            pprec->Ro       = MEMDATA_1.RO;
                                     // �।��� ᮤ�ঠ��� ����
            pprec->N2       = MEMDATA_1.N2;
                                     // �।��� ᮤ�ঠ��� 㣫���᫮��
            pprec->CO2      = MEMDATA_1.CO2;

            pprec++;

            if (point == endpoint_1)
               break;                // ����� ���᪠
         }
      }                              // ���-�� ����ᥩ
      SEND_1.RESPONSEPERIODCHR.NumRecord = count;

      if ((point == endpoint_1) || (count==0) || (point > ARC4-1))
                                     // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEPERIODCHR.ResponseStatus = 0;
      else                           // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEPERIODCHR.ResponseStatus = 1;
                                     // ������ �p�䨪�
      SaveMessagePrefix_1 (count*sizeof(struct periodrecordchr)+9,0x90);
                                     // ������ ���p�
      movmem (&SEND_1.RESPONSEPERIODCHR,&BufTrans_1[5],
         count * sizeof(struct periodrecordchr)+2);

      BufTrans_1[4] = 3 & num  | (GLCONFIG.IDRUN[num-1].Dens<<5);         // ����p ��⪨
   }
}
*/
//--------------------------------------------------------
/*
void RdDayChr_1() {                       // �� ������ ��ࠬ��஢ �஬�⮣��
   static unsigned int  startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point;
   int                 err;
   int                 count;
   struct reqdaily       *prd;          // ��p���p� ���p�� - �⥭�� ������ ������
   struct dailyrecordchr *pdr;          // ��p���p� ����� ������ ��� ��p����

   err = 0;

   if (BufInKadr_1[2]-sizeof(reqdaily) != 7)
     err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4] & 3;

   if ((num & 3) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pdr = &SEND_1.RESPONSEDAILYCHR.DAILYRECORDCHR[0];
   prd = &RESIV_1.REQDAILY;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prd,sizeof(reqdaily));

   if (prd->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥��� �����
     if (STARTENDTIME_1.RequestNum == prd->RequestNum)
        return;

     if (STARTENDTIME_1.RequestNum +1 != prd->RequestNum)
        err |= 0x04;                    // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prd->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

                                        // ��p���⪠ 1-�� ���p��
   if (STARTENDTIME_1.RequestNum == 0) {
      pbtime = &STARTENDTIME_1.STARTTIME;

      pbtime->seconds = 0;
      pbtime->minutes = 0;
                                     // ����p���� ��;
      pbtime->hours   = GLCONFIG.ContrHour;
      pbtime->date    = prd->StartDay;
      pbtime->month   = prd->StartMonth;
      pbtime->year    = prd->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                   // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = 59;
                                     // ����p���� ��
      pbtime->hours   = (GLCONFIG.ContrHour) ? GLCONFIG.ContrHour-1 : 23;

      if (GLCONFIG.ContrHour)
         dayPlus(prd->EndDay,prd->EndMonth,prd->EndYear,
                 pbtime->date,pbtime->month,pbtime->year);
      else {
         pbtime->date    = prd->EndDay;
         pbtime->month   = prd->EndMonth;
         pbtime->year    = prd->EndYear;
      }
      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��
                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if ( err == 0) {
                                     // ����� ����
         page = GetPage(STARTENDTIME_1.num,2);
                                     // ��砫�� ������
         startpoint_1 = Mem_DaySearchStart2_1(page);

                                     // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC2)    //{    // ����� ����
            SetBinTime2(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC2) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = Mem_DaySearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC2)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = Mem_DaySearchEnd_1(page);

         if ((startpoint_1 > ARC2-1) || (endpoint_1 > ARC2-1))
            err |= 0x40;
      }
      if (err == 0) {
         Mem_DayRd_1(startpoint_1,page); // �⥭�� 1-� ����� ���筮�� ����
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//              if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                 // ����� ��p���⪨ ��p���� ���p��


   if (err) {
      if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x94);  // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;      // ����p ��⪨
         BufTrans_1[5] = 0;            // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;            // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);  // �訡�� ��p����p�� ���p��
   }
   else {                            // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                     // ����� ����
      page  = GetPage(STARTENDTIME_1.num,2);
      point = Mem_DeyNextIndex_1(startpoint_1,9*STARTENDTIME_1.RequestNum,page);

      count = 0;

      if (point < ARC2) {             // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<9;i++) {
            count++;                 // ���-�� ����ᥩ
            if (i) {
               point = Mem_DeyNextIndex_1(point,1,page);
               if (point > ARC2-1)
                  break;             // ����� ���᪠
            }
            Mem_DayRd_1(point,page);   // �⥭�� ����� ���筮�� ����

            pbtime = &MEMDATA_1.STARTTIME;
                                     // �����
            pdr->StartMonth   = pbtime->month;
            pdr->StartDay     = pbtime->date;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
       Vl.Val=MEMDATA_1.Heat;
       w=Vl.Bt[0]&0x1;

            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
               pdr->StartDay |= 0x80;// ᯮᮡ ����p���� �� ��⪥
                                     // ���
            pdr->StartYear = pbtime->year;
                                     // ���� ⥯���
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            pdr->FullHeat   = MEMDATA_1.FullHeat*Koef/1.0e3;
                                     // �।��� 㤥�쭠� ⥯���
            pdr->Heat      = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
                Vl.Val=pdr->Heat;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pdr->Heat =  Vl.Val;
                                     // �।��� ���⭮���
            pdr->Ro        = MEMDATA_1.RO;
                                     // �।��� ᮤ�ঠ��� ����
            pdr->N2        = MEMDATA_1.N2;
                                     // �।��� ᮤ�ঠ��� 㣫���᫮��
            pdr->CO2       = MEMDATA_1.CO2;

            pdr++;

            if (point == endpoint_1)
               break;                // ����� ���᪠
         }
      }
                                     // ���-�� ����ᥩ
      SEND_1.RESPONSEDAILYCHR.NumRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC2-1))
                                     // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEDAILYCHR.ResponseStatus = 0;
      else                           // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEDAILYCHR.ResponseStatus = 1;
                                     // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizeof(struct dailyrecordchr)+9,0x91);
                                     // ������ ���p�
      movmem(&SEND_1.RESPONSEDAILYCHR,&BufTrans_1[5],
             count*sizeof(struct dailyrecordchr)+2);
      BufTrans_1[4] = 3 & num  | (GLCONFIG.IDRUN[num-1].Dens<<5);         // ����p ��⪨
   }
}
*/
//--------------------------------------------------------
/*
void RdCyclChr_1() {                      // �� 横����� ��ࠬ��஢ - ���
   static unsigned int   startpoint_1,endpoint_1;
   struct bintime        *pbintime;
   unsigned char         num;
   unsigned char         page;
   unsigned int          point;
   int                   err;
   struct reqcykl        *prc;//��p���p� ���p�� - �⥭�� 横���᪨� ������
   struct cyklrecordchr  *pcr;//��p���p� ����� ������ ��� ��p����
   int                   count;

   err = 0;

   if (BufInKadr_1[2]-sizeof(struct reqcykl) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pcr = &SEND_1.RESPONSECYKLCHR.CYKLRECORDCHR[0];

   prc = &RESIV_1.REQCYKL;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prc,sizeof(struct reqcykl));

   if (prc->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prc->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prc->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prc->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

                                        // ��p���⪠ 1-�� ���p��
   if (STARTENDTIME_1.RequestNum == 0) {
      pbintime = &STARTENDTIME_1.STARTTIME;

      pbintime->seconds = prc->StartSeconds;
      pbintime->minutes = prc->StartMinutes;
      pbintime->hours   = prc->StartHours;
      pbintime->date    = prc->StartDay;
      pbintime->month   = prc->StartMonth;
      pbintime->year    = prc->StartYear;

      if (Mem_TestReqTime_1(pbintime))
         err |= 8;                   // ����p��� �p����� ���p��

      pbintime = &STARTENDTIME_1.ENDTIME;

      pbintime->seconds = prc->EndSeconds;
      pbintime->minutes = prc->EndMinutes;
      pbintime->hours   = prc->EndHours;
      pbintime->date    = prc->EndDay;
      pbintime->month   = prc->EndMonth;
      pbintime->year    = prc->EndYear;

      if (Mem_TestReqTime_1(pbintime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��
                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                     // ��p���� 横���᪮�� ����
         page = Mem_CyklGetPage_1(STARTENDTIME_1.num);
                                     // ��砫�� ������
         startpoint_1 = Mem_DaySearchStart2_1(page);

                                     // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC2)    //{     // ����� ����
            SetBinTime2(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC2) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = Mem_DaySearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC2)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = Mem_DaySearchEnd_1(page);

         if ((startpoint_1 > ARC2-1) || (endpoint_1 > ARC2-1))
            err |= 0x40;
      }
      if (err == 0) {
         Mem_DayRd_1(startpoint_1,page); // �⥭�� ����� 横���᪮�� ����
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                 // ����� ��p���⪨ 1-�� ���p��


   if (err) {
      if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x98);  // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;      // ����p ��⪨
         BufTrans_1[5] = 0;            // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;            // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);  // �訡�� ��p����p�� ���p��
   }
   else {                            // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                     // ��p���� 横���᪮�� ����
      page  = Mem_CyklGetPage_1(STARTENDTIME_1.num);
      point = Mem_DeyNextIndex_1(startpoint_1,9*STARTENDTIME_1.RequestNum,page);

      count = 0;
      if (point < ARC2) {
         for (int i=0;i<9;i++) {
            count++;                 // �p������� ��p�������
            if (i) {
               point = Mem_DeyNextIndex_1(point,1,page);
               if (point > ARC2-1)
                  break;             // ����� ���᪠
            }
            Mem_DayRd_1(point,page);   // �⥭�� ����� 横���᪮�� ����

            pbintime = &MEMDATA_1.STARTTIME;
                                     // �����
            pcr->StartMonth   = pbintime->month;
            pcr->StartDay     = pbintime->date;
            pcr->StartYear    = pbintime->year;
            pcr->StartHours   = pbintime->hours;
            pcr->StartMinutes = pbintime->minutes;
            pcr->StartSeconds = pbintime->seconds;

            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
               pcr->StartDay |= 0x80;// ᯮᮡ ����p���� �� ��⪥
                                     // ��p��� ������� � ᥪ㭤��
            pcr->Period   = MEMDATA_1.count;
                                     // ���� ⥯���
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            pcr->FullHeat   = MEMDATA_1.FullHeat*Koef/1.0e3;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
       Vl.Val=MEMDATA_1.Heat;
       w=Vl.Bt[0]&0x1;

            pcr->Heat = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];// �।��� 㤥�쭠� ⥯���
                Vl.Val=pcr->Heat;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pcr->Heat =  Vl.Val;
            pcr->Ro   = MEMDATA_1.RO;  // �।��� ���⭮���
            pcr->N2   = MEMDATA_1.N2; // �।��� ᮤ�ঠ��� ����
            pcr->CO2  = MEMDATA_1.CO2;// �।��� ᮤ�ঠ��� 㣫���᫮��

            pcr++;

            if (point == endpoint_1)
               break;                // ����� ���᪠
         }
      }                              // ���-�� ����ᥩ
      SEND_1.RESPONSECYKLCHR.NumCyklRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC2-1))
                                     // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSECYKLCHR.ResponseStatus = 0;
      else
         SEND_1.RESPONSECYKLCHR.ResponseStatus = 1;
                                     // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizeof(struct cyklrecordchr)+9,0x92);
                                     // ������ ���p�
      movmem(&SEND_1.RESPONSECYKLCHR,&BufTrans_1[5],
             count*sizeof(struct cyklrecordchr)+2);

      BufTrans_1[4] = 3 & num | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);         // ����p ��⪨
   }
}
*/
//--------------------------------------------------------
/*
void RdHourChr_1() {                      // �� �ᮢ�� ��ࠬ��஢  - ���
   static unsigned int  startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point;
   int                 err;
   int                 count;
   struct reqhour       *prh;           // ��p���p� ���p�� - �⥭�� �ᮢ�� ������
   struct hourrecordchr *phr;           // ��p���p� ����� ������ ��� ��p����

   err = 0;

   if (BufInKadr_1[2]-sizeof(reqhour) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   phr = &SEND_1.RESPONSEHOURCHR.HOURRECORDCHR[0];

   prh = &RESIV_1.REQHOUR;              // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prh,sizeof(reqhour));

   if (prh->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prh->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prh->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prh->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

   if (STARTENDTIME_1.RequestNum==0) {  // ��p���⪠ 1-�� ���p��
      pbtime = &STARTENDTIME_1.STARTTIME;

      pbtime->seconds = 0;
      pbtime->minutes = 0;
      pbtime->hours   = prh->StartHours;
      pbtime->date    = prh->StartDay;
      pbtime->month   = prh->StartMonth;
      pbtime->year    = prh->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                      // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = 59;
      pbtime->hours   = prh->EndHours;
      pbtime->date    = prh->EndDay;
      pbtime->month   = prh->EndMonth;
      pbtime->year    = prh->EndYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��

                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                     // �ᮢ�� ����
         page = GetPage(STARTENDTIME_1.num,1);
                                     // ��砫�� ������
         startpoint_1 = MemPerioddataSearchStart2_1(page);

                                     // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC4)    //{  // ����� ����
            SetBinTime4(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC4-1) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = MemPerioddataSearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC4)
               startpoint_1 = 0;
         }
         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = MemPerioddataSearchEnd_1(page);
         if ((startpoint_1 > ARC4-1) || (endpoint_1 > ARC4-1))
            err |= 0x40;
      }

      if (err == 0) {                // �⥭�� 1-� ����� �ᮢ��� ����
         MemPerioddataRd_1(startpoint_1,page);
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                    // ����� ��p���⪨ ��p���� ���p��


   if (err) {
      if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x93);   // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;       // ����p ��⪨
         BufTrans_1[5] = 0;             // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;             // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // �ᮢ�� ����
      page  = GetPage(STARTENDTIME_1.num,1);
      point = NextIndex_1(startpoint_1,9*STARTENDTIME_1.RequestNum,page);

      count = 0;
      if (point < ARC4) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<9;i++) {
            count++;                    // ���-�� ����ᥩ
            if (i) {
               point = NextIndex_1(point,1,page);
               if (point > ARC4-1)
                  break;                // ����� ���᪠
            }                           // �⥭�� ����� �ᮢ��� ����
            MemPerioddataRd_1(point,page);

            pbtime = &MEMDATA_1.STARTTIME;
                                        // ���
            phr->StartMonth   = pbtime->month;
            phr->StartDay     = pbtime->date;
            phr->StartYear    = pbtime->year;
            phr->StartHours   = pbtime->hours;

            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
               phr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                                        // ��p��� ������� � ������
            phr->Period   = MEMDATA_1.count;
                                        // ���� ⥯���
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            phr->FullHeat   = MEMDATA_1.FullHeat*Koef/1.0e3;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
       Vl.Val=MEMDATA_1.Heat;
       w=Vl.Bt[0]&0x1;
            phr->Heat = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE]; // �।��� 㤥�쭠� ⥯���
                Vl.Val=phr->Heat;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                phr->Heat =  Vl.Val;
            phr->Ro   = MEMDATA_1.RO;   // �।��� ���⭮���
            phr->N2   = MEMDATA_1.N2;   // �।��� ᮤ�ঠ��� ����
            phr->CO2  = MEMDATA_1.CO2;  // �।��� ᮤ�ঠ��� 㣫���᫮��

            phr++;
            if (point == endpoint_1)
               break;                   // ����� ���᪠
         }
      }
                                        // ���-�� ����ᥩ
      SEND_1.RESPONSEHOURCHR.NumHourRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC4-1))
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEHOURCHR.ResponseStatus = 0;
      else                              // ����� �⢥� = 1; ���� �� �����
         SEND_1.RESPONSEHOURCHR.ResponseStatus = 1;
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizeof(struct hourrecordchr)+9,0x93);
                                        // ������ ���p�
      movmem(&SEND_1.RESPONSEHOURCHR,&BufTrans_1[5],
             count*sizeof(struct hourrecordchr)+2);

      BufTrans_1[4] = 3 & num | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);          // ����p ��⪨
   }
}
*/
//--------------------------------------------------------
/*
void RdPeriodDen_1() {                    // �� �����. ��ࠬ��஢  - ���
   static unsigned int    startpoint_1,endpoint_1;
   struct bintime         *pbtime;
   struct ReqPeriod       *prp;         // ��p���p� ���p��
   unsigned char          num;
   unsigned char          page;
   struct periodrecordden *pprec;       // ��p���p� ���p�⨢��� ������ ��� ��p����
   unsigned int           point;
   int                    count,Nrec;
   int err = 0;

   if (BufInKadr_1[2]-sizeof(ReqPeriod) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨
   Nrec=7;

   pprec = &SEND_1.RESPONSEPERIODDEN.PERIODRECORDDEN[0];

   prp = &RESIV_1.REQPERIOD;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prp,sizeof(ReqPeriod));

   if (prp->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prp->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prp->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }                                    // ����� �����
   STARTENDTIME_1.RequestNum = prp->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨
                                        // ��p���⪠ 1-�� ���p��
   if (STARTENDTIME_1.RequestNum == 0) {
      pbtime = &STARTENDTIME_1.STARTTIME;

      pbtime->seconds = 0;
      pbtime->minutes = prp->StartMinutes;
      pbtime->hours   = prp->StartHours;
      pbtime->date    = prp->StartDay;
      pbtime->month   = prp->StartMonth;
      pbtime->year    = prp->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                   // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = prp->EndMinutes+GLCONFIG.Period-1;

      if (pbtime->minutes > 59)
         pbtime->minutes = 59;

      pbtime->hours   = prp->EndHours;
      pbtime->date    = prp->EndDay;
      pbtime->month   = prp->EndMonth;
      pbtime->year    = prp->EndYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��
                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                     // ��p�����᪨� ����
         page = GetPage(STARTENDTIME_1.num,0);
                                     // ��砫�� ������
         startpoint_1 = MemPerioddataSearchStart2_1(page);

         if (startpoint_1 < ARC4)    //{    // ����� ����
            SetBinTime4(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC4-1) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = MemPerioddataSearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC4)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = MemPerioddataSearchEnd_1(page);

         if ((startpoint_1 > ARC4-1) || (endpoint_1 > ARC4-1))
            err |= 0x40;
      }
      if (err == 0) {                // �⥭�� 1-� ����� ��ਮ���᪮�� ����
         MemPerioddataRd_1(startpoint_1,page);
                                     // �訡��, �᫨ ���⮢�� �६� � ��ࢮ� ����� ��娢�
                                     // �ॢ�蠥� ����筮� �६� � �����
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                 // ����� ��p���⪨ ��p���� ���p��

   if (err) {
      if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x95);// ������ �p�䨪�
         BufTrans_1[4] = 3 & num;      // ����p ��⪨
         BufTrans_1[5] = 0;            // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;            // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // ��p�����᪨� ����
      page  = GetPage(STARTENDTIME_1.num,0);
      point = NextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);

      count = 0;
      if (point < ARC4) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<Nrec;i++) {
            count++;                    // �p������� ��p�������
            if (i) {
               point = NextIndex_1(point,1,page);
               if (point > ARC4-1)
                  break;                // ����� ���᪠
            }                           // �⥭�� ����� ��p�����᪮�� ����
            MemPerioddataRd_1(point,page);

            pbtime = &MEMDATA_1.STARTTIME;
                                     // �����
            pprec->StartMonth   = pbtime->month;
            pprec->StartDay     = pbtime->date;
            pprec->StartYear    = pbtime->year;
            pprec->StartHours   = pbtime->hours;
            pprec->StartMinutes = pbtime->minutes;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;

            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
              {                       // ᯮᮡ ����p���� �� ��⪥
               pprec->StartDay |= 0x80;
               pprec->DP_V   = MEMDATA_1.dp;   // ��� ��ꥬ �� ࠡ��� �᫮����, �3 (��� ���稪�)
              }
            else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {  pprec->DP_V = MEMDATA_1.dp;
                  Vl.Val=pprec->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pprec->DP_V =  Vl.Val;
               }
               else
                { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
                   pprec->DP_V   = MEMDATA_1.dp;
                  else
                  pprec->DP_V   = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   // �।��� ��९�� ��������, ���/�2
                  Vl.Val=pprec->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  pprec->DP_V =  Vl.Val;
               }
             }                         // ��p��� ������� � ������
            pprec->Period   = MEMDATA_1.count;
            pprec->V      = MEMDATA_1.q;  // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            {  if  (GLCONFIG.EDIZM_T.EdIzmE == 1)
                Koef = 1.0;
               else
                Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
            pprec->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
   Vl.Val=MEMDATA_1.p;
   w=Vl.Bt[0]&0x1;
           if (GLCONFIG.EDIZM_T.EdIzP == 0)
            pprec->P  = MEMDATA_1.p;
           else
            pprec->P      = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
                Vl.Val=pprec->P;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pprec->P =  Vl.Val;
            pprec->t      = MEMDATA_1.t;  // �।��� ⥬������, �ࠤ. ������
            pprec->Ro       = MEMDATA_1.RO;
   Vl.Val=MEMDATA_1.Heat;
   w=Vl.Bt[0]&0x1;
            if (GLCONFIG.EDIZM_T.EdIzmE == 1)
             pprec->UTS = MEMDATA_1.Heat;
            else
             pprec->UTS     = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];     // �. ⥯��� ᣮ࠭�� ����.
                Vl.Val=pprec->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pprec->UTS =  Vl.Val;

            pprec++;

            if (point == endpoint_1)
               break;                // ����� ���᪠
         }
      }                              // ���-�� ����ᥩ
      SEND_1.RESPONSEPERIODDEN.NumRecord = count;

      if ((point == endpoint_1) || (count==0) || (point > ARC4-1))
                                     // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEPERIODDEN.ResponseStatus = 0;
      else                           // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEPERIODDEN.ResponseStatus = 1;
                                     // ������ �p�䨪�
      SaveMessagePrefix_1 (count*sizeof(struct periodrecordden)+9,0x95);
                                     // ������ ���p�
      movmem (&SEND_1.RESPONSEPERIODDEN,&BufTrans_1[5],
         count * sizeof(struct periodrecordden)+2);

      BufTrans_1[4] = 3 & num | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
   }
}
*/
//--------------------------------------------------------
/*
void RdDayDen_1() {                       // �� ������ ��ࠬ��஢ �஬�⮣��
   static unsigned int  startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point;
   int                 err,Nrec,lenrecord;
   int                 count, nfc,k;
   struct reqdaily       *prd;          // ��p���p� ���p�� - �⥭�� ������ ������
   struct dailyrecordden *pdr;          // ��p���p� ����� ������ ��� ��p����
   char typeCount;
   
   err = 0;
   if (BufInKadr_1[2]-sizeof(reqdaily) != 7)
     err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4] & 3;
   typeCount = (GLCONFIG.IDRUN[num-1].TypeRun == 4) ||
     (GLCONFIG.IDRUN[num-1].TypeRun == 5) ||(GLCONFIG.IDRUN[num-1].TypeRun == 10);
   nfc = BufInKadr_1[3];
//   if (nfc==0x14)
    { Nrec=7;
      lenrecord = sizeof(struct dailyrecordden); // ��� Vrtot -4
    }
//    else
//    { Nrec=8;
//      lenrecord = sizeof(struct dailyrecordden);
//    }

   if ((num & 3) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pdr = &SEND_1.RESPONSEDAILYDEN.DAILYRECORDDEN[0];
   prd = &RESIV_1.REQDAILY;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prd,sizeof(reqdaily));

   if (prd->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥��� �����
     if (STARTENDTIME_1.RequestNum == prd->RequestNum)
        return;

     if (STARTENDTIME_1.RequestNum +1 != prd->RequestNum)
        err |= 0x04;                    // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prd->RequestNum;
   STARTENDTIME_1.num = (3 & num-1);    // ����� ��⪨

                                        // ��p���⪠ 1-�� ���p��
   if (STARTENDTIME_1.RequestNum == 0) {
      CopyKoef();
      pbtime = &STARTENDTIME_1.STARTTIME;
      pbtime->seconds = 0;
      pbtime->minutes = 0;
                                     // ����p���� ��;
      pbtime->hours   = GLCONFIG.ContrHour;
      pbtime->date    = prd->StartDay;
      pbtime->month   = prd->StartMonth;
      pbtime->year    = prd->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                   // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = 59;
                                     // ����p���� ��
      pbtime->hours   = (GLCONFIG.ContrHour) ? GLCONFIG.ContrHour-1 : 23;

      if (GLCONFIG.ContrHour)
         dayPlus(prd->EndDay,prd->EndMonth,prd->EndYear,
                 pbtime->date,pbtime->month,pbtime->year);
      else {
         pbtime->date    = prd->EndDay;
         pbtime->month   = prd->EndMonth;
         pbtime->year    = prd->EndYear;
      }
      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��
                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if ( err == 0) {
                                     // ����� ����
         page = GetPage(STARTENDTIME_1.num,2);
                                     // ��砫�� ������
         startpoint_1 = Mem_DaySearchStart2_1(page);

                                     // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC2)    //{    // ����� ����
            SetBinTime2(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC2) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = Mem_DaySearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC2)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = Mem_DaySearchEnd_1(page);

         if ((startpoint_1 > ARC2-1) || (endpoint_1 > ARC2-1))
            err |= 0x40;
      }
      if (err == 0) {
         Mem_DayRd_1(startpoint_1,page); // �⥭�� 1-� ����� ���筮�� ����
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                 // ����� ��p���⪨ ��p���� ���p��


   if (err) {
      if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,nfc+128);  // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;      // ����p ��⪨
         BufTrans_1[5] = 0;            // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;            // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);  // �訡�� ��p����p�� ���p��
   }
   else {                            // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                     // ����� ����
      page  = GetPage(STARTENDTIME_1.num,2);
      point = Mem_DeyNextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);

      count = 0;

      if (point < ARC2) {             // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<Nrec;i++) {
            count++;                 // ���-�� ����ᥩ
            if (i) {
               point = Mem_DeyNextIndex_1(point,1,page);
               if (point > ARC2-1)
                  break;             // ����� ���᪠
            }
            Mem_DayRd_1(point,page);   // �⥭�� ����� ���筮�� ����

            pbtime = &MEMDATA_1.STARTTIME;
                                     // �����
            pdr->StartMonth   = pbtime->month;
            pdr->StartDay     = pbtime->date;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;
            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
             {  pdr->StartDay |= 0x80;// ᯮᮡ ����p���� �� ��⪥
                pdr->DP_V   = MEMDATA_1.dp;   // ��ꥬ �� �3
             }
            else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {  pdr->DP_V = MEMDATA_1.dp;
                  Vl.Val=pdr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pdr->DP_V =  Vl.Val;
               }
               else
                { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
                   pdr->DP_V   = MEMDATA_1.dp;
                  else
                  pdr->DP_V   = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   // �।��� ��९�� ��������, ���/�2
                  Vl.Val=pdr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  pdr->DP_V =  Vl.Val;
               }
             }                       // ���
            pdr->StartYear = pbtime->year;

            pdr->V      = MEMDATA_1.q;  // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            {  if  (GLCONFIG.EDIZM_T.EdIzmE == 1)
                Koef = 1.0;
               else
                Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
            pdr->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
   Vl.Val=MEMDATA_1.p;
   w=Vl.Bt[0]&0x1;
           if (GLCONFIG.EDIZM_T.EdIzP == 0)
            pdr->P  = MEMDATA_1.p;
           else
            pdr->P = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
                Vl.Val=pdr->P;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pdr->P =  Vl.Val;
            pdr->t      = MEMDATA_1.t;  // �।��� ⥬������, �ࠤ. ������
            pdr->Ro        = MEMDATA_1.RO;
   Vl.Val=MEMDATA_1.Heat;
   w=Vl.Bt[0]&0x1;
            if (GLCONFIG.EDIZM_T.EdIzmE == 1)
             pdr->UTS = MEMDATA_1.Heat;
            else
             pdr->UTS = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];     // �. ⥯��� ᣮ࠭�� ����.
                Vl.Val=pdr->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pdr->UTS =  Vl.Val;

//           if (nfc==0x1B)
//            pdr->Vrtot  = MEMDATA_1.CO2;   // ��騩 ��� � ��, ���.�3;
//           else  pdr->Vrtot  = 0.0;
            pdr++;

            if (point == endpoint_1)
               break;                // ����� ���᪠
         }
      }
                                     // ���-�� ����ᥩ
      SEND_1.RESPONSEDAILYDEN.NumRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC2-1))
                                     // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEDAILYDEN.ResponseStatus = 0;
      else                           // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEDAILYDEN.ResponseStatus = 1;
                                     // ������ �p�䨪�
       SaveMessagePrefix_1(count*lenrecord+9,nfc+128);
                                     // ������ ���p�
      movmem(&SEND_1.RESPONSEDAILYDEN,&BufTrans_1[5],
             count*sizeof(struct dailyrecordden)+2);
        BufTrans_1[4] = 3 & num | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
   }
} */
//--------------------------------------------------------
/*
void RdCyclDen_1() {                      // �� 横����� ��ࠬ��஢ - ���
   static unsigned int   startpoint_1,endpoint_1;
   struct bintime        *pbintime;
   unsigned char         num;
   unsigned char         page;
   unsigned int          point;
   int                   err;
   struct reqcykl        *prc;//��p���p� ���p�� - �⥭�� 横���᪨� ������
   struct cyklrecordden  *pcr;//��p���p� ����� ������ ��� ��p����
   int                   count,Nrec;

   err = 0;
   if (BufInKadr_1[2]-sizeof(struct reqcykl) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];
   Nrec = 7;
   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pcr = &SEND_1.RESPONSECYKLDEN.CYKLRECORDDEN[0];

   prc = &RESIV_1.REQCYKL;
                                        // ��p������� ��p����p�� ���p��
   movmem (&BufInKadr_1[5],prc,sizeof(struct reqcykl));

   if (prc->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prc->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prc->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prc->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

                                        // ��p���⪠ 1-�� ���p��
   if (STARTENDTIME_1.RequestNum == 0) {
      pbintime = &STARTENDTIME_1.STARTTIME;

      pbintime->seconds = prc->StartSeconds;
      pbintime->minutes = prc->StartMinutes;
      pbintime->hours   = prc->StartHours;
      pbintime->date    = prc->StartDay;
      pbintime->month   = prc->StartMonth;
      pbintime->year    = prc->StartYear;

      if (Mem_TestReqTime_1(pbintime))
         err |= 8;                   // ����p��� �p����� ���p��

      pbintime = &STARTENDTIME_1.ENDTIME;

      pbintime->seconds = prc->EndSeconds;
      pbintime->minutes = prc->EndMinutes;
      pbintime->hours   = prc->EndHours;
      pbintime->date    = prc->EndDay;
      pbintime->month   = prc->EndMonth;
      pbintime->year    = prc->EndYear;

      if (Mem_TestReqTime_1(pbintime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��
                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                     // ��p���� 横���᪮�� ����
         page = Mem_CyklGetPage_1(STARTENDTIME_1.num);
                                     // ��砫�� ������
         startpoint_1 = Mem_DaySearchStart2_1(page);

                                     // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC2)    //{     // ����� ����
            SetBinTime2(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC2) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = Mem_DaySearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC2)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = Mem_DaySearchEnd_1(page);

         if ((startpoint_1 > ARC2-1) || (endpoint_1 > ARC2-1))
            err |= 0x40;
      }
      if (err == 0) {
         Mem_DayRd_1(startpoint_1,page); // �⥭�� ����� 横���᪮�� ����
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                 // ����� ��p���⪨ 1-�� ���p��

   if (err) {
      if (err & 0xC0) {              // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x98);  // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;      // ����p ��⪨
         BufTrans_1[5] = 0;            // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;            // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);  // �訡�� ��p����p�� ���p��
   }
   else {                            // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                     // ��p���� 横���᪮�� ����
      page  = Mem_CyklGetPage_1(STARTENDTIME_1.num);
      point = Mem_DeyNextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);

      count = 0;
      if (point < ARC2) {
         for (int i=0; i<Nrec; i++) {
            count++;                 // �p������� ��p�������
            if (i) {
               point = Mem_DeyNextIndex_1(point,1,page);
               if (point > ARC2-1)
                  break;             // ����� ���᪠
            }
            Mem_DayRd_1(point,page);   // �⥭�� ����� 横���᪮�� ����

            pbintime = &MEMDATA_1.STARTTIME;
                                     // �����
            pcr->StartMonth   = pbintime->month;
            pcr->StartDay     = pbintime->date;
            pcr->StartYear    = pbintime->year;
            pcr->StartHours   = pbintime->hours;
            pcr->StartMinutes = pbintime->minutes;
            pcr->StartSeconds = pbintime->seconds;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;

            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
              {  pcr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                pcr->DP_V   = MEMDATA_1.dp;   // ��ꥬ �� �3
             }
             else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {  pcr->DP_V = MEMDATA_1.dp;
                  Vl.Val=pcr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pcr->DP_V =  Vl.Val;
               }
               else
               {  pcr->DP_V   = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   // �।��� ��९�� ��������, ���/�2 ���     |
                  Vl.Val=pcr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  pcr->DP_V =  Vl.Val;
               }
             }                        // ��p��� ������� � ᥪ㭤��
            pcr->Period   = MEMDATA_1.count;

            pcr->V      = MEMDATA_1.q;  // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            pcr->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
   Vl.Val=MEMDATA_1.p;
   w=Vl.Bt[0]&0x1;
            pcr->P      = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
                Vl.Val=pcr->P;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pcr->P =  Vl.Val;
            pcr->t      = MEMDATA_1.t;  // �।��� ⥬������, �ࠤ. ������
            pcr->Ro   = MEMDATA_1.RO;  // �।��� ���⭮���
   Vl.Val=MEMDATA_1.Heat;
   w=Vl.Bt[0]&0x1;
            pcr->UTS  = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];   // 㤥�쭠� ⥯��� ᣮ࠭�� ���/�3
                Vl.Val=pcr->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                pcr->UTS =  Vl.Val;

            pcr++;

            if (point == endpoint_1)
               break;                // ����� ���᪠
         }
      }                              // ���-�� ����ᥩ
      SEND_1.RESPONSECYKLDEN.NumCyklRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC2-1))
                                     // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSECYKLDEN.ResponseStatus = 0;
      else
         SEND_1.RESPONSECYKLDEN.ResponseStatus = 1;
                                     // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizeof(struct cyklrecordden)+9,0x98);
                                     // ������ ���p�
      movmem(&SEND_1.RESPONSECYKLDEN,&BufTrans_1[5],
             count*sizeof(struct cyklrecordden)+2);

      BufTrans_1[4] = 3 & num  | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);;
   }
}
*/
//--------------------------------------------------------
/*
void RdHourDen_1() {                      // �� �ᮢ�� ��ࠬ��஢  - ���
   static unsigned int  startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point;
   int                 err;
   int                 count,Nrec;
   struct reqhour       *prh;           // ��p���p� ���p�� - �⥭�� �ᮢ�� ������
   struct hourrecordden *phr;           // ��p���p� ����� ������ ��� ��p����

   err = 0;

   if (BufInKadr_1[2]-sizeof(reqhour) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];
   Nrec = 7;

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   phr = &SEND_1.RESPONSEHOURDEN.HOURRECORDDEN[0];

   prh = &RESIV_1.REQHOUR;              // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prh,sizeof(reqhour));

   if (prh->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prh->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prh->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prh->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

   if (STARTENDTIME_1.RequestNum==0) {  // ��p���⪠ 1-�� ���p��
      CopyKoef();
      pbtime = &STARTENDTIME_1.STARTTIME;
      pbtime->seconds = 0;
      pbtime->minutes = 0;
      pbtime->hours   = prh->StartHours;
      pbtime->date    = prh->StartDay;
      pbtime->month   = prh->StartMonth;
      pbtime->year    = prh->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                      // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = 59;
      pbtime->hours   = prh->EndHours;
      pbtime->date    = prh->EndDay;
      pbtime->month   = prh->EndMonth;
      pbtime->year    = prh->EndYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                // ����p��� ᮮ⭮襭�� �p���� ���p��

                                     // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                     // �ᮢ�� ����
         page = GetPage(STARTENDTIME_1.num,1);
                                     // ��砫�� ������
         startpoint_1 = MemPerioddataSearchStart2_1(page);

                                     // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC4)    //{  // ����� ����
            SetBinTime4(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                     // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC4-1) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                     // ��砫�� ������
            startpoint_1 = MemPerioddataSearchStart_1(page);
         else {
            startpoint_1++;            // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC4)
               startpoint_1 = 0;
         }
         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                        // ������ ������
            endpoint_1 = MemPerioddataSearchEnd_1(page);
         if ((startpoint_1 > ARC4-1) || (endpoint_1 > ARC4-1))
            err |= 0x40;
      }

      if (err == 0) {                // �⥭�� 1-� ����� �ᮢ��� ����
         MemPerioddataRd_1(startpoint_1,page);
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                    // ����� ��p���⪨ ��p���� ���p��


   if (err) {
      if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x99);   // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;       // ����p ��⪨
         BufTrans_1[5] = 0;             // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;             // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // �ᮢ�� ����
      page  = GetPage(STARTENDTIME_1.num,1);
      point = NextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);

      count = 0;
      if (point < ARC4) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<Nrec;i++) {
            count++;                    // ���-�� ����ᥩ
            if (i) {
               point = NextIndex_1(point,1,page);
               if (point > ARC4-1)
                  break;                // ����� ���᪠
            }                           // �⥭�� ����� �ᮢ��� ����
            MemPerioddataRd_1(point,page);

            pbtime = &MEMDATA_1.STARTTIME;
                                        // ���
            phr->StartMonth   = pbtime->month;
            phr->StartDay     = pbtime->date;
            phr->StartYear    = pbtime->year;
            phr->StartHours   = pbtime->hours;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;

            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
             {  phr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                phr->DP_V   = MEMDATA_1.dp;   // ��ꥬ �3
             }
             else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {  phr->DP_V = MEMDATA_1.dp;
                  Vl.Val=phr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  phr->DP_V =  Vl.Val;
               }
               else
                { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
                   phr->DP_V   = MEMDATA_1.dp;
                  else
                   phr->DP_V   = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   // �।��� ��९�� ��������, ���/�2
                  Vl.Val=phr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  phr->DP_V =  Vl.Val;
               }
             }              // ��p��� ������� � ������
            phr->Period   = MEMDATA_1.count;
            phr->V      = MEMDATA_1.q;  // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            {  if  (GLCONFIG.EDIZM_T.EdIzmE == 1)
                Koef = 1.0;
               else
                Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
            phr->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
   Vl.Val=MEMDATA_1.p;
   w=Vl.Bt[0]&0x1;
           if (GLCONFIG.EDIZM_T.EdIzP == 0)
            phr->P  = MEMDATA_1.p;
           else
            phr->P      = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
                Vl.Val=phr->P;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                phr->P =  Vl.Val;
            phr->t      = MEMDATA_1.t;  // �।��� ⥬������, �ࠤ. ������
            phr->Ro   = MEMDATA_1.RO;   // �।��� ���⭮���
   Vl.Val=MEMDATA_1.Heat;
   w=Vl.Bt[0]&0x1;
            if (GLCONFIG.EDIZM_T.EdIzmE == 1)
             phr->UTS = MEMDATA_1.Heat;
            else
             phr->UTS = MEMDATA_1.Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];     // �. ⥯��� ᣮ࠭�� ����.
                Vl.Val=phr->UTS;
                Vl.Bt[0]=Vl.Bt[0]&0xFE;
                Vl.Bt[0]=Vl.Bt[0] | w;
                phr->UTS =  Vl.Val;
            phr++;
            if (point == endpoint_1)
               break;                   // ����� ���᪠
         }
      }
                                        // ���-�� ����ᥩ
      SEND_1.RESPONSEHOURDEN.NumHourRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC4-1))
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEHOURDEN.ResponseStatus = 0;
      else                              // ����� �⢥� = 1; ���� �� �����
         SEND_1.RESPONSEHOURDEN.ResponseStatus = 1;
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizeof(struct hourrecordden)+9,0x99);
                                        // ������ ���p�
      movmem(&SEND_1.RESPONSEHOUR,&BufTrans_1[5],
             count*sizeof(struct hourrecordden)+2);

      BufTrans_1[4] = 3 & num | (INTPAR.Com2Mode<<7) | (INTPAR.HeatReport<<6);
   }
}
*/
//--------------------------------------------------------
void RdIntPar_1() {                     // �⥭�� ����७��� ��ࠬ��஢
 RdIntParp(BufInKadr_1,BufTrans_1);
}
/*
   int err = 0;

   if (BufInKadr_1[2] != 6)
      err |= 1;                         // ����p��� ����� ���p��

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // �訡�� ��p����p�� ���p��
   else {
					// ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(struct intpar)+4,0xD2);
					// ������ ���p�
      memmove(&BufTrans_1[4],&INTPAR,sizeof(struct intpar)-2);
   }
}
*/
//--------------------------------------------------------
void RdIntPar2_1() {                     // �⥭�� ����७��� ��ࠬ��஢ 2
 RdIntPar2p(BufInKadr_1,BufTrans_1);                      // ����室������ ��砫쭮� ���䨣�p�樨
}
/*
   intpar2 INTPAR2;
   int err = 0;

   if (BufInKadr_1[2] != 6)
      err |= 1;                         // ����p��� ����� ���p��

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // �訡�� ��p����p�� ���p��
   else {
   INTPAR2.HartNumCyclRep = INTPAR.HartNumCyclRep;  //���-�� 横��� ����
   INTPAR2.HartNumImmedRep = INTPAR.HartNumImmedRep; // ���-�� ����������� ����஢
   INTPAR2.HartReqDelay  = INTPAR.HartReqDelay;
   INTPAR2.Com1NoWrite  = INTPAR.Com1NoWrite;  // १��  0/1
   INTPAR2.CntBufLen  = INTPAR.CntBufLen;  // ����� �⢥� �.6 (0 - 23, 1-27 ����) 2-���㫥��� ��娢�� �� SD-����
   INTPAR2.CntAddBufShift = INTPAR.CntAddBufShift; // ��ࠬ��� ��஫� ����������� (0,1,...)
   INTPAR2.VolumeDelta = INTPAR.VolumeDelta; // १�� � �.�. 4 ����
   INTPAR2.AlarmVolumeMinTime = INTPAR.AlarmVolumeMinTime; // �����. �६� ��� ���਩���� ��ꥬ�

					// ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(struct intpar2)+6,0xE2);
					// ������ ���p�
      memmove(&BufTrans_1[4],&INTPAR2,sizeof(struct intpar2));
   }
}
*/
//--------------------------------------------------------
void RdAddConf_1() {                     // �⥭�� ��������. ��ࠬ��஢ ����.
 RdAddConfp(BufInKadr_1,BufTrans_1);                      // ����室������ ��砫쭮� ���䨣�p�樨
}
/*
   addpar ADDPAR;
   int err = 0;

   if (BufInKadr_1[2] != 6)
      err |= 1;                         // ����p��� ����� ���p��

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // �訡�� ��p����p�� ���p��
   else {
   ADDPAR.RS485Com5Speed = INTPAR.RS485Com5Speed-1;// ᪮���� �� COM5 - RS485
   ADDPAR.Com4Speed = INTPAR.Com4Speed; // ᪮���� �� �OM3
   ADDPAR.Com2Mode = INTPAR.Com2Mode; // ���⭮��� � ����, 0-���
   ADDPAR.OdoMask = INTPAR.OdoMask; // ��᪠ ���ਧ�樨
   ADDPAR.ChannelJoin = INTPAR.ChannelJoin;  // ���襭�� ���਩��� ᨣ������樨
   ADDPAR.QminFlag = INTPAR.QminFlag; // �ਧ��� ���室� �� ��������� ��室
   ADDPAR.FullNormVolume = INTPAR.FullNormVolume; // � ��娢 ���� ��ꥬ ��� ������਩��
   ADDPAR.Keyboard = INTPAR.Keyboard; // ����稥 ����������
   ADDPAR.HeatReport = INTPAR.HeatReport;
					// ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(struct addpar)+6,0xE4);
					// ������ ���p�
      memmove(&BufTrans_1[4],&ADDPAR,sizeof(struct addpar));
   }
}
*/
//--------------------------------------------------------
void WrIntPar_1() {    // ������ �������⥫��� ��ࠬ��஢
 WrIntParp(BufInKadr_1,BufTrans_1,1);                      // ����室������ ��砫쭮� ���䨣�p�樨
}
/*
   int err = 0;
 unsigned int k;
 char  Setspeed4, Setspeed5;
//  if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

   if (BufInKadr_1[2]-22 != sizeof(struct intpar)-2)
      err |= 1;                         // ����p��� ����� ���p��
					// �p���p�� ��p���
 //  if (memcmp(&BufInKadr_1[4],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 4;

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // ��ࠬ���� �� ����ᠭ�
   else {
      memmove(&RESIV_1,&BufInKadr_1[20],sizeof(intpar));
      Setspeed4 = (INTPAR.Com4Speed != RESIV_1.WRINTPAR.Com4Speed);
//      Setspeed5 = (INTPAR.RS485Com5Speed != RESIV_1.WRINTPAR.RS485Com5Speed);
      memmove(&INTPAR,&BufInKadr_1[20],sizeof(intpar));
       if (Setspeed4) SetSpeedCom4();
      Mem_IntParWr();
      SetHartOut();

      ChkQminFlag();                    // �����⠭���� 䫠���� ��� Qmin

      SaveMessagePrefix_1(6,0xD3);      // ��ࠬ���� ����ᠭ�
   }
}
*/
//--------------------------------------------------------
void WrAddConf_1() {       // ������ �������⥫��� ��ࠬ��஢ ���䨣
 WrAddConfp(BufInKadr_1,BufTrans_1,1);                      // ����室������ ��砫쭮� ���䨣�p�樨
}
/*
   int err = 0;
   int i;
   addpar ADDPAR;
   unsigned int k;
   char  Setspeed4, Setspeed5;
   unsigned char flagStart;
//  if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������
       flagStart = 0x0F;
   if (BufInKadr_1[2]-22 != sizeof(struct addpar))
      err |= 1;                         // ����p��� ����� ���p��
					// �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[4],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 4;

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // ��ࠬ���� �� ����ᠭ�
   else {
      memmove(&ADDPAR,&BufInKadr_1[20],sizeof(addpar));
      ADDPAR.RS485Com5Speed++;
      Setspeed4 = (INTPAR.Com4Speed != ADDPAR.Com4Speed);
      Setspeed5 = (INTPAR.RS485Com5Speed != ADDPAR.RS485Com5Speed);
      if (Setspeed4)
      {  MEMAUDIT_1.type = 26;
	 MEMAUDIT_1.altvalue = INTPAR.Com4Speed;
	 MEMAUDIT_1.newvalue = ADDPAR.Com4Speed;
	 INTPAR.Com4Speed = ADDPAR.Com4Speed; // ᪮���� �� �OM4
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
      if (Setspeed5)
       { MEMAUDIT_1.type = 25;
	 MEMAUDIT_1.altvalue = INTPAR.RS485Com5Speed;
	 MEMAUDIT_1.newvalue = ADDPAR.RS485Com5Speed;
	 INTPAR.RS485Com5Speed = ADDPAR.RS485Com5Speed; // ᪮���� �� �OM5
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
      if (INTPAR.Com2Mode != ADDPAR.Com2Mode)
       { MEMAUDIT_1.type = 55;
	 MEMAUDIT_1.altvalue = INTPAR.Com2Mode;
	 MEMAUDIT_1.newvalue = ADDPAR.Com2Mode;
	 INTPAR.Com2Mode = ADDPAR.Com2Mode; // ���⭮��� � ����, 0-���
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
      if (INTPAR.OdoMask != ADDPAR.OdoMask)
       { MEMAUDIT_1.type = 56;
	 MEMAUDIT_1.altvalue = INTPAR.OdoMask;
	 MEMAUDIT_1.newvalue = ADDPAR.OdoMask;
	 INTPAR.OdoMask = ADDPAR.OdoMask; // ��᪠ ���ਧ�樨
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
      if (INTPAR.ChannelJoin != ADDPAR.ChannelJoin)
       { MEMAUDIT_1.type = 57;
	 MEMAUDIT_1.altvalue = INTPAR.ChannelJoin;
	 MEMAUDIT_1.newvalue = ADDPAR.ChannelJoin;
	 INTPAR.ChannelJoin = ADDPAR.ChannelJoin;  // ���襭�� ���਩��� ᨣ������樨
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
      if (INTPAR.QminFlag != ADDPAR.QminFlag)
       { MEMAUDIT_1.type = 58;
         MEMAUDIT_1.altvalue = INTPAR.QminFlag;
         MEMAUDIT_1.newvalue = ADDPAR.QminFlag;
         INTPAR.QminFlag = ADDPAR.QminFlag; // �ਧ��� ���室� �� ��������� ��室
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
	 flagStart = 0;
       }
      if (INTPAR.FullNormVolume != ADDPAR.FullNormVolume)
       { MEMAUDIT_1.type = 59;
         MEMAUDIT_1.altvalue = INTPAR.FullNormVolume;
         MEMAUDIT_1.newvalue = ADDPAR.FullNormVolume;
         INTPAR.FullNormVolume = ADDPAR.FullNormVolume; // � ��娢 ���� ��ꥬ ��� ������਩��
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
      if (INTPAR.Keyboard != ADDPAR.Keyboard)
       { MEMAUDIT_1.type = 60;
         MEMAUDIT_1.altvalue = INTPAR.Keyboard;
         MEMAUDIT_1.newvalue = ADDPAR.Keyboard;
         INTPAR.Keyboard = ADDPAR.Keyboard; //  ASCII=0 ��� RTU=1
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
      if (INTPAR.HeatReport != ADDPAR.HeatReport)
       { MEMAUDIT_1.type = 60;
         MEMAUDIT_1.altvalue = INTPAR.HeatReport;
         MEMAUDIT_1.newvalue = ADDPAR.HeatReport;
         INTPAR.HeatReport = ADDPAR.HeatReport; //  ASCII=0 ��� RTU=1
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }
       if (Setspeed4) SetSpeedCom4();
      Mem_IntParWr();
      ChkQminFlag();                    // �����⠭���� 䫠���� ��� Qmin
     	 n_delay(1);
	 if (flagStart == 0) StartCPU = 0; //�������

      SaveMessagePrefix_1(6,0xE5);      // ��ࠬ���� ����ᠭ�
   }
}
*/
//--------------------------------------------------------
void WrIntPar2_1() {                     // ������ ����७��� ��ࠬ��஢
 WrIntPar2p(BufInKadr_1,BufTrans_1,1);                      // ����室������ ��砫쭮� ���䨣�p�樨
}
/*
   int err = 0;
   intpar2 INTPAR2;
   unsigned int k;
//  if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

   if (BufInKadr_1[2]-22 != sizeof(struct intpar2))
      err |= 1;                         // ����p��� ����� ���p��
					// �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[4],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 4;

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // ����⠭�� �� ����ᠭ�
   else {
      memmove(&INTPAR2,&BufInKadr_1[20],sizeof(intpar2));
      if (INTPAR2.CntBufLen == 2)
      { EraseSD();
     //����⪠ ��ᠭ�樮��஢������ ����㯠
        strncpy(MEMSECUR_1.login, Tlogin1,4);
        MEMSECUR_1.TIMEBEG = BINTIME;
        memset(&MEMSECUR_1.TIMEEND,0,6);
        MEMSECUR_1.Port = 1;
        MEMSECUR_1.mode  = 28;
        k = Mem_SecurWr_1(Mem_SecurGetPage(),0);
      }
      else if (INTPAR2.CntBufLen == 3)
        for (k=0; k<3; k++)
        {
           outportb(0x1D0,BASE_CFG_PAGE);// � ����� �� ���
           poke(SEGMENT,INDMGN+k*2,1); //����� ������� ���ᨢ� ���᪠
           kSDm[k] = 1; //������ ���ᨢ� ���᪠
           poke(SEGMENT,INDPER+k*2,1); //����� ������� ���ᨢ� ���᪠
           kSDp[k] = 1; //������ ���ᨢ� ���᪠
//printf("km[%d]=%d kp[%d]=%d\n",kSDm[k],kSDp[k]);
        }
      else
       INTPAR.CntBufLen  = INTPAR2.CntBufLen;  // ����� �⢥� �.6 (0 - 23, 1-27 ����)

      INTPAR.HartNumCyclRep = INTPAR2.HartNumCyclRep;
      INTPAR.HartNumImmedRep = INTPAR2.HartNumImmedRep;
      INTPAR.HartReqDelay  = INTPAR2.HartReqDelay;
      INTPAR.Com1NoWrite  = INTPAR2.Com1NoWrite;  // RTU=1/ASCII=0  ��������� 1-���� 0-���
      INTPAR.CntAddBufShift = INTPAR2.CntAddBufShift; // ��ࠬ��� ��஫� ����������� (0,1,...)
      if (INTPAR.VolumeDelta != INTPAR2.VolumeDelta)
       { INTPAR.VolumeDelta = INTPAR2.VolumeDelta; // ���४�� 室� �ᮢ
         InQueue(1,conf_ds1340);
       }
      INTPAR.AlarmVolumeMinTime = INTPAR2.AlarmVolumeMinTime; // �����. �६� ��� ���਩���� ��ꥬ�

      Mem_IntParWr();
      SetHartOut();
      SaveMessagePrefix_1(6,0xE3);      // ��ࠬ���� ����ᠭ�
      if (INTPAR2.CntBufLen == 2)
      {
  	n_delay(1);
	StartCPU = 0; //�������
      }
   }
}
*/
//--------------------------------------------------------
void RdNames_1() {                      // �⥭�� ������������
 RdNamesp(BufInKadr_1,BufTrans_1);                      // ����室������ ��砫쭮� ���䨣�p�樨
}
/*
   struct rdnames *p;

   int err = 0;

   if (BufInKadr_1[2] != 6)
      err |= 1;                          // ����p��� ����� ���p��

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      p    = &SEND_1.RDNAMES;             // ������������

                                        // �࣠������
      memcpy(p->NamePlant,GLCONFIG.NamePlant,16);
                                        // ��ꥪ�
      memcpy(p->NameCal,  GLCONFIG.NameCal,  16);
                                        // �����᪮� �����
      memcpy(p->Serial,  GLCONFIG.Serial,  8);

      memcpy(p->Telephone,GLCONFIG.Telephone,13);// ⥫�䮭
      memcpy(p->IP_Adress,  GLCONFIG.IP_Adress,  13); //IP ����
      memcpy(p->IP_Port,  GLCONFIG.IP_Port,  4); // ����
                                        // ������ �p�䨪�
      if (BufInKadr_1[3] == 0x54)
       SaveMessagePrefix_1(sizeof(rdnames)+6,0xD4);
      else
      SaveMessagePrefix_1(sizeof(rdnames)+6,0xA4);
                                        // ������ � ���p ��।��
      memmove(&BufTrans_1[4],&SEND_1,sizeof(rdnames));
   }
}
*/
//--------------------------------------------------------
void WrNames_1() {                      // ������ ������������
 WrNamesp(BufInKadr_1,BufTrans_1,1);
}
/*
   struct wrnames *p;
   int i,KS=52;
   char c,e;
   unsigned long L;
   unsigned char Ser[6];
   unsigned char psSer[11];
   char far *LgPars;
   int err = 0;

   if (BufInKadr_1[2]-22 != sizeof(wrnames))
      err |= 1;                         // ����p��� ����� ���p��

                                        // �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[4],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 4;

   if (err)
      SaveMessagePrefix_1(6,0xFF);
   else {
      p = &RESIV_1.WRNAMES;             // ������������
                                        // ��p������� ��p����p��
      memmove(&RESIV_1,&BufInKadr_1[20],sizeof(wrnames));

                                        // �࣠������
      memcpy(GLCONFIG.NamePlant,p->NamePlant,16);
					// ��ꥪ�
      memcpy(GLCONFIG.NameCal,p->NameCal,16);
				       // �����᪮� �����
      if (memcmp(GLCONFIG.Serial,p->Serial,8)) //!=
       { MEMAUDIT_1.type = 47;
	  movmem((GLCONFIG.Serial+3),&MEMAUDIT_1.altvalue,4);
	  memcpy(GLCONFIG.Serial,p->Serial,8);
	  movmem((p->Serial+3),&MEMAUDIT_1.newvalue,4);
	       for (i=0; i<GLCONFIG.NumRun; i++)   // ࠧ�᫠�� �����. �� ��⪠�
		     Mem_AuditWr_1(Mem_GetAuditPage(i));
       }

// ���᫥��� ��஫�
//       memcpy(GLCONFIG.Serial,p->Serial,8);

        memcpy(Ser,GLCONFIG.Serial+1,6);
        L= atol(Ser);
        if (L==0)
          memcpy(psSer,"          ",10);
        else
        for (i=0; i<10; i++)
	{ c =(L % KS);
          e=c+65;
          if (e > 90) e+=6;
          psSer[i] = e;
          L = L + c + i+1;
          if (i==0) L=L+INTPAR.CntAddBufShift; // ��.��ࠬ��� ����� ���� ���稪�
        }                                 //���. ��ࠬ��� ��஫� ���.
        psSer[11] = '\0';
        memcpy(LogPassw[0].Password,psSer,10);
      outportb(0x1d0,RESERV_CFG_PAGE);       // ��⠭����� ��p����� �����
      LgPars = (char far *)MK_FP(SEGMENT,4100);
      _fmemcpy(LgPars,(char far *)psSer,10);
      memcpy(GLCONFIG.Telephone,p->Telephone,13);// ⥫�䮭
      memcpy(GLCONFIG.IP_Adress, p->IP_Adress,  13); //IP ����
      memcpy(GLCONFIG.IP_Port, p->IP_Port,  4); // ����

      if (Mem_ConfigWr() == 0)  {       // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
         SaveMessagePrefix_1(6,0xD5);   // ������������ 䫠� ����ᠭ�
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // ������������ �� ����ᠭ�
   }
}
*/
//------------------------------------------------
void RdVolume_1() {                        // �⥭�� ��ꥬ��
  RdVolumep(BufInKadr_1,BufTrans_1);
}
/*
   struct rdvolumes *p;
   int err = 0;
   char num;

   if (BufInKadr_1[2] != 7)
      err |= 1;                          // ����p��� ����� ���p��

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      num = 3 & BufInKadr_1[4];
      p    = &SEND_1.RDVOLUM;             // ��ꥬ�
                                        // �����
      p->VruTotal = ADCONFIG.ADRUN[num-1].Vtot_l;
      p->VruShtat = ADCONFIG.ADRUN[num-1].Vtrush;
      p->VruAvar  = ADCONFIG.ADRUN[num-1].Vtrual;                                  // ������ �p�䨪�
      p->VsuTotal = Persist_GetQTotal(num-1);
      p->VsuShtat = 0.0;
      p->VsuAvar  = 0.0;
      SaveMessagePrefix_1(sizeof(rdvolumes)+7,0xE6);
      BufTrans_1[4] = num;                 // ����p ��⪨
                                        // ������ � ���p ��।��
      memmove(&BufTrans_1[5],&SEND_1,sizeof(rdvolumes));
   }
}
*/
//--------------------------------------------------------
void RdNameSD_1(){     // �⥭�� ������������ �� SD=�����
   struct rdnameSD *p;
   unsigned char run;

   int err = 0;

   if (BufInKadr_1[2] != 7)
      err |= 1;                          // ����p��� ����� ���p��

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      run = BufInKadr_1[4]-1;
      p    = &SEND_1.RDNAMESD;             // ������������
                                        // ��ꥪ�
      memcpy(p->NameCal, GLCONFIG.NameCal, 16);
                                        // �����᪮� �����
      memcpy(p->Serial, GLCONFIG.Serial, 8);
                                         // ��� ��⪨
      memcpy(p->NameRun, GLCONFIG.IDRUN[run].NameRun, 16);
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(rdnameSD)+7,0xA4);
                                        // ������ � ���p ��।��
      BufTrans_1[4] = run+1;
      memmove(&BufTrans_1[5],&SEND_1,sizeof(rdnameSD));
   }
}
//--------------------------------------------------------
void SDsend_1()
{ char buf[80];
   int i,err = 0;
   {
    if (BufInKadr_1[2] != 6)
      err |= 1;                          // ����p��� ����� ���p��
      memcpy(&BufTrans_1[4],&InitSD_Res,10);
// printf("3-c=%d s=%d m=%d\n",counttime, BINTIME.seconds,BINTIME.minutes);
    if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
    else
      SaveMessagePrefix_1(16,0xA5);
//sprintf(buf,"%02X %02X %02X err=%2X\n\r",BufTrans_1[4], BufTrans_1[5],BufTrans_1[6], err);
//ComPrint_2(buf);
    CheckBufTransIniSend_1();
   }
    if (SysTakt == 1)
    for (i=0; i<GLCONFIG.NumRun; i++)
     { nRunG  = i;
       WriteArcData();      // ������ � ��娢 � ��������
     }
    else
     if (BINTIME.seconds % 2 == 0)
      for (i=0; i<2; i++)
      { nRunG  = i;
        WriteArcData();      // ������ � ��娢 � ��������
      }
     else
      { nRunG  = 2;
        WriteArcData();      // ������ � ��娢 � ��������
      }
}
//--------------------------------------------------------
void InitSD_1(){     // ���樠������  SD-����� �� COM1
    bSDOff1 = 1;
// printf("2-c=%d s=%d\n",counttime, BINTIME.seconds);
}
//----------------------------------------------------------
void RdEdizm_1()
{
  RdEdizmp(BufInKadr_1,BufTrans_1);
}
/*
   struct Edizm      *ped;              // ��p����p� ��⪨

   int           err = 0;

   if (BufInKadr_1[2] != 6)
      err |= 1;                          // ����p��� ����� ���p��

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SaveMessagePrefix_1(sizeof(Edizm)+4,0xE7); // ������ �p�䨪�
      movmem(&GLCONFIG.EDIZM_T,&BufTrans_1[4],sizeof(Edizm)-5);// ������ ���p�
      BufTrans_1[10] =  GLCONFIG.EDIZM_T.T_SU | (GLCONFIG.EDIZM_T.T_VVP << 2)
       | (GLCONFIG.EDIZM_T.T_AP << 4);
//printf("TSU=%02X\n" ,BufTrans_1[10]);
      movmem(&GLCONFIG.EDIZM_T.EdIzP,&BufTrans_1[11],3);
   }
}
*/
//-----------------------------------------------------------
void WrEdizm_1()
{
  WrEdizmp(BufInKadr_1,BufTrans_1,1);
}
/*
   int       k, err = 0;
   char T_SUn,T_SUP, EdEn;
   char *buf;

   if (BufInKadr_1[2]-22 != sizeof(Edizm)-2)
      err |= 1;                          // ����p��� ����� ���p��

                                         // �p���p�� ��p���
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 4;

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // �� ����ᠭ�
   else
            // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
   {
     T_SUn  = (BufInKadr_1[26] & 0x03);
     T_SUP =  (BufInKadr_1[26]>>2) & 0x03;
     EdEn = BufInKadr_1[29];
     if (T_SUP != GLCONFIG.EDIZM_T.T_VVP)
     {
         MEMAUDIT_1.type     = 67;
         MEMAUDIT_1.altvalue = GLCONFIG.EDIZM_T.T_VVP;
         MEMAUDIT_1.newvalue = T_SUP;
        for (k=0; k<3; k++)
        {  Mem_AuditWr_1(Mem_GetAuditPage(k));// ������ ���� ����⥫���
           CalcZc[k]=1;
        }
     }
     if (T_SUn != GLCONFIG.EDIZM_T.T_SU)
     {
         MEMAUDIT_1.type     = 62;
         MEMAUDIT_1.altvalue = GLCONFIG.EDIZM_T.T_SU;
         MEMAUDIT_1.newvalue = T_SUn;
      for (k=0; k<3; k++)
      {  Mem_AuditWr_1(Mem_GetAuditPage(k));// ������ ���� ����⥫���
        GLCONFIG.IDRUN[k].ROnom *= KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // ��ॢ�� �  T_SUn ��.�
        GLCONFIG.IDRUN[k].Heat *=  KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // ��ॢ�� �  T_SUn ��.�
        GLCONFIG.IDRUN[k].R0min *= KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // ��ॢ�� �  T_SUn ��.�
        GLCONFIG.IDRUN[k].R0max *= KoRo[GLCONFIG.EDIZM_T.T_SU][T_SUn]; // ��ॢ�� �  T_SUn ��.�
            CalcZc[k]=1;
//printf("Rnom=%f Heat=%f\n", GLCONFIG.IDRUN[k].ROnom, GLCONFIG.IDRUN[k].Heat);
      }
     }
       for (k=0; k<3; k++)
       {
            if (GLCONFIG.EDIZM_T.EdIzmE != EdEn)
            { MEMAUDIT_1.altvalue = GLCONFIG.IDRUN[k].HsVV;
              if (GLCONFIG.EDIZM_T.EdIzmE == 1)
               GLCONFIG.IDRUN[k].HsVV = GLCONFIG.IDRUN[k].Heat
                     * KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
              else
               GLCONFIG.IDRUN[k].HsVV = GLCONFIG.IDRUN[k].Heat*KoefEn[EdEn]
                     * KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
              MEMAUDIT_1.type     = 37;
              MEMAUDIT_1.newvalue = GLCONFIG.IDRUN[k].HsVV;
              Mem_AuditWr_1(Mem_GetAuditPage(k));
            }
          if (T_SUP != GLCONFIG.EDIZM_T.T_VVP)
          {
            MEMAUDIT_1.altvalue = GLCONFIG.IDRUN[k].RoVV;
            GLCONFIG.IDRUN[k].RoVV *= KoRo[GLCONFIG.EDIZM_T.T_VVP][T_SUP]; // ��ॢ�� � T 0 ��.�
            MEMAUDIT_1.type     = 1;
            MEMAUDIT_1.newvalue = GLCONFIG.IDRUN[k].RoVV;
            Mem_AuditWr_1(Mem_GetAuditPage(k));

            MEMAUDIT_1.altvalue = GLCONFIG.IDRUN[k].HsVV;
            GLCONFIG.IDRUN[k].HsVV *= KoRo[GLCONFIG.EDIZM_T.T_VVP][T_SUP]; // ��ॢ�� � T 0 ��.�
            MEMAUDIT_1.type     = 37;
            MEMAUDIT_1.newvalue = GLCONFIG.IDRUN[k].HsVV;
            Mem_AuditWr_1(Mem_GetAuditPage(k));
          }
       } //for (k=0; k<3; k++)
            GLCONFIG.EDIZM_T.T_SU =  T_SUn;
            GLCONFIG.EDIZM_T.T_VVP =  T_SUP;
            GLCONFIG.EDIZM_T.T_AP =  (BufInKadr_1[26]>>4);
//printf("T_SU=%d T_SUP=%d T_AP=%d\n", T_SUn, T_SUP, GLCONFIG.EDIZM_T.T_AP);
      if (BufInKadr_1[27] != GLCONFIG.EDIZM_T.EdIzP) {
         MEMAUDIT_1.type     = 63;
         MEMAUDIT_1.altvalue = GLCONFIG.EDIZM_T.EdIzP;
         MEMAUDIT_1.newvalue = BufInKadr_1[27];
         GLCONFIG.EDIZM_T.EdIzP = BufInKadr_1[27];
	 Mem_AuditWr_1(Mem_GetAuditPage(0));// ������ ���� ����⥫���
	 Mem_AuditWr_1(Mem_GetAuditPage(1));// ������ ���� ����⥫���
	 Mem_AuditWr_1(Mem_GetAuditPage(2));// ������ ���� ����⥫���
      }
      if (BufInKadr_1[28] != GLCONFIG.EDIZM_T.EdIzmdP) {
         MEMAUDIT_1.type     = 64;
         MEMAUDIT_1.altvalue = GLCONFIG.EDIZM_T.EdIzmdP;
         MEMAUDIT_1.newvalue = BufInKadr_1[28];
         GLCONFIG.EDIZM_T.EdIzmdP = BufInKadr_1[28];
	 Mem_AuditWr_1(Mem_GetAuditPage(0));// ������ ���� ����⥫���
	 Mem_AuditWr_1(Mem_GetAuditPage(1));// ������ ���� ����⥫���
	 Mem_AuditWr_1(Mem_GetAuditPage(2));// ������ ���� ����⥫���
       }
      if (BufInKadr_1[29] != GLCONFIG.EDIZM_T.EdIzmE) {
         MEMAUDIT_1.type     = 65;
         MEMAUDIT_1.altvalue = GLCONFIG.EDIZM_T.EdIzmE;
         MEMAUDIT_1.newvalue = BufInKadr_1[29];
         GLCONFIG.EDIZM_T.EdIzmE =BufInKadr_1[29];
	 Mem_AuditWr_1(Mem_GetAuditPage(0));// ������ ���� ����⥫���
	 Mem_AuditWr_1(Mem_GetAuditPage(1));// ������ ���� ����⥫���
	 Mem_AuditWr_1(Mem_GetAuditPage(2));// ������ ���� ����⥫���
      }

      movmem(&BufInKadr_1[4+16],&GLCONFIG.EDIZM_T,6);//��p������� ��p����p��
      if (Mem_ConfigWr()==0)
      {
         SaveMessagePrefix_1(6,0xE8);  //������� ����७�� ����ᠭ�
      }
      else
      {   SaveMessagePrefix_1(6,0xFF);  //������� ����७�� �� ����ᠭ�
//         sprintf(buf,"err=%d\n\r",err);
//          ComPrint_2(buf);
      }
   }
}
*/
//--------------------------------------------------
