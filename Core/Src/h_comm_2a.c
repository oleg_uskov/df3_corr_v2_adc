//////////////////////////////////////////////////////////////////
// ��� �� ����� ���1.
//////////////////////////////////////////////////////////////////

//#ifdef NEW_BUTN

#include <stdio.h>
#include <stdlib.h>


#include <math.h>
#include <string.h>
#include "h_main.h"
 
#include "comm_str.h"
#include "h_comm_2.h"
#include "h_time.h"
#include "h_calc.h"
#include "h_analog.h"
#include "h_mem.h"
#include "h_hart.h"
#include "h_disp.h"
 
#include "h_valarm.h"
#include "h_485.h"
#include "h_smooth.h"                     // ��� �業�� ����� ��饭�� ���稪�
#include "h_persist.h"
#include "h_summwint.h"
//#include "h_msd.h"

//---------------------------------------------------------------------------
#define         V_ALARM            511
#define         maxindex_1         384  //768
#define         transindex_1       526
#define         maxcounttimeaut_1   20  // ����. ���-�� ⠪⮢ �� 25 ��
#define   RIRQ0    0xFF38    //C0M2=FF3E   COM1=FF38
#define         SyncByte          0xAA  // ᨭ�p����� �p� ���p��
#define         wPolynom        0xA001  // �������

//---------------------------------------------------------------------------
void ComPrint_2m(char *Txt);
void ComPrint_2(char *txt);
void n_delay (int mlsek);
void GetVersion(struct Id *pid);
void interrupt Int_0C(...);
void Int_Com_In_1();                    // �p��� ���p� �� �p�p뢠���
void Int_Com_Out_1();                   // ��p���� ���p� �� �p�p뢠���
void Ini_Reciv_1();
void Swap4(char *p);
long Search_SD_blokmg(int nRun, char y,char m,char d,char h,char mn,char s,
                      char y1,char m1,char d1,char h1,char mn1,char s1);
long BlankBlok(long,int);
int Read_SD_blokmg(int,long);
int Read_SD_blok(int nRun, char y,char m,char d,char h,char mn,char s,
                     char y1,char m1,char d1,char h1,char mn1,char s1);
void  RecievMODBUS_1();
void  RecievMODBUSP(unsigned char* BufInKadr,unsigned char* BufTrans,unsigned int* SendCount, char nPort);

unsigned char NumCal;
//---------------------------------------------------------------------------
extern float IndRe[3],IndC_[3],IndKsh[3],IndQ[3],StartRe[3],IndDP[3];
extern struct mon_summ MonthSumm[3];    // ������ �㬬����
                                        // �㬬���� ���਩��� ��ꥬ��
extern struct valarm_data far *ValarmSumm;
extern struct valarm_data far *ValarmSumm2;
extern struct valarm_data ValarmData_1; // ������ ��娢� ���਩��� ��ꥬ��
//extern long  AllAlarmCount[3];          // ���稪 ��� ���਩
//     IzmAlarmCount[3];                // ���稪 ���਩ ����७��

extern struct glconfig    GLCONFIG;
extern struct AddCfg      ADCONFIG;
extern struct intpar      INTPAR;
extern struct bintime     BINTIME;      // ����筠� ��p���p� �p�����
extern struct sumtdata    SUMTDATA[];   // ��� �㬬�p������ ������ �� ������, ��, ��⪨
extern struct calcdata    CALCDATA[];

extern struct memdata      MEMDATA_1;   // ��p���p� ����� ���� - 26 ����
extern struct memaudit     MEMAUDIT_1;  // ��p���p� ����� �p娢� ����⥫���
extern struct memalarm     MEMALARM_1;  // ��p���p� ����� �p娢� ���p��
extern struct startendtime STARTENDTIME_1;//,STARTENDTIME_12K,STARTENDTIME_11K; // ��p���p� ��� ���᪠ ����ᥩ � ��p�����᪮� �p娢�
extern struct memsecurit   MEMSECUR_1;
extern struct new_count Cnt[];          // ���ᨢ ������ ���稪�
extern unsigned char HartFormat;        // �ଠ� Hart-�������
//extern struct hartdata *ph;             // ���� ���� ���稪��
extern struct rs485data *p485d;         // ���� ���� ���稪��
extern unsigned char BufTrans[];
extern unsigned int SendCount;
extern unsigned int outindex,Dostup;
extern unsigned long int TimeReq1;       // �६� �� ��᫥����� ����, �
extern unsigned char AnCom5;
//void SearchKadr_1();                    // ���� �p���⮣� ���p� � ���p� �p����
//void SearchKadr_Hartservice_1();      //���� �p���⮣� ���p�
extern unsigned char mdatar[514];
extern long Ble;
extern int LongQueue[4];
extern char str[];
extern unsigned int  counttime;
extern unsigned int di1p[2][4],di2p[2][4];
extern int CErrMB;
extern unsigned char errMODBUS[];
extern int SysTakt;
extern char CalcZc[];
extern float KoRo[3][3];
extern  bintime logTimeb1;
//extern FILE *fp;
extern  char  ModbusTCP_1;
extern  char  ModbusRTU_1;

//---------------------------------------------------------------------------
unsigned char Flag_Hartservice_1;       // �ਧ��� ०��� �࠭��樨 HART<->COM
float g_Qst_1;                          // ��砫�� ��ꥬ �� ��, �3 �� ������ �����
extern int Nwr,Nwr1; // ����� �����
extern unsigned int g_cntIniRecCom1;           // ���稪 ���樠����権 �ਥ�� �� COM1
extern double KoefP[4]; // = {1.0,0.0980665,98.0655,0.980665};
extern double KoefdP[3]; // = {1.0, 0.00980655, 0.0980665};
extern double KoefEn[3]; // = {238.846, 1.0, 0.277778};
                                        // ��� ����� � ����ᠭ�ﬨ ����㭨��樨 �� COM1
unsigned int    counttimeaut_1;         // ���-�� ⠪⮢ �� 25 ��
unsigned int    SendCount_1;
unsigned int    comindex_1;             // ����� inindex � ������ �p�p뢠��� �� ⠩��p�
unsigned int    inindex_1;              // �. �� ���⮩ �������, inc �� �p�p. �p�������, =0 �p� ����.
//---------------------------------------------------------------------------
unsigned char LastRecFlag_1 = 0;        // 䫠� �뤠� ��᫥���� �����
unsigned int    outindex_1,Dostup1;
unsigned char   flagspeed_1;            // 䫠� ��������� ᪮p��� �� COM-��p��
                                        // ���p ��� ��p����
unsigned char   BufTrans_1[transindex_1+1];
                                        // ���p �p����
unsigned char   BufResiv_1[maxindex_1+1];
unsigned char   BufOutKadr_1[256];      // ���p ���஢ ��� ��p����
unsigned char   BufInKadr_1[256];       // ���p �p���⮣� ����
struct calcdata   COPY_CALCDATA_1[3];
struct bintime BinTmp_1,BinTmp2_1;
char numlogin1;
unsigned char  Tlogin1[5];
char attempt1[7];
char PASSWR1[16];
char SDMgn1, SDPer1;  // 䫠� ������ �����
int nRunSD1;
unsigned char MBAPH_1[7];
extern char portC;
//unsigned long TimeKP1[2];
//char NumKan1;
char Comnd;
//---------------------------------------------------------------------------
union send SEND_1;
//---------------------------------------------------------------------------
union resiv RESIV_1;
/*--------------- ��砫� ���ᠭ�� ��p���p� � �㭪樨 0x56 ----------------*/
extern GornalPassw LogPassw[6];   //6*15=90 ���� c 4096 ���� ���. 81
/*--------------- ��砫� ���ᠭ�� ��p���p� � �㭪樨 0x57 ----------------*/
extern GornalDostup ArhivDostup[10]; //60*17=1020    ���� c 4246 ���� ���. 81
//---------------------------------------------------------------------------
float Round_toKg(float, double ,float*);

//---------------------------------------------------------------------------
// �㭪�� ���樠����樨 COM ����
// ��p�� COM ��p� � ᪮p���� ��p���� �� 300 �� 57600 bps
void Port_Ini_1()
{//asm cli
      outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����
      outportb(comport1+2,0x87);           // ������� FIFO
      outportb(comport1+1,0x00);        // ������� ���뢠��� �ਥ����� � ��।��稪�
//asm sti
}
//---------------------------------------------------------------------------
void IniCfgSpeed_2()
{
   int s;
//asm cli
   switch (GLCONFIG.Speed_2) {            // ᪮p���� �� COM-��p��
      case 0: s=192; break; //1200
      case 1: s=96; break; //2400
      case 2: s=48; break; //4800
      case 3: s=24; break;//9600
      case 4: s=12; break;//19200
      case 5: s=6; break;  // 38400
      case 6: s=4; break; //  57600
      case 7: s=2; break; //115200
      default: s=24;   //9600
   }
    outportb(comport1+3,inportb(comport1+3) | 0x80);
   _AX=s;
   outportb(comport1,_AL);              // ��. ���� ����⥫�
   _AX=s;
   outportb(comport1+1,_AH);            // ��. ���� ����⥫�
    outportb(comport1+3,inportb(comport1+3) & 0x7F);

  flagspeed_2 = 0;  //�p�� 䫠�� �p�������� ��������� ᪮p���
//asm sti
}
//---------------------------------------------------------------------------
//��⠭���� ����p� �p�p뢠��� �� com1-IRQ4
void Ini_Int_Com_1()
{ char s;
//   outportb(comport1+1,0x01); //������� �p�p뢠��� �p������� � ��।��稪�
//asm cli
 s = inportb(comport1+5);
 s = inportb(comport1);
   setvect(0x0C,Int_0C);
//asm sti
}

//---------------------------------------------------------------------------
//��p���� ���p� �� �p�p뢠��� ��p����稪�
//�p��� ���p� �� �p�p뢠��� �p�������
void interrupt Int_0C(...)
{
  static unsigned char IdInt;           // p�����p �����䨪�樨 �p�p뢠���
//asm cli;
//  do
    {
      IdInt = inportb(comport1+2) & 7;       // p�����p �����䨪�樨 �p�p뢠���
        switch(IdInt)
          {
	    case 0x06:                  // �訡�� ��� ��p�p�
              inportb(comport1+5);       // p�����p ����� �����
              break;
            case 0x04:                  // �p��� ���p�
              Int_Com_In_1();
              break;
            case 0x02:                  // ��p���� ���p�
              Int_Com_Out_1();
              break;
            case 0x00:                  // ��������� ����� ������
              inportb(comport1+6);       // p�����p ����� ������
              break;
	  }
    }
//  while ( inportb(comport1+2) != 1);
//asm sti;
  outport(0xFF22,0x000C);
}
//---------------------------------------------------------------------------
void Int_Com_In_1()                     // �p��� ���p� �� �p�p뢠���
{
  char dt;
 // int k;
//ComPrint_2("R");
//asm cli
/*  outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����
  outportb(comport1+4,0x00);            // DTR + ��⨥ RTS + �p�p뢠���
  outportb(comport1+1,0x01);            // p��p���� �p�p뢠��� �p�������
*/
  if( inindex_1 < maxindex_1 )
    { if ((inportb(comport1+5) & 1)==1)
     do
      { dt=inportb(comport1);   //�⥭�� ��p�
	BufResiv_1[inindex_1] = dt;
	if (inindex_1 < maxindex_1 )  inindex_1++;  //ᬥ饭�� �� ᢮�. �������
      }
      while  ((inportb(comport1+5) & 1)==1);
    }
  else
    { do
       BufResiv_1[inindex_1] = inportb(comport1); // �⥭�� ��p�
      while  ((inportb(comport1+5) & 1)==1);
    }
//asm sti
}
//---------------------------------------------------------------------------
void Int_Com_Out_1()  //��p���� ���p� �� �p�p뢠���
{
  int i;
 static int count;
//ComPrint_2("S");
/*  outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����
  outportb(comport1+4,0x02);               //DTR + RTS + �p�p뢠���
  outportb(comport1+1,0x02);            // p��p���� �p�p뢠��� ��।��稪�
*/
//asm cli
    if ( outindex_1 < SendCount_1 )
    { count = 0;
    do
     { outportb(comport1,BufTrans_1[outindex_1]);
      outindex_1++; count++;

     }
     while ((outindex_1 < SendCount_1) && (count < 16));
    }
  else
    {
//      outportb(comport1,BufTrans_1[outindex_1]);
   inindex_1=0;                          // ᬥ饭�� �p����
  comindex_1 = inindex_1;               // ����� inindex � ������ �p�p뢠��� �� ⠩��p�
  counttimeaut_1 = 0;                   // ���-�� ⠪⮢ ⠩���� ��᫥ �p���� ��᫥����� ����
 outportb(comport1+2,0x83);   // ������ ���� �ਥ����� FIFO
  for(i=0; i<100; i++) { if(inportb(comport1+5) & 0x40) break; }
//  outportb(comport1+4,0x09);            // DTR + ��⨥ RTS + �p�p뢠���
  if (GLCONFIG.Speed_1 < 2)     //2400
    n_delay(10);
  else
  if (GLCONFIG.Speed_1 < 3)   // 4800
    n_delay(6);
  else  if (GLCONFIG.Speed_1 < 5)
    n_delay(2);                    //9600   19200
  outportb(comport1+4,0x00);   //RTS=0
  outportb(comport1+1,0x01);            // p��p���� �p�p뢠��� �p�������
//  outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����

//  outport(RIRQ0,(inport(RIRQ0) & 0xFFF7));// p��p���� IRQ0
  g_cntIniRecCom1++;
     }
//asm sti
}
//---------------------------------------------------------------------------
//���樠������ ��p���� ���p�� ᮮ�饭�� �� �p�p뢠��� ��p����稪�
void Ini_Send_1()
{
//  char str[40];
  int kkk = BufTrans_1[2];
//sprintf(str,"IS r1=%02X r4=%02X r3=%02X\n\r",inportb (comport1+1),inportb (comport1+4),inportb (comport1+3));
//ComPrint_2(str);
//asm cli
  if (kkk > 255) kkk = 0;
  if (((BufTrans_1[3] == 0xA0)||(BufTrans_1[3] == 0xA1))
     && (BufTrans_1[2] == 0x0C)) kkk+=512;
//sprintf(str,"Trans1=%d\n",kkk);
//ComPrint_2(str);
  int Prmb;
  Prmb = GLCONFIG.PreambNum_1;
  movmem(BufTrans_1,BufTrans_1+Prmb,kkk);  // ᤢ����� ���p ��।��
  memset(BufTrans_1,0xFF,Prmb);          // �������� �ॠ����
  if (Prmb != 0) {
     BufTrans_1[kkk+Prmb] = 0xFF;
     BufTrans_1[kkk+Prmb+1] = 0xFF;
  }
  if (Prmb == 0)
    SendCount_1 = kkk;//+Prmb-1;  //����� ���p�
  else
    SendCount_1 = kkk+Prmb+1;
//sprintf(str,"SCount=%d\n\r",SendCount_1);
//ComPrint_2(str);
      if(flagspeed_1) SendCount_1 += 2;//��� ��p������ 2-� ���⮢ �p� ��������� ᪮p���
  outindex_1=0;       //ᬥ饭�� � ��p��������� ���p�
//  outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����
  outportb(comport1+2,0x85);  // ������ ���� ��।��. FIFO
//  outportb(comport1+4,0x0B);               //DTR + RTS + �p�p뢠���
  outportb(comport1+4,0x02);  // DTR=1 ��।��
  outportb (comport1+1,0x02);              //p��p���� �p�p뢠��� ��p����稪�
//  outport(RIRQ0,(inport(RIRQ0) & 0xFFF7)); //p��p���� IRQ0
//asm sti
}
//---------------------------------------------------------------------------
void Ini_Reciv_1()  //���樠������ �p����
{//char str[40];
// memset(BufResiv_1,0,maxindex_1+1);    // ���⪠ �室���� ���p�
     if(flagspeed_2) IniCfgSpeed_2();

//sprintf(str,"IR r1=%02X r4=%02X r3=%02X\n\r",inportb (comport1+1),inportb (comport1+4),inportb (comport1+3));
//ComPrint_2(str);
//asm cli

  inindex_1=0;                          // ᬥ饭�� �p����
  comindex_1 = inindex_1;               // ����� inindex � ������ �p�p뢠��� �� ⠩��p�
  counttimeaut_1 = 0;                   // ���-�� ⠪⮢ ⠩���� ��᫥ �p���� ��᫥����� ����
  outportb(comport1+2,0x83);   // ������ ���� �ਥ����� FIFO
//  outportb(comport1+4,0x09);            // DTR + ��⨥ RTS + �p�p뢠���
  outportb(comport1+4,0x00);   // RTS=0
//  outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����
  outportb(comport1+1,0x01);            // p��p���� �p�p뢠��� �p�������
//  outport(RIRQ0,(inport(RIRQ0) & 0xFFF7));// p��p���� IRQ0
//asm sti
  g_cntIniRecCom1++;                    // ���稪 ���樠����権 �ਥ�� �� COM1
                                        // ��� ����� � ����ᠭ�ﬨ ����㭨��樨 �� COM1
}
//---------------------------------------------------------------------------
// �p�������� �p� �맮�� ����p�⭮�� �p�p뢠��� ⠩��p�
void ChekBufResiv_1() {
   unsigned char           *up,*bsrch,*esrch; //,str[40];
   unsigned int   insrch;
   int l;
   char bf[20];
// ComPrint_2("Ch ");
/*
   outportb(comport1+3,0x03);        // �p��� ��p����   n,8,1, �����
   if (inportb(comport1+1)!=0x02)
   outportb(comport1+1,0x01);            // p��p���� �p�p뢠��� �p�������
   if (inportb(comport1+4)!=0x02)
   outportb(comport1+4,0x00);            // p��p���� �p�p뢠��� �p�������
*/
   if (inindex_1>4)
   {
//sprintf(str,"CBR r1=%02X r4=%02X r3=%02X\n\r",inportb (comport1+1),inportb (comport1+4),inportb (comport1+3));
//ComPrint_2(str);
    if (Flag_Hartservice_1)
    {// sprintf(str,"ind1_s=%d %d\n\r",inindex_1,comindex_1);
// ComPrint_2(str);
       TimeReq1=0;// ��� ⠩��� ���� TimeReq1=0;
    if (inindex_1==comindex_1)
     { SearchKadr_Hartservice_1(); inindex_1=0;}
     else comindex_1=inindex_1;
    }
    else //  Flag_Hartservice_1==0
    { bsrch = BufResiv_1;
      insrch = inindex_1;
                                  // ���� ᨭ�p�ᨬ����
      up = (unsigned char*)memchr(bsrch,SyncByte,insrch);
// sprintf(str,"ind1=%d up=%04X\n\r",inindex_1,up);
//ComPrint_2(str);
/*
str[0]=0x0;
for (l=0; l<inindex_1; l++)
{ sprintf(bf," %02X",BufResiv_1[l]);
  strcat(str,bf);
 }
 strcat(str,"\n\r");
ComPrint_2(str);
*/
     if ((BufResiv_1[2]==0) && (BufResiv_1[3]==0)) // �� MOSCAD TCP
     { ModbusTCP_1=1; up = 0;}
     else ModbusTCP_1=0;
     if (up)
     { if (*(up+2) <= inindex_1)
      {                                         // ���� �p���⮣� ���p� � ���p� �p����
       InQueueInt(0,SearchKadr_1);               // � ���筮� ०���
      }
     }
      else  // �᫨ up=0, ᨭ�஡��� �� ������
     {// sprintf(str,"in1=%d %X %X %X %X\n\r",inindex_1,BufResiv_1[0],BufResiv_1[1],BufResiv_1[2],BufResiv_1[3]);
     //  ComPrint_2(str);
//       flagspeed_1=1;
       if (ModbusTCP_1) bsrch += 6;
       up = (unsigned char*)memchr(bsrch,GLCONFIG.NumCal,insrch);
       if ((up) && ((up[1]==0x04) || (up[1]==0x03)))
       { ModbusRTU_1 = 1;
	InQueueInt(0,SearchKadr_1); // �ਥ� ����� MODBUS
       }
       else
       { ModbusRTU_1 = 0;
       Ini_Reciv_1();
     }
      }
     } //else  (Flag_Hartservice_1)
    } //if (inindex_1>4)
}

/*
   if (inindex_1) {
     if ( (comindex_1 == inindex_1) && (counttimeaut_1 == 0) )
      {                                    // ���� �p���⮣� ���p� � ���p� �p����
      if (Flag_Hartservice_1 == 0)
	 SearchKadr_1();               // � ���筮� ०���
      else
         SearchKadr_Hartservice_1();   // � ०��� HART<->COM1
      }     // endif (comindex_1==inindex_1)&& ...

      if (comindex_1 == inindex_1) {
//ComPrint_2("Br2 ");
       counttimeaut_1++;
         if (counttimeaut_1 >= maxcounttimeaut_1) {
              inindex_1 = 0;                // ** //
//             InQueueInt(0,Ini_Reciv_1);    // ���樠������ �p����
            Ini_Reciv_1();    // ���樠������ �p����
	 }
      }
      else {   // if (comindex_1==inidex_1)
         comindex_1 = inindex_1;
         counttimeaut_1 = 0;              // ���-�� ⠪⮢ ⠩����
           }

   } // endif (inidex_1)
}
*/
//---------------------------------------------------------------------------
void SearchKadr_1()//���� �p���⮣� ���p� � ���p� �p����
{
//int l;
//   extern struct glconfig  GLCONFIG;
   unsigned char           *uk,*begin,*bsrch,*esrch;
   unsigned char           count,Nfun_FU;           // ����� ���p�
//   register
   unsigned int   insrch;
   unsigned int            crc,blength,b1,b2,b3,b4;
   char bf[40];
//sprintf(bf,"ind1=%d\n\r",inindex_1);
//ComPrint_2(bf);
/*str[0]=0x0;
for (l=0; l<inindex_1; l++)
{ sprintf(bf," %02X",BufResiv_1[l]);
  strcat(str,bf);
 }
 strcat(str,"\n\r");
ComPrint_2(str);
*/
   bsrch = BufResiv_1;
   esrch  = BufResiv_1 + inindex_1;
   insrch = inindex_1;
   blength = inindex_1;
   while (bsrch<esrch) {
				  // ���� ᨭ�p�ᨬ����
      uk = (unsigned char*)memchr(bsrch,SyncByte,insrch);
      if (ModbusTCP_1) uk=0;  // // MODBUS TCP
      if (ModbusRTU_1) uk=0;  // MODBUS RTU
      if (uk) {                   // ��।��� ᨭ��ᨬ��� ������
	 inindex_1 = 0;             // ** //  ���⠢� - �㡭� - �㡮��� - �����
				  // �⮡� �� �뫮 ����p��� �室�� �� ⠩��p�
b1 = *(uk+1);
b2 = *(uk+2);  b3 = *(uk+3);
b4 = *(uk+4);
Nfun_FU = ((b3 == 1) || (b3 == 2) || (b3 == 3) || (b3 == 6) || (b3 == 10));
// ********* �⫠���
         if ((((b1 == GLCONFIG.NumCal) ||
         (b1 == (unsigned char)INTPAR.RS485Com5Speed)&& Nfun_FU) && (b4>0)) ||
	 (b1 == GLCONFIG.NumCal)&&((b3 == 0x7E)||(b3 == 0x7D)) ||
             (b1 == 255) && (b2 == 6) && (b3 == 1))
         {
	    begin = uk;           // 㪠��⥫� �� ᨭ�p�����

	    count = *(uk+2) - 2;  // ��� 2-� ���⮢ ����p��쭮� �㬬�

	    if (count >= 4) {
               if (count > 254) count = 0;
	       crc = Mem_Crc16Int(uk,count);
	       uk += count;

	       if (crc == *((unsigned int*)uk)) {         // �p������� ����p��쭮� �㬬�
		  movmem(begin,BufInKadr_1,count);        // �᢮������ ���p �p����
//                  asm sti
//ComPrint_2("CN  ");
                  NumCal = b1;
		  InQueueInt(0,CaseNumFun_1);             // �� �믮������ �������
		  break;                                  // ��ࢠ�� ����
	       }
	       else {                                     // �� �� ᮢ����
//                bsrch += 1;                             // ��३� � ᫥���饬�
		  bsrch = begin + 1;                      // ��३� � ᫥���饬�
		  insrch = blength - (bsrch - BufResiv_1);  // 㬥����� ����� ���᪠
							  // ᨬ���� � BufResiv
//                insrch -= 1;                            // 㬥����� ����� ���᪠
//                insrch = blength - (bsrch - BufResiv_1);  // 㬥����� ����� ���᪠
	       }
	    }
	    else {
	       bsrch = begin + 1;                      // ��३� � ᫥���饬�
	       insrch = blength - (bsrch - BufResiv_1);  // 㬥����� ����� ���᪠
	    }
	 }
	 else {                                        // �� �����
//          bsrch += 1;                                // ��३� � ᫥���饬�
	    bsrch = uk + 1;                            // ��३� � ᫥���饬�
	    insrch = blength - (bsrch - BufResiv_1);     // 㬥����� ����� ���᪠
						       // ᨬ���� � BufResiv
	    insrch -= 1;                               // 㬥����� ����� ���᪠
//          insrch = blength - (bsrch - BufResiv_1);     // 㬥����� ����� ���᪠
	 }
      }
      else {                                           // ᨭ��ᨬ���� ��� � ����
       if  (ModbusTCP_1) bsrch += 6;
       uk = (unsigned char*)memchr(bsrch,GLCONFIG.NumCal,insrch);
       if ((uk) && ((uk[1]==0x04) || (uk[1]==0x03)))
         if  (((*(uk-1) + 6) <= inindex_1) && (ModbusTCP_1))
         { count = *(uk-1);
           movmem(uk-6,MBAPH_1,7);
           movmem(uk,BufInKadr_1,count);          // �᢮������ ���p �p����
           Comnd = uk[1];
           InQueueInt(0,RecievMODBUS_1);
         }
         else
         if  (inindex_1 - (uk-bsrch) == 8)
         {  count = 6;
            begin = uk;
            crc = Mem_Crc16Int(uk,count);
            Comnd = uk[1];
            uk += count;
            if (crc == *((unsigned int*)uk))          // �p������� ����p��쭮� �㬬�
            {
              uk = uk -  count;
              movmem(uk,BufInKadr_1,count);          // �᢮������ ���p �p����
//              printf("Reciev MODBUS_1 %d %d",BufInKadr_1[2],BufInKadr_1[3]);
              InQueueInt(0,RecievMODBUS_1);
            }
         }
	 inindex_1 = 0;
//ComPrint_2("N1");
	 Ini_Reciv_1();                      // ���樠������ �p����
	 break;
      }
   }  //end While
}
//--------------------------------------------------------------------------
void  RecievMODBUS_1()
{
  if (Dostup1 > 0)
  RecievMODBUSP(BufInKadr_1,BufTrans_1,&SendCount_1,1);
}

//---------------------------------------------------------------------------
void CaseNumFun_1() {
   int sel = 0;
   int ex,ex1,ex2,ex3;
   char buf[40];
//sprintf(buf,"Ds1=%d Kadr3=%d\n\r",Dostup1,BufInKadr_1[3]);
//ComPrint_2m(buf);
   ex = ex1=ex2=ex3=1;
//   if (AnCom5==0x55)
    if (Nwr == 0) { if (Dostup1 == 3) {Nwr1=1; Dostup1=2;}} // �᫨ ����� ������,
   else if (Nwr1==1) {Dostup1 = 3;Nwr1=0;} //GLCONFIG.StartDostup1;Nwr1=0;}
   // � �஢��� ����㯠 �� ����� ���� == 3

   switch(BufInKadr_1[3]) {
     case 0x01: InQueue(0,AnswId_1);       break; // ���p�� �����䨪��p�
     case 0x58: InQueue(0,fPassword_1);  break;        // ��砫� ᥠ��
     case 0x59: InQueue(0,fendSeans_1);  break;       // ����� ᥠ��
     case 0x7D: InQueue(0,AnswDbg_1);      break; // �뤠� �⫠��筮� ���ଠ樨
     case 0x54: InQueue(0,RdNames_1);      break; // �⥭�� ������������
     default:{ if (Dostup1 == 0) sel++; ex=0; } // ��� ����㯠
   }
   if ((sel==0)&&(ex==1)) goto L1;
   if (Dostup1 == 0)
    {InQueue(0,NoDostup_1); sel=0; goto L1;} // ��� ����㯠
//sprintf(buf,"Ds1=%d sel=%d ex=%d\n\r\0",Dostup1,sel,ex);
//ComPrint_2(buf);

	  if (Dostup1 == 4)
	   switch(BufInKadr_1[3]) {
           case 0x02: if (NumCal == INTPAR.RS485Com5Speed) InQueue(0,RdStat_1);
                      else sel++; break; // �⥭�� ����᪨� ��p����p��
           case 0x03: if (NumCal == INTPAR.RS485Com5Speed)
              InQueue(0,WrStat_1);
                      else
                      sel++; break; // ������ ����᪨� ��p����p��
           case 0x05:  InQueue(0,RdInstparam_1);        break; // ��������� ��p����p�
           case 0x06: if (NumCal == INTPAR.RS485Com5Speed) InQueue(0,AnswChim_1);
                      else sel++; break; //�⥭�� 娬��᪮�� ��⠢�
           case 0x0A: if (NumCal == INTPAR.RS485Com5Speed) InQueue(0,RdDiaph_1);
                      else sel++; break; // ������ ����᪨� ��p����p��
           case 0x43:  InQueue(0,WrRecfg_1); break; // ������ ��p����䨣�p�樨
           default:{InQueue(0,NoDostup_1); sel=0; goto L1; } // ��� ����㯠
	  }
	  else if (Dostup1 == 7)     // �����������
	   switch(BufInKadr_1[3])
	   { // ������ ᯨ᪠ ���� � ��஫��
	   case 0x56: if (Nwr != 0) InQueue(0,fCreateUser_1);  break; // ᮧ����� ���.����ᥩ
	   case 0x54: InQueue(0,RdNames_1);      break; // �⥭�� ������������
	   case 0x55: if (Nwr != 0) InQueue(0,WrNames_1);  break; // ������ ������������
	   case 0x60: InQueue(0,fUserRd_1);  break; //�⥭�� ���짮��⥫��
           case 0x61: InQueue(0,RdSecur_1);      break;  // �⥭�� ��娢� ������᭮��
           case 0x67: InQueue(0,RdEdizm_1);      break; // �⥭�� ������ ����७��
           case 0x68: InQueue(0,WrEdizm_1);      break; // ������ ������ ����७��
	   case 0x59:  break;
	   default:  sel++;
	   }
//---------------------------------------------
  else if ((Dostup1 > 0)&& (!ex))
   {    switch(BufInKadr_1[3])
     {  // �⥭��
      case 0x01: InQueue(0,AnswId_1);       break; // ���p�� �����䨪��p�
      case 0x02: InQueue(0,RdStat_1);       break; // �⥭�� ����᪨� ��p����p��
      case 0x04: InQueue(0,RdCalcparam_1);  break; // ����� ��p����p�
      case 0x05: InQueue(0,RdInstparam_1);  break; // ��������� ��p����p�
      case 0x08: InQueue(0,RdConst_1);      break; // �⥭�� ����⠭�
      case 0x0A: InQueue(0,RdDiaph_1);      break; // �� ��ࠬ��஢ �����.   - ���
      case 0x0C: InQueue(0,RdValarm_1);     break; // �⥭�� ���਩��� ��ꥬ��
//      case 0x10: InQueue(0,RdPeriodChr_1);  break; // �� �����. ��ࠬ��஢  - ���
//      case 0x11: InQueue(0,RdDayChr_1);     break; // �� ������ ��ࠬ��஢ - ���
//      case 0x12: InQueue(0,RdCyclChr_1);    break; // �� 横����� ��ࠬ��஢ - ���
//      case 0x13: InQueue(0,RdHourChr_1);    break; // �� �ᮢ�� ��ࠬ��஢  - ���
      case 0x11:
      case 0x1B:
      case 0x14:// if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
		 InQueue(0,RdDay_1);// else InQueue(0,RdDayDen_1);
		  break; // �⥭�� ���筮�� ����
      case 0x15:// if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
		 InQueue(0,RdPeriod_1);// else InQueue(0,RdPeriodDen_1);
		 break; // �⥭�� ��p�����᪮�� ����
      case 0x16: InQueue(0,RdAudit_1);      break; // �⥭�� ��p���� ����⥫���
      case 0x17: InQueue(0,RdAlarm_1);      break; // �⥭�� ��p���� ���p��
      case 0x18: //if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
		 InQueue(0,Comm_RdCykl_1);// else InQueue(0,RdCyclDen_1);
		 break; // �⥭�� 横���᪨� ������
      case 0x13:
      case 0x19:// if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
		 InQueue(0,RdHour_1); //else InQueue(0,RdHourDen_1);
		 break; // �⥭�� �ᮢ��� ����
//      case 0x1A: InQueue(0,RdPeriodDen_1);  break; // �� �����. ��ࠬ��஢  - ���⭮���
//      case 0x1B: if ((INTPAR.Com2Mode==0) && (INTPAR.HeatReport==0))
//		 InQueue(0,RdDay_1); else InQueue(0,RdDayDen_1);
//		  break; // �⥭�� ���筮�� ���� � ��騬 ��ꥬ�� � ��
//      case 0x1C: InQueue(0,RdCyclDen_1);    break; // �� 横����� ��ࠬ��஢ - ���⭮���
//      case 0x1D: InQueue(0,RdHourDen_1);    break; // �� �ᮢ�� ��ࠬ��஢  - ���⭮���
      case 0x22: InQueue(0,AnswSys_1);      break; // �⥭�� ��⥬��� ��p����p��
      case 0x44:  // �⥭�� ��p����p�� ���稪�� ��� �����
      case 0x46: InQueue(0,RdParamdat_1);   break;// �⥭�� ��p����p�� ���稪�� ��� ��-1
      case 0x54: InQueue(0,RdNames_1);      break; // �⥭�� ������������
      case 0x67: InQueue(0,RdEdizm_1);      break; // �⥭�� ������ ����७��

      default: if (Dostup1 == 1) sel++; ex1=0;
     }
    if ((Dostup1 > 1)&&(!ex1))
      {
	switch(BufInKadr_1[3])
	{ // ������ 娬��⠢�
	 case 0x03: if ((NumCal == INTPAR.RS485Com5Speed)&&(Dostup1 == 2))
		     InQueue(0,WrStat_1);
		     else   ex2=0;
		     break; // ������ ����᪨� ��p����p��
	 case 0x06: InQueue(0,AnswChim_1);     break; // �⥭�� 娬��᪮�� ��⠢�
	 case 0x07: InQueue(0,WrChim_1);       break; // ������ 娬��᪮�� ��⠢�
//	 case 0x57: InQueue(0,fRdArchDost_1);  break;  // �⥭�� ��ୠ�� ����㯠
	 case 0x61: InQueue(0,RdSecur_1);      break;  // �⥭�� ��娢� ������᭮��
	 case 0x42: InQueue(0,SetTime_1);      break; // ��⠭���� �p�����
	 case 0x23: InQueue(0,WrSys_1);        break; // ������ ��⥬��� ��p����p��
	 case 0x20: InQueue(0,SD_RdCykl_1); sel=1; TimeReq1=0; break; // �� ���������� �� SD-����
	 case 0x21: InQueue(0,SD_RdPeriod_1); sel=1; TimeReq1=0; break; // �� ��ਮ���᪨� �� SD-����
	 case 0x24: InQueue(0,RdNameSD_1);     break; // �� ���� �� SD-�����
	 case 0x66: InQueue(0,RdVolume_1);       break; // �⥭�� ��ꥬ�� ���稪�
	 case 0x43: InQueue(0,WrRecfg_1);      break; // ������ ��p����䨣�p�樨
	 case 0x49: InQueue(0,AnswClbrTable_1);break; // �⢥� �� ����� �⥭�� ⠡���� �����஢��
	 case 0x4B: InQueue(0,AnswCode_1);     break; // �⢥� �� ����� �⥭�� ���� ���
	 case 0x52: InQueue(0,RdIntPar_1);     break; // �⥭�� �������⥫��� ��ࠬ��஢
	 case 0x62: InQueue(0,RdIntPar2_1);     break; // �⥭�� ����७��� ��ࠬ��஢2
         case 0x63: InQueue(0,WrIntPar2_1);     break; // ������ ����७��� ��ࠬ��஢2
	 case 0x64: InQueue(0,RdAddConf_1);     break; // �⥭�� �������⥫��� ��ࠬ. ���䨣��樨
         case 0x7E: InQueue(0,WrDbg_1);        break; // ������ �⫠��筮� ���ଠ樨
	  default: if (Dostup1 == 2) sel++; ex2=0;
	}
	if ((Dostup1 > 2)&&(!ex2))
	{  // ������ ��ࠬ��஢
     switch(BufInKadr_1[3]) {
      case 0x03: InQueue(0,WrStat_1);       break; // ������ ����᪨� ��p����p��
      case 0x09: InQueue(0,WrConst_1);      break; // ������ ����⠭�
      case 0x0B: InQueue(0,WrDiaph_1);      break; // �� ��ࠬ��஢ �����.   - ���
      case 0x25: InQueue(0,InitSD_1); sel=1;TimeReq1=0;  break; // ���樠������ SD-�����
      case 0x41: InQueue(0,WrStCfg_1);      break; // ������ ��砫쭮� ���䨣�p�樨
      case 0x45: InQueue(0,WrParamdat_1);   break; // ������ ��p����p�� ���稪��
//      case 0x49: InQueue(0,AnswClbrTable_1);break; // �⢥� �� ����� �⥭�� ⠡���� �����஢��
      case 0x4A: InQueue(0,WrClbrTable_1);  break; // ������ ⠡���� �����஢��
//      case 0x4B: InQueue(0,AnswCode_1);     break; // �⢥� �� ����� �⥭�� ���� ���
//      case 0x4C: InQueue(0,WrClbr_1);       break; // ������ �窨 �����஢��
      case 0x4E: InQueue(0,AnswerHComY_1);   break; // ���室 � ०�� HART<->COM
      case 0x4F: InQueue(0,AnswerHCom_1);   break; // ���室 � ०�� HART<->COM
//      case 0x50: InQueue(0,RdNoAlarmConstFlag_1);   break; // �⥭�� 䫠�� ����� ���਩
//      case 0x51: InQueue(0,WrNoAlarmConstFlag_1);   break; // ������ 䫠�� ����� ���਩
      case 0x53: InQueue(0,WrIntPar_1);     break; // ������ ����७��� ��ࠬ��஢
      case 0x40: InQueue(0,AnswStCfg_1);    break; // ����室������ ��砫쭮� ���䨣�p�樨
      case 0x65: InQueue(0,WrAddConf_1);     break; // ������ �������⥫��� ��ࠬ. ���䨣��樨
      case 0x68: InQueue(0,WrEdizm_1);      break; // ������ ������ ����७��

	  default:   // Ini_Reciv(); //AnswError;  // �訡��� ����p �㭪樨
	  if (Dostup1 == 3) sel++; ex3=0;
	  }

	} // if (Dostup1 > 2)
      } // if (Dostup1 > 1)
   } //if (Dostup1 > 0)
L1:
   if (sel == 0)
   {
       InQueue(0,CheckBufTransIniSend_1);
       TimeReq1=0;// ��� ⠩��� ���� TimeReq1=0;
   }
   else Ini_Reciv_1(); //AnswError;  // �訡��� ����p �㭪樨
}
//---------------------------------------------------------------------------
void CheckBufTransIniSend_1()
{ CheckBufTrans_1();
  Ini_Send_1();
}
//---------------------------------------------------------------------------
/*
void AnswError_1()
{
InQueue(0,Ini_Reciv_1); //�訡��� ����p �㭪樨, �� �⢥���
//  Ini_Reciv_1(); //�訡��� ����p �㭪樨, �� �⢥���
}
*/
//---------------------------------------------------------------------------
void AnswId_1() //���p�� �����䨪��p�
{
  AnswIdp(BufTrans_1);
}
/*
  struct Id         *pid;
  struct IdRunCom   *prun;
  struct IdRun      *pglrun;
  struct bintime    *ptime;
  char *bf;
  int i;

  ptime = &BINTIME;                    //����筠� ��p���p� �p�����
  pid   = &SEND_1.ID;
  if ((NumCal == INTPAR.RS485Com5Speed)&&(NumCal !=GLCONFIG.NumCal))
  {
  bf = &BufTrans_1[5];
  pglrun = &GLCONFIG.IDRUN[0];
  memset(&BufTrans_1[0],0x0,165);
 BufTrans_1[4] = GLCONFIG.NumRun;

  for(i=0;i<3;i++)
    { movmem(pglrun,bf+i*17,16);            //��� ��⪨
      *(bf+i*17+16) = pglrun->TypeP;    //⨯ ���稪� ��������
      pglrun++;
    }
  bf = &BufTrans_1[56];
  *bf     = ptime->month;    //�����
  *(bf+1) = ptime->date | 0x40;     //���� + ���� ������ * 0x20
  *(bf+2) = ptime->year;     //���
  *(bf+3) = ptime->hours;    //��
  *(bf+4) = ptime->minutes;  //������
  *(bf+5) = ptime->seconds;  //ᥪ㭤�
  bf = &BufTrans_1[63];
  *bf = 37;  //����� ���ᨨ
  *(bf+6) = 12;
  *(bf+7) = 8; //70
  *(bf+8) = 0x20; //�����筮�
  *(bf+9) = 0;
  *(bf+10) = 0; //73
//  bf = &BufTrans_1[74];

  SaveMessagePrefix_1(165,0x81);
//  memset(&BufTrans_1[132],0x0,31);
  BufTrans_1[1] = NumCal;
  }
  else
  {
  prun = &SEND_1.ID.IDRUN[0];
  pglrun = &GLCONFIG.IDRUN[0];
  pid->NumRun = GLCONFIG.NumRun;      //���-�� ��⮪
  for(i=0;i<3;i++)
    { movmem(pglrun,prun,16);            //��� ��⪨
      prun->TypeP    = pglrun->TypeP;    //⨯ ���稪� ��������
      if (i==0)
      { prun->TypeP = prun->TypeP | (GLCONFIG.EDIZM_T.EdIzmdP << 5);
        prun->TypeP = prun->TypeP | (GLCONFIG.EDIZM_T.EdIzP << 2);
      }
      else if (i==1)
       prun->TypeP = prun->TypeP | (GLCONFIG.EDIZM_T.EdIzmE << 2);
      prun->TypeRun  = pglrun->TypeRun;  //ᯮᮡ ����p���� �� ��⪥
//      prun->TypeRun = prun->TypeRun & 0x0F;
      if (GLCONFIG.IDRUN[i].Dens == 1)
         prun->TypeRun = prun->TypeRun | 0x20;
      if (GLCONFIG.IDRUN[i].nGerg91_NX == 1)
         prun->TypeRun = prun->TypeRun | 0x40; // Gerg91 ���� NX19
      if (GLCONFIG.IDRUN[i].nGerg88_AGA8 == 1)
         prun->TypeRun = prun->TypeRun | 0x80; // Gerg88 ���� AGA8


//0 - 3095, 2 - 3095+3051CD, 1 - P+T+dP, 3 - P+T+dP+dP, 4 - ���稪
      prun->NumTurbo = pglrun->NumTurbo; //���-�� �3 �� ������
      prun->Qmin     = pglrun->Qmin;     //��������� p��室
      prun->Qmax     = pglrun->Qmax;     //���ᨬ���� p��室
      prun->Pmax     = pglrun->Pmax;     //��p孨� �p���� ���稪� ��������
      prun->dPmax   = pglrun->dPhmax;    //��p�. �p���� ���稪�� ��p�����
      prun++;
      pglrun++;
    }
//  pid->TypeCount = GLCONFIG.TypeCount | GLCONFIG.EDIZM_T.T_SU; //䫠�� ���. ���䨣�p�樨 - ⥬������ �ਢ������ � ��
  pid->TypeCount = GLCONFIG.TypeCount | (GLCONFIG.EDIZM_T.T_VVP << 2); //䫠�� ���. ���䨣�p�樨 - ⥬������ �ਢ������ ���⭮��

  pid->ContrHour = GLCONFIG.ContrHour; //����p���� ��
  pid->Period    = GLCONFIG.Period;    //���p��� ���������� � ������
  // ��p�� �p��p������� ���ᯥ祭��
  GetVersion(pid);

  pid->Month     = ptime->month;    //�����
  pid->Day       = ptime->date;     //����
  pid->Year      = ptime->year;     //���
  pid->Hours     = ptime->hours;    //��
  pid->Minutes   = ptime->minutes;  //������
  pid->Seconds   = ptime->seconds;  //ᥪ㭤�
  SaveMessagePrefix_1(sizeof(Id)+6,0x81);
  movmem(&SEND_1,&BufTrans_1[4],sizeof(Id));
}
}
*/
//---------------------------------------------------------------------------
void RdStat_1() //�⥭�� ����᪨� ��p����p��
{
 RdStatp(BufInKadr_1,BufTrans_1,NumCal);
}
/*
  struct rdstat   *p;
  struct IdRun    *run; //��p����p� ��⪨
  struct bintime  *ptime;//����筠� ��p���p� �p�����
  unsigned char num;
  int           err = 0;
  unsigned char *bf;

  if (BufInKadr_1[2] != 7 ) err |= 1;//����p��� ����� ���p��
  num = BufInKadr_1[4]; //����p ��⪨
    if(num > GLCONFIG.NumRun) err |= 2;//����p��� ����p� ��⪨
ptime = &BINTIME;
  if(err) SaveMessagePrefix_1(6,0xFF);//�訡�� ��p����p�� ���p��
  else
  if ((NumCal == INTPAR.RS485Com5Speed) &&(NumCal !=GLCONFIG.NumCal))
  {
  run = &GLCONFIG.IDRUN[num-1];
  memset(&BufTrans_1[0],0x0,130);
  BufTrans_1[4] = num;
  bf = &BufTrans_1[5];
  movmem(run,bf,16);            //��� ��⪨
  bf = &BufTrans_1[122];
  *bf     = ptime->month;    //�����
  *(bf+1) = ptime->date;     //���� + ���� ������ * 0x20
  *(bf+2) = ptime->year;     //���
  *(bf+3) = ptime->hours;    //��
  *(bf+4) = ptime->minutes;  //������
  *(bf+5) = ptime->seconds;  //ᥪ㭤�
  bf = &BufTrans_1[21];
  *(float*)bf = run->RoVV; // ROnom;
  *(float*)(bf+4) = run->NCO2;
  *(float*)(bf+8) = run->NN2;
//  Swap4(bf);
//  Swap4(bf+4);
//  Swap4(bf+8);
  bf = &BufTrans_1[41];
  *(float*)bf = run->Pb;
//  Swap4(bf);
  bf = &BufTrans_1[65];
  *(float*)bf = run->HsVV;// Heat;
//  Swap4(bf);
  SaveMessagePrefix_1(130,0x82); //������ �p�䨪�
  BufTrans_1[1] = NumCal;
  }
  else
    {   p   = &SEND_1.RDSTAT;     //娬��᪨� ��⠢
        run = &GLCONFIG.IDRUN[num-1];
      p->TypeCount = GLCONFIG.TypeCount;//��� 0,1: ��50\GERG\NX19 [0\1\2]
                                        //��� 7: ��p-const\��p-p���. [0\1]
      p->TypeRun  = run->TypeRun; //ᯮᮡ ����p���� �� ��⪥
//0 - 3095, 2 - 3095+3051CD, 1 - P+T+dP, 3 - P+T+dP+dP, 4 - ���稪
      p->TypeP    = run->TypeP;   //⨯ ���稪� �������� �� ��⪥
      p->ROnom = run->RoVV;//���⭮��� ���� �� �.�., ��/�^3
      p->Pb    = run->Pb;   //��஬����᪮� ��������, �� p�. ��.
      p->NCO2  = run->NCO2; //����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
      p->NN2   = run->NN2;  //����p��� ���� ���� � ������� ᬥ�, %
//      movmem(&(run->NameRun[0]),&(p->NameRun[0]),16); //��� ��⪨
      movmem((run->NameRun),(p->NameRun),16); //��� ��⪨
      p->D20    = run->D20;   //����p����� ������p �p��, ��
      p->d20    = run->d20;   //����p����� ������p ����p����, ��
      if ((run->TypeRun == 4) || (run->TypeRun == 5) || (run->TypeRun == 10))
       p->dPots  = run->dPots;   // � �3/�
      else
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       p->dPots  = run->dPots;
      else
       p->dPots  = run->dPots*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]; //�p����� ���窨, ���/�2
      p->Bd     = run->Bd ;   // ����-�� ���-�� ���-� ���ਠ�� ����ࠣ��, ���^-1(��� A)
      p->kBd    = run->kBd;   //����-�� B
      p->kCd    = run->kCd;   //����-�� C
      p->Bt     = run->Bt ;   // ����-�� ���-�� ���-� ���ਠ�� �p��, ���^-1(��� A)
      p->kBt    = run->kBt;   //����-�� B
      p->kCt    = run->kCt;   //����-�� C
      p->R      = run->R  ;   // ���. ��客����� ��㡮�஢���, ��
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       p->ValueSwitch =  run->ValueSwitch;
      else
       p->ValueSwitch =  run->ValueSwitch*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];//��p�� ��p����祭�� �� ��p�����
      p->typeOtb= run->typeOtb; // 0 - 㣫����, 1 - 䫠�楢�
        ptime = &BINTIME;
      p->Month  = ptime->month;   //�����
      p->Day    = ptime->date;    //����
      p->Year   = ptime->year;    //���
      p->Hours  = ptime->hours;   //��
      p->Minutes= ptime->minutes; //�����
      p->Seconds= ptime->seconds; //ᥪ㭤�

      SaveMessagePrefix_1(sizeof(rdstat)+7,0x82); //������ �p�䨪�
      BufTrans_1[4] = num;                        //����p ��⪨
      movmem(&SEND_1,&BufTrans_1[5],sizeof(rdstat));//������ ���p�
    }
}
*/
//---------------------------------------------------------------------------
void WrStat_1() //������ ����᪨� ��p����p��
{
 WrStatp(BufInKadr_1,BufTrans_1,NumCal,1);
}
/*
  struct wrstat   *p;
  struct IdRun    *run; //��p����p� ��⪨
  unsigned char num;
  int           err = 0, nMeth;
  unsigned char PasFU[15];
  unsigned char *pc;
  float RoFU,N2FU,CO2FU,TeplFU,PbFU;
  float dPotsw,UHeat,Ep;

  num = BufInKadr_1[4]; //����p ��⪨
  if ((NumCal == INTPAR.RS485Com5Speed)&&(NumCal !=GLCONFIG.NumCal))
  {
    if (BufInKadr_1[2] != 140 ) err |= 1;//����p��� ����� ���p��
    if (num > GLCONFIG.NumRun)  err |= 2;//����p��� ����p� ��⪨
   Mem_LoginRd();
   memcpy(PasFU,LogPassw[1].login,4);
   memcpy(&PasFU[4],LogPassw[1].Password,10);
//PasFU[14] = 0x0;
//printf("PasFU=%s\n",PasFU);
//    if (memcmp(&BufInKadr_1[5],PasFU,14))  err |= 4;
    pc = &BufInKadr_1[37];
    RoFU = *(float *)pc;
    if (RoFU < 0.66)         // ������ �।�� ���������
             err |= 8;
    if (RoFU > 1.13)           // ��� ��� ��⮤��
      err |= 16;
    if(err)
    { SaveMessagePrefix_1(6,0xFF);  //����᪨� ��p����p� �� ����ᠭ�
      BufTrans_1[1] = NumCal;
    }
    else  //�p��� �믮������ - 8 ��
    {
     pc = &BufInKadr_1[41];
     CO2FU = *(float *)pc;
     pc = &BufInKadr_1[45];
     N2FU = *(float *)pc;
     pc = &BufInKadr_1[57];
     PbFU = *(float *)pc;
     pc = &BufInKadr_1[81];
     TeplFU = *(float *)pc;
//printf("RoFU=%f CO2FU=%f N2FU=%f TeplFU=%f\n");
     run = &GLCONFIG.IDRUN[num-1];
      if(run->RoVV  != RoFU)
        { MEMAUDIT_1.type     = 1; //���⭮��� ���� �� �.�., ��/�^3
          MEMAUDIT_1.altvalue = run->RoVV;
          run->ROnom      = RoFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];
          run->RoVV      = RoFU;
          MEMAUDIT_1.newvalue = run->RoVV;
          CalcZc[num-1] = 1;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      UHeat =  TeplFU;    // � ���*�
//      TeplFU = Round_toKg(UHeat,KoefEn[GLCONFIG.EDIZM_T.EdIzmE],&Ep); // �� MEGAS� � ���
//printf("UHeat=%f Heat=%f HsVV=%f dh=%f Ep=%f\n",UHeat,p->UHeat,run->HsVV, run->Heat  - p->UHeat,Ep);
      Ep = 1e-3;
     if (UHeat > 5.0)
     { if (fabs(run->HsVV  - UHeat)>Ep)
      { MEMAUDIT_1.type     = 37;
        MEMAUDIT_1.altvalue = run->HsVV; //*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
        run->Heat  = TeplFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
        MEMAUDIT_1.newvalue = UHeat;
        run->HsVV = UHeat;
        CalcZc[num-1]=run->nGerg88_AGA8;
                                    // ������ ���� ����⥫���
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      }
      else
       { run->HsVV = UHeat;
         run->Heat  = TeplFU*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
       }
     }
      if(run->NCO2  != CO2FU)
        { MEMAUDIT_1.type     = 2;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          MEMAUDIT_1.altvalue = run->NCO2;
          run->NCO2   = CO2FU;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          CalcZc[num-1]=1;
          MEMAUDIT_1.newvalue = run->NCO2;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->NN2  != N2FU)
        { MEMAUDIT_1.type     = 3;
          MEMAUDIT_1.altvalue = run->NN2;
          run->NN2    = N2FU; //����p��� ���� ���� � ������� ᬥ�, %
          CalcZc[num-1]=1;
          MEMAUDIT_1.newvalue = run->NN2;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->Pb  != PbFU)
        { MEMAUDIT_1.type     = 6;  //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT_1.altvalue = run->Pb;
          run->Pb     = PbFU;    //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT_1.newvalue = run->Pb;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���

      if( Mem_ConfigWr() == 0 )         // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
        SaveMessagePrefix_1(6,0x83);    // ����᪨� ��p����p� ����ᠭ�
      else
        SaveMessagePrefix_1(6,0xFF);    // ����᪨� ��p����p� �� ����ᠭ�
      BufTrans_1[1] = NumCal;
    }
   }
   else
   {
    if( BufInKadr_1[2]-23 != sizeof(wrstat) ) err |= 1;//����p��� ����� ���p��
// 23 = 4+1+16+2 - �p����㫠, ����p ��⪨, ��p���, ����p. �㬬�
    if(num > GLCONFIG.NumRun)             err |= 2;//����p��� ����p� ��⪨
//    if(memcmp(&BufInKadr_1[5],GLCONFIG.PASSWORD,16)) //�p���p�� ��p���
    if (memcmp(&BufInKadr_1[5],PASSWR1,14))
                                          err |= 4;
    p   = &RESIV_1.WRSTAT;    //����᪨� ��p����p�
    movmem(&BufInKadr_1[4+17],&RESIV_1,sizeof(wrstat));//��p������� ��p����p��
    if (p->ROnom < 0.66)  err |= 8;        // ������ �।�� ���������
    if (p->ROnom > 1.13) err |= 16;          // ��� ��� ��⮤��

    if(err)
     SaveMessagePrefix_1(6,0xFF);  //����᪨� ��p����p� �� ����ᠭ�
    else  //�p��� �믮������ - 8 ��
    {
//TestPeriodRes();
      run = &GLCONFIG.IDRUN[num-1];
      if(memcmp((p->NameRun),(run->NameRun),16))
        { MEMAUDIT_1.type     = 0;
          movmem((run->NameRun),&MEMAUDIT_1.altvalue,4);
          movmem((p->NameRun),(run->NameRun),16);   //��� ��⪨
          movmem((run->NameRun),&MEMAUDIT_1.newvalue,4);
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->RoVV  != p->ROnom)
        { MEMAUDIT_1.type     = 1; //���⭮��� ���� �� �.�., ��/�^3
          MEMAUDIT_1.altvalue = run->RoVV;
          run->ROnom = p->ROnom*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];
          run->RoVV = p->ROnom;
          MEMAUDIT_1.newvalue = run->RoVV;
          CalcZc[num-1] = 1;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->Pb  != p->Pb)
        { MEMAUDIT_1.type     = 6;  //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT_1.altvalue = run->Pb;
          run->Pb     = p->Pb;    //��஬����᪮� ��������, �� p�. ��.
          MEMAUDIT_1.newvalue = run->Pb;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->NCO2  != p->NCO2)
        { MEMAUDIT_1.type     = 2;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          MEMAUDIT_1.altvalue = run->NCO2;
          run->NCO2   = p->NCO2;//����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
          MEMAUDIT_1.newvalue = run->NCO2;
         CalcZc[num-1] = 1;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->NN2  != p->NN2)
        { MEMAUDIT_1.type     = 3;
          MEMAUDIT_1.altvalue = run->NN2;
          run->NN2    = p->NN2; //����p��� ���� ���� � ������� ᬥ�, %
          MEMAUDIT_1.newvalue = run->NN2;
         CalcZc[num-1] = 1;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->D20  != p->D20)
        { MEMAUDIT_1.type     = 4;
          MEMAUDIT_1.altvalue = run->D20;
          run->D20    = p->D20;   //����p����� ������p �p��, ��
          MEMAUDIT_1.newvalue = run->D20;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      if(run->d20  != p->d20)
        { MEMAUDIT_1.type     = 5;
          MEMAUDIT_1.altvalue = run->d20;
          run->d20    = p->d20;   //����p����� ������p ����p����, ��
          MEMAUDIT_1.newvalue = run->d20;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
      dPotsw =  p->dPots;    // � ���
      if ((run->TypeRun == 4) || (run->TypeRun == 5) || (run->TypeRun == 10))
       p->dPots = dPotsw;
      else
       if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
        p->dPots  = dPotsw;
       else
        p->dPots = dPotsw/KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]; // � ���/�2
      if(run->dPots != p->dPots)
        { MEMAUDIT_1.type     = 7;
          if ((run->TypeRun == 4) || (run->TypeRun == 5) || (run->TypeRun == 10))
            MEMAUDIT_1.altvalue = run->dPots;
          else
          { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
             MEMAUDIT_1.altvalue = run->dPots;
            else
             MEMAUDIT_1.altvalue = run->dPots*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
          }
          run->dPots  = p->dPots; //�p����� ���窨, ���/�2
          MEMAUDIT_1.newvalue = dPotsw;
          Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        }
//    if(GLCONFIG.TypeCount & 0x80) //��� 7: ��p-const\��p-p���. [0\1]
     {
      if(run->Bd != p->Bd)
      { MEMAUDIT_1.type     = 20;
        MEMAUDIT_1.altvalue = run->Bd;
        run->Bd     = p->Bd;    //����. A ����p����
        MEMAUDIT_1.newvalue = run->Bd;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kBd != p->kBd)
      { MEMAUDIT_1.type     = 21;
        MEMAUDIT_1.altvalue = run->kBd;
        run->kBd    = p->kBd;   //����-�� B ����p����
        MEMAUDIT_1.newvalue = run->kBd;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kCd != p->kCd)
      { MEMAUDIT_1.type     = 23;
        MEMAUDIT_1.altvalue = run->kCd;
        run->kCd    = p->kCd;   //����-�� C ����p����
        MEMAUDIT_1.newvalue = run->kCd;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->Bt != p->Bt)
      { MEMAUDIT_1.type     = 33;
        MEMAUDIT_1.altvalue = run->Bt;
        run->Bt     = p->Bt;    //����-�� � �p��
        MEMAUDIT_1.newvalue = run->Bt;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kBt != p->kBt)
      { MEMAUDIT_1.type     = 34;
        MEMAUDIT_1.altvalue = run->kBt;
        run->kBt    = p->kBt;   //����-�� B �p��
        MEMAUDIT_1.newvalue = run->kBt;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->kCt != p->kCt)
      { MEMAUDIT_1.type     = 35;
        MEMAUDIT_1.altvalue = run->kCt;
        run->kCt    = p->kCt;   //����-�� C �p��
        MEMAUDIT_1.newvalue = run->kCt;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
    }
      if(run->R != p->R)
      { MEMAUDIT_1.type     = 19;
        MEMAUDIT_1.altvalue = run->R;
        run->R      = p->R;     // ���. ��客����� ��㡮�஢���,R� ��
        MEMAUDIT_1.newvalue = run->R;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      dPotsw =  p->ValueSwitch;    // � ���
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       p->ValueSwitch = dPotsw;
      else
       p->ValueSwitch = dPotsw/KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
      if(run->ValueSwitch != p->ValueSwitch)
      { MEMAUDIT_1.type     = 11;
        if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
         MEMAUDIT_1.altvalue = run->ValueSwitch;
        else
         MEMAUDIT_1.altvalue = run->ValueSwitch*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];;
        run->ValueSwitch  = p->ValueSwitch; //��p�� ��p����祭�� �� ��p�����
        MEMAUDIT_1.newvalue = dPotsw;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
      if(run->typeOtb != p->typeOtb)//0 - 㣫����, 1 - 䫠�楢�
      { MEMAUDIT_1.type     = 12;//⨯ �⡮p� ��������
        *(unsigned long*)&MEMAUDIT_1.altvalue = (unsigned long)run->typeOtb;
        run->typeOtb      = p->typeOtb;
        *(unsigned long*)&MEMAUDIT_1.newvalue = (unsigned long)run->typeOtb;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
        Calc_SetNoErrTypeOtb();
      }

      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���

      if( Mem_ConfigWr() == 0 )         // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
        SaveMessagePrefix_1(6,0x83);    // ����᪨� ��p����p� ����ᠭ�
      else
        SaveMessagePrefix_1(6,0xFF);    // ����᪨� ��p����p� �� ����ᠭ�
//TestPeriodRead();
    }
}
}
*/
//---------------------------------------------------------------------------
void RdCalcparam_1() {                   // �⥭�� ������ ���������� ��p����p��
 RdCalcparamp(BufInKadr_1,BufTrans_1);
}
/*
  struct calcparam  *pcp;
   struct calcdata   *pcc;
   int num;
   int err =  0;
   union vm {
      unsigned char Bt[8];
      double Val;
   } Vm;
   struct bintime    *ptime;            // ����筠� ��p���p� �p�����
   float KofE;

   if (BufInKadr_1[2] != 7)
      err |= 1;                         // ����p��� ����� ���p��
//ComPrint_2("Cl");
   num = BufInKadr_1[4];
   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // �訡�� ��p����p�� ���p��
   else {
      SetCopyCalcData_1(num-1);
      pcc = &COPY_CALCDATA_1[num-1];    // ����� ��p����p�� p���� ��⪨
      pcp = &SEND_1.CALCPARAM;          // ����� ��������� ��p����p�
      ptime = &BINTIME;
      pcp->TypeCount = pcc->TypeCount;  // ��� 0,1: ��50\GERG\NX19 [0\1\2]; ��� 7: ��p-const\��p-p���. [0\1]
                                        // ᯮᮡ ����p���� �� ��⪥
      pcp->TypeRun = GLCONFIG.IDRUN[num-1].TypeRun;
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       pcp->Pin = pcc->Pin;
      else
       pcp->Pin    = pcc->Pin*KoefP[GLCONFIG.EDIZM_T.EdIzP];           // ���. ��� �����. �������� �� �室� �p��p����, ���/�2
      pcp->t      = pcc->t;

      ConstCorr(P, pcp->Pin, GLCONFIG.IDRUN[num-1].constdata);
      ConstCorr(T, pcp->t, GLCONFIG.IDRUN[num-1].constdata);

      if ((pcp->TypeRun) == 4 || (pcp->TypeRun) == 5    // ���稪
                   || (pcp->TypeRun) == 10)
        { pcp->sqrtQ  = pcc->kQind;      // �-� �ਢ������
          pcp->dP  = GetQwRaw(num-1); //GetQw
          pcp->Q = GetQ(num-1);
        }
      else
        { pcp->sqrtQ  = pcc->sqrtQ;      // ��p��� ����p���
          if (GLCONFIG.IDRUN[num-1].Vlag == 1) pcp->dP = IndDP[num-1];
          else
          { if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
             pcp->dP  = IndDP[num-1];
            else
              pcp->dP  = IndDP[num-1]*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
          }
          pcp->Q = IndQ[num-1];
       }
      if (GLCONFIG.IDRUN[num-1].Vlag == 0)
      ConstCorr(DP, pcp->dP, GLCONFIG.IDRUN[num-1].constdata);
      pcp->Vs = Mem_GetDayQ(num-1);     // ��ꥬ �� ��⪨
                                        // ��ꥬ �� �p��� ��⪨ �� ��᫥���� ����� ���筮�� ����
      pcp->Va = Mem_DayRdEnd_1(GetPage(num-1,2)) ? MEMDATA_1.q : 0;
      if (GLCONFIG.EDIZM_T.EdIzmE == 0)
       KofE=KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1000.0;
      else
      { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
         KofE= 1.0;
        else
         KofE=KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
      }
      pcp->PrevDayFHeat = Mem_DayRdEnd_1(GetPage(num-1,2)) ? MEMDATA_1.FullHeat*KofE/1000.0 : 0;
//printf("FH=%f\n",pcp->PrevDayFHeat);
                                        // ��騩 ��ꥬ, ���. �3
      pcp->Vtot   = Persist_GetQTotal(num - 1) / 1000;

                                        // ���ࠢ��� �����⥫�� �� ���㯫���� ��. �஬�� �-�� ��� ��९���
                                        // ��� ��騩 ��ꥬ �� �� ��� ���稪�
      pcp->Kp = ((pcp->TypeRun == 4) || (pcp->TypeRun == 5)
      ||  (pcp->TypeRun == 10)) ? Persist_GetQwCountTotal(num-1) : pcc->Kp;

//    pcp->Ksh    = pcc->Ksh;           // ���ࠢ-� ����-�� �� ��-��� ��-��
      pcp->Ksh    = IndKsh[num-1];      // ���ࠢ-� ����-�� �� ��-��� ��-��

         pcp->Alpha_Eps  = pcc->Eps;    // ����-�� ���७��

      pcp->ROru   = pcc->ROmk1;         // ���⭮��� � ࠡ��� �᫮����
      pcp->E      = pcc->E;             // �-� ᪮��� �室�
      pcp->K      = pcc->K;             // �����樥�� ᦨ�������
      pcp->Dd     = pcc->Dd;            // ������� ����ࠣ�� �� �.�., ��
      pcp->Dt     = pcc->Dt;            // ������� ��㡮�஢��� �� �.�., ��

         pcp->mod_beta = pcc->Beta;     // �⭮�⥫쭮� �⢥��⨥

      pcp->RO     = pcc->ROnom;         // ���⭮��� �� �
      pcp->N2     = pcc->NN2;           // ����
      pcp->CO2    = pcc->NCO2;          // 㣫���᫮�

      ReadMonthSumm(num-1);             // ��ꥬ �� ���� �����
      pcp->Vpm           = (float)MonthSumm[num-1].PrevMonthV;
                                        // ⥯��� �� ���� �����
      pcp->PrevMonthFHeat = (float)MonthSumm[num-1].PrevMonthFHeat*KofE/1000.0;
                                        // ��� ���稪� - p��室 �p� p����� �᫮����
                                        // ��� ����p���� - B2 ��� C
      if ((pcp->TypeRun) == 4 || (pcp->TypeRun) == 5
        || (pcp->TypeRun) == 10) { // ���稪
         pcp->B2_C = IndDP[num-1];
         ConstCorr(QW, pcp->B2_C, GLCONFIG.IDRUN[num-1].constdata);
      }
      else {                            // ����ࠣ��
            pcp->B2_C = IndC_[num-1];
      }
      pcp->Kapa   = pcc->capa;          // �������
      pcp->Re     = IndRe[num-1];       // ����⢨�-� �᫮ ��������
      pcp->Mu     = pcc->Mu;            // �������᪠� �離����, ���*c/�2

      pcp->Vm  = GetVm(num-1);          // ����� Q1_C1 - ��ꥬ � ��砫� �����
        if (GLCONFIG.EDIZM_T.EdIzmE == 1)
          pcp->Heat    = pcc->Heat;
        else
         pcp->Heat    = pcc->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];    // 㤥�쭠� ⥯��� ᣮp���� ���/�3
         pcp->B0_Z   = pcc->Z;          // 䠪�p ᦨ������� �p� p����� �᫮����
         pcp->F_Zc   = pcc->Zc;         // 䠪�p ᦨ������� �p� �⠭��p��� �᫮����
      ConstCorr(RO, pcp->RO, GLCONFIG.IDRUN[num-1].constdata);
      ConstCorr(N2, pcp->N2, GLCONFIG.IDRUN[num-1].constdata);
      ConstCorr(CO2, pcp->CO2, GLCONFIG.IDRUN[num-1].constdata);
      ConstCorr(Heat, pcp->Heat, GLCONFIG.IDRUN[num-1].constdata);
      pcp->Pa      = pcc->Pa;           // ���. ��������, ���/�2
      pcp->Month = ptime->month; // �����
      pcp->Day   = ptime->date;  // ����

      if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5)
//      (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
         pcp->Day |= 0x80;              // ᯮᮡ ����p���� �� ��⪥

      pcp->Year  = ptime->year;  // ���
      pcp->Hours = ptime->hours; // ��
                                        // �����
      pcp->Minutes = ptime->minutes;
                                        // ᥪ㭤�
      pcp->Seconds = ptime->seconds;

      movmem(pcp,&BufTrans_1[5],sizeof(calcparam));
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(calcparam)+7,0x84);
      BufTrans_1[4] = num;              // ����p ��⪨
   }
}
*/
//---------------------------------------------------------------------------
void RdInstparam_1() {                  // ��������� ��p����p�
 RdInstparamp(BufInKadr_1,BufTrans_1);                     // ��������� ��p����p�
}
/*   struct calcdata   *pcalc;
   struct bintime    *ptime;            // ����筠� ��p���p� �p�����
   struct instparam  *pip;              // ��������� ��p����p�
   struct IdRun      *run;              // ��p����p� ��⪨
   unsigned char num;
   int err =  0;
// 
   if (BufInKadr_1[2] != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = 3 & BufInKadr_1[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // �訡�� ��p����p�� ���p��
   else {
      SetCopyCalcData_1(num-1);
      pip   = &SEND_1.INSTPARAM;        // ��������� ��p����p�
      ptime = &BINTIME;
//    pcalc = &CALCDATA[num-1];
      pcalc = &COPY_CALCDATA_1[num-1];
      run   = &GLCONFIG.IDRUN[num-1];
                                        // p��室 �p� p.�., �3/�� ��� ��p����
      float Qru = ((errMODBUS[num-1] > (CErrMB/SysTakt))&&(run->constdata & 0x08))
       ? 0.0 : GetQw3(num-1);
      pip->dP = ((run->TypeRun) == 4 || (run->TypeRun) == 5
       || (run->TypeRun) == 10) ? Qru : IndDP[num-1]*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];;
      pip->P    = pcalc->Pa*KoefP[GLCONFIG.EDIZM_T.EdIzP];            // ��������;
      pip->t    = pcalc->t;             // ⥬��p���p�;

      ConstCorr(((run->TypeRun) == 4 || (run->TypeRun) == 5
      || (run->TypeRun) == 10) ? QW : DP, pip->dP, run->constdata);
      if (run->Vlag == 1) pip->dP = IndDP[num-1];
      ConstCorr(P, pip->P, run->constdata);
      ConstCorr(T, pip->t, run->constdata);

    pip->Q = ((run->TypeRun) == 4 || (run->TypeRun) == 5) ? GetQ(num-1) : pcalc->Q;  //GetQRaw(num-1)
      if ((run->TypeRun) == 10) pip->Q = GetQ(num-1);

      pip->Vs = Mem_GetDayQ(num-1);//p��室 �� ��⪨

                                        // ��ꥬ �� �p��� ��⪨ �� ��᫥���� ����� ���筮�� ����
      pip->Va = Mem_DayRdEnd_1(GetPage(num-1,2)) ? MEMDATA_1.q : 0;

                                        // ��騩 ��ꥬ, ���. �3
      pip->Vtot   = Persist_GetQTotal(num - 1) / 1000;

      pip->Month   = ptime->month;      // �����
      pip->Day     = ptime->date;       // ����

      if( (GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
      || (run->TypeRun) == 10)
        pip->Day |= 0x80;               // ᯮᮡ ����p���� �� ��⪥ - ���稪

      pip->Year    = ptime->year;       // ���
      pip->Hours   = ptime->hours;      // ��
      pip->Minutes = ptime->minutes;    // �����
      pip->Seconds = ptime->seconds;    // ᥪ㭤�
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(instparam)+7,0x85);
      BufTrans_1[4] = num;              // ����p ��⪨
                                        // ������ ���p�
      movmem(&SEND_1,&BufTrans_1[5],sizeof(instparam));
   }
}
*/
//---------------------------------------------------------------------------
void AnswChim_1() {                     // �⥭�� 娬��᪮�� ��⠢�
 AnswChimp(BufInKadr_1,BufTrans_1,NumCal);                     // ��������� ��p����p�
}
/*
   extern struct bintime BINTIME;       // ����筠� ��p���p� �p�����
   struct calcdata   *pcalc;
   struct rdchim *p;
   struct IdRun *run;                   // ��p����p� ��⪨
   struct bintime *ptime;               // ����筠� ��p���p� �p�����
   unsigned char num;
   int i,i1,l1,err = 0;
   struct instparam  *pip;              // ��������� ��p����p�
 
  if (BufInKadr_1[2] != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // �訡�� ��p����p�� ���p��
   else 
   {
//if ((INTPAR.HartReqDelay & 0x01) == 1) // ᮢ���⨬���� � ������� �� �.6
  if ((NumCal == INTPAR.RS485Com5Speed)&&(NumCal !=GLCONFIG.NumCal))
     {
      SetCopyCalcData_1(num-1);
      pip   = &SEND_1.INSTPARAM;        // ��������� ��p����p�
      pcalc = &COPY_CALCDATA_1[num-1];
      run   = &GLCONFIG.IDRUN[num-1];
      pip->Q = ((run->TypeRun) == 4 || (run->TypeRun) == 5) ? GetQRaw(num-1) : pcalc->Q;
      if ((run->TypeRun) == 10) pip->Q = GetQ(num-1);

      pip->Vs = Mem_GetDayQ(num-1);//p��室 �� ��⪨
                                        // ��ꥬ �� �p��� ��⪨ �� ��᫥���� ����� ���筮�� ����
      pip->Va = Mem_DayRdEnd_1(GetPage(num-1,2)) ? MEMDATA_1.q : 0;
                                        // ��騩 ��ꥬ, ���. �3
      pip->Vtot   = Persist_GetQTotal(num - 1) / 1000;
                                     // ������ �p�䨪�
//      BufTrans_1[4] = num;              // ����p ��⪨
      if ((INTPAR.CntBufLen) != 0) // ����� �⢥� 27 ��� 23
      { i1 = 9; l1 = 27;
      memset(&BufTrans_1[5],0x0,4);
      }
      else {i1 = 5; l1 = 23;}
      movmem(&SEND_1.INSTPARAM.Q,&BufTrans_1[i1],sizeof(instparam));
      if ((INTPAR.HartReqDelay & 0x60) != 0) // swap4-ᮢ���⨬���� � ������� �� �.4
      {
       for (i=i1; i<(i1+16); i+=4)
       Swap4(&BufTrans_1[i]);  //��ॢ����� �����
      }
        SaveMessagePrefix_1(l1,0x86);
        BufTrans_1[1] = NumCal;
     }// �����  if ((INTPAR.HartReqDelay & 0x01) == 1) // ᮢ���⨬���� � ������� �� �.4
     else
     {
      p = &SEND_1.RDCHIM;               // 娬��᪨� ��⠢
      run = &GLCONFIG.IDRUN[num-1];
      ptime = &BINTIME;

      p->TypeP = run->TypeP;            // ⨯ ���稪� ��������
      p->ROnom = run->RoVV;            // ���⭮��� ���� �� �.�., ��/�^3
      p->Pb    = run->Pb;               // !��஬����᪮� ��������, �� p�. ��.
      p->NCO2  = run->NCO2;             // ����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
      p->NN2   = run->NN2;              // ����p��� ���� ���� � ������� ᬥ�, %
     p->UHeat  = run->HsVV;     // 㤥�쭠� ⥯��� ᣮ࠭�� � ���
//      p->UHeat  = run->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];    // 㤥�쭠� ⥯��� ᣮ࠭�� � ���

      p->Month   = ptime->month;        // �����
      p->Day     = ptime->date;         // ����
      p->Year    = ptime->year;         // ���
      p->Hours   = ptime->hours;        // ��
      p->Minutes = ptime->minutes;      // �����
      p->Seconds = ptime->seconds;      // ᥪ㭤�
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(sizeof(rdchim)+7,0x86);
     movmem(&SEND_1,&BufTrans_1[5],sizeof(rdchim)); // ������ ���p�
     }
      BufTrans_1[4] = num;              // ����p ��⪨
   }
}
*/
//---------------------------------------------------------------------------
void WrChim_1() {                       // ������ 娬��᪮�� ��⠢�
 WrChimp(BufInKadr_1,BufTrans_1,1);                     // ��������� ��p����p�
}
/*
   struct wrchim *p;
   struct IdRun  *run;                  // ��p����p� ��⪨
   unsigned char num;
   int err = 0;
   char buf[30];
   float UHeat, Ep;
//   if (Nwr1 == 0)
//      err |= 0x80;                      // 䫠� ����� �������

   if (BufInKadr_1[2]-23 != sizeof(wrchim))
      err |= 1;                         // ����p��� ����� ���p��

   p   = &RESIV_1.WRCHIM;               // 娬��᪨� ��⠢

   num = BufInKadr_1[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨
                                        // �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[5],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[5],PASSWR1,14))
      err |= 4;
                                       // ��p������� ��p����p��
   movmem(&BufInKadr_1[4+17],&RESIV_1,sizeof(wrchim));
   if ((p->ROnom > 1.13) || (p->ROnom < 0.55))         // ��� ��� ��⮤��
      err |= 16;
//sprintf(buf,"err=%X ROnom=%f\n\r\0",err,p->ROnom);
//ComPrint_2m(buf);
   if (err)
      SaveMessagePrefix_1(6,0xFF);      // 娬��᪨� ��⠢ �� ����ᠭ
   else {
      run = &GLCONFIG.IDRUN[num-1];

      if (run->RoVV  != p->ROnom) {
         MEMAUDIT_1.type     = 1;
         MEMAUDIT_1.altvalue = run->RoVV;
         run->ROnom        = p->ROnom*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];  // ���⭮��� ���� �� �.�., ��/�^3
         MEMAUDIT_1.newvalue = p->ROnom;
         run->RoVV = p->ROnom;
         CalcZc[num-1]=1;
                                        // ������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      }
      else
        run->ROnom = run->RoVV*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];  // ���⭮��� ���� �� �.�., ��/�^3

      if (run->Pb  != p->Pb) {
         MEMAUDIT_1.type     = 6;
         MEMAUDIT_1.altvalue = run->Pb;
         run->Pb             = p->Pb;   // ��஬����᪮� ��������, �� p�. ��.
         MEMAUDIT_1.newvalue = run->Pb;
                                        // ������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      }

      if (run->NCO2  != p->NCO2) {
         MEMAUDIT_1.type     = 2;
         MEMAUDIT_1.altvalue = run->NCO2;
         run->NCO2           = p->NCO2; // ����p��� ���� ����ᨤ� 㣫�p��� � ������� ᬥ�, %
         MEMAUDIT_1.newvalue = run->NCO2;
         CalcZc[num-1]=1;
                                        // ������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      }

      if (run->NN2  != p->NN2) {
         MEMAUDIT_1.type     = 3;
         MEMAUDIT_1.altvalue = run->NN2;
         run->NN2            = p->NN2;  // ����p��� ���� ���� � ������� ᬥ�, %
         MEMAUDIT_1.newvalue = run->NN2;
         CalcZc[num-1]=1;
                                        // ������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));
      }
      UHeat =  p->UHeat;    // � ���*�
      double  Koef = (GLCONFIG.EDIZM_T.EdIzmE == 1) ? 1.0 : KoefEn[GLCONFIG.EDIZM_T.EdIzmE] ;
      p->UHeat = Round_toKg(UHeat,Koef,&Ep);
//printf("UHeat=%f Heat=%f HsVV=%f dh=%f Ep=%f\n",UHeat,p->UHeat,run->HsVV, run->Heat  - p->UHeat,Ep);
     if (UHeat > 5)
     { if (fabs(run->HsVV  - UHeat)>Ep)
      { MEMAUDIT_1.type     = 37;
        MEMAUDIT_1.altvalue = run->HsVV;
        run->Heat  = p->UHeat*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
        MEMAUDIT_1.newvalue = UHeat;
        run->HsVV = UHeat;
        CalcZc[num-1]=run->nGerg88_AGA8;
        Mem_AuditWr_1(Mem_GetAuditPage(num-1));  // ������ ���� ����⥫���
      }
      else
       { run->HsVV = UHeat;
         run->Heat  = p->UHeat*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];   // 㤥�쭠� ⥯���  � ���
       }
      }
      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���

      if (Mem_ConfigWr() == 0)          // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
         SaveMessagePrefix_1(6,0x87);   // 娬��᪨� ��⠢ ����ᠭ
      else
         SaveMessagePrefix_1(6,0xFF);   // 娬��᪨� ��⠢ �� ����ᠭ
   }
}
*/
//---------------------------------------------------------------------------
void RdConst_1()  //�⥭�� ����⠭�
{
 RdConstp(BufInKadr_1,BufTrans_1);                     // ��������� ��p����p�
}
/*
  struct rdflconst  *p;
  struct IdRun      *prun;   //��p����p� ��⪨
  unsigned char     num;
  int           err = 0;
  if( BufInKadr_1[2] != 7 ) err |= 1;   //����p��� ����� ���p��
  num = 3 & BufInKadr_1[4];
  if(num > GLCONFIG.NumRun) err |= 2; //����p��� ����p� ��⪨
  if(err) SaveMessagePrefix_1(6,0xFF);  //�訡�� ��p����p�� ���p��
  else
  { p    = &SEND_1.RDFLCONST;        //����⠭��
    prun = &GLCONFIG.IDRUN[num-1]; //㪠��⥫� �� ��p����p� ��⪨

//      p->TypeRun = prun->TypeRun | (prun->Chromat<<6) | (prun->Dens<<5);
     p->TypeRun = prun->TypeRun | (prun->Dens<<7);
    p->flag    = prun->constdata;  //���筨� ����⠭�\���稪 0\1
      if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
       p->constdP  = prun->constdP;
      else
       p->constdP  = prun->constdP*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP]; //�p����� ���窨, ���/�2
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       p->constP  = prun->constP;
      else
       p->constP  = prun->constP*KoefP[GLCONFIG.EDIZM_T.EdIzP];
//    p->constP  = prun->constP;     //����⠭� ��������
    p->constt  = prun->constt;     //����⠭� ⥬��p���p�
    p->constqw = prun->constqw;    //����⠭� p��室� �p� p.�., �3/��

      p->constRo   = prun->RoVV;       // ���⭮���
      p->constHeat  = prun->HsVV;
      p->constCO2  = prun->NCO2;        // 㣫���᫮�
      p->constN2   = prun->NN2;         // ����
      SaveMessagePrefix_1(sizeof(rdflconst)+7,0x88);  //������ �p�䨪�
      BufTrans_1[4] = num;                          //����p ��⪨
      movmem(&SEND_1,&BufTrans_1[5],sizeof(rdflconst));  //������ ���p�
  }
}
*/
/*//-----------------------------------------------------------
void SetVauditCount_1(unsigned char Old, unsigned char New,
                    unsigned char One,unsigned char nRun) {

   if ( (Old & One) != 0  && (New & One) == 0) {
      AllAlarmCount[nRun]++;            // �த������ ���稪�
//    IzmAlarmCount[nRun]++;
   }
   else                                 // ��⨥ � ����⠭��
   if ( (Old & One) == 0  && (New & One) != 0) {
      AllAlarmCount[nRun]--;
//    IzmAlarmCount[nRun]--;            // 㬥����� ���稪�
   }
}
*/
//---------------------------------------------------------------------------
void WrConst_1() {                        // ������ ����⠭�
 WrConstp(BufInKadr_1,BufTrans_1,1);                     // ��������� ��p����p�
}
/*   struct wrflconst  *p;
   struct IdRun      *prun;             // ��p����p� ��⪨
   unsigned char     num;
   int err = 0;
   int i;
   float Cp,Ep;
   unsigned char OldFlags,NewFlags,
                 OldFlags1,NewFlags1,OneFlag;

//   if (Nwr1 == 0) err |= 0x80;           // 䫠� ����� �������

   if (BufInKadr_1[2]-23 != sizeof(wrflconst))
      err |= 1;                          // ����p��� ����� ���p��

   num = 3 & BufInKadr_1[4];

   if (num > GLCONFIG.NumRun)
      err |= 2;                          // ����p��� ����p� ��⪨
                                        // �p���p�� ��p���
//   if (memcmp(&BufInKadr_1[5],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[5],PASSWR1,14))
      err |= 4;

   p   = &RESIV_1.WRFLCONST;            // ����⠭��
                                        // ��p������� ��p����p��
   movmem(&BufInKadr_1[4+17],&RESIV_1,sizeof(wrflconst));

   if (p->constRo > 1.13) || (p->constRo < 0.66))
      err |= 16;

   if (err)
      SaveMessagePrefix_1(6,0xFF);         // ����⠭�� �� ����ᠭ�
   else {
      prun = &GLCONFIG.IDRUN[num-1];     // 㪠��⥫� �� ��p����p� ��⪨
                                        // �⮡ࠦ��� ����⠭�� �� ����⥫��⢠� �� ���室� �� ����⠭��
      unsigned char fConst = prun->constdata & ~(p->flag);

      OldFlags = prun->constdata;
      NewFlags = p->flag;

//    SetVauditCount(OldFlags,NewFlags,0x01,num-1);
//    SetVauditCount(OldFlags,NewFlags,0x02,num-1);
//    SetVauditCount(OldFlags,NewFlags,0x04,num-1);
//    SetVauditCount(OldFlags,NewFlags,0x08,num-1);

      for (i=0; i<4; i++) {             // 䨪��� ���਩
         OneFlag = 1 << i;              // ����ன�� �� ��।��� ��ࠬ���
         OldFlags1 = OldFlags & OneFlag;
         NewFlags1 = NewFlags & OneFlag;
                                        // �ந��諠 ᬥ�� �ਧ����
                                        // � ��� ����� �� ������ ���਩
         if (OldFlags1 != NewFlags1) { // && (prun->NoAlarmConstFlag == 0)) {
            switch(i) {
               case 0:                  // �������
                                        // �� ���稪
                  if (((GLCONFIG.IDRUN[num-1].TypeRun) != 4) && ((GLCONFIG.IDRUN[num-1].TypeRun) != 5)
                  && ((GLCONFIG.IDRUN[num-1].TypeRun) != 10)) {
                                        // ���⠭���� �� ����⠭��
                     if (NewFlags1 == 0) {
//                        if (prun->NoAlarmConstFlag == 0)
                           Mem_AlarmWrCodRun(num-1,0x97,Mem_GetDayQ(num-1));
                     }
                     else {             // ��⨥ � ����⠭��
                                        // ��� �����
//                        if (prun->NoAlarmConstFlag == 0)
                           Mem_AlarmWrCodRun(num-1,0x17,Mem_GetDayQ(num-1));
                     }
                  }
                  break;
               case 1:                  // ��������
                  if (NewFlags1 == 0) { // ���⠭���� �� ����⠭��
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0xA0,Mem_GetDayQ(num-1));
                  }
                  else {                // ��⨥ � ����⠭��
                                        // ��� �����
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0x20,Mem_GetDayQ(num-1));
                  }
                  break;
               case 2:                  // �����������
                  if (NewFlags1 == 0) { // ���⠭���� �� ����⠭��
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0xA1,Mem_GetDayQ(num-1));
                  }
                  else {                // ��⨥ � ����⠭��
                                        // ��� �����
//                     if (prun->NoAlarmConstFlag == 0)
                        Mem_AlarmWrCodRun(num-1,0x21,Mem_GetDayQ(num-1));
                  }
                  break;
               case 3:                  // �������
                  if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
                  || (GLCONFIG.IDRUN[num-1].TypeRun) == 10) {
                                        // ���⠭���� �� ����⠭��
                     if (NewFlags1 == 0) {
//                        if (prun->NoAlarmConstFlag == 0)
                           Mem_AlarmWrCodRun(num-1,0xA2,Mem_GetDayQ(num-1));
                     }
                     else {             // ��⨥ � ����⠭��
                                        // ��� �����
//                        if (prun->NoAlarmConstFlag == 0)
                           Mem_AlarmWrCodRun(num-1,0x22,Mem_GetDayQ(num-1));
                     }
                  }
                  break;
            }
         }
      }

      if (prun->constdata != p->flag) {
         MEMAUDIT_1.type     = 40;

         *(unsigned long*)&MEMAUDIT_1.altvalue = (unsigned long)prun->constdata;

         prun->constdata = p->flag;      // ���筨� ����⠭�\���稪 0\1

         *(unsigned long*)&MEMAUDIT_1.newvalue = (unsigned long)prun->constdata;

         Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
       Cp= p->constdP;
        if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
         p->constdP  = Round_toKg(Cp,1.0,&Ep);
        else
         p->constdP = Round_toKg(Cp,KoefdP[GLCONFIG.EDIZM_T.EdIzmdP],&Ep);
      if (fabs(prun->constdP - p->constdP)>Ep || (fConst & 0x01)) {
         MEMAUDIT_1.type     = 41;
         if (GLCONFIG.EDIZM_T.EdIzmdP == 0)
          MEMAUDIT_1.altvalue = prun->constdP;
         else
          MEMAUDIT_1.altvalue = prun->constdP*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];
         prun->constdP   = p->constdP;  //����⠭� ��p�����
         MEMAUDIT_1.newvalue = Cp;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }
       Cp= p->constP;
       double KP = (GLCONFIG.EDIZM_T.EdIzP == 0) ? 1.0 : KoefP[GLCONFIG.EDIZM_T.EdIzP];
       p->constP = Round_toKg(Cp,KP,&Ep);
      if (fabs(prun->constP - p->constP)>Ep || (fConst & 0x02)) {
         MEMAUDIT_1.type     = 42;
         if (GLCONFIG.EDIZM_T.EdIzP == 0)
          MEMAUDIT_1.altvalue = prun->constP;
         else
          MEMAUDIT_1.altvalue = prun->constP*KoefP[GLCONFIG.EDIZM_T.EdIzP];
         prun->constP    = p->constP;   //����⠭� ��������
         MEMAUDIT_1.newvalue = Cp;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }

      if ((prun->constt != p->constt) || (fConst & 0x04)) {
         MEMAUDIT_1.type     = 43;
         MEMAUDIT_1.altvalue = prun->constt;
         prun->constt    = p->constt;   //����⠭� ⥬��p���p�
         MEMAUDIT_1.newvalue = prun->constt;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }

      if ((prun->constqw != p->constqw) || (fConst & 0x08)) {
         MEMAUDIT_1.type     = 44;
         MEMAUDIT_1.altvalue = prun->constqw;
         prun->constqw   = p->constqw;   //����⠭� p��室� �p� p.�., �3/��
         MEMAUDIT_1.newvalue = prun->constqw;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1));//������ ���� ����⥫���
      }

      if ((prun->RoVV != p->constRo) || (fConst & 0x10)) {
         MEMAUDIT_1.type     = 46;
         MEMAUDIT_1.altvalue = prun->RoVV;
         prun->RoVV       = p->constRo;       // ����⠭� ���⭮��
         prun->ROnom       = p->constRo*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];       // ����⠭� ���⭮��
         CalcZc[num-1] = 1;
         MEMAUDIT_1.newvalue = prun->RoVV;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1)); // ������ ���� ����⥫���
      }
       Cp= p->constHeat;
        if (GLCONFIG.EDIZM_T.EdIzmE == 1)
         p->constHeat  = Round_toKg(Cp,1.0,&Ep);
        else
         p->constHeat = Round_toKg(Cp,KoefEn[GLCONFIG.EDIZM_T.EdIzmE],&Ep);
     if (p->constHeat > 5.0)
     { if (fabs(prun->HsVV - p->constHeat)>Ep || (fConst & 0x20)) {
         MEMAUDIT_1.type     = 37;
         MEMAUDIT_1.altvalue = prun->HsVV; //prun->Heat*KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
         prun->Heat        = p->constHeat*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];     // ����⠭� 㤥�쭮� ⥯����
         prun->HsVV        = Cp;  //p->constHeat;     // ����⠭� 㤥�쭮� ⥯����
         MEMAUDIT_1.newvalue = Cp;
         CalcZc[num-1] = prun->nGerg88_AGA8;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1)); // ������ ���� ����⥫���
      }
      else if (fConst & 0x20)
      { prun->HsVV        = Cp;
        prun->Heat        = p->constHeat*KoRo[GLCONFIG.EDIZM_T.T_VVP][GLCONFIG.EDIZM_T.T_SU];     // ����⠭� 㤥�쭮� ⥯����
      }
     }
      if ((prun->NCO2 != p->constCO2) || (fConst & 0x40)) {
         MEMAUDIT_1.type     = 38;
         MEMAUDIT_1.altvalue = prun->NCO2;
         prun->NCO2        = p->constCO2;          // ����⠭� ��2
         CalcZc[num-1] = 1;
         MEMAUDIT_1.newvalue = prun->NCO2;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1)); // ������ ���� ����⥫���
      }

      if ((prun->NN2 != p->constN2) || (fConst & 0x80)) {
         MEMAUDIT_1.type     = 39;
         MEMAUDIT_1.altvalue = prun->NN2;
         prun->NN2         = p->constN2;         // ����⠭� N2
         MEMAUDIT_1.newvalue = prun->NN2;
         CalcZc[num-1] = 1;
         Mem_AuditWr_1(Mem_GetAuditPage(num-1)); // ������ ���� ����⥫���
      }

      StartRe[num-1] = 1e6;             // ������ � ��砫쭮�� ���⮢���

      if (Mem_ConfigWr() == 0)          // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
         SaveMessagePrefix_1(6,0x89);   // ����⠭�� ����ᠭ�
      else
         SaveMessagePrefix_1(6,0xFF);   // ����⠭�� �� ����ᠭ�
   }
}*/
//---------------------------------------------------------------------------
void RdPeriod_1() {                     // �⥭�� ���p�⨢��� (��p�����᪨�) ������
portC=1;
RdPeriodp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1);
}
/*   static unsigned int  startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   struct ReqPeriod    *prp;            // ��p���p� ���p��
   unsigned char       num;
   unsigned char       page;
   struct periodrecord *pprec;          // ��p���p� ���p�⨢��� ������ ��� ��p����
   unsigned int        point;
   int                 count, sizepp, Nrec;
   int err = 0;

//#ifdef PLOT
//         if (INTPAR.Com2Mode==0) {Nrec = 10; spp=4;}
//#else
//          else {Nrec = 9; spp=0;}

//#endif

   Nrec = 9;
   sizepp = sizeof(struct periodrecord);
   pprec = &SEND_1.RESPONSEPERIOD.PERIODRECORD[0];

   if (BufInKadr_1[2] - sizeof(ReqPeriod) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   prp = &RESIV_1.REQPERIOD;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prp,sizeof(ReqPeriod));

   if (prp->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prp->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prp->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prp->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨
   if (STARTENDTIME_1.RequestNum == 0) {// ��p���⪠ 1-�� ���p��
   //      CopyKoef();
      pbtime = &STARTENDTIME_1.STARTTIME;
      pbtime->seconds = 0;
      pbtime->minutes = prp->StartMinutes;
      pbtime->hours   = prp->StartHours;
      pbtime->date    = prp->StartDay;
      pbtime->month   = prp->StartMonth;
      pbtime->year    = prp->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                      // ����p��� �p����� ���p��
 //     STARTENDTIME_1.RequestNum = prp->RequestNum;
      pbtime = &STARTENDTIME_1.ENDTIME;

      pbtime->seconds = 59;
      pbtime->minutes = prp->EndMinutes+GLCONFIG.Period-1;

      if (pbtime->minutes > 59)
         pbtime->minutes= 59;

      pbtime->hours   = prp->EndHours;
      pbtime->date    = prp->EndDay;
      pbtime->month   = prp->EndMonth;
      pbtime->year    = prp->EndYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                   // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��

                                        // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                        // ��p�����᪨� ����
         page = GetPage(STARTENDTIME_1.num,0);
                                        // ��砫�� ������
         startpoint_1 = MemPerioddataSearchStart2_1(page);

         if (startpoint_1 < ARC4)       //{     // ����� ����
            SetBinTime4(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                        // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC4-1) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
            startpoint_1 = MemPerioddataSearchStart_1(page); // ��砫�� ������
         else {
            startpoint_1++;                // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC4)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                           // ������ ������
            endpoint_1 = MemPerioddataSearchEnd_1(page);

         if ((startpoint_1 > ARC4-1) || (endpoint_1 > ARC4-1))
            err |= 0x40;
      }

      if (err == 0) {
                                        // �⥭�� 1-� ����� ��ਮ���᪮�� ����
         MemPerioddataRd_1(startpoint_1,page);
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                    // ����� ��p���⪨ ��p���� ���p��
//   STARTENDTIME_1.RequestNum = prp->RequestNum;
   if (err) {
//      STARTENDTIME_1.RequestNum = 0xFF;
      if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x95);   // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;       // ����p ��⪨
         BufTrans_1[5] = 0;             // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;             // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // ��p�����᪨� ����
      page  = GetPage(STARTENDTIME_1.num,0);
      point = NextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);

      count = 0;

      if (point < ARC4) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<Nrec;i++) {       // Nrec ����ᥩ � ����� ���뫪�
            count++;                    // �p������� ��p�������
//            if (sizepp*count > 245) {count--;break;}
            if (i) {
               point = NextIndex_1(point,1,page);

               if (point > ARC4-1)
                  break;                // ����� ���᪠
            }
                                        // �⥭�� ����� ��p�����᪮�� ����
            MemPerioddataRd_1(point,page);

            pbtime = &MEMDATA_1.STARTTIME;
                                        // ���
            pprec->StartMonth   = pbtime->month;
            pprec->StartDay     = pbtime->date;
            pprec->StartYear    = pbtime->year;
            pprec->StartHours   = pbtime->hours;
            pprec->StartMinutes = pbtime->minutes;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;
            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
             {  pprec->StartDay |= 0x80;
                pprec->DP_V = MEMDATA_1.dp;  // ��ꥬ �� ࠡ��� �᫮����, �3 (��� ���稪�)
             }
             else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {   pprec->DP_V = MEMDATA_1.dp;
                  Vl.Val=pprec->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pprec->DP_V =  Vl.Val;
               }
               else
               { if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
                   pprec->DP_V = MEMDATA_1.dp;
                 else
                  pprec->DP_V = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];// �।��� ��९�� ��������, ���/�2 ���     |
                  Vl.Val=pprec->DP_V;
                  Vl.Bt[0]=Vl.Bt[0]&0xFE;
                  Vl.Bt[0]=Vl.Bt[0] | w;
                  pprec->DP_V =  Vl.Val;
               }
             }                           // ��p��� ������� � ������
            pprec->Period = MEMDATA_1.count;
            pprec->V      = MEMDATA_1.q;// ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
               Koef = 1.0;
              else
               Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
            pprec->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
   Vl.Val=MEMDATA_1.p;
   w=Vl.Bt[0]&0x1;
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       pprec->P    = MEMDATA_1.p;
      else
       pprec->P    = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP]; // �।��� ��������, ���/�2
   Vl.Val=pprec->P;
   Vl.Bt[0]=Vl.Bt[0]&0xFE;
   Vl.Bt[0]=Vl.Bt[0] | w;
   pprec->P =  Vl.Val;
            pprec->t      = MEMDATA_1.t;// �।��� ⥬������, �ࠤ. ������

//#ifdef PLOT
//            pprec->Ro     = MEMDATA_1.RO;
//#endif

            pprec++;

            if (point == endpoint_1)
               break;                   // ����� ���᪠
         }
      }
                                        // ���-�� ����ᥩ
      if (count > Nrec)  count = 0;
      SEND_1.RESPONSEPERIOD.NumRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC4-1)  || (STARTENDTIME_1.RequestNum == 0xFF))
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
      {   SEND_1.RESPONSEPERIOD.ResponseStatus = 0;
//          STARTENDTIME_1.RequestNum = 0xFF;
      }
      else                              // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEPERIOD.ResponseStatus = 1;
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizepp+9,0x95);
                                        // ������ ���p�
      movmem(&SEND_1.RESPONSEPERIOD,&BufTrans_1[5],count*sizepp+2);
      BufTrans_1[4] = 3 & num;          // ����p ��⪨
   }
}
*/
//---------------------------------------------------------------------------
void RdAlarm_1()  //�⥭�� ��p���� ���p��
{
 portC=1;
 RdAlarmp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1);
}
/*
{ static unsigned int startpoint_1,endpoint_1;
  struct bintime      *pbtime;
  struct reqalarm     *preq;  //��p���p� ���p��
  unsigned char       num;
  unsigned char       page;
  struct alarmrecord  *prec;  //��p���p� ������ ��� ��p����
  unsigned int        point;
  int                 count,Nrec;

  prec = &SEND_1.RESPONSEALARM.ALARMRECORD[0];
  int err = 0;
  Nrec = 18;
  if(BufInKadr_1[2] - sizeof(reqalarm) != 7) err |= 1;//����p��� ����� ���p��
  num = BufInKadr_1[4];
  if( (3 & num) > GLCONFIG.NumRun) err |= 2;//����p��� ����p� ��⪨
  preq = &RESIV_1.REQALARM;
  movmem(&BufInKadr_1[5],preq,sizeof(reqalarm));//��p������� ��p����p�� ���p��
  if ((preq->RequestNum != 0)  && (STARTENDTIME_1.RequestNum != 0xFF))
  {// printf("R0=%d R1=%d\n",preq->RequestNum,STARTENDTIME_1.RequestNum);
    if( STARTENDTIME_1.RequestNum == preq->RequestNum)   {;}
    else
    if( STARTENDTIME_1.RequestNum +1 != preq->RequestNum)  //����� �����
    err |= 0x04;  //����p��� ��᫥����⥫쭮�� ���p�ᮢ
  }
  STARTENDTIME_1.RequestNum = preq->RequestNum; //����� �����
  STARTENDTIME_1.num = 3 & (num-1);             //����� ��⪨

  if(STARTENDTIME_1.RequestNum == 0)  //��p���⪠ 1-�� ���p��
  { pbtime = &STARTENDTIME_1.STARTTIME;
    pbtime->seconds = preq->StartSeconds;
    pbtime->minutes = preq->StartMinutes;
    pbtime->hours   = preq->StartHours;
    pbtime->date    = preq->StartDay;
    pbtime->month   = preq->StartMonth;
    pbtime->year    = preq->StartYear;
    if( Mem_TestReqTime_1(pbtime) ) err |= 8;//����p��� �p����� ���p��
    pbtime = &STARTENDTIME_1.ENDTIME;
    pbtime->seconds = preq->EndSeconds;
    pbtime->minutes = preq->EndMinutes;
    pbtime->hours   = preq->EndHours;
    pbtime->date    = preq->EndDay;
    pbtime->month   = preq->EndMonth;
    pbtime->year    = preq->EndYear;
    if( Mem_TestReqTime_1(pbtime) ) err |= 0x10;//����p��� �p����� ���p��
    if( Mem_TestReqTimeSE_1() ) err |= 0x20;//����p��� ᮮ⭮襭�� �p���� ���p��
//�������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
    if ( err == 0 )
    { page = Mem_AlarmGetPage(STARTENDTIME_1.num);//��p���� ��p���� ���p��
//      startpoint_1 = Mem_AlarmSearchStart_1(page);  //��砫�� ������
       startpoint_1 = Mem_AlarmSearchStart2_1(page);   // ��砫�� ������

                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
       if (startpoint_1 < 1152) {  // ����� ����
          int tInd,tPoint;

          tInd = startpoint_1*14+256;
// ��࠭�� ��⠭������ �㭪樥�  Mem_AlarmSearchStart2_1(page)
          BinTmp_1.year    = peekb(SEGMENT,tInd+5);
          BinTmp_1.month   = peekb(SEGMENT,tInd+4);
          BinTmp_1.date    = peekb(SEGMENT,tInd+3);
          BinTmp_1.hours   = peekb(SEGMENT,tInd+2);
          BinTmp_1.minutes = peekb(SEGMENT,tInd+1);
          BinTmp_1.seconds = peekb(SEGMENT,tInd);

          tPoint = peek(SEGMENT,0);

          tInd = tPoint*14+256;

          BinTmp2_1.year    = peekb(SEGMENT,tInd+5);
          BinTmp2_1.month   = peekb(SEGMENT,tInd+4);
          BinTmp2_1.date    = peekb(SEGMENT,tInd+3);
          BinTmp2_1.hours   = peekb(SEGMENT,tInd+2);
          BinTmp2_1.minutes = peekb(SEGMENT,tInd+1);
          BinTmp2_1.seconds = peekb(SEGMENT,tInd);
       }
                                       // ��������� < ��砫� ��娢�
       if ((startpoint_1 > 1152) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
          startpoint_1 = Mem_AlarmSearchStart_1(page); // ��砫�� ������
       else {
          startpoint_1++;                // �த������ ��������� 㪠��⥫� �� 1
          if (startpoint_1 == 1152)
             startpoint_1 = 0;
       }

        if( Mem_TestReqTimeEq_1() == 0) endpoint_1 = startpoint_1;
        else endpoint_1 = Mem_AlarmSearchEnd_1(page); //������ ������
//printf("startpoint_1=%d endpoint_1=%d\n",startpoint_1, endpoint_1);
      if((startpoint_1 > 1151) || (endpoint_1 > 1151)) err |= 0x40;
    }
    if ( err == 0 )
    { Mem_AlarmRd_1(startpoint_1,page); //�⥭�� 1-� ����� ���� ���p��
//����p��� ᮮ⭮襭�� �p���� ���p�� � �p娢�
      if( Mem_AlarmTestReqTimeSAER_1() ) err |= 0x80;
      else
      if (Mem_AlarmTestReqTimeSABR_1())  err |= 0x80;
    }
  }//����� ��p���⪨ ��p���� ���p��

  if(err)
  { if( err & 0xC0 ) //��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
    { SaveMessagePrefix_1(9,0x97);  //������ �p�䨪�
      BufTrans_1[4] = 3 & num;      //����p ��⪨
      BufTrans_1[5] = 0;            //���-�� ����ᥩ � �⢥�
      BufTrans_1[6] = 0;            //�p��������� ���
    }
    else  SaveMessagePrefix_1(6,0xFF);  //�訡�� ��p����p�� ���p��
  }
  else  //�訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
  { page  = Mem_AlarmGetPage(STARTENDTIME_1.num);//��p���� ��p����
    point = Mem_AlarmNextIndex_1(startpoint_1, 18*STARTENDTIME_1.RequestNum, page);
    count = 0;
    if( point < 1152 )  //���� ����� � ⠪�� ᬥ饭���
    { for(int i=0;i<18;i++)
      { count++;  //�p������� ��p�������
        if(i)
        { point = Mem_AlarmNextIndex_1(point,1,page);
          if(point > 1151) break;     //����� ���᪠
        }
        Mem_AlarmRd_1(point,page);    //�⥭�� ����� ����
        pbtime = &MEMALARM_1.TIME;
        prec->StartMonth   = pbtime->month;     //�����
        prec->StartDay     = pbtime->date;      //����
        prec->StartYear    = pbtime->year;      //���
        prec->StartHours   = pbtime->hours;     //��
        prec->StartMinutes = pbtime->minutes;   //�����
        prec->StartSeconds = pbtime->seconds;   //ᥪ㭤�

        prec->alarmcode = (unsigned char)MEMALARM_1.type; //⨯ ���p��
        prec->pointnum  = (unsigned char)(MEMALARM_1.type/256);//���䨣�p��� ���稪��

        prec->runnum    = STARTENDTIME_1.num+1;//����p ��⪨
        prec->value     = MEMALARM_1.value;    //�p����饥 ���祭��
        prec++;
        if(point == endpoint_1) break;  //����� ���᪠
      }
    }
      if (count > Nrec)  count = 0;
      SEND_1.RESPONSEALARM.NumRecord = count; //���-�� ����ᥩ
      if((point == endpoint_1)||(count==0)||(point > 1151)) SEND_1.RESPONSEALARM.ResponseStatus = 0;  //����� �⢥� (0= �����襭�, 1= ���� �� �����)
      else SEND_1.RESPONSEALARM.ResponseStatus = 1;  //����� �⢥� (0= �����襭�, 1= ���� �� �����)
      SaveMessagePrefix_1(count*sizeof(struct alarmrecord)+9,0x97);//������ �p�䨪�
      movmem(&SEND_1.RESPONSEALARM,&BufTrans_1[5],count*sizeof(struct alarmrecord)+2); //������ ���p�
      BufTrans_1[4] = 3 & num;                //����p ��⪨
  }
       // cpystartendtime1(NumKan1);
}*/
//---------------------------------------------------------------------------
void RdAudit_1()  //�⥭�� ��p���� ����⥫���
{
portC=1;
 RdAuditp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1);
}

/*
{ static unsigned int  startpoint_1,endpoint_1;
  struct bintime      *pbtime;
  struct reqaudit     *preq;  //��p���p� ���p��
  unsigned char       num;
  unsigned char       page;
  struct auditrecord  *prec;  //��p���p� ������ ��� ��p����
  unsigned int        point;
  int                 count;

  prec = &SEND_1.RESPONSEAUDIT.AUDITRECORD[0];
  int err = 0;
  if(BufInKadr_1[2] - sizeof(reqaudit) != 7) err |= 1;//����p��� ����� ���p��
  num = BufInKadr_1[4];
  if((3 & num) > GLCONFIG.NumRun) err |= 2;//����p��� ����p� ��⪨
  preq = &RESIV_1.REQAUDIT;
  movmem(&BufInKadr_1[5],preq,sizeof(reqaudit));//��p������� ��p����p�� ���p��
  if ((preq->RequestNum != 0)  && (STARTENDTIME_1.RequestNum != 0xFF))
  {// printf("R0=%d R1=%d\n",preq->RequestNum,STARTENDTIME_1.RequestNum);
    if( STARTENDTIME_1.RequestNum == preq->RequestNum)   {;}
    else
    if( STARTENDTIME_1.RequestNum +1 != preq->RequestNum)  //����� �����
    err |= 0x04;  //����p��� ��᫥����⥫쭮�� ���p�ᮢ
  }
  STARTENDTIME_1.RequestNum = preq->RequestNum; //����� �����
  STARTENDTIME_1.num = 3 & (num-1);             //����� ��⪨

  if(STARTENDTIME_1.RequestNum == 0)  //��p���⪠ 1-�� ���p��
  { pbtime = &STARTENDTIME_1.STARTTIME;
    pbtime->seconds = preq->StartSeconds;
    pbtime->minutes = preq->StartMinutes;
    pbtime->hours   = preq->StartHours;
    pbtime->date    = preq->StartDay;
    pbtime->month   = preq->StartMonth;
    pbtime->year    = preq->StartYear;
    if( Mem_TestReqTime_1(pbtime) ) err |= 8;//����p��� �p����� ���p��
    pbtime = &STARTENDTIME_1.ENDTIME;
    pbtime->seconds = preq->EndSeconds;
    pbtime->minutes = preq->EndMinutes;
    pbtime->hours   = preq->EndHours;
    pbtime->date    = preq->EndDay;
    pbtime->month   = preq->EndMonth;
    pbtime->year    = preq->EndYear;
    if( Mem_TestReqTime_1(pbtime) ) err |= 0x10;//����p��� �p����� ���p��
    if( Mem_TestReqTimeSE_1() ) err |= 0x20;//����p��� ᮮ⭮襭�� �p���� ���p��
//�������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
    if ( err == 0 )
    { page = Mem_GetAuditPage(STARTENDTIME_1.num);//��p���� ��p���� ����⥫���
 //     startpoint_1 = Mem_AuditSearchStart_1(page);  //��砫�� ������
       startpoint_1 = Mem_AuditSearchStart2_1(page);   // ��砫�� ������

                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
       if (startpoint_1 < ARCAU) {  // ����� ����
          int tInd,tPoint;

          tInd = startpoint_1*17+256;
// ��࠭�� � Mem_AuditSearchStart2_1(page)
          BinTmp_1.year    = peekb(SEGMENT,tInd+5);
          BinTmp_1.month   = peekb(SEGMENT,tInd+4);
          BinTmp_1.date    = peekb(SEGMENT,tInd+3);
          BinTmp_1.hours   = peekb(SEGMENT,tInd+2);
          BinTmp_1.minutes = peekb(SEGMENT,tInd+1);
          BinTmp_1.seconds = peekb(SEGMENT,tInd);

          tPoint = peek(SEGMENT,0);

          tInd = tPoint*17+256;

          BinTmp2_1.year    = peekb(SEGMENT,tInd+5);
          BinTmp2_1.month   = peekb(SEGMENT,tInd+4);
          BinTmp2_1.date    = peekb(SEGMENT,tInd+3);
          BinTmp2_1.hours   = peekb(SEGMENT,tInd+2);
          BinTmp2_1.minutes = peekb(SEGMENT,tInd+1);
          BinTmp2_1.seconds = peekb(SEGMENT,tInd);
       }
                                       // ��������� < ��砫� ��娢�
       if ((startpoint_1 > ARCAU) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
          startpoint_1 = Mem_AuditSearchStart_1(page); // ��砫�� ������
       else {
          startpoint_1++;                // �த������ ��������� 㪠��⥫� �� 1
          if (startpoint_1 == ARCAU)
             startpoint_1 = 0;
       }

        if( Mem_TestReqTimeEq_1() == 0) endpoint_1 = startpoint_1;
        else endpoint_1 = Mem_AuditSearchEnd_1(page); //������ ������
      if((startpoint_1 > ARCAU-1) || (endpoint_1 > ARCAU-1)) err |= 0x40;
    }
//sprintf(str,"err=%d sp1=%d ep1=%d\n\r",err,startpoint_1,endpoint_1);
//ComPrint_2(str);
//n_delay(20);
    if ( err == 0 )
    { Mem_AuditRd_1(startpoint_1,page); //�⥭�� 1-� ����� ���� ����⥫���
      //����p��� ᮮ⭮襭�� �p���� ���p�� � �p娢�
      if( Mem_AuditTestReqTimeSAER_1() ) err |= 0x80;
      else
      if( Mem_AuditTestReqTimeSABR_1() ) err |= 0x80;
    }
  }//����� ��p���⪨ ��p���� ���p��

  if ( err )
  { if( err & 0xC0 ) //��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
    { SaveMessagePrefix_1(9,0x96);  //������ �p�䨪�
      BufTrans_1[4] = 3 & num;      //����p ��⪨
      BufTrans_1[5] = 0;            //���-�� ����ᥩ � �⢥�
      BufTrans_1[6] = 0;            //�p��������� ���
    }
    else  SaveMessagePrefix_1(6,0xFF);  //�訡�� ��p����p�� ���p��
  }
  else  //�訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
  { page  = Mem_GetAuditPage(STARTENDTIME_1.num);//��p���� ��p���� ����⥫���
    point = Mem_AuditNextIndex(startpoint_1, 14*STARTENDTIME_1.RequestNum, page);
    count = 0;
    if(point < ARCAU-1)
    { for(int i=0;i<14;i++)
      { count++;  //�p������� ��p�������
        if(i)
        { point = Mem_AuditNextIndex(point,1,page);
          if(point > ARCAU-1) break;      //����� ���᪠
        }
        Mem_AuditRd_1(point,page);    //�⥭�� ����� ���� ����⥫���
        pbtime = &MEMAUDIT_1.TIME;
        prec->StartMonth   = pbtime->month;     //�����
        prec->StartDay     = pbtime->date;      //����
        prec->StartYear    = pbtime->year;      //���
        prec->StartHours   = pbtime->hours;     //��
        prec->StartMinutes = pbtime->minutes;   //�����
        prec->StartSeconds = pbtime->seconds;   //ᥪ㭤�
        prec->ParemeterNum = MEMAUDIT_1.type;     //����p ��p����p�
        prec->RunNum       = STARTENDTIME_1.num+1;//����p ��⪨
        prec->OldValue     = MEMAUDIT_1.altvalue; //�p����饥 ���祭��
        prec->NewValue     = MEMAUDIT_1.newvalue; //����� ���祭��
        prec++;
        if(point == endpoint_1) break;  //����� ���᪠
      }
    }
//sprintf(str,"count=%d y=%d m=%d d=%d h=%d\n\r",count,pbtime->year,pbtime->month,pbtime->date,pbtime->hours);
//ComPrint_2(str);
//n_delay(20);
      if (count > Nrec)  count = 0;
      SEND_1.RESPONSEAUDIT.NumRecord = count;   //���-�� ����ᥩ
        if((point==endpoint_1)||(count==0)||(point>947)) SEND_1.RESPONSEAUDIT.ResponseStatus = 0;//�����襭�
        else SEND_1.RESPONSEAUDIT.ResponseStatus = 1;  //���� �� �����
      SaveMessagePrefix_1(count*sizeof(struct auditrecord)+9,0x96);//������ �p�䨪�
      movmem(&SEND_1.RESPONSEAUDIT,&BufTrans_1[5],count*sizeof(struct auditrecord)+2); //������ ���p�
      BufTrans_1[4] = 3 & num;                //����p ��⪨
  }
        cpystartendtime1(NumKan1);
}*/
//------------------------------------------------------------------------
void Comm_RdCykl_1() {                  // �⥭�� 横���᪨� ������
portC=1;
 Comm_RdCyklp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1);
}

/*
   static unsigned int   startpoint_1,endpoint_1;
   struct bintime        *pbintime;
   unsigned char         num;
   unsigned char         page;
   unsigned int          point;
   int                   err;
   struct reqcykl        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������
   struct cyklrecord     *pcr;          // ��p���p� ����� ������ ��� ��p����
   int                   count, sizepc, Nrec, spp;
//         if (INTPAR.Com2Mode==0) {Nrec = 10; spp=4;}
//          else {Nrec = 9; spp=0;}
   Nrec = 9;
   sizepc = sizeof(struct cyklrecord);
   pcr = &SEND_1.RESPONSECYKL.CYKLRECORD[0];
   err = 0;

   if (BufInKadr_1[2] - sizeof(struct reqcykl) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   prc = &RESIV_1.REQCYKL;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prc,sizeof(struct reqcykl));

   if (prc->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prc->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prc->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prc->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

   if (STARTENDTIME_1.RequestNum == 0) {// ��p���⪠ 1-�� ���p��
      pbintime = &STARTENDTIME_1.STARTTIME;

      pbintime->seconds = prc->StartSeconds;
      pbintime->minutes = prc->StartMinutes;
      pbintime->hours   = prc->StartHours;
      pbintime->date    = prc->StartDay;
      pbintime->month   = prc->StartMonth;
      pbintime->year    = prc->StartYear;

      if (Mem_TestReqTime_1(pbintime))
         err |= 8;                      // ����p��� �p����� ���p��

      pbintime = &STARTENDTIME_1.ENDTIME;

      pbintime->seconds = prc->EndSeconds;
      pbintime->minutes = prc->EndMinutes;
      pbintime->hours   = prc->EndHours;
      pbintime->date    = prc->EndDay;
      pbintime->month   = prc->EndMonth;
      pbintime->year    = prc->EndYear;


      if (Mem_TestReqTime_1(pbintime))
         err |= 0x10;                   // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��
                                        // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {                   // ��p���� 横���᪮�� ����
         page = Mem_CyklGetPage_1(STARTENDTIME_1.num);
                                        // ��砫�� ������
         startpoint_1 = Mem_DaySearchStart2_1(page);

                                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC2)       //{     // ����� ����
            SetBinTime2(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                        // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC2) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                        // ��砫�� ������
            startpoint_1 = Mem_DaySearchStart_1(page);
         else {
            startpoint_1++;             // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC2)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                           // ������ ������
            endpoint_1 = Mem_DaySearchEnd_1(page);

         if ((startpoint_1 > ARC2-1) || (endpoint_1 > ARC2-1))
            err |= 0x40;
      }
      if (err == 0) {
         Mem_DayRd_1(startpoint_1,page);// �⥭�� ����� 横���᪮�� ����
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                    // ����� ��p���⪨ 1-�� ���p��
   if (err) {
      if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x98);   // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;       // ����p ��⪨
         BufTrans_1[5] = 0;             // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;             // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // ��p���� 横���᪮�� ����
      page  = Mem_CyklGetPage_1(STARTENDTIME_1.num);
      point = Mem_DeyNextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);
      count = 0;

      if (point < ARC2) {
         for (int i=0;i<Nrec;i++) {        // 10 ����ᥩ � ����� ���뫪�
            count++;                     // �p������� ��p�������
//            if (sizepc*count > 245) {count--; break;}
            if (i) {
               point = Mem_DeyNextIndex_1(point,1,page);
               if (point > ARC2-1) break;// ����� ���᪠
            }

            Mem_DayRd_1(point,page);     // �⥭�� ����� 横���᪮�� ����

            pbintime = &MEMDATA_1.STARTTIME;
                                        // ���
            pcr->StartMonth   = pbintime->month;
            pcr->StartDay     = pbintime->date;
            pcr->StartYear    = pbintime->year;
            pcr->StartHours   = pbintime->hours;
            pcr->StartMinutes = pbintime->minutes;
            pcr->StartSeconds = pbintime->seconds;
   union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;
            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
            { pcr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
              pcr->DP_V   = MEMDATA_1.dp;   // ��ꥬ �3
            }
            else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {   pcr->DP_V = MEMDATA_1.dp;
                  Vl.Val=pcr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pcr->DP_V =  Vl.Val;
               }
               else
               {  pcr->DP_V   = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   //  ��ꥬ �� �3
                 Vl.Val=pcr->DP_V;
                 Vl.Bt[0]=Vl.Bt[0]&0xFE;
                 Vl.Bt[0]=Vl.Bt[0] | w;
                 pcr->DP_V =  Vl.Val;
               }
            }                           // ��p��� ������� � ᥪ㭤��
            pcr->Period = MEMDATA_1.count;
            pcr->V      = MEMDATA_1.q;  // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            pcr->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
      Vl.Val=MEMDATA_1.p;
      w=Vl.Bt[0]&0x1;
      pcr->P      = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
      Vl.Val=pcr->P;
      Vl.Bt[0]=Vl.Bt[0]&0xFE;
      Vl.Bt[0]=Vl.Bt[0] | w;
      pcr->P =  Vl.Val;
      pcr->t      = MEMDATA_1.t;  // �।��� ⥬������, �ࠤ. ������
//#ifdef PLOT
//            pcr->Ro     = MEMDATA_1.RO;
//#endif
            pcr++;
            if (point == endpoint_1)
               break;  //����� ���᪠
         }
      }                                 // ���-�� ����ᥩ
      SEND_1.RESPONSECYKL.NumCyklRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC2-1))
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSECYKL.ResponseStatus = 0;
      else
         SEND_1.RESPONSECYKL.ResponseStatus = 1;
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizepc+9,0x98);

                                        // ������ ���p�
      movmem(&SEND_1.RESPONSECYKL,&BufTrans_1[5],count*sizepc+2);

      BufTrans_1[4] = 3 & num;          // ����p ��⪨
   }
}
*/
//---------------------------------------------------------------------------
void RdHour_1() {                       // �⥭�� �ᮢ��� ����
portC=1;
RdHourp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1);
}
/*
void RdHour_1() {                       // �⥭�� �ᮢ��� ����
   static unsigned int  startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point;
   int                 err;
   int                 count, sizeph, Nrec, spp;
   struct reqhour      *prh;            // ��p���p� ���p�� - �⥭�� �ᮢ�� ������
   struct hourrecord   *phr;            // ��p���p� ����� ������ ��� ��p����
   Nrec = 9;
   sizeph = sizeof(struct hourrecord);
   phr = &SEND_1.RESPONSEHOUR.HOURRECORD[0];
   err = 0;

   if (BufInKadr_1[2] - sizeof(reqhour) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   prh = &RESIV_1.REQHOUR;              // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prh,sizeof(reqhour));

   if (prh->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prh->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prh->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        // ����� �����
   STARTENDTIME_1.RequestNum = prh->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

   if (STARTENDTIME_1.RequestNum == 0) {// ��p���⪠ 1-�� ���p��
//      CopyKoef(); 

      pbtime = &STARTENDTIME_1.STARTTIME;
      pbtime->seconds = 0;
      pbtime->minutes = 0;
      pbtime->hours   = prh->StartHours;
      pbtime->date    = prh->StartDay;
      pbtime->month   = prh->StartMonth;
      pbtime->year    = prh->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                      // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;

      pbtime->seconds = 59;
      pbtime->minutes = 59;
      pbtime->hours   = prh->EndHours;
      pbtime->date    = prh->EndDay;
      pbtime->month   = prh->EndMonth;
      pbtime->year    = prh->EndYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                   // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��
                                        // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                        // �ᮢ�� ����
         page = GetPage(STARTENDTIME_1.num,1);
                                        // ��砫�� ������
         startpoint_1 = MemPerioddataSearchStart2_1(page);

                                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC4)       //{     // ����� ����
            SetBinTime4(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                        // ��������� < ��砫� ��娢�
         if ((startpoint_1 > ARC4-1) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
            startpoint_1 = MemPerioddataSearchStart_1(page); // ��砫�� ������
         else {
            startpoint_1++;             // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC4)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                           // ������ ������
            endpoint_1 = MemPerioddataSearchEnd_1(page);

         if ((startpoint_1 > ARC4-1) || (endpoint_1 > ARC4-1))
            err |= 0x40;
      }

      if (err == 0) {                   // �⥭�� 1-� ����� �ᮢ��� ����
         MemPerioddataRd_1(startpoint_1,page);
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                    // ����� ��p���⪨ ��p���� ���p��


   if (err) {
      if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,0x99);   // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;       // ����p ��⪨
         BufTrans_1[5] = 0;             // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;             // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // �ᮢ�� ����
      page  = GetPage(STARTENDTIME_1.num,1);
      point = NextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);
      count = 0;

      if (point < ARC4) {               // ���� ����� � ⠪�� ᬥ饭���
         for (int i=0;i<Nrec;i++) {       // 10 ����ᥩ � ����� ���뫪�
            count++;                    // ���-�� ����ᥩ
//            if (sizeph*count > 245) {count--; break;}
            if (i) {
               point = NextIndex_1(point,1,page);
                                        // ����� ���᪠
               if (point > ARC4-1) break;
            }
                                        // �⥭�� ����� �ᮢ��� ����
            MemPerioddataRd_1(point,page);

            pbtime = &MEMDATA_1.STARTTIME;
                                        //���
            phr->StartMonth   = pbtime->month;
            phr->StartDay     = pbtime->date;
            phr->StartYear    = pbtime->year;
            phr->StartHours   = pbtime->hours;
   union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;
            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
              { phr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                phr->DP_V   = MEMDATA_1.dp;   //  ��ꥬ �� �3
              }
              else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {  phr->DP_V = MEMDATA_1.dp;
                  Vl.Val=phr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  phr->DP_V =  Vl.Val;
               }
               else
               {  if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
                   phr->DP_V = MEMDATA_1.dp;
                  else
                  phr->DP_V   = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];   // �।��� ��९�� ��������, ���/�2 ���     |
                   Vl.Val=phr->DP_V;
                   Vl.Bt[0]=Vl.Bt[0]&0xFE;
                   Vl.Bt[0]=Vl.Bt[0] | w;
                   phr->DP_V =  Vl.Val;
                 }
               }                            // ��p��� ������� � ������
            phr->Period = MEMDATA_1.count;
            phr->V      = MEMDATA_1.q;  // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
               Koef = 1.0;
              else
               Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
             }
            phr->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
      Vl.Val=MEMDATA_1.p;
      w=Vl.Bt[0]&0x1;
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       phr->P = MEMDATA_1.p;
      else
       phr->P = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];    // �।��� ��������, ���/�2
      Vl.Val=phr->P;
      Vl.Bt[0]=Vl.Bt[0]&0xFE;
      Vl.Bt[0]=Vl.Bt[0] | w;
      phr->P =  Vl.Val;
            phr->t      = MEMDATA_1.t;  // �।��� ⥬������, �ࠤ. ������
//#ifdef PLOT
//            phr->Ro     = MEMDATA_1.RO;
//#endif

            phr++;

            if (point == endpoint_1)
               break;                   // ����� ���᪠
         }
      }
      if (count > Nrec) count = 0;
                                        // ���-�� ����ᥩ
      SEND_1.RESPONSEHOUR.NumHourRecord = count;

      if ((point == endpoint_1)||(count==0)||(point > ARC4-1))
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEHOUR.ResponseStatus = 0;
      else                              // ����� �⢥� = 1; ���� �� �����
         SEND_1.RESPONSEHOUR.ResponseStatus = 1;
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizeph+9,0x99);
                                        // ������ ���p�
      movmem(&SEND_1.RESPONSEHOUR,&BufTrans_1[5],count*sizeph+2);

      BufTrans_1[4] = 3 & num;          // ����p ��⪨
   }
         cpystartendtime1(NumKan1);
}*/
//---------------------------------------------------------------------------
void RdDay_1() {                        // �⥭�� ���筮�� ����
portC=1;
RdDayp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1);
}
/*
   static unsigned int  startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point;
   int                 err,lenrecord,nfc,k;
   int                 count, Nrec,sizerd;
   struct reqdaily     *prd;            // ��p���p� ���p�� - �⥭�� ������ ������
   struct dailyrecord  *pdr;            // ��p���p� ����� ������ ��� ��p����

   pdr = &SEND_1.RESPONSEDAILY.DAILYRECORD[0];
   err = 0;
   sizerd = sizeof(struct dailyrecord);
   if (BufInKadr_1[2] - sizeof(reqdaily) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4] & 3;
  nfc = BufInKadr_1[3];
   if (nfc==0x14)
    { Nrec=10;
      lenrecord = sizerd-4; // ��� Vrtot
    }
    else
    { Nrec=9;
      lenrecord = sizerd;
    }

   if ((num & 3) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   prd = &RESIV_1.REQDAILY;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[5],prd,sizeof(reqdaily));

   if (prd->RequestNum != 0) {
                                        // ������� �०��� �⢥� �� ������ ���㫥�� �����
      if (STARTENDTIME_1.RequestNum == prd->RequestNum)
         return;

      if (STARTENDTIME_1.RequestNum +1 != prd->RequestNum)
         err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
   }
                                        //����� �����
   STARTENDTIME_1.RequestNum = prd->RequestNum;
   STARTENDTIME_1.num = 3 & (num-1);    // ����� ��⪨

   if (STARTENDTIME_1.RequestNum == 0) {// ��p���⪠ 1-�� ���p��
//      CopyKoef();
      pbtime = &STARTENDTIME_1.STARTTIME;
      pbtime->seconds = 0;
      pbtime->minutes = 0;
                                        // ����p���� ��;
      pbtime->hours   = GLCONFIG.ContrHour;
      pbtime->date    = prd->StartDay;
      pbtime->month   = prd->StartMonth;
      pbtime->year    = prd->StartYear;

      if (Mem_TestReqTime_1(pbtime))
         err |= 8;                      // ����p��� �p����� ���p��

      pbtime = &STARTENDTIME_1.ENDTIME;
      pbtime->seconds = 59;
      pbtime->minutes = 59;
                                        // ����p���� ��;
      pbtime->hours   = (GLCONFIG.ContrHour) ? GLCONFIG.ContrHour-1 : 23;

      if (GLCONFIG.ContrHour)
         dayPlus(prd->EndDay,prd->EndMonth,prd->EndYear,
                pbtime->date,pbtime->month,pbtime->year);
      else {
         pbtime->date    = prd->EndDay;
         pbtime->month   = prd->EndMonth;
         pbtime->year    = prd->EndYear;
      }

      if (Mem_TestReqTime_1(pbtime))
         err |= 0x10;                   // ����p��� �p����� ���p��

      if (Mem_TestReqTimeSE_1())
         err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��

                                        // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
      if (err == 0) {
                                        // ����� ����
         page = GetPage(STARTENDTIME_1.num,2);
                                        // ��砫�� ������
         startpoint_1 = Mem_DaySearchStart2_1(page);

                                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
         if (startpoint_1 < ARC2)       //{      // ����� ����
            SetBinTime2(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
                                        // ��������� < ��砫� ��娢�

         if ((startpoint_1 > ARC2) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
                                        // ��砫�� ������
            startpoint_1 = Mem_DaySearchStart_1(page);
         else {
            startpoint_1++;             // �த������ ��������� 㪠��⥫� �� 1
            if (startpoint_1 == ARC2)
               startpoint_1 = 0;
         }

         if (Mem_TestReqTimeEq_1() == 0)
            endpoint_1 = startpoint_1;
         else                           // ������ ������
            endpoint_1 = Mem_DaySearchEnd_1(page);

         if ((startpoint_1 > ARC2-1) || (endpoint_1 > ARC2-1))
            err |= 0x40;
      }

      if (err == 0) {
         Mem_DayRd_1(startpoint_1,page);// �⥭�� 1-� ����� ���筮�� ����
         if (Mem_TestReqTimeEnd_1() || Mem_TestReqTimeBeg_1())
//         if (Mem_TestReqTimeEnd_1())
            err |= 0x80;
      }
   }                                    // ����� ��p���⪨ ��p���� ���p��


   if (err) {
      if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
         SaveMessagePrefix_1(9,nfc+128);   // ������ �p�䨪�
         BufTrans_1[4] = 3 & num;       // ����p ��⪨
         BufTrans_1[5] = 0;             // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;             // �p��������� ���
      }
      else
         SaveMessagePrefix_1(6,0xFF);   // �訡�� ��p����p�� ���p��
   }
   else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                        // ����� ����
      page  = GetPage(STARTENDTIME_1.num,2);
      point = Mem_DeyNextIndex_1(startpoint_1,Nrec*STARTENDTIME_1.RequestNum,page);

      count = 0;

      if (point < ARC2) {               // ���� ����� � ⠪�� ᬥ饭���
         for(int i=0;i<Nrec;i++) {        // 10 ����ᥩ � ����� ���뫪�
            count++;                    // ���-�� ����ᥩ
            if (i) {
               point = Mem_DeyNextIndex_1(point,1,page);
               if (point > ARC2-1)
                  break;                // ����� ���᪠
            }

            Mem_DayRd_1(point,page);    // �⥭�� ����� ���筮�� ����
            pbtime = &MEMDATA_1.STARTTIME;
                                        // �����
            pdr->StartMonth = pbtime->month;
                                        // ����
            pdr->StartDay = pbtime->date;
union {
      unsigned char Bt[4];
      float Val;
   } Vl;
char w;
   Vl.Val=MEMDATA_1.dp;
   w=Vl.Bt[0]&0x1;
            if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
            || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
             {  pdr->StartDay |= 0x80;// ᯮᮡ ����p���� �� ��⪥
                pdr->DP_V = MEMDATA_1.dp;  //��ꥬ �� �3
             }
             else
             { if (GLCONFIG.IDRUN[num-1].Vlag == 1)
               {  pdr->DP_V = MEMDATA_1.dp;
                  Vl.Val=pdr->DP_V;
                  Vl.Bt[0]=Vl.Bt[0] | 0x01;
                  pdr->DP_V =  Vl.Val;
               }
               else
               {  if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
                   pdr->DP_V = MEMDATA_1.dp;
                  else
                  pdr->DP_V = MEMDATA_1.dp*KoefdP[GLCONFIG.EDIZM_T.EdIzmdP];  // �।��� ��९�� ��������, ���/�2 ���     |
                   Vl.Val=pdr->DP_V;
                   Vl.Bt[0]=Vl.Bt[0]&0xFE;
                   Vl.Bt[0]=Vl.Bt[0] | w;
                   pdr->DP_V =  Vl.Val;
                 }
             }                           // ���
            pdr->StartYear = pbtime->year;

            pdr->V    = MEMDATA_1.q;    // ��ꥬ, �3
            float Koef;
            if  (GLCONFIG.EDIZM_T.EdIzmE == 0)
             Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE]/1.0e3;
            else
            { if (GLCONFIG.EDIZM_T.EdIzmE == 1)
               Koef = 1.0;
              else
               Koef = KoefEn[GLCONFIG.EDIZM_T.EdIzmE];
            }
            pdr->En   = MEMDATA_1.FullHeat*Koef/1.0e3;
      Vl.Val=MEMDATA_1.p;
      w=Vl.Bt[0]&0x1;
      if (GLCONFIG.EDIZM_T.EdIzP == 0)
       pdr->P    = MEMDATA_1.p;
      else
       pdr->P    = MEMDATA_1.p*KoefP[GLCONFIG.EDIZM_T.EdIzP];   // �।��� ��������, ���/�2
      Vl.Val=pdr->P;
      Vl.Bt[0]=Vl.Bt[0]&0xFE;
      Vl.Bt[0]=Vl.Bt[0] | w;
      pdr->P =  Vl.Val;
            pdr->t    = MEMDATA_1.t;    // �।��� ⥬������, �ࠤ. ������
           if (nfc==0x1B)
            pdr->Vrtot= MEMDATA_1.CO2; // ��騩 ��� � ��, ���.�3;
           else  pdr->Vrtot=0;
            pdr++;

            if (point == endpoint_1)
               break;                   // ����� ���᪠
         }
      }
                                        // ���-�� ����ᥩ
      if (count > Nrec)  count = 0;
      SEND_1.RESPONSEDAILY.NumRecord = count;

      if ((point == endpoint_1) || (count==0) || (point > ARC2-1))
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEDAILY.ResponseStatus = 0;
      else                              // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
         SEND_1.RESPONSEDAILY.ResponseStatus = 1;
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(count*lenrecord+9,nfc+128);
                                        // ������ ���p�
      BufTrans_1[5] = SEND_1.RESPONSEDAILY.NumRecord;
      BufTrans_1[6] = SEND_1.RESPONSEDAILY.ResponseStatus;
      for (k=0; k<count; k++)
       movmem(&SEND_1.RESPONSEDAILY.DAILYRECORD[k],
              &BufTrans_1[7]+k*lenrecord,lenrecord);

      BufTrans_1[4] = 3 & num;          // ����� ��⪨
   }
//    cpystartendtime1(NumKan1);
}*/
//------------------------------------------------------------------------
int NeedVSumm_1(struct reqvalarm *prd) {
   long t1,t2,tc;
   struct bintime      bTmp;

// STARTENDTIME_1.STARTTIME.seconds = 0;
// STARTENDTIME_1.STARTTIME.minutes = 0;
                                        // ����p���� ��;
// STARTENDTIME_1.STARTTIME.hours   = GLCONFIG.ContrHour;
// STARTENDTIME_1.STARTTIME.date    = prd->StartDay;
// STARTENDTIME_1.STARTTIME.month   = prd->StartMonth;
// STARTENDTIME_1.STARTTIME.year    = prd->StartYear;

   bTmp.seconds = 0;
   bTmp.minutes = 0;
   bTmp.hours   = GLCONFIG.ContrHour;

   bTmp.date    = prd->StartDay;
   bTmp.month   = prd->StartMonth;
   bTmp.year    = prd->StartYear;

   t1 = GetContrHSec(bTmp);

// bTmp = STARTENDTIME_1.ENDTIME;         // ������ ��⪨
// t2 = GetContrHSec(bTmp);

   tc = GetContrHSec(BINTIME);          // ⥪�騥 ��⪨

// if (t1 <= tc && tc <= t2)            // �᫨ �������� � ���ࢠ�
   if (t1 == tc)                        // �᫨ ��⪨ ⥪�騥
      return 1;                         // �㬬��� �뤠��
   else
      return 0;                         // �㬬��� �� �뤠��
}
//------------------------------------------------------------------------
/*                                        // �⥭�� �㬬��� ���਩��� ��ꥬ��
//int RdValarmSumm_1(struct reqvalarm *prd,struct valarmrecord *pdr) {
int RdValarmSumm_1(struct valarmrecord *pdr) {
   unsigned char       num;
   int count;

   num = BufInKadr_1[4] & 3;

// if (NeedVSumm_1(prd) == 1) {           // �᫨ �������� � ���ࢠ�
      outportb(0x1D0,BASE_CFG_PAGE);

      FP_SEG(ValarmSumm) = SEGMENT;         // 㪠��⥫� �� �㬬���� ����. ��ꥬ��
      FP_OFF(ValarmSumm) = 576;
      FP_SEG(ValarmSumm2) = SEGMENT;         // 㪠��⥫� �� �㬬���� ����. ��ꥬ��
      FP_OFF(ValarmSumm2) = 768;
                                        // �����
      pdr->StartMonth = (ValarmSumm+num-1)->STARTTIME.month;
                                        // ����
      pdr->StartDay   = (ValarmSumm+num-1)->STARTTIME.date;

      if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
      || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
         pdr->StartDay |= 0x80;         // ᯮᮡ ����p���� �� ��⪥
                                        // ���
      pdr->StartYear  = (ValarmSumm+num-1)->STARTTIME.year;

                                        // �ਢ������ ��ꥬ ��� ���਩
      pdr->VTotSt = (ValarmSumm+num-1)->VTotSt + (ValarmSumm2+num-1)->VTotSt;
                                        // ࠡ�稩 ��ꥬ ��� ���਩
      pdr->VTotWc = (ValarmSumm+num-1)->VTotWc + (ValarmSumm2+num-1)->VTotWc;
                                        // �ਢ������ ��ꥬ �����⥫��� ���਩
      pdr->VAddSt = (ValarmSumm+num-1)->VAddSt + (ValarmSumm2+num-1)->VAddSt;

                                        // ���⥫쭮��� ��� ���਩
      pdr->TTot   = (ValarmSumm+num-1)->TTot + (ValarmSumm2+num-1)->TTot;
                                        // ���⥫쭮��� �⪫�祭�� ��⠭��
      pdr->Toff   = (ValarmSumm+num-1)->Toff + (ValarmSumm2+num-1)->Toff;
                                        // ���⥫쭮��� �����⥫��� ���਩
      pdr->Tqmin  = (ValarmSumm+num-1)->Tqmin + (ValarmSumm2+num-1)->Tqmin;

      count = 1;
                                        // ���-�� ����ᥩ
      SEND_1.RESPONSEVALARM.NumRecord = 1;

      SEND_1.RESPONSEVALARM.ResponseStatus = 0;
                                        // ������ �p�䨪�
      SaveMessagePrefix_1(count*sizeof(struct valarmrecord)+9,0x8C);
                                        // ������ ���p�
      movmem(&SEND_1.RESPONSEVALARM,&BufTrans_1[5],count*sizeof(struct valarmrecord)+2);

      BufTrans_1[4] = 3 & num;            // ����p ��⪨

      return 1;
}
*/
//---------------------------------------------------------------------------

void RdValarm_1() {                       // �⥭�� ���਩��� ��ꥬ��
portC=1;
RdValarmp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1,&ValarmData_1);
}
/*
   static unsigned int startpoint_1,endpoint_1;
   struct bintime      *pbtime;
   unsigned char       num;
   unsigned char       page;
   unsigned int        point;
   int                 err;
   int                 count; //,RecLen
   struct reqvalarm    *prd;            // ��p���p� ���p�� - �⥭�� ���਩��� ��ꥬ��
   struct valarmrecord *pdr;            // ��p���p� ����� ������ ��� ��p����

//   RecLen = sizeof(valarm_data);
   pdr = &SEND_1.RESPONSEVALARM.VALARMRECORD[0];
   err = 0;

   if (BufInKadr_1[2]-sizeof(reqvalarm) != 7)
      err |= 1;                         // ����p��� ����� ���p��

   num = BufInKadr_1[4] & 3;

   if ((num & 3) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   prd = &RESIV_1.REQVALARM;
                                        // ��p������� ��p����p�� ���p��
   memmove(prd,&BufInKadr_1[5],sizeof(reqvalarm));

// if (LastRecFlag_1 == 1) {              // ��� �뤠�� �㬬���
//    RdValarmSumm_1(prd,pdr);
//    LastRecFlag_1 = 0;                  // �뤠�� ��᫥���� ������ �� ��娢�
// }
// else {                               //
   if (NeedVSumm_1(prd) == 1) {              // ��砫쭠� ��� ⥪���
      RdValarmSumm_1(pdr);
//    LastRecFlag = 0;                  // �뤠�� ��᫥���� ������ �� ��娢�
   }
   else {                               //
      if (prd->RequestNum != 0) {
                                           // ������� �०��� �⢥� �� ������ ���㫥�� �����
         if (STARTENDTIME_1.RequestNum == prd->RequestNum)
            return;

         if (STARTENDTIME_1.RequestNum +1 != prd->RequestNum)
            err |= 0x04;                   // ����p��� ��᫥����⥫쭮�� ���p�ᮢ
      }
                                           // ����� �����
      STARTENDTIME_1.RequestNum = prd->RequestNum;
      STARTENDTIME_1.num = 3 & (num-1);      // ����� ��⪨

      if (STARTENDTIME_1.RequestNum == 0) {  // ��p���⪠ 1-�� ���p��
         pbtime = &STARTENDTIME_1.STARTTIME;
         pbtime->seconds = 0;
         pbtime->minutes = 0;
                                           // ����p���� ��;
         pbtime->hours   = GLCONFIG.ContrHour;
         pbtime->date    = prd->StartDay;
         pbtime->month   = prd->StartMonth;
         pbtime->year    = prd->StartYear;

         if (Mem_TestReqTime_1(pbtime))
            err |= 8;                      // ����p��� �p����� ���p��

         pbtime = &STARTENDTIME_1.ENDTIME;
         pbtime->seconds = 59;
         pbtime->minutes = 59;
                                           // ����p���� ��;
         pbtime->hours   = (GLCONFIG.ContrHour) ? GLCONFIG.ContrHour-1 : 23;

         if (GLCONFIG.ContrHour)
            dayPlus(prd->EndDay,prd->EndMonth,prd->EndYear,
                   pbtime->date,pbtime->month,pbtime->year);
         else {
            pbtime->date    = prd->EndDay;
            pbtime->month   = prd->EndMonth;
            pbtime->year    = prd->EndYear;
         }

         if (Mem_TestReqTime_1(pbtime))
            err |= 0x10;                   // ����p��� �p����� ���p��

         if (Mem_TestReqTimeSE_1())
            err |= 0x20;                   // ����p��� ᮮ⭮襭�� �p���� ���p��
                                           // �������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
         if (err == 0) {                   // ����� ����
            page = GetValarmPage(num-1);
                                           // ��砫�� ������
            startpoint_1 = ValarmDataSearchStart2_1(page);
                                           // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
            if (startpoint_1 < V_ALARM) {    // ����� ����
               SetBinTimeV1(page,startpoint_1,&BinTmp_1,&BinTmp2_1);
            }
                                           // ��������� < ��砫� ��娢�
            if ((startpoint_1 > V_ALARM) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
               startpoint_1 = ValarmDataSearchStart_1(page); // ��砫�� ������
            else {
               startpoint_1++;               // �த������ ��������� 㪠��⥫� �� 1
               if (startpoint_1 == V_ALARM)
                  startpoint_1 = 0;
            }

            if (Mem_TestReqTimeEq_1() == 0)
               endpoint_1 = startpoint_1;
            else                           // ������ ������
               endpoint_1 = ValarmDataSearchEnd_1(page);

            if ((startpoint_1 > V_ALARM-1) || (endpoint_1 > V_ALARM-1))
               err |= 0x40;
         }
         if (err == 0) {
            ValarmDataRd_1(startpoint_1,page); // �⥭�� 1-� �����

            if (Mem_VTestReqTimeEnd_1())
               err |= 0x80;
         }
      }                                    // ����� ��p���⪨ ��p���� ���p��


      if (err) {
         if (err & 0xC0) {                 // ��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
//          if (RdValarmSumm_1(prd,pdr) == 0) {
               SaveMessagePrefix_1(9,0x8C);     // ������ �p�䨪�
               BufTrans_1[4] = 3 & num;         // ����p ��⪨
               BufTrans_1[5] = 0;               // ���-�� ����ᥩ � �⢥�
               BufTrans_1[6] = 0;               // �p��������� ���
//          }
         }
         else
            SaveMessagePrefix_1(6,0xFF);     // �訡�� ��p����p�� ���p��
      }
      else {                               // �訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
                                           // ����� ����
         page = GetValarmPage(num-1);
         point = NextIndex_1Valarm(startpoint_1,9*STARTENDTIME_1.RequestNum,page);
         count = 0;

         if (point < V_ALARM) {            // ���� ����� � ⠪�� ᬥ饭���
            for (int i=0;i<9;i++) {
               count++;                    // ���-�� ����ᥩ
               if (i) {
                  point = NextIndex_1Valarm(point,1,page);
                  if (point > V_ALARM-1)
                     break;                // ����� ���᪠
               }
                                           // �⥭�� ����� ���਩��� ��ꥬ��
               ValarmDataRd_1(point,page);
               pbtime = &ValarmData_1.STARTTIME;
                                           // �����
               pdr->StartMonth   = pbtime->month;
                                           // ����
               pdr->StartDay     = pbtime->date;

               if ((GLCONFIG.IDRUN[num-1].TypeRun) == 4 || (GLCONFIG.IDRUN[num-1].TypeRun) == 5
               || (GLCONFIG.IDRUN[num-1].TypeRun) == 10)
                  pdr->StartDay |= 0x80;   // ᯮᮡ ����p���� �� ��⪥
                                           // ���
               pdr->StartYear     = pbtime->year;

                                           // �ਢ������ ��ꥬ ��� ���਩
               pdr->VTotSt = ValarmData_1.VTotSt;
                                           // ࠡ�稩 ��ꥬ ��� ���਩
               pdr->VTotWc = ValarmData_1.VTotWc;
                                           // �ਢ������ ���������� j��ꥬ
               pdr->VAddSt = ValarmData_1.VAddSt;

                                           // ���⥫쭮��� ��� ���਩
               pdr->TTot   = ValarmData_1.TTot;
                                           // ���⥫쭮��� �⪫�祭�� ��⠭��
               pdr->Toff   = ValarmData_1.Toff;
                                           // ���⥫쭮��� ࠡ��� �� ���.���祭���
               pdr->Tqmin  = ValarmData_1.Tqmin;

               pdr++;
               if (point == endpoint_1)
                  break;                   // ����� ���᪠
            }
         }
                                           // ���-�� ����ᥩ
      if (count > Nrec)  count = 0;
         SEND_1.RESPONSEVALARM.NumRecord = count;

         if ((point == endpoint_1)||(count==0)||(point > V_ALARM-1)) {
                                        // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
//          if (NeedVSumm_1(prd) == 1) {
//             SEND_1.RESPONSEVALARM.ResponseStatus = 1;
//             LastRecFlag_1 = 1;          // �뤠�� ��᫥���� ������ �� ��娢�
//          }
//          else {                          // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
               SEND_1.RESPONSEVALARM.ResponseStatus = 0;
//             LastRecFlag_1 = 0;         // �뤠�� ��᫥���� ������ �� ��娢�
//          }
         }                               // ������ �p�䨪�
         else                           // ����� �⢥� (0= �����襭�, 1= ���� �� �����)
            SEND_1.RESPONSEVALARM.ResponseStatus = 1;

         SaveMessagePrefix_1(count*sizeof(struct valarmrecord)+9,0x8C);
                                           // ������ ���p�
         movmem(&SEND_1.RESPONSEVALARM,&BufTrans_1[5],count*sizeof(struct valarmrecord)+2);

         BufTrans_1[4] = 3 & num;            // ����p ��⪨
      }
   }
}
*/
//---------------------------------------------------------------------------
void AnswSys_1() {                      // �⥭�� ��⥬��� ��p����p��
 AnswSysp(BufInKadr_1,BufTrans_1);
}
/*
   struct sys        *psys;

   psys = &SEND_1.SYS;

   psys->Period    = GLCONFIG.Period;   // ���p��� ���������� � ������
   psys->ContrHour = GLCONFIG.ContrHour;// ����p���� ��

                                        // �����
   psys->SummerMonth = GLCONFIG.SummerMonth;
   psys->SummerDay = GLCONFIG.SummerDay;// ����
                                        // �� ��p�室� �� ��⭥� �p���
   psys->SummerHours = GLCONFIG.SummerHours;
                                        // �����
   psys->WinterMonth = GLCONFIG.WinterMonth;
                                        // ����
   psys->WinterDay = GLCONFIG.WinterDay;
                                        // �� ��p�室� �� ������ �p���
   psys->WinterHours = GLCONFIG.WinterHours;

   psys->vol_odor = GLCONFIG.vol_odor;  // ��ꥬ ��� ���ਧ���, �3/���
   psys->termQmax = GLCONFIG.termQmax;  // ���ᨬ���� ��ꥬ ��� �����ॢ�, �3/��

   psys->PreambNum = GLCONFIG.PreambNum;// �᫮ �ॠ��� COM2
                                        // �᫮ �ॠ��� COM1
   psys->PreambNum_1 = GLCONFIG.PreambNum_1;

                                        // ������ �p�䨪�
   SaveMessagePrefix_1(sizeof(sys)+6,0xA2);
                                        // ������ ���p�
   movmem(&SEND_1,&BufTrans_1[4],sizeof(sys));
}
*/
//---------------------------------------------------------------------------
void WrSys_1() {                        // ������ ��⥬��� ��p����p��
 WrSysp(BufInKadr_1,BufTrans_1,1);
}
/*
   struct sys        *psys;
   int               err;
   unsigned char     *pchar;


   err = 0;

//   if (Nwr1 == 0) err |= 0x80;          // 䫠� ����� �������

   psys = &RESIV_1.SYS;

//   if (memcmp(&BufInKadr_1[4],GLCONFIG.PASSWORD,16))
    if (memcmp(&BufInKadr_1[4],PASSWR1,14))
      err |= 1;
   else {                               // �⥭�� �p������ ��p����p��
      movmem(&BufInKadr_1[4+16],&RESIV_1,sizeof(sys));
      if (GLCONFIG.Period != psys->Period) {
         MEMAUDIT_1.type     = 32;
         *(unsigned long*)&MEMAUDIT_1.altvalue = (unsigned long)GLCONFIG.Period;
                                        // ���p��� ���������� � ������
         GLCONFIG.Period   = psys->Period;
         *(unsigned long*)&MEMAUDIT_1.newvalue = (unsigned long)GLCONFIG.Period;
                                        // ������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(0));
         Mem_AuditWr_1(Mem_GetAuditPage(1));
         Mem_AuditWr_1(Mem_GetAuditPage(2));
      }

      if (GLCONFIG.ContrHour != psys->ContrHour) {
         MEMAUDIT_1.type     = 131;
         *(unsigned long*)&MEMAUDIT_1.altvalue = (unsigned long)GLCONFIG.ContrHour;
                                        // ����p���� ��
         GLCONFIG.ContrHour = psys->ContrHour;
         *(unsigned long*)&MEMAUDIT_1.newvalue = (unsigned long)GLCONFIG.ContrHour;
                                        // ������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(0));
         Mem_AuditWr_1(Mem_GetAuditPage(1));
         Mem_AuditWr_1(Mem_GetAuditPage(2));
      }

      if (BufInKadr_1[2] > 30) {        // 㤫������ �����

         if (GLCONFIG.vol_odor != psys->vol_odor) {
            MEMAUDIT_1.type = 157;
            *(float*)&MEMAUDIT_1.altvalue = GLCONFIG.vol_odor;
                                        // ��ꥬ ��� ���ਧ���, �3/���
            GLCONFIG.vol_odor = psys->vol_odor;
            *(float*)&MEMAUDIT_1.newvalue = GLCONFIG.vol_odor;
                                        // ������ ���� ����⥫���
            Mem_AuditWr_1(Mem_GetAuditPage(0));
            Mem_AuditWr_1(Mem_GetAuditPage(1));
            Mem_AuditWr_1(Mem_GetAuditPage(2));
         }

         if (GLCONFIG.termQmax != psys->termQmax) {
            MEMAUDIT_1.type = 158;
            *(float*)&MEMAUDIT_1.altvalue = GLCONFIG.termQmax;
                                        // ���ᨬ���� ��ꥬ ��� �����ॢ�
            GLCONFIG.termQmax = psys->termQmax;
            *(float*)&MEMAUDIT_1.newvalue = GLCONFIG.termQmax;
                                        // ������ ���� ����⥫���
            Mem_AuditWr_1(Mem_GetAuditPage(0));
            Mem_AuditWr_1(Mem_GetAuditPage(1));
            Mem_AuditWr_1(Mem_GetAuditPage(2));
         }

         if (GLCONFIG.PreambNum != psys->PreambNum) {
            MEMAUDIT_1.type = 30;
            *(float*)&MEMAUDIT_1.altvalue = GLCONFIG.PreambNum;
                                        // �᫮ �ॠ��� �� ���2
            GLCONFIG.PreambNum = psys->PreambNum;
            *(float*)&MEMAUDIT_1.newvalue = GLCONFIG.PreambNum;
                                        // ������ ���� ����⥫���
            Mem_AuditWr_1(Mem_GetAuditPage(0));
            Mem_AuditWr_1(Mem_GetAuditPage(1));
            Mem_AuditWr_1(Mem_GetAuditPage(2));
         }

         if (GLCONFIG.PreambNum_1 != psys->PreambNum_1) {
            MEMAUDIT_1.type = 30;
            *(float*)&MEMAUDIT_1.altvalue = GLCONFIG.PreambNum_1;
                                        // �᫮ �ॠ��� �� ���1
            GLCONFIG.PreambNum_1 = psys->PreambNum_1;
            *(float*)&MEMAUDIT_1.newvalue = GLCONFIG.PreambNum_1;
                                        // ������ ���� ����⥫���
            Mem_AuditWr_1(Mem_GetAuditPage(0));
            Mem_AuditWr_1(Mem_GetAuditPage(1));
            Mem_AuditWr_1(Mem_GetAuditPage(2));
         }
      }                                 // ����� ��ࠡ�⪨ 㤫������� ���

      if ((GLCONFIG.SummerMonth != psys->SummerMonth) || (GLCONFIG.SummerDay != psys->SummerDay) || (GLCONFIG.SummerHours != psys->SummerHours)) {
         MEMAUDIT_1.type     = 28;
         pchar = (unsigned char*)&MEMAUDIT_1.altvalue;
                                        // �����
         *pchar++ = GLCONFIG.SummerMonth;
                                        // ����
         *pchar++ = GetDaySummer();
                                        // �� ��p�室� �� ��⭥� �p���
         *pchar++ = GLCONFIG.SummerHours;
         *pchar   = 0;
                                        // �����
         GLCONFIG.SummerMonth = psys->SummerMonth;
                                        // ����
         GLCONFIG.SummerDay   = psys->SummerDay;
                                        // �� ��p�室� �� ��⭥� �p���
         GLCONFIG.SummerHours = psys->SummerHours;

                                        // ��࠭��� ���� ������ ���室� �� ��⭥� �६�
         SetSummer(BINTIME.year, psys->SummerMonth, psys->SummerDay);

         pchar = (unsigned char*)&MEMAUDIT_1.newvalue;
                                        // �����
         *pchar++ = GLCONFIG.SummerMonth;
                                        // ����
         *pchar++ = GetDaySummer();
                                        // �� ��p�室� �� ��⭥� �p���
         *pchar++ = GLCONFIG.SummerHours;
         *pchar   = 0;
                                        // ������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(0));
         Mem_AuditWr_1(Mem_GetAuditPage(1));
         Mem_AuditWr_1(Mem_GetAuditPage(2));
      }

      if ((GLCONFIG.WinterMonth != psys->WinterMonth) || (GLCONFIG.WinterDay != psys->WinterDay) || (GLCONFIG.WinterHours != psys->WinterHours)) {
         MEMAUDIT_1.type     = 29;
         pchar = (unsigned char*)&MEMAUDIT_1.altvalue;
                                        // �����
         *pchar++ = GLCONFIG.WinterMonth;
         *pchar++ = GetDayWinter();     // ����
                                        // �� ��p�室� �� ������ �p���
         *pchar++ = GLCONFIG.WinterHours;
         *pchar   = 0;
                                        // �����
         GLCONFIG.WinterMonth = psys->WinterMonth;
                                        // ����
         GLCONFIG.WinterDay   = psys->WinterDay;
                                        // �� ��p�室� �� ������ �p���
         GLCONFIG.WinterHours = psys->WinterHours;

                                        // ��࠭��� ���� ������ ���室� �� ������ �६�
         SetWinter(BINTIME.year, psys->WinterMonth, psys->WinterDay);

         pchar = (unsigned char*)&MEMAUDIT_1.newvalue;
                                        // �����
         *pchar++ = GLCONFIG.WinterMonth;
         *pchar++ = GetDayWinter();     // ����
                                        // �� ��p�室� �� ������ �p���
         *pchar++ = GLCONFIG.WinterHours;
         *pchar   = 0;
         Mem_AuditWr_1(Mem_GetAuditPage(0));//������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(1));//������ ���� ����⥫���
         Mem_AuditWr_1(Mem_GetAuditPage(2));//������ ���� ����⥫���
      }
      if (Mem_ConfigWr() != 0)
         err |= 1;                      // ������ ��p���p� ���䨣�p�樨 ���᫨⥫�
   }

   if (err)
      SaveMessagePrefix_1(6,0xFF);      // ��⥬�� ��p����p� �� ����ᠭ�
   else
      SaveMessagePrefix_1(6,0xA3);      // ����ᠭ�
}
*/
//---------------------------------------------------------------------------
void ComPrint_2m(char *Txt) {
   int i,len;

   len = strlen(Txt);
   memset(BufTrans,0,40);
   movmem(&Txt[0],&BufTrans[0], len);
   SendCount = len;// + 1;
   outindex=0;                        // ᬥ饭�� � ��p��������� ���p�
//  outportb(comport+4,0x0B);               //DTR + RTS + �p�p뢠���
   outportb(comport+4,0x02);               //DTR + RTS + �p�p뢠���
  outportb (comport+1,0x02);    //p��p���� �p�p뢠��� ��p����稪� � ����� �ਥ��
}

//-----------------------------------------------------------------
void Swap4(char *p)
{ char t;
	t = p[0];
	p[0] = p[3];
	p[3] = t;
	t = p[1];
	p[1] = p[2];
	p[2] = t;
}
//-----------------------------------------------------------
void fPassword_1() {    // ���� ����� � ��஫� �㭪�� 0x58
//sprintf(str,"1-Tlogin1=%s Dostup=%d\n\r",Tlogin1,Dostup1);
//ComPrint_2(str);
 fPasswordp(BufInKadr_1,BufTrans_1,Tlogin1,1,&Dostup1,attempt1);
}
/*
   int    err = 0,i,dst,k;
   struct npasword *pas;  // 14 ����
   char *buf1="    \0";
   char nlog[5];
   char u1,u2;
   char far *pENP;

   if (BufInKadr_1[2] != 20)
      err |= 1;                         // ����p��� ����� ���p��
    pas = &RESIV_1.NPASWORD;
    movmem(&BufInKadr_1[4],pas,14); // 14
    movmem(pas->nlogin,nlog,4); nlog[4]='\0';
    Mem_LoginRd();   // �� ��� � LogPassw
    i=0;
    do
     { if (memcmp(pas->nlogin, LogPassw[i].login,4)==0)
       break;
       i++;
     }
     while (i<6);
     dst = 0;
     k=i;
    attempt[k]++;
    if ((attempt[k] < LATT) && (k<6))
   //�஢�ઠ ��஫�
  {
   u1 =(strcmp(Tlogin1,nlog)==0);  // Tlogin1 == nlog (��� � �����)
   u2 =(strcmp(Tlogin1,buf1)==0);  // Tlogin1 == '' (����)
   if ((Dostup1 == GLCONFIG.StartDostup1) // ��室��� ���ﭨ�
       &&(strcmp(Tlogin1,buf1)==0)&&(memcmp(pas->nlogin,buf1,4))
       &&(strlen(nlog)==4))  // ��� �� �஡��� � ����� 4 ᨬ����
   {
      for (i=0; i<6; i++)
      { if ((memcmp(pas->nlogin, LogPassw[i].login,4)==0))    //����� ࠢ��
//           if ((i==0)&&(strlen(LogPassw[i].Password)==0) ||
         if ((i==0)&&(LogPassw[i].Password[0]==0x0) ||   //��஫� ����������� = 0000
            (memcmp(pas->npassw, LogPassw[i].Password,10)==0)) // ��஫� ࠢ��� i-�� ��஫�
         { if (i == 0)        // ��� ���� �����������
           {Dostup1 = 7; dst=Dostup1;
            if (LogPassw[i].Password[0]==0x0)
            { outportb(0x1d0,RESERV_CFG_PAGE);       // ��⠭����� ��p����� �����
              pENP = (char far *)MK_FP(SEGMENT,4100);
              memcpy(LogPassw[i].Password,"          ",10);
              _fmemcpy(pENP,(char far *)LogPassw[i].Password,10);
            }
           }
           else      // i > 0
             Dostup1 = LogPassw[i].kodostup;
           strncpy(Tlogin1 ,pas->nlogin,4);
           Tlogin1[4]='\0';
           numlogin1 = i;
           logTimeb1 = BINTIME;//�६� ��砫� ᥠ�� = ⥪�饥 �६�
           dst = Dostup1;
           attempt[i] = 0;
           break;     // ��室 �� 横�� �� i
         }   // ����� if ��஫� ࠢ�� dst > 0
      }  // for (i=0; i<6; i++)
   }   // if ((Dostup1 == GLCONFIG.StartDostup1)
   else
      if (u1 && !u2)  // Tlogin1 �� ����, Tlogin1 == ��� � �����
      { attempt[k]=0;
        if (Dostup1) dst = Dostup1;
        else { Dostup1 = GLCONFIG.StartDostup1;
               strncpy(Tlogin1 ,"    \0",5);
             }
        goto D1;
      }   // ����� �� ᮢ������
      else
        Dostup1 = GLCONFIG.StartDostup1;
//     if ((memcmp(pas->nlogin,buf1,4)!=0) &&   // nlogin != �஡���
//        (memcmp(pas->nlogin,Tlogin1,4)==0)) dst = Dostup1;  // ������ �室
  }  //  if (attempt[k] < LATT)
  else   // �᫮ ����⮪ >=  LATT
   {
     if (attempt[k] >= LATT) err |= 2; //if (attempt[k] >= LATT)
    // �� ����⥫��⢠ - ����⪠ ����㯠 � ������ nlog
     if ((attempt[k] == LATT) && (GLCONFIG.StartDostup1!=3)) //
     {    //����⪠ ��ᠭ�樮��஢������ ����㯠
      strncpy(MEMSECUR_1.login, pas->nlogin,4);
      MEMSECUR_1.TIMEBEG = BINTIME;
      memset(&MEMSECUR_1.TIMEEND,0,6);
      MEMSECUR_1.Port = 1;
      MEMSECUR_1.mode  = 2;
      i = Mem_SecurWr_1(Mem_SecurGetPage(),0);
     }
     if (attempt[6] >= LATT) attempt[6] = 0;
     if (attempt[0] >= LATT) attempt[0] = 0;
    } // else    if (attempt[k] < 3)

   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else
   {
//    if (memcmp(pas->nlogin,buf1,4)!=0)    // nlogin != �஡���
    if (dst > 0)                         // �஢��� ����㯠 > 0
    { strncpy(MEMSECUR_1.login, Tlogin1,4);
      MEMSECUR_1.TIMEBEG = logTimeb1;
      memset(&MEMSECUR_1.TIMEEND,0,6);
      MEMSECUR_1.Port = 1;
      MEMSECUR_1.mode  = 0;
      i = Mem_SecurWr_1(Mem_SecurGetPage(),1);
      outportb(0x1D0,BASE_CFG_PAGE);
      poke(SEGMENT,3620,di1p[0][0]); // ������� EraseConf = 0
      poke(SEGMENT,3622,di2p[0][0]); // ������� EraseConf = 0
      pENP = (char far *)MK_FP(SEGMENT,3608);  //������
      _fmemcpy(pENP,&logTimeb1,6);
       strncpy(PASSWR1,Tlogin1,4);
       strncpy(PASSWR1+4,pas->npassw,10);
       PASSWR1[14]='\0';
      for (i=0; i<6; i++)
      attempt[i] = 0;
     }
//      if (u1 && !u2) dst=Dostup1; //  Tlogin1 == nlog  �  Tlogin1 - �� ����
      if (!u1 && !u2)  // Tlogin1 != ��� � Tlogin1 - �� ����
       {// if ((numlogin1>=0)&&(numlogin1<6))
  //          GornalD_Wr_1(numlogin1);
         Mem_WrEndSeans_1();           // ����� ᥠ��
         Dostup1=GLCONFIG.StartDostup1;
         strncpy(Tlogin1 ,"    \0",5);
         memcpy(PASSWR1,GLCONFIG.PASSWORD,14);
       }
D1:      BufTrans_1[4] = dst; //Dostup1;
      SaveMessagePrefix_1(7,0xD8);
// ������ Tlogin1 � Dostup1 � ���
      outportb(0x1D0,BASE_CFG_PAGE); //  ����� ��࠭���
           // � ������� �� ���
      pENP = (char far *)MK_FP(SEGMENT,16368);
      _fmemcpy(pENP,Tlogin1,4);  //������
      _fmemcpy(pENP+4,&Dostup1,2);
   }  // err ==0
}
*/
//-------------------------------------------------------------------------

void NoDostup_1() {    // �⢥� "��� ����㯠"

      BufTrans_1[4] = 8;
      SaveMessagePrefix_1(7,0xD8);
}

//---------------------------------------------------------------------------
/*
void GornalD_Wr_1(int numl)   //���������� ��ୠ�� ����㯠
{ int i,k,n;
  struct GornalDostup *pad,*pad1;

  n = numl;
  k=Mem_ArhivDRd(&numl); //�⥭�� ��娢� �� ��� � ArhivDostup
                         // k - ������⢮ ��⠭��� ����ᥩ
   if (k < 10)
    { strncpy(ArhivDostup[k].login, Tlogin1,4);
      ArhivDostup[k].TIMEBEG = logTimeb1;
      ArhivDostup[k].TIMEEND = BINTIME;
      ArhivDostup[k].Port = 1;
    } else if (k==10)
    { for (i=0; i<9; i++)
      { pad = &ArhivDostup[i];
        pad1 = &ArhivDostup[i+1];
        movmem(pad1,pad,sizeof(struct GornalDostup));
      }
      k=9;
      strncpy(ArhivDostup[k].login, Tlogin1,4);
      ArhivDostup[k].TIMEBEG = logTimeb1;
      ArhivDostup[k].TIMEEND = BINTIME;
      ArhivDostup[k].Port = 1;
    }
     for (i=k+1; i<10; i++)
     {
      ArhivDostup[i].login[0]='\0';
      ArhivDostup[i].Port = 0;
     }
      Mem_ArhivDWr(n); //������ � ���
}
*/
//---------------------------------------------------------------------------
void fendSeans_1() {    // ����� ᥠ�� �裡 �㭪�� 0x59
  fendSeansp(BufInKadr_1,BufTrans_1,Tlogin1,1,&Dostup1);
}
/*
void fendSeansp(unsigned char* BufInKadr,unsigned char*  BufTrans, unsigned char* Tlogin, char nPort, unsigned int* Dostup)
(
   int    err = 0,num;
   struct SendSeans *es;
   char *buf1="    \0";
//   char Tlog[4];
   char far *pENP;

   if (BufInKadr_1[2] != 10)
      err |= 1;                         // ����p��� ����� ���p��
    es = &RESIV_1.ENDSEANS;
//    movmem(&BufInKadr_1[4],es,sizeof(struct SendSeans)); // 4 ����
//    memcpy(Tlog,Tlogin1,4);
//sprintf(str,"Tlogin1=%s buf=%s\n\r",Tlogin1,buf1);
//ComPrint_2(str);

    if ((memcmp(buf1,Tlogin1,4)))
// �᫨ ⥪��� ��� = ������
       {// if ((numlogin1>=0)&&(numlogin1<6))
 //           GornalD_Wr_1(numlogin1);
         Mem_WrEndSeans_1();
         Dostup1=GLCONFIG.StartDostup1;
         strncpy(Tlogin1 ,"    \0",5);
         memcpy(PASSWR1,GLCONFIG.PASSWORD,14);
// ������ Tlogin1 � Dostup1 � ���
      outportb(0x1D0,BASE_CFG_PAGE); //  ����� ��࠭���
					// � ������� �� ���
      pENP = (char far *)MK_FP(SEGMENT,16368);
      _fmemcpy(pENP,Tlogin1,4);  //������
      _fmemcpy(pENP+4,&Dostup1,2);
       }
    else err |= 2;
   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
        movmem(Tlogin1,&BufTrans_1[4],4);
      SaveMessagePrefix_1(10,0xD9);
        }

}
*/
//---------------------------------------------------------------------------
void fCreateUser_1() {    // ᮧ����� ���짮��⥫�� �㭪�� 0x56
  fCreateUserp(BufInKadr_1,BufTrans_1,Tlogin1,1,&Dostup1);
}
/*
   int   k,i, err = 0;
   struct logpass *lg;

   if (BufInKadr_1[2] != 88)
      err |= 1;                         // ����p��� ����� ���p��
    lg = &RESIV_1.LOGPASS[0];
    movmem(&BufInKadr_1[4],&RESIV_1,5*sizeof(struct logpass));
    GLCONFIG.StartDostup1 = BufInKadr_1[84];
    GLCONFIG.StartDostup = BufInKadr_1[85];
    if (Mem_ConfigWr() != 0)
	 err |= 2;
    Mem_LoginRd();  //� LogPassw
    for (i=0; i<5; i++)
    { lg = &RESIV_1.LOGPASS[i];
      if (lg->write > 0)
	{
	  strncpy(MEMSECUR_1.login, lg->nlogin,4);
	  MEMSECUR_1.TIMEBEG = BINTIME;
	  memset(&MEMSECUR_1.TIMEEND,0,6);
	  MEMSECUR_1.Port = 1;
	  MEMSECUR_1.mode  = 22;
	  k = Mem_SecurWr_2(Mem_SecurGetPage(),0,1);
	  movmem(lg,&LogPassw[i+1],15);
	}
    }
   Mem_LoginWr(); //������ � ��� �� LogPassw
   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SaveMessagePrefix_1(6,0xD6);
        }
}
*/
//---------------------------------------------------------------------------
/*
void fRdArchDost_1() {    // �⥭�� ��娢� �����᭮�� �㭪�� 0x57
   int   j,k,i, err = 0;
   struct  GornalDostup *ad;
   if (BufInKadr_1[2] != 8)
      err |= 1;                         // ����p��� ����� ���p��
      ad = ArhivDostup;
      i = BufInKadr_1[5];
      k=Mem_ArhivDRd(&i);  // ��⠥� k ����ᥩ i-�� ���짮��⥫�
      BufTrans_1[4] = BufInKadr_1[4];
      if ((k==0) || (k > 10))
      { BufTrans_1[5] = 0;
        BufTrans_1[6] = 0;
        SaveMessagePrefix_1(9,0xD7);
        return;
      }
      else
     {
      BufTrans_1[5] = k;
      if (i < 5)
       BufTrans_1[6] = 1; //���� �த�������
      else BufTrans_1[6] = 0;
      j = sizeof(struct  GornalDostup);  // = 17
      movmem(ad,&BufTrans_1[7],k*j);
     }
         if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SaveMessagePrefix_1(j*k+9,0xD7);
        }
}
*/
//---------------------------------------------------------------------------
void fUserRd_1() {    // �⥭�� ���짮��⥫�� �㭪�� 0x60
   int   i, err = 0;
   struct logpass *lg;
   char kD;

   if (BufInKadr_1[2] != 6)
      err |= 1;
                      // ����p��� ����� ���p��
     Mem_LoginRd(); //�⥭��� �� ��� � LogPassw
    movmem(&LogPassw[1],&BufTrans_1[4],5*sizeof(struct logpass));
    BufTrans_1[79] = GLCONFIG.StartDostup1;
    BufTrans_1[80] = GLCONFIG.StartDostup;
   if (err)
      SaveMessagePrefix_1(6,0xFF);        // �訡�� ��p����p�� ���p��
   else {
      SaveMessagePrefix_1(83,0xE0);
        }
}
//---------------------------------------------------------------------------
void RdSecur_1()  //�⥭�� ��p���� ������᭮��
{
portC=1;
RdSecurp(BufInKadr_1,BufTrans_1,&STARTENDTIME_1);
}

/*
{ static unsigned int startpoint_1,endpoint_1;
  struct bintime      *pbtime;
  struct reqsecur     *preq;  //��p���p� ���p��
  unsigned char       num;
  unsigned char       page;
  struct securrecord  *prec;  //��p���p� ������ ��� ��p����
  unsigned int        point;
  int                 count;

  prec = &SEND_1.RESPONSESECUR.SECURRECORD[0];
  int err = 0;
  if(BufInKadr_1[2] - sizeof(reqsecur) != 7) err |= 1;//����p��� ����� ���p��
  num = BufInKadr_1[4];
  if( (3 & num) > GLCONFIG.NumRun) err |= 2;//����p��� ����p� ��⪨
  preq = &RESIV_1.REQSECUR;
  movmem(&BufInKadr_1[5],preq,sizeof(reqsecur));//��p������� ��p����p�� ���p��
  if(preq->RequestNum != 0)
  { if( STARTENDTIME_1.RequestNum +1 != preq->RequestNum)  //����� �����
    err |= 0x04;  //����p��� ��᫥����⥫쭮�� ���p�ᮢ
  }
  STARTENDTIME_1.RequestNum = preq->RequestNum; //����� �����
  STARTENDTIME_1.num = 3 & (num-1);             //����� ��⪨

  if(STARTENDTIME_1.RequestNum == 0)  //��p���⪠ 1-�� ���p��
  { pbtime = &STARTENDTIME_1.STARTTIME;
    pbtime->seconds = preq->StartSeconds;
    pbtime->minutes = preq->StartMinutes;
    pbtime->hours   = preq->StartHours;
    pbtime->date    = preq->StartDay;
    pbtime->month   = preq->StartMonth;
    pbtime->year    = preq->StartYear;
    if( Mem_TestReqTime_1(pbtime) ) err |= 8;//����p��� �p����� ���p��
    pbtime = &STARTENDTIME_1.ENDTIME;
    pbtime->seconds = preq->EndSeconds;
    pbtime->minutes = preq->EndMinutes;
    pbtime->hours   = preq->EndHours;
    pbtime->date    = preq->EndDay;
    pbtime->month   = preq->EndMonth;
    pbtime->year    = preq->EndYear;
    if( Mem_TestReqTime_1(pbtime) ) err |= 0x10;//����p��� �p����� ���p��
    if( Mem_TestReqTimeSE_1() ) err |= 0x20;//����p��� ᮮ⭮襭�� �p���� ���p��
//�������� �訡�� �� 1 �� 0x20 - �訡�� ���p��
    if ( err == 0 )
    { page = Mem_SecurGetPage();//��p���� ��p���� ���p��
      startpoint_1  = Mem_SecurSearchStart2_1(page);   // ��砫�� ������
                        // �᫨ �訡�� ��� ��������� ��� ����� ��砫쭮�
       if (startpoint_1 < ARCH_SECURITY) {  // ����� ����
          int tInd,tPoint;
// ��࠭�� � Mem_SecurSearchStart2_1(page)
          tInd = startpoint_1*LEN_SECURITY+BEGADDR_SECURITY;
          BinTmp_1.year    = peekb(SEGMENT+0x10,tInd+9);
          BinTmp_1.month   = peekb(SEGMENT+0x10,tInd+8);
          BinTmp_1.date    = peekb(SEGMENT+0x10,tInd+7);
          BinTmp_1.hours   = peekb(SEGMENT+0x10,tInd+6);
          BinTmp_1.minutes = peekb(SEGMENT+0x10,tInd+5);
          BinTmp_1.seconds = peekb(SEGMENT+0x10,tInd+4);

          tPoint = peek(SEGMENT,BEGADDR_SECURITY);
          tInd = tPoint*LEN_SECURITY+BEGADDR_SECURITY;
          BinTmp2_1.year    = peekb(SEGMENT+0x10,tInd+9);
          BinTmp2_1.month   = peekb(SEGMENT+0x10,tInd+8);
          BinTmp2_1.date    = peekb(SEGMENT+0x10,tInd+7);
          BinTmp2_1.hours   = peekb(SEGMENT+0x10,tInd+6);
          BinTmp2_1.minutes = peekb(SEGMENT+0x10,tInd+5);
          BinTmp2_1.seconds = peekb(SEGMENT+0x10,tInd+4);
       }
                                       // ��������� < ��砫� ��娢�
       if ((startpoint_1 > ARCH_SECURITY) || (BinToSec(BinTmp_1) < BinToSec(BinTmp2_1)))
          startpoint_1 = Mem_SecurSearchStart_1(page); // ��砫�� ������
       else {
          startpoint_1++;                // �த������ ��������� 㪠��⥫� �� 1
          if (startpoint_1 == ARCH_SECURITY)
             startpoint_1 = 0;
       }

        if( Mem_TestReqTimeEq_1() == 0) endpoint_1 = startpoint_1;
        else endpoint_1 = Mem_SecurSearchEnd_1(page); //������ ������
//printf("startpoint_1=%d endpoint_1=%d\n",startpoint_1, endpoint_1);
      if((startpoint_1 > (ARCH_SECURITY-1)) || (endpoint_1 > (ARCH_SECURITY-1))) err |= 0x40;
    }
    if ( err == 0 )
    { Mem_SecurRd_1(startpoint_1,page); //�⥭�� 1-� ����� ���� ���p��
//����p��� ᮮ⭮襭�� �p���� ���p�� � �p娢�
      if( Mem_SecurTestReqTimeSAER_1() ) err |= 0x80;
      else
      if (Mem_SecurTestReqTimeSABR_1())  err |= 0x80;
    }
  }//����� ��p���⪨ ��p���� ���p��

  if(err)
  { if( err & 0xC0 ) //��� ����ᥩ � ���p�訢���� �p������ - �� 1-�� ���p���
    { SaveMessagePrefix_1(9,0xE1);  //������ �p�䨪�
      BufTrans_1[4] = 3 & num;      //����p ��⪨
      BufTrans_1[5] = 0;            //���-�� ����ᥩ � �⢥�
      BufTrans_1[6] = 0;            //�p��������� ���
    }
    else  SaveMessagePrefix_1(6,0xFF);  //�訡�� ��p����p�� ���p��
  }
  else  //�訡�� � ���p��� ��� - ��p���⪠ ��� ���p�ᮢ
  { page  = Mem_SecurGetPage();//��p���� ��p����
    point = Mem_SecurNextIndex_1(startpoint_1, 13*STARTENDTIME_1.RequestNum, page);
    count = 0;
    if( point < ARCH_SECURITY )  //���� ����� � ⠪�� ᬥ饭���
    { for(int i=0;i<13;i++)
      { count++;  //�p������� ��p�������
        if(i)
        { point = Mem_SecurNextIndex_1(point,1,page);
          if(point > (ARCH_SECURITY-1)) break;     //����� ���᪠
        }
        Mem_SecurRd_1(point,page);    //�⥭�� ����� ����
        pbtime = &MEMSECUR_1.TIMEBEG;
        prec->StartMonth   = pbtime->month;     //�����
        prec->StartDay     = pbtime->date;      //����
        prec->StartYear    = pbtime->year;      //���
        prec->StartHours   = pbtime->hours;     //��
        prec->StartMinutes = pbtime->minutes;   //�����
        prec->StartSeconds = pbtime->seconds;   //ᥪ㭤�

        pbtime = &MEMSECUR_1.TIMEEND;
        prec->EndMonth   =  pbtime->month;
        prec->EndDay   =  pbtime->date;
        prec->EndYear    = pbtime->year;      //���
        prec->EndHours   = pbtime->hours;     //��
        prec->EndMinutes = pbtime->minutes;   //�����
        prec->EndSeconds = pbtime->seconds;   //ᥪ㭤�
        prec->mode       = MEMSECUR_1.mode;
        prec->ncomport      =  MEMSECUR_1.Port;
        memcpy(prec->nlogin, MEMSECUR_1.login,4);
        prec++;
        if(point == endpoint_1) break;  //����� ���᪠
      }
    }
      if (count > Nrec)  count = 0;
      SEND_1.RESPONSESECUR.NumRecord = count; //���-�� ����ᥩ
      if((point == endpoint_1)||(count==0)||(point > (ARCH_SECURITY-1))) SEND_1.RESPONSESECUR.ResponseStatus = 0;  //����� �⢥� (0= �����襭�, 1= ���� �� �����)
      else SEND_1.RESPONSESECUR.ResponseStatus = 1;  //����� �⢥� (0= �����襭�, 1= ���� �� �����)
      SaveMessagePrefix_1(count*sizeof(struct securrecord)+9,0xE1);//������ �p�䨪�
      movmem(&SEND_1.RESPONSESECUR,&BufTrans_1[5],count*sizeof(struct securrecord)+2); //������ ���p�
      BufTrans_1[4] = 3 & num;                //����p ��⪨
  }
}
*/
//---------------------------------------------------------------------------
void SD_MgnSend_1() {   // �뤠� ���������� ������ �� SD-�����
   long                  nbl;
   char                  count;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������
   char str[40];

   prc = &RESIV_1.REQCYKLSD;
   if (!BufInKadr_1[5])   // ���� �����, ��砫�� ����
    { count = 0;
      nbl = Search_SD_blokmg(nRunSD1, prc->StartYear,prc->StartMonth,
   prc->StartDay,prc->StartHours,prc->StartMinutes,prc->StartSeconds,
   prc->EndYear,prc->EndMonth,
   prc->EndDay,prc->EndHours,prc->EndMinutes,prc->EndSeconds);
//      nbl ������ ���� ࠢ�� -1 (ARCMBL), �᫨ �� ���� �� ������
      if ((Ble < 0) || (Ble >= ARCMBL)) Ble = nbl;
//printf("mgn-nbl=%ld Ble=%ld\n",nbl,Ble);
//sprintf(str,"mgn-nbl=%ld Ble=%ld\n",nbl,Ble);
//ComPrint_2(str);
      if ((nbl >= 0)&&(nbl < ARCMBL))
       count = Read_SD_blokmg(nRunSD1,nbl);
     }
   else
   { nbl = prc->RequestNum;      //�� ���� �����
     if (nbl <  (ARCMBL-1)) nbl++; else nbl=0;
     nbl = BlankBlok(nbl,nRunSD1);
//      nbl = NextBlokmg(nbl);
     count = 0;
     if ((nbl >= 0)&&(nbl < ARCMBL))
      count = Read_SD_blokmg(nRunSD1,nbl);  //count==0, �᫨ ��᫥���� ����, ���� count==1
   }
//printf("nbl=%ld c=%d\n",nbl,count);
//sprintf(str,"nbl=%ld c=%d\n",nbl,count);
//ComPrint_2(str);
   if ((nbl >= 0)&&(nbl < ARCMBL))
    {
     BufTrans_1[5] = count; *(long*)(BufTrans_1+6) = nbl;
     movmem(mdatar,&BufTrans_1[12],512);
     BufTrans_1[4] = 3 & (nRunSD1+1);                //����p ��⪨
     SaveMessagePrefix_1(12,0xA0);     // ������ �p�䨪�
//sprintf(str,"BufTrans_1=%02X %02X %02X\n\r",BufTrans_1[4],BufTrans_1[5],BufTrans_1[6]);
//ComPrint_2(str);

    }
   else
   {
         SaveMessagePrefix_1(9,0xA0);     // ������ �p�䨪�
         BufTrans_1[4] = 3 & (nRunSD1+1);         // ����p ��⪨
         BufTrans_1[5] = 0;               // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;               // �p��������� ���
   }
   SDMgn1 = 0;
   CheckBufTransIniSend_1();
   TimeReq1=0;// ��� ⠩��� ���� TimeReq1=0;

//printf("%d ",counttime);
}
//---------------------------------------------------------------------------
void SD_RdCykl_1() {                    // �⥭�� 横���᪨� ������
   struct bintime        *pbintime;
   unsigned char         num,nR,nRun;
   unsigned char         page,bSD;
   unsigned int          point;
   int                   err;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������
   struct cyklrecordSD     *pcr;          // ��p���p� ����� ������ ��� ��p����
   int                    sizepc, Nrec;
   long                  nbl;
   char                  count;

   err = 0;

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   pcr = &SEND_1.RESPONSECYKLSD.CYKLRECORD[0];

   prc = &RESIV_1.REQCYKLSD;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[6],prc,sizeof(struct reqcyklSD));
   if (err)
   {  SaveMessagePrefix_1(6,0xFF);     // �訡�� ��p����p�� ���p��
      return;
   }                                  // ����� �����
     nRunSD1 = 3 & (num-1);      // ����� ��⪨
     nRun = Time_GetNumrun();
   switch(GLCONFIG.NumRun)
   {
   case 1: bSD=((LongQueue[3]) ||  (counttime > 37));
        break;
   case 2: bSD=((LongQueue[3]) ||  (counttime > (20*nRun+17)));
        break;
   case 3: nR = (nRun==2) ? 1:nRun;
           bSD=((LongQueue[3]) ||  (counttime > (20*nR+17)));
        break;
   }
//printf("Mgn2=%d %d %d\n",counttime,nRun,bSD);
   if (bSD)
      SDMgn1 = 1;
    else
      InQueue(3,SD_MgnSend_1);
}
//------------------------------------------------------------
void SD_PerSend_1() {   // �⥭�� ��ਮ���᪨� ������ �� SD-�����
   long                  nbl;
   char                  count;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� 横���᪨� ������


   prc = &RESIV_1.REQCYKLSD;
   if (!BufInKadr_1[5])   // ���� �����, ��砫�� ����
    { count = 0;
      nbl = Search_SD_blokP(nRunSD1, prc->StartYear,prc->StartMonth,
   prc->StartDay,prc->StartHours,prc->StartMinutes,prc->StartSeconds,
   prc->EndYear,prc->EndMonth,
   prc->EndDay,prc->EndHours,prc->EndMinutes,prc->EndSeconds);
//      nbl ������ ���� ࠢ�� -1 (ARCPBL), �᫨ �� ���� �� ������
      if ((Ble < 0) || (Ble >= ARCPBL)) Ble = nbl;
//printf("per-nbl=%ld Ble=%ld\n",nbl,Ble);
      if ((nbl >= 0)&&(nbl < ARCPBL))
       count = Read_SD_blokP(nRunSD1,nbl);
     }
   else
   { nbl = prc->RequestNum;      //�� ���� �����
     if (nbl <  (ARCPBL-1)) nbl++; else nbl=0;
     nbl = BlankBlokP(nbl,nRunSD1);
     count = 0;
     if ((nbl >= 0)&&(nbl < ARCPBL))
      count = Read_SD_blokP(nRunSD1,nbl);  //count==0, �᫨ ��᫥���� ����, ���� count==1
   }
//printf("nbl=%ld c=%d\n",nbl,count);

   if ((nbl >= 0)&&(nbl < ARCPBL))
    {
     BufTrans_1[5] = count; *(long*)(BufTrans_1+6) = nbl;
     movmem(mdatar,&BufTrans_1[12],512);
     BufTrans_1[4] = nRunSD1+1;                //����p ��⪨
     SaveMessagePrefix_1(12,0xA1);     // ������ �p�䨪�
    }
   else
   {
         SaveMessagePrefix_1(9,0xA1);     // ������ �p�䨪�
         BufTrans_1[4] = nRunSD1+1;         // ����p ��⪨
         BufTrans_1[5] = 0;               // ���-�� ����ᥩ � �⢥�
         BufTrans_1[6] = 0;               // �p��������� ���
   }
   SDPer1 = 0;
   CheckBufTransIniSend_1();
   TimeReq1=0;// ��� ⠩��� ���� TimeReq=0;
}
//---------------------------------------------------------------
void SD_RdPeriod_1() {                    // �⥭�� ��ਮ���᪨� ������
   struct bintime        *pbintime;
   unsigned char         num,nR,nRun;
   unsigned char         bSD,page;
   unsigned int          point;
   int                   err;
   struct reqcyklSD        *prc;          // ��p���p� ���p�� - �⥭�� ��ਮ���᪨� � ����� ������
//   struct periodrecordSD   *pcr;          // ��p���p� ����� ������ ��� ��p���� ��ਮ���᪨�

   int                    sizepc, Nrec;
   long                  nbl;
   char                  count;

   err = 0;

   num = BufInKadr_1[4];

   if ((3 & num) > GLCONFIG.NumRun)
      err |= 2;                         // ����p��� ����p� ��⪨

   prc = &RESIV_1.REQCYKLSD;
                                        // ��p������� ��p����p�� ���p��
   movmem(&BufInKadr_1[6],prc,sizeof(struct reqcyklSD));
   if (err)
   {  SaveMessagePrefix_1(6,0xFF);     // �訡�� ��p����p�� ���p��
      return;
   }                                  // ����� �����
     nRunSD1 = 3 & (num-1);      // ����� ��⪨
     nRun = Time_GetNumrun();
   switch(GLCONFIG.NumRun)
   {
   case 1: bSD=((LongQueue[3]) ||  (counttime > 35));
        break;
   case 2: bSD=((LongQueue[3]) ||  (counttime > (20*nRun+15)));
        break;
   case 3: nR = (nRun==2) ? 1:nR;
           bSD=((LongQueue[3]) ||  (counttime > (20*nR+15)));
        break;
   }
//printf("Per2=%d %d %d\n",counttime,nRun,bSD);
   if (bSD)
      SDPer1 = 1;
    else
      InQueue(3,SD_PerSend_1);

}
//--------------------------------------------------------------
/*
void cpystartendtime1(char NumKan)
{   if (NumKan==2)
    memcpy(&STARTENDTIME_12K,&STARTENDTIME_1,sizeof(startendtime));
   else
   if (NumKan==1)
   memcpy(&STARTENDTIME_11K,&STARTENDTIME_1,sizeof(startendtime));
}
*/