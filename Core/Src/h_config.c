#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "main.h"
#include "h_main.h"
#include "h_calc.h"
#include "h_mem.h"
//#include "comm_str.h"

//#define PAGE_SIZE 16384
//#define SEGMENT 0xF800

extern struct I2C_HandleTypeDef hi2c1;
extern struct glconfig    GLCONFIG;  //224 �����
extern struct AddCfg       ADCONFIG;
extern struct IdRun CONFIG_IDRUN[3]; // ���䨣���� �� ��⪠�
extern struct AdRun CONFIG_ADRUN[3];               //120*3=360
extern struct SPI_HandleTypeDef hspi3;
extern uint16_t SizeCfg, SizeIdR,SizeIntPar;
extern uint16_t SizeAdCfg, SizeAdR;
extern uint16_t SizeFHP;
extern struct intpar INTPAR;
extern struct glconfig    RES_GLCONFIG;
extern struct glconfig *MainCfg;
extern struct IdRun  *IdRn; 
//struct glconfig  *ResCfg;
//struct IdRun  *Res_IdRn;

extern struct AddCfg  *AdCfg; // = &ADCONFIG;
struct AdRun  *AdRn; 
extern char Z_in[],Z_Out[];
extern uint8_t AdrAddCfg[]; 

extern uint8_t AdrCfg[]; 
extern uint8_t AdrIdRun[3][2];
extern uint8_t AdrAdRun[3][2];
extern uint8_t AdrIntP[]; 
extern uint8_t AdrFHP[2];
char  *SetToNull; //  = (char *)MK_FP(SEGMENT,0);
// 3*21*4=252 �����
float XJMC[3][21]= {{ 0.9070, 0.0310, 0.0050, 0.0450, 0.0084, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0010, 0.0015, 0.0003, 0.0004, 0.0004, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},   
    { 0.8590, 0.0100, 0.0150, 0.0850, 0.0230, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0035, 0.0035, 0.0005, 0.0005, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
    { 0.9650, 0.0030, 0.0060, 0.0180, 0.0045, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0010, 0.0010, 0.0005, 0.0003, 0.0007, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 }};

void P23K256_Write_Data(struct SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
uint8_t T24_I2C_256_Write_Data(struct I2C_HandleTypeDef *hi2c, uint16_t MemAddress,  uint8_t *txData, uint16_t TxSize); 
//--------------------------------------------------------
/*
void GetVersion(struct Id *pid) {
   pid->Ver       = 10;                //� MODBUS ASCII FLOWSIC;  MODBUS RTU FLOWSIC, RMG, RMA, KROHNE,����
   pid->VerDen    = 17 | 0x40;     //  | 0x20,0x40 - 0xE0
   pid->VerMes    = 12 | 0x80;  //0x10 - �������� ������ 8.1, 0x20,0x30...0xF0 -
                               // ������ 8.2,8.3 - 8.15
   pid->VerGod    = 19;
//   gl->Ver = pid->Ver;        prun->constdP
}
*/
//-------------------------------------------------------------------
void    SetGlConfig (struct glconfig *Cfg, struct IdRun *IdR) {
// struct Id *glid;
   unsigned char   NameC[17]    = "DANYFLOW        "; // ��� �����������
   unsigned char   NameP[17]    = "UKRINTEX        "; // ������������ �p���p�����
   unsigned char   PASSW[17]    = "                "; // ��p���
   unsigned char   TEL[13]    = "+38050001122";  //�������
   unsigned char   IPADR[13]    = "192.168.0.100"; // IP-�����
   unsigned char   PORT[4]    = "8080";// ����� �����
   int i,j;
   float fTmpTau,fTmpRo;
//   uint8_t *pAdr;
    
   memmove(Cfg->NameCal,NameC,16);
   memmove(Cfg->NamePlant,NameP,16);
   memmove(Cfg->PASSWORD,PASSW,16);
   memmove(Cfg->Telephone,TEL,13);
   memmove(Cfg->IP_Adress,IPADR,13);
   memmove(Cfg->IP_Port,PORT,4);

   Cfg->NumCal    = 1; // ����p �����������
   Cfg->ContrHour = 7; // ����p������ ���
   Cfg->NumRun    = 3; // ���-�� �����
   Cfg->TypeCount = 0x00; // ��� 1,0: ��50\GERG91\NX19 [00\01\10]
		       // ��� 7 : ��p-const\��p-p���. [0\1]
   Cfg->StartConfigFlag = 0;           // ���� ���. �������p����
   Cfg->Period          = 1;          // ����p��� ���������� � �������
//   GetVersion(glid);
   Cfg->Ver             = 10; //8; // ��p��� �p��p������� �����������
   Cfg->SummerMonth     = 0x03;        // �����
   Cfg->SummerDay       = 0xC9;        // ����
   Cfg->SummerHours     = 0x03;        // ��� ��p����� �� ������ �p���
   Cfg->WinterMonth     = 0x0A;        // �����
   Cfg->WinterDay       = 0xC9;        // ����
   Cfg->WinterHours     = 0x04;        // ��� ��p����� �� ������ �p���
   Cfg->Speed_1         = 5;           // ���p���� �� COM2-��p��
   Cfg->Speed_2         = 7;           // ���p���� �� COM1-��p��
   Cfg->Com1NoWrite     = 0;           // ������ ������ �� COM1
   Cfg->scr_conf        = 0x17;        // ����� ������������ �������
   Cfg->vol_odor        = 0.0;         // ����� ��� 1-�� �����������, �3/���
   Cfg->termQmax        = 0.0 ;        // ����� ��� 2-�� �����������, �3/���
   Cfg->termQmin        = 0.0;         // ������
   Cfg->termDACmin      = 0;         // ������
   Cfg->PreambNum       = 0;           // ����� �������� �� ���2
   Cfg->PreambNum_1     = 0;           // ����� �������� �� ���1

   Cfg->RoTable[0]  = 0.6682;          // �����
   Cfg->RoTable[4]  = 1.1649;          // ����

   Cfg->TauTable[0]  = 514.798;        // ������ �� ������
   Cfg->TauTable[4]  = 523.213;        // ������ �� �����

   fTmpTau = (Cfg->TauTable[4] - Cfg->TauTable[0]) / 4.0;
   fTmpRo = (Cfg->RoTable[4] - Cfg->RoTable[0]) / 4.0;

   for (i=1; i<4; i++) {               // ������� ���������� ����������
      Cfg->RoTable[i]  = Cfg->RoTable[0] + i*fTmpRo;
      Cfg->TauTable[i] = Cfg->TauTable[0] + i*fTmpTau;
   }
//   if (memcmp(LogPassw[0].login,"Admn",4)) // ��� Admn
   { Cfg->StartDostup = 3;
     Cfg->StartDostup1 = 3;
   }

   for (i=0; i<sizeof(Cfg->reserve); i++)
      Cfg->reserve[i] = 0;              // ������
      memset(&Cfg->EDIZM_T,0,12);
      Cfg->EDIZM_T.EdIzmE = 1;  //1- ���� 2-���
      Cfg->EDIZM_T.TIME_SU0.date=1;
      Cfg->EDIZM_T.TIME_SU0.month = 1;
      Cfg->EDIZM_T.TIME_SU0.year = 50;
      Cfg->EDIZM_T.T_AP = 0;
       Cfg->EDIZM_T.T_SU = 1;
      Cfg->crc =  Mem_Crc16((uint8_t*)Cfg,SizeCfg-4);
   
      IdR->TypeRun     = 4; //4 �������  // ������ ����p���� �� �����
      (IdR+1)->TypeRun     = 10; //10 ��� MODBUS
      (IdR+2)->TypeRun     = 1; // P+dP+T
      IdR->TypeCounter = 0; // ��� �������� �������/FlowSic600/RMG/RMA/Krohne
      IdR->AddrMODBUS = 0;   // MODBUS ����� ����������� �� ��������
      (IdR+1)->TypeCounter =1; // ��� �������� FlowSic600
      (IdR+1)->AddrMODBUS = 1;
      (IdR+2)->TypeCounter = 0; // ��� �������� �������
      (IdR+2)->AddrMODBUS = 0;

for (i=0; i<3; i++) { // ��p����p� �����
      switch (i) {
	 case 0:
	    memmove((IdR+i)->NameRun,"Run1             ",16); // ��� �����
	    break;
	 case 1:
	    memmove((IdR+i)->NameRun,"Run2             ",16); // ��� �����
	    break;
	 case 2:
	    memmove((IdR+i)->NameRun,"Run3             ",16); // ��� �����
	    break;
      }

      (IdR+i)->TypeP       = 0 ;         // ��� ������� �������� - 0\1 ���.\�����.
      (IdR+i)->NumTurbo    = 1.0;        // ���-�� �3 �� 1 �������
      (IdR+i)->Qmin        = 40.0;       // ����������� p�����
      (IdR+i)->Qmax        = 1000.0;     // ������������ p�����

      (IdR+i)->Qw          = 50.0 ;       // p����� �p� p.�., �3/���

      (IdR+i)->Pmin        = 1.02;        // ������ �p���� ������� ��������, ���\��2
      (IdR+i)->Pmax        = 50.0;       // ��p���� �p���� ������� ��������, ���\��2
      (IdR+i)->dPhmin      = 63.0;      // ����. �p���� ��p�����, ���\�2
      (IdR+i)->dPhmax      = 6300.0;     // ��p�. �p���� ��p�����, ���\�2
      (IdR+i)->R0min      = 0.67;        // ����. �p���� ���������, ���\�2
      (IdR+i)->R0max      =  1.06;      // ��p�. �p���� ���������, ���\�2
      (IdR+i)->tmin        = -23.150;      // ����. �p���� �����p���p�, �p. �
      (IdR+i)->tmax        = 60.0;       // ��p�. �p���� �����p���p�, �p. �

      (IdR+i)->D20         = 300.0;//52.0;482.41;      // ����p����� ������p �p���, ��
      (IdR+i)->d20         = 66.2; //164.921; 20.0;     // ����p����� ������p ����p����, ��

      (IdR+i)->Rn          = 0.11;      // ������ ����������� ����p����, ��
      (IdR+i)->TauP        = 2.0;      // �������� �������, ���

      (IdR+i)->dPots       = 5.0;        // �p����� �������, ���/�2
      (IdR+i)->ValueSwitch = 630.0;        // ��p�� ��p��������� �� ��p�����, ���/�2
      (IdR+i)->typeOtb     = 0;          // 0 - �������, 1 - ���������
      (IdR+i)->R           = 0.1;       // ���. ������������� ������������, ��
      (IdR+i)->Bd          = 0.000016206;  // ����-�� ���-�� ����-� ��������� ���������, ���^-1(��� A)
      (IdR+i)->kBd         = 6.571e-9;        // ����-�� B
      (IdR+i)->kCd         = 0.0;  //e-12;        // ����-�� C
      (IdR+i)->Bt          = 0.0000111;  // ����-�� ���-�� ����-� ��������� �p���, ���^-1(��� A)
      (IdR+i)->kBt         = 7.7e-9;        // ����-�� B
      (IdR+i)->kCt         = -3.4e-12;        // ����-�� C
      (IdR+i)->Q           = 0.0 ;       // p����� �p� p.�., �3/���

      (IdR+i)->constdata   = 0x00;       // �������� ���������\������ 0\1 
      (IdR+i)->NoAlarmConstFlag = 0;     // ���� "��������� ��� ������"
      (IdR+i)->constP      = 10;        // ��������� ��������, ���\��2
      (IdR+i)->constdP     = 63.0;        // ��������� ��p�����, ���\�2
      (IdR+i)->constt      = 20.0;      // ��������� �����p���p�, �p��. �
      (IdR+i)->constqw     = 50.0;        // ��������� p������ �p� p.�., �3/���
      (IdR+0)->ROnom       = 0.699957;     // ��������� ���� ��� �.�., ��/�^3
      (IdR+1)->ROnom       = 0.7;
      (IdR+2)->ROnom       = 0.68;
      (IdR+0)->Heat        = 10.0;  
      (IdR+1)->Heat        = 10.1;       //
      (IdR+2)->Heat        = 38.2013;       //

      (IdR+i)->Pb          = 750.0;      // ��������������� ��������, �� p�. ��.
      (IdR+i)->NCO2        = 1.0;     // ����p��� ���� �������� ����p��� � ������� �����, %
      (IdR+i)->NN2         = 1.0;     // ����p��� ���� ����� � ������� �����, %
//      (IdR+i)->RoVV       = 0.699957;     // ��������� ���� ��� �.�., ��/�^3

      (IdR+0)->RoVV       = 0.699957;     // ��������� ���� ��� �.�., ��/�^3
      (IdR+1)->RoVV       = 0.7;
      (IdR+2)->RoVV       = 0.68;

      (IdR+0)->HsVV        = 38.0;  
      (IdR+1)->HsVV        = 39.1;       //
      (IdR+2)->HsVV        = 38.2013;       //

int ii;
//      for ( ii=0; ii<5; ii++)
//	 (IdR+i)->hartadrt[ii] = 0;
    (IdR)->hartadrt[0] = 0X1A;
    (IdR)->hartadrt[1] = 0XBC;
    (IdR)->hartadrt[2] = 0X00;
    (IdR)->hartadrt[3] = 0X04;
    (IdR)->hartadrt[4] = 0X97;
    (IdR)->hartadrp[0] = 0X02;
    (IdR)->hartadrp[1] = 0X0A;
    (IdR)->hartadrp[2] = 0X00;
    (IdR)->hartadrp[3] = 0X13;
    (IdR)->hartadrp[4] = 0X40;
    
    (IdR+1)->hartadrt[0] = 0X01;
    (IdR+1)->hartadrt[1] = 0X20;
    (IdR+1)->hartadrt[2] = 0X00;
    (IdR+1)->hartadrt[3] = 0X00;
    (IdR+1)->hartadrt[4] = 0X21;
    (IdR+1)->hartadrp[0] = 0X02;
    (IdR+1)->hartadrp[1] = 0X0A;
    (IdR+1)->hartadrp[2] = 0X00;
    (IdR+1)->hartadrp[3] = 0X00;
    (IdR+1)->hartadrp[4] = 0X22;

    (IdR+2)->hartadrt[0] = 0X01;
    (IdR+2)->hartadrt[1] = 0X20;
    (IdR+2)->hartadrt[2] = 0X00;
    (IdR+2)->hartadrt[3] = 0X00;
    (IdR+2)->hartadrt[4] = 0X31;
    (IdR+2)->hartadrp[0] = 0X02;
    (IdR+2)->hartadrp[1] = 0X0A;
    (IdR+2)->hartadrp[2] = 0X00;
    (IdR+2)->hartadrp[3] = 0X00;
    (IdR+2)->hartadrp[4] = 0X32;
    (IdR+2)->hartadrdph[0] = 0X26;
    (IdR+2)->hartadrdph[1] = 0X06;
    (IdR+2)->hartadrdph[2] = 0X70;
    (IdR+2)->hartadrdph[3] = 0X1D;
    (IdR+2)->hartadrdph[4] = 0X81;

//    for (ii=0; ii<5; ii++)
//	 (IdR+i)->hartadrp[ii] = 0;
//      for (ii=0; ii<5; ii++)
//	 (IdR+i)->hartadrdph[ii] = 0;
//      for (ii=0; ii<5; ii++)
//	 (IdR+i)->hartadrdph[ii] = 0;
      for (ii=0; ii<4; ii++)
	  (IdR+i)->NumHartC[ii] = 1;
      (IdR+i)->Dens = 0;

      (IdR+i)->sensData[0].nInterface = 0; // ��� ����������

      (IdR+i)->sensData[0].aTable[0].code  = -40.0;
      (IdR+i)->sensData[0].aTable[0].value = -40.0;
      (IdR+i)->sensData[0].aTable[1].code  = -15.0;
      (IdR+i)->sensData[0].aTable[1].value = -15.0;
      (IdR+i)->sensData[0].aTable[2].code  =  10.0;
      (IdR+i)->sensData[0].aTable[2].value =  10.0;
      (IdR+i)->sensData[0].aTable[3].code  =  35.0;
      (IdR+i)->sensData[0].aTable[3].value =  35.0;
      (IdR+i)->sensData[0].aTable[4].code  =  60.0;
      (IdR+i)->sensData[0].aTable[4].value =  60.0;

      (IdR+i)->sensData[1].nInterface = 0; // ��� ����������

      (IdR+i)->sensData[1].aTable[0].code  = 0.0;
      (IdR+i)->sensData[1].aTable[0].value = 0.0;
      (IdR+i)->sensData[1].aTable[1].code  = 12.5;
      (IdR+i)->sensData[1].aTable[1].value = 12.5;
      (IdR+i)->sensData[1].aTable[2].code  = 25.0;
      (IdR+i)->sensData[1].aTable[2].value = 25.0;
      (IdR+i)->sensData[1].aTable[3].code  = 37.5;
      (IdR+i)->sensData[1].aTable[3].value = 37.5;
      (IdR+i)->sensData[1].aTable[4].code  = 50.0;
      (IdR+i)->sensData[1].aTable[4].value = 50.0;

      (IdR+i)->sensData[2].nInterface = 0; // ��� ����������

      (IdR+i)->sensData[2].aTable[0].code  = 0.0;
      (IdR+i)->sensData[2].aTable[0].value = 0.0;
      (IdR+i)->sensData[2].aTable[1].code  = 1575.0;
      (IdR+i)->sensData[2].aTable[1].value = 1575.0;
      (IdR+i)->sensData[2].aTable[2].code  = 3150.0;
      (IdR+i)->sensData[2].aTable[2].value = 3150.0;
      (IdR+i)->sensData[2].aTable[3].code  = 4725.0;
      (IdR+i)->sensData[2].aTable[3].value = 4725.0;
      (IdR+i)->sensData[2].aTable[4].code  = 6300.0;
      (IdR+i)->sensData[2].aTable[4].value = 6300.0;

      (IdR+i)->sensData[3].nInterface = 0; // ��� ����������

      (IdR+i)->sensData[3].aTable[0].code  = 0.0;
      (IdR+i)->sensData[3].aTable[0].value = 0.0;
      (IdR+i)->sensData[3].aTable[1].code  = 157.5;
      (IdR+i)->sensData[3].aTable[1].value = 157.5;
      (IdR+i)->sensData[3].aTable[2].code  = 315.0;
      (IdR+i)->sensData[3].aTable[2].value = 315.0;
      (IdR+i)->sensData[3].aTable[3].code  = 472.5;
      (IdR+i)->sensData[3].aTable[3].value = 472.5;
      (IdR+i)->sensData[3].aTable[4].code  = 630.0;
      (IdR+i)->sensData[3].aTable[4].value = 630.0;
      (IdR+i)->InterfaceDens = 4; // ��� ����������
       (IdR+i)->typeDens = 0; // ������� ��� ������� ��������� ��� �����
       (IdR+i)->KUzs=0x82;
       (IdR+i)->Vlag=0x0;//IdR+1

}  // ����� ����� �� i
         IdR->nKcompress = 1;  //1-GERG91
         (IdR+1)->nKcompress = 2;  //2-Gerg88
         (IdR+2)->nKcompress = 1; //3- AGA8
         IdR->constqw = 80;
         (IdR+1)->constP = 7;
         (IdR+1)->constt = 5;
         (IdR+2)->constP = 12;
         (IdR+2)->constt = 25;
        (IdR+1)->NumHartC[1] = 2;
        (IdR+2)->NumHartC[2] = 2;
  for (i=0; i<3; i++) 
  {
   IdR = &CONFIG_IDRUN[i];
   IdR->crc =  Mem_Crc16((uint8_t*)IdR,sizeof(struct IdRun)-2);
  }      
}
//---------------------------------------------------------------------
void SetIntParms() {                    // ��������� ���������� ����������

   INTPAR.HartNumCyclRep     = 12;      // ����� ������ ������� ��� ���
   INTPAR.HartNumImmedRep    = 1;       // ����� ����������� ��������
   INTPAR.HartReqDelay       = 0;       // ���.�������� ����� �������� HART
   INTPAR.RS485Com5Speed     = 5;       // �������� �� COM5 - RS485 ���
   INTPAR.Com4Speed          = 3;       // �������� �� �OM4 ���
   INTPAR.Com2Mode           = 0;       // ����� ��� ���2: 0-232, 1-485
   INTPAR.Com1NoWrite        = 0;       // ������ ������ �� ���1
   INTPAR.OdoMask            = 1;       // ����� ����������
   INTPAR.CntBufLen          = 0;       // ����� ������ ��������
   INTPAR.CntAddBufShift     = 0;      // ����� ���.������ / �������� ������ ���
   INTPAR.VolumeDelta        = 0.0;     // ���������� ����������� �� ������
   INTPAR.ChannelJoin        = 0;       // ������� ����������� ������
   INTPAR.QminFlag           = 0;       // ������� �������� �� ����������� ������
   INTPAR.AlarmVolumeMinTime = 60;      // �����. ����� ��� ���������� ������
   INTPAR.FullNormVolume     = 0;       // � ����� ���� ����� ��� ������������
   INTPAR.Keyboard           = 0;       // ������� ����������
   INTPAR.crc =  Mem_Crc16(&INTPAR.HartNumCyclRep,sizeof(INTPAR)-4);

    //   Mem_IntParWr();
//printf("SetIntParms\n");
}
//---------------------------------------------------------------------------
void config()
{
   int i;
   double Vtot[3],Vsh[3],Valr[3],VtotSu[3];
   uint8_t *pAdr;
    //   char far *pcXI;
  AdRn =  &CONFIG_ADRUN[0];
  AdCfg = &ADCONFIG;
 
  IdRn = &CONFIG_IDRUN[0];
  MainCfg = &GLCONFIG;

//   struct IdRun      *prun;             // ��p����p� �����
//   int nRun;
//   struct Id *glid;
//   glconfig *Cfg;

   unsigned char   NameC[17]    = "DANYFLOW        "; // ��� �����������
   unsigned char   NameP[17]    = "UKRINTEX        "; // ������������ �p���p�����
   unsigned char   PASSW[17]    = "                "; // ��p���
   unsigned char   TEL[13]    = "+38050001122";  //�������
   unsigned char   IPADR[13]    = "192.168.0.100"; // IP-�����
   unsigned char   PORT[4]    = "8080";// ����� �����
/*
   intpar   *IntPars; // = (intpar *)  MK_FP(SEGMENT,3584);
*/
//   unsigned int ExtChroma = 2766;// ��� ������ ����������� XI 3 ������� �� 21*4 ���� (252 �����)
        AdCfg->Takt = 1;
        AdCfg->corrsek = 3;
        AdCfg->crc = Mem_Crc16((uint8_t*)AdCfg,sizeof(struct AddCfg)-2);
   for (i=0; i<sizeof(AdCfg->Reserv) ; i++)
     AdCfg->Reserv[i] = 0;
   AdCfg->LASTTIME.date = 1;
   AdCfg->LASTTIME.month = 1;
   AdCfg->LASTTIME.year = 0;
   AdCfg->LASTTIME.hours = 7;
   AdCfg->LASTTIME.minutes = 0;
   AdCfg->LASTTIME.seconds = 0;
  for (i=0; i<3; i++)
  { (AdRn+i)->P = 10.0;
    (AdRn+i)->dP = 20.0;
    (AdRn+i)->dPh = 22.0;
    (AdRn+i)->dPh_s = CONFIG_ADRUN[3].dPh;
    (AdRn+i)->t = 30.0;
    (AdRn+i)->Qw_s = 50.0/3600.0;
    (AdRn+i)->ROdens = 0.7;
    (AdRn+i)->crc = Mem_Crc16((uint8_t*)(AdRn+i),sizeof(struct AdRun)-2);
  }
  //������ ADCONFIG
  P23K256_Write_Data(&hspi3,AdrAddCfg,(uint8_t*)AdCfg,SizeAdCfg);  
//  T24_I2C_256_Write_Data(&hi2c1, AdrAddCfg[0]*256+AdrAddCfg[1],  (uint8_t*)AdCfg, SizeAdCfg);   
  // ������ CONFIG_AdRUN[] �� ������
  for (i=0; i<3; i++)
  { 
   pAdr = &AdrAdRun[i][0]; 
   P23K256_Write_Data(&hspi3,pAdr,(uint8_t*)(AdRn+i),SizeAdR);   
//   T24_I2C_256_Write_Data(&hi2c1, AdrAdRun[i][0]*256+AdrAdRun[i][1],(uint8_t*)(AdRn+i), SizeAdR); 
  }

  //printf("ResCfg\n");

   SetGlConfig(MainCfg,IdRn);
   //������ GLCONFIG 
  uint8_t Blank[5] = {0,0,0,0,0};
  memmove((MainCfg+83),Blank,5);
//  MainCfg->crc =  Mem_Crc16((uint8_t*)&GLCONFIG,SizeCfg-4);//MainCfg
  P23K256_Write_Data(&hspi3,AdrCfg,(uint8_t*)MainCfg,SizeCfg-2);   
// ������ ��������� ������������ � �/� AT24C256C
 //  T24_I2C_256_Write_Data(&hi2c1, AdrCfg[0]*256+AdrCfg[1],  (uint8_t*)MainCfg, SizeCfg); 
  // ������ CONFIG_IDRUN[] �� ������
   for (i=0; i<3; i++)
   { 
    pAdr = &AdrIdRun[i][0]; 
    P23K256_Write_Data(&hspi3,pAdr,(uint8_t*)(IdRn+i),SizeIdR);   
//    T24_I2C_256_Write_Data(&hi2c1, AdrIdRun[i][0]*256+AdrIdRun[i][1],(uint8_t*)(IdRn+i), SizeIdR); 
   }
//printf("MainCfg\n");
 //  MainCfg->crc = Mem_Crc16((char*)MainCfg,sizeof(struct glconfig)-2);
//   pcXI = (char far *)MK_FP(SEGMENT,ExtChroma);  //2766
//   memcpy(pcXI,XJM,252);     //������ �   ��� ��  ���   XJM

   P23K256_Write_Data(&hspi3,AdrFHP,(uint8_t*)XJMC,SizeFHP);   
// ������ ��������� ������������ � �/� AT24C256C
// for (i=0; i<3; i++)
//   {
//   T24_I2C_256_Write_Data(&hi2c1, AdrFHP[i][0]*256+AdrFHP[i][1],  (uint8_t*)&XJMC[i][0], SizeFHP); 
//   } 
   SetIntParms();                       // ��������� ���������� ����������
   P23K256_Write_Data(&hspi3,AdrIntP, (uint8_t*)&INTPAR, SizeIntPar-2);   
//   T24_I2C_256_Write_Data(&hi2c1, AdrIntP[0]*256+AdrIntP[1],  (uint8_t*)&INTPAR, SizeIntPar-2); 

   //   SetDostup();

}
