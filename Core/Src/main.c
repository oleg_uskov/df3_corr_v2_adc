/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <math.h>
#include "h_calc.h"
#include "h_mem.h"
#include "h_main.h"
#include "h_time.h"
#include "h_disp.h"
#include "hart_driver.h"

//#include "LiquidCrystal.h"

#include "w25.h"
#include "stm32l4xx_hal_rtc.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define DWT_CONTROL *(volatile unsigned long *)0xE0001000
#define SCB_DEMCR   *(volatile unsigned long *)0xE000EDFC
#define  COUNT_DMA2 40
#define  COUNT_URT3 40
#define  COM5_BUF_LEN     80
#define MODE8  (0x00000700U)
#define MODE16 (0x00000F00U)
//#define UART_TXFIFO_THRESHOLD_1_2   USART_CR3_TXFTCFG_1        
#define  MODBUS_SEND_LEN_FB 8
#define  MODBUS_SEND_LEN_ASCII  17
#define  MODBUS_SEND_LEN_RTU 8
#define  COUNT_MODBUS 80
#define  maxindex          550
void DWT_Init(void) {
	SCB_DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; //
	DWT_CONTROL |= DWT_CTRL_CYCCNTENA_Msk;  // 
}

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TestAddr  0x20000//0x7F//0x007F0000
#define TestSize  520 // äëÿ ìàññèâà

#define TestSizeW  64 // äëÿ çàïèñè
#define TestSizeR  64 // äëÿ ÷òåíèÿ
#define maxq    30   //ìàêñ äëèíà î÷åðåäè
#define KQu      3  // Êîëè÷åñòâî î÷åðåäåé
#define SyncByte    0xAA        // ñèíõpîáàéò ïpè çàïpîñå
#define LTIMEREQ 600    // ñåê ïîñëå ïîñëåäíåãî îïðîñà

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CRC_HandleTypeDef hcrc;

QSPI_HandleTypeDef hqspi;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
SPI_HandleTypeDef hspi3;
//DMA_HandleTypeDef hdma_spi1_rx;
//DMA_HandleTypeDef hdma_spi1_tx;

TIM_HandleTypeDef htim16;
TIM_HandleTypeDef htim17;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
//DMA_HandleTypeDef hdma_usart1_rx;
//DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */
/*  Àðõèâû: Ïåðèîäè÷åñêèé PER = 1, ×àñîâîé HOU = 2. Îáà àðõèâû îäèíàêîâîãî ðàç-
ìåðà, ïî 100 Êá íà íèòêó.Îäíà çàïèñü 64 áàéòà. Íà îäíó íèòêó 100 Ê /64 = 1600
çàïèñåé.
  Ñóòî÷íûé àðõèâ DAY = 3, 28 Ê íà íèêó,  28 Ê/64 = 448 çàïèñåé.
  Àâàðèéíûé àðõèâ ALR = 4, 24 K íà íèòêó, 24Ê / 16 = 1536 çàïèñåé ïî 16 áàéò
  Àðõèâ âìåøàòåëüñòâ AUD = 5, 40 K íà íèòêó, 44 Ê / 32 = 1408 çàïèñåé
  Àðõèâ áåçîïàñíîñòè SCR = 6, 32 K íà íèòêó, 32 Ê / 32 = 1024 ýàïèñè
  Àðõèâ àâàðèéíûõ îáúåìîâ AVOL = 16 K íà íèòêó, 16 Ê / 32 = 512 çàïèñè.
  Ñóììàòîðû ïåðèîäè÷åñêèõ - ïî 74 áàéòà íà íèòêó = 74*3=222 á,
  Ñóììàòîðû ÷àñîâûõ - 74*3=222 á,
  Ñóììàòîðû ñóòî÷íûõ - 74*3 = 222 á.
*/
uint32_t alt1, alt2, alt3, count1, count2, count3;
uint32_t ctim1, ctim2, ctim3;
uint8_t  bflag_c1,bflag_c2;
uint32_t intr[3]; //, intr2, intr3; [2]
uint32_t Interval[3] = {0,0,0};  //èíòåðâàä ìåæäó èìïóëüñàìè


double dQw_1, dQw_3, dQw_2;  // ñåêóíäíûé ðàñõîä ñ÷åò÷èêîâ ãàçà
 double dQw, Qw;
//uint16_t Pim = 0;
uint32_t k,in1,in2,k1;
uint32_t ct1, ct2, ct3;
float Kd = 0.75;
uint32_t T16; 

 uint8_t St_Send=1, Busy=1;
 HAL_StatusTypeDef St=1;
 char _print[20];
 uint32_t Ad;
// Äëÿ W25
 __IO uint8_t CmdCplt, RxCplt, TxCplt, StatusMatch, TimeOut,Rx23Cplt;
 __IO uint8_t Rx23Cplt,Tx23Cplt,RxCpltU3,TxCpltU3;
  uint16_t BSectorErase[9] = {0,4,79,154,175,193,226,250,262}; // äî 427
  uint16_t KolSectorArch[9] = {0,25,25,7,6,11,8,4,55};// 55};  
  uint16_t RecSizeW[9] = {0, 64,64,64,16,32,32,32,64};// 0, REC,HOR,DAY,ALR,AUD,SCR,AVOL
  uint16_t RecSizeR[9] = {0, 64,64,64,16,18,18,32,64};
  uint16_t KolZapArcAll[9] = {0,1600,1600,448,1536,1408,1024,512, 3520};//3520 191};
  uint16_t KolZapArc[9] = {0,1536,1536,384,1280,1280,896,384,3456}; //  3456 128};
  uint16_t SectorSize=4096;  
  uint16_t TimeoutW = 3000;
  uint8_t bPowerOff;  // ïðèçíàê õîëîäíîãî èëè ãîðÿ÷åãî ñòàðòà
  uint16_t Zap_in_Block;
  uint16_t st_cntIniRecCom1;
  uint16_t g_cntIniRecCom1;
  uint16_t st_cntIniRecCom2;
  uint16_t g_cntIniRecCom2;
  
  uint16_t EraseBl[9][3]; //={0}; //,0,0,0,0,0,0,0,0}; // 54 áàéòà
    HAL_StatusTypeDef Er_I2C;
// uint32_t BaudRates_huart12[]={1200,2400,4800,9600,19200,38400,57600,115200};
// uint32_t BaudRates_huart2[]={1200,2400,4800,9600,19200,38400,57600,115200};

 char  Comnd; 
int ScrButHit(void);                        // îïðåäåëåíèå íàæàòèÿ êíîïêè ýêðàíîâ
//void n_delay (int mlsek);
unsigned int Crc16(unsigned char* buf, int iSize);
void config(void);
void BRAKET1(float* XI);
int Time_GetNumrun(void);                  // ÷òåíèå òåêóùåãî íîìåpà íèòêè
void initialData(void);
void DZOFPT_B(void);
void Task_Calc_CorrSetCalcData();
void ChekBufResiv1();
void Ini_Reciv1();
void UART_EndTxTransfer(UART_HandleTypeDef *huart);
void Mem_AlarmSetNoalarm();
static void UART_DMATransmitCplt(DMA_HandleTypeDef *hdma);
static void UART_DMAReceiveCplt(DMA_HandleTypeDef *hdma);
void SetRS485DataAdr(void); 
void RS485_SetRS485Err(void);
void RS485Com4IniHF(void);  
void RS485Ini(void);
void  StartUZS(int);
void  FirstReadFS(int,int,uint16_t); // ¯¥à¢®¥ çâ¥­¨¥ ®¡ê¥¬  ¨§ “‡‘ FlowSic
void Result_MODBUS(void);
void Time_GetCount(void); 
uint32_t  SetComSpeed(uint8_t);
void SetSpeedUZS(uint8_t nRun);
void Persist_AddQwCountTotal(int iRun, double QwCountTotal);
double Persist_GetQwCountTotal(int iRun);
void setCursor(uint8_t col, uint8_t row);
void PrintForm0(void);
void SetWindows1();
void SetWindows2();
void GlPrint();
void Uart1_Watch(void);
void IniCfgSpeed1();
void IniCfgSpeed2();
void SetSpeed_USART3(uint32_t);
void ZapuskUZS();
void LCDPrint(uint8_t stroka, uint8_t colonka, char * Text);
void LCDresligt(void);


typedef void (*pf)(void);
struct tq                               // ñòpóêòópà ýëåìåíòà î÷åpåäè ê òàéìåpó
{ unsigned int  Time;                   // êîëè÷åñòâî òàêòîâ
  int           Prioritet;              // íîìåp ïpèîpèòåòà ôóíêöèè
  pf            Task;                   // óêàçàòåëü íà ôóíêöèþ
};
int InQueue(int n,pf func);






/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM17_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_QUADSPI_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);
static void MX_SPI3_Init(void);
static void MX_CRC_Init(void);
static void MX_TIM16_Init(void);
/* USER CODE BEGIN PFP */
static void QSPI_AutoPollingMemReady_IT(QSPI_HandleTypeDef *hqspi);
static void QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi,uint32_t Timeout);
uint8_t QSPI_Send_CMD_IT(uint32_t instruction, uint32_t address,uint32_t dummyCycles, 
                    uint32_t addressMode, uint32_t dataMode, uint32_t dataSize);
uint8_t WriteDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *wrData);
uint8_t ReadDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData);
void WriteDataW25(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *wrData);
void ReadDataW25(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData);

HAL_StatusTypeDef HAL_QSPI_AutoPolling_IT(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd, QSPI_AutoPollingTypeDef *cfg);
void LiquidCrystal(GPIO_TypeDef *gpioport,uint16_t d0, uint16_t d1,
    uint16_t d2, uint16_t d3,uint16_t d4, uint16_t d5, uint16_t d6, uint16_t d7);
size_t print(const char str[]);
void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc);

unsigned char Flag_Hartservice;         // ¯à¨§­ ª à¥¦¨¬  âà ­á«ïæ¨¨ HART<->COM2
//extern int CalcNumRun;                  // íîìåp íèòêè äëÿ ðàñ÷åòà
uint16_t  counttime;         // êîë-âî òàêòîâ äëÿ ñ÷èòûâàíèÿ âpåìåíè
extern int NRep;//rNRep;                  // ìàêñ. è ðåàëüíîå ÷èñëî íåìåäë. ïîâòîðîâ
extern unsigned int inindex1,inindex2;  // ñì. íà ïóñòîé ýëåìåíò, inc ïî ïpåp. ïpèåìíèêà, =0 ïpè èíèö.
extern unsigned int comindex;           // ñì. íà ïóñòîé ýëåìåíò, inc ïî ïpåp. ïpèåìíèêà, =0 ïpè èíèö.

extern uint8_t BufResiv1[maxindex];
extern uint8_t BufResiv2[maxindex];
extern int   flagsomwi;                 // ôëàã äëÿ ïåpåõîäà ñ ëåòíåãî íà çèìíåå âpåìÿ
extern struct tdata TDATA;              // äëÿ ñóììèpîâàíèÿ äàííûõ
extern struct bintime POWER_OFF_TIME;   // âðåìÿ âûêëþ÷åíèÿ
extern int   starttime;                 // âpåìÿ "pàñêpóòêè" ñèñòåìû
extern unsigned char Flag_Hartservice_1;
extern int cWatchDog;
extern struct clock_image TIME_WRTIME;  // èñïîëüçóåòñÿ ïpè óñòàíîâêå âpåìåíè

extern struct memaudit    MEMAUDIT;     // ñòpóêòópà çàïèñè àpõèâà âìåøàòåëüñòâ
extern struct memdata     MEMDATA;      // ñòpóêòópà çàïèñè îò÷åòà - 26 áàéò
extern struct bintime     BINTIME;      // äâîè÷íàÿ ñòpóêòópà âpåìåíè
struct bintime     COPY_BINTIME;      // äâîè÷íàÿ ñòpóêòópà âpåìåíè

extern struct intpar      INTPAR;
extern struct calcdata    CALCDATA[3];
extern struct memalarm    MEMALARM;     // ñòpóêòópà çàïèñè àpõèâà àâàpèé
extern struct clock_image wtime;
extern long QcntT;                      // ñ÷åò÷èê ñåêóíä äëÿ ðàñõîäà ïî ñ÷åò÷èêó
extern char TxRx1,TxRx2,TxRx485; // ïðèåì/ïåðåäà÷à 0/1
extern unsigned int BASE_COM3;
uint8_t   glnumrun;                         // íîìåp íèòêè äëÿ àðõèâà
extern double IndQ[3];
extern unsigned int altcount0,altcount1;
extern double KVolumeUZS[];
extern unsigned long int Vpred[5];
extern unsigned int di1,di2,di1p[2][4],di2p[2][4];
extern int NzBlk[],NzBlkp[];
extern int bSDini;
extern int bSDini_1;
extern int bWrMgn;
extern unsigned char spr;
extern struct bintime logTimeb,logTimeb1;
extern unsigned long Tval2,Tval22;
extern unsigned  int Tval12;
extern unsigned long TimeKP1[],TimeKP2[];
extern char MV[3][4];
//extern int AGA_F[];
char Calc59Mgn1, Calc1Per1,Calc5Conf1;
extern char NumChrom[];
extern char FirstCalc,TimeCalc;
extern float T_st;
char NumChrom1[3]={1,2,3};
//struct IdRun IDRUN[3]; // 520*3=1560+168=1728 // ïàpàìåòpû íèòîê
extern uint8_t flagspeed1;
extern uint8_t MODBUS_SendBuf[32];// Com5SendBuf[32];              // ¡ãä¥à ¯®áë«ª¨
extern uint8_t MODBUS_RecvBuf[COM5_BUF_LEN+1]; //Com5RecvBuf[COM5_BUF_LEN+1]; // ¡ãä¥à ¯à¥¬ 
extern uint8_t ResMODBUS;
extern struct printform  PRINTFORM;
//---------------------------------------------
struct bintime     TIME_SETTIME;        // èñïîëüçóåòñÿ ïpè óñòàíîâêå âpåìåíè

long ta1,ta2,tb1,tb2;           // äëÿ çàäàíèÿ çàäåðæêè
int sms_send;
float q2;
extern int   od2;
extern char Bterm,Bq;
//extern char hart_buf_out[],hart_buf_out2[];     // áóôåp ïåpåäà÷è
//extern int  InHartBufCount,InHartBufCount2;         // ñìåùåíèå â êàäpå äëÿ HART-ìîäåìà (ïpèåì)
//extern char hartnum2,hartnums,hartnum;
extern unsigned int      rs485num;       // êîë-âî äàò÷èêîâ äëÿ RS485
//extern int noAsk;
extern unsigned int  CodeFSn[3][16];
extern unsigned int  codeAlarmFS[],codeAlarmFSK[],codeNoAlarmFSK[];
extern unsigned int codeNoAlarmFS[];
extern char ErStatus[3][16], NoErStatus[3][16];
extern unsigned int SendCount;
extern uint8_t BufTrans1[];
extern unsigned int outindex1;
extern unsigned char errMODBUS[];
extern unsigned int errMB[],errQw[];
extern int Tal1[3],Tal2[3],Twr1[3],Twr2[3];
extern unsigned char Com5SendBuf[];              // áóôåð ïîñûëêè
extern char ComH[];
//extern int nv;
extern unsigned long int Vr1[],Vws1[],VrlS[];
extern double   Vpo[3];// îáúåì çà âðåìÿ âûêëþ÷åíèÿ âû÷èñëèòåëÿ
extern unsigned char run_SMS;
extern int    hartcount3,hartcount2,hartcount1;             // òåêóùèé íîìåp äàò÷èêà â ñïèñêå äëÿ ñêàíèpîâàíèÿ
extern uint8_t mRun, nUZS; // íîìåð íèòêè è ïîðÿäêîâûé íîìåð ÓÇÑ
extern unsigned int ModbusLen;
extern double  Vwsr[]; //Vthr,
extern unsigned int  Dostup,Dostup1;
//extern char numlogin,numlogin1,km;
extern unsigned char  Tlogin[];
extern unsigned char Tlogin1[];
extern double VrtS[];
extern int cWatchDog,nWatch;
extern struct memsecurit   MEMSECUR;
/*--------------- íà÷àëî îïèñàíèÿ ñòpóêòópû ê ôóíêöèè 0x56 ----------------*/
//extern GornalPassw LogPassw[6];   //6*15=90 áàéò c 4096 áàéòà ñòð. 81
extern struct sint IS;
extern char attempt[],attempt1[];
extern int fMB; // îïðåäåëÿåò ôóíêöèþ ÷òåíèÿ ïî MODBUS (1-ñòàòóc, 2-îáúåì, 3-ðàñõîä)
extern unsigned char mcmd0[6];   // 48 bit
extern unsigned char mdatar[514];
extern unsigned char mdataw[514];
extern long Nb1;
extern unsigned char   BufTrans2[];
extern unsigned int    outindex2;
extern unsigned int    SendCount2;
extern char Bnew[];
extern int WrMgnAl[],WrPerAl[];
extern int KSD;
extern struct conf ConfBlock;
extern long TLastAl[];
extern unsigned int buf_err[2][3];
extern float  AmountQru[3][6];
extern int Amk[3];
extern char PASSWR2[];
extern char PASSWR1[];
extern char SDMgn2,SDMgn1,SDPer1,SDPer2;
extern unsigned long OFFsetmgn,OFFsetper;
extern int NoWriteMgn;
extern float Vru1[3],Vs1[3];
extern unsigned long int StFl;
extern char MgnAlarm;
extern char ErrMODBUSQw[3];
extern unsigned  char lCom4IntHF;

uint8_t str90[90];
extern  float SumHeat[2],SumRo[2],SumCO2[2],SumN2[2];
extern  int KHeat[2],KRo[2],KCO2[2],KN2[2];
extern  char FBCom;
extern double  BMIX[];
extern float TOLD[];
extern int IniSD;
extern float Dmin;//0.000001;   //ìîëÿðíàÿ ïëîòíîñòü ãàçà íèçøàÿ
extern float Dmax;//40.0;    //ìîëÿðíàÿ ïëîòíîñòü ãàçà âûñøàÿ
extern unsigned char Runed_1_3;
//extern unsigned char errRv[];
extern float StartRe[];
extern float XJM[3][21];
extern unsigned char QstErrFlag;
extern int m_nDaySummer;                // äåíü ïåðåõîäà íà ëåòíåå âðåìÿ, 0 - åñëè ïåðåõîä çàïðåùåí
extern int m_nDayWinter;                // äåíü ïåðåõîäà íà çèìíåå âðåìÿ, 0 - åñëè ïåðåõîä çàïðåùåí
extern struct DataWithCRC Persist_m_data;
extern unsigned char HartSensReady;     // ãîòîâíîñòü HART-äàò÷èêîâ
uint8_t buf_inMB;
int sp1;
char fC;
struct glconfig    GLCONFIG;
extern struct glconfig    RES_GLCONFIG;
extern struct tdata        TDATA;              // ¤«ï áã¬¬¨p®¢ ­¨ï ¤ ­­ëå ¯¥à¨®¤¨ç¥áª¨å
extern struct tdata        HDATA;              // ¤«ï áã¬¬¨p®¢ ­¨ï ¤ ­­ëå ç á®¢ëå


struct IdRun CONFIG_IDRUN[3];
struct AddCfg       ADCONFIG;           // äîïîëíèòåëüíàÿ êîíôèãóðàöèÿ
struct AdRun CONFIG_ADRUN[3]; 
struct glconfig *MainCfg;
struct IdRun  *IdRn;
unsigned char AnCom2,AnCom5; // ïðèçíàê íàëè÷èÿ COM2
unsigned char AnCom1; // ïðèçíàê íàëè÷èÿ COM1
unsigned char LatNRun[3][20];
unsigned char NeedCom1;                 // âçÿòü ÑÎÌ1 ïîä ñâÿçü
unsigned char CalcStartFlag = 0;        // ïðèçíàê çàïóñêà ðàñ÷åòà äèàôðàãìû
unsigned char CorrStartFlag = 0;        // ïðèçíàê çàïóñêà ðàñ÷åòà ñ÷åò÷èêà
unsigned char CalcReadyFlag[3]={0,0,0}; // ïðèçíàê ãîòîâíîñòè äàííûõ ðàñ÷åòà
char TPlat=3;                           // òèï ïëàòû =0(S), =1(N), = 3(K)
unsigned char AvSig;
unsigned char sh[4]={0x20,0x20,0x20,0x20};
unsigned char EvenFlagRMA[3];
float  Kki; // êîýôôèöèåíò êîððåêöèè èíòåðâàëà ìåæ èìï
char Bdisp;
unsigned int Obs_ss[4]; // ñèãíàë "Òðåáóåòñÿ îáñëóæèâàíèå ó/ç ñ÷åò÷èêà"
unsigned int Obs_err[5];
char StartCPU;
int   starttime;   
unsigned int Flash_ROM;
unsigned char firstMB[3], firstER[3], firstRun;
float dVal1[3],dVFul1[3],dVal2[3],dVFul2[3];
double Vrual1[3],Vful1[3]; // ïðåäûäóùåå çíà÷åíèå ïðèðàùåíèÿ îáúåìà ðó ÓÇÑ
struct bintime OpTimeB;
int port2 = 0x2F8;
unsigned long int StartTim;
unsigned long int t3;
char NoWriteCycle[3];
unsigned int ExtKoef = 3132; //äî 3248  äëÿ çàïèñè êîýôôèöèåíòîâ KoefP,KoefdP,KoefE,KoRo
//unsigned int ExtChroma = 2766;// äëÿ çàïèñè õðîìàòîãàìì XI 3 ìàññèâà ïî 21*4 áàéò (252 áàéòà)
double KoefP[4]   = {1.0,0.0980665,98.0655,0.980665}; //èç êã/ñì2 â ÌÏà, êÏà èëè Áàð
double KoefdP[3]  = {1.0, 0.00980655, 0.0980665};  // èç êã/ì2 â êÏà èëè ìÁàð
double KoefEn[3]  = {238.846, 1.0, 0.277778};  // èç ÌÄæ â êêàë èëè êÂò÷
//char Densomer;
unsigned int ResPassw,ResPassw1;
//FILE *fp;
//char far *pcXI;
unsigned int ExtChroma = 2766;// äëÿ çàïèñè õðîìàòîãàìì XI 3 ìàññèâà ïî 21*4 áàéò (252 áàéòà)
uint8_t  ModbusTCP,ModbusTCP_1,ModbusRTU,ModbusRTU_1;
//  = {{ 0.9070, 0.0310, 0.0050, 0.0450, 0.0084, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0010, 0.0015, 0.0003, 0.0004, 0.0004, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
//    { 0.9650, 0.0030, 0.0060, 0.0180, 0.0045, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0010, 0.0010, 0.0005, 0.0003, 0.0007, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 },
//    { 0.8590, 0.0100, 0.0150, 0.0850, 0.0230, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0035, 0.0035, 0.0005, 0.0005, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000 }};
float XJ[3][21];
extern double Z[];
extern float XI[];
extern int CID[];
char CalcBR[3],Z_Out[3],Z_in[3];
char ic;
int NCC;
uint16_t di1s[3],di2s[3];
uint16_t  KolElS[3],KolElM[3],KolElH[3],KolElD[3];
uint16_t NumZap[3];
uint16_t SizeCfg, SizeIdR,SizeAdCfg,SizeAdR,SizeIntPar,SizeCALC;
uint16_t SizeFHP;
_Bool b_Massomer[3],b_Counter[3];
uint8_t   bBeginCalc;
uint8_t   TxCpltDMA4,  RxCpltDMA4,TxCpltDMA5,  RxCpltDMA5,TxCpltUSR3,  RxCpltUSR3; 
uint8_t  TxCpltMODBUS,RxCpltMODBUS;
int   startflag;
GPIO_PinState BUT1, BUT2;
uint8_t In_But1,In_But2;
uint8_t  SetW1,SetW2;
uint8_t  FlagW1, FlagW2;
uint16_t DelayW1, DelayW2;	

//union
//{    
// uint8_t MASS_QS[3][ARCS_B + BLARC_B];
// struct memdata As[3][ARCS+BLARC]; 
//} dataArc;
//union
//{
//    uint8_t Bl_s[BLARC_B];  // áëîê 4096 á
//    struct memdata As[BLARC]; //64 çïèñè
//} BlockAs;
uint16_t ARCS_1 = ARCS + BLARC; // ÷èñëî ñòðîê âìåñòå ñ çàïàñíûìè (64)


uint8_t AdrCfg[2] = {0,32}; //ñ 32-ãî áàéòà 224 áàéòà äëÿ glconfig
uint8_t AdrIdRun[3][2] = {1,0,2,0xC0,4,0x80};  // äî (6,0x40) ïî 448 áàéò ,
uint8_t AdrAddCfg[2] = {6,0x40}; // Àäðåñ äîïîëíèòåëüíîé êîíôèãóðàöèÿ 32 áàéòà
uint8_t AdrAdRun[3][2] = {6,0x60,7,0x00,7,0xA0}; // äî 8,0x40 //Àäðåñà Äîï. êîíôèãóðàöèÿ ïî íèò7êàì ïî 160 áàéò
uint8_t AdrIntP[2]={8,0x40}; //32 äî 8,0x60
uint8_t AdrCALC[3][2]={8,0x60,9,0xC0,0xB,0x20}; //352*3 äî 0xC,0x80
uint8_t AdrpointARch[9][2] = {0,0,0x0C,0x80,0x0C,0x8C,0x0C,0x98,0x0C,0xA4,0x0C,0xB0,
    0x0C,0xBC,0x0C,0xC8,0x0C,0xD4}; // äî (0xC,0xE0) 3 232
uint8_t AdrEraseBl[2]= {0xC,0xE0};  // äî (0xD, 0x16)            54    
uint8_t AdrSumm[3][3][2]= {0xD,0x16,0xD,0xF4,0xE,0xD2,0xf,0xB0,0x10,0x8E,
  0x11,0x6C,0x12,0x4A,0x13,0x28,0x14,0x06};//äî (0x14,0xE4) //222*3*3=1998=7CE áàéò   222=DE
uint8_t AdrFHP[2] = {0x14,0xE4}; // 252 áàéò äî (15E0)  {14e4,1538,158C} äî {15E0}
struct AddCfg     *AdCfg; // = &ADCONFIG;
struct pointARch pArchiv;
struct addrTemper addrTemper1 ={{0x00,0xA0},{0x20,0xA0}};

//RTC_HandleTypeDef hrtc;
//TIM_HandleTypeDef htim1;
//TIM_HandleTypeDef htim17;
    
uint32_t TimeReq;       // âðåìÿ îò ïîñëåäíåãî îïðîñà, ñ

    // èñïîëüçóåòñÿ ïðè ñòàðòå ñèñòåìû
int SysTakt, nRn;
unsigned char HartFormat = 17;          // ôîðìàò Hart-êîìàíäû
//unsigned char Flag_Hartservice;         // ïðèçíàê ðåæèìà òðàíñëÿöèè HART<->COM2
                  
char  RS485HF[3],RS485MB;
int             value;                  // äëÿ ñòàpòà
//unsigned int    calc_buferr[30];
struct memdata    *pmpd;
//extern int Nsp[3], Nsp1[3];
unsigned int hart2;                              //íàëè÷èå HART2
int Isum;
long SumDens;
char startT,startT1,startT2;
char Power;
unsigned char nTmp;
//unsigned char hartc;
unsigned char wminute,whours;
uint32_t TimeReq,TimeReq1;       // âðåìÿ îò ïîñëåäíåãî îïðîñà, ñ
int Nwr,NwrA,Nwr1,Nwr2; // îòêðûòèå äâåðöû âû÷èñëèòåëÿ è çàïðåò çàïèñè
//#define   RIRQ3             0xFF3E
unsigned int kn,kr,ns,bSDOff,bSDOn,bSDOff_P,bSDOff1;
float KoRo[3][3]={1.0, 1.0737565, 1.0174792,0.9313097, 1.0, 0.9475883,0.9828210,1.0553105,1.0};
//float KoRo[3][3];
int w;
long Tin;
unsigned int pZ;
//struct    Watch_mem Watchmem[5];
//unsigned long int tls;
union
{
char msc[8];
long msl[2];
} stms;
int KolAGA8;
//----------------------- äëÿ äèñïåò÷åðà çàäà÷ -----------------------------------
struct tq  TimeQueue[maxq];                    // î÷åpåäü ýëåìåíòîâ ê òàéìåpó
int LongTimeQueue=0;                    // äëèíà î÷åpåäè ýëåìåíòîâ ê òàéìåpó

pf  WorkTask;                           // óêàçàòåëü íà âûïîëíÿåìóþ ôóíêöèþ
pf  Queue[3][maxq];                     // ìàññèâ î÷åpåäåé ôóíêöèé
uint16_t LongQueue[3];                       // ìàññèâ äëèí î÷åpåäåé ôóíêöèé
pf  *ukQ;
int a;
//__IO uint8_t CmdCplt, RxCplt, TxCplt, StatusMatch, TimeOut;
uint8_t StartR;
uint8_t *pAdr;

struct {
   struct bintime LastTime;
   char b[2];
} OffTime;
struct bintime LastTime;
char EraseConf;
int bSDYes;
char utr[3],utr1[3];
//int nF1;  //ïåðåõîä ê ÑÓ 0 ãð.Ö
uint8_t CalcZc[3],Zc1[3];
char FirstC[3],FirstT[3];
char BRZ[3];
int nRnz,nRnZc,nRnB;
struct compress ZcKs[3];
float  ZcK1[3][2]={0.8,0.9,0.8,0.9,0.8,0.9};
uint8_t kT;
uint32_t _secTakt;
void setCursor(uint8_t col, uint8_t row);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
float    OdoDelta,                      // à §­®áâì
         OdoPrev,                       // ¯à¥¤ë¤ãé¥¥ §­ ç¥­¨¥ ®¡ê¥¬ 
         OdoRest;                       // ®áâ â®ª ®â ¯à¥¤ë¤ãé¥£® à áç¥â 
int      OdoImp,                        // ç¨á«® ¨¬¯ã«ìá®¢ ª ¢ë¤ ç¥
         Fod;                           // à ááâ®ï­¨¥ ¬¥¦¤ã ¨¬¯ã«ìá ¬¨
char OdoEnable;                         // à §à¥è¥­¨¥ ¢ë¤ ç¨ ¨¬¯ã«ìá®¢
int ResetHartS;
OdoStruc OdoTab[2];                     // â ¡«¨æ  ¤«ï ¢ë¤ ç¨ ¨¬¯ã«ìá®¢
int      OdoWrInd,                      // ¨­¤¥ªá ¤«ï § ¯¨á¨ ¢ â ¡«¨æã
         OdoRdInd;                      // ¨­¤¥ªá ¤«ï çâ¥­¨ï ¨§ â ¡«¨æë
unsigned char OdoNumRun;                // ­¨âª  à áç¥â  ®¤®à¨§ æ¨¨
unsigned char PrevOdoMask;              // ¬ áª  ®¤®à¨§ æ¨¨ ­  ¯à¥¤ë¤ãé¥¬ â ªâ¥
unsigned char nChan;
float Qodo[3];                          // à áå®¤ë ¤«ï ®¤®à¨§ æ¨¨ ¯® ­¨âª ¬
int   iQodo;                            // ç¨á«® ­¨â®ª ¤«ï à áç¥â  ®¡ê¥¬ 
                                        // ¨á¯®«ì§ã¥âáï ¯à¨ áâ àâ¥ á¨áâ¥¬ë

  uint32_t _i;
  uint32_t tmpreg = 0;
  uint8_t card;
	
  char buffer[512];
//  static FATFS g_sFatFs;
//  FRESULT fresult,fresult1,fresult2,fresult3;
//  FIL file;
  int len;
//  UINT bytes_written;
  uint16_t sizefile;

  uint32_t t_17, t_16;
  uint16_t tm1,tm2;
  RTC_TimeTypeDef sTime;  //= {0};
  RTC_DateTypeDef sDate; //= {0};
  RTC_AlarmTypeDef sAlarm;// = {0};

void P23K256_Write_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize) 
{
	uint8_t Instruction = 2;

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi, &Instruction, 1, HAL_MAX_DELAY);
	HAL_SPI_Transmit(hspi, Adress, 2, HAL_MAX_DELAY);  //Adress[1]
	HAL_SPI_Transmit(hspi, txData, TxSize, HAL_MAX_DELAY); 
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
}
uint8_t P23K256_Read_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *rxData, uint16_t RxSize) 
    {
	uint8_t Write_Cmd[]={0x01,0x41};
	uint8_t Instruction = 3;

//	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
//	HAL_SPI_Transmit(hspi, Write_Cmd, 2, HAL_MAX_DELAY);
//	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
    
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi, &Instruction, 1, HAL_MAX_DELAY);
	HAL_SPI_Transmit(hspi, Adress, 2, HAL_MAX_DELAY);
	HAL_StatusTypeDef sr = HAL_SPI_Receive(hspi, rxData, RxSize, HAL_MAX_DELAY); 
//        if (sr != HAL_OK) while(1);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
    return sr;
}

//---------------------------------------------------------------------------
  void Init_parchiv(uint8_t k)
  { uint8_t n;
    uint8_t Adr[2];
 //   for (k=0; k<3; k++)
    {                // çàïèñåé  
     pArchiv.di1p[k] = 1599; // 1536 + 64 ïî 64 á = 100 Ê
     pArchiv.di2p[k] = 0;
     pArchiv.di1h[k] = 1599; // 1536 + 64 ïî 64 á = 100 Ê
     pArchiv.di2h[k] = 0;
     pArchiv.di1d[k] = 447; // 384 + 64 ïî 64 á = 28 Ê
     pArchiv.di2d[k] = 0;
     pArchiv.di1Al[k] = 1535;  // 1280 + 256 ïð 16 á = 24 Ê
     pArchiv.di2Al[k] = 0;
     pArchiv.di1Au[k] = 1407; //1280 + 128 ïî 32 á = 44 Ê
     pArchiv.di2Au[k] = 0;
     pArchiv.di1Scr[k] = 1023; //896 + 128 ïî 32 á = 32 Ê
     pArchiv.di2Scr[k] = 0 ;
     pArchiv.di1AV[k] = 511; //384 + 128 ïî 32 á   = 16 Ê
     pArchiv.di2AV[k]= 0;
     pArchiv.di1s[k] = 3519; //3520;  //  3456 + 64  ïî 64 á = 220 Ê 55 ñåêòîðîâ
     pArchiv.di2s[k] = 0;
    }   
// Çàïèñü íîìåðîâ ñòåðòûõ ñåêòîðîâ
    for (n=1; n<10; n++)
      EraseBl[n][k] = 0; // ñòåðòûå áëîêè âñåõ àðõèâîâ = 0
 };

//----------------------------------------------------------------  
  void Init_arcPer(uint8_t m) //ìàñêà 001,010,011,100,101,110,111 - íàáîð íèòîê
{ uint8_t k;
  uint8_t mask=1;
    
    for (k=0; k<3; k++)
    { 
        if ((mask & m) > 0)
      {  pArchiv.di1p[k] = 1599; // 1536 + 64 ïî 64 á = 100 Ê
         pArchiv.di2p[k] = 0;
         EraseBl[PER][k] = 0; // íîìåð ñòðòîãî áëîêà
      }
      mask = mask << 1;
    }      
//îáíóëåíèå àðõèâà per 
     P23K256_Write_Data(&hspi3,&AdrpointARch[PER][0], (uint8_t*)&pArchiv.di1p[0],12);   
// Çàïèñü ñòåðòûõ ñåêòîðîâ àðõèâà PER
    P23K256_Write_Data(&hspi3,AdrEraseBl,(uint8_t*)EraseBl, sizeof(EraseBl));
}
//---------------------------------------------
//îáíóëåíèå àðõèâà hour
void Init_arcHou(uint8_t m) // m=0..7 (000 - íåò îáíóëåíèÿ, 001 -îáóëèòü 1-þ íèòêó
{  uint8_t k;
   uint8_t mask=1;
    
    for (k=0; k<3; k++)
    { 
        if ((mask & m) > 0)
      {  pArchiv.di1h[k] = 1599; // 1536 + 64 ïî 64 á = 100 Ê
         pArchiv.di2h[k] = 0;
         EraseBl[HOU][k] = 0; // íîìåð ñòðòîãî áëîêà
      }
      mask = mask << 1;
    }      
    //îáíóëåíèå àðõèâà ÷àñîâûõ
     P23K256_Write_Data(&hspi3,&AdrpointARch[HOU][0], (uint8_t*)&pArchiv.di1h[k],12);   
// Çàïèñü ñòåðòûõ ñåêòîðîâ àðõèâà 
    P23K256_Write_Data(&hspi3,AdrEraseBl,(uint8_t*)EraseBl, sizeof(EraseBl));
}
///--------------------------------------------------------
//îáíóëåíèå àðõèâà Day
void Init_arcDay(uint8_t m)
{  uint8_t k;
   uint8_t mask=1;
    
    for (k=0; k<3; k++)
    { 
        if ((mask & m) > 0)
      {  pArchiv.di1d[k] = 447; // 447 = 384 + 64 ïî 64 á = 28 Ê
         pArchiv.di2d[k] = 0;
         EraseBl[DAY][k] = 0; // íîìåð ñòðòîãî áëîêà
      }
      mask = mask << 1;
    }      
//îáíóëåíèå àðõèâà day
     P23K256_Write_Data(&hspi3,&AdrpointARch[DAY][0], (uint8_t*)&pArchiv.di1d[0],12);   
// Çàïèñü ñòåðòûõ ñåêòîðîâ àðõèâà 
     P23K256_Write_Data(&hspi3,AdrEraseBl,(uint8_t*)EraseBl, sizeof(EraseBl));
}
//----------------------------------------------------
//îáíóëåíèå àðõèâà àâàðèé
void Init_arcAlarm(uint8_t m)
{  uint8_t k;
   uint8_t mask=1;
    
    for (k=0; k<3; k++)
    { 
        if ((mask & m) > 0)
      {  pArchiv.di1Al[k] = 1535;  // 1280 + 256 ïð 16 á = 24 Ê
         pArchiv.di2Al[k] = 0;
         EraseBl[ALR][k] = 0; // íîìåð ñòðòîãî áëîêà
      }
      mask = mask << 1;
    }      
//îáíóëåíèå àðõèâà alarm
     P23K256_Write_Data(&hspi3,&AdrpointARch[ALR][0], (uint8_t*)&pArchiv.di1Al[0],12);   
// Çàïèñü ñòåðòûõ ñåêòîðîâ àðõèâà 
     P23K256_Write_Data(&hspi3,AdrEraseBl,(uint8_t*)EraseBl, sizeof(EraseBl));
}
//--------------------------------------------------------
//îáíóëåíèå àðõèâà âìåøàòåëüñòâ
void Init_arcAudit(uint8_t m)
{  uint8_t k;
   uint8_t mask=1;
    
    for (k=0; k<3; k++)
    { 
        if ((mask & m) > 0)
      {  pArchiv.di1Au[k] = 1407;  // 1280 + 256 ïð 16 á = 24 Ê
         pArchiv.di2Au[k] = 0;
         EraseBl[AUD][k] = 0; // íîìåð ñòðòîãî áëîêà
      }
      mask = mask << 1;
    }      
//îáíóëåíèå àðõèâà audit
     P23K256_Write_Data(&hspi3,&AdrpointARch[AUD][0], (uint8_t*)&pArchiv.di1Au[0],12);   
// Çàïèñü ñòåðòûõ ñåêòîðîâ àðõèâà audit
    P23K256_Write_Data(&hspi3,AdrEraseBl,(uint8_t*)EraseBl, sizeof(EraseBl));
}
//-----------------------------------------------------------      
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{// ïðåðûâàíèÿ ïî òàéìåðó 5 ìñ
struct tq   *ukT;
int t;
    
   if (*(&htim->Instance)  == TIM17)
   {if (t_17==0) //(t_17==200)
	{_secTakt++;
//	 t_17=0;
     if (_secTakt == 60) 
         {_secTakt = 0; } //counttime = 0; }
//    HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
//    HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
//    HAL_RTC_GetAlarm(&hrtc, &sAlarm, RTC_ALARM_A, RTC_FORMAT_BIN);  
//    BINTIME.seconds = sTime.Seconds; 
//    BINTIME.minutes = sTime.Minutes;
//    BINTIME.hours = sTime.Hours;
//    BINTIME.date = sDate.Date;
//    BINTIME.month = sDate.Month;
//    BINTIME.year = sDate.Year;
	}
	t_17++;
    counttime++; // = t_17;
   if (Flag_Hartservice == 0)
   { if ((counttime % 4) == 0)
       ChekBufResiv1();  //¯à®¢¥àª  ¯à¨¥¬­®£® ¡ãä¥à  COM2
   }   
//    if ((counttime+2 % 4) == 0)   
//       ChekBufResiv2();  //¯à®¢¥àª  ¯à¨¥¬­®£® ¡ãä¥à  COM2
    
 //      counttime = t_17;
    if(LongTimeQueue)           //?p®??p?  ¤«?­? ®??p?¤? ? ? ©¬?p?
    { ukT = TimeQueue;
      tm1=0;  //?®«-?® ?«?¬?­?®?, ?®¤«?¦ ??? ?¤ «?­?? ?§ ®??p?¤? ? ? ©¬?p?
      for (t=0; t<LongTimeQueue; t++)
      { ukT->Time--;  //¤??p. ?p?¬?­? ­ ?®¦¤?­?? ? ®??p?¤? ???? ?«?¬?­?®?
	    if (ukT->Time == 0)
	    { InQueue(ukT->Prioritet,ukT->Task); //­®¬?p ?p?®p????  § ¤ ??
	      tm1++;
	    }
	    ukT++;
	  }
      if (tm1)//?¤ «?­?? ? ??p?§ ???? ?«?¬?­?®? ®??p?¤?
	  { if (tm1 == LongTimeQueue) LongTimeQueue=0;
	    else
	    { ukT = TimeQueue;
	      tm2 = LongTimeQueue;
	      for (t=0; t<tm2; t++)  //??p?§ ???? ?«?¬?­?®?
		  {if (ukT->Time == 0)    //®?p?¤?«?­?? ?«?¬?­?  ¤«? ?¤ «?­??
		    { LongTimeQueue--;
		      if (LongTimeQueue == 0) break;
			   memmove(ukT,ukT+1,sizeof(struct tq)*(tm2-t-1));
		      tm1--; if (tm1==0) break;
		      ukT--;  //?p®??p? , ??«? ??p?¬???­­?© ?«?¬?­? = 0
		    }
		  ukT++;
		  }
	    }
	  }
    }
  } // end  TIM17
  else  if (*(&htim->Instance)  == TIM16)
   { 
		t_16++;

//       if ((t_16 % 500) == 0)
//          InQueue(1,CalculateTemper);
   }
}
//--------------------------------------------------

void Time_GetCount(void)
{  // ïîäñ÷åò èìïóëüñîâ â ñåêóíäó îò ñ÷åò÷èêîâ ãàçà
 uint32_t val1, val2, val3; //, ct1, ct2, ct3;
// double dQw;
    long long TR;
    struct IdRun  *prun1;  
    struct IdRun  *prun2;
    struct IdRun  *prun3;
    struct AdRun  *aprun1;
    struct AdRun  *aprun2;
    struct AdRun  *aprun3;
    
    prun1 = &CONFIG_IDRUN[0];
    prun2 = &CONFIG_IDRUN[1];
    prun3 = &CONFIG_IDRUN[2];
    aprun1  = &CONFIG_ADRUN[0];
    aprun2  = &CONFIG_ADRUN[1];
    aprun3  = &CONFIG_ADRUN[2];
    
    // äëÿ ïåðâîãî ñ÷åò÷èêà Fl-1      
      if (alt1 > ct1) {          // áûëî ïåpåïîëíåíèå ñ÷åò÷èêà
	    val1 = 0xFFFFFFFF - alt1;
	    val1 += ct1;
	    val1++;
      }
      else                             // íåò ïåpåïîëíåíèÿ ñ÷åò÷èêà
        val1 = ct1-alt1;
      alt1 = ct1;

      if (val1 > 0)
      {   dQw    = (double)val1*prun1->NumTurbo; // ñåêóíäíûé îáúåì *GLCONFIG.IDRUN[0].NumTurbo)
       Persist_AddQwCountTotal(0, dQw); // óâåëè÷åíèå îáúåìà dQw
       aprun1->Vtot_l =Persist_GetQwCountTotal(0); // îáùèé îáúåì
          
        if (Interval[0] < 96)  // Â×
          dQw_1    = dQw;  // ñåêóíäíûé ðàñõîä 
      //Í× è Â×
        aprun1->Qw_s = dQw_1;
        prun1->Qw = (double)3600*dQw_1; //÷àñîâîé ðàñõîä â ðó
        if (prun1->Qw > 1.2*prun1->Qmax) prun1->Qw = 1.2*prun1->Qmax;  
        if (prun1->Qw > 0)
         Kd = pow(0.1/prun1->Qw,0.1);            
        
      }
      else 
      {    TR =  (long long)(T16 - intr[0]) - (long long)Interval[0];
          if (TR > 0)  // îò ïðîøëîãî èìïóëüñà ïðîøëî áîëüøå Intervalà
          {
           if (prun1->Qw > 0.2)
            prun1->Qw = Kd * prun1->Qw; // Óìåíüøåíèå ðàñõîäà ïðè îòñóñòâèè èìïóëüñîâ
           else  prun1->Qw = 0.0;
          }         
      }       
    // äëÿ âòîðîãî ñ÷åò÷èêà Fl-2 
      if (alt2 > ct2) {          // áûëî ïåpåïîëíåíèå ñ÷åò÷èêà
	    val2 = 0xFFFF - alt2;
	    val2 += ct2;
	    val2++;
      }
      else                             // íåò ïåpåïîëíåíèÿ ñ÷åò÷èêà
        val2 = ct2-alt2;
      alt2 = ct2;

      if (val2 > 0)
      {  dQw    = (double)val2*CONFIG_IDRUN[1].NumTurbo; // ñåêóíäíûé îáúåì *GLCONFIG.IDRUN[1].NumTurbo)
         Persist_AddQwCountTotal(1, dQw); // óâåëè÷åíèå îáúåìà dQw
         aprun2->Vtot_l =Persist_GetQwCountTotal(1); // îáùèé îáúåì
          
        if (Interval[1] < 96)  // Â×
          dQw_2    = dQw;  // ñåêóíäíûé ðàñõîä 
      //Í× è Â×
        aprun2->Qw_s = dQw_2;        
        prun2->Qw = (double)3600*dQw_2; //÷àñîâîé ðàñõîä â ðó
        if (prun2->Qw > 1.2*prun2->Qmax) prun2->Qw = 1.2*prun2->Qmax;  
        if (prun2->Qw > 0)
         Kd = pow(0.1/prun2->Qw,0.1);            
        
      }
      else if (Interval[1] > 0)
      {    TR =  (long long)(T16 - intr[1]) - (long long)Interval[1];
          if (TR > 0)  // îò ïðîøëîãî èìïóëüñà ïðîøëî áîëüøå Intervalà
          {
           if (prun2->Qw > 0.2)
            prun2->Qw = Kd * prun2->Qw;
           else  prun2->Qw = 0.0;
          }         
      }
    // äëÿ òðåòüåãî ñ÷åò÷èêà Fl-3 
//      if (alt3 > ct3) {          // áûëî ïåpåïîëíåíèå ñ÷åò÷èêà
//	    val3 = 0xFFFF - alt3;
//	    val3 += ct3;
//	    val3++;
//      }
//      else                             // íåò ïåpåïîëíåíèÿ ñ÷åò÷èêà
//        val3 = ct3-alt3;
//      alt3 = ct3;

//      if (val3 > 0)
//      {    
//         dQw    = (double)val3*CONFIG_IDRUN[2].NumTurbo; // ñåêóíäíûé îáúåì *GLCONFIG.IDRUN[2].NumTurbo)
//       Persist_AddQwCountTotal(2, dQw); // óâåëè÷åíèå îáúåìà dQw
//       aprun3->Vtot_l =Persist_GetQwCountTotal(2); // îáùèé îáúåì
//          
//        if (Interval[2] < 96)  // Â×
//          dQw_3    = dQw;  // ñåêóíäíûé ðàñõîä 
//  //Í× è Â×
//        aprun3->Qw_s = dQw_3;
//        prun3->Qw = (double)3600*dQw_3; //÷àñîâîé ðàñõîä â ðó
//        if (prun3->Qw > 1.2*prun2->Qmax) prun2->Qw = 1.2*prun2->Qmax;  
//        if (prun3->Qw > 0)
//         Kd = pow(0.1/prun3->Qw,0.1);            
//      }
//      else 
//      {    TR =  (long long)(T16 - intr[2]) - (long long)Interval[2];
//          if (TR > 0)  // îò ïðîøëîãî èìïóëüñà ïðîøëî áîëüøå Intervalà
//          {
//           if (prun3->Qw > 0.2)
//            prun3->Qw = Kd * prun3->Qw;
//           else  prun3->Qw = 0.0;
//          } 
//      }          
}
//-------------------------------------------------------
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
struct bintime    *pbintime;
    
    ct1 = count1; ct2 = count2; ct3 = count3;
     T16 = t_16;  // çàïîìíèòü ñ÷åò÷èê òàêòîâ â íà÷àëå ñåêóíäû
    pbintime = &TIME_SETTIME;
    counttime = 0;
    t_17 = 0;
    StartR = 0xFF;
   if (Time_GetFlagTime() == 1)
   { //  startT=0;
    sDate.Month =   pbintime->month;           //¬¥áïæ
    sDate.Date  =   pbintime->date;            //¤¥­ì
    sDate.Year    = pbintime->year;           //£®¤
    sTime.Hours   = pbintime->hours;          //ç á
    sTime.Minutes = pbintime->minutes;        //¬¨­ãâ 
    sTime.Seconds = pbintime->seconds;        //á¥ªã­¤ 
//  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
//  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
//  sDate.WeekDay = RTC_WEEKDAY_FRIDAY;
  if (HAL_RTC_SetDate(hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
   COPY_BINTIME = TIME_SETTIME;
   BINTIME = TIME_SETTIME;
   Time_ResFlagTime();               // á¡p®á ä« £  ãáâ ­®¢ª¨ ¢p¥¬¥­¨
 } //if (Time_GetFlagTime() == 1)
    HAL_RTC_GetTime(hrtc, &sTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(hrtc, &sDate, RTC_FORMAT_BIN);
//    HAL_RTC_GetAlarm(hrtc, &sAlarm, RTC_ALARM_A, RTC_FORMAT_BIN);  
    BINTIME.seconds = sTime.Seconds; 
    BINTIME.minutes = sTime.Minutes;
    BINTIME.hours = sTime.Hours;
    BINTIME.date = sDate.Date;
    BINTIME.month = sDate.Month;
    BINTIME.year = sDate.Year;
 // ïîäñ÷åò èìïóëüñîâ îò ñ÷åò÷èêîâ ãàçà
    Time_GetCount();
    Uart1_Watch();
}

//-------------------------------------------
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
        
}
//-----------------------------------------------
void Conf_P23K256(SPI_HandleTypeDef *hspi)
{	uint8_t Write_Cmd[]={0x01,0x41};  // Çàïèñü 01000001 â ðåãèñòð
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
	HAL_SPI_Transmit(hspi, Write_Cmd, 2, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
}

//----------------------------------------------------------
int InQueue(int n,pf func) 
{
int k;
   if (n > KQu-1)
      return(-1);
   k= LongQueue[n]; 
//   asm cli
   if (LongQueue[n] == maxq) {
//      asm sti
      return(-2);
   }
   Queue[n][k] = func;       // ®??p?¤? ??­???©
   LongQueue[n]++;
//   asm sti
   return(0);  
}
int InTimeQueue(unsigned int time,int n,pf func) {
   struct tq  *ukQ;                             // ?? § ??«? ­  ?«?¬?­? ®??p?¤? ? ? ©¬?p?
   if (n > KQu-1)
      return(-1);
   if (LongTimeQueue == maxq)
      return(-2);
   if (time == 0)
      return(-3);
//   asm cli
   ukQ = &TimeQueue[LongTimeQueue];
   ukQ->Time      = time;                // ?p?¬?
   ukQ->Prioritet = n;                   // ?p?®p???? § ¤ ??
   ukQ->Task      = func;                // ?? § ??«? ­  § ¤ ??
   LongTimeQueue++;
//   asm sti
   return(0);
}
//---------------------------------------------------------------
int InQueueInt(int n,pf func) {
   if (n > KQu-1)
      return(-1);
   if (LongQueue[n] == maxq)
      return(-2);                       // ??«? ®??p?¤? § ?®«­?­ 
   Queue[n][LongQueue[n]] = func;       // ¬ ???? ®??p?¤?© § ¤ ?
   LongQueue[n]++;
   return(0);
}
//---------------------------------------------------------------
void TimeCorrection()  //ïîä÷èñòêà õâîñòîâ ïðè âêëþ÷åíèè
{ char n_Run;

   for(n_Run = 0; n_Run < GLCONFIG.NumRun; n_Run++)
   {
         struct bintime _BINTIME = BINTIME;    
                                        // çàïèñàòü â TDATA ïåðèîäè÷åñêèé ñóììàòîð;
         Mem_Rdtdata(PER,n_Run, &TDATA);
//ñêîððåêòèðîâàòü ñèñòåìíîå âðåìÿ äî ïðåäøåñòâóþùåãî øòàòíîãî ìîìåíòå
         if (GLCONFIG.Period == 0)
            GLCONFIG.Period = 60;
         _BINTIME.seconds = 0;
         _BINTIME.minutes /= GLCONFIG.Period;
         _BINTIME.minutes *= GLCONFIG.Period;
 //ïðè íåîáõîäèìîñòè, ïðîèçâåñòè êîððåêöèþ ïåðèîäè÷åêñêîãî è ÷àñîâîãî ñóììàòîðîâ 
 // è ñäåëàòü çàïèñü â ïåðèîäè÷åñêèé îò÷åò
         if (memcmp(&TDATA.STARTTIME, &_BINTIME, sizeof(struct bintime)))
         {
             Mem_StartPeriodHour(n_Run);      // êîððåêöèÿ ñóììàòîðîâ ïðè ñòàðòå
          }
                                        // çàïèñàòü â TDATA ÷àñîâîé ñóììàòîð
         Mem_Rdtdata(HOU,n_Run, &TDATA);
//ñêîððåêòèðîâàòü ñèñòåìíîå âðåìÿ äî ïðåäøåñòâóþùåãî øòàòíîãî ìîìåíòå
         _BINTIME.seconds = 0;
         _BINTIME.minutes = 0;
 //ïðè íåîáõîäèìîñòè, ïðîèçâåñòè êîððåêöèþ ÷àñîâîãî è ñóòî÷íîãî ñóììàòîðîâ 
 // è ñäåëàòü çàïèñü â ÷àñîâîé îò÷åò
         if (memcmp(&TDATA.STARTTIME, &_BINTIME, sizeof(struct bintime))) 
         {
         Mem_StartHourDay(n_Run); }        // êîððåêöèÿ ñóììàòîðîâ ïðè ñòàðòå
                                        //  çàïèñàòü â TDATA ñóòî÷íûé ñóììàòîð
         Mem_Rdtdata(DAY, n_Run, &TDATA);
//ñêîððåêòèðîâàòü ñèñòåìíîå âðåìÿ äî ïðåäøåñòâóþùåãî øòàòíîãî ìîìåíòà
// íàêîïëåíèÿ  ñóòî÷íûõ äàííûõ       
//         TimeCorrectionForDay(&_BINTIME, GLCONFIG.ContrHour);
 //ïðè íåîáõîäèìîñòè, ïðîèçâåñòè êîððåêöèþ ñóòî÷íîãî è ìåñÿ÷íîãîñóììàòîðîâ 
 // è ñäåëàòü çàïèñü â ñóòî÷íûé îò÷åò 
         if (memcmp(&TDATA.STARTTIME, &_BINTIME, sizeof(struct bintime)))
            {
					// êîððåêöèÿ ñóììàòîðîâ ïðè ñòàðòå
            Mem_StartDayMonth(POWER_OFF_TIME, n_Run);
         }
      }
}
//-----------------------------------------------
void Task_Start_Calc(void)
{
      if ((CONFIG_IDRUN[kT].TypeRun == 4)
          || (CONFIG_IDRUN[kT].TypeRun == 5)
          || (CONFIG_IDRUN[kT].TypeRun == 10))
      {// ïðîãðàììà ðàñ÷åòà îáúåìà ãàçà â ÑÓ  äëÿ ñ÷åò÷èêà ãàçà
            InQueue(2, Task_Calc_CorrSetCalcData);
      } // if ((CONFIG_IDRUN[k/].TypeRun) == 4 || ...
      else      // äèàôðàãìà è êàðèîëèñîâûå ðàñõîäîìåðû, âëàãîìåð
      {
  // ïðîãðàììà ðàñ÷åòà ðàñõîäà äëÿ äèàôðàãìû      
         InQueue(2, TaskSetCalcData);
      }
// çàïèñü èíäåêñîâ àðõèâîâ â ÝÍÏ
   P23K256_Write_Data(&hspi3,&AdrpointARch[1][0], (uint8_t*)&pArchiv,
         sizeof(struct pointARch));  
// Çàïèñü ñòåðòûõ ñåêòîðîâ àðõèâîâ â ÝÍÏ
    P23K256_Write_Data(&hspi3,AdrEraseBl,(uint8_t*)EraseBl, sizeof(EraseBl));
      memcpy(&LastTime,&BINTIME,6);       // òåêóùåå âðåìÿ -> â âðåìÿ âûêëþ÷åíèÿ
}  // êîíåö öèêëà ïî êT   
//------------------------------------------------------------
void Calc_BRAKET(void)
{
    nRnz = Time_GetNumrun();
    BRAKET1(XI);
//    BRAKET(XI);
    BRZ[nRnz]++;    
    InQueue(2,DZOFPT_B);
}
/*void Calc_BRAKET2(void)
{
    InQueue(2,DZOFPT_B);  
    
}
*/
//------------------------------------------------------------
void CalcFHP(void)
{    int NC,n;
    
     nRnz = Time_GetNumrun();
//   Àëãîðèòì îáðàáîòêè íîâûõ ÔÕÏ
     if ((CONFIG_IDRUN[nRnz].nKcompress==3)&&((BRZ[nRnz]==0)|| (CalcBR[nRnz]))&&(nRnz < GLCONFIG.NumRun))   //AGA8
     { // ïåðåïèñü êîìïîíåíòîâ èç XJ â XI
        NC=0;
        for (n=0; n<21; n++)
        {  XI[n] = 0.0;
// âõîäíîé ìàññèâ ñîäåðæàíèÿ êîìïîíåíòîâ
//printf("XJ=%f ",XJ[nRnz][n]);
           if (XJ[nRnz][n] != 0.0)
           { NC++;
             CID[NC-1] = n;
             XI[NC-1]  = XJ[nRnz][n];
//printf("XI=%f ",XI[NC-1]);
           }
        }
//printf("BRAKET\n");
// âûçîâ ôóíêöèè ïðåäâàðèòåëüíîé îáðàáîòêè ÔÕÏ
//printf("nRnz=%d\n",nRnz);
	   Z_in[nRnz] = 1;
           if (CalcBR[nRnz]) Z_in[nRnz] = 2;
           CalcBR[nRnz] = 0;
           nRnB=nRnz;
           nRnZc = nRnz;
           NCC = NC;
           InQueue(2,Calc_BRAKET);     // XI âçÿòî èç êîíôèãóðàöèè
     }    //if ((CONFIG_IDRUN[nRnz].nKcompress==3)
     else InQueue(2,Task_Start_Calc);
}
//---------------------------------------------------------------------------
void Task_Start(void)
{
    if (kT == GLCONFIG.NumRun)  kT = 0;
    glnumrun = kT;
//  printf("Zapusk Calc kT=%d Z_in=%d\n",kT,Z_in[kT]);
     nRnz = kT;
     if ((CONFIG_IDRUN[kT].nKcompress==3) && ((Z_in[kT] == 1) || (Z_in[kT] == 2)
          || (CalcBR[kT]==1)))
      InQueue(2,CalcFHP);
     else
      InQueue(2,Task_Start_Calc);
}
//---------------------------------------------------------------------------
void SetTaskTakt(void) {                    // çàïóñêàåòñÿ â íà÷àëå ñåêóíäû
//***************************************************
     nRnz = 0;
    kT=0;
sprintf((char*)str90,"nRun=%d t_17=%d\n\r",kT,t_17);
HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_SET);   //RTS USART1 set
HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),1000);
   memcpy(&AdCfg->LASTTIME,&BINTIME,6); // çàïèñûâàåì òåêóùåå âðåìÿ (âûêëþ÷åíèÿ)
  if (bBeginCalc)  // 1-é ðàç ïðîïóñêàåì 
  { //íà 2-é ñåêóíäå ïèøåì
    if (bBeginCalc == 1)
    { bBeginCalc = 2;   
      memcpy(&AdCfg->LASTTIME,&POWER_OFF_TIME,6); // çàïèñûâàåì âðåìÿ âûêëþ÷åíèÿ
      P23K256_Write_Data(&hspi3,AdrAddCfg,(uint8_t*)AdCfg,SizeAdCfg); // çàïèñü äîï ïàðàìåòðîâ 
    }    
  }   
  else bBeginCalc = 1; 
   if (PRINTFORM.ligt) {
      PRINTFORM.ligt--;
      if (PRINTFORM.ligt == 0)       // âpåìÿ ñâå÷åíèÿ èíäèêàòîpà
         LCDresligt();
   }
     if ((In_But1 < BUTMAX)&&(In_But2 < BUTMAX))
        GlPrint();    
   
     Uart1_Watch();     
//    Uart2_Watch()
// Ðàñ÷åò ðàñõîäà è îáúåìà ãàçà
    InQueue(2,Task_Start); 
}
//---------------------------------------------------------------------------
int Time_GetNumrun(void) {                  // ÷òåíèå òåêóùåãî íîìåpà íèòêè
/*    if (GLCONFIG.NumRun == 1) glnumrun = 0;
   else if (GLCONFIG.NumRun == 2) glnumrun = counttime / 20;
   if (GLCONFIG.NumRun == 3)
  { if (BINTIME.seconds % 2 == 0)
   { glnumrun = (counttime-1) / 20;
   }
   else  glnumrun = 2;
   }
*/ glnumrun = kT;
   if (glnumrun == GLCONFIG.NumRun) glnumrun = 0;
   return glnumrun;
}
//---------------------------------------------------------------------------

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
   uint32_t _i = 0;
//   uint8_t StReg1 = 0x00, StReg2 = 0x02;  // QE = 1;
   uint16_t i,j,k,n, n_Run,nRuns,cR[3];
   uint16_t crcCfg[2],crcInt; 

    IdRn = CONFIG_IDRUN;
    t_16 = 0;
    t_17= 0;
    counttime = 0;  
    StartR = 0;
    bPowerOff = 0;
    bBeginCalc = 0;
    Bdisp = 1;
    In_But1 = 0;
    In_But2 = 0;
    SetW1 = 0;
    SetW2 = 0;
    FlagW1 = 0; FlagW2 = 0;
    DelayW1 = 0;
    DelayW2 = 0;

    for (i=0; i<3; i++)
   {
    Interval[i]=0;
    CALCDATA[i].Re = 1e6;
    StartRe[i] = 1.0e6;
      BRZ[i]=0;
      TOLD[i] = 0.0;
      Z_Out[i] = 0;
      CalcZc[i] = 0;
      Zc1[i] = 1;
      CalcBR[i] = 0;
      Z_in[i] = 1;
//      AGA_F[i] = 1;
      FirstC[i] = 0x0F;
      FirstT[i] = 0x0F;
      NoWriteCycle[i] =0x0;
      Calc_CorSetNoErr(i);
      KolElS[i]=0;
      KolElM[i]=0;
      KolElH[i]=0;
      KolElD[i]=0;
//      NumZap[i] = 0;
   }
       TxCpltDMA4 = 0;
       RxCpltDMA4 = 0;
       startflag = 0;
       RxCpltUSR3 = 0;
       TxCpltUSR3 = 0;
       ResMODBUS = 80;
      st_cntIniRecCom1 = 0;
      g_cntIniRecCom1 = 0;
      st_cntIniRecCom2 = 0;
      g_cntIniRecCom2 = 0;
      nUZS = 0;
      PRINTFORM.ligt = 15;
      Flag_Hartservice = 0;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
	DWT_Init();
   KolAGA8=0;
   SysTakt = 1;
   SizeCfg = sizeof(struct glconfig);  //224 (224)
   SizeIdR = sizeof(struct IdRun); //448(448)  
   SizeAdCfg = sizeof(struct AddCfg);  //6 (32)
   SizeAdR = sizeof(struct AdRun); //160  (160)
   SizeIntPar = sizeof(struct intpar);  //28  (32)
   SizeCALC = sizeof(struct calcdata);  //344  (352)
   SizeFHP = sizeof(XJ);  // 252;
   AdCfg = &ADCONFIG;
   MainCfg = &GLCONFIG;
//   MX_GPIO_Init();
//  MX_SPI1_Init();
//  MX_SPI2_Init();
//  MX_SPI3_Init();
 // MX_CRC_Init();
 
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  hqspi.Instance = QUADSPI;
  HAL_QSPI_DeInit(&hqspi);
  MX_QUADSPI_Init();
  HAL_QSPI_MspInit(&hqspi);
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_RTC_Init();
  MX_TIM17_Init();
  MX_USART1_UART_Init();
  //HART_start();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_QUADSPI_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
//  MX_FATFS_Init();
  MX_SPI3_Init();
  MX_CRC_Init();
  MX_TIM16_Init();
  /* USER CODE BEGIN 2 */


//Conf_P23K256(&hspi3);  // óñòàíîâêà ðåãèñòðà ñòàòóñà â ðåæèì (01)
//   config(); // çàäàíèå íà÷àëüíûõ çíà÷åíèé
//// ×òåíèå îñíîâíîé êîíôèãóðàöèè   
//   P23K256_Read_Data(&hspi3,AdrCfg,(uint8_t*)MainCfg,SizeCfg);   
//   crcCfg[0] =  Mem_Crc16((uint8_t*)MainCfg,SizeCfg-4);
//   flagspeed1 = 0;
//   IniCfgSpeed1();  
//   //flagspeed2 = 1;
////   IniCfgSpeed2();  
//   if (MainCfg->crc != crcCfg[0])
//   {// sprintf(str90,"Êîíòðîëüíà ñóììà îñí. êîíôèãóðàöèè íåâåðíà crc=%04X crcCfg=%04X\n\r Ñ÷èòàíà ðåçåðâíàÿ êîíôèãóðàöèÿ\n\r",MainCfg->crc,crcCfg[0]);
//       sprintf((char*)str90,"CRC Main CONFIG invalid! crc=%04X crcCfg=%04X Read reserve Main CONFIG\n\r",MainCfg->crc,crcCfg[0]);
//       HAL_UART_Transmit(&huart2,(uint8_t*)str90,strlen((char*)str90),1000);
//   }
//   for (n=0; n<3; n++)
//   {
//     pAdr = &AdrIdRun[n][0];
//     P23K256_Read_Data(&hspi3,pAdr,(uint8_t*)&CONFIG_IDRUN[n],SizeIdR);  
//     cR[n] = Mem_Crc16((uint8_t*)&CONFIG_IDRUN[n],SizeIdR-2);
//     if (cR[n] != CONFIG_IDRUN[n].crc)
//     { sprintf((char*)str90,"CRC Run %d CONFIG invalid! crc=%04X cR[%d]=%04X Read reserve CONFIG\n\r",n+1,(IdRn+n)->crc, n+1, cR[n]);
//       HAL_UART_Transmit_IT(&huart2,str90,strlen((char*)str90));
//     }
//   }
////   uint32_t ks = SetComSpeed((CONFIG_IDRUN[1].KUzs & 0x7F) + 3);
////   SetSpeedUZS(1);
//  
//   MX_USART3_UART_Init(); // RS-485 ÓÇÑ
//  
//// ×òåíèå äîïîëíèòåëüíîé êîíôèãóðàöèè ADCONFIG îáùåé ÷àñòè
//   P23K256_Read_Data(&hspi3,AdrAddCfg,(uint8_t*)AdCfg,SizeAdCfg);  
//   memcpy(&LastTime,&AdCfg->LASTTIME,6);
//   memcpy(&POWER_OFF_TIME,&LastTime,6);
//   crcCfg[1] = Mem_Crc16((uint8_t*)AdCfg,sizeof(struct AddCfg)-2);
//// ×òåíèå äîïîëíèòåëüíîé êîíôèãóðàöèè ïî íèòêàì CONFIG_ADRUN[n]
//   for (n=0; n<3; n++)
//   {
//     pAdr = &AdrAdRun[n][0];
//     P23K256_Read_Data(&hspi3,pAdr,(uint8_t*)&CONFIG_ADRUN[n],SizeAdR);  
//     cR[n] = Mem_Crc16((uint8_t*)&CONFIG_ADRUN[n],sizeof(struct AdRun)-2);       
//   }

//// ×òåíèå âíóòðåííèõ ïàðàìåòðîâ
//   P23K256_Read_Data(&hspi3,AdrIntP, (uint8_t*)&INTPAR, SizeIntPar);   
//   crcInt =  Mem_Crc16(&INTPAR.HartNumCyclRep,SizeIntPar-4);
//   if (INTPAR.crc != crcInt)
//   { sprintf((char*)str90,"CRC INTPARAM invalid! crc=%04X crcInt=%04X Read reserve INTPARAM\n\r",INTPAR.crc,crcInt);
//     HAL_UART_Transmit_IT(&huart2,(uint8_t*)str90,strlen((char*)str90)); 
//   }

//// ×òåíèå ðàñ÷åòíûõ äàííûõ CALCDATA   
////   for (n=0; n<3; n++)
////   {
////     P23K256_Read_Data(&hspi3,&AdrCALC[n][0], (uint8_t*)&CALCDATA[n], SizeCALC);   
////   }
//// ------------ îòëàäêà ----------------
//   NumZap[2] = CALCDATA[2].H;
//   /* Initialize all configured peripherals */

//    LiquidCrystal(GPIOD, D0_Pin, D1_Pin, D2_Pin, D3_Pin, D4_Pin, D5_Pin, D6_Pin,
//	D7_Pin);

////HAL_GPIO_WritePin(GPIOC, CARD_CS_Pin, GPIO_PIN_SET);


//   
//   _secTakt = 0;
//    lCom4IntHF = 0;  // MODBUS åñòü
//    SetRS485DataAdr();  
//     HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_SET); // ïîäñòâåòêà äèñïëåÿ ÂÊË
////     PRINTFORM.ligt = 60; 
//    rs485num = 0;
////  SetRS485FBDataAdr();
//    if (rs485num > 0) //åñòü äàò÷èêè ïî RS485 ïî ïîðòó HART3 (USART3)
//    {
////printf("RS485Com4Ini\n");
//      sp1=0x00E3;
//      RS485Com4IniHF(); //èíèöèàëèçàöèÿ îïðîñà äàò÷èêîâ
//      RS485_SetRS485Err();                 // ãáâ ­®¢ª  ¢ á®áâ®ï­¨¥ ®è¨¡ª¨
//      RS485Ini();    //îïðîñ ÀÖÏ ïî RS-485 
//    }  
////**************************   
////   for (n=0; n<3; n++) Init_parchiv(n);
////       //çàïèñü èíäåêñîâ àðõèâîâ ïîñëå èíèöèàëèçàöèè
////    P23K256_Write_Data(&hspi3,&AdrpointARch[1][0], (uint8_t*)&pArchiv,
////         sizeof(struct pointARch));   
////    P23K256_Write_Data(&hspi3,AdrEraseBl,(uint8_t*)EraseBl, sizeof(EraseBl));
////*****************************************

//// ×òåíèå õðîìàòîãðàìì ÔÕÏ   
//   for (n=0; n<3; n++)
//   {
//    if (CONFIG_IDRUN[n].nKcompress==3)
//     KolAGA8++;
//   }
//   if (KolAGA8 > 0)
//   { initialData(); // çàïîëíåíèå ìàññèâîâ ìåòîäà AGA8
//// ×òåíèå ÔÕÏ 
//    P23K256_Read_Data(&hspi3,AdrFHP,(uint8_t*)&XJ,SizeFHP);   
//   }   

//       for (j=0; j<3; j++)
//       { ZcKs[j].K = ZcK1[j][0];    ZcKs[j].Z = ZcK1[j][1];
//         CALCDATA[j].K=ZcKs[j].K; CALCDATA[j].Z=ZcKs[j].Z;
//         NumChrom[j] = NumChrom1[j];
//       }
//   CalibrateIni();                      // Çàäàòü èíòåðïîëÿöèîííûå òàáëèöû
//    inindex1 = 0; 
//    Ini_Reciv1();
////    Ini_Reciv2();
//      
//       // ñápîñ îøèáîê ãpàíèö ìåòîäîâ pàñ÷åòà
//   Calc_SetNoErr();
//   Calc_SetNoErrTypeOtb();                                      // ÀÂÀÐÈÉÍÛÅ ÎÁÚÅÌÛ
//			// îáíóëåíèå ñ÷åò÷èêîâ àâàðèé è ÷òåíèå ìèí. èíòåðâàëà
//   if (GLCONFIG.EDIZM_T.T_SU==1) T_st= 273.15;     //0 ãð.Ö èëè  GERG88
//   else if (GLCONFIG.EDIZM_T.T_SU==2)  T_st= 288.15;  // 15 ãð. Ö
//   else  T_st= 293.15;           // 20 ãð. Ö
//     nRuns=GLCONFIG.NumRun; // ÷èñëî íèòîê
//     RS485MB = 0;
//  for (i=0; i < GLCONFIG.NumRun; i++)
// {    RS485HF[i] = 0;
//     if ((CONFIG_IDRUN[i].TypeRun == 4)
//     || (CONFIG_IDRUN[i].TypeRun == 5)
//     || (CONFIG_IDRUN[i].TypeRun == 10))
//      if ((CONFIG_IDRUN[i].TypeCounter > 0) && (CONFIG_IDRUN[i].AddrMODBUS > 0))
//       { RS485HF[i] = 1; // ???? ? ??®¤-???????? ?® RS485
//	     RS485MB++; // = 1;
//       }
// }
// if (RS485MB) { mRun=0; ZapuskUZS();}
// 
//    for (i=0; i< KQu; i++)
//     LongQueue[i] = 0;
//   i = 0;
//   fC = 0;

//  uint8_t Takt;
//  Takt=0;
//  uint32_t k2 = 0;
//  for (i=0; i< GLCONFIG.NumRun; i++)
//  { b_Massomer[i]=(CONFIG_IDRUN[i].TypeRun==8    //Rotamass
//      || CONFIG_IDRUN[i].TypeRun==9);
//    b_Counter[i] = (((CONFIG_IDRUN[i].TypeRun) == 4) ||   //Ñ÷åò÷èê
//       ((CONFIG_IDRUN[i].TypeRun) == 5) ||  ((CONFIG_IDRUN[i].TypeRun) == 10));
//  }
//  HAL_TIM_Base_Start_IT(&htim17);
//  HAL_TIM_Base_Start_IT(&htim16);
////  HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN);
  InitSensor();
//  while (StartR == 0) // îæèäàíèå 1-é ñåêóíäû ñðàáàòûâàíèÿ RTC
//  { RunSensor();
//  }    
//uint32_t t1,t2,tPause;
//uint8_t D;

//  intr[0] = t_16; intr[1] = t_16; intr[2] = t_16;
//  if   (BINTIME.date < LastTime.date) 
//        D = BINTIME.date + 31 ;
//  else  D = BINTIME.date;
//  t2= (long)LastTime.date*86400L + (long)LastTime.hours*3600L+(long)(LastTime.minutes*60)+(long)LastTime.seconds;
//  t1= (long)D*86400L +(long)BINTIME.hours*3600L+(long)(BINTIME.minutes*60)+(long)BINTIME.seconds;
//  tPause=t1-t2;
//  if (tPause > 30)
//  { // âûêëþ÷åíèå ïèòàíèÿ   
//      bPowerOff = 1;
//  } 
//  else    // ãîðÿ÷èé ñòàðò
//  {
//      bPowerOff = 0;
//  }
//      if (bPowerOff==1)
//      { // âûêëþ÷åíèå ïèòàíèÿ   
//       Mem_AlarmSetPowerOff();
//       Mem_AlarmSetPowerOn();        
//      } 
//      else    // ãîðÿ÷èé ñòàðò
//      {
//       Mem_AlarmSetStop();
//       Mem_AlarmSetStart();
//      }
//   if (RS485MB > 0)
//     { StartUZS(bPowerOff);
//     }  
//  // Êîððåêöèÿ ñóììàòîðîâ ïðè âêëþ÷åíèè (ïîä÷èñòêà õâîñòîâ)
// //   TimeCorrection();

//  /* USER CODE END 2 */

//  /* Infinite loop */
//  /* USER CODE BEGIN WHILE */
  while (1)
  {
		HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_SET); // подстветка дисплея ВКЛ
//     a = LongQueue[i];
//      if (a)
//      {
//         ukQ = &Queue[i][0]; 
//         WorkTask = *ukQ;
//         a--;
//	     LongQueue[i] = a; 
//         if (a)
//            memmove(ukQ,ukQ+1,sizeof(pf) * a);
//         Queue[i][a] = 0; 
//	     WorkTask();     // ???®«­?­?? ??­???? 
//	     i = 0;
//      }
//      else
//         i = (i < (KQu-1))? (i + 1) : 0 ;

//      if (counttime > 2) fC = 0;
//      if  ((counttime < 3) && (fC == 0))
//    {      InQueue(2,SetTaskTakt);       
//        fC = 1;                      
//    }   

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    RunSensor();

  }  // end while(1)
 
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART3;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
//---------------------------------
void SetSpeed_USART3(uint32_t ks)
{
  huart3.Init.BaudRate = ks; //1200;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }    
}    

  * @brief QUADSPI Initialization Function
  * @param None
  * @retval None
  */
static void MX_QUADSPI_Init(void)
{

  /* USER CODE BEGIN QUADSPI_Init 0 */

  /* USER CODE END QUADSPI_Init 0 */

  /* USER CODE BEGIN QUADSPI_Init 1 */

  /* USER CODE END QUADSPI_Init 1 */
  /* QUADSPI parameter configuration*/
  hqspi.Instance = QUADSPI;
  hqspi.Init.ClockPrescaler = 0;
  hqspi.Init.FifoThreshold = 4;
  hqspi.Init.SampleShifting = QSPI_SAMPLE_SHIFTING_NONE;
  hqspi.Init.FlashSize = 22;
  hqspi.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_2_CYCLE;
  hqspi.Init.ClockMode = QSPI_CLOCK_MODE_0;
  if (HAL_QSPI_Init(&hqspi) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN QUADSPI_Init 2 */

  /* USER CODE END QUADSPI_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

//  RTC_TimeTypeDef sTime = {0};
//  RTC_DateTypeDef sDate = {0};
//  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only 
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 0;
  hrtc.Init.SynchPrediv = 32767;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
    
  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date */
 /*
  sTime.Hours = 0;
  sTime.Minutes = 0;
  sTime.Seconds = 0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_WEDNESDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 1;
  sDate.Year = 20;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
*/  
  /** Enable the Alarm A */
  sAlarm.AlarmTime.Hours = 0;
  sAlarm.AlarmTime.Minutes = 0;
  sAlarm.AlarmTime.Seconds = 1;
  sAlarm.AlarmTime.SubSeconds = 0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_ALL;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief SPI3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI3_Init(void)
{

  /* USER CODE BEGIN SPI3_Init 0 */

  /* USER CODE END SPI3_Init 0 */

  /* USER CODE BEGIN SPI3_Init 1 */

  /* USER CODE END SPI3_Init 1 */
  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 7;
  hspi3.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi3.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI3_Init 2 */

  /* USER CODE END SPI3_Init 2 */

}

/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM16_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = 20;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 952;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */

}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 20;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 4760;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */
  //if (GLCONFIG.Speed_1 > 7) GLCONFIG.Speed_1 = 5;
  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;

  huart1.Init.BaudRate = 38400;//115200;//1200
//if (Flag_Hartservice == 1)    
//{  huart1.Init.WordLength = UART_WORDLENGTH_9B;
//  huart1.Init.Parity = UART_PARITY_ODD;   
//}
//else
{ huart1.Init.WordLength = UART_WORDLENGTH_8B;//9B
  huart1.Init.Parity = UART_PARITY_NONE; //ODD;
}  
  huart1.Init.StopBits = UART_STOPBITS_1;
  
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, FILTR_Pin|ADC3_Pin|ADC2_Pin|ADC1_Pin 
                          |E_LCD_Pin|U1_RTS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, K1_out_Pin|U3_RTS_Pin|K2_out_Pin|SPI3_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, K3_out_Pin|K4_out_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, D0_Pin|D1_Pin|D2_Pin|D3_Pin 
                          |D4_Pin|D5_Pin|D6_Pin|D7_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, CARD_CS_Pin|CARD_VCC_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RS_LCD_GPIO_Port, RS_LCD_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LED_Pin|U2_RTS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : FL_2_EXTI_4_Pin FL_1_EXTI_5_Pin */
  GPIO_InitStruct.Pin = FL_2_EXTI_4_Pin|FL_1_EXTI_5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : FILTR_Pin ADC3_Pin ADC2_Pin ADC1_Pin 
                           U1_RTS_Pin */
  GPIO_InitStruct.Pin = FILTR_Pin|ADC3_Pin|ADC2_Pin|ADC1_Pin 
                          |U1_RTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : K1_out_Pin U3_RTS_Pin K2_out_Pin */
  GPIO_InitStruct.Pin = K1_out_Pin|U3_RTS_Pin|K2_out_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : K3_out_Pin K4_out_Pin */
  GPIO_InitStruct.Pin = K3_out_Pin|K4_out_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : K1_in_Pin check_220_Pin */
  GPIO_InitStruct.Pin = K1_in_Pin|check_220_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : K2_in_Pin K3_in_Pin K4_in_Pin */
  GPIO_InitStruct.Pin = K2_in_Pin|K3_in_Pin|K4_in_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : D0_Pin D1_Pin D2_Pin D3_Pin 
                           D4_Pin D5_Pin D6_Pin D7_Pin */
  GPIO_InitStruct.Pin = D0_Pin|D1_Pin|D2_Pin|D3_Pin 
                          |D4_Pin|D5_Pin|D6_Pin|D7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : CARD_CS_Pin CARD_VCC_Pin */
  GPIO_InitStruct.Pin = CARD_CS_Pin|CARD_VCC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : CARD_Pin */
  GPIO_InitStruct.Pin = CARD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(CARD_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : RS_LCD_Pin */
  GPIO_InitStruct.Pin = RS_LCD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(RS_LCD_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : E_LCD_Pin */
  GPIO_InitStruct.Pin = E_LCD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(E_LCD_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_Pin U2_RTS_Pin */
  GPIO_InitStruct.Pin = LED_Pin|U2_RTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : GERCON_Pin */
  GPIO_InitStruct.Pin = GERCON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GERCON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SPI3_CS_Pin */
  GPIO_InitStruct.Pin = SPI3_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(SPI3_CS_GPIO_Port, &GPIO_InitStruct);

  /**/
  HAL_I2CEx_EnableFastModePlus(I2C_FASTMODEPLUS_PB8);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
//------------------------------------------------------
static void QSPI_AutoPollingMemReady(QSPI_HandleTypeDef *hqspi,uint32_t Timeout)
{
  QSPI_CommandTypeDef     sCommand;
  QSPI_AutoPollingTypeDef sConfig;
  
 /* Configure automatic polling mode to wait for memory ready ------ */  
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = W25X_ReadStatusReg1;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_1_LINE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  sConfig.Match           = 0x00;
  sConfig.Mask            = 0x01;
  sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
  sConfig.StatusBytesSize = 1;
  sConfig.Interval        = 0x10;
  sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  if (HAL_QSPI_AutoPolling(hqspi, &sCommand, &sConfig, Timeout) != HAL_OK)
  {
    Error_Handler();
  }
}
//-------------------------------
/**
  * @brief  This function read the SR of the memory and wait the EOP.
  * @param  hqspi: QSPI handle
  * @retval None
  */
static void QSPI_AutoPollingMemReady_IT(QSPI_HandleTypeDef *hqspi)
{
  QSPI_CommandTypeDef     sCommand;
  QSPI_AutoPollingTypeDef sConfig;

  /* Configure automatic polling mode to wait for memory ready ------ */  
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = W25X_ReadStatusReg1;
  sCommand.AddressMode       = QSPI_ADDRESS_NONE;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_1_LINE;
  sCommand.DummyCycles       = 0;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  sConfig.Match           = 0x00;
  sConfig.Mask            = 0x01;
  sConfig.MatchMode       = QSPI_MATCH_MODE_AND;
  sConfig.StatusBytesSize = 1;
  sConfig.Interval        = 0x10;
  sConfig.AutomaticStop   = QSPI_AUTOMATIC_STOP_ENABLE;

  if (HAL_QSPI_AutoPolling_IT(hqspi, &sCommand, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
}
//------------------------------------------------------

/**
  * @brief  Command completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_CmdCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  CmdCplt++;
}
/**
  * @brief  Rx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_RxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  RxCplt++;
}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c1)
{   Rx23Cplt++;  }
    
/**
  * @brief  Tx Transfer completed callbacks.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_TxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
  TxCplt++; 
}

/**
  * @brief  Status Match callbacks
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_StatusMatchCallback(QSPI_HandleTypeDef *hqspi)
{
  StatusMatch++;
}

/**
  * @brief  Transfer Error callback.
  * @param  hqspi: QSPI handle
  * @retval None
  */
void HAL_QSPI_ErrorCallback(QSPI_HandleTypeDef *hqspi)
{
  Error_Handler();
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
   if (huart->Instance == USART1)
   { 
		if (Flag_Hartservice == 1)
		 HART_FinishRx();
   } 
}
//-------------------------------------------------------  
//Îêîí÷àíèå ïåðåäà÷è 
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{   HAL_StatusTypeDef ErrDMA2;
   if (huart->Instance == USART1)
   { 
       
      if (Flag_Hartservice == 1)
        HART_FinishTx();
      else
      { UART_EndTxTransfer(&huart1);
        Ini_Reciv1();
        g_cntIniRecCom1++;
      }   
   } 
   else if (huart->Instance == USART2)
   { 
       UART_EndTxTransfer(&huart2);
//       Ini_Reciv2();
       g_cntIniRecCom2++;
   } 
   else if (huart->Instance == USART3)
   {
    TxCpltUSR3++; // Ïåðåäà÷à îêîí÷åíà
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);  //RTS UART3 (HART3) ñíÿòü  
       // âêëþ÷àåì ïðèåì 
    if ((ErrDMA2 = HAL_UART_Receive_IT(&huart3, MODBUS_RecvBuf,COUNT_MODBUS)) != HAL_OK) //ResMODBUS
	{	Error_Handler(); }      
    RxCpltUSR3 = 0;
   }
}
//-----------------------------------------
void IniCfgSpeed() {
//huart1.Init.BaudRate = BaudRates_huart1[GLCONFIG.Speed];   
MX_USART1_UART_Init();
   flagspeed1 = 0;                       // ?ip«? ??aua ?pNi«oa??? ???N?N??? ?¬«p«???
}    
//----------------------------------------
// Ïðåðûâàíèÿ ïðè ïðèõîäå èìïóëüñîâ îò ñ÷åò÷èêîâ
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{  long long T; 
    uint8_t str20[20];
    
  if (GPIO_Pin== FL_1_EXTI_5_Pin)   //GPIO_PIN_5 
  { 
//Ñáðàñûâàåì ôëàã ïðåðûâàíèÿ PIN_5 (FL-1)
    __NOP(); __NOP(); __NOP();  

      count1++;
      T = t_16 - intr[0]; // èíòåðâàë ìåæäó èìïóëüñàìè â 10ìñ
      if (T < 0) Interval[0] = T + UINT32_MAX+1; // 4294967295.0;
      else Interval[0] = T;
      intr[0] = t_16;
      if (Interval[0] > 95)   // * CONFIG_IDRUN[2].NumTurbo
          dQw_1    = CONFIG_IDRUN[0].NumTurbo/((float)Interval[0]*0.001); // ñåêóíäíûé ðàñõîä
      memset(str20,0,20);
      sprintf((char*)str20,"dQw=%4.2f c1=%d",dQw_1,count1);
      LCDPrint(0, 0, (char*)str20);
  }    
  else if (GPIO_Pin== FL_2_EXTI_4_Pin)  //GPIO_PIN_4 = FI_1_EXTI_5_Pin     
  {
  //Ñáðàñûâàåì ôëàã ïðåðûâàíèÿ PIN_4 (FL-2)
    __NOP(); __NOP(); __NOP();
      count2++;
      T = t_16 - intr[1]; // èíòåðâàë ìåæäó èìïóëüñàìè â 10ìñ
      if (T < 0) Interval[1] = T + UINT32_MAX+1; // 4294967295.0;
      else Interval[1] = T;
      intr[1] = t_16;
      if (Interval[1] > 95)   // * GLCONFIG.IDRUN[2].NumTurbo
          dQw_2    = CONFIG_IDRUN[1].NumTurbo/((float)Interval[1]*0.001); // ñåêóíäíûé ðàñõîä
  }    
  else{
        __NOP();
      }

 }
//------------------------------------------------------
//Ñòèðàíèå ñåêòîðà áåç ïðåðûâàíèé
void EraseSectorW25(uint16_t TypeArch, uint8_t Run, uint16_t NumSector)
{ uint8_t QSend, Busy;
  uint32_t AdrSectErase;

  AdrSectErase =  NumSector * SectorSize;
//  Ad = AdrSectErase;
  W25QXX_Write_Enable(); // WEL=1
  if (QSPI_Send_CMD(W25X_SectorErase, AdrSectErase, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_NONE, 0)!=QSPI_OK)
         Error_Handler();
  QSPI_AutoPollingMemReady(&hqspi,TimeoutW);
} 

//Ñòèðàíèå ñåêòîðà ñ ïðåðûâàíèÿìè
void EraseSectorW25_IT(uint16_t TypeArch,  uint16_t NumSector)
{ uint8_t QSend, Busy;
  uint32_t AdrSectErase;

  CmdCplt = 0; 
  AdrSectErase = NumSector*SectorSize;
//    Ad = AdrSectErase;
  W25QXX_Write_Enable(); // WEL=1
  if (QSPI_Send_CMD_IT(W25X_SectorErase, AdrSectErase, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_NONE, 0)!=QSPI_OK)
         Error_Handler();
  while (CmdCplt == 0);// îæèäàíèå ñðàáàòûâàíèÿ ïðåðûâàíèÿ
  CmdCplt = 0;
  QSPI_AutoPollingMemReady(&hqspi,TimeoutW);
} 
//------------------------------------------------------------
// Çàïèñü ìàññèâà wrData â ïàìÿòü W25Q ñ ïðåäâàðèòåëüíûì ñòèðàíèÿ ñåêòîðà
void WriteDataW25(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *wrData)
{ uint8_t QSend, Busy;
  uint32_t AdrWrite;
  uint16_t NumSector;
   
    NumSector =  BSectorErase[TypeArch]+Run*KolSectorArch[TypeArch] + NumRec /(SectorSize / RecSizeW[TypeArch]);
    //Ad = NumSector;
    if (NumSector != EraseBl[TypeArch][Run]) // åñëè çàäàííûé ñåêòîð íå ñò¸ðò
    { EraseSectorW25(TypeArch, Run, NumSector); // òî ñòèðàíèå
      EraseBl[TypeArch][Run] = NumSector;  // ôèêñàöèÿ ôàêòà, ÷òî çàäàííûé ñåêòîð ñòåðò
    }   
    AdrWrite = (BSectorErase[TypeArch]+Run*KolSectorArch[TypeArch])*SectorSize + NumRec*RecSizeW[TypeArch];
//    Ad = AdrWrite;
    W25QXX_Write_Enable();
    QSend = QSPI_Send_CMD(W25X_PageProgram, AdrWrite, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_1_LINE, RecSizeR[TypeArch]);
   if ((QSend != 0) || (HAL_QSPI_Transmit(&hqspi, wrData,TimeoutW) != HAL_OK))
               Error_Handler();
//     if  (W25QXX_Wait_Busy() != 0)
//         Error_Handler();
   QSPI_AutoPollingMemReady(&hqspi,TimeoutW);  
}
//------------------------------------------------------------
// Çàïèñü ìàññèâà wrData â ïàìÿòü W25Q ñ ïðåäâàðèòåëüíûì ñòèðàíèÿ ñåêòîðà
uint8_t WriteDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *wrData)
{ uint8_t QSend, Busy;
  uint32_t AdrWrite;
  uint16_t NumSector;
  uint8_t rdData[64];
 
    NumSector =  BSectorErase[TypeArch]+Run*KolSectorArch[TypeArch] + NumRec /(SectorSize / RecSizeW[TypeArch]);
//    Ad = NumSector;
    if (NumSector != EraseBl[TypeArch][Run]) // åñëè çàäàííûé ñåêòîð íå ñò¸ðò
    { EraseSectorW25_IT(TypeArch, NumSector); // òî ñòèðàíèå
      EraseBl[TypeArch][Run] = NumSector;  // ôèêñàöèÿ ôàêòà, ÷òî çàäàííûé ñåêòîð ñòåðò
    }  

     AdrWrite = (BSectorErase[TypeArch]+Run*KolSectorArch[TypeArch])*SectorSize + NumRec*RecSizeW[TypeArch];
//    Ad = AdrWrite;
    TxCplt = 0;
    W25QXX_Write_Enable();
    QSend = QSPI_Send_CMD(W25X_PageProgram, AdrWrite, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_1_LINE, RecSizeR[TypeArch]);
    if ((QSend != 0) || (HAL_QSPI_Transmit_IT(&hqspi, wrData) != HAL_OK))
        return 1; //       Error_Handler();
    while (TxCplt == 0);
    TxCplt = 0;
    QSPI_AutoPollingMemReady(&hqspi,TimeoutW);
    return 0;    
    }
//--------------------------------------------------------------------------
// ×òåíèå äàííûõ èç QSPI áåç ïðåðûâàíèé
void ReadDataW25(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData)
{
  uint8_t QSend, Busy;
  uint32_t AdrReadW;

   AdrReadW= (BSectorErase[TypeArch]+Run*KolSectorArch[TypeArch])* SectorSize + NumRec*RecSizeW[TypeArch];
   
//    Ad = AdrReadW;
   St_Send=QSPI_Send_CMD(W25X_FastReadDual, AdrReadW, 8, QSPI_ADDRESS_1_LINE, QSPI_DATA_2_LINES, RecSizeR[TypeArch]);
   if ((St_Send!=0) || (HAL_QSPI_Receive(&hqspi, rdData, TimeoutW) != HAL_OK))
         Error_Handler();
}
//--------------------------------------------------------------------------
// ×òåíèå äàííûõ èç QSPI ïî IT
uint8_t ReadDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData)
{
  uint8_t QSend, Busy;
  uint32_t AdrReadW;

   RxCplt = 0;
   AdrReadW= (BSectorErase[TypeArch]+Run*KolSectorArch[TypeArch])* SectorSize + NumRec*RecSizeW[TypeArch];
   Ad = AdrReadW;
    St_Send=QSPI_Send_CMD(W25X_FastReadDual, AdrReadW, 8, QSPI_ADDRESS_1_LINE, QSPI_DATA_2_LINES, RecSizeR[TypeArch]);
   if ((St_Send!=0) || (HAL_QSPI_Receive_IT(&hqspi, rdData) != HAL_OK))
         return 1; //Error_Handler();
    while (RxCplt == 0); // îæèäàíèå êîíöà ïðèåìà
   RxCplt = 0;
   return 0;
//   QSPI_AutoPollingMemReady(&hqspi,TimeoutW);   // îæèäàíèå ñðàáàòûâàíèÿ ôëåøêè W25  
}
//--------------------------------------------------------------
void  StartUZS(int Offn)
{ int i;

      for (i=0; i < GLCONFIG.NumRun; i++)
        // Offn=1 ¤«¨â¥«ì­®áâì ¯¥à¥áâ àâ  > 20 c
//       if (CONFIG_IDRUN[i].TypeCounter == 1)
       if  ((INTPAR.Keyboard != 0) && (CONFIG_IDRUN[i].TypeCounter == 1))       // MOSCAD ASCII
         FirstReadFS(i,Offn,MODBUS_SEND_LEN_ASCII); //¯¥à¢®¥ çâ¥­¨¥ ®¡ê¥¬  ¨ à áå®¤  ¨§ “‡‘
       else                    // MOSCAD RTU
         FirstReadFS(i,Offn,MODBUS_SEND_LEN_RTU);

    }
//----------------------------------------------------------
//------------------ ¯¥à¢®¥ çâ¥­¨¥ ®¡ê¥¬   ¨§ “‡‘ FlowSic
void  FirstReadFS(int n_Run, int On,uint16_t MbLen)
{//  unsigned long int Vs;
  int nerr;
  double *v;
  double Vthr;
//  char far *pENP;

  if (CONFIG_IDRUN[n_Run].TypeRun == 10)
   {        // ==10
//      outportb(0x1D0,BASE_CFG_PAGE); // â® ¢ë¡à âì áâà ­¨æã
//					// ¨ ¢§ïâì ¨§ 
//      pENP = (char far *)MK_FP(SEGMENT,6156+n_Run*8); // çâ¥­¨¥
//      v = &Vthr;
//      _fmemcpy(v,pENP,8);
//printf("Vthr=%lf On=%d\n",Vthr,On);
      ModbusLen = MbLen; // MODBUS_SEND_ASCII;
     if (On == 1)                 // ¥á«¨ ¤«¨â¥«ì­®áâì ¯¥à¥áâ àâ  > 20 c
     {  firstMB[n_Run] = 1;
       Vwsr[n_Run] = 0.0;
     }
      else                             //   ¥á«¨ ¯¥à¥§ ¯ãáª < 20 c,
	 Vwsr[n_Run] = Vthr;
   }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
//while (1);
memset(str90,0,90);
sprintf((char*)str90, "*** Error_UART\n\r");
HAL_UART_Transmit(&huart2,str90,sizeof(str90),1000);
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
