
#include "w25.h"

/**
 * 
 * 1 Block = 16 Sectors
 * 1 Sector = 16 Pages = 4096 Bytes
 * 1 Pages = 256 Bytes
*/

extern QSPI_HandleTypeDef hqspi;

uint16_t W25QXX_TYPE = W25Q64;
uint8_t W25QXX_BUFFER[4096];


uint8_t QSPI_Send_CMD(uint32_t instruction, uint32_t address,uint32_t dummyCycles, 
                    uint32_t addressMode, uint32_t dataMode, uint32_t dataSize)
{
    QSPI_CommandTypeDef Cmdhandler;

    Cmdhandler.Instruction        = instruction;                 	
    Cmdhandler.Address            = address;
    Cmdhandler.AlternateBytes     = 0xA0;
    Cmdhandler.AddressSize        = QSPI_ADDRESS_24_BITS;
		
    Cmdhandler.AlternateBytesSize = QSPI_ALTERNATE_BYTES_8_BITS;                             
    Cmdhandler.DummyCycles        = dummyCycles;                   
    Cmdhandler.InstructionMode    = QSPI_INSTRUCTION_1_LINE;				
    Cmdhandler.AddressMode        = addressMode;
    Cmdhandler.AlternateByteMode  = QSPI_ALTERNATE_BYTES_NONE;    					      				
    Cmdhandler.DataMode           = dataMode;
    Cmdhandler.NbData             = dataSize;            				              
    Cmdhandler.DdrMode            = QSPI_DDR_MODE_DISABLE;           	
    Cmdhandler.DdrHoldHalfCycle   = QSPI_DDR_HHC_ANALOG_DELAY;
    Cmdhandler.SIOOMode           = QSPI_SIOO_INST_EVERY_CMD;

    if(HAL_QSPI_Command(&hqspi, &Cmdhandler, 5000) != HAL_OK)
      return QSPI_ERROR;

    return QSPI_OK;
}
//------------------------------------------------------------------
uint8_t QSPI_Send_CMD_IT(uint32_t instruction, uint32_t address,uint32_t dummyCycles, 
                    uint32_t addressMode, uint32_t dataMode, uint32_t dataSize)
{
    QSPI_CommandTypeDef Cmdhandler;

    Cmdhandler.Instruction        = instruction;                 	
    Cmdhandler.Address            = address;
    Cmdhandler.AlternateBytes     = 0x00;
    Cmdhandler.AddressSize        = QSPI_ADDRESS_24_BITS;
		
    Cmdhandler.AlternateBytesSize = QSPI_ALTERNATE_BYTES_8_BITS;                             
    Cmdhandler.DummyCycles        = dummyCycles;                   
    Cmdhandler.InstructionMode    = QSPI_INSTRUCTION_1_LINE;				
    Cmdhandler.AddressMode        = addressMode;
    Cmdhandler.AlternateByteMode  = QSPI_ALTERNATE_BYTES_NONE;    					      				
    Cmdhandler.DataMode           = dataMode;
    Cmdhandler.NbData             = dataSize;            				              
    Cmdhandler.DdrMode            = QSPI_DDR_MODE_DISABLE;           	
    Cmdhandler.DdrHoldHalfCycle   = QSPI_DDR_HHC_ANALOG_DELAY;
    Cmdhandler.SIOOMode           = QSPI_SIOO_INST_EVERY_CMD;

    if(HAL_QSPI_Command_IT(&hqspi, &Cmdhandler) != HAL_OK)
      return QSPI_ERROR;

    return QSPI_OK;
}

/**
 * 				0xEF13,W25Q80
 * 				0xEF14,W25Q16
 * 				0xEF15,W25Q32
 * 				0xEF16,W25Q64
 * 				0xEF17,W25Q128
 * 				0xEF18,W25Q256
 */
uint16_t W25QXX_ReadID(void)
{
    uint8_t ID[2];
    uint16_t deviceID;

    QSPI_Send_CMD(W25X_ManufactDeviceID, 0x00, 8, QSPI_ADDRESS_1_LINE, QSPI_DATA_1_LINE, sizeof(ID));
    HAL_QSPI_Receive(&hqspi, ID, 5000);

    deviceID = (ID[0] << 8) | ID[1];

    return deviceID;
}


uint8_t W25QXX_Read(uint8_t *pBuf, uint32_t ReadAddr, uint32_t ReadSize)
{
//	QSPI_Send_CMD(W25X_FastReadData , ReadAddr, 8, QSPI_ADDRESS_1_LINE, QSPI_DATA_1_LINE, ReadSize);
   QSPI_Send_CMD(W25X_FastReadDual, ReadAddr, 8, QSPI_ADDRESS_1_LINE, QSPI_DATA_2_LINES, ReadSize);

    if(HAL_QSPI_Receive(&hqspi, pBuf, 3000) != HAL_OK)
        return W25QXX_ERROR;

    return W25QXX_OK;
}


//void 
    uint8_t W25QXX_Wait_Busy(void)
{
    uint8_t status = 1,status2=1;
    HAL_StatusTypeDef St_Res;
    uint8_t St_Send;
    while((status & 0x01) == 0x01){
       St_Send=QSPI_Send_CMD(W25X_ReadStatusReg1, 0x00, 0, QSPI_ADDRESS_NONE, QSPI_DATA_1_LINE, 1);
        St_Res=HAL_QSPI_Receive(&hqspi, &status, 5000);
//        St_Send=QSPI_Send_CMD(W25X_ReadStatusReg2, 0x00, 0, QSPI_ADDRESS_NONE, QSPI_DATA_1_LINE, 1);
//        St_Res=HAL_QSPI_Receive(&hqspi, &status2, 5000);    
        }
    return status;
}


void W25QXX_Erase_Sector(uint32_t EraseAddr)
{
    W25QXX_Write_Enable();
    W25QXX_Wait_Busy();

    QSPI_Send_CMD(W25X_SectorErase, EraseAddr, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_NONE, 1);
    W25QXX_Wait_Busy();
}


void W25QXX_Write_Page(uint8_t *pBuf, uint32_t WriteAddr, uint32_t WriteSize)
{
    if(WriteSize > W25X_PAGE_SIZE)
        return;
    
    W25QXX_Write_Enable();
    W25QXX_Wait_Busy();

    QSPI_Send_CMD(W25X_PageProgram, WriteAddr, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_1_LINE, WriteSize);
//    QSPI_Send_CMD(W25X_QuadPageProgram, WriteAddr, 8, QSPI_ADDRESS_1_LINE, QSPI_DATA_4_LINES, WriteSize);
    HAL_QSPI_Transmit(&hqspi, pBuf, 5000);
    W25QXX_Wait_Busy();
}

	/*
� * @param pBuf ������� �������� ������
� * @param WriteAddr ��������� ����� ������ (�������� 32 ����)
� * @param WriteSize ���������� ������������ ������ (�������� 32 ����)
� */
void W25QXX_Write_NoCheck(uint8_t *pBuf, uint32_t WriteAddr, uint32_t WriteSize)
{
    uint32_t pageremain = W25X_PAGE_SIZE - WriteAddr % W25X_PAGE_SIZE; //���������� ������, ���������� �� ����� ��������

    if(WriteSize <= pageremain)
        pageremain = WriteSize;

    while(1)
    {
        W25QXX_Write_Page(pBuf, WriteAddr, pageremain);

        if(WriteSize == pageremain)
            break;              //����������
        else                    
        {
            pBuf += pageremain;
            WriteAddr += pageremain;
            WriteSize -= pageremain;

            pageremain = (WriteSize > W25X_PAGE_SIZE) ? W25X_PAGE_SIZE : WriteSize;
        }
    }
}



void W25QXX_Write(uint8_t *pBuf, uint32_t WriteAddr, uint32_t WriteSize)
{
    uint32_t secpos = WriteAddr / W25X_SECTOR_SIZE; //����� �������
    uint32_t secoff = WriteAddr % W25X_SECTOR_SIZE; //��������
    uint32_t secremain = W25X_SECTOR_SIZE - secoff; //��������� �����
    uint32_t i;
    uint8_t * W25QXX_BUF = W25QXX_BUFFER;

    if(WriteSize <= secremain)
        secremain = WriteSize; 

    while(1)
    {
        W25QXX_Read(W25QXX_BUF, secpos * W25X_SECTOR_SIZE, W25X_SECTOR_SIZE); //���������� ����������� ����� �������

        for(i = 0; i < secremain; i++) 
        {
            if(W25QXX_BUF[secoff + i] != 0xFF)
                break; //
        }

        if(i < secremain) //
        {
            W25QXX_Erase_Sector(secpos * W25X_SECTOR_SIZE);

            for(i = 0; i < secremain; i++)	 
            {
                W25QXX_BUF[i + secoff] = pBuf[i];
            }

            W25QXX_Write_NoCheck(W25QXX_BUF, secpos * W25X_SECTOR_SIZE, W25X_SECTOR_SIZE); 

        }
        else
            W25QXX_Write_NoCheck(pBuf, WriteAddr, secremain); 

        if(WriteSize == secremain)
            break;
        else
        {
            secpos++;       
            secoff = 0;    

            pBuf += secremain;          
            WriteAddr += secremain;     
            WriteSize -= secremain;		

            secremain = (WriteSize > W25X_SECTOR_SIZE) ? W25X_SECTOR_SIZE : WriteSize;
        }
    }
}
