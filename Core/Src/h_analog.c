////////////////////////////////////////////////////////////////////
// ����� � ��������� ������ � ���������� ��������.
////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <math.h>

#define MAIN_ANALOG

#include "h_main.h"
#include "h_analog.h"
#include "h_mem.h"

//int buf_adc[8],buf_adc1[6],bb[4];
/* union
  { unsigned long int C;
    float Cf;
  } Cu;
*/
unsigned long int read_word(int kn);
void NewFilter(float *a_average, int *a);
extern float TAU_flt,Ros_flt;       // ��������� ��������
extern float KoRo[3][3];

/////////////////////////////////////////////////////
//unsigned AnalCodes[8][10] = {
//   { 2000, 2001, 2001, 2001, 2000, 1999, 1999, 1999, 2000, 2001},
//   { 2010, 2020, 2000, 1900, 2011, 1910, 2120, 2010, 2100, 2010},
//   { 2020, 2021, 2022, 2021, 2020, 2019, 2018, 2019, 2020, 2021},
//   { 2030, 2031, 2032, 2031, 2030, 2029, 2028, 2029, 2030, 2031},
//   { 2040, 2041, 2042, 2041, 2040, 2039, 2038, 2039, 2040, 2041},
//   { 2050, 2051, 2052, 2051, 2050, 2049, 2048, 2049, 2050, 2051},
//   { 2060, 2061, 2062, 2061, 2060, 2059, 2058, 2059, 2060, 2061},
//   { 2070, 2071, 2072, 2071, 2070, 2069, 2068, 2069, 2070, 2071}
//};
//int nChan;
//int nVal[10] = { 0, 0, 0, 0, 0, 0, 0, 0 };       // ������� �� �������
//
/////////////////////////////////////////////////////////
//unsigned ReadAnalog (unsigned char Chan) {
//   unsigned RetVal;
//
//   RetVal = AnalCodes[Chan][nVal[Chan]];
//
//   if (nVal[Chan] < 9)
//      nVal[Chan]++;
//   else
//      nVal[Chan] = 0;
//
//   return RetVal;    // ������������ ��������
//}
////////////////////////////////////////////////////////////////////
extern struct glconfig GLCONFIG;
extern struct AddCfg   ADCONFIG;
extern struct IdRun CONFIG_IDRUN[3]; // ���䨣���� �� ��⪠�
extern struct AdRun CONFIG_ADRUN[3];               //120*3=360

unsigned char SignAnalogSecond[3][5]; // ������� ������ ������ ���������� ����������
extern int N_Pdost,              // ������� ������ ������
      N_Pdost_max;           // ����� ������ ��������� ��� �������� ������
unsigned char FreqAlarm;         // ������� ������ �� ����������
void NoDensErr(void);
void SetDensErr(void);
///////////////////////////////////////////////////////
// ��������� ������������ ������������
///////////////////////////////////////////////////////
void CalibrateIni(void)
{
   int i,j,k;

   for (i=0; i<3; i++)                     // ����.����� �����
      for (j=0; j<5; j++)                  // ����.����� ��������
         SignAnalogSecond[i][j] = 2;
   for(i=0; i<3; i++)                      // ������������ ������������
      for(j=0; j<5; j++) {
     if (j<4)
         for(k=0; k<(m_nPoints-1); k++) {
            m_pCoeff[i][j][k].codeLow = CONFIG_IDRUN[i].sensData[j].aTable[k].code;
            m_pCoeff[i][j][k].valueLow = CONFIG_IDRUN[i].sensData[j].aTable[k].value;
            float diffCode = CONFIG_IDRUN[i].sensData[j].aTable[k+1].code
                             - CONFIG_IDRUN[i].sensData[j].aTable[k].code;
            m_pCoeff[i][j][k].slope = diffCode > 0 ?
               (CONFIG_IDRUN[i].sensData[j].aTable[k+1].value -
                CONFIG_IDRUN[i].sensData[j].aTable[k].value) / diffCode : 0;
         }
       else  // j=4
           for(k=0; k<(m_nPoints-1); k++) {
            m_pCoeff[i][j][k].codeLow = GLCONFIG.TauTable[k];
            m_pCoeff[i][j][k].valueLow = GLCONFIG.RoTable[k];
            float diffCode = GLCONFIG.TauTable[k+1]
                             - GLCONFIG.TauTable[k];
            m_pCoeff[i][j][k].slope = diffCode > 0 ?
               (GLCONFIG.RoTable[k+1] -
                GLCONFIG.RoTable[k]) / diffCode : 0;
         }
      }
}

//////////////////////////////////////////////////////////////////////
// ������ ���������� ������.
// ���� ����� ���������� �  �����������
// ���������� �� ������� � �������� TIMER_HZ.
// ������ ��� �������������� ����� TICKS_PER_SAMPLE �����,
// ����������� �������  m_aCode[nRun][nChan] � m_aCodeOper[nRun][nChan]
///////////////////////////////////////////////////////////////////////
/*
void CAnalogRead(int nRun)
{
   int i;
   struct IdRun *pr;
   unsigned code;
   unsigned char nChan;

   pr = &GLCONFIG.IDRUN[nRun];

   if(++m_cTick >= TICKS_PER_SAMPLE) {        // ���� ������
      m_cTick = 0;                            // �������� ����� ������

      for (i=0; i<5; i++)                     // ���� �� ����������
         if (pr->sensData[i].nInterface == tiAN_WS) {   // ��������� ���-WinSys

            switch(i) {
               case 0:
                  nChan = pr->hartadrt[0];
                  break;
               case 1:
                  nChan = pr->hartadrp[0];
                  break;
               case 2:
                  nChan = pr->hartadrdph[0];
                  break;
               case 3:
                  nChan = pr->hartadrdpl[0];
                  break;
               case 4:
                  nChan = 0x00; //��������� ������ �� 0-� ������
                  break;
            }
            code = read_analog(nChan);                  // �������� ��� � ��������� �������
//          code = ReadAnalog(nChan);                   // �������� ��� c ���������

            m_aCode[nRun][i][m_cSample] = code;         // �������� ��������
         }

      if(++m_cSample >= SAMPLES_PER_RESULT) { // ���������� ������ ����� �������
         m_pSample = m_cSample;
         m_cSample = 0;                       // �������� �� �����
      }
   }
}
*/
//////////////////////////////////////////////////////////////
// ��������� �������-������������������ ��������
// �� ������ ����� � �������
//////////////////////////////////////////////////////////////
float GetCalibrValue(unsigned char nRun, unsigned char nSens, float Code) {
   int i,j,k;
   float fTmp;
   struct IdRun *prun;
   struct AdRun *aprun;

   prun  = &CONFIG_IDRUN[nRun];
   aprun  = &CONFIG_ADRUN[nRun];
                                  // ��� HART - ������� ������ (��� � �������)
   if ((prun->sensData[nSens].nInterface == tiHART)||(prun->sensData[nSens].nInterface == tiRS485DPH))
   {
      switch (nSens) {
         case 0:  // T
            fTmp = Code;
            break;
         case 1:  //P
            fTmp  =
              (aprun->edp == 0xED) ? Code * 10.19716 :   // ��� -> ���/��2
              (aprun->edp == 0x0C) ? Code * 0.01019716 : // ��� -> ���/��2
              Code;                                     // 0x0A: ���/��2 -> ���/��2
//printf("nSens=%d edp=%02X Code=%f fTmp=%f nRun=%d\n",nSens,aprun->edp,Code,fTmp,nRun);
            break;
         case 2:     // dPh
            fTmp =
               (aprun->eddph == 0x0C) ? Code * 101.9716 : // ��� -> ���/�2
               (aprun->eddph == 0x09) ? Code * 10.0 :     // ��/��2 -> ��/�2
               (aprun->eddph == 0x0A) ? Code * 10000.0 :     // ���/��2 -> ��/�2
               Code; // 0xEE x82: ���/�2 -> ���/�2
            break;
         case 3:      //dPl
            fTmp =
               (aprun->eddpl == 0x0C) ? Code * 101.9716 : // ��� -> ���/�2
               (aprun->eddpl == 0x09) ? Code * 10.0 :     // ��/��2 -> ��/�2
               (aprun->eddpl == 0x0A) ? Code * 10000.0 :     // ���/��2 -> ��/�2               (aprun->eddpl == 0x09) ? Code * 10.0 :     // ���/��2 -> ��/�2
               Code;                                     // x82 xEE: ���/�2 -> ���/�2
            break;
      }
   }
   else
      fTmp = Code;
   for(i=0; i<(m_nPoints-1); i++) {
      if(fTmp < m_pCoeff[nRun][nSens][i].codeLow)
         break;
   }
   if (i==0)
      return
         m_pCoeff[nRun][nSens][0].valueLow +
         m_pCoeff[nRun][nSens][0].slope * (fTmp - m_pCoeff[nRun][nSens][0].codeLow);
   else
      return
         m_pCoeff[nRun][nSens][i-1].valueLow +
         m_pCoeff[nRun][nSens][i-1].slope * (fTmp - m_pCoeff[nRun][nSens][i-1].codeLow);
}

/////////////////////////////////////////////////
/*
void AnalogToCalc(int NumRun) { // ������ ���������� �������� ����� �� ���� �������
   struct IdRun      *pr;  //��p����p� �����
   struct AdRun     *apr; //��p����p� �����
   int i, MeasUnit,i_flt;
   float Value,fTmp,fTemp;
   unsigned char nChan;
   int iii;
   unsigned int iTmp,err;
//   char SensName[5];
   float Average;
   float ftmp2,Top,Bottom;

   pr = (struct IdRun *)&GLCONFIG.IDRUN[NumRun];    // ��������� �� ��p����p� �����
   apr = (struct AdRun *)&ADCONFIG.ADRUN[NumRun];   // ��������� �� ��p����p� �����
   for (i=0; i<5; i++) {                            // ���� �� ����������
      if ((pr->sensData[i].nInterface == tiAN_WS)&&(i<4)
       || (i==4)&&(pr->InterfaceDens == tiAN_WS)  ) //��������� ����������
       {  // ��������� ���-WinSys
            switch(i) {
               case 0:
                  nChan = pr->hartadrt[1];
                  break;
               case 1:
                  nChan = pr->hartadrp[1];
                  break;
               case 2:
                  nChan = pr->hartadrdph[1];
                  break;
               case 3:
                  nChan = pr->hartadrdpl[1];
                  break;
               case 4:
                  nChan = 0x00; //��������� ������ �� 0-� ������
                  break;
            }
//         NewFilter(&fTemp,(int *)&m_aCode[NumRun][i]);
         nChan=nChan | 0x10;
//        Cu.C=read_word(nChan);
          Cu.C=10.0;
//printf("Cu.C=%8X\n",Cu.C);
//        if((Cu.C==0xFFFFFFFF)||(Cu.C==0xFFFF0000))
          if ((Cu.C & 0x7F800000)== 0x7F800000)   //�������� �� NAN
        {  Cu.C=0;}
         fTemp=Cu.Cf;
//printf("fTemp=%f ",fTemp);
         ftmp2 = fTemp * 1000.0;
//ftmp2=900.0;
//printf("fTmp2=%f\n",ftmp2);
         flt_ankan[NumRun][i] = ftmp2;
         Value = GetCalibrValue(NumRun,i, ftmp2);    // ��������
//printf("Value=%f i=%d\n",Value,i);
         switch(i) {
            case 0:                             // 0 - �����p���p�
               apr->edt = 0x20;                 // ��� �����p���p�
               apr->t   = Value;                // ��������
               break;
            case 1:                             // 1 - ��������
               apr->edp = 0x0A;                 // ��� ��������
               apr->P   = Value;                // ��������
               apr->P_s   = Value;                // ��������
               break;
            case 2:                             // 2 - ������� �������
               apr->eddph = 0xEE;               // ��� ��p�. ��p�����
//             apr->eddph = 0x09;               // ��� ��p�. ��p�����
               apr->dPh   = Value;              // ��������
               apr->dPh_s   = Value;                // ��������
               break;
            case 3:                             // 3 - ������ �������
               apr->eddpl = 0xEE;               // ��� ����. ��p�����
//             apr->eddpl = 0x09;               // ��� ����. ��p�����
               apr->dPl   = Value;              // ��������
//               apr->dPl_s   = Value;                // ��������
               break;
            case 4:                             // 4 - ���������
               Top =  1.05;
               Bottom = 0.66;
              if (Value > Bottom && Value < Top) //������ ���������� ���
             { apr->ROdens = Value*KoRo[0][GLCONFIG.EDIZM_T.T_SU];
//               apr->ROchr  = Value;                // ��������
               N_Pdost = 0;                      // �������� ������� ������ ������
               if (FreqAlarm == 1)              // ���� ������ ��� ����
               { FreqAlarm = 0;                 // ����� ������ �� ����������
                 NoDensErr(void);
               } // if (FreqAlarm == 1)
             }  // if ((Value > Bottom && Value < Top)
              else  // ������ ����������
              {
//printf("N_Pdost= %d N_Pdost_max= %d FreqAlarm= %d\n",N_Pdost,N_Pdost_max,FreqAlarm);
            } //else if (Value > Bottom && Value < Top)
              Ros_flt= Value;       //��������
              TAU_flt = ftmp2;      // ���
//printf("ROdens=%f Run=%d\n",apr->ROdens,NumRun);

               break;
         }
      }
   }
}
*/
//-----------------------
/*
void NewFilter(float *a_average, int *a) {
   int amin,amax,indmin,indmax,i,j;
   float aa;

// 1. ����� ������������ � ������������� �������� �� ������� �������
   amin = a[0];
   amax = a[0];
   indmin = 0;
   indmax = 0;

   for (i=1; i<10; i++) {
      if (a[i] < amin) {
         amin = a[i];
         indmin = i;
      }
      if (a[i] > amax) {
         amax = a[i];
         indmax = i;
      }
   }

// 2. ��������� 8 ���������� �������� � ����� 1
   j = 0;
   for (i=0; i<10; i++) {
      if (i!=indmin && i!=indmax) {
         buf_adc[j] = a[i];
         j++;
      }
   }

// 3. ����� ������������ � ������������� �������� � ������������� ������� 8 �����
   amin = buf_adc[0];
   amax = buf_adc[0];
   indmin = 0;
   indmax = 0;

   for (i=1; i<8; i++) {
      if (buf_adc[i] < amin) {
         amin = buf_adc[i];
         indmin = i;
      }
      if (buf_adc[i] > amax) {
         amax = buf_adc[i];
         indmax = i;
      }
   }

// 4. ��������� 6 ���������� �������� � ����� 2
   j = 0;
   for (i=0; i<8; i++) {
      if (i!=indmin && i!=indmax) {
         buf_adc1[j] = buf_adc[i];
         j++;
      }
   }

   *a_average = 0.0;
   for (i=0; i<6; i++)
      *a_average += buf_adc1[i];
   *a_average /= 6;
}
*/