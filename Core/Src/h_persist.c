///////////////////////////////////////////////////////////
// ������������ ������ �� ���������.
///////////////////////////////////////////////////////////

//#include <dos.h>

#include "h_analog.h"
#include "h_mem.h"
#include "h_calc.h"
#include "h_persist.h"

#define PG_NUMBER BASE_CFG_PAGE         // �������� ����������������� ������
#define PG_OFFSET 4096                  // �������� �� ��������

struct DataWithCRC Persist_m_data;
extern double Vpred;
extern char  RS485HF[];
extern struct glconfig GLCONFIG;
extern struct calcdata    CALCDATA[];
/*
//---------------------------------------------------------------------------
int Persist_Read() {                    // ��������� �� ����������������� ������
   unsigned int Crc;
   unsigned  char Pg;
   Pg = inport(0x1D0);
   asm   cli                            // ���p����� �p�p������

   outportb(0x1d0,PG_NUMBER);           // ���������� ��p����� ������

   asm   push ds

   asm   mov si,PG_OFFSET               // �������� �� ������
   asm   mov ax,ds
   asm   mov es,ax                      // ������� �p�������

   _DI = (unsigned int)&Persist_m_data;

   asm   mov ax,0F800h
   asm   mov ds,ax                      // ������� ���������
   asm   cld                            // ���p����� �p� ������

   _CX = sizeof(DataWithCRC);

   asm   rep movsb                      // ������ ������ � ����p������ ������

   asm   pop ds

   asm   sti                            // p��p����� �p�p������

   Crc = Mem_Crc16(&Persist_m_data, sizeof(DataWithCRC) - 2);

// printf("Crc=%X Persist_m_data.wCrc=%X\n",Crc,Persist_m_data.wCrc);
   outportb(0x1d0,Pg);
   if (Crc == Persist_m_data.wCrc)      // ����� �������
      return 0;
   else
      return 1;
}
*/
//---------------------------------------------------------------------------
/*
void Persist_Write() {                  // �������� � ����������������� ������
   unsigned  char Pg;
   Pg = inport(0x1D0);
                                        // ��������� ������ ����������� ������
   Persist_m_data.wCrc = Mem_Crc16(&Persist_m_data, sizeof(DataWithCRC) - 2);

   if (!Mem_GetStat12V())               // ���p������ ����������p� < 8,6V
      return;

   outportb(0x1d0,PG_NUMBER);           // ��������� ��p����� ������ - ��������� ��p�����

   asm   mov di,PG_OFFSET               // �������� �� ���� �������p����

   asm   mov ax,0F800h
   asm   mov es,ax                      // ��������� ��������������� ��������

  _SI = (unsigned int)&Persist_m_data;

   asm   cld                            // ���p����� �p� ������

  _CX = sizeof(DataWithCRC);

   asm   rep movsb                      // ������ ������ � ����p������ ������
   outportb(0x1d0,Pg);
}
*/
//-----------------------------------------------------------
double Persist_GetQwCountTotal(int iRun) {

   return Persist_m_data.run[iRun].QwCountTotal;
}

//-----------------------------------------------------------
void Persist_SetQwCountTotal(int iRun, double QwCountTotal) {

   Persist_m_data.run[iRun].QwCountTotal = QwCountTotal;
//   Persist_Write();
}
//-----------------------------------------------------------
void Persist_SetQTotal(int iRun, double QTotal) {
   Persist_m_data.run[iRun].QTotal = QTotal;
//   Persist_Write();
}

//-----------------------------------------------------------
void Persist_AddQwCountTotal(int iRun, double QwCountTotal) {

   Persist_m_data.run[iRun].QwCountTotal += QwCountTotal;
//   if (( RS485HF[iRun]==0) || (GLCONFIG.IDRUN[iRun].TypeRun) != 10)
     if (Persist_m_data.run[iRun].QwCountTotal >= 10.0e+10)
     Persist_m_data.run[iRun].QwCountTotal -=10.0e+10;

//   Persist_Write();
    
}

//-----------------------------------------------------------
double Persist_GetQTotal(int iRun) {

   return Persist_m_data.run[iRun].QTotal;
}
//-----------------------------------------------------------
double Persist_GetETotal(int iRun) {

   return Persist_m_data.run[iRun].ETotal;
}

//-----------------------------------------------------------
void Persist_AddQTotal(int iRun, double QTotal) {

   Persist_m_data.run[iRun].QTotal += QTotal;
   Persist_m_data.run[iRun].ETotal += QTotal*CALCDATA[iRun].Heat;

//   Persist_Write();
}

//-----------------------------------------------------------
void Persist_SetDefault(char empR) {
  if (empR & 0x20)
   Persist_m_data.run[0].QwCountTotal = Persist_m_data.run[0].QTotal = 0.0;
  if (empR & 0x40)
   Persist_m_data.run[1].QwCountTotal = Persist_m_data.run[1].QTotal = 0.0;
  if (empR & 0x80)
   Persist_m_data.run[2].QwCountTotal = Persist_m_data.run[2].QTotal = 0.0;

  // Persist_Write();
}
//-----------------------------------------------------------
//void Persist_ReadAndSetDefault() {
//   int err;
//   err = Persist_Read();
// printf("err=%d\n",err);
// if (err != 0)
//    Persist_SetDefault();
//}
