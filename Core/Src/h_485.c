//////////////////////////////////////////////////////////////////
// ���� ���稪�� � ����䥩ᮬ RS-485.
//////////////////////////////////////////////////////////////////

#include <stdio.h>
//#include <bios.h>
//#include <mem.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "main.h"
#include "h_comm_2.h"
#include "h_hart.h"
#include "h_main.h"
#include "h_analog.h"
#include "h_mem.h"
#include "h_calc.h"
#include "h_485.h"
//#include "h_valarm.h"
//#include "ct.h"
//#include "ecp.h"
#include "h_persist.h"

//...........................................................
//#define DMACR   0xFFCA
#define  SP1BDIV  0xFF18        // ����⥫� ��� ��� ᪮��� baud rate
#define  portIn2  0xFF16        // COM4 �ਥ�
#define  portOut2 0xFF14        // COM4 ��।��
#define  IRQ_COM4 0xFF42     // ॣ���� ���뢠��� ���� ���4
//#define  portMB   0x2F8// 0x310 //       // COM5 �ਥ� � ��।��
//#define  RIRQ1   0xFF3E//0xFF3A      // ॣ���� ���뢠��� ���� ���5 (COM2)
#define  COUNT_MODBUS 80
//............................................................
#define   COM5_BUF_LEN     80

#define   COM4_BUF_LEN     80
#define   RS485_KADR_LEN    7

//...........................................................
void Com4IntComIn();    // �p��� �⢥� �� �p�p뢠���
void Com4IntComOut();   // ����� �� �p�p뢠���
//void MX_USART3_UART_Init(uint32_t);
void Result_MODBUS(); //�ନ஢ ����� �� MODBUS
void Result_MODBUS_500(); //�ନ஢ ����� �� MODBUS
void Result_MODBUS_RMA();
void Result_MODBUS_RMG();
void Result_MODBUS_Kroh();
void Result_MODBUS_GUVR();
void Result_485();      // ࠧ��� �⢥�
void RS485DataWr();     // ������� ����p����� ���祭�� � RS485DATA
                        // ����祭�� ������饣� ���祭��
float Char485ToFloat (unsigned char *p);
                        // ����஫� ���������� ���祭��
int ChkDiaT485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485);
int ChkDiaP485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485);
int ChkDiadPh485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485);
int ChkDiadPl485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485);
int ChkDiaPl485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485);
//void printAT();
void n_delay (int);
void  Com4IniSendHF();
void Swap4(uint8_t* p);
uint32_t DiffSec(struct bintime *Time1,struct bintime *Time2);
float  Mem_GetDayQ(uint8_t run);
void UART_EndRxTransfer(UART_HandleTypeDef *huart);
//...............................................
extern struct glconfig GLCONFIG;
extern struct AddCfg   ADCONFIG;
extern struct intpar   INTPAR;
//extern char   hartnum;     // ���-�� HART-���稪��
extern struct bintime  BINTIME;     // ����筠� ��p���p� �p�����
extern struct memalarm MEMALARM;    // ��p���p� ����� �p娢� ���p��
extern float           valALARM[];  // �࠭��� ���������� �� � � �
extern int   starttime;             // �p��� "p��p�⪨" ��⥬�
float TAU_flt,Ros_flt;       // ��।���� ���祭��
extern unsigned char FreqAlarm;         // �ਧ��� ���ਨ �� ���⭮����
extern char  RS485HF[];
extern float IndQs[3],Qsn[];
extern double IndDP[3],IndQ[3];
extern unsigned char CorrStartFlag;
extern int SysTakt;
extern unsigned int  counttime;
extern unsigned char whours,firstMB[], firstRun;
extern struct clock_image wtime;
extern char RS485MB;
extern uint8_t str90[90];
extern UART_HandleTypeDef huart2;
extern int sms_send;
extern int run_SMS;
extern unsigned char AnCom5;
extern unsigned char EvenFlagRMA[3];
extern float dVal1[],dVFul1[],dVal2[],dVFul2[];
extern double Vful1[],Vrual1[];
extern unsigned char   SetShelf[3];            // �ਧ��� ����窨 �� ��⪠� - ��९��
extern long QcntT;
extern int sp1;
extern unsigned char firstER[];
extern struct memsecurit     MEMSECUR;
extern unsigned char  Tlogin[];
extern float KoRo[3][3];

extern int ExtCfgBase;
extern char FirstC[];
extern   long Tin;
extern struct memdata      MEMDATA_1;   // ��p���p� ����� ���� - 26 ����
extern UART_HandleTypeDef huart3, hlpuart1;
extern uint8_t  TxCpltUSR3,  RxCpltUSR3; 
//...............................................
unsigned long int Vws1[3];
unsigned char Com4SendBuf[32];              // ���� ���뫪�
unsigned char Com4RecvBuf[COM4_BUF_LEN+1]; // ���� �६�
unsigned char Com4CopyBuf[COM4_BUF_LEN+1]; // ����� ���� �ਥ��
unsigned char Com4Kadr[RS485_KADR_LEN];  // �뤥����� ����
unsigned int  UART3_InInd;         // ������ ���� �ਥ��
unsigned int  UART3_OutInd;                  // ������ ���� ���뫪�
uint8_t MODBUS_SendBuf[32];// Com5SendBuf[32];              // ���� ���뫪�
uint8_t MODBUS_RecvBuf[COM5_BUF_LEN+1]; //Com5RecvBuf[COM5_BUF_LEN+1]; // ���� �६�
unsigned int  LPUART1_InInd;                   // ������ ���� �ਥ��
unsigned int  LPUART1_OutInd;                  // ������ ���� ���뫪�

unsigned int      rs485num;       // ���-�� ���稪�� ��� RS485
unsigned int      rs485count = 0; // ⥪騩 ����� ���稪� RS485 � 横�� ����
struct rs485data  RS485DATA[12];
struct rs485data  *p485; 
struct IdRun      *pr;            // ��p����p� ��⪨
int               n485Out;        // ���-�� ���⢥⮢ ��� 䨪�樨 �訡��
unsigned char     ReqByte;        // ⥪�騩 ���� �����
unsigned char  Err485;            // �訡�� �⢥� �� RS485
unsigned int g_cnt485;            // ���稪 ��।����� ᨬ�����
				  // ��� ����� � ����ᠭ�ﬨ �裡 �� 485
unsigned char TxRx485;
uint8_t ComH[8];//, ComT[17];
uint8_t fMB; // ��।���� �㭪�� �⥭�� �� MODBUS (1-����c, 2-��ꥬ, 3-��室)
unsigned int StatusFS[3];  // ᫮�� ����� ��� FlowSick 16 ���
unsigned long StatusFS500[3];  //  ᫮�� ����� ��� FlowSick500 32 ���
unsigned int StatusFS2[3];  //2-e ᫮�� ����� ��� Krohne
unsigned int  CodeFSn[3][16];
//FlowSic
unsigned int codeNoAlarmFS[16]={0x101,0x102,0x183,0x184,0x185,0x186,0x187,0x188,
	       0x189,0x18A,0x18B,0x18C,0x18D,0x18E,0x10F,0x191};
unsigned int codeAlarmFS[16]={0x181,0x182,0x103,0x104,0x105,0x106,0x107,0x108,
		  0x109,0x10A,0x10B,0x10C,0x10D,0x10E,0x18F,0x111};
//Krohne
unsigned int codeNoAlarmFSK[16]={0x102,0x114,0x115,0x116,0x117,0x11A,
	       0x11B,0x11C,0,0,0,0,0,0,0,0};
unsigned int codeAlarmFSK[16]={0x182,0x194,0x195,0x196,0x197,0x19A,
		  0x19B,0x19C,0,0,0,0,0,0,0,0};
unsigned int codeAlarm_RMA[4]={0x183,0x182,0x19D,0x19E};
unsigned int codeNoAlarm_RMA[4]={0x103,0x102,0x11D,0x11E};
unsigned int codeAlarm_GUVR[7]={0x179,0x178,0x19F,0x17A,0x17B,0x17C,0x17D};
unsigned int codeNoAlarm_GUVR[7]={0xF9,0xF8,0x11F,0xFA,0xFB,0xFC,0xFD};

char ErStatus[3][16], NoErStatus[3][16];
double  Qr; //,Vr2,Vr3,Vr4,Vr6,Vr_h[3],Vr_d[3];
double VrtS[3];
unsigned int L=0;
unsigned int ModbusLen;
//unsigned long int Vpred[6] ={0,999999999,999999999,4294967295,4294967295,4294967296};
unsigned long int Vpred[6] ={0,4294967295,4294967295,4294967295,4294967295,4294967295};
unsigned long int Vhi,Vr1[3],VrlS[3];
uint8_t errMODBUS[3];
unsigned int errMB[3],Al112[3]; // �訡�� �裡 ��� �� MODBUS
unsigned char ReadV[3];
//char MdV[] = {1,1,2,2,3,5,8,10,15,20,26,30,40,46,50,42,35,30,25,20,15,10,8,6,4,2,3};
int nv;
uint8_t mRun; 
double KVolumeUZS[3];
uint16_t CErrMB=20;
double  Vthr,Vwsr[3];
unsigned int st_cnt485;
unsigned   int s5,s4;
char km,AlarmUZS[3],WarnUZS[3];
long Tal1[3],Tal2[3],Twr1[3],Twr2[3];
float dVFullSum[3],dVrual[3],dVFul[3];
char CodA[4];
struct bintime TLastAl[3];
unsigned int buf_err[2][3];
unsigned  char spr,lCom4IntHF;
float t2,p2;
uint8_t ResMODBUS;
uint8_t nUZS; // ����� ����� � ���������� ����� ���

//-------------------------------------------------------------------
uint32_t  SetComSpeed(uint8_t n)
{ uint32_t s4; 
     switch (n) {
      case 0: s4 = 1200;  break;
      case 1: s4 = 2400; break;
      case 2: s4 = 4800; break;
      case 3: s4 = 9600; break;
      case 4: s4 = 19200; break;
      case 5: s4 = 38400; break;
      case 6: s4 =  57600; break;
      case 7: s4 = 115200; break;
      default: s4 = 9600;
     }     
   return s4;
}       
//....................................................................
void SetSpeedDatchMB()
{ uint8_t s;
  uint32_t s4;

   s= INTPAR.Com4Speed;
   s4 = SetComSpeed(s);
//   MX_USART3_UART_Init(s4);
}
//-------------------------------------------------------
void SetSpeedUZS(uint8_t nRun)
{ uint8_t s;
  uint32_t s4;
    
    s = (CONFIG_IDRUN[nRun].KUzs & 0x7F) + 3;
    s4 = SetComSpeed(s);
//    MX_USART3_UART_Init(s4);
}
//....................................................................
void SetSpeedRun5(uint8_t nRun)
{// unsigned   int s;
//printf("Uzs[%d]=%02X ",nRun,GLCONFIG.IDRUN[nRun].KUzs);

/*switch ((CONFIG_IDRUN[nRun].KUzs & 0x7F) + 3) {
      case 0: s5=192; break; //1200
      case 1: s5=96; break; //2400
      case 2: s5=48; break; //4800
      case 3: s5=24; break;//9600
      case 4: s5=12; break;//19200
      case 5: s5=6; break;  // 38400
      case 6: s5=4; break; //  57600
      case 7: s5=2; break; //115200
      default: s5=24;   //9600
   }
*/    
// printf("s5=%d\n",s5);
//  outportb(CT_C5LCR,0x80);           // ��p������� �� ���. ����⥫� �����
//   _AX=s5;
//   outportb(CT_C5BASE,_AL);              // ��. ���� ����⥫�
//   _AX=s5;
//   outportb(CT_C5IER,_AH);            // ��. ���� ����⥫�

}
//-------------------------------------------------------------------
//��⠭���� �����樥�� ������� ⠩��p� � p����� p����� �p������p����稪�
void RS485Com5IniHF()  //ct_C5Ini()
{ unsigned   int s,k;
/*
setvect(CT_INT1IT, ct_INTC5); // ��⠭����� ����� ���뢠��� Com5 0D
  SetSpeedRun5(mRun);
  outportb(CT_C5MCR,0x2); //DTR=0 RTS=0  COM5
  outportb(CT_C5LCR,0x03);  // �p��� ��p����   n,8,1, �����
  outportb(CT_C5FCR,0x87);  // ࠧ���� FIFO, ��� FIFO
  outportb(CT_I1CON, 0x17);    // ࠧ���� ���뢠��� �� �஢�� INT1
   s= inportb(CT_C5LSR);
   s= inportb(CT_C5BASE);
*/
}
//-------------------------------------------------------------------
/*
void interrupt Com4IntHF(...) {  //Int11
  static unsigned int Status2,Id2; //p�����p �����䨪�樨 �p�p뢠���
  unsigned int k,i;
  unsigned char b;

	Status2 = inport(CT_SP1STS);
//printf("Id=%X ",Status2);
        if (Status2 & 0x0010) outport(CT_SP1STS,(Status2 & 0xFFEF));
	Id2 =  Status2 & 0x0080;
	if (Id2){         // �ਥ�
//putchar(lCom4IntHF);
         if  (lCom4IntHF == 0)
         {
           Com4RecvBuf[Com4InInd]=inportb(portIn2);   // �⥭�� ��p�
//putchar(Com4RecvBuf[Com4InInd]);
           if (Com4InInd < COM4_BUF_LEN )
            Com4InInd++;  //���� ���p�
         }
         else
         {
           Com5RecvBuf[Com5InInd]=inportb(portIn2);   // �⥭�� ��p�
//putchar(Com5RecvBuf[Com5InInd]);
           if (Com5InInd < COM4_BUF_LEN )
           Com5InInd++;  //���� ���p�
         }
        }    //	if (Id2)  ����� �p���� ���� �� �p�p뢠��
	Id2 = inport(CT_SP1CT);
    if  ((Id2 & 0x0040) && (inport(CT_SP1STS) & 0x0040)) // ��।��
    {
     if ((Com5OutInd < ModbusLen)&&(lCom4IntHF != 0)  ||
         (Com4OutInd < ModbusLen)&&(lCom4IntHF == 0))
     {
      if  (lCom4IntHF == 0)
      {
      b = Com4SendBuf[Com4OutInd];
for (k=0; k<400; k++){};
      k=0;
      for (i=0; i<8; i++)// �ନ஢���� ���⭮�� ���
       { k=k ^ (b & 0x01);
         b=b>>1;
       }
      if (k==0)
       Id2 = (Id2 | 0x0400) & 0xFFE7;
      else
       Id2 = Id2 & 0xFBFF | 0x0018;

//printf("I %04X ",Id2);
       outport(CT_SP1CT,Id2);

      outportb(portOut2,Com4SendBuf[Com4OutInd]);
//printf("OInd=%d ",Com4OutInd);
      Com4OutInd++;
      } else    //if  (lCom4IntHF != 0)
      {
      outportb(portOut2,Com5SendBuf[Com5OutInd]);
//printf("5Ind=%d ",Com5OutInd);
      Com5OutInd++;
      }
    } // if (Com5OutInd < ModbusLen)
      else
      {
       asm sti
       i=0;
       while (!(inport(CT_SP1STS) & 0x0004)){if (i++>100) break;}
       Id2 = Id2 & 0xFEBF; //������� ��।���
       Id2 = Id2 | 0x00A0;  //p��p���� �p�p뢠��� �� �p����
//printf("R %04X ",Id2);
       g_cnt485++;
       k=s4/130;
       n_delay(k);
       outport(RTS_C3,(inport(RTS_C3) & 0xFFF7));    // ��⨥ RTS COM4
      }
    }
     if ((Com5InInd >= COM4_BUF_LEN)&&(lCom4IntHF != 0)  ||
         (Com4InInd >= COM4_BUF_LEN)&&(lCom4IntHF == 0))
        Id2 = Id2 & 0xFF5F; //�������  �ਥ�

  outport(CT_SP1CT,Id2);
//printf(" R2 %04X\n",Id2);
 _AX= 0x11;
  outportb(0xFF22,_AL);  // EOI type = 11 ���  ����஫��� ���뢠���
}
*/
//-------------------------------------------------------------------
//᪮���� � p���� p����� �p������p����稪�
void RS485Com4IniHF()         //  ����� �������� �������� � ������������� 
{
//printf("lC=%d\n",lCom4IntHF);

  SetSpeedDatchMB();   // ���稪�
  
//   outport(RTS_C3,(inport(RTS_C3) & 0xFFF7));    // ��⨥ RTS COM4
//   outport(IRQ_COM4,(inport(IRQ_COM4) & 0xFFF7));  //p��p���� IRQ COM4
//   setvect(0x11,Com4IntHF);
}
//----------------------------------------------------------
// ���樠������ ��p���� ���p�� �� �p�p뢠��� ��p����稪� MODBUS
// ���� ���� ���������� ��� �������� �� RS-485
void Com4IniSendHF()
{   unsigned int Id;
    LPUART1_InInd = 0;               // ������ ���� �ਥ��
    LPUART1_OutInd = 0;              // ������ ���� ��p����
 
  { UART3_InInd = 0;               // ������ ���� �ਥ��
   UART3_OutInd = 0;              // ������ ���� ��p����
  }
  if (lCom4IntHF == 0)  // ��� HART-�������  �� ����� (UART3)
  {   // �������� RS-485 �� UART3
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
    SetSpeedDatchMB(); 
//      SetSpeedCom4();
  }
//  else   // ���� ��� �� ������������ 
//  {  
//   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);  //RTS LPUART1 MODBUS ����������         
//   SetSpeedUZS(mRun);  // ���
//  }
}

//---------------------------------------------------------------------------
// ���樠������ ��p���� ���p�� �� �p�p뢠��� ��p����稪� �� RS-485 ���
// ��� �������� �� RS-485 �������� � ��������������� ���-RS-485
//.........................................................
void RS485Ini() {                    // ����� 横�� ��p�� ��� �� RS-485
    if (rs485num != 0) {              // �᫨ ���� 485-���稪�
      rs485count++;
      if (rs485count >= rs485num)    // ���室 �� ����
	 rs485count = 0;
				     // ����� ����� ��-�� � ����� ������
      Com4SendBuf[0] = (RS485DATA[rs485count].DevNum<<4)
			| RS485DATA[rs485count].ChanNum;
      ReqByte = Com4SendBuf[0];      // ⥪�騩 ���� �����
      Com4SendBuf[1] = 0xFF;         // ��� RTS
      Com4SendBuf[2] = 0xFF;
      ModbusLen = 2;
//printf("Buf4=%02X %02X %02X ", Com4SendBuf[0],Com4SendBuf[1],Com4SendBuf[2]);

//      memcpy(Com5SendBuf,Com4SendBuf,ModbusLen);
      InTimeQueue(40,0,Result_485);  // ���⠢��� � ��।� ࠧ��� �⢥�
      lCom4IntHF = 0; // ��।�� � �ਥ� � ����  Com4SendBuf
      Com4IniSendHF();                 // ���樠������ ��p����
   }
}
//---------------------------------------------------------------------------
// ��ॢ���� ��� 16-���� ᨬ���� (2 ����) � 楫��
unsigned int T2bToInt(unsigned char *b2)
{
  unsigned char al,ah;
  unsigned int a;
  if (b2[0]>0x40)  ah=b2[0]-0x37;
     else ah=b2[0]-0x30;
  if (b2[1]>0x40)  al=b2[1]-0x37;
     else al=b2[1]-0x30;
     a=ah*16+al;
  return a;
}
//---------------------------------------------------------
void ReadMODBUS(uint8_t LCR,char addr, unsigned int NReg,unsigned int KolReg)
{  union  
  {  unsigned int Am;
     char ch2[2];
  } Cih;
  char sc;
  int i;
  unsigned int ks;
  ComH[0]=addr;
  if (LCR < 2)
  ComH[1]=0x03; // �㭪�� �⥭�� ॣ���஢
  else
  ComH[1]=0x04; // �㭪�� �⥭�� ॣ���஢
  Cih.Am=NReg;  // ��砫�� ���� ॣ.
  ComH[2]=Cih.ch2[1];
  ComH[3]=Cih.ch2[0];
  Cih.Am=KolReg;  // ���-�� ॣ. ��� �⥭��
  ComH[4]=Cih.ch2[1];
  ComH[5]=Cih.ch2[0];
  sc=0;
 if ((LCR & 0x1)==1)
  {
  for (i=0; i<6; i++)
  sc+=ComH[i];
  ComH[6]=0xFF-sc+1; // LCR   ����஫쭠� �㬬�
  }
 else if ((LCR & 0x1)==0)
  {
   ks = Mem_Crc16(ComH,sizeof(ComH)-2);
//   *((unsigned int*)&ComH[6]) = ks;
    memcpy(&ComH[6],&ks,2);
  }
}  
//---------------------------------------------------------
void HexToASCII(uint8_t *ch, uint8_t *ct)
{ int i;
 ct[0] = 0x3A;
 for (i = 0; i< 7; i++)
 { ct[2*i+2] = (ch[i] & 0x0F)+ 0x30;
   if (ct[2*i+2] >  0x39) ct[2*i+2]+=7;
   ct[2*i+1] = ((ch[i] >> 4) & 0x0F) + 0x30;
   if (ct[2*i+1] >  0x39) ct[2*i+1]+=7;

//   printf("%2X %2X ",ct[2*i+1], ct[2*i+2]);
 }
// printf("\n");
   ct[15] = 0x0D;
   ct[16] = 0x0A;
}
//---------------------------------------------------------
unsigned char ASCIIToHex(uint8_t *ct)
{ int i,n;  //:010304413D8127 n - ����� �室��� ��ப� ��� : � 0D 0A
  unsigned char ch[65];
  unsigned char sc;

  n= (T2bToInt(ct+4)*2+8)/2;
  if (n>64) n=20;

  for (i=0; i < n; i++)
  {
    ch[i] =T2bToInt(ct+2*i);
  }
  sc=0;
  for (i=0; i<(n-1); i++)
  sc+=ch[i];
  ch[n]=0xFF-sc+1; // LCR   ����஫쭠� �㬬�
//if (fMB==4)
//printf(" fMB=%d LCR=%X LC=%X %X%X\n",fMB,ch[n],ch[n-1],*(ct+2*(n-1)),*(ct+2*(n-1)+1));
 if (ch[n-1]==ch[n]) return 1;
 else return 0;
}
//---------------------------------------------------------
//  ��ॢ���� ���� 16-���� ᨬ����(����) � 楫�� ��� �����
unsigned int T4bToInt(uint8_t *b4)
{
  char al,ah;
  unsigned int a;
  unsigned int k;

  if (b4[0]>0x40)  ah=b4[0]-0x37;
     else ah=b4[0]-0x30;
  if (b4[1]>0x40)  al=b4[1]-0x37;
     else al=b4[1]-0x30;
     a=ah*16+al;
  if (b4[2]>0x40)  ah=b4[2]-0x37;
     else ah=b4[2]-0x30;
  if (b4[3]>0x40)  al=b4[3]-0x37;
     else al=b4[3]-0x30;
     k=(ah*16+al)+a*256;

  return k;
}
//---------------------------------------------------------
//  ��ॢ���� ��ᥬ� 16-���� ᨬ�����(���) � ������� 楫�� ��� �����
unsigned long int T8bToInt(uint8_t *b8)
{
  char al,ah,i,k;
   union sf
  {
     char ch2[4];
     unsigned long int  kk;
  } Ch;

  k=3;
for (i=0; i<8; i=i+2)
{
 if (b8[i]>0x40)  ah=b8[i]-0x37;
     else ah=b8[i]-0x30;
 if (b8[i+1]>0x40)  al=b8[i+1]-0x37;
     else al=b8[i+1]-0x30;
 Ch.ch2[k]=16*ah+al;// printf("C=%X ",Cfh.ch2[i]);
 k--;
}
//  printf(" ch2=%X%X%X%X \n", Ch.ch2[0],Ch.ch2[1],Ch.ch2[2],Ch.ch2[3]);
  return Ch.kk;
}
//----------------------------------------------------------------------
float H8bToFloat(uint8_t *b8)
{
  //float r;
  unsigned int a,b,i,k;
  union sf
  {
     char ch2[4];
     float Fm;
  } Cfh;
//printf(b8);
//  printf("1- ch2=%X%X%X%X ", Cfh.ch2[0],Cfh.ch2[1],Cfh.ch2[2],Cfh.ch2[3]);
  k=3;
for (i=0; i<8; i=i+2)
{
 if (b8[i]>0x40)  a=b8[i]-0x37;
     else a=b8[i]-0x30;
 if (b8[i+1]>0x40)  b=b8[i+1]-0x37;
     else b=b8[i+1]-0x30;
 Cfh.ch2[k]=16*a+b;// printf("C=%X ",Cfh.ch2[k]);
 k--;
}
//r=Cfh.Fm;
//  printf("\n2- ch2=%X%X%X%X ", Cfh.ch2[0],Cfh.ch2[1],Cfh.ch2[2],Cfh.ch2[3]);
//  printf("Fm=%f\n", Cfh.Fm);
 return Cfh.Fm;
}

//..........................................................
void RS485IniHF() {  // �������� ������� � ��� �� ���� LPUART1 �
int  i;
struct IdRun    *prun;       // ��p����p� ��⪨

//printf("TypeCount[%d]=%d fMB=%d counttime=%d\n",mRun,GLCONFIG.IDRUN[mRun].TypeCounter,fMB,counttime);
// if (GLCONFIG.IDRUN[mRun].AddrMODBUS > 0)
  prun  = &CONFIG_IDRUN[mRun];

// for (i=1; i<100; i++); //����প� �� 0,3 ms
 if (prun->TypeCounter == 1)  // FlowSic600
 {
//  if  (INTPAR.Com1NoWrite == 0)        // MODBUS ASCII
  if  (INTPAR.Keyboard != 0)        // MODBUS ASCII=1, RTU=0
  {
   if (fMB==2)
   {    
    ReadMODBUS(1,prun->AddrMODBUS, 3003,1); // �⥭�� ����� ���
    ResMODBUS = 70;
   }       
   else if(fMB==3)
   {    
    ReadMODBUS(1,prun->AddrMODBUS, 7001,1); // �⥭�� ��室�
    ResMODBUS = 70;   
   }    
   else if(fMB==4)
//   ReadMODBUS(1,CONFIG_IDRUN[mRun].AddrMODBUS, 5016,2); // �⥭�� ��饣� ��ꥬ�
   { if ((prun->KUzs & 0x80)==0) //����� ��⮪
     ReadMODBUS(1,prun->AddrMODBUS, 5013,7); // �⥭�� ��饣� ��ꥬ� � ����. ���ࠢ�����
    else //��אַ� ��⮪
    {    
     ReadMODBUS(1,prun->AddrMODBUS, 5011,7); // �⥭�� ��饣� ��ꥬ�
     ResMODBUS = 70;
    }        
   } 
   if ((fMB>0) && (fMB<5))
   { HexToASCII(ComH, MODBUS_SendBuf); // ��ॢ�� � ASCII
      MODBUS_SendBuf[17] = 0xF0;         // ��� RTS
      MODBUS_SendBuf[18] = 'A';//0xFF;
      MODBUS_SendBuf[19] = 'B';
      MODBUS_SendBuf[20] = 0x00;

     if (fMB==4)
      InTimeQueue(50,0,Result_MODBUS);  // ���⠢��� � ��।� ࠧ��� �⢥�
     else
      InTimeQueue(20,0,Result_MODBUS);  // 5 ���⠢��� � ��।� ࠧ��� �⢥�
     ModbusLen = MODBUS_SEND_LEN;
      km=0;
//      ct_c5inis();                 // ���樠������ ��p����
     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
     SetSpeedUZS(mRun);
//     if (HAL_UART_Transmit_DMA(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK) 
      if (HAL_UART_Transmit_IT(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK)       
      {	Error_Handler();  }
   }
  }
  else if (INTPAR.Keyboard == 0)  //MODBUS RTU
  {
   if (fMB==2)
   {    
     ReadMODBUS(0,prun->AddrMODBUS, 3003,1); // �⥭�� ����� ���
     ResMODBUS = 7;
   }
   else if(fMB==3)
   { ReadMODBUS(0,prun->AddrMODBUS, 7001,1); // �⥭�� ��室�
     ResMODBUS = 9;
   }       
   else 
       if(fMB==4)
//   ReadMODBUS(0,CONFIG_IDRUN[mRun].AddrMODBUS, 5016,2); // �⥭�� ��饣� ��ꥬ�
   { if ((prun->KUzs & 0x80)==0) //����� ��⮪
     { ReadMODBUS(0,prun->AddrMODBUS, 5013,7); // �⥭�� ��饣� ��ꥬ� � ���਩���� ��ꥬ� � ����. ���ࠢ�����
       ResMODBUS = 33;
     }         
    else //��אַ� ��⮪
    {    
     ReadMODBUS(0,prun->AddrMODBUS, 5011,7); // �⥭�� ��饣� ��ꥬ� � ���਩���� ��ꥬ�
     ResMODBUS = 33;
    }    
   } 
    if ((fMB>0) && (fMB<5))
   {
      memcpy(MODBUS_SendBuf,ComH,8);
      MODBUS_SendBuf[8] = 0xFF;         // ��� RTS
      MODBUS_SendBuf[9] = 0xFF;//0xFF;
      if (fMB==4)
      InTimeQueue(25,0,Result_MODBUS);  // 5 ���⠢��� � ��।� ࠧ��� �⢥�
      else
       InTimeQueue(20,0,Result_MODBUS);
      ModbusLen = MODBUS_SEND_LEN_RMA;
// printf("Send fMB=%d mRun=%d counttime=%d\n",fMB,mRun,counttime);
      km=0;
     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
     SetSpeedUZS(mRun);
//memset(str90,0,90);
//sprintf((char*)str90, "Transmit ModbusLen=%d  s=%d c=%d\n\r", ModbusLen, BINTIME.seconds,counttime);
//HAL_UART_Transmit(&huart2,str90,sizeof(str90),1000);

//     if (HAL_UART_Transmit_DMA(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK) 
      if (HAL_UART_Transmit_IT(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK)       
      {	Error_Handler();  }
   }
  }  // if (INTPAR.Keyboard == 0) RTU
 }
 //else 
/*
 if (prun->TypeCounter == 3)// ��� Ecosonic RMA
 {
  if (fMB==2)
    ReadMODBUS(0,prun->AddrMODBUS, 3000,1); // �⥭�� ����� ���
   else if(fMB==3)
    ReadMODBUS(0,prun->AddrMODBUS, 1016,2); // �⥭�� ��室�
   else if(fMB==4)
   ReadMODBUS(0,prun->AddrMODBUS, 1000,4); // �⥭�� ��饣� ��ꥬ�
   else if(fMB==5)
    ReadMODBUS(0,prun->AddrMODBUS, 1008,4); // �⥭�� ���਩���� ��饣� ��ꥬ�

   if ((fMB>0) && (fMB<6))
   {
   memcpy(Com5SendBuf,ComH,8);
   Com5SendBuf[8] = 0xFF;         // ��� RTS
   Com5SendBuf[9] = 0xFF;
   InTimeQueue(5,0,Result_MODBUS_RMA);  // ���⠢��� � ��।� ࠧ��� �⢥�
   ModbusLen = MODBUS_SEND_LEN_RMA;
      km=0;
     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
     SetSpeedUZS(mRun);
     if (HAL_UART_Transmit_DMA(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK) 
//      if (HAL_UART_Transmit_IT(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK)       
      {	Error_Handler();  }
  }
 }
 else  if (prun->TypeCounter == 2)// ���  RMG USE-09
 {
 if (fMB==2)
    ReadMODBUS(0,prun->AddrMODBUS, 4000,22); // �⥭�� ����� ���
  else if (fMB==3)
    ReadMODBUS(0,prun->AddrMODBUS, 6230,2); // �⥭�� ��室�
   else if (fMB==4)     //  3008,12  3016
  ReadMODBUS(0,prun->AddrMODBUS, 3008,12); // �⥭�� ��饣� ��ꥬ� double
//   else if (fMB==5)
//   ReadMODBUS(0,prun->AddrMODBUS, 3008,4); // �⥭�� ���਩���� ��ꥬ� double
  if ((fMB>1) && (fMB<6))
  {
     memcpy(Com5SendBuf,ComH,8);
      Com5SendBuf[8] = 0xFF;         // ��� RTS
      Com5SendBuf[9] = 0xFF;
      if ((fMB == 2) || (fMB == 4))
       InTimeQueue(5,0,Result_MODBUS_RMG);  // ���⠢��� � ��।� ࠧ��� �⢥�
      else
      InTimeQueue(4,0,Result_MODBUS_RMG);  // ���⠢��� � ��।� ࠧ��� �⢥�
     ModbusLen = MODBUS_SEND_LEN_RMA;
      km=0;
// printf("Send fMB=%d mRun=%d counttime=%d\n",fMB,mRun,counttime);
     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
     SetSpeedUZS(mRun);
     if (HAL_UART_Transmit_DMA(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK) 
//      if (HAL_UART_Transmit_IT(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK)       
      {	Error_Handler();  }
   }
 }
 else  if (prun->TypeCounter == 4)// ���  KROHNE
 {
 if (fMB==2)
    ReadMODBUS(2,prun->AddrMODBUS, 5001,2);   //5001-Alarm  6 ���
  else if (fMB==3)
    ReadMODBUS(2,prun->AddrMODBUS, 7055,2); // �⥭�� ��室�
   else if (fMB==4)
    ReadMODBUS(2,prun->AddrMODBUS, 8003,4); // �⥭�� ��饣� ��ꥬ� double
   else if(fMB==5)
    ReadMODBUS(2,prun->AddrMODBUS, 8011,4); // �⥭�� ���਩���� ��饣� ��ꥬ�
       if ((fMB>0) && (fMB<6))
  {
     memcpy(Com5SendBuf,ComH,8);
      Com5SendBuf[8] = 0xFF;         // ��� RTS
      Com5SendBuf[9] = 0xFF;
     InTimeQueue(4,0,Result_MODBUS_Kroh);  // ���⠢��� � ��।� ࠧ��� �⢥�
     ModbusLen = MODBUS_SEND_LEN_RMA;
      km=0;
     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
     SetSpeedUZS(mRun);
     if (HAL_UART_Transmit_DMA(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK) 
//      if (HAL_UART_Transmit_IT(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK)       
      {	Error_Handler();  }
  }
}  // ==4
*/
 else  if (prun->TypeCounter == 5)// ���  ����
 {// printf("Guvr fMB=%d Adr=%d %04X\n ",fMB,CONFIG_IDRUN[mRun].AddrMODBUS,1793);
 if (fMB==2)
    ReadMODBUS(0,prun->AddrMODBUS, 1793,13);   //1793-Q, Alarm, V
       if ((fMB>0) && (fMB<3))
  {
     memcpy(MODBUS_SendBuf,ComH,8);
      MODBUS_SendBuf[8] = 0xFF;         // ��� RTS
      MODBUS_SendBuf[9] = 0xFF;
     InTimeQueue(8,0,Result_MODBUS_GUVR);  // ���⠢��� � ��।� ࠧ��� �⢥�
     ModbusLen = MODBUS_SEND_LEN_RMA;
      km=0;
     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
     SetSpeedUZS(mRun);
//     if (HAL_UART_Transmit_DMA(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK) 
      if (HAL_UART_Transmit_IT(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK)       
      {	Error_Handler();  }
  }
 }
 else  if (prun->TypeCounter == 6)// ��� Flowsic500
 {
   if (fMB==2)
    ReadMODBUS(0,prun->AddrMODBUS, 3200,4); // �⥭�� ����� ���
   else if(fMB==3)
    ReadMODBUS(0,prun->AddrMODBUS, 7001,2); // �⥭�� ��室�
   else if(fMB==4)
   ReadMODBUS(0,prun->AddrMODBUS, 4100,5); // �⥭�� ��饣� � ���਩���� ��ꥬ�
   if ((fMB>0) && (fMB<5))
   {
      memcpy(MODBUS_SendBuf,ComH,8);
      MODBUS_SendBuf[8] = 0xFF;         // ��� RTS
//      Com5SendBuf[9] = 0xFF;//0xFF;
//printf(Com5SendBuf);
      InTimeQueue(25,0,Result_MODBUS_500);  // 5 ���⠢��� � ��।� ࠧ��� �⢥�
      ModbusLen = MODBUS_SEND_LEN_RMA;
      km=0;
     HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); // ����������� RTS UART3 ��� ��������
     SetSpeedUZS(mRun);
//     if (HAL_UART_Transmit_DMA(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK) 
      if (HAL_UART_Transmit_IT(&huart3, MODBUS_SendBuf,ModbusLen) != HAL_OK)       
      {	Error_Handler();  }
   }
  }  // if (INTPAR.Keyboard == 0) RTU
 }

//}
//---------------------------------------------------------------------------
void ZapuskUZS(int nRn)
{  float Q3;
   struct IdRun    *prun;       // ��p����p� ��⪨
//   char far *pc;
   struct AdRun    *aprun;       // ������
    fMB = 0;
    mRun=(nRn+1) % GLCONFIG.NumRun;
//memset(str90,0,90);
//sprintf((char*)str90, "1 ZapuskUZS fMB=%d mRun=%d s=%d c=%d\n\r", fMB,mRun, BINTIME.seconds,counttime);
//HAL_UART_Transmit(&huart2,str90,sizeof(str90),1000);

    //printf("ZapuskUZS nR=%d mRun=%d counttime=%d\n",nRn,mRun,counttime);
//printf("First[%d}=%d Type=%d\n",nRn,FirstC[nRn], CONFIG_IDRUN[nRn].TypeRun);
    if ( RS485HF[mRun] == 1)  // ����祭 MOBUS
    {

//       if (AnCom5 != 0x55)
//        lCom4IntHF = 0xFF;  // ࠡ�� � MODBUS ��� �� 4-���⮢�� ����
//       if (spr != (CONFIG_IDRUN[mRun].KUzs & 0x7F))
//	{ if (AnCom5 == 0x55)
//	   RS485Com5IniHF();
//	  else
//	   RS485Com4IniHF();
//printf("spr[%1d]=%1d\n",mRun,spr);
	   spr = (CONFIG_IDRUN[mRun].KUzs & 0x7F);
//	}
     if (CONFIG_IDRUN[mRun].TypeCounter == 1)  // FlowSic
     {    if (CONFIG_IDRUN[mRun].TypeRun == 10)  fMB = 4;
       else  fMB = 3;
//memset(str90,0,90);
//sprintf((char*)str90, "ZapuskUZS fMB=%d mRun=%d s=%d c=%d\n\r", fMB,mRun, BINTIME.seconds,counttime);
//HAL_UART_Transmit(&huart2,str90,sizeof(str90),1000);
         RS485IniHF(); //����� MODBUS
     }
     else if (CONFIG_IDRUN[mRun].TypeCounter == 2) // RMG  USZ09
     { if (CONFIG_IDRUN[mRun].TypeRun == 10 )
         fMB = 4; //fMB = 5;
       else
        fMB = 3;
       RS485IniHF(); //����� MODBUS
     }
     else if (CONFIG_IDRUN[mRun].TypeCounter == 3) //   ��� Ecosonic RMA
     { if (CONFIG_IDRUN[mRun].TypeRun == 10 )
       fMB = 5;
       else  fMB = 3;
       EvenFlagRMA[mRun]=!EvenFlagRMA[mRun];
       RS485IniHF(); //����� MODBUS
     }
     else if (CONFIG_IDRUN[mRun].TypeCounter == 4) //   ��� Krohne
     { if (CONFIG_IDRUN[mRun].TypeRun == 10 )  fMB = 5;
       else  fMB = 3;
       RS485IniHF(); //����� MODBUS
     }
   else if (CONFIG_IDRUN[mRun].TypeCounter == 5) //   ��� ����
     { //if (GLCONFIG.IDRUN[mRun].TypeRun == 10 )  fMB = 5;
        fMB = 2;
       RS485IniHF(); //����� MODBUS
     }
     if (CONFIG_IDRUN[mRun].TypeCounter == 6)  // FlowSic500
     {    if (CONFIG_IDRUN[mRun].TypeRun == 10)  fMB = 4;
       else  fMB = 3;
       RS485IniHF(); //����� MODBUS
     }
     if (FirstC[mRun]==0x0F)
      FirstC[mRun] = 1; //3; // ����� �� ��⪥ mRun ��ࠢ���
     else FirstC[mRun] = 0;
    }
}

//------------------------------------------------------------------
void   AnalysAvFS(int nRun)      // ��楤�� ࠧ��� ����� � �ନ஢���� ���਩
{ int i;
  unsigned  int mask,err;
  unsigned int    *pcodeFS;

  pcodeFS = &CodeFSn[nRun][0];
if (CONFIG_IDRUN[nRun].TypeCounter == 1)
 {
  mask=0x0001;
   for (i=0; i<16; i++)
  {
      err = ((StatusFS[nRun] & mask) == 0) ? codeAlarmFS[i] : codeNoAlarmFS[i];
      mask = mask<<1;
   if (err == codeAlarmFS[i]) ErStatus[nRun][i]++; else ErStatus[nRun][i]=0;
   if (err == codeNoAlarmFS[i]) NoErStatus[nRun][i]++; else NoErStatus[nRun][i]=0;

   if ((ErStatus[nRun][i] > 2) || (NoErStatus[nRun][i] > 2))       //�᫨ ����� 3 ⠪� ��ন���
   {if (err == codeAlarmFS[i])  ErStatus[nRun][i]=0;
    if (err == codeNoAlarmFS[i]) NoErStatus[nRun][i]=0;

    if (err != pcodeFS[i])
    {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[i] = err;                 // �p�᢮��� ����� ���祭��
//      if ((sms_send)&&(err==0x182))
//      { run_SMS = nRun;
//	InQueue(2,printAT);  // ���뫪� ��� �� 0-� ��⪥
//	sms_send=0;
//      }
    }
   }
  }
}
else  if (CONFIG_IDRUN[nRun].TypeCounter == 3)   // RMA
 {  mask=0x0001;
   for (i=0; i<2; i++)
  {
      err = ((StatusFS[nRun] & mask) == 0) ? codeNoAlarm_RMA[i] : codeAlarm_RMA[i];
      mask = mask<<1;
   if (err == codeAlarm_RMA[i]) ErStatus[nRun][i]++; else ErStatus[nRun][i]=0;
   if (err == codeNoAlarm_RMA[i]) NoErStatus[nRun][i]++; else NoErStatus[nRun][i]=0;
//printf("err=%X pcodeFS=%X St=%X\n",err,pcodeFS[i],StatusFS[nRun]);

   if ((ErStatus[nRun][i] > 2) || (NoErStatus[nRun][i] > 2))       //�᫨ ����� 3 ⠪� ��ন���
   {if (err == codeAlarm_RMA[i])  ErStatus[nRun][i]=0;
    if (err == codeNoAlarm_RMA[i]) NoErStatus[nRun][i]=0;

    if (err != pcodeFS[i]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[i] = err;                 // �p�᢮��� ����� ���祭��
//      if ((sms_send)&&((err==0x19B)||(err==0x182)))
//      { run_SMS = nRun;
//	InQueue(2,printAT);  // ���뫪� ��� �� 0-� ��⪥
//	sms_send=0;
//      }
    }
   }
  }
 }//if (CONFIG_IDRUN[nRun].TypeCounter == 3)
 else  if (CONFIG_IDRUN[nRun].TypeCounter == 2) //RMG
 {// printf("Status=%04X\n",StatusFS[nRun]);
  if ((StatusFS[nRun] & 0x1)  == 0)
    err =  codeNoAlarmFS[1]; else err = codeAlarmFS[1];
    if (err != pcodeFS[1])
    {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[1] = err;
      if (err == codeAlarmFS[1]) AlarmUZS[nRun] = 1;
    }       // �p�᢮��� ����� ���祭��
//    else AlarmUZS[nRun] = 0;
    if ((StatusFS[nRun] & 0x100)  == 0)
       err =  codeAlarmFS[2]; else err = codeNoAlarmFS[2];
    if (err != pcodeFS[2]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[2] = err;
      if (err == codeNoAlarmFS[2]) WarnUZS[nRun] = 1;
       }             // �p�᢮��� ����� ���祭��
//      else  WarnUZS[nRun] = 0;
//printf("FS n=%d m=%d A=%d W=%d St=%2X\n",nRun,mRun,AlarmUZS[mRun],WarnUZS[mRun],StatusFS[nRun]);
//      if ((sms_send)&&(err==0x182))
//      { run_SMS = nRun;
//	InQueue(2,printAT);  // ���뫪� ��� �� nRun-� ��⪥
//	sms_send=0;
//      }
  }   //if (CONFIG_IDRUN[nRun].TypeCounter == 2)
 else  if (CONFIG_IDRUN[nRun].TypeCounter == 4) //KROHNE
 {
  mask=0x0001;
   for (i=0; i<6; i++)
  {
      err = ((StatusFS[nRun] & mask) != 0) ? codeAlarmFSK[i] : codeNoAlarmFSK[i];
//printf("err=%X mask=%X codeAlarmFSK=%X ",err,mask,codeAlarmFSK[i]);
      mask = mask<<1;
   if (err == codeAlarmFSK[i]) ErStatus[nRun][i]++; else ErStatus[nRun][i]=0;
   if (err == codeNoAlarmFSK[i]) NoErStatus[nRun][i]++; else NoErStatus[nRun][i]=0;

   if ((ErStatus[nRun][i] > 2) || (NoErStatus[nRun][i] > 2))       //�᫨ ����� 3 ⠪� ��ন���
   { if (err == codeAlarmFSK[i])  ErStatus[nRun][i]=0;
     if (err == codeNoAlarmFSK[i]) NoErStatus[nRun][i]=0;

     if (err != pcodeFS[i]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[i] = err;                 // �p�᢮��� ����� ���祭��
//      if ((sms_send)&&(err==0x182))
//      { run_SMS = nRun;
//	InQueue(2,printAT);  // ���뫪� ��� �� 0-� ��⪥
//	sms_send=0;
//      }
     }
   }
  }  // for (i=0; i<6
//   putchar('\n');
    mask=0x1;
    for (i=6; i<8; i++)
    {
     err = ((StatusFS2[nRun] & mask) != 0) ? codeAlarmFSK[i] : codeNoAlarmFSK[i];
//printf("2-err=%X mask=%X pcodeFS=%X ",err,mask,pcodeFS[i]);
     mask = mask<<1;
     if (err == codeAlarmFSK[i]) ErStatus[nRun][i]++; else ErStatus[nRun][i]=0;
     if (err == codeNoAlarmFSK[i]) NoErStatus[nRun][i]++; else NoErStatus[nRun][i]=0;
     if ((ErStatus[nRun][i] > 2) || (NoErStatus[nRun][i] > 2))       //�᫨ ����� 3 ⠪� ��ন���
     { if (err == codeAlarmFSK[i])  ErStatus[nRun][i]=0;
       if (err == codeNoAlarmFSK[i]) NoErStatus[nRun][i]=0;
       if (err != pcodeFS[i]) {
	Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
	pcodeFS[i] = err;                 // �p�᢮��� ����� ���祭��
       }
     }
    }   // end for (i=7
//putchar('\n');
 }
 else  if (CONFIG_IDRUN[nRun].TypeCounter == 5) //����
 {
  mask=0x0003;
  if (!(StatusFS[nRun] & 0x0040))
 {
//  printf("Run=%d StatusFS2=%4X\n",nRun,StatusFS[nRun]);
   for (i=0; i<3; i++)
  {
      err = ((StatusFS[nRun] & mask) == i) ? codeAlarm_GUVR[i] : codeNoAlarm_GUVR[i];
   if (err == codeAlarm_GUVR[i]) ErStatus[nRun][i]++; else ErStatus[nRun][i]=0;
   if (err == codeNoAlarm_GUVR[i]) NoErStatus[nRun][i]++; else NoErStatus[nRun][i]=0;
//printf("err=%X ErStatus=%X\n",err,ErStatus[nRun][i]);

   if ((ErStatus[nRun][i] > 2) || (NoErStatus[nRun][i] > 2))       //�᫨ ����� 3 ⠪� ��ন���
   {if (err == codeAlarm_GUVR[i])  ErStatus[nRun][i]=0;
    if (err == codeNoAlarm_GUVR[i]) NoErStatus[nRun][i]=0;

    if (err != pcodeFS[i]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[i] = err;                 // �p�᢮��� ����� ���祭��
//      if ((sms_send)&&(err==0x19F))
//      { run_SMS = nRun;
//	InQueue(2,printAT);  // ���뫪� ��� �� 0-� ��⪥
//	sms_send=0;
//      }
     }
    }
   }
  }
  else
  {
//  printf("Run=%d StatusFS4=%4X\n",nRun,StatusFS[nRun]);
   mask = (StatusFS[nRun]&0xFF00) >> 8;
   switch(mask)
   { case 0xB3:
       i=3;
       err = 0x17A;
     break;
     case 0xB4:
       i=4;
       err = 0x17B;
     break;
     case 0xBB:
       i=5;
       err = 0x17C;
     break;
     case 0xBC:
       i=6;
       err = 0x17D;
     break;
     case 0x8B:
       i=2;
      err =   0x19F;
     break;
     case 0x9C:
       i=2;
      err =   0x11F;
     break;
     default: i=0;
   }
    if (i)
      if (err != pcodeFS[i]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[i] = err;                 // �p�᢮��� ����� ���祭��
      pcodeFS[2] = err;
      if (err==0x11F) for (i=3; i<7; i++) pcodeFS[i] = err;
//      if ((sms_send)&&(err==0x19F))
//      { run_SMS = nRun;
//	InQueue(2,printAT);  // ���뫪� ��� �� 0-� ��⪥
//	sms_send=0;
//      }
     }
//printf("err= %04X\n",err);
  }
 }
 else  if (CONFIG_IDRUN[nRun].TypeCounter == 6) //FlowSic500
 {
  if ((StatusFS500[nRun] & 0x80021020) != 0)  // 20100280 ���ਨ ���� 7, 9, 20, 29
       err = 0xCD; else err = 0x4D;
    mask = 0;
    if (err != pcodeFS[0])
    {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[0] = err;
      mask = 1;
    }       // �p�᢮��� ����� ���祭��
    if ((StatusFS500[nRun] & 0x0000E01C) != 0)  //1CE00000  �।�०����� ���� 21, 22, 23, 26, 27, 28
       err = 0xCE; else err = 0x4E;
    if (err != pcodeFS[1]) {
      Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
      pcodeFS[1] = err;
      mask=1;
      }             // �p�᢮��� ����� ���祭��
    if ((mask) || ((firstER[nRun]) && (StatusFS500[nRun] > 0L)))
     { *(long*)CodA= StatusFS500[nRun];
      Mem_AlarmWrCod(nRun,0x4F,CodA);// �뢮� ��⮢ �����
      firstER[nRun] = 0;
     }
//printf("Status=%ld mask=%d\n", StatusFS500[nRun], mask);

/*    if ((StatusFS500[nRun] & 0x00040000) != 0)
       MEMSECUR.mode  = 30;
    else
       MEMSECUR.mode  = 29;
    err = MEMSECUR.mode;
    if (err != pcodeFS[2])
    {
       strncpy(MEMSECUR.login,"Safe",4);
       MEMSECUR.TIMEBEG = BINTIME;
       memset(&MEMSECUR.TIMEEND,0,6);
       MEMSECUR.Port = nRun+1; // ����� �/�
       mask = Mem_SecurWr_2(Mem_SecurGetPage(),0,2);
       pcodeFS[2] = err;
    }
*/
  }
}
//------------------------------------------------------------------
void   VolumeWS_RMG(int nRun,double Vws2)
{ double *v;
  //char far *pENP;
  struct IdRun    *prun;       // ��p����p� ��⪨
  struct AdRun    *aprun;       // ��p����p� ��⪨
  double dQw;
//  struct bintime Tcurr[3];
  long *l;
//  long T;
  //char far *pc;

   prun = &CONFIG_IDRUN[nRun];        // 㪠��⥫� �� ��p����p� ⥪. ��⪨
   aprun   = &CONFIG_ADRUN[nRun];        // 㪠��⥫� �� ��p����p� ⥪. ���
//   Tcurr[nRun] =  BinToSec(BINTIME);
   if (firstMB[nRun]==0)  // �� ��ࢮ� �⥭��
   { dQw = Vws2 - Vwsr[nRun];  //����� - �।��饥
     if (dQw < 0.0) dQw = 0.0;
//    Tin= Tcurr[nRun]-TLastAl[nRun];
      Tin = DiffSec(&BINTIME,&TLastAl[nRun]);
       if (Tin == 0) Tin = 1;
//printf("run=%d dQw=%lf Vws2=%lf s=%d c=%d T=%ld\n",nRun, dQw,Vws2,BINTIME.seconds,counttime,Tin);
    if (dQw/Tin > 1.2*(prun->Qmax/3600.0)) dQw = Tin*prun->Qmax/3600.0;
   }
     else { dQw = 0.0; firstMB[nRun] = 0; }
//printf("Vwsr=%lf Vws2=%lf dQw=%lf\n",Vwsr[nRun],Vws2,dQw);
   Vwsr[nRun]= Vws2;      //�।��饥 = �����
     if (dQw > 0.0)
      memcpy(&TLastAl[nRun],&BINTIME,6);  // TLastAl[nRun] = BINTIME
//      outportb(0x1D0,BASE_CFG_PAGE); // � ����� ��࠭���
//      pENP = (char far *)MK_FP(SEGMENT,16356+nRun*4);
//      l=&TLastAl[nRun];
//      _fmemcpy(pENP,l,4); //������ TLastAl � ���
   aprun->Qw_s = (float)dQw;

//   Persist_AddQwCountTotal(nRun, dQw);
     prun->Qw =(float)(3600.0*dQw);
 
//printf("Run=%d Qw=%f sec=%d T=%d\n",nRun,prun->Qw,BINTIME.seconds,counttime);
//      if ((FirstC[nRun]==0x0F) && (GLCONFIG.IDRUN[nRun].TypeRun == 10))
        if (FirstC[nRun]==1)
         FirstC[nRun] = 2;   // �⢥� �� ����� �� ��⪥ nRun ����祭

}
//--------------------------------------------------------------------
// �ਥ� �� MODBUS �⢥⮢ �� ������ 1-�� ॣ 3003 ����� ���,
    // 2 - �� ॣ 5010,5011 (��ꥬ�) 3- �� ॣ 7001 (��室 � ��)
void Result_MODBUS()  //FlowSic600
{
uint8_t     *pb;
int     i,count,np;
static unsigned int L;
unsigned long int Vrl;
unsigned  int  err;
double Cmax,Vtot;
unsigned char pc;
unsigned int err2;

 struct IdRun    *prun;       // ��p����p� ��⪨
  struct AdRun    *aprun;       // ��p����p� ��⪨

  aprun  = &CONFIG_ADRUN[mRun];
  prun  = &CONFIG_IDRUN[mRun];
//   if (Com5InInd < (COM5_BUF_LEN-1))
//printf("Run=%d fMB=%d Rec=%d\n",mRun,fMB,Com5InInd);
//printf("Run=%d Rec=%d\n",mRun,Com5InInd);
//printf("Res  mRun=%d Com5InInd=%d fMB=%d c=%d\n",mRun ,Com5InInd,fMB,counttime);
  UART_EndRxTransfer(&huart3); //���������� �����
//UART3_InInd = COUNT_MODBUS - hdma_uart3_rx.Instance->CNDTR;    
  UART3_InInd = COUNT_MODBUS - huart3.RxXferCount;//hdma_uart5_rx.Instance->CNDTR
//  UART3_InInd = ResMODBUS - huart3.RxXferCount;//hdma_uart5_rx.Instance->CNDTR

//memset(str90,0,90);
//sprintf((char*)str90, "Result=%d UART3_InInd=%d fMB=%d mRun=%d s=%d c=%d\n\r",RxCpltUSR3, UART3_InInd,fMB,mRun, BINTIME.seconds,counttime);
//HAL_UART_Transmit(&huart2,str90,sizeof(str90),1000);

 pb = NULL;
 np=0; 			      // 㪠��⥫� �� ��砫� ���p�
 L=0;
 count = 128;
 
 if  (INTPAR.Keyboard != 0)     // MOSCAD ASCII
 { if (UART3_InInd  > 12)
   {
//   if ((UART3_InInd < 14)|| ((UART3_InInd < 18)&&(fMB==3))
//      || ((UART3_InInd < 26)&&(fMB==4)&&(km<1)&&(UART3_InInd > 0)))
//  { InTimeQueue(1,0,Result_MODBUS);    // ����প� 25 �� 1 ࠧ
////  InQueue(3,Result_MODBUS);  // �᫨ �� �ᯥ�� �ਭ��� �⢥� �� �������� �६�
//    km++;
//    return;
//  }
  km=0;
   for (i=0; i<UART3_InInd; i++) {    // ���� ����� � ����� ������
      if ((MODBUS_RecvBuf[i]==0x3A) && (MODBUS_RecvBuf[i+3]==0x30) && (MODBUS_RecvBuf[i+4]==0x33))
      {
	 pb = &MODBUS_RecvBuf[i];          // ��砫�� ���� ����
         np = 1;
	if   ( T2bToInt(pb+1) == CONFIG_IDRUN[mRun].AddrMODBUS)
	 {	 count =T2bToInt(pb+5)*2+7;    // ����� ���p� ��� ��
	 }
	else {pb=NULL;np=0;}
	 break;                      // ���p �뤥���
      }
   }

  if ((np)&&(pb))
   L =  ASCIIToHex(pb+1);// - �஢�ઠ ����஫쭮� �㬬� LCR
//printf("fMB=%d L=%d pb=%d Com5InInd=%d count=%d\n",fMB,L,(pb!=0),Com5InInd,counttime);
  }
 }   
 else  // MOSCAD RTU
 {
//   if ((UART3_InInd < 7) || (((UART3_InInd < 30)&&(fMB==4))&&(km<1)))
//  {InTimeQueue(1,0,Result_MODBUS);  // ����প� 25 ��
// // �᫨ �� �ᯥ�� �ਭ��� �⢥� �� �������� �६�
////    printf("1-c=%d\n",counttime);
//    km++;
//    return;
//  }

 if (UART3_InInd > 5)
  {
   for (i=0; i<UART3_InInd; i++)     // ���� ����� � ����� ������
      if ((MODBUS_RecvBuf[i]==prun->AddrMODBUS) && (MODBUS_RecvBuf[i+1]==0x03)
       && (((fMB==2)&&(UART3_InInd >= 6)&&(UART3_InInd < 9)) ||
       ((fMB==3)&&(UART3_InInd >= 8)&&(UART3_InInd < 12)) || ((fMB==4)&&(UART3_InInd >= 32)&&(UART3_InInd < 36))))
      {
	 pb = &MODBUS_RecvBuf[i];          // ��砫�� ���� ����
         np = 1;
	 count =*(pb+2)+3;    // ����� ���p� ��� ��
	 break;                      // ���p �뤥���
      }
   if ((np)&&(pb))
   L =  (Mem_Crc16(pb,count)==*(int*)(pb+count));// - �஢�ઠ ����஫쭮� �㬬� LCR
  } 
  //printf("fMB=%d L=%d pb=%d Com5InInd=%d counttime=%d\n",fMB,L,(pb!=0),Com5InInd,counttime);
 }  // ����� MOSCAD RTU

   if ((np) && (UART3_InInd>=count) && (L))
   {
    if (fMB==2)  // �����
     { if  (INTPAR.Keyboard != 0)
       StatusFS[mRun] = T4bToInt(pb+7);
       else
       { pc=*(pb+3); *(pb+3) = *(pb+4); *(pb+4) = pc;
        StatusFS[mRun] =  *(unsigned int*)(pb+3);
       }
       AnalysAvFS(mRun);      // ��������� ������� ������� � ������������ ������
//    printf("StatusFS=%4X s=%d time3=%d\n",StatusFS[mRun],BINTIME.seconds,counttime);
     }
    else
    if (fMB==3)     //��室
    { if  (INTPAR.Keyboard != 0)
        Qr = H8bToFloat(pb+7);
      else
      { Swap4(pb+3);
float Q;          
//        Q = *(float*)(pb+3);
       memcpy(&Q,(pb+3),4);
        Qr = Q;          
      }
//     printf("3-Count=%d mRun=%d Qr=%f\n",counttime,mRun,Qr);
//     Qsn[mRun] = Qr;
    if ((prun->KUzs & 0x80)==0) //�������� �����
    {
     IndQs[mRun] = -Qr;
     aprun->dPh_s = -Qr;
    }
    else    // ��אַ� ��⮪
    {
     IndQs[mRun] = Qr;
     aprun->dPh_s = Qr;
    }
//     printf("Qr=%f time2=%d\n",Qr,counttime);
    if (Qr < -prun->dPots) //Qst
// ������ "������ ��������� ������) / "����� ������� ������"
    {
      if ((prun->KUzs & 0x80) ==0)   //�������� �� ������ � �������� �������
      {
       err2 = 0x4C;
       if (err2 != buf_err[1][mRun])
       {
        Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
        buf_err[1][mRun] = err2;                    // �p������� ����� ��������
//printf("End forward");
       }
      }
      else   // �� ������ �����
      {  err2 = 0xC0;
       if (err2 != buf_err[0][mRun])
       {
        Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
        buf_err[0][mRun] = err2;               
//printf("Begin revers");
       }
      }
    }
    else  //if (Qr < -prun->dPots)
     if (Qr > prun->dPots) //Qst
//������ "����� ��������� ������" / "������ ������� ������"
     {
      if ((prun->KUzs & 0x80) == 0)  //�������� �� ������ � �������� �������
      { err2 = 0xCC;
        if (err2 != buf_err[1][mRun])
        {
         Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
         buf_err[1][mRun] = err2;               
//printf("Begin forward");
        }
      }
      else                // � ������ �������
      { err2 = 0x40;
        if (err2 != buf_err[0][mRun])
        {
         Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
         buf_err[0][mRun] = err2;               
//printf("End revers");
        }
       }
     }
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] >2)    // ������������ ��������� ����� 2 ����� �������
     { err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 
      }
     }
     }
    else
    if (fMB==4)    // ����� ����� �� � ��
    {
    if  (INTPAR.Keyboard != 0)     //ASCII
    {
     if ((prun->KUzs & 0x80)==0)  //�������� �����
      Vhi  = T8bToInt(pb+15);  // 5014 -  ����. ���������
     else   //��אַ� ��⮪
      Vhi  = T8bToInt(pb+31);  // 5011 -   -  ����. ���������
     Vrl = T8bToInt(pb+7);      // 5011 ��� 5013 ��������� �����
    }
    else      // RTU
    {
     if ((prun->KUzs & 0x80)==0)  //�������� �����
     { Swap4(pb+7);
       Vhi = *(unsigned long*)(pb+7); // 5014 -  ����. ���������
     }
     else   //��אַ� ��⮪
     { Swap4(pb+15);
       Vhi = *(unsigned long*)(pb+15);  // 5011 -  ����. ���������
     }
      Swap4(pb+3);
      Vrl = *(unsigned long*)(pb+3);     // 5011 ��� 5013 ��������� �����
    }
  //    if (firstMB[mRun])
       if  (Vhi > 0)
        KVolumeUZS[mRun] = 1000.0/(float)Vhi;
//printf("KVolume=%lf \n",KVolumeUZS[mRun]);
       if (KVolumeUZS[mRun] > 0)
      { Cmax = (double)Vpred[CONFIG_IDRUN[mRun].TypeCounter]/KVolumeUZS[mRun];
        aprun->Vtrual=(double)Vrl / KVolumeUZS[mRun]; //100.0;
      }

    if  (INTPAR.Keyboard != 0)  // ASCII
    {
      Vrl  = T8bToInt(pb+47);  // // 5016 ��� 5018 - ������ ����� ��. �����
      Vhi  = T8bToInt(pb+55);    //5017 ��� 5019  ������� �����
    }
    else      // RTU
    {Swap4(pb+23);
     Vrl = *(unsigned long*)(pb+23); ////5016 ��� 5018  ������� �����
     Swap4(pb+27);
     Vhi = *(unsigned long*)(pb+27); // 5017 ��� 5019 - ������ ����� ��. �����
    }
     if (KVolumeUZS[mRun] > 0)
     aprun->Vtot_l=(double)Vrl / KVolumeUZS[mRun]; //100.0;
//printf(" FS Vtot_l=%12.2lf Vrl=%lu Vhi=%lu  ",aprun->Vtot_l,Vrl,Vhi);
//     printf("4-Count=%d mRun=%d\n",counttime,mRun);
     aprun->Vtot_h = Vhi;
//  printf("Vhi=%lu \n",Vhi);
     Vtot=(double)Vhi*Cmax+aprun->Vtot_l;
     aprun->Vtot_l = Vtot;
     aprun->Vtrush = Vtot - aprun->Vtrual;
     Persist_SetQwCountTotal(mRun,aprun->Vtot_l);
//printf("starttime=%d Vtot=%lf\n",starttime,Vtot);
//     if (starttime<2)
      VolumeWS_RMG(mRun,Vtot);//H485_VolumeWS2(mRun,Vr1[mRun]);
      errMODBUS[mRun] = 0;
      err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
    }

   }  //if ((pb != 0) && (Com5InInd>count))
   else // �訡�� �裡
   {
//   printf("mRun=%d fMB=%d L=%d pb=%d Buf: %s\n",mRun,fMB,L,(pb!=0),Com5RecvBuf);
   if ((fMB==4)  || (fMB==3)) // &&(CONFIG_IDRUN[mRun].TypeRun != 10))
   {
     if (fMB==4)
     {// prun->Qw = 0.0;
       aprun->Qw_s = 0.0;
     }
     Al112[mRun]=0;
      if (errMODBUS[mRun] < CErrMB+1)
     errMODBUS[mRun]++;
//      printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
     if (errMODBUS[mRun] > (CErrMB/SysTakt)) // 3-6 ����⮪(3 ᥪ)
     {
//      printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
       prun->Qw = 0.0;
       err = 0x192;
       if (err != errMB[mRun])
       {
        Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
        errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
       }
     }
    } //   if ((fMB==4)  ||
   }  //else �訡�� �裡
   MODBUS_RecvBuf[UART3_InInd]=0;
//printf("Reciev: %d Buf=%s\n",Com5InInd,Com5RecvBuf);
//printf("mRun=%d fMB=%d c=%d\n",mRun,fMB,counttime);
    if (fMB > 2)
    {  fMB--;
      InQueue(1,RS485IniHF);
    }
}
//-----------------------------------------------------------
/*
void Result_MODBUS_RMA()
{
unsigned char     *pb;
int     i,count,np;
static unsigned int L;
unsigned long int Vrl;
unsigned  int  err;
struct IdRun    *prun;       // ��p����p� ��⪨
struct AdRun    *aprun;       // ��p����p� ��⪨
double VrS, Vth,Cmax; // = 429496.7296;
double  VrtSl;
unsigned int    *pcodeFS;
  pcodeFS = &CodeFSn[mRun][0];
  KVolumeUZS[mRun] = 10000.0;
  aprun  = &CONFIG_ADRUN[mRun];
  prun  = &CONFIG_IDRUN[mRun];
  Cmax = (double)Vpred[CONFIG_IDRUN[mRun].TypeCounter]/KVolumeUZS[mRun];
// printf("mRun=%d InInd=%d fMB=%d \n",mRun,Com5InInd,fMB);

   if ((UART3_InInd < 7)||((UART3_InInd < 13)&&(fMB==4))
      || (((UART3_InInd < 13)&&(fMB==5))&&(km<31)&&(UART3_InInd > 2)))
  { InQueue(3,Result_MODBUS_RMA);  // �᫨ �� �ᯥ�� �ਭ��� �⢥� �� �������� �६�
    km++; for (i=0; i<300; i++);  // �������� 0.9 �� �� 31 ࠧ (31*0.9=28 ��)
    return;
  }
  km=0;
 pb = NULL;
 np=0; 			      // 㪠��⥫� �� ��砫� ���p�
 L=0;
 count = 128;
// printf("fMB=%d Com5InInd=%d\n",fMB,Com5InInd);
 if (UART3_InInd > 5)
 {
   for (i=0; i<UART3_InInd; i++)     // ���� ����� � ����� ������
      if ((MODBUS_RecvBuf[i]==prun->AddrMODBUS) && (MODBUS_RecvBuf[i+1]==0x03))
      {
	 pb = &MODBUS_RecvBuf[i];          // ��砫�� ���� ����
         np = 1;
	 count =*(pb+2)+3;    // ����� ���p� ��� ��
	 break;                      // ���p �뤥���
      }
 }
  if ((np)&&(pb))
  L =  (Mem_Crc16(pb,count)==*(int*)(pb+count));// - �஢�ઠ ����஫쭮� �㬬� LCR
//printf("fMB=%d L=%d pb=%d Com5InInd=%d counttime=%d\n",fMB,L,(pb!=0),Com5InInd,counttime);
   if ((np) && (UART3_InInd>=count) && (L))
   {
    if (fMB==2)  // �����
     { StatusFS[mRun] = *(pb+4);
       AnalysAvFS(mRun);      // ��楤�� ࠧ��� ����� � �ନ஢���� ���਩
//     printf("2-Count=%d mRun=%d\n",counttime,mRun);
//    printf("StatusFS=%2X time3=%d\n",StatusFS[mRun],counttime);
     }
    else
    if (fMB==3)     //��室
    {Swap4(pb+3);
     Qr = (*(long*)(pb+3))/100.0;
//     printf("3-Count=%d mRun=%d\n",counttime,mRun);
     IndQs[mRun] = Qr;
     aprun->dPh_s = Qr;
//printf("mRun=%d Qr=%f time2=%d\n",mRun,Qr,counttime);
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] > 2)
     {
      err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
    }
    else
    if (fMB==4)    // ��騩 ��ꥬ �� � ��
    {Swap4(pb+7);
     Vrl = *(unsigned long*)(pb+7);
     aprun->Vtot_l=(double)Vrl / KVolumeUZS[mRun];
//     printf("4-Count=%d mRun=%d\n",counttime,mRun);
     Swap4(pb+3);
     Vhi  = *(unsigned long*)(pb+3);
     aprun->Vtot_h = Vhi;
      Vth = (double)Vhi;
     VrS = Cmax*Vth +  aprun->Vtot_l;
     aprun->Vtrush = VrS;   // ����
//printf("Vrs=%lf Vth=%lf Vt_l=%lf\n",VrS, Cmax*Vth,aprun->Vtot_l);
     aprun->Vtot_l = VrS+VrtS[mRun];
     aprun->Vtrush = aprun->Vtot_l - aprun->Vtrual;
     Persist_SetQwCountTotal(mRun,aprun->Vtot_l);
//printf("Vtot=%lf\n",aprun->Vtot_l);
//     Vr1[mRun]=Vrl+VrlS[mRun];
//     if ((starttime<2)&&((CONFIG_IDRUN[mRun].TypeRun) == 10))
       VolumeWS_RMG(mRun,aprun->Vtot_l);
     dVFul[mRun] = (float)(aprun->Vtot_l - Vful1[mRun]);
//printf("Vtot_l=%lf Vful1=%lf\n",aprun->Vtot_l,Vful1[mRun]);
     Vful1[mRun] = aprun->Vtot_l;
//     dVFullSum[mRun] = dVFul1[mRun] + dVFul2[mRun] + dVFul[mRun];

     char ErV=(dVFul[mRun]<=1.0e-4) && (dVFul1[mRun]<=1.0e-4) && (dVFul2[mRun]<=1.0e-4);
//printf("dVFul=%f dVFul1=%f dVFul2=%f ErV=%d\n",
//        dVFul[mRun], dVFul1[mRun], dVFul2[mRun], ErV);
     dVFul2[mRun] = dVFul1[mRun];
     dVFul1[mRun] = dVFul[mRun];
//printf("dVFullSum[%d]=%f dVFul=%f Qr=%f\n",mRun, dVFullSum[mRun],dVFul[mRun],aprun->dPh_s);
//     if ((fabs(aprun->dPh_s) > 1.0e-4) && (dVFullSum[mRun] <= 1.0e-4))
     if ((fabs(aprun->dPh_s) > 1.0e-4) && (ErV))
      {
       err=0x19D;
       if (err != pcodeFS[2]) {
//        printf("Alarm! No volume\n");
        Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
        pcodeFS[2] = err;                 // �p�᢮��� ����� ���祭��
       }
      }
     else//   if ((fabs(aprun->dPh_s) <= 1.0e-4) || (dVFullSum[mRun] > 1.0e-3))
      {
       err=0x11D;
       if (err != pcodeFS[2]) {
//printf("No Alarm.  Volume OK!\n");
        Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
        pcodeFS[2] = err;                 // �p�᢮��� ����� ���祭��
       }
      }
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] > 1)
     {
      err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
    }   //if (fMB==4)
    else
    if (fMB==5)    // ���਩�� ��騩 ��ꥬ �� � ��
    {Swap4(pb+7);
     VrlS[mRun] = *(unsigned long*)(pb+7);
     VrtSl = (double)VrlS[mRun] / KVolumeUZS[mRun];  //10000.0;
//     printf("4-Count=%d mRun=%d\n",counttime,mRun);
     Swap4(pb+3);
     Vhi  = *(unsigned long*)(pb+3);
     aprun->Vtot_h = Vhi;
     Vth = (double)Vhi;
     VrtS[mRun] = Cmax*Vth +  VrtSl;
     aprun->Vtrual = VrtS[mRun];      // ���਩��
//printf("Vrts=%lf Vth=%lf Vtl=%lf\n",VrtS[mRun], Cmax*Vth,VrtSl);
//printf("Vrual1[%d]=%f VrtS=%f\n", mRun, Vrual1[mRun],Vr);
     dVrual[mRun] = (float)(VrtS[mRun] - Vrual1[mRun]);
     Vrual1[mRun] = VrtS[mRun];
//printf("dVrual[%d]=%f dVal1=%f\n",mRun,dVrual[mRun],dVal1[mRun]);
     if ((dVrual[mRun] > 1e-4) && (dVal1[mRun] > 1e-4) && (dVal2[mRun] > 1e-4))
     {   //����� "����७�� � ����襭��� ����譮����"
      err=0x19E;
//printf("err=%X pcodeFS[3]=%X\n",err, pcodeFS[3]);
      if (err != pcodeFS[3]) {
       Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
       pcodeFS[3] = err;                 // �p�᢮��� ����� ���祭��
      }
     }  //if ((dVrual[mRun] > 0.0) && (dVal1[mRun] > 0.0))
     else
     {    //����� ���ਨ. "����७�� � ��ଥ"
      err=0x11E;
//printf("err=%X pcodeFS[3]=%X\n",err, pcodeFS[3]);
      if (err != pcodeFS[3]) {
//printf("End Alarm Volume OK\n");
       Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
       pcodeFS[3] = err;                 // �p�᢮��� ����� ���祭��
      }
     }    // else
     dVal2[mRun] = dVal1[mRun];
     dVal1[mRun] = dVrual[mRun];
   } //if (fMB==5)

   }  //if ((pb != 0) && (Com5InInd>count))
   else // �訡�� �裡
   {//  printf("L=%d pb=%d Com5InInd=%d\n",L,(pb!=0),Com5InInd);
 //   printf("L=%d pb=%d Buf: %s\n",L,(pb!=0),Com5RecvBuf);
   if ((fMB==4)  || (fMB==3))
   {  if (fMB==4)
      {//prun->Qw = 0;
       aprun->Qw_s = 0;
//       Qr = 0.0;
       IndQs[mRun] = Qr;
       aprun->dPh_s = Qr;
       }
       Al112[mRun]=0;
     if (errMODBUS[mRun] < CErrMB+1)
     errMODBUS[mRun]++;
     if (errMODBUS[mRun] > CErrMB/SysTakt) // 10-5 ����⮪ (10 c)
     {
//      printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
      prun->Qw = 0;
      err = 0x192;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
    }
   }
   if (fMB > 2)
    { fMB--;
      { if ((EvenFlagRMA[mRun]!=0) && (fMB==3)) fMB--;
        else {if ((EvenFlagRMA[mRun]==0) && (fMB==2)) return;}
      }
//printf("fMB=%d counttime=%d\n",fMB, counttime);
      InQueue(1,RS485IniHF);
    }
 }
//-----------------------------------------------------------
void Swap2(unsigned char* p, int k)
{int i;
 char m;
 for (i=0; i<k; i=i+2)
 {m=p[i]; p[i]=p[i+1]; p[i+1]=m;}
}*/
//------------------------------------------------------------
/*
void Result_MODBUS_RMG()
{
unsigned char     *pb;
int     i,count,np;
static unsigned int L;
unsigned long int Vrl;
unsigned  int  err;
struct IdRun    *prun;       // ��p����p� ��⪨
struct AdRun    *aprun;       // ��p����p� ��⪨
double  VrtSl;
  aprun  = &CONFIG_ADRUN[mRun];
  prun  = &CONFIG_IDRUN[mRun];

//   if (((Com5InInd < 9) //||(Com5InInd < 9)&&(fMB==3)
//      ||(Com5InInd < 44)&&(fMB==2))&&(km<31)&&(Com5InInd > 2))
//  { InQueue(3,Result_MODBUS_RMG);  // �᫨ �� �ᯥ�� �ਭ��� �⢥� �� �������� �६�
//    km++; for (i=0; i<300; i++);  // �������� 0.9 �� �� 31 ࠧ (31*0.9=28 ��)
//    return;
//  }
  km=0;
//printf("km=%d\n",km);
 pb = NULL;
 np=0; 			      // 㪠��⥫� �� ��砫� ���p�
 L=0;
 count = 128;
// if (Com5InInd > 5)
 {
   for (i=0; i<Com5InInd; i++)     // ���� ����� � ����� ������
      if ((Com5RecvBuf[i]==prun->AddrMODBUS) && (Com5RecvBuf[i+1]==0x03))
      {
	 pb = &Com5RecvBuf[i];          // ��砫�� ���� ����
         np = 1;
	 count =*(pb+2)+3;    // ����� ���p� ��� ��
	 break;                      // ���p �뤥���
      }
 }
  if ((np)&&(pb))
  L =  (Mem_Crc16(pb,count)==*(int*)(pb+count));// - �஢�ઠ ����஫쭮� �㬬� LCR
//printf("L=%d pb=%d Com5InInd=%d count=%d\n",L,(pb!=0),Com5InInd,count);
   if ((np) && (Com5InInd>=count) && (L))
   {
    if (fMB==2)  // �����
//     { StatusFS[mRun] = 256 * (*(pb+4)) + *(pb+6);
     { StatusFS[mRun] = 256 * (*(pb+6)) + *(pb+4);
       AnalysAvFS(mRun);      // ��楤�� ࠧ��� ����� � �ନ஢���� ���਩
//    printf("2- Count=%d mRun=%d\n",counttime,mRun);

    if (AlarmUZS[mRun])
     Tal2[mRun] = QcntT;
    else
     Tal1[mRun] = QcntT;
//printf("Al=%d Tal1 = %lu Tal2=%lu\n",AlarmUZS[mRun],Tal1[mRun],Tal2[mRun]);
    if ((AlarmUZS[mRun])&&((Tal2[mRun]-Tal1[mRun]) > 2))
       {
//printf("Alarm: ");
         CodA[0]=0; CodA[1]=*(pb+36) & 0x0F;     
         CodA[2]=*(pb+31); CodA[3]=*(pb+32);     //99-96,91-64
         if (*(long*)(pb+31) != 0L)
          Mem_AlarmWrCod(mRun,0x46,CodA);
         CodA[0]=*(pb+29); CodA[1]=*(pb+30);   //63-32
         CodA[2]=*(pb+27); CodA[3]=*(pb+28);
         if (*(long*)(pb+27) != 0L)
          Mem_AlarmWrCod(mRun,0x45,CodA);
         CodA[0]=*(pb+25); CodA[1]=*(pb+26);   //32-0
         CodA[2]=*(pb+23); CodA[3]=*(pb+24);
         if (*(long*)(pb+23) != 0L)
          Mem_AlarmWrCod(mRun,0x44,CodA);
//putchar('\n');
        AlarmUZS[mRun]=0;
        }
    if (WarnUZS[mRun])
     Twr2[mRun] = QcntT;
    else
     Twr1[mRun] = QcntT;
//printf("Wr=%d Twr1 = %lu Twr2=%lu\n",WarnUZS[mRun],Twr1[mRun],Twr2[mRun]);
       if ((WarnUZS[mRun])&&((Twr2[mRun]-Twr1[mRun]) > 2))  // ���� ���� �।�०�����
       {
//printf("Warn: ");
         *(pb+36)=*(pb+36) & 0xF0;
//         *(pb+45)=0;
//       *(pb+46)=0;
         CodA[0]=*(pb+45); CodA[1]=*(pb+46);
         CodA[2]=*(pb+43); CodA[3]=*(pb+44);
         if (*(long*)(pb+43)!= 0L)
          Mem_AlarmWrCod(mRun,0x49,CodA);
         CodA[0]=*(pb+41); CodA[1]=*(pb+42);
         CodA[2]=*(pb+39); CodA[3]=*(pb+40);
         if (*(long*)(pb+39)!= 0L)
          Mem_AlarmWrCod(mRun,0x48,CodA);
         CodA[0]=*(pb+37); CodA[1]=*(pb+38);
         CodA[2]=*(pb+35); CodA[3]=*(pb+36); // ������ (96-99)-� ����
         if (*(long*)(pb+35)!= 0L)
          Mem_AlarmWrCod(mRun,0x47,CodA);
//putchar('\n');
        WarnUZS[mRun]=0;
       }
//   printf("2-Count=%d mRun=%d\n",counttime,mRun);
     }
    else
    if (fMB==3)     //��室
    {
     Swap2(pb+3,4);
     Qr = *(float*)(pb+3);
//  printf("3-Count=%d mRun=%d\n",counttime,mRun);
     IndQs[mRun] = Qr;
     aprun->dPh_s = Qr;
// printf("Qr=%f time2=%d\n",Qr,counttime);
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] > 2)
     {
      err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
      }
    }
    else
    if (fMB==4)    // ��騩 ��ꥬ �� � ��
    {
       Swap2(pb+19,8);  //Swap2(pb+3,8);
     Vthr  = *(double*)(pb+19);
//printf("Vthr=%lf s=%d c=%d\n",Vthr, BINTIME.seconds,counttime);
     aprun->Vtot_l = Vthr;  //�����
     aprun->Vtrush = Vthr - aprun->Vtrual;  //����
     Persist_SetQwCountTotal(mRun,aprun->Vtot_l);
//     if (starttime<2)
      VolumeWS_RMG(mRun,Vthr);
     Swap2(pb+3,8);
     Vthr  = *(double*)(pb+3);
//printf("Avar Vthr=%lf\n",Vthr);
     aprun->Vtrual = Vthr;  //���਩��

      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] > 2)
     {
      err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
      }
//    printf("4-Count=%d mRun=%d\n",counttime,mRun);
    }

   }  //if ((pb != 0) && (Com5InInd>count))
   else // �訡�� �裡
   {//  printf("L=%d pb=%d Com5InInd=%d fMB=%d\n",L,(pb!=0),Com5InInd,fMB);
 //   printf("L=%d pb=%d Buf: %s\n",L,(pb!=0),Com5RecvBuf);
   if ((fMB==4)  || (fMB==3))
   {  if (fMB==4)
      {
//printf("fMB=4  Qw=%f\n",prun->Qw);
//      prun->Qw = 0;
       aprun->Qw_s = 0;
      }
      Al112[mRun]=0;
     if (errMODBUS[mRun] < CErrMB+1)
     errMODBUS[mRun]++;
     if (errMODBUS[mRun] > CErrMB/SysTakt) // 10-5 ����⮪ (10 c)
     {
//      printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
      prun->Qw = 0;
      err = 0x192;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
    }
   }
   if (fMB > 2)
    {  fMB--;
//      InQueue(1,RS485IniHF);
    RS485IniHF();
    }
}
*/
//------------------------------------------------------------
/*
void Result_MODBUS_Kroh()
{
unsigned char     *pb;
int     i,count,np;
static unsigned int L;
unsigned long int Vrl;
unsigned  int  err;
struct IdRun    *prun;       // ��p����p� ��⪨
struct AdRun    *aprun;       // ��p����p� ��⪨
double VrS, Vth;
double Cmax; // = 4294.967296;
double  VrtSl;
unsigned char cm;

  aprun  = &CONFIG_ADRUN[mRun];
  prun  = &CONFIG_IDRUN[mRun];
  KVolumeUZS[mRun] = 1000000.0;
  Cmax = (double)Vpred[CONFIG_IDRUN[mRun].TypeCounter]/KVolumeUZS[mRun];
 pb = NULL;
 np=0; 			      // 㪠��⥫� �� ��砫� ���p�
 L=0;
 count = 128;
 if (Com5InInd > 5)
 {
 if (CONFIG_IDRUN[mRun].TypeCounter == 4) cm=4;
 else cm=3;
    for (i=0; i<Com5InInd; i++)     // ���� ����� � ����� ������
      if ((Com5RecvBuf[i]==prun->AddrMODBUS) && (Com5RecvBuf[i+1]==cm))
      {
	 pb = &Com5RecvBuf[i];          // ��砫�� ���� ����
	 np = 1;
	 count =*(pb+2)+3;    // ����� ���p� ��� ��
	 break;                      // ���p �뤥���
      }
 }
  if ((np)&&(pb))
  L =  (Mem_Crc16(pb,count)==*(int*)(pb+count));// - �஢�ઠ ����஫쭮� �㬬� LCR
//printf("L=%d pb=%d Com5InInd=%d count=%d\n",L,(pb!=0),Com5InInd,count);
   if ((np) && (Com5InInd>=count) && (L))
   {
    if (fMB==2)  // �����
     { cm = *(pb+3);
       *(pb+3) = *(pb+4); *(pb+4) = cm;
       StatusFS2[mRun] = *(int*)(pb+3);
       cm = *(pb+5);
       *(pb+5) = *(pb+6); *(pb+6) = cm;
       StatusFS[mRun] = *(int*)(pb+5);
       AnalysAvFS(mRun);      // ��楤�� ࠧ��� ����� � �ନ஢���� ���਩
//     printf("2 Count=%d mRun=%d\n",counttime,mRun);
//    printf("StatusFS=%4X StatusFS2=%4X time3=%d\n",StatusFS[mRun],StatusFS2[mRun],counttime);
     }
    else
    if (fMB==3)     //��室
    {Swap4(pb+3);
     Qr = *(float*)(pb+3)*3600.0;
//     printf("3-Count=%d mRun=%d\n",counttime,mRun);
     IndQs[mRun] = Qr;
     aprun->dPh_s = Qr;
//printf("Qr=%f time2=%d\n",Qr,counttime);
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] > 2)
     {
      err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
    }
    else
    if (fMB==4)    // ��騩 ��ꥬ �� � ��
    { Swap4(pb+7);
     Vrl = *(unsigned long*)(pb+7);
     aprun->Vtot_l=(double)Vrl /  KVolumeUZS[mRun]; //1000000.0;
//printf("Vrl=%ld KVol=%lf\n",Vrl,KVolumeUZS[mRun]);
//     printf("4-Count=%d mRun=%d\n",counttime,mRun);
     Swap4(pb+3);
     Vhi  = *(unsigned long*)(pb+3);
     aprun->Vtot_h = Vhi;
//printf("Vhi=%ld Cmax=%lf\n",Vhi,Cmax);
      Vth = (double)Vhi;
     VrS = Cmax*Vth +  aprun->Vtot_l;
     aprun->Vtrush = VrS;  // ����
//printf("Vrs=%lf Vth=%lf Vt_l=%lf\n",VrS, Cmax*Vth,aprun->Vtot_l);
     aprun->Vtot_l = VrS+VrtS[mRun];  // �����
    Persist_SetQwCountTotal(mRun,aprun->Vtot_l);
//printf("Vtot=%lf\n",aprun->Vtot_l);
//     if (starttime<2)
     VolumeWS_RMG(mRun, aprun->Vtot_l);  //H485_VolumeWS2(mRun,Vr1[mRun]);
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] > 1)
     {
      err=0x112;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
      }
    }
    else
    if (fMB==5)    // ���਩�� ��騩 ��ꥬ �� � ��
    {Swap4(pb+7);
     VrlS[mRun] = *(unsigned long*)(pb+7);
//printf("Vr1S=%lu %ld\n",Vr1,VrlS[mRun]);
     VrtSl = (double)VrlS[mRun] / KVolumeUZS[mRun]; //1000000.0;
//printf("4-Count=%d mRun=%d\n",counttime,mRun);
     Swap4(pb+3);
     Vhi  = *(unsigned long*)(pb+3);
     aprun->Vtot_h = Vhi;
     Vth = (double)Vhi;
     VrtS[mRun] = Cmax*Vth +  VrtSl;
     aprun->Vtrual = VrtS[mRun]; // ���਩��
//printf("Vrts=%lf Vth=%lf Vtl=%lf\n",VrtS[mRun], Cmax*Vth,VrtSl);
    }

   }  //if ((pb != 0) && (Com5InInd>count))
   else // �訡�� �裡
   {//  printf("L=%d pb=%d Com5InInd=%d\n",L,(pb!=0),Com5InInd);
 //   printf("L=%d pb=%d Buf: %s\n",L,(pb!=0),Com5RecvBuf);
   if ((fMB==4)  || (fMB==3))
   {  if (fMB==4)
      {//prun->Qw = 0;
       aprun->Qw_s = 0;
       }
       Al112[mRun]=0;
      if (errMODBUS[mRun] < CErrMB+1)
     errMODBUS[mRun]++;
     if (errMODBUS[mRun] > CErrMB/SysTakt) // 10-5 ����⮪ (10 c)
     {
//      printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
      prun->Qw = 0;
      err = 0x192;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
    }
   }
   if (fMB > 2)
    {  fMB--;
      InQueue(1,RS485IniHF);
    }
    else
    {
     if (AnCom5 == 0x55)
     {
      outportb(CT_C5LCR,0x03);  // �p��� ��p����   n,8,1, �����
//printf("c3=%d\n",counttime);
     }
     else
     {
      outport(CT_SP1CT, 0x00E1);// ��⠭���� ��⮢ enable TX � RX, MODE1 (E1)
     }
   }
}
*/
//--------------------------------------------------------------------
// �ਥ� �� MODBUS �⢥⮢ �� ������ 1-�� ॣ 3200 ����� ���,
    // 2 - �� ॣ 4000 (��ꥬ�) 3- �� ॣ 7002 (��室 � ��)
void Result_MODBUS_500()  //FlowSic500
{
uint8_t     *pb;
int     i,count,np,kc;
static unsigned int L;
unsigned long int Vrl;
unsigned  int  err;
double Cmax,Vtot;
unsigned char pc;

  struct IdRun    *prun;       // ��p����p� ��⪨
  struct AdRun    *aprun;       // ��p����p� ��⪨

  aprun  = &CONFIG_ADRUN[mRun];
  prun  = &CONFIG_IDRUN[mRun];
 pb = NULL;
 np=0; 			      // 㪠��⥫� �� ��砫� ���p�
 L=0;
 count = 128;
 // MOSCAD RTU
// {
//   if ((UART3_InInd < 7)||(UART3_InInd < 14) && ((fMB==4)&& (km<31)&&(UART3_InInd > 2)))
//  { InQueue(3,Result_MODBUS_500);  // �᫨ �� �ᯥ�� �ਭ��� �⢥� �� �������� �६�
//    km++; for (i=0; i<300; i++);  // �������� 0.9 �� �� 31 ࠧ (31*0.9=28 ��)
//    return;
//  }
km=0;
//printf("FS500=%d In=%d IndQs0=%8.4f IndQs1=%8.4f Qsn0=%8.4f Qsn1=%8.4f\n",counttime,Com5InInd,IndQs[0],IndQs[1],Qsn[0],Qsn[1]);
 if (UART3_InInd > 5)
  {
   for (i=0; i<UART3_InInd; i++)     // ���� ����� � ����� ������
      if (((MODBUS_RecvBuf[i]==prun->AddrMODBUS) && (MODBUS_RecvBuf[i+1]==0x03) &&
      (fMB==2)&&(UART3_InInd >= 13)&&(UART3_InInd < 15)) ||
      ((fMB==3)&&(UART3_InInd >= 9)&&(UART3_InInd < 13)) || ((fMB==4)&&(UART3_InInd >= 15)&&(UART3_InInd < 18)))
      {
	 pb = &MODBUS_RecvBuf[i];          // ��砫�� ���� ����
         np = 1;
	 count =*(pb+2)+3;    // ����� ���p� ��� ��
	 break;                      // ���p �뤥���
      }
  }
  if ((np)&&(pb))
  L =  (Mem_Crc16(pb,count)==*(int*)(pb+count));// - �஢�ઠ ����஫쭮� �㬬� LCR
//printf("fMB=%d L=%d pb=%d Com5InInd=%d counttime=%d\n",fMB,L,(pb!=0),Com5InInd,counttime);
  if ((np) && (UART3_InInd>=count) && (L))
  {
    if (fMB==2)  // �����   FlowSic500
     {// pc=*(pb+3); *(pb+3) = *(pb+4); *(pb+4) = pc;
//printf("Ind=%d count=%d c=%d s=%d\n",Com5InInd,count,counttime,BINTIME.seconds);

       StatusFS500[mRun] =  *(unsigned long*)(pb+3);
       AnalysAvFS(mRun);      // ��楤�� ࠧ��� ����� � �ନ஢���� ���਩
//printf("StatusFS500=%lu %02X %02X %02X %02X\n",StatusFS500[mRun],pb[3],pb[4],pb[5],pb[6]);
     }
    else
    if (fMB==3)     //��室 FlowSic500
    {// Swap4(pb+3);
    Qr = Char485ToFloat(pb+3);
//        Qr = *(float*)(pb+3);
// printf("3-Count=%d mRun=%d Qr=%f Ind=%d\n",counttime,mRun,Qr,Com5InInd);
      Qsn[mRun] = Qr;
      IndQs[mRun] = Qr;
      aprun->dPh_s = Qr;
//printf("FS Qr=%f run=%d time2=%d\n",Qr,mRun,counttime);
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
      if (Al112[mRun] >2)   // �ନ஢���� ᮮ�饭�� �१ 2 ⠪� ����
      { err=0x112;
        if (err != errMB[mRun]) {
        Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
        errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
        }
      }
    }
    else
    if (fMB==4)    // ��騩 ��ꥬ FlowSic500
    { char c=*(pb+3); *(pb+3) = *(pb+4); *(pb+4)=c;
      kc = *(int*)(pb+3);   //  4100 - �����樥�� ��ꥬ� ���稪�
     Swap4(pb+5);
      Vrl = *(unsigned long*)(pb+5);   //4102 - ����� ��ꥬ
        if  ((kc < -3) || (kc > 2))
        KVolumeUZS[mRun] = 1.0;
        else
        KVolumeUZS[mRun] = 1.0/pow(10,kc);// 10((float)kc);
//printf("KVolume=%lf kc= %d Vrl=%ld\n",KVolumeUZS[mRun],kc,Vrl);
       if (KVolumeUZS[mRun] > 0)
       { Cmax = (double)Vpred[CONFIG_IDRUN[mRun].TypeCounter]/KVolumeUZS[mRun];
        aprun->Vtot_l=(double)Vrl / KVolumeUZS[mRun]; //100.0;
       }
    Swap4(pb+9);
       Vrl = *(unsigned long*)(pb+9); // 4104 - ���਩�� ��ꥬ
       if (KVolumeUZS[mRun] > 0)
       aprun->Vtrual=(double)Vrl / KVolumeUZS[mRun]; //100.0; //���਩�� ��ꥬ
       aprun->Vtrush = aprun->Vtot_l - aprun->Vtrual; //���� ������਩�� ��ꥬ
       Persist_SetQwCountTotal(mRun,aprun->Vtot_l);
//printf("Vtot=%lf\n",Vtot);
//       if (starttime<2)
        VolumeWS_RMG(mRun,aprun->Vtot_l);//H485_VolumeWS2(mRun,Vr1[mRun]);
       errMODBUS[mRun] = 0;
       err=0x112;
       if (err != errMB[mRun]) {
       Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
       errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
    }
   }  //if ((pb != 0) && (Com5InInd>count))
   else // �訡�� �裡
   {
//   printf("mRun=%d fMB=%d L=%d pb=%d Buf: %s\n",mRun,fMB,L,(pb!=0),Com5RecvBuf);
   if ((fMB==4)  || (fMB==3)) // &&(CONFIG_IDRUN[mRun].TypeRun != 10))
   {
     if (fMB==4)
     {// prun->Qw = 0.0;
       aprun->Qw_s = 0.0;
     }
     Al112[mRun]=0;
      if (errMODBUS[mRun] < CErrMB+1)
     errMODBUS[mRun]++;
//      printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
     if (errMODBUS[mRun] > (CErrMB/SysTakt)) // 3-6 ����⮪(3 ᥪ)
     {
//      printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
        prun->Qw = 0.0;
       err = 0x192;
       if (err != errMB[mRun])
       {
        Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
        errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
       }
     }
    } //   if ((fMB==4)  ||
   }  //else �訡�� �裡
   memset(MODBUS_RecvBuf,0,COM5_BUF_LEN);                 // ������ �室��� ���p
//printf("Reciev: %d Buf=%s\n",Com5InInd,Com5RecvBuf);
//printf("mRun=%d fMB=%d c=%d\n",mRun,fMB,counttime);
    if (fMB > 2)
    {  fMB--;
      InQueue(1,RS485IniHF);
    }
}
//------------------------------------------------------------
void Result_MODBUS_GUVR()
{
unsigned char     *pb;
int     i,count,np;
static unsigned int L;
unsigned long int Vrl;
unsigned  int  err;
struct IdRun    *prun;       // ��p����p� ��⪨
struct AdRun    *aprun;       // ��p����p� ��⪨
double VrS, Vth;
double Cmax; // = 4294.967296;
double  VrtSl;
unsigned char cm;
unsigned int err2;

  aprun  = &CONFIG_ADRUN[mRun];
  prun  = &CONFIG_IDRUN[mRun];
  Cmax =  429.4967296;
 pb = NULL;
 np=0; 			      // 㪠��⥫� �� ��砫� ���p�
 L=0;
 count = 128;
 if (UART3_InInd > 5)
 {
// if (CONFIG_IDRUN[mRun].TypeCounter == 4) cm=4;
// else
  cm=3;
    for (i=0; i<UART3_InInd; i++)     // ���� ����� � ����� ������
      if ((MODBUS_RecvBuf[i]==prun->AddrMODBUS) && (MODBUS_RecvBuf[i+1]==cm))
      {
	 pb = &MODBUS_RecvBuf[i];          // ��砫�� ���� ����
	 np = 1;
	 count =*(pb+2)+3;    // ����� ���p� ��� ��
	 break;                      // ���p �뤥���
      }
 }
  if ((np)&&(pb))
  L =  (Mem_Crc16(pb,count)==*(int*)(pb+count));// - �஢�ઠ ����஫쭮� �㬬� LCR
//printf("L=%d pb=%d Com5InInd=%d count=%d\n",L,(pb!=0),Com5InInd,count);
   if ((np) && (UART3_InInd>=count) && (L))
   {
    if (fMB==2)  // �����, ��室, ��ꥬ
     { cm = *(pb+7);
       *(pb+7) = *(pb+8); *(pb+8) = cm;
       StatusFS[mRun] = *(int*)(pb+7);
       AnalysAvFS(mRun);      // ��楤�� ࠧ��� ����� � �ନ஢���� ���਩
//    printf("StatusFS=%04X\n",StatusFS[mRun]);
     Qr = *(float*)(pb+3);
//     printf("3-Count=%d mRun=%d\n",counttime,mRun);
     Qsn[mRun] = Qr;
     IndQs[mRun] = Qr;
     aprun->dPh_s = Qr;
//printf("GV Qr=%f run=%d Qs0=%8.4f Qs1=%8.4f\n",Qr,mRun,IndQs[0],IndQs[1]);
      if (CONFIG_IDRUN[mRun].TypeRun == 10)
   {
    if (Qr < -prun->dPots) //Qst
// ����� "��砫� ���⭮�� ��⮪�) / "����� ��אַ�� ��⮪�"
    {
      if ((prun->KUzs & 0x80) ==0)  //����஥� �� ࠡ��� � ����� ��⮪��
      {
       Qsn[mRun] = -Qr;
       IndQs[mRun] = -Qr;
       aprun->dPh_s = -Qr;
       err2 = 0x4C;
       if (err2 != buf_err[1][mRun])
       {
        Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
        buf_err[1][mRun] = err2;               // �p�᢮��� ����� ���祭��
//printf("End forward");
       }
      }
      else   // �� ��אַ� ��⮪
      {  err2 = 0xC0;
       if (err2 != buf_err[0][mRun])
       {
        Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
        buf_err[0][mRun] = err2;               // �p�᢮��� ����� ���祭��
//printf("Begin revers");
       }
      }
    }
    else  //if (Qr < -prun->dPots)
     if (Qr > prun->dPots) //Qst
//����� "����� ���⭮�� ��⮪�" / "��砫� ��אַ�� ��⮪�"
     {
      if ((prun->KUzs & 0x80) == 0)  //����஥� �� ࠡ��� � ����� ��⮪��
      { err2 = 0xCC;
        if (err2 != buf_err[1][mRun])
        {
         Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
         buf_err[1][mRun] = err2;               // �p�᢮��� ����� ���祭��
//printf("Begin forward");
        }
      }
      else                // � ���� ��⮪��
      { err2 = 0x40;
        if (err2 != buf_err[0][mRun])
        {
         Mem_AlarmWrCodRun(mRun,err2,Mem_GetDayQ(mRun));
         buf_err[0][mRun] = err2;               // �p�᢮��� ����� ���祭��
//printf("End revers");
        }
       }
     }
    else
    {
     IndQs[mRun] = 0;
     aprun->dPh_s = 0;
    }
//printf("KUzs=%d\n",prun->KUzs);
     if ((prun->KUzs & 0x80)==0) //����� ��⮪
//   ����� ��ꥬ � ���⭮� ���ࠢ�����
     {
       Vrl = *(unsigned long*)(pb+17);
       Vhi  = *(unsigned long*)(pb+21);
     }
     else
// ����� ��ꥬ  � ��אַ� ���ࠢ�����
     {
       Vrl = *(unsigned long*)(pb+9);
       Vhi  = *(unsigned long*)(pb+13);
     }
     aprun->Vtot_l=(double)Vrl / 10000000.0;// KVolumeUZS[mRun];
//printf("Vrl=%lu\n",Vrl);
     aprun->Vtot_h = Vhi;
//printf("Vhi=%ld Cmax=%lf\n",Vhi,Cmax);
     Vth = (double)Vhi;
     VrS = Cmax*Vth +  aprun->Vtot_l;
//printf("Vrs=%lf Vth=%lf Vt_l=%lf\n",VrS, Cmax*Vth,aprun->Vtot_l);
     aprun->Vtot_l = VrS; //+VrtS[mRun];  // ����� (���਩�� ���� � ������)
     VrtS[mRun] = 0.0;   //��਩�� ???
     aprun->Vtrush = VrS; //-VrtS[mRun];  // ����
     aprun->Vtrual = VrtS[mRun];      // ���਩��
     Persist_SetQwCountTotal(mRun,aprun->Vtot_l);
//printf("Vtot=%lf\n",aprun->Vtot_l);
//     if (starttime<2)
     VolumeWS_RMG(mRun, aprun->Vtot_l);  //H485_VolumeWS2(mRun,Vr1[mRun]);
  } // if (CONFIG_IDRUN[mRun].TypeRun) == 10)
      errMODBUS[mRun] = 0;
      if (Al112[mRun] < 3)
       Al112[mRun]++;
     if (Al112[mRun] > 1)
     {
      err=0x112;
      if (err != errMB[mRun]) {
       Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
       errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
    } //if (fMB==2)
   }  //if ((pb != 0) && (Com5InInd>count))
   else // �訡�� �裡
   {//  printf("L=%d pb=%d Com5InInd=%d\n",L,(pb!=0),Com5InInd);
 //   printf("L=%d pb=%d Buf: %s\n",L,(pb!=0),Com5RecvBuf);
      if (CONFIG_IDRUN[mRun].TypeRun == 10)
      {//  prun->Qw = 0.0;
         aprun->Qw_s = 0.0;
      }
      if (errMODBUS[mRun] < CErrMB+1)
       Al112[mRun]=0;
       errMODBUS[mRun]++;  errMODBUS[mRun]++;
     if (errMODBUS[mRun] > CErrMB/(SysTakt)) // 10-5 ����⮪ (10 c)
     {
 //     printf("errMODBUS=%d errMB=%X\n",errMODBUS[mRun],errMB[mRun]);
        prun->Qw = 0.0;
      err = 0x192;
      if (err != errMB[mRun]) {
      Mem_AlarmWrCodRun(mRun,err,Mem_GetDayQ(mRun));
      errMB[mRun] = err;                 // �p�᢮��� ����� ���祭��
      }
     }
   }
   if (fMB > 2)
    {  fMB--;
      InQueue(1,RS485IniHF);
    }
/*    else
    {
     if (AnCom5 == 0x55)
     {
      outportb(CT_C5LCR,0x03);  // �p��� ��p����   n,8,1, �����
//printf("c3=%d\n",counttime);
     }
     else
     {
      outport(CT_SP1CT, 0x00E1);// ��⠭���� ��⮢ enable TX � RX, MODE1 (E1)
     }
   }
*/    
}

//-----------------------------------------------------------
void Result_485() {            //�ਥ� � ��� RS-485
   int               i,count;
   int               err;
   unsigned char     ksum,type;
   unsigned char     *pb;
   struct rs485data   *p485data;
   struct memalarm   *pmemalarm;        // ��p���p� ����� �p娢� ���p��
   float Value;
   p485data = &RS485DATA[rs485count];

// ct_DMA1Stop(); //��⠭����� �ਥ�
// ct_sp1inis();
//  Com4InInd = 40-inport(CT_D1TC);
                                        // ����p������ �p���⮣� ���p�
// memmove(Com4RecvBuf,Com5RecvBuf,Com5InInd);
  g_cnt485++;
   pb = 0;                              // 㪠��⥫� �� ��砫� ���p�
   for (i=0; i<LPUART1_InInd; i++) {    // ���� ����� � ����� ������
      if ((Com4RecvBuf[i]==ReqByte) && (Com4RecvBuf[i+1]==4)) {
         pb = &Com4RecvBuf[i];          // ��砫�� ���� ����
         count = RS485_KADR_LEN-1;     // ����� ���p� ��� ��
         break;                      // ���p �뤥���
      }
   }
//   printf("pb=%2.2X count=%d\n",pb,count);

// pmemalarm = &MEMALARM;
   ksum = 0;                           // ��砫쭮� ���祭�� ����p��쭮� �㬬�
				       // ��䨪�p����� ��砫� ���p�
   if (pb != 0 && (Com4RecvBuf+LPUART1_InInd-pb) >= RS485_KADR_LEN) {
      for (i=0; i<count; i++) {         // ����p��쭠� �㬬�
         ksum ^= pb[i];
//printf("%2.2X %2.2X %d\n",ksum, pb[i],i);
      }
//    printf("%2.2X %2.2X %d\n",ksum, pb[i],i);
      if (ksum == pb[i]) {             // �㬬� ᮢ����
//         memmove(&Com4Kadr,pb,RS485_KADR_LEN);   // ����p������ ���p�
           memmove(Com4Kadr,pb,RS485_KADR_LEN);   // ����p������ ���p�

         RS485DataWr();                // ������� �p���⮥ ���祭��
         if (p485data->timeout == (n485Out+1)) { // �p��. ���ﭨ� �訡��
	    Value = Mem_GetDayQ(p485data->run);
            Mem_AlarmWrCodRun(p485data->run,(1<<p485data->type)*256,Value);
         }
         p485data->timeout = 0;  // �p�� �訡�� �p���᪮� �⢥⮢
         Err485 = 0;
      }
      else                           // �訡�� ����p��쭮� �㬬�
         goto _rpt_test;
   }
   else {                            // ��� ��砫� ���p�
_rpt_test:
      if (p485data->timeout == n485Out) {           // ���⨦���� ���ﭨ� �訡��
         Err485 = 1; // �訡�� ���� RS485
         Mem_AlarmWrCodRun(p485data->run,(1<<p485data->type)*256 | 0x80,Mem_GetDayQ(p485data->run));
      }
      if (p485data->timeout < (n485Out+1))
         p485data->timeout++;                          // ���稪 ���⢥⮢
   }

   memset(Com4RecvBuf,0,COM4_BUF_LEN);                 // ������ �室��� ���p
   RS485Ini();        // ���樠����p�� ��p����� ���p�� ������ � ���稪�� 485
}
//...........................................................
void RS485DataWr()  { // ������� ����p����� ���祭�� � RS485DATA
   struct rs485data  *p485;
   unsigned char *pkadr;
   float fTmp,fTmp2;
   unsigned int iTmp;

   p485  = &RS485DATA[rs485count];
   pkadr = Com4Kadr;

   fTmp = Char485ToFloat(pkadr+2)*1000.0; // ��p����� � �����������
// fTmp2 = fTmp * 10.0;
// iTmp = (unsigned int)(fTmp2 + 0.5f);
// fTmp2 = (float)iTmp/10.0f;
   fTmp2 = fTmp * 10.0;
   fTmp2 = floor(fTmp2 + 0.5f);
   fTmp2 = fTmp2/10.0f;
   p485->Value = fTmp2;

// printf("p485->Value=%f\n",p485->Value);
}

//---------------------------------------------------------------------------
float Char485ToFloat(unsigned char *p) {
   float a;
   unsigned char* q = (unsigned char*)&a;
   unsigned long* val = (unsigned long*)&a;
   const float limit = 10.0e+05;

   *q++ = p[3];
   *q++ = p[2];
   *q++ = p[1];
   *q++ = p[0];

   if ((*val & 0x7f800000) == 0x7f800000)   // �᪫���� NAN � INFINITY
      *val &= 0xff7fffff;

   if (a < -limit)
      a = -limit;
   else if(a > limit)
      a = limit;

   return a;
}

//............. ����뫪� �� ��⪨ � ⠡���� 485 ............
void FromRunTo485(int Nrun, int Type, unsigned char *Addr) {

   if (((pr->sensData[Type].nInterface == tiAN_RS485)&&(Type<4))
      || ((pr->InterfaceDens ==  tiAN_RS485)&&(Type==4)))
  {
      p485->type = Type;
      p485->DevNum = *(Addr+0);  // ������ ����� ��-��
      p485->ChanNum = *(Addr+1); // ������ ����� ������
      p485->run = (unsigned char)Nrun;       // ����p ��⪨
      rs485num++;                            // ���-�� 485 ���稪��
      p485++;       //㪠��⥫� �� ᫥���騩
   }
}
//....................................................................
void SetRS485DataAdr() {       // ��p�. ������ 485-�������� �� ����-�� ���������
   int   NumRun;                   
   int j;

   NumRun = GLCONFIG.NumRun;       
   rs485num = 0;                   

   p485 = &RS485DATA[0];
   for (int i=0; i<NumRun; i++) {  
      pr = &CONFIG_IDRUN[i];      

      switch (pr->TypeRun) {      // case �� ������� ����p���� �� �����

         case 0:          // 0 - 3095  -  �� �����������
	    break;

         case 1:          // 1 - P+T+dP
            FromRunTo485(i,0,pr->hartadrt);
            FromRunTo485(i,1,pr->hartadrp);
            FromRunTo485(i,2,pr->hartadrdph);
            break;

         case 2:          // 2 - 3095+3051CD - �� �����������
            break;

	 case 3:          // 3 - P+T+dP+dP
            FromRunTo485(i,0,pr->hartadrt);
            FromRunTo485(i,1,pr->hartadrp);
            FromRunTo485(i,2,pr->hartadrdph);
            FromRunTo485(i,3,pr->hartadrdpl);
            break;

         case 4:          // 4 - �������
            FromRunTo485(i,0,pr->hartadrt);
            FromRunTo485(i,1,pr->hartadrp);
            break;
	 case 5:          // 5 -  ������� ��
            FromRunTo485(i,0,pr->hartadrt);
            FromRunTo485(i,1,pr->hartadrp);
            break;
	 case 10:          // 10 - ���������� ���
            FromRunTo485(i,0,pr->hartadrt);
            FromRunTo485(i,1,pr->hartadrp);
            break;
       }
       unsigned char Adr[2];
       Adr[0]=0x01;  // Nom Device
       Adr[1]=0x00;  // Nom Chanal
       if (pr->Dens)    FromRunTo485(i,4,Adr);
   }
     n485Out = 3;
     starttime += rs485num;              //   �p��� "p���p����" �������
}

//....................................................................
void RS485_SetRS485Err() {        // ��������� � ��������� ������
   struct  rs485data  *p485;
   int i;
   p485   = RS485DATA;           // ��������� �� ��p��� ����p �������

   for (i=0; i<rs485count;i++) {

      p485->Value = 0.0;         // �������� ���������� ��������

      p485->timeout = n485Out+1; // // ��� ������ �� �������

      p485->DiaErr  = 0;            // ������� ������� �� �������

      p485++;                     // ��������� �� ����p �������
   }

//   if (hartnum == 0)           // ���� ��� HART-��������, �������� �������
      for (i=0; i<3; i++) {
         CONFIG_ADRUN[i].statt = 0;
         CONFIG_ADRUN[i].statP = 0;
         CONFIG_ADRUN[i].statdPh = 0;
         CONFIG_ADRUN[i].statdPl = 0;
      }
   }
//..........................................................
void RS485_Set485Data(int numrun) { // ���������� ������ �� ������ �� ��p����p� 485-��������


   struct rs485data *p485;
   struct AdRun *aprun;
   struct IdRun *prun;
//   int numrun;
   int erdia;

//   numrun = Time_GetNumrun();           // �⥭�� ⥪�饣� ����p� ��⪨
   prun = &CONFIG_IDRUN[numrun];      // �. �� ��p���p� ⥪�饣� ����p� ��⪨
   aprun = &CONFIG_ADRUN[numrun];     // �. �� ��p���p� ⥪�饣� ����p� ��⪨
   p485 = RS485DATA;                    // 㪠��⥫� �� ��p�� ����p ���稪�

   for (int i=0; i<rs485num;i++) {

      if (p485->run == numrun) {   // ����p ��⪨ (0...2), � ���p�� �p��p����� ����.

         switch(p485->type) {      // ⨯ ����p塞��� ��p����p�

            case 0:                            // 1-�����p���p�
               aprun->statt = (p485->timeout >= (n485Out+1)) ?
                  (aprun->statt & 0x0C) | 2 : aprun->statt & 0x0C; // ��� �⢥�
               aprun->typet   = p485->type;

               if (p485->timeout == 0) {
                  erdia = ChkDiaT485(prun,aprun,p485); // ����஫� ���������
//                 if (erdia == 0)
//                    aprun->t = p485->Value;
               }
               break;

            case 1:                             // 2-��������
               aprun->statP = (p485->timeout >= (n485Out+1)) ?
                  (aprun->statP & 0x0C) | 2 : aprun->statP & 0x0C; // ��� �⢥�
               aprun->typep  = p485->type;

               if (p485->timeout == 0) {
                  erdia = ChkDiaP485(prun,aprun,p485); // ����஫� ���������
//                  if (erdia == 0)
//                     aprun->P = p485->Value;
//printf("p485->P= %f ",p485->Value);
               }
               break;
/*
            case 2:        // 4-��p��. ��p����
               aprun->statdPh = (p485->timeout >= (n485Out+1)) ?
                  (aprun->statdPh & 0x0C) | 2 : aprun->statdPh & 0x0C; // ��� �⢥�
               aprun->typedph  = p485->type;

               if (p485->timeout == 0) {
                  erdia = ChkDiadPh485(prun,aprun,p485);   // ����஫� ���������
//                  if (erdia == 0)
//                     aprun->dPh = p485->Value;
               }
               break;

            case 3:                    // 8-����.��p����
               aprun->statdPl = (p485->timeout >= (n485Out+1)) ?
                  (aprun->statdPl & 0x0C) | 2 : aprun->statdPl & 0x0C; // ��� �⢥�
               aprun->typedpl  = p485->type;

               if (p485->timeout == 0) {
                  erdia = ChkDiadPl485(prun,aprun,p485);   / �������� ���������
//                  if (erdia == 0)
//                     aprun->dPl = p485->Value;
               }
               break;
*/               
            case 4:                      // ���������
               if (p485->timeout == 0) {
                  erdia = ChkDiaPl485(prun,aprun,p485);   // �������� ���������
               }
               break;
         }
      }
      p485++; //㪠��⥫� �� ����p ���稪�
   }
   SetCopyCalcData(numrun);     // ����� ����� ��� ���������� ��ࠬ��஢
}
//............................................................
int ChkDiaT485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485) {
// struct memalarm *pm;            // ��p���p� ����� �p娢� ���p��
// pm = &MEMALARM;
   int numrun;
   float Top,Bottom,Value;
//   numrun = Time_GetNumrun();           // �⥭�� ⥪�饣� ����p� ��⪨
//   aprun = &ADCONFIG.ADRUN[numrun];

   if (prun->constdata & 4)  {      // ⥬������ � ���稪��
            // ���௮��஢����� ���祭�� �
      Value = GetCalibrValue(Time_GetNumrun(),0,p485->Value);
      Top = prun->tmax+1.0;
      Bottom = prun->tmin-1.0;
//    printf ("Value=%f Top=%f Bottom=%f\n", Value, Top, Bottom);
      if ((Value >= Bottom) && (Value <= Top)){  // � ��ଥ
// printf ("DiaErr=%d  n485Out=%d\n",p485->DiaErr,n485Out);
         if (p485->DiaErr == (n485Out+1)) {  // �p��. ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x100,Mem_GetDayQ(p485->run));
            aprun->statt &= 0xF7;             // �p���� �訡�� ���������
            p485->status &= 0xF7;            // �p���� �訡�� ���������
         }
         p485->DiaErr = 0;                   // �p�� ���稪 �訡�� ����������
               aprun->edt = 0x20;                 // ��� ⥬��p���p�
               aprun->t   = Value;                // ���祭��

         return 0;
      }
      else {
         if (p485->DiaErr == n485Out) {      // ���⨦���� ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x180,Mem_GetDayQ(p485->run));

            aprun->statt |= 0x08;             // ��⠭����� �p����� �訡�� ��������
            p485->status |= 0x08;            // ��⠭����� �p����� �訡�� ���������
         }
         if (p485->DiaErr < (n485Out+1))
            p485->DiaErr++;                  // ���稪 �訡��
         return 1;
      }
   }
   else
      return 1;                              // ����뫪� �� �㦭�
}
//............................................................
int ChkDiaP485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485) {
   float Top,Bottom,Value;

   if (prun->constdata & 2)  {      // �������� � ���稪��

                                    // ���௮��஢����� ���祭�� P
      Value = GetCalibrValue(Time_GetNumrun(),1,p485->Value);

      if (prun->TypeP == 0) {       // ��᮫�⭮�
         Top = valALARM[1]*1.2;
         Bottom = valALARM[0]*0.9;
      }
      else {
         Top = valALARM[1]*1.2;
         Bottom = valALARM[0]-1.0;
      }
//printf("Top=&f Bot=%f Val=%f\n",Top,Bottom,Value);
      if ((Value >= Bottom) && (Value <= Top)){
         if (p485->DiaErr == (n485Out+1)) {   // �p��. ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x200,Mem_GetDayQ(p485->run));

            aprun->statP &= 0xF7;              // �p���� �訡�� ���������
            p485->status &= 0xF7;              // �p���� �訡�� ��������
         }
         p485->DiaErr = 0;                    // �p�� ���稪 �訡�� ����������
         aprun->P   = Value;                // ���祭��
         aprun->edp = 0x0A;
         aprun->P_s   = Value;                // ���祭��
//printf("P=%f\n ",aprun->P);
         return 0;
      }
      else {
         if (p485->DiaErr == n485Out) {       // ���⨦���� ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x280,Mem_GetDayQ(p485->run));

            aprun->statP |= 0x08;   // ��⠭����� �p����� �訡�� ���������
            p485->status |= 0x08;   // ��⠭����� �p����� �訡�� ���������
         }
         if (p485->DiaErr < (n485Out+1))
            p485->DiaErr++;        // ���稪 �訡�� ���������
         return 1;
      }
   }
   else
      return 1;                    // ����뫪� �� �㦭�
}
//............................................................
/*
int ChkDiadPh485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485) {
   float Top,Bottom,Value;
   int run;
   run =  Time_GetNumrun();
   if (prun->constdata & 1)  {     // ��९�� � ���稪��
                                   // ���௮��஢����� ���祭�� dPh
      Value = GetCalibrValue(run,2,p485->Value);

      Top = prun->dPhmax * 1.2;
      Bottom = -GetDpMin();

      if ((Value >= Bottom) && (Value <= Top)) {
         if (p485->DiaErr == (n485Out+1)) {   // �p��. ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x400,Mem_GetDayQ(p485->run));

            aprun->statdPh &= 0xF7;   // �p���� �訡�� ���������
            p485->status &= 0xF7;    // �p���� �訡�� ���������
         }
         p485->DiaErr = 0;           // �p�� ���稪� �訡�� ����������
         aprun->dPh = Value;
         aprun->eddph = 0xEE;
         aprun->dPh_s   = Value;                // ���祭��
	if ((prun->TypeRun != 3)&& prun->TypeRun != 2)  // ��� ������� ��९���
	{      SetShelf[run] = 0;
	      if (GetDpMinShelf() != 0) {   //䫠� "���室 �� Qmin" ���
						   // 26.10.07 ��� ��࣠���
		if (Value > prun->dPots && Value < prun->dPhmin) {
			 Value= prun->dPhmin;
			  aprun->dPh = prun->dPhmin;
			  SetShelf[run] = 1;
			}
		     }                              //++++++++++++++++++
//printf("Val=%f dPl=%f\n",Value,aprun->dPh);
	      }  // if ((prun->TypeRun  != 3
         return 0;
      }
      else {
         if (p485->DiaErr == n485Out) {       // ���⨦���� ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x480,Mem_GetDayQ(p485->run));

            aprun->statdPh |= 0x08;     // ��⠭����� �p����� �訡�� ���������
            p485->status |= 0x08;      // ��⠭����� �p����� �訡�� ���������
         }
         if (p485->DiaErr < (n485Out+1))
            p485->DiaErr++;              // ���稪 �訡�� ���������
         return 1;
      }
   }
   else
      return 1;                    // ����뫪� �� �㦭�

}
//............................................................
int GetDpMinR() {                        // 13.09.06
   return 6300;
}

int ChkDiadPl485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485) {
// struct memalarm *pm;            // ��p���p� ����� �p娢� ���p��
// pm = &MEMALARM;
   float Top,Bottom,Value;
  int run;

   run =  Time_GetNumrun();
   if (prun->constdata & 1)  {     // ��९�� � ���稪��

                                   // ���௮��஢����� ���祭�� dPl
      Value = GetCalibrValue(run,3,p485->Value);

      Top = prun->ValueSwitch * 1.2;
      Bottom = -GetDpMinR();

      if ((Value >= Bottom) && (Value <= Top)){
         if (p485->DiaErr == (n485Out+1)) {   // �p��. ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x800,Mem_GetDayQ(p485->run));

            aprun->statdPl &= 0xF7;   // �p���� �訡�� ���������
            p485->status &= 0xF7;    // �p���� �訡�� ���������
         }
         p485->DiaErr = 0;           // �p�� ���稪� �訡�� ����������
         aprun->dPl = Value;
         aprun->eddpl = 0xEE;
         aprun->dPl_s   = Value;                // ���祭��
	 SetShelf[run] = 0;
	      if (GetDpMinShelf() != 0) {   //䫠� "���室 �� Qmin" ���
						   // 26.10.07 ��� ��࣠���
		if (Value > prun->dPots && Value < prun->dPhmin) {
			 Value= prun->dPhmin;
			  aprun->dPl = prun->dPhmin;
			  SetShelf[run] = 1;
			}
		     }                              //++++++++++++++++++
         return 0;
      }
      else {
         if (p485->DiaErr == n485Out) {       // ���⨦���� ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x880,Mem_GetDayQ(p485->run));

            aprun->statdPl |= 0x08;     // ��⠭����� �p����� �訡�� ���������
            p485->status |= 0x08;      // ��⠭����� �p����� �訡�� ���������
         }
         if (p485->DiaErr < (n485Out+1))
            p485->DiaErr++;            // ���稪 �訡�� ���������
         return 1;
      }
   }
   else
      return 1;                    // ����뫪� �� �㦭�
}
*/
int ChkDiaPl485 (struct IdRun *prun, struct AdRun *aprun, struct rs485data *p485) {
   float Top,Bottom,Value;
   unsigned int err;
                                   // ���௮��஢����� ���祭�� Pl
      Value = GetCalibrValue(Time_GetNumrun(),4,p485->Value);
//printf("pV=%f V=%f\n",p485->Value,Value);
         Ros_flt= Value;
         TAU_flt = p485->Value;
      Top =  1.18;
      Bottom = 0.66;
      if ((Value > Bottom) && (Value < Top)){
         if (p485->DiaErr == (n485Out+1)) {   // �p��. ���ﭨ� �訡��
            Mem_AlarmWrCodRun(p485->run,0x04,Mem_GetDayQ(p485->run));
            p485->DiaErr = 0;
            p485->status &= 0xF7;    // �p���� �訡�� ���������
         }
         FreqAlarm = 0;
         p485->DiaErr = 0;           // �p�� ���稪� �訡�� ����������
         aprun->ROdens = Value*KoRo[0][GLCONFIG.EDIZM_T.T_SU];
//         aprun->ROchr  = Value;                // ���祭��
//printf("1-Ros=%f TAU=%f\n",Ros_flt,TAU_flt);
         return 0;
      }
      else {
         if (p485->DiaErr == n485Out) {       // ���⨦���� ���ﭨ� �訡��
           if  (Value <= Bottom) err = 0x1A6;
           else if (Value >= Top) err = 0x1A5;
            Mem_AlarmWrCodRun(p485->run,err,Mem_GetDayQ(p485->run));
            Mem_AlarmWrCodRun(p485->run,0x84,Mem_GetDayQ(p485->run));
 //           aprun->ROdens = prun->ROnom;
//printf("2-Ros=%f TAU=%f\n",Ros_flt,TAU_flt);
	    FreqAlarm = 1;
            p485->status |= 0x08;      // ��⠭����� �p����� �訡�� ���������
         }
         if (p485->DiaErr < (n485Out+1))
            p485->DiaErr++;            // ���稪 �訡�� ���������
         return 1;
      }
}

//...........................................................
void RS485Watch()
{
   // �।����饥 ���祭�� ���稪� ��।����� ᨬ�����
//   static unsigned int st_cnt485;

   // ���稪 ��� ॠ����樨 ����প�
   static int st_cntDelay485;

   // ���쭥��� ��� �㤥� ��뢠���� ����� ��⨩ ࠧ
   if(++st_cntDelay485 < 3)
      return;

   st_cntDelay485 = 0;

   if(g_cnt485 == st_cnt485)
    {
      RS485Com4IniHF();
      RS485Ini();
    }
   st_cnt485 = g_cnt485;
}

//--------------------------------------------------
uint32_t DiffSec(struct bintime *Time1,struct bintime *Time2)  // �������� (Time1-Time2)
{  uint8_t D;
   uint32_t t1,t2;
    
  if   (Time1->date < Time2->date) 
        D = Time1->date + 31 ;
  else  D = Time1->date;
  t2= (long)Time2->date*86400L + (long)Time2->hours*3600L+(long)(Time2->minutes*60)+(long)Time2->seconds;
  t1= (long)D*86400L +(long)BINTIME.hours*3600L+(long)(BINTIME.minutes*60)+(long)BINTIME.seconds;
  return (t1-t2);
} 
//-----------------------------------------------------------------
void Swap4(uint8_t *p)
{ char t;
	t = p[0];
	p[0] = p[3];
	p[3] = t;
	t = p[1];
	p[1] = p[2];
	p[2] = t;
}
//------------------------------------------------------------------------