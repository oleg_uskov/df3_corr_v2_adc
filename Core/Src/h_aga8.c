//////////////////////////////////////////////////////////////////
// ������ ������� ����������� �� ������ AGA8-DC92
//////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <mem.h>
//#include <dos.h>
//#include <time.h>
#include "h_calc.h"
#include "h_mem.h"
#include "h_main.h"

#include "H_AGA8.h"


//-----------------------------------------------------------
//extern float XJ[], XJ1[], XJ2[];
//extern int CalcZc[];
extern int counttime;
extern    struct calcdata     CALCDATA[3];
extern struct glconfig  GLCONFIG;
extern struct IdRun CONFIG_IDRUN[3];
//extern char CalcBRZ;
extern char nRnz,nRnZc,nRnB;
extern struct bintime     BINTIME;      // �������� ��p����p� �p�����
extern unsigned int ExtChroma;
extern char CalcBR[];
extern char Z_Out[],Z_in[];
extern int NCC;
extern float KUh;
extern struct compress ZcKs[3];
extern float ZcK1[3][2];
extern char bROavto;
extern float KoRo[3][3];
extern double KoefEn[];

float RLair[3] = {1.204449,1.292923,1.225410}; //20, 0, 15
unsigned char Runed_1_3; // ���� ����� ���� ��� ����� ��� � �������������� 3-� �����
float XI[21];
double  BMIX[3],DRo[3];
double RGAS,TCM, DCM, AZ[3], AD,DR, MWX;
double  FN[3][58];
float P, TOLD[3],Trun[3],Prn[3]; //T,
double T;
double ROa[3];
double  WW[3], HH[3],  TOL;
double UU[3],RK3P0[3], Q2P0[3];
double AX1[3], AX2[3], AX3[3], FL[3], F1[3], F2[3], F3[3];
double  RK5P0,RK2P5,U5P0,U2P5,Q1P0;
double ROfhp[3],HSfhp[3];
double E1;
float     P_st = 0.101325;        // MPa
extern float     T_st; // = 273.15;  //293.15;      // T=0 ��.���.
char NumChrom[3];
char bT1,bT2,TimeCalc;
float Dmin;//0.000001;   //�������� ��������� ���� ������
float Dmax;//40.0;    //�������� ��������� ���� ������
//float  delP;     //= fabs(P - Prn[nRnZc]);

//double   CMW[21],EI[21],RKI[21],WI[21],QI[21],HI[21],MI[21],DI[21];
//double   BEIJ[21][21],BKIJ[21][21],BWIJ[21][21],BUIJ[21][21] ;
int iAGA, iBMIX;
 //   ,AGA_F[3];
int CID[21];
//extern unsigned int pZ;
//void DCAGA (double*);        // ���� �������
void TEMP (double,int);
void DZOFPT(int, float ,float);
void AGA8_1(void);  //(int,double, double);
void BMIX_B(void); //(double*);
void Task_Calc_CorrQnom(void);
void TaskCalc_RO(void);
//void ToCalc();
//void n_delay (int mlsek);

void initialData(void)
{ int i,j;

EIJ[0][0] = 1.0;
for (j=1; j<19; j++)
  EIJ[0][j] = EE1[j-1];
for (j=19; j<21; j++)
  EIJ[0][j] = 1.0;
EIJ[1][0] = 1.0; EIJ[1][1] = 1.0;
for (j=2; j<14; j++)
  EIJ[1][j] = EE2[j-2];
for (j=14; j<21; j++)
  EIJ[1][j] = 1.0;
EIJ[2][0] = 1.0; EIJ[2][1] = 1.0; EIJ[2][2] = 1.0;
for (j=3; j<19; j++)
  EIJ[2][j] = EE3[j-3];
for (j=19; j<21; j++)
  EIJ[2][j] = 1.0;
for (j=0; j<4; j++) EIJ[3][j] = 1.0;
for (j=4; j<14; j++)
  EIJ[3][j] = EE4[j-4];
for (j=14; j<21; j++)
  EIJ[3][j] = 1.0;
for (j=0; j<7; j++) EIJ[4][j] = 1.0;
for (j=7; j<12; j++)
  EIJ[4][j] = EE5[j-7];
for (j=12; j<21; j++)
  EIJ[4][j] = 1.0;

for (j=0; j<21; j++)
  EIJ[5][j] = 1.0;

for (j=0; j<14; j++) EIJ[6][j] = 1.0;
for (j=14; j<19; j++)
  EIJ[6][j] = EE7[j-14];
for (j=19; j<21; j++)
  EIJ[6][j] = 1.0;

for (j=0; j<8; j++) EIJ[7][j] = 1.0;
for (j=8; j<12; j++)
  EIJ[7][j] = EE8[j-8];
for (j=12; j<21; j++)
  EIJ[7][j] = 1.0;

for (i=8; i<21; i++)
for (j=0; j<21; j++)
  EIJ[i][j] = 1.0;

//------------------------------
   UIJ[0][0] = 1.0;
for (j=1; j<19; j++)
  UIJ[0][j] = UU1[j-1];
for (j=19; j<21; j++)
  UIJ[0][j] = 1.0;

   UIJ[1][0] = 1.0; UIJ[1][1] = 1.0;
for (j=2; j<12; j++)
  UIJ[1][j] = UU2[j-2];
for (j=12; j<21; j++)
  UIJ[1][j] = 1.0;

   UIJ[2][0] = 1.0; UIJ[2][1] = 1.0; UIJ[2][2] = 1.0;
for (j=3; j<19; j++)
  UIJ[2][j] = UU3[j-3];
for (j=19; j<21; j++)
  UIJ[2][j] = 1.0;

for (j=0; j<4; j++) UIJ[3][j] = 1.0;
for (j=4; j<14; j++)
  UIJ[3][j] = UU4[j-4];
for (j=14; j<21; j++)
  UIJ[3][j] = 1.0;

for (i=4; i<6; i++)
for (j=0; j<21; j++)
  UIJ[i][j] = 1.0;

for (j=0; j<14; j++) UIJ[6][j] = 1.0;
for (j=14; j<19; j++)
  UIJ[6][j] = UU7[j-14];
for (j=19; j<21; j++)
  UIJ[6][j] = 1.0;

for (i=7; i<21; i++)
for (j=0; j<21; j++)
  UIJ[i][j] = 1.0;

  KIJ[0][0] = 1.0;
for (j=1; j<19; j++)
  KIJ[0][j] = KK1[j-1];
for (j=19; j<21; j++)
  KIJ[0][j] = 1.0;

  KIJ[1][0] = 1.0; KIJ[1][1] = 1.0;
for (j=2; j<8; j++)
  KIJ[1][j] = KK2[j-2];
for (j=8; j<21; j++)
  KIJ[1][j] = 1.0;

  KIJ[2][0] = 1.0; KIJ[2][1] = 1.0; KIJ[2][2] = 1.0;
for (j=3; j<19; j++)
  KIJ[2][j] = KK3[j-3];
for (j=19; j<21; j++)
  KIJ[2][j] = 1.0;

  for (j=0; j<4; j++) KIJ[3][j] = 1.0;
for (j=4; j<8; j++)
  KIJ[3][j] = KK4[j-4];
for (j=8; j<21; j++)
  KIJ[3][j] = 1.0;

for (i=4; i<6; i++)
for (j=0; j<21; j++)
  KIJ[i][j] = 1.0;

  for (j=0; j<14; j++) KIJ[6][j] = 1.0;
  for (j=14; j<19; j++)
    KIJ[6][j] = KK7[j-14];
  for (j=19; j<21; j++)
    KIJ[6][j] = 1.0;

for (i=7; i<21; i++)
 for (j=0; j<21; j++)
  KIJ[i][j] = 1.0;
//--------------------------------------

for (i=0; i<21; i++)
 for (j=0; j<21; j++)
  GIJ[i][j] = 1.0;
GIJ[0][2] = 0.807653;
GIJ[0][7] = 1.957310;
GIJ[1][2] = 0.982746;
GIJ[2][3] = 0.370296;
GIJ[2][5] = 1.673090;

RGAS = 8.31451e-3;
//TLOW = 0.0 ;
//THIGH = 10000.0;
//PLOW = 0.5e-9 ;
//PHIGH = 275.0 ;
//DHIGH = 12.0 ;
}

//======================================================================
void Bf(double T, double *BMIXk)
{
//    PURPOSE:
//      Calculates the second virial coefficient using the AGA8� method given temperature.
//    DESCRIPTION OF ARGUMENTS:
//      �      - Temperature in kelvins. (Input)
//      BMIXk   - Second virial coefficient in mol/dm^3. (Output)
//------------------------------------------------------------------------------------------
double T0P5, T2P0, T3P0, T3P5, T4P5, T6P0, T11P0;
double   T7P5,T9P5,T12P0,T12P5,BM ;
int n;
//printf("Bf nRun=%d T=%f\n",nRnZc,T);
  T0P5   =sqrt(T);   //T0P5 ??
  T2P0 =T*T;
  T3P0 = T*T2P0;
  T3P5 = T3P0*T0P5 ;
  T4P5 = T*T3P5 ;
  T6P0 =T3P0*T3P0;
  T11P0 = T4P5*T4P5*T2P0 ;
  T7P5 =T2P0*T*T0P5 ;
  T9P5  = T7P5*T2P0 ;
  T12P0 = T9P5*T0P5*T2P0 ;
  T12P5 = T12P0*T0P5 ;
//printf("TO=%le T2=%le T30=%le T35=%le T4=%le T6=%le T11=%le T7=%le T9=%le T120=%le T125=%le\n",
//    T0P5,T2P0,T3P0,T3P5,T4P5,T6P0, T11P0, T7P5,T9P5,T12P0, T12P5);
double BM1 = BI[nRnZc][0]+BI[nRnZc][1]/T0P5+BI[nRnZc][2]/T;
double BM2 = BI[nRnZc][3]/T3P5+BI[nRnZc][4]*T0P5+BI[nRnZc][5]/T4P5;
double BM3 = BI[nRnZc][6]/T0P5+BI[nRnZc][7]/T7P5+BI[nRnZc][8]/T9P5+BI[nRnZc][9]/T6P0+BI[nRnZc][10]/T12P0;
BM3 = BM3 + BM1 + BM2;
 BM1 = BI[nRnZc][11]/T12P5+BI[nRnZc][12]*T6P0+BI[nRnZc][13]/T2P0+BI[nRnZc][14]/T3P0+BI[nRnZc][15]/T2P0;
 BM2 = BI[nRnZc][16]/T2P0+BI[nRnZc][17]/T11P0 ;
*BMIXk = BM1 + BM2 + BM3;
// BMIXk=BI[nRnZc][0]+BI[nRnZc][1]/T0P5+BI[nRnZc][2]/T+BI[nRnZc][3]/T3P5+BI[nRnZc][4]*T0P5+BI[nRnZc][5]/T4P5
//    +BI[nRnZc][6]/T0P5+BI[nRnZc][7]/T7P5+BI[nRnZc][8]/T9P5+BI[nRnZc][9]/T6P0+BI[nRnZc][10]/T12P0
//    +BI[nRnZc][11]/T12P5+BI[nRnZc][12]*T6P0+BI[nRnZc][13]/T2P0+BI[nRnZc][14]/T3P0+BI[nRnZc][15]/T2P0
//    +BI[nRnZc][16]/T2P0+BI[nRnZc][17]/T11P0 ;
//printf("Bf BMIX=%le T=%le run=%d\n",*BMIXk,T,nRnZc);
/*printf("2-BI ");
 for (n=0; n < 18; n++)
printf(" %le ",BI[nRnZc][n]);
printf("\n");
*/
}
void Task_Start_Calc(void);
void DZOFPT_B(void)
{
    nRnz = Time_GetNumrun();
    DZOFPT(nRnz, P, T); 
    InQueue(2,Task_Start_Calc);
}
//======================================================================
 //void CHARDL ( double* XI)  //, double* ZB, double* DB)
 //{
//    PURPOSE:
//      Sets up composition dependent terms. Subroutine PARAMDL should
//      be called before this routine

//    DESCRIPTION OF ARGUMENTS:
//      NCC      - Number of components. (Input)
//      XI       - Array values for mole fraction composition. (Input)
//                This array corresponds to the CID array used in subroutine PARAMDL
//      ZB       - Compressibility at 60�F and 14.73 psia. (Output)
//      DB      - Molar density at 60�F and 14.73 psia. (Output)
//------------------------------------------------------------------------------------------
void BRAKET1(float* XI)
{
int i,j,n;
//double ZDETAIL ; //DDETAIL,
double  TMFRAC;

//printf("BR=%d s=%d c1=%d ",BR,BINTIME.seconds,counttime);
for (j=0; j < 3; j++)
 TOLD[j] = 0;
//::.Normalize mole fractions and calculate molar mass
TMFRAC =0.0;
 for (j=0; j < NCC; j++)
 {   TMFRAC = TMFRAC + XI[j];
}
//printf("BRAKET NCC=%d TMFRAC-%f\n",NCC, TMFRAC);
 for ( j=0; j < NCC; j++)
 {    XI[j] = XI[j]/TMFRAC;
//printf(" %f", XI[j]);
 }
//printf("\n");
 for (n=0; n < 18; n++)
 BI[nRnZc][n] = 0.0;

RK5P0=0.0;
RK2P5=0.0;
U5P0=0.0;
U2P5=0.0;
Q1P0=0.0;
MWX = 0;

 WW[nRnZc] = 0.0;
 HH[nRnZc] = 0.0;
 Prn[nRnZc] = 0.09;
 E1 = 0;
// double MWR = 28.96256;

 for (j=0; j<NCC; j++)
 {  MWX=MWX+XI[j]*MW[CID[j]];
    E1 = E1+XI[j]*HI[CID[j]];
 }

//printf("MWX=%le E1=%le\n",MWX,E1);
 iBMIX = 0;
//printf("TMFRAC = %le ",TMFRAC);
//printf("s=%d nRnz=%d nRnZc=%d\n",BINTIME.seconds,nRnz,nRnZc);
// if ((BR==1)||(CalcBR[nRnz]==1)) nRnZc = nRnz;  // nRnZc=nRnz;}
 nRnZc = Time_GetNumrun();
 bT1 = 1; bT2 = 0;
   BMIX_B(); //(XI) ;
//    if (BR==1)  DZOFPT(nRnB, P, T);
//    else
 // InQueue(2,DZOFPT_B);  
// DZOFPT(nRnZc, P, T);  //���� Zc
}
//=====================================================
 void BMIX_B(void)//(double* XI)
{ int i,j,n;
  int ii,jj;
  double  XIJ,WIJ,EEIJ,E0P5,E2P0,E3P0,E3P5,E4P5,E6P0;
  double  E7P5,E9P5,E12P0,E12P5;
  double  E11P0,S3;
B1:
    i = iBMIX;

  { ii = CID[i];
//printf("i=%d ii=%d CID=%d XI=%e KI=%e RK2P5=%e\n",i,ii,CID[i],XI[i],KI[ii],RK2P5);
    RK2P5  =RK2P5 + XI[i]*KI[ii]*KI[ii]*sqrt(KI[ii]) ;
    U2P5   =U2P5 + XI[i]*EI[ii]*EI[ii]*sqrt(EI[ii]) ;
    WW[nRnZc] = WW[nRnZc] + XI[i]*GI[ii];
    Q1P0  =Q1P0 + XI[i]*QI[ii];
    HH[nRnZc] = HH[nRnZc] + XI[i]*XI[i]*FI[ii];
//printf("iBMIX=%d nRnZc=%d GI=%f WW1=%le WW2=%le HH=%le\n",i,nRnZc,GI[i],WW[nRnZc],WW[(nRnZc+1)%2],HH[nRnZc]);
    for (j=i;  j < NCC; j++)
    { if (i != j)
       XIJ = 2*XI[i]*XI[j];
      else
       XIJ = XI[i]*XI[j];
      jj = CID[j];
//if (i==0)
//printf("jj=%d XIJ=%le\n",jj,XIJ);
     if (KIJ[ii][jj] != 1.0)
      RK5P0=RK5P0 + XIJ * (KIJ[ii][jj]*KIJ[ii][jj]*KIJ[ii][jj]*KIJ[ii][jj]
      *KIJ[ii][jj]-1.0) * KI[ii]*KI[ii]*KI[jj]*KI[jj]*sqrt(KI[ii]*KI[jj]);
     if (UIJ[ii][jj] != 1.0)
      U5P0=U5P0 + XIJ*(UIJ[ii][jj]*UIJ[ii][jj]*UIJ[ii][jj]*UIJ[ii][jj]*UIJ[ii][jj]-1.0)
      *sqrt(EI[ii]*EI[ii]*EI[ii]*EI[ii]*EI[ii]*EI[jj]*EI[jj]*EI[jj]*EI[jj]*EI[jj]);
     if  (GIJ[ii][jj] != 1.0)
      WW[nRnZc] = WW[nRnZc]+XIJ*(GIJ[ii][jj]-1.0)*((WI[ii] + WI[jj])/2.0);
//:..Terms in second viria! coefficients
      EEIJ = EIJ[ii][jj]*sqrt(EI[ii]*EI[jj]);
//printf("EEIJ=%le ii=%d jj=%d EIJ=%5.3f EIi=%le EIj=%le\n",EEIJ, ii,jj,EIJ[ii][jj],EI[ii],EI[jj]);
      WIJ = GIJ[ii][jj]*(GI[ii]+GI[jj])/2.0;
      E0P5 = sqrt(EEIJ);
      E2P0 = EEIJ*EEIJ ;
      E3P0 = EEIJ*E2P0 ;
      E3P5 = E3P0*E0P5 ;
      E4P5 = EEIJ*E3P5 ;
      E6P0 = E3P0*E3P0 ;
      E11P0= E4P5*E4P5*E2P0 ;
      E7P5 = E4P5*EEIJ*E2P0 ;
      E9P5 = E7P5*E2P0 ;
      E12P0=E11P0*EEIJ ;
      E12P5= E12P0*E0P5 ;
      S3 = XIJ*KI[ii]*KI[jj]*sqrt(KI[ii]*KI[jj]) ;
      BI[nRnZc][0] = BI[nRnZc][0] + S3 ;
      BI[nRnZc][1] = BI[nRnZc][1] + S3*E0P5 ;
      BI[nRnZc][2] = BI[nRnZc][2] + S3*EEIJ ;
      BI[nRnZc][3] = BI[nRnZc][3] + S3*E3P5 ;
      BI[nRnZc][4] = BI[nRnZc][4] + S3*WIJ/E0P5 ;
      BI[nRnZc][5] = BI[nRnZc][5] + S3*WIJ*E4P5 ;
      BI[nRnZc][6] = BI[nRnZc][6] + S3*QI[ii]*QI[jj]*E0P5 ;
      BI[nRnZc][7] = BI[nRnZc][7] + S3*SI[ii]*SI[jj]*E7P5 ;
      BI[nRnZc][8] = BI[nRnZc][8] + S3*SI[ii]*SI[jj]*E9P5 ;
      BI[nRnZc][9] = BI[nRnZc][9] + S3*WI[ii]*WI[jj]*E6P0 ;
      BI[nRnZc][10] = BI[nRnZc][10] + S3*WI[ii]*WI[jj]*E12P0 ;
      BI[nRnZc][11] = BI[nRnZc][11] + S3*WI[ii]*WI[jj]*E12P5 ;
      BI[nRnZc][12] = BI[nRnZc][12] + S3*FI[ii]*FI[jj]/E6P0 ;
      BI[nRnZc][13] = BI[nRnZc][13] + S3*E2P0 ;
      BI[nRnZc][14] = BI[nRnZc][14] + S3*E3P0 ;
      BI[nRnZc][15] = BI[nRnZc][15] + S3*QI[ii]*QI[jj]*E2P0 ;
      BI[nRnZc][16] = BI[nRnZc][16] + S3*E2P0 ;
      BI[nRnZc][17] = BI[nRnZc][17] + S3*E11P0 ;
//if (i==0)
//printf("S3=%le XIJ=%le E11P0=%le EEIJ=%le KI=%le BI[0]=%le BI[1]=%le\n",
//       S3,XIJ,E11P0,EEIJ,KI[ii], BI[0],BI[1]);
   }  // end j
//printf("WW1=%le WW2=%le ",WW[nRnZc],WW[(nRnZc+1)%2]);

} // end i
//printf("iBMIX=%d RK5P0=%le,RK2P5=%le,U5P0=%le,U2P5=%le Q1P0=%le HH=%le\n",iBMIX,RK5P0,RK2P5,U5P0,U2P5,Q1P0,HH);
   iBMIX++;
//printf("iBMIX=%d s=%d c2=%d ",iBMIX,BINTIME.seconds,counttime); //BINTIME.seconds,
/*
   if (Z_in == 2)
   {
      Runed_1_3 = ((GLCONFIG.NumRun==1) || (Time_GetNumrun()==2));
     if ((counttime > 7) && (counttime < 20)&&(bT1))
       {bT1=0; bT2=1; if (!Runed_1_3) ToCalc();}   // �� ������������ � �� 3-� �����
     if ((counttime > 23)&&(bT2)) {bT1=1; bT2=0; ToCalc();}
   }
*/
   if (iBMIX < NCC)
   {
     goto B1;
//        BMIX_B();
   }
//   else
//   {
    RK3P0[nRnZc]  = pow((RK5P0 + RK2P5*RK2P5),0.60) ;
    UU[nRnZc]    = pow((U5P0 + U2P5*U2P5),0.20) ;
    Q2P0[nRnZc]  =Q1P0*Q1P0 ;
//printf("1-BI=");
    for ( i=0; i<18; i++)
    {
     BI[nRnZc][i]=BI[nRnZc][i]*A[i];
//printf(" %le ",BI[nRnZc][i]);
    }
//printf("Rn=%d RK3P0=%le, UU=%le,Q2P0=%le WW=%le HH=%le\n",nRnZc, RK3P0[nRnZc], UU[nRnZc],Q2P0[nRnZc],WW[nRnZc],HH[nRnZc] );
//printf("Call DZ s=%d c=%d iBMIX=%d\n",BINTIME.seconds, counttime,iBMIX);
     T_st = (GLCONFIG.EDIZM_T.T_SU==0) ? 293.15 : (GLCONFIG.EDIZM_T.T_SU==1) ? 273.15 : 288.15;
     T = T_st;
     P=P_st;
 //  }
//printf("RK3P0=%e,UU=%e,Q2P0=%e\n",RK3P0,UU,Q2P0);
}
//------------------------------------
int TaskCalc_AGA8()
{
//   struct IdRun    *prun;               // ��������� �� ��p����p� �����
//   int nRun;
struct   calcdata* uk;
    float P1;
   int sch;
   struct IdRun    *prun;               // 㪠��⥫� �� ��p����p� ��⪨

   nRnZc = Time_GetNumrun();
   uk = &CALCDATA[nRnZc];
   prun  = &CONFIG_IDRUN[nRnZc]; 
   
//printf("Calc_AGA8 Z_Out=%d\n",Z_Out[nRnZc]);
   sch = ((prun->TypeRun == 4) || (prun->TypeRun == 5)
         || (prun->TypeRun == 10));

   P1 = uk->Pa*0.0980665;              // �⥭�� ⥪�饣� ����p� ��⪨
//     if  (Z_Out[nRnZc] != 1) // (AGA_F[nRnZc] == 1)
     {
      P = P1; //uk->Pa*0.0980665; //12.0;
      T = uk->T;  //273.15+6.85; //uk->T;
//      delP= fabs(P - Prn[nRnZc])/P;
//      if (delP < 0.02)
      {
       Dmin=0.000001; Dmax= 40.0;    //0.4;
/*       if (P<0.61)  {Dmin=0.000001; Dmax=0.4;}
       else if (P<1.01)   {Dmin=0.2; Dmax=0.6;}
       else if (P<2.01)   {Dmin=0.2; Dmax=1.3;}
       else if (P<3.01)   {Dmin=0.7; Dmax=2.0;}
       else if (P<4.01)   {Dmin=1.0; Dmax=2.8;}
       else if (P<5.01)   {Dmin=1.4; Dmax=3.7;}
       else if (P<6.01)   {Dmin=1.80; Dmax=4.8;}
       else if (P<7.01)   {Dmin=2.0; Dmax=6.0;}
       else if (P<8.1)   {Dmin=2.2; Dmax=7.6;}
       else if (P<9.1)   {Dmin=3.0; Dmax=9.2;}
       else if (P<10.1)
        { if (T>293.10) {Dmin=3.2; Dmax=5.2;}
          else  {Dmin=4.3; Dmax=10.5;}
        }
       else
          {if (T>293.10) {Dmin=4.0; Dmax=6.5;}
           else
           if (T>248.0) {Dmin=6.1; Dmax=10.2;}
           else   {Dmin=9.8; Dmax=12.8;}
          }
*/
      }
//printf("P=%f T=%f Dmin=%f Dmax=%f\n",P, T, Dmin, Dmax);
//      Trun[nRnZc] = T;
//      Prn[nRnZc] = P;
     }
   Z_in[nRnZc] = 3;
   DZOFPT(nRnZc, P, T);
     if (Z_Out[nRnZc] == 2 || Z_Out[nRnZc] == 4)
     {
// printf("��ன run=%d c=%d\n",nRnZc,counttime);
      if (Z_Out[nRnZc] == 4)
       {// printf("�訡�� ���� ���� ᦨ������� Z_Out=4 Z=%f %d\n",AZ[nRnZc],counttime);
         Z_in[nRnZc] = 0;
        }
/*      { if (sch == 1)
           Task_Calc_CorrQnom();
        else
           TaskCalc_RO();  // ���室 �� ���� ��室�
      }
*/
   return 0;
     }
     else  if ((Z_Out[nRnZc] == 3)&&(Z_in[nRnZc] == 3))
       {  Z_in[nRnZc] = 0;
          return 0;
/*         if (sch == 1)
           Task_Calc_CorrQnom();
        else
           TaskCalc_RO();  // ���室 �� ���� ��室�
*/
       }
     else   // (Z_Out == 1) ��� (Z_Out>4) ���  (Z_Out<=0)
     {// printf("pass 1 Z_Out[%d]=%d  c=%d\n",nRnZc, Z_Out[nRnZc],counttime);
//       if  (Z_Out[nRnZc] != 1)
//       {
         Z_in[nRnZc] = 0;
         return 1;
/*         if (sch == 1)
          Task_Calc_CorrEnd();
         else
          TaskCalcEnd();
        }
*/
/*        else  // if (Z_Out[nRnZc] == 1)
        {// printf("���� run=%d c=%d\n",nRnZc,counttime);
          if (sch == 1)
           Task_Calc_CorrQnom();
          else
           TaskCalc_RO();  // ���室 �� ���� ��室�
        }
*/
     }  //����� else   // (Z_Out == 1) ��� (Z_Out>4)

 return 0;
}
//---------------------------------------------------
void TaskCalc_AGA8_L()
{
   struct IdRun    *prun;               // ��������� �� ��p����p� �����
   int k,Rn,n;
   struct calcdata *uk;
//   int sch;
   double Ks;
//   char far *pcXI,*pcZc;

// printf("Z_in=%d  CalcBR[%d]=%d BR=%d\n",Z_in, nRnZc, CalcBR[nRnz],BR);
   uk = &CALCDATA[nRnZc];                // �⥭�� ⥪�饣� ����p� ��⪨
   prun  = &CONFIG_IDRUN[nRnZc];       // ⥪�騩 ����p ��⪨
//printf("AGA Run=%d RK3P0=%le, UU=%le,Q2P0,=%le\n",nRnZc, RK3P0[nRnZc], UU[nRnZc],Q2P0[nRnZc]);
//printf("AGA Z_in=%d Zc0=%f Zc1=%f Zc2=%f\n",Z_in,CALCDATA[0].Zc,CALCDATA[1].Zc,CALCDATA[2].Zc);
//   sch = ((prun->TypeRun == 4) || (prun->TypeRun == 5)
//         || (prun->TypeRun == 10));
   Ks = uk->K;
//printf("nRnZc=%d  BR=%d Z_Out=%d Z_in=%d\n",nRnZc,BR,Z_Out[nRnZc],Z_in[nRnZc]);
   if ((Z_in[nRnZc] == 1 || Z_in[nRnZc] == 2)&&(Z_Out[nRnZc] == 2 || Z_Out[nRnZc] == 4))
   {//  outportb(0x1D0,BASE_CFG_PAGE); // �� ������� ��������
   //    pcZc = (char far *)MK_FP(SEGMENT,3018);  //3018 + 36 = 3054
       ROfhp[nRnZc] = DR*MWX;
       HSfhp[nRnZc] = E1*P/(RGAS*T)/AZ[nRnZc]; // �������� ������� �������� ������
//printf("nRnZc = %d  ROfhp = %f HSfhp=%f\n",nRnZc,ROfhp[nRnZc],HSfhp[nRnZc]);
       if (prun->constdata & 0x20)       //Heat ��� �� Pst, Tst
       {  CONFIG_IDRUN[nRnZc].Heat = HSfhp[nRnZc]; // � ��� ��� �������
           prun->HsVV = HSfhp[nRnZc] * KoefEn[GLCONFIG.EDIZM_T.EdIzmE]; // �� ��� �  EdIzmE
           prun->HsVV = prun->HsVV*KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
       }
      if (prun->constdata & 0x10)
      {  prun->ROnom = ROfhp[nRnZc]; // Ro ��� �� Pst, Tst
         prun->RoVV = ROfhp[nRnZc]*KoRo[GLCONFIG.EDIZM_T.T_SU][GLCONFIG.EDIZM_T.T_VVP];
         if (prun->typeDens == 1) // ������� � ������������ ���������
           prun->RoVV /=  RLair[GLCONFIG.EDIZM_T.T_VVP]; // ��������� ������� ��� ����������� �����
      }
      for (k=0; k< GLCONFIG.NumRun; k++)
      if ((k!=nRnZc)&&(NumChrom[nRnZc] == NumChrom[k]) && (CONFIG_IDRUN[k].nKcompress == 3))
      {  CALCDATA[k].Zc = AZ[nRnZc];
         UU[k] = UU[nRnZc]; RK3P0[k] = RK3P0[nRnZc]; Q2P0[k] = Q2P0[nRnZc];
         HH[k] = HH[nRnZc]; WW[k] = WW[nRnZc];
         CONFIG_IDRUN[k].ROnom = prun->ROnom;
         CONFIG_IDRUN[k].Heat =  prun->Heat;
         CONFIG_IDRUN[k].RoVV =  prun->RoVV;
         CONFIG_IDRUN[k].HsVV =  prun->HsVV;
//         memcpy(&BI[k][0],&BI[nRnZc][0],18*4);
         for ( n=0; n<18; n++)
          BI[k][n] =  BI[nRnZc][n];
//printf("k=%d BI13=%le BI13=%le\n",k,BI[k][13],BI[nRnZc][13]);
//printf("Rn=%d RK3P0=%le, UU=%le,Q2P0=%le WW=%le HH=%le\n",Rn, RK3P0[Rn], UU[Rn],Q2P0[Rn],WW[Rn],HH[Rn] );

//        _fmemcpy(pcZc+k*12,&(CALCDATA[k].Zc),4);     //������  � ��� �� ���  Zc
      }
      CALCDATA[nRnZc].Zc = AZ[nRnZc];
//printf("nRnZc=%d Zc=%f\n",nRnZc, uk->Zc);
//       Z_Out[0] = 0; Z_Out[1] = 0; Z_Out[2] = 0;
      if (Z_in[nRnZc] == 2)
      {
       CalcBR[nRnZc] = 0;
      }
      else if (Z_in[nRnZc] ==1)  //(BR == 1)
      {
        Z_in[nRnZc] = 0;

      }
      return;
   }
   else  if ((Z_in[nRnZc] == 3)&&(Z_Out[nRnZc]==2 || Z_Out[nRnZc]==4))         //(CalcBRZ==0) //������ Z ��������
   { float dZ ;
    if ((Z_Out[nRnZc]!=4) && (AZ[nRnZc] > 0.4) && (AZ[nRnZc] < 1.2))
     uk->Z = AZ[nRnZc];
//printf("AZ[%d]=%f Z�=%f Z_Out=%d\n",nRnZc,AZ[nRnZc],uk->Zc,Z_Out[nRnZc]);
//printf("RK3P0=%le UU=%le Q2P0=%le\n",RK3P0[nRnZc],UU[nRnZc],Q2P0[nRnZc]);
     if (uk->Zc > 0.6)
     { Ks = uk->Z / uk->Zc;
       uk->Kgerg = Ks;
       uk->K = Ks;
     }
     else Ks=1;
       ZcKs[nRnZc].K=uk->K;  ZcKs[nRnZc].Z=uk->Z;
//       outportb(0x1D0,BASE_CFG_PAGE);
//       pcZc = (char far *)MK_FP(SEGMENT,3018);  //3018 + 36 = 3054
//      _fmemcpy(pcZc+nRnZc*8,&ZcK[nRnZc],8);     //������  � ��� �� ���  Z,K
//        pcZc = &ZcK1[nRnZc][0];
//        memcpy(pcZc+nRnZc*8,&ZcKs[nRnZc],8);
        ZcK1[nRnZc][0]  =  ZcKs[nRnZc].K;
        ZcK1[nRnZc][1]  =  ZcKs[nRnZc].Z;
//printf("nRnZc=%d P=%le T=%le Z=%f s=%d cz=%d\n",
//        nRnZc,P, T, uk->Z,BINTIME.seconds,counttime);
    return;
   } //else  if (Z_in==3)
//printf("AZ[%d]=%lf\n",nRnZc,AZ[nRnZc]);
//     if  ((AGA_F[nRnZc] >= 2) || (CalcBRZ == 2))    //1-� ��� 2-� ������
 }
//����������� TEMP (T)
//C======================================================================
void TEMP (double T, int run)
{
/*    PURPOSE:
�      Sets up temperature dependent terms.
�
�    DESCRIPTION OF ARGUMENTS:
�    T      - Temperature in kelvins. (Input]
C--------------------------------------------------------------------------
*/

double TR0P5, TR1P5, TR2P0, TR3P0, TR4P0, TR5P0, TR6P0 ;
double TR7P0, TR8P0, TR9P0, TR11P0, TR13P0, TR21P0 ;
double TR22P0, TR23P0, TR; //, T ;

//::.CALCULATE SECOND VIRIAL COEFFICIENT B
 Bf(T,&BMIX[run]); // ����� BMIXk

//::.CALCULATE FN[13] THROUGH FN[58]
//::.NOTE: FN[1]-FN[12] DO NOT CONTRIBUTE TO CSM TERMS
//printf("3 Bmix[%d]=%le Bmix=%le UU=%le T=%e WW=%le\n", run,BMIX[run],BMIX[(run+1)%2],UU[run],T,WW[run]);
TR    = T/UU[run];
TR0P5  =sqrt(TR) ;
TR1P5  = TR*TR0P5;
TR2P0  = TR*TR ;
TR3P0  =TR*TR2P0 ;
TR4P0  =TR*TR3P0 ;
TR5P0  =TR*TR4P0 ;
TR6P0  =TR*TR5P0 ;
TR7P0  =TR*TR6P0 ;
TR8P0  =TR*TR7P0 ;
TR9P0  =TR*TR8P0 ;
TR11P0 =TR6P0*TR5P0;
TR13P0 =TR6P0*TR7P0 ;
TR21P0 =TR9P0*TR9P0*TR3P0;
TR22P0 =TR*TR21P0 ;
TR23P0 =TR*TR22P0 ;

FN[run][12] = A[12]*HH[run]*TR6P0;
FN[run][13] = A[13]/TR2P0 ;
FN[run][14] = A[14]/TR3P0 ;
FN[run][15] = A[15]*Q2P0[run]/TR2P0;
FN[run][16] = A[16]/TR2P0 ;
FN[run][17] = A[17]/TR11P0;
FN[run][18] = A[18]*TR0P5 ;
FN[run][19] = A[19]/TR0P5 ;
FN[run][20] = A[20] ;
FN[run][21] = A[21]/TR4P0;
FN[run][22] = A[22]/TR6P0 ;
FN[run][23] = A[23]/TR21P0;
FN[run][24] = A[24]*WW[run]/TR23P0;
FN[run][25] = A[25]*Q2P0[run]/TR22P0;
FN[run][26] = A[26]*HH[run]*TR ;
FN[run][27] = A[27]*Q2P0[run]*TR0P5;
FN[run][28] = A[28]*WW[run]/TR7P0 ;
FN[run][29] = A[29]*HH[run]*TR ;
FN[run][30] = A[30]/TR6P0 ;
FN[run][31] = A[31]*WW[run]/TR4P0;
FN[run][32] = A[32]*WW[run]/TR ;
FN[run][33] = A[33]*WW[run]/TR9P0;
FN[run][34] = A[34]*HH[run]*TR13P0;
FN[run][35] = A[35]/TR21P0 ;
FN[run][36] = A[36]*Q2P0[run]/TR8P0;
FN[run][37] = A[37]*TR0P5 ;
FN[run][38] = A[38] ;
FN[run][39] = A[39]/TR2P0;
FN[run][40] = A[40]/TR7P0 ;
FN[run][41] = A[41]*Q2P0[run]/TR9P0;
FN[run][42] = A[42]/TR22P0 ;
FN[run][43] = A[43]/TR23P0 ;
FN[run][44] = A[44]/TR ;
FN[run][45] = A[45]/TR9P0;
FN[run][46] = A[46]*Q2P0[run]/TR3P0;
FN[run][47] = A[47]/TR8P0 ;
FN[run][48] = A[48]*Q2P0[run]/TR23P0;
FN[run][49] = A[49]/TR1P5 ;
FN[run][50] = A[50]*WW[run]/TR5P0;
FN[run][51] = A[51]*Q2P0[run]*TR0P5;
FN[run][52] = A[52]/TR4P0 ;
FN[run][53] = A[53]*WW[run]/TR7P0;
FN[run][54] = A[54]/TR3P0 ;
FN[run][55] = A[55]*WW[run] ;
FN[run][56] = A[56]/TR ;
FN[run][57] = A[57]*Q2P0[run];
//for (int n=12; n<58; n++)
//printf("FN=%le\n",FN[n]);
}
//================================================
//������� ZDETAIL(D, T)
//======================================================================
//double ZDETAIL(double D, double T)
void PZOFDT(int run, double AD, float T, double* P, double* Z)
{
//    PURPOSE:
//      This function calculates the compressibility factor from the
//      AGA8 model as a function of density and temperature..

//    DESCRIPTION OF ARGUMENTS:
//      AD    - Molar density in mol/dm^3. (Input)
//      �    - Temperature in kelvins. (Input)
//      ZDETAIL - Compressibility factor. (Output)
//----------------------------------------------------------------------

double D1, D2, D3, D4, D5, D6, D7, D8, D9, EXP1, EXP2, EXP3, EXP4 ;
double F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,ZDET;
    //printf("ZDETAIL nRnZc=%d D=%le T=%e\n",nRnZc,AD,T);
//printf("ZD nRnZc=%d T=%f TOLD=%f\n",nRnZc, T, TOLD[nRnZc]);
//   if (T != TOLD[run])  TEMP(T,run);// if (T==280.0) pZ=1;}
//   TOLD[run] = T;
   D1 = RK3P0[run]*AD ;
   D2 = D1*D1 ;
   D3 = D2*D1 ;
   D4 = D3*D1 ;
   D5 = D4*D1 ;
   D6 = D5*D1 ;
   D7 = D6*D1 ;
   D8 = D7*D1 ;
   D9 = D8*D1 ;

  EXP1 = exp(-D1) ;
  EXP2 = exp(-D2) ;
  EXP3 = exp(-D3) ;
  EXP4 = exp(-D4) ;
  F1 = 1.0 + BMIX[run]*AD + FN[run][12]*D1*(EXP3-1.0-3.0*D3*EXP3);
  F2 = (FN[run][13]  + FN[run][14] + FN[run][15])*D1*(EXP2- 1.0 - 2.0*D2*EXP2);
  F3 = (FN[run][16] + FN[run][17])*D1*(EXP4 - 1.0 - 4.0*D4*EXP4);
  F4 = (FN[run][18]+ FN[run][19])*D2*2.0;
  F5 = (FN[run][20] + FN[run][21] + FN[run][22])*D2*(2.0 - 2.0*D2)*EXP2;
  F6 = (FN[run][23] + FN[run][24] + FN[run][25])*D2*(2.0 - 4.0*D4)*EXP4;
  F7 =  FN[run][26]*D2*(2.0 - 4.0*D4)*EXP4 + FN[run][27]*D3*3.0;
  F8 = (FN[run][28] + FN[run][29])*D3*(3.0 - D1)*EXP1;
  F9 = (FN[run][30] + FN[run][31])*D3*(3.0 - 2.0*D2)*EXP2;
  F10 = (FN[run][32] + FN[run][33])*D3*(3.0 - 3.0*D3)*EXP3;
  ZDET = F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9 + F10;
  F1 = (FN[run][34] + FN[run][35]+ FN[run][36])*D3*(3.0 - 4.0*D4)*EXP4;
  F2 = (FN[run][37] + FN[run][38])*D4*4.0;
  F3 = (FN[run][39] + FN[run][40] + FN[run][41])*D4*(4.0 - 2.0*D2)*EXP2;
  F4 = (FN[run][42] + FN[run][43])*D4*(4.0 - 4.0*D4)*EXP4 + FN[run][44]*D5*5.0;
  F5 = (FN[run][45] + FN[run][46])*D5*(5.0 - 2.0*D2)*EXP2;
  F6 = (FN[run][47] + FN[run][48])*D5*(5.0 - 4.0*D4)*EXP4;
  F7 = FN[run][49]*D6*6.0 + FN[run][50]*D6*(6.0-2.0*D2)*EXP2;
  F8 = FN[run][51]*D7*7.0 + FN[run][52]*D7*(7.0 - 2.0*D2)*EXP2;
  F9 = FN[run][53]*D8*(8.0 - D1)*EXP1;
  F10 = (FN[run][54] + FN[run][55])*D8*(8.0 - 2.0*D2)*EXP2;
  ZDET = ZDET + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9 + F10;
  F1 = (FN[run][56] + FN[run][57])*D9*(9.0 - 2.0*D2)*EXP2;
  ZDET = ZDET + F1;
  
/*  double ZDET = 1.0 + BMIX[run]*AD + FN[run][12]*D1*(EXP3-1.0-3.0*D3*EXP3)
  +   (FN[run][13]  + FN[run][14] + FN[run][15])*D1*(EXP2- 1.0 - 2.0*D2*EXP2)
  +  (FN[run][16] + FN[run][17])*D1*(EXP4 - 1.0 - 4.0*D4*EXP4)
  +  (FN[run][18]+ FN[run][19])*D2*2.0
  +  (FN[run][20] + FN[run][21] + FN[run][22])*D2*(2.0 - 2.0*D2)*EXP2  
  +  (FN[run][23] + FN[run][24] + FN[run][25])*D2*(2.0 - 4.0*D4)*EXP4 
  +   FN[run][26]*D2*(2.0 - 4.0*D4)*EXP4
  +   FN[run][27]*D3*3.0
  +  (FN[run][28] + FN[run][29])*D3*(3.0 - D1)*EXP1
  +  (FN[run][30] + FN[run][31])*D3*(3.0 - 2.0*D2)*EXP2
  +  (FN[run][32] + FN[run][33])*D3*(3.0 - 3.0*D3)*EXP3
  +  (FN[run][34] + FN[run][35]+ FN[run][36])*D3*(3.0 - 4.0*D4)*EXP4
  +  (FN[run][37] + FN[run][38])*D4*4.0
  +  (FN[run][39] + FN[run][40] + FN[run][41])*D4*(4.0 - 2.0*D2)*EXP2
  +  (FN[run][42] + FN[run][43])*D4*(4.0 - 4.0*D4)*EXP4
  +   FN[run][44]*D5*5.0
  +  (FN[run][45] + FN[run][46])*D5*(5.0 - 2.0*D2)*EXP2
  +  (FN[run][47] + FN[run][48])*D5*(5.0 - 4.0*D4)*EXP4
  +   FN[run][49]*D6*6.0
  +   FN[run][50]*D6*(6.0-2.0*D2)*EXP2
  +   FN[run][51]*D7*7.0
  +   FN[run][52]*D7*(7.0 - 2.0*D2)*EXP2
  +   FN[run][53]*D8*(8.0 - D1)*EXP1
  +  (FN[run][54] + FN[run][55])*D8*(8.0 - 2.0*D2)*EXP2
  +  (FN[run][56] + FN[run][57])*D9*(9.0 - 2.0*D2)*EXP2;
  */
  *Z = ZDET;
  *P =  ZDET * AD * RGAS * T;
  //printf("nRnZc=%d Z = %le D=%le P=%le\n",nRnZc, ZDET,AD,*P);

// return ZDET;
}
//=================================================================
void DZOFPT(int Run, float P,float T)   //, double* Z, double* BMIX)
{ double Fp1;
  double Z1;

//printf("DZ run=%d  Z_in=%d Z_Out=%d s=%d c=%d\n",Run,Z_in,Z_Out[Run], BINTIME.seconds, counttime);
   if  (Z_Out[Run] != 1)  //(AGA_F[Run] == 1)
   {// printf("P=%f T=%f  Z_in=%d\n",P,T,Z_in[Run]);
     TOL = 0.5e-7;
     if ((Z_in[Run]==1) || (Z_in[Run]==2))
     {
      AX1[Run] = 0.000001;   //�������� ��������� ���� ������
      AX2[Run] = 40.0;    //�������� ��������� ���� ������
     }
     else
     {
      AX1[Run] = Dmin;//0.000001;   //�������� ��������� ���� ������
      AX2[Run] = Dmax;//40.0;    //�������� ��������� ���� ������
     }
// printf("DZ Run=%d P=%lf T=%lf AX1=%le AX2=%le\n",Run ,P,T,AX1[Run],AX2[Run]);
   if (T != TOLD[Run])  TEMP(T,Run);// if (T==280.0) pZ=1;}
   TOLD[Run] = T;
      DR = 0;
      PZOFDT (Run,AX1[Run], T, &Fp1, &Z1);
//printf("F1=%lf P=%f",Fp1,P);
      F1[Run] = Fp1 - P;
//printf("F1=%lf\n",F1);
      PZOFDT (Run,AX2[Run], T, &F2[Run], &Z1);
//printf("F2=%le\n T=%f",F2[nRnZc],T);
      F2[Run] = F2[Run] - P;
//printf("F2=%le\n",F2);
   }
//   if (AGA_F[Run] == 1)
//printf("AX1[%d]=%le AX2=%le\n",Run,AX1[Run],AX2[Run]);
/*   if (Z_Out[Run] != 1)
    iAGA = 0;
   else
    iAGA = 2;
*/
    iAGA = 0;
   nRnZc = Run;
     AGA8_1(); //(nRnZc,P, T);
//    if ((Z_Out[nRnZc] == 2) || (Z_Out[nRnZc] == 3) || (Z_Out[nRnZc] == 4))
//      TaskCalc_AGA8_L();
}
//=================================================================

void AGA8_1()  //(int nRnZc, double P,double T)   //, double* Z, double* BMIX)
{//  int i;
   double Z1;
A1:
   if  (F1[nRnZc]*F2[nRnZc] >= 0)
   {
//printf("*** nRnZc=%d F1=%le F2=%le\n",nRnZc,F1[nRnZc],F2[nRnZc]);
     Z_Out[nRnZc] = 3;
     TaskCalc_AGA8_L();
     return;
   }
// ----------------------------------------------------------------
// BEGIN ITERATING ������ ��������
// ----------------------------------------------------------------
//   for (i=0; i<50; i++)
// ...Use False Position to get point 3.
//   {
	 A2:     AX3[nRnZc] = AX1[nRnZc] - F1[nRnZc]*(AX2[nRnZc] - AX1[nRnZc])/(F2[nRnZc] - F1[nRnZc]);  //AX3-�������� ���������, F1,F2-��������
     PZOFDT(nRnZc,AX3[nRnZc], T, &F3[nRnZc], &Z1); //������ �������� F3
//printf("F3=%le AX3=%le\n",F3[nRnZc],AX3[nRnZc]);
     F3[nRnZc] = F3[nRnZc] - P;
//printf("F3-P=%le\n",F3);

// ...Use points 1, 2, and 3 to estimate the root using Chamber's
// ...method (quadratic solution).
//  ������ �������� ���������  DR
     DR = AX1[nRnZc]*F2[nRnZc]*F3[nRnZc]/((F1[nRnZc] - F2[nRnZc])*(F1[nRnZc] - F3[nRnZc])) + AX2[nRnZc]*F1[nRnZc]*F3[nRnZc]/((F2[nRnZc] - F1[nRnZc])*(F2[nRnZc] - F3[nRnZc]))
        + AX3[nRnZc]*F1[nRnZc]*F2[nRnZc]/((F3[nRnZc] - F1[nRnZc])*(F3[nRnZc] - F2[nRnZc]));
     if ((DR - AX1[nRnZc])*(DR - AX2[nRnZc])>=0)
        DR = (AX1[nRnZc] + AX2[nRnZc])/2.0;
     DRo[nRnZc] = DR;
     PZOFDT(nRnZc,DR, T, &FL[nRnZc], &Z1); //������ �������� F
//printf("FL[%d]=%le DR=%le iAGA=%d\n",nRnZc,FL[nRnZc],DR,iAGA);

     FL[nRnZc] = FL[nRnZc] - P;
//printf("i=%d DR=%lf FL=%le Z=%lf\n",i,DR,FL,Z1);
     if (fabs(FL[nRnZc]) <= TOL)     // ��������� �������
     { AZ[nRnZc] = Z1;
       Z_Out[nRnZc] = 2;
//       printf("Z_Out[%d]=%d iAGA=%d Z=%lf DR=%le\n",nRnZc,Z_Out[nRnZc],iAGA,Z1,DR);
       ROfhp[nRnZc] = DR*MWX;

// printf("iAGA=%d Z=%lf DR=%le RO=%6.4lf\n",iAGA,Z1,DR,ROa[nRnZc]);
         TaskCalc_AGA8_L();
//printf("OUT1 nRnZc=%d s=%d c4=%d  Z=%le\n",nRnZc,BINTIME.seconds,counttime,Z1);
       return ;
     } //�������� ����������� �������, ��������� = DR
// ...Discard quadratic solution if false position root is closer.
//  �������� ������������, ������ ����� �������� X1, X2 � ����� ��������
     if ((fabs(F3[nRnZc]) < fabs(FL[nRnZc])) && (FL[nRnZc]*F3[nRnZc] > 0))
     { if (F3[nRnZc]*F1[nRnZc] > 0)
       { AX1[nRnZc] = AX3[nRnZc]; F1[nRnZc] = F3[nRnZc];
//printf("F3<FL F3*F1 > 0\n");
       }
       else
       { AX2[nRnZc] = AX3[nRnZc]; F2[nRnZc] = F3[nRnZc];
//printf("F3<FL F3*F1 <= 0\n");
       }
     }  // ����� if ((fabs(F3[nRnZc]) < fabs(FL[nRnZc]))
     else
// ... Swap in new value from quadratic solution
     {
       if (FL[nRnZc]*F3[nRnZc] < 0)
        { AX1[nRnZc] = DR; F1[nRnZc] = FL[nRnZc];
          AX2[nRnZc] = AX3[nRnZc];
          F2[nRnZc] = F3[nRnZc];
//printf("F3>=FL FL*F3 < 0\n");
        }
        else
        { if (F3[nRnZc]*F1[nRnZc] > 0)
          {
//printf("F3>=FL F3*F1 > 0\n");
           AX1[nRnZc] = DR;  F1[nRnZc] = FL[nRnZc];
          }
          else
          {
//printf("F3>=FL F3*F1 <= 0\n");
            AX2[nRnZc] = DR;
            F2[nRnZc] = FL[nRnZc];
          }
        }
     }   // ����� else if ((fabs(F3[nRnZc]) < fabs(FL[nRnZc]))
//   }   // ����� ����� for (i=0; i<50;

   iAGA++;

           if  (iAGA<10)
           {  goto A2;
//              AGA8_1();
//              printf("2-nRnZc=%d iAGA=%d AGA_F=%d s=%d c=%d\n", nRnZc,iAGA,AGA_F[nRnZc],BINTIME.seconds,counttime);
           }
           else      // �� �������� ��������, ����� � ��� ����
           {// printf("out2-nRnZc=%d Z1=%le c=%d iAGA=%d\n", nRnZc,Z1,counttime,iAGA);
            if ((Z1<1.1)&&(Z1>0.4)) AZ[nRnZc] = Z1;
             Z_Out[nRnZc] = 4;
             if (fabs(FL[nRnZc]) < 5e-4) Z_Out[nRnZc] = 2;
//printf("Z_Out=4 FL=%e iAGA=%d Z=%e c=%d\n",fabs(FL[nRnZc]),iAGA,Z1,counttime);
               TaskCalc_AGA8_L();
           }
}
//=================================================================
