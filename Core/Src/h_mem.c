////////////////////////////////////////////////////////////////
// �㭪樨 ����㯠 � ��娢�� ���᫨⥫�.
///////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
//#include <mem.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "comm_str.h"
#include "h_time.h"
#include "h_main.h"
#include "main.h"
#include "h_calc.h"
#include "h_analog.h"
#include "h_mem.h"
#include "h_disp.h"

#include "h_comm_2.h"
//#include "h_valarm.h"
#include "h_hart.h"
#include "h_smooth.h"
#include "h_persist.h"
//#include "h_msd.h"
//-------------------------------------------------------------------------------
int GetFullVolFlag();                   // �ਧ��� ������� ��ꥬ� � ��娢��
int Time_GetCalcNumRun();               // �⥭�� ⥪�饣� ����p� ��⪨ - �����誠
                                        // ���稪 ���਩
//extern struct valarm_count AllAlarmCount[];
                                        // ���稪 ���権 Qmin
//extern struct min_count MinAlarmCount[];
extern unsigned int  counttime;         // ���-�� ⠪⮢ ��� ���뢠��� �p�����
//extern struct I2C_HandleTypeDef hi2c1;
extern float    OdoDelta,               // ࠧ�����
		OdoPrev,                // �।��饥 ���祭�� ��ꥬ�
                OdoRest;                // ���⮪ �� �।��饣� ����
extern int      OdoImp,                 // �᫮ �����ᮢ � �뤠�
                Fod;                    // ����ﭨ� ����� �����ᠬ�
extern OdoStruc OdoTab[];              // ⠡��� ��� �뤠� �����ᮢ
extern int      OdoWrInd,               // ������ ��� ����� � ⠡����
                OdoRdInd;               // ������ ��� �⥭�� �� ⠡����
extern unsigned char OdoNumRun;         // ��⪠ ���� ���ਧ�樨
extern unsigned char PrevOdoMask;       // ��᪠ ���ਧ�樨 �� �।��饬 ⠪�
extern float Qodo[];                   // ��室� ��� ���ਧ�樨 �� ��⪠�
extern int   iQodo;                     // �᫮ ��⮪ ��� ���� ��ꥬ�
extern unsigned int CalcTakt;
extern double IndQ[],IndDP[];
extern float IndQs[];
extern unsigned char CountUse;          // ⨯ �c���짮����� ���稪�� (MONO,STEREO)
extern struct bintime  BINTIME;         // ����筠� ��p���p� �p�����
extern struct calcdata CALCDATA[];

extern struct bintime      LastTime;
extern struct valarm_data ValarmData;   // ����� �㬬��� ���਩��� ��ꥬ��
//extern struct valarm_data ValarmData_1; // ����� �㬬��� ���਩��� ��ꥬ��
//extern struct valarm_data far *ValarmSumm;     // �㬬���� ���਩��� ��ꥬ��
//extern struct valarm_data far *ValarmSumm2;    // ��.�㬬���� ���਩��� ��ꥬ��

extern unsigned char Chan1ErrFlag,      // �ਧ���� �訡�� �� �������
                     Chan2ErrFlag;
extern float Kday;                      // �����樥�� ��� ࠧ���� ������ ��ꥬ��
extern float Qadwc[],Qadsc[];
extern double  Vr_h[],Vr_d[];
struct bintime HARTTimeb1,HARTTimeb;
extern char startT;
int kSDm[3],kSDp[3];
/*--------------- ��砫� ���ᠭ�� ��p���p� � �㭪樨 0x56 ----------------*/
//extern GornalPassw LogPassw[6];   //6*15=90 ���� c 4096 ���� ���. 81
/*--------------- ��砫� ���ᠭ�� ��p���p� � �㭪樨 0x57 ----------------*/
//extern GornalDostup ArhivDostup[10]; //10*17=170*6=1020 ���� c 4246 ���� ���. 81
//GornalDostup1 ArhivDostup1;
extern struct bintime logTimeb,logTimeb1;
extern unsigned char Tlogin[5],Tlogin1[5];
//extern struct BlockMgn BLOKMGN[];
//extern struct BlockPer BLOKPER[];
//extern char Bnew[],BnewP[]; //䫠� ��砫� ������ �����
//extern struct Msearch SearchMgn[3][140],SearchPer[3][140];
extern int KSD;
extern unsigned int errMB[],Al112[];
extern char str[60];
extern int bSDOff,bSDOff_P;
extern struct bintime OpTimeB;
extern unsigned long int StartTim;
//extern char Calc59Mgn; //,utr[];
extern double  Vwsr[];
extern char NoWriteCycle[];
extern struct glconfig     GLCONFIG;           // ������� ���䨣����
extern struct IdRun CONFIG_IDRUN[3]; // ���䨣���� �� ��⪠�
extern struct AddCfg       ADCONFIG;           // �������⥫쭠� ���䨣����
extern struct AdRun CONFIG_ADRUN[3];               //120*3=360
extern uint8_t AdrIdRun[3][2];
extern struct IdRun  *IdRn;
extern uint16_t SizeCfg, SizeIdR,SizeIntPar;
extern uint8_t AdrCfg[];
extern  uint16_t Zap_in_Block;
extern  uint16_t SectorSize;//=4096;   
extern SPI_HandleTypeDef hspi3;
extern uint8_t AdrSumm[3][3][2]; //= {0xD,0x16,0xD,0xF4,0xE,0xD2,0xf,0xB0,0x10,0x8E,
//  0x11,0x6C,0x12,0x4A,0x13,0x28,0x14,0x06};//�� (0x14,0xE4) //222*3*3=1998=7CE ����   222=DE
extern _Bool b_Massomer[3],b_Counter[3];
extern unsigned int         membuferrrun0[20]; // �訡�� �� �p����饬 ⠪� p�����
extern unsigned int         membuferrrun1[20]; // �訡�� �� �p����饬 ⠪� p�����
extern unsigned int         membuferrrun2[20]; // �訡�� �� �p����饬 ⠪� p�����
//extern unsigned int coderred[];
//extern unsigned int coderrgr[];
//extern unsigned char QstErrFlag[3];
extern  RTC_TimeTypeDef sTime;
extern  UART_HandleTypeDef huart2;
//---------------------------------------------------------------------------
char Bq,Bterm;
struct mon_summ MonthSumm[3];           // ������ �㬬����
//char far *MS;
struct bintime      POWER_OFF_TIME;     // �६� �몫�祭��

struct intpar       INTPAR;             // ����७��� ��ࠬ����
struct memdata      MEMDATA;            // ��p���p� ����� ���� - 26 ����
//struct index        INDEX;
struct startendtime STARTENDTIME;       // ��p���p� ��� ���᪠ ����ᥩ
struct startendtime STARTENDTIME_1;     // ��p���p� ��� ���᪠ ����ᥩ
struct tdata        TDATA;              // ��� �㬬�p������ ������ ��ਮ���᪨�
struct tdata        HDATA;              // ��� �㬬�p������ ������ �ᮢ��
struct memaudit     MEMAUDIT;           // ��p���p� ����� �p娢� ����⥫���
struct memalarm     MEMALARM;           // ��p���p� ����� �p娢� ���p��
struct memalarmC     MEMALARMC;           // ��p���p� ����� �p娢� ���p��
struct memsecurit     MEMSECUR;           // ��p���p� ����� �p娢� ���p��
struct memdata ZapAs;

unsigned int coderred[]={0xA7,0x27,0xA8,0x28,0xA9,0x29,0xAA,0x2A,0x8D,0x0D};
unsigned int coderrgr[]= {0xAB,0x2B,0xAC,0x2C,0xAD,0x2D,0xAE,0x2E,\
                          0xAF,0x2F,0xB0,0x30,0xB1,0x31,0xB2,0x32,\
                          0xB3,0x33,0xB4,0x34,0xB5,0x35};
unsigned char QstErrFlag[3] = {0,0,0};
unsigned char CurrHour,                 // ⥪�饥 ���祭�� ��
              PrevHour;                 // �।��饥 ���祭�� ��
float Vb[KOL_RUN];
struct glconfig RES_GLCONFIG;           // १�ࢭ�� � ���
float V1,V2;
unsigned char fV1=0,fV2=0;
unsigned int di1,di2,di1p[2][4],di2p[2][4];
int NzBlk[3],NzBlkp[3]; //⥪�騩 ����� �����
long Nb1;
int runSDm,runSDp;
int WrMgnAl[3],WrPerAl[3], MgnAl[3];
int NoWriteMgn;
int bWrMgn;
float Vru1[3],Vs1[3]; //�।��騥 ���祭�� Vr1-ࠡ�祣� ��ꥬ�, Vs1- ��ꥬ� � ��
char MgnAlarm;
extern struct pointARch pArchiv;
//-------------------------------------------------------
void Ini_Int_Com_1();    //��⠭���� ����p� �p�p뢠��� �� com2-IRQ3
void Ini_Reciv_1();       //���樠������ �p����
void IniCfgSpeed_1();
void Port_Ini_1();
long BlokTime(int, long); // ���� � ��� �६���� tm
void PrintForm0();
extern char  StatusForm[];
extern  struct printform  PRINTFORM;            // ��� �p������� �뢮��� �� �������p

void WriteValarmData(int Page,int nRun);
void ValarmSummIni(struct bintime BinT, int nRun);
//unsigned int Crc16(unsigned char* buf, int iSize);
double Persist_GetQwCountTotal(int);
void Persist_AddQwCountTotal(int iRun, double QwCountTotal);
void P23K256_Write_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
uint8_t ReadDataW25_IT(uint16_t TypeArch, uint8_t Run, uint32_t NumRec,uint8_t *rdData);
void P23K256_Read_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *rxData, uint16_t RxSize);
//uint8_t T24_I2C_256_Write_Data(struct I2C_HandleTypeDef *hi2c, uint16_t MemAddress,  uint8_t *txData, uint16_t TxSize); 
//-------------------------------------------------------------------------------

void ClrArc(unsigned char empRun) {
  if (empRun & 0x20)
 {                                        // 1 ��⪠
 }
  if (empRun & 0x40)
 {                                        // 2 ��⪠
 }
  if (empRun & 0x80)
 {                                        // 3 ��⪠
 }
}
//---------------------------------------------------------------------------
void Swap6(uint8_t *M1);
//------------------ ����� ��������� ������ �� ������� struct bintime
 // di1=pArchiv.di1s[nRun]; di2=pArchiv.di2s[nRun];
 uint16_t FindDevide2(uint16_t TypeArch, uint8_t Run, uint16_t di1,uint16_t di2, unsigned char  *FINDTime, uint16_t *din2)
{ uint16_t in1,in2, fin, dpoint;
  unsigned char  pTime[6];
  uint8_t  MemDat[64];//  KolZapArc[TypeArch]];
    
    in1 = di1; in2 = di2;
    dpoint = (in1 <= in2) ? (in2-in1+1) : KolZapArcAll[TypeArch] - in1 + in2 +1;
    while (dpoint > 10)
    { 
       fin = dpoint / 2 + in1;
       if (fin >= KolZapArcAll[TypeArch]) fin -= KolZapArcAll[TypeArch]; 
       ReadDataW25_IT(TypeArch, Run, fin ,MemDat); 
        Swap6(MemDat);
        if (memcmp(FINDTime,MemDat,6) < 0) 
            in2 = fin;  //FINDTime < MemDat
        else in1 = fin;//FINDTime => MemDat
       dpoint = (in1 <= in2) ? (in2-in1+1) : KolZapArcAll[TypeArch] - in1 + in2 +1;
    }
    *din2 = in2; 
    return in1;
}
//----------------
void Swap6(uint8_t *M1)
{ uint8_t C,m;
    
    for (m=0; m<3; m++)
    {
     C = *(M1+ m);
     *(M1+ m) = *(M1+5-m);   
     *(M1+ 5-m) = C;
    }    
}

//----------------------------------------------------------------
uint16_t Mem_TestReqTime(struct bintime *pbtime)   //����p��� �p����� ���p���
{ int err = 0;
  if((pbtime->month < 1) || (pbtime->month > 12)) err |= 1;   //�����
  if((pbtime->date < 1)  || (pbtime->date > 31))  err |= 2;   //����
  if( pbtime->year > 99)                          err |= 4;   //���
  if( pbtime->hours > 23)                         err |= 8;   //���
  if( pbtime->minutes > 59)                       err |= 0x10;//������
  if( pbtime->seconds > 59)                       err |= 0x20;//�������
  return err;
}
//---------------------------------------------------------------------------
uint16_t  Mem_TestReqTimeSEp(struct startendtime *STARTENDTIM)//����p��� ����������� �p���� ���p���
{unsigned char *s1,*s2;
unsigned char st[6],en[6],i;
// _ES = _DS;

s1 = (unsigned char *)&(STARTENDTIM->STARTTIME); 
s2 = (unsigned char *)&(STARTENDTIM->ENDTIME); 
for (i=0; i<6; i++)
{  st[5-i] = *(s1+i);
   en[5-i] = *(s2+i);
}
 if (memcmp(st,en,6) <=0)  return 0; else return 1;
}

//-------------------------------------------------------
uint16_t Mem_TestReqTimeEqp(struct startendtime *STARTENDTIM)      // ����p��� ����������� �p���� ���p��� �� p��������
{unsigned char *s1,*s2;
unsigned char st[6],en[6],i;

s1 = (unsigned char *)&(STARTENDTIM->STARTTIME); 
s2 = (unsigned char *)&(STARTENDTIM->ENDTIME); 
for (i=0; i<6; i++)
{  st[5-i] = *(s1+i);
   en[5-i] = *(s2+i);
}
 if (memcmp(st,en,6) ==0)  return 0;
 else return 1; 
}
//------------------------------------------------------
uint16_t  Mem_TestReqTimeEndp(struct startendtime *STARTENDTIM,uint8_t *s1)  //����p��� ����������� �p���� � �p���� � ���p���
{uint8_t *s2;
unsigned char st[6],en[6],i;
    
s2 = (uint8_t *)&(STARTENDTIM->ENDTIME);
for (i=0; i<6; i++)
{  st[5-i] = *(s1+i);
   en[5-i] = *(s2+i);
}
for (i=1; i<6; i++)
if ((st[i] > 60) || (st[1]==0) || (st[2]==0))  { return 1; }
for (i=1; i<6; i++)
if ((en[i] > 60) || (en[1]==0) || (en[2]==0)) { return 0; }

//�p��� � ������ �p���� <= �������� �p��� � ���p���
 if (memcmp(st,en,6) <=0)  return 0;
 else return 1; 
}
//------------------------------------------------------
uint16_t  Mem_TestReqTimeStart(struct startendtime *STARTENDTIM,uint8_t* s1)  //����p��� ����������� �p���� � �p���� � ���p���
{ uint8_t *s2;
  uint8_t st[6],en[6],i;
//  uint8_t b1,b2;
    
//s1 = (uint8_t *)&MEMDATA.STARTTIME; 
s2 = (uint8_t *)&(STARTENDTIM->STARTTIME); 
for (i=0; i<6; i++)
{  st[5-i] = *(s1+i);
   en[5-i] = *(s2+i);
}
//b1=0; b2=0;
for (i=1; i<6; i++)
if ((st[i] > 60) || (st[1]==0) || (st[2]==0))  { return 0; }
for (i=1; i<6; i++)
if ((en[i] > 60) || (en[1]==0) || (en[2]==0)) {  return 1; }
 //�p��� � ��p��� ������ �p���� > ��������� �p��� � ���p���
 if (memcmp(st,en,6) <= 0) return 0;
 else return 1; 
}
//---------------------------------------------------------------------------
/*
uint16_t Mem_TestReqTimeBegp(struct startendtime *STARTENDTIM)  //����p��� ����������� �p���� � �p���� � ���p���
{unsigned char *s1,*s2;
unsigned char st[6],en[6],i;
// uint8_t b1,b2;
    
s1 = (unsigned char *)&MEMDATA.STARTTIME; //.year);
s2 = (unsigned char *)&(STARTENDTIM->STARTTIME); //.year);
for (i=0; i<6; i++)
{  st[5-i] = *(s1+i);
   en[5-i] = *(s2+i);
}
//b1=0; b2=0;
for (i=1; i<6; i++)
if ((st[i] > 60) || (st[1]==0) || (st[2]==0))  { return 0; }
for (i=1; i<6; i++)
if ((en[i] > 60) || (en[1]==0) || (en[2]==0)) { return 1; }

 //�p��� � ��p��� ������ �p���� <= �������� �p��� � ���p���
 if (memcmp(st,en,6) >=0)  return 0;
 else return 1; 
}
*/
//---------------------------------------------------------------------------
uint16_t Mem_NextIndex(uint8_t TypeArch, uint16_t start, uint16_t index, uint16_t startindex, uint16_t endindex )
{ 
    if (startindex > endindex) {         // ����p �������� ���������
      start += index;
      if (start > (KolZapArcAll[TypeArch]-1))
         start -= KolZapArcAll[TypeArch];
      return start;
   }
   else {                               // ����p �������� �������� ���
      start += index;                   // ��p���� ���������� ��p�� �p�����
      if (start <= endindex)
         return start;
      else
         return 0xFFFF; //65535;
   }
}
//---------------------------------------------------------------------
uint8_t WriteCycleData(uint8_t run, struct memdata ZapAs)
{ uint16_t di2; 
    
//    sprintf(str,"WCD Run=%d NumRec=%d\n\r",run, pArchiv.di2s[run]);
//    uint16_t Lstr = strlen(str);
//    HAL_UART_Transmit(&huart2,(uint8_t*)str,Lstr,10);  //_IT
 
    Persist_AddQwCountTotal(run, (double)(IndQ[run]/3600.0));
       if (CONFIG_IDRUN[run].TypeRun == 10)
       {  ZapAs.Vru_total = CONFIG_ADRUN[run].Vtot_l;
       }   
       else
       {  ZapAs.Vru_total = Persist_GetQwCountTotal(run);          
       }    
       MEMDATA.Vru_total = ZapAs.Vru_total;

       if (pArchiv.di2s[run] > (KolZapArcAll[SEC]-1)) 
             { pArchiv.di2s[run] = 0; pArchiv.di1s[run] = KolZapArcAll[SEC]-1;} 
       Zap_in_Block = SectorSize / RecSizeW[SEC];
       if ((pArchiv.di2s[run] == 0)  && (pArchiv.di1s[run] == (KolZapArcAll[SEC]-1))) 
       {    pArchiv.di1s[run]=0;          
// ������ � �����  �� ��������� 1-�� ����� 
           di2=pArchiv.di2s[run];
         if (WriteDataW25_IT(SEC, run, di2,(uint8_t*)&ZapAs))
             return 1;
       } else 
        { if (pArchiv.di2s[run] >= pArchiv.di1s[run]) // ������ ������
         {  
            if (pArchiv.di2s[run] < (KolZapArcAll[SEC]-1))
            { pArchiv.di2s[run]++;  
              if (pArchiv.di2s[run] >= (KolZapArcAll[SEC]- Zap_in_Block))
              { pArchiv.di1s[run]++; }
            } else {pArchiv.di2s[run] = 0; pArchiv.di1s[run]=Zap_in_Block;}
            di2=pArchiv.di2s[run];
            if (WriteDataW25_IT(SEC, run, di2,(uint8_t*)&ZapAs))
                return 1;
         }
         else  // �� ������ ������  di2 < di1
            { if (pArchiv.di2s[run] < (KolZapArcAll[SEC]-1)) 
              {pArchiv.di2s[run]++; 
               if (pArchiv.di1s[run] < (KolZapArcAll[SEC]-1)) 
                   pArchiv.di1s[run]++; else pArchiv.di1s[run] = 0; //Zap_in_Block;
              }   
              else {pArchiv.di2s[run] = 0; pArchiv.di1s[run] = Zap_in_Block;}         
              di2=pArchiv.di2s[run];
              if (WriteDataW25_IT(SEC, run, di2,(uint8_t*)&ZapAs))
                  return 1;
            }
            return 0;
        }
 }
//------------------------------------------------------
// ������ ������ ������ PER �� ��������� MEMDATA
 // MemdataWr (PER, nRun, &pArchiv.di1p[nRun], &pArchiv.di2p[nRun], MEMDATA) 
// ���������� ������ HOU, DAY
 uint8_t  MemdataWr (uint8_t TypeArch, uint8_t run, uint16_t* di1, uint16_t* di2,struct memdata Zapis) 
{//uint8_t  nRun = Time_GetNumrun();    

//    sprintf(str,"MWr Run=%d NumRec=%d\n\r",run, *di2);
//    uint16_t Lstr = strlen(str);
//    HAL_UART_Transmit_IT(&huart2,(uint8_t*)str,Lstr);  //*(di2)
          
       if (*di2 > (KolZapArcAll[TypeArch]-1)) 
             { *di2 = 0; *di1 = KolZapArcAll[TypeArch]-1;} 
       Zap_in_Block = SectorSize / RecSizeW[TypeArch];
       if ((*di2 == 0)  && (*di1 == (KolZapArcAll[TypeArch]-1))) 
       {    *di1=0;          
// ������ � �����  �� ��������� 1-�� ����� 
         if (WriteDataW25_IT(TypeArch, run, *di2,(uint8_t*)&Zapis))
             return 1;
       } else 
        { if (*di2 >= *di1) // ������ ������
         {  
            if (*di2 < (KolZapArcAll[TypeArch]-1))
            { (*di2)++;  
              if (*di2 >= (KolZapArcAll[TypeArch]- Zap_in_Block))
              { (*di1)++; }
            } else {*di2 = 0; *di1=Zap_in_Block;}
            
            if (WriteDataW25_IT(TypeArch, run, *di2,(uint8_t*)&Zapis))
                return 1;
         }
         else  // �� ������ ������  di2 < di1
            { if (*di2 < (KolZapArcAll[TypeArch]-1)) 
              {(*di2)++; 
               if (*di1 < (KolZapArcAll[TypeArch]-1)) 
                   (*di1)++; else di1 = 0; //Zap_in_Block;
              }   
              else {*di2 = 0; *di1 = Zap_in_Block;}         
              if (WriteDataW25_IT(TypeArch, run, *di2,(uint8_t*)&Zapis))
                  return 1;
            }
            return 0;
        }
 }   
//--------------------------------------------------------------
 uint8_t Mem_AuditWr(uint8_t run, struct memaudit audit)
 {
  //MEMAUDIT.TIME = BINTIME;  //�������� ��p����p� �p����� �� ��������
     memcpy( &audit.TIME,&BINTIME,6);//�������� ��p����p� �p�����
  audit.checksum = Mem_Crc16((uint8_t*)&audit,sizeof(struct memaudit)-2);//����p������ �����
       if (pArchiv.di2Au[run] > (KolZapArcAll[AUD]-1)) 
             { pArchiv.di2Au[run] = 0; pArchiv.di1Au[run] = KolZapArcAll[AUD]-1;} 
       Zap_in_Block = SectorSize / RecSizeW[AUD];
       if ((pArchiv.di2Au[run] == 0)  && (pArchiv.di1Au[run] == (KolZapArcAll[AUD]-1))) 
       {    pArchiv.di1Au[run]=0;          
// ������ � �����  �� ��������� 1-�� ����� 
           if (WriteDataW25_IT(AUD, run, pArchiv.di2Au[run],(uint8_t*)&audit))
               return 1;
       } else 
        { if (pArchiv.di2Au[run] >= pArchiv.di1Au[run]) // ������ ������
         {  
            if (pArchiv.di2Au[run] < (KolZapArcAll[AUD]-1))
            { pArchiv.di2Au[run]++;  
              if (pArchiv.di2Au[run] >= (KolZapArcAll[AUD]- Zap_in_Block))
              { pArchiv.di1Au[run]++; }
            } else {pArchiv.di2Au[run] = 0; pArchiv.di1Au[run]=Zap_in_Block;}
            
           if ( WriteDataW25_IT(AUD, run, pArchiv.di2Au[run],(uint8_t*)&audit))
               return 1;
         }
         else  // �� ������ ������  di2 < di1
            { if (pArchiv.di2Au[run] < (KolZapArcAll[AUD]-1)) 
              {pArchiv.di2Au[run]++; 
               if (pArchiv.di1Au[run] < (KolZapArcAll[AUD]-1)) 
                   pArchiv.di1Au[run]++; else pArchiv.di1Au[run] = 0; //Zap_in_Block;
              }   
              else {pArchiv.di2Au[run] = 0; pArchiv.di1Au[run] = Zap_in_Block;}         
              if (WriteDataW25_IT(AUD, run, pArchiv.di2Au[run],(uint8_t*)&audit))
                 return 1;
            }
        }
  return 0;
 }     
//--------------------------------------------------------------
 uint8_t Mem_AlarmWr(uint8_t run, struct memalarm alarm)
 {
//  MEMALARM.TIME = BINTIME;  //�������� ��p����p� �p�����
//     memcpy( &alarm.TIME,&BINTIME,6);//�������� ��p����p� �p�����
     alarm.crc = Mem_Crc16((uint8_t*)&alarm,sizeof(struct memalarm)-2);//����p������ �����
       if (pArchiv.di2Al[run] > (KolZapArcAll[ALR]-1)) 
             { pArchiv.di2Al[run] = 0; pArchiv.di1Al[run] = KolZapArcAll[ALR]-1;} 
       Zap_in_Block = SectorSize / RecSizeW[ALR];
       if ((pArchiv.di2Al[run] == 0)  && (pArchiv.di1Al[run] == (KolZapArcAll[ALR]-1))) 
       {    pArchiv.di1Al[run]=0;          
// ������ � �����  �� ��������� 1-�� ����� 
           if (WriteDataW25_IT(ALR, run, pArchiv.di2Al[run],(uint8_t*)&alarm))
               return 1;
       } else 
        { if (pArchiv.di2Al[run] >= pArchiv.di1Al[run]) // ������ ������
         {  
            if (pArchiv.di2Al[run] < (KolZapArcAll[ALR]-1))
            { pArchiv.di2Al[run]++;  
              if (pArchiv.di2Al[run] >= (KolZapArcAll[ALR]- Zap_in_Block))
              { pArchiv.di1Al[run]++; }
            } else {pArchiv.di2Al[run] = 0; pArchiv.di1Al[run]=Zap_in_Block;}
            
           if ( WriteDataW25_IT(ALR, run, pArchiv.di2Al[run],(uint8_t*)&alarm))
               return 1;
         }
         else  // �� ������ ������  di2 < di1
            { if (pArchiv.di2Al[run] < (KolZapArcAll[ALR]-1)) 
              {pArchiv.di2Al[run]++; 
               if (pArchiv.di1Al[run] < (KolZapArcAll[ALR]-1)) 
                   pArchiv.di1Al[run]++; else pArchiv.di1Al[run] = 0; //Zap_in_Block;
              }   
              else {pArchiv.di2Al[run] = 0; pArchiv.di1Al[run] = Zap_in_Block;}         
              if (WriteDataW25_IT(ALR, run, pArchiv.di2Al[run],(uint8_t*)&alarm))
                 return 1;
            }
        }
  return 0;
 }
 //--------------------------------------------------------------
void Mem_ConfigRdChk(uint8_t Cfg) { // ����������� ������
}
uint16_t Mem_ConfigWrChk(uint8_t Cfg) { //������ ����������� �����
}
//--------------------------------------------------------------
 int  Mem_ConfigRunWr(uint8_t run) {         // ������ �������p���� � ���p������������� ������ - 0.81 ��
   uint8_t *pAdr; 
   struct IdRun *IdR;
     
   // ������ CONFIG_IDRUN[] �� ������
      pAdr = &AdrIdRun[run][0];
      IdR = &CONFIG_IDRUN[run];
     // ����p������ �����
      IdR->crc =  Mem_Crc16((uint8_t*)IdR,sizeof(struct IdRun)-2);
     //    ������ ������������ �� ����� num-1      
      P23K256_Write_Data(&hspi3,pAdr,(uint8_t*)&CONFIG_IDRUN[run],SizeIdR); //(uint8_t*)IdR
//     ������ ��������� ������������    
//      T24_I2C_256_Write_Data(&hi2c1, AdrIdRun[run][0]*256+AdrIdRun[run][1],(uint8_t*)&CONFIG_IDRUN[run], SizeIdR);      

return 0; 
 }
 
 int Mem_ConfigWr(void) //������ MainConfig
 { uint8_t Er_I2C;
   GLCONFIG.crc = Mem_Crc16((uint8_t*)&GLCONFIG,SizeCfg-4);
    
   P23K256_Write_Data(&hspi3,AdrCfg,(uint8_t*)&GLCONFIG,SizeCfg-2);   
     //������ ��������� ������������
//   Er_I2C = T24_I2C_256_Write_Data(&hi2c1, AdrCfg[0]*256+AdrCfg[1],  (uint8_t*)&GLCONFIG, SizeCfg-2); 
   return Er_I2C;;  
 }     
//-----------------------------------------------------------------------
uint16_t Mem_SearchStart(uint8_t TypeArch, uint8_t run, struct startendtime *STARTENDTIM)
{ uint16_t startpoint, endpoint; 
  uint8_t *s1;
  uint16_t ip,i;
  uint8_t ft[6],md[6];
  uint8_t  MemDat[64]; //[KolZapArc[SEC]];
  uint16_t di1,di2;  
    
    switch(TypeArch)
    {   case SEC: di1 = pArchiv.di1s[run]; di2=pArchiv.di2s[run];break;
        case PER: di1 = pArchiv.di1p[run]; di2=pArchiv.di2p[run];break;
        case HOU: di1 = pArchiv.di1h[run]; di2=pArchiv.di2h[run];break;
        case DAY: di1 = pArchiv.di1d[run]; di2=pArchiv.di2d[run];break;
        case AUD: di1 = pArchiv.di1Au[run]; di2=pArchiv.di2Au[run];break;
        case ALR: di1 = pArchiv.di1Al[run]; di2=pArchiv.di2Al[run];break;
        default: {di1 = 0; di2 = 0;}
    }           
    
    s1 = (unsigned char *)&(STARTENDTIM->STARTTIME); 
   for (ip=0; ip<6; ip++)
   { ft[5-ip] = *(s1+ip); }
 
   startpoint = FindDevide2(TypeArch, run, di1,di2, ft, &endpoint);  
   for (ip=startpoint; ip<=endpoint; ip++)
   { 
       ReadDataW25_IT(TypeArch, run, ip ,MemDat); 
       for (i=0; i<6; i++)
       { md[5-i] = MemDat[i]; }
        if (memcmp(ft,md,6) <= 0) // ����  ft<=md
        { return ip;  break;   }
   }
   return 0xFFFF;
}
//-------------------------------------------------------
uint16_t Mem_SearchEnd(uint8_t TypeArch,uint8_t run, struct startendtime *STARTENDTIM)
{ uint16_t startpoint, endpoint; 
  uint8_t *s1;
  uint16_t ip,j;
  uint8_t ft[6],md[6];
  uint8_t  MemDat[64];
  uint16_t di1,di2;  
    
    switch(TypeArch)
    {   case SEC: di1 = pArchiv.di1s[run]; di2=pArchiv.di2s[run];break;
        case PER: di1 = pArchiv.di1p[run]; di2=pArchiv.di2p[run];break;
        case HOU: di1 = pArchiv.di1h[run]; di2=pArchiv.di2h[run];break;
        case DAY: di1 = pArchiv.di1d[run]; di2=pArchiv.di2d[run];break;
        case AUD: di1 = pArchiv.di1Au[run]; di2=pArchiv.di2Au[run];break;
        case ALR: di1 = pArchiv.di1Al[run]; di2=pArchiv.di2Al[run];break;        
        default: {di1 = 0; di1 = 0;}
    }        
    s1 = (unsigned char *)&(STARTENDTIM->ENDTIME); 
   for (ip=0; ip<6; ip++)
   { ft[5-ip] = *(s1+ip); }
 
   startpoint = FindDevide2(TypeArch, run, di1,di2, ft, &endpoint);  
   for (ip=endpoint; ip>=startpoint; ip--)
   { 
       ReadDataW25_IT(TypeArch, run, ip ,MemDat); 
       for (j=0; j<6; j++)
       { md[5-j] = MemDat[j]; }
        if (memcmp(ft,md,6) >= 0) // ft<=md
        { return ip;  break;   }
   }
   return 0xFFFF;
}
//--------------------------------------------------------------------------- 
uint8_t Mem_SecDayRd (uint8_t TypeArch, uint8_t run, uint16_t ip) //, uint8_t* MemDat)
{
  return  ReadDataW25_IT(TypeArch, run, ip ,(uint8_t*)&MEMDATA); 
}
//---------------------------------------------------------------------------
uint8_t Mem_DayWr(uint8_t run) {
   return MemdataWr (DAY, run, &pArchiv.di1d[run], &pArchiv.di2d[run], MEMDATA);
}
//---------------------------------------------------------------------------
uint16_t Mem_SecDayRdEnd(uint8_t TypeArch, uint8_t run)
{ uint16_t di2;
    if (TypeArch==SEC) di2 = pArchiv.di2s[run];
    else if (TypeArch==DAY) di2 = pArchiv.di2d[run];
         else return 1;
  if (ReadDataW25_IT(TypeArch, run, di2 ,(uint8_t*)&MEMDATA))
      return 1;
  else return 0;    
}

//---------------------------------------------------------------------------
// ������ � ������ ���������� ���� typeS, ����� run, 
void Mem_Wrtdata(uint8_t typeS,uint8_t run, struct tdata SDATA)
{
    
 P23K256_Write_Data(&hspi3,&AdrSumm[typeS-1][run][0], (uint8_t*)&SDATA,sizeof(struct tdata));
}
void Mem_Rdtdata(uint8_t typeS,uint8_t run, struct tdata * pSDATA)
{
 P23K256_Read_Data(&hspi3,&AdrSumm[typeS-1][run][0], (uint8_t*)pSDATA,sizeof(struct tdata));          
}
//---------------------------------------------------------------------------
void Mem_Testtdata() {
   struct tdata    *pd;       // ��� �㬬�p������ ������
   unsigned int ks;

   pd = &TDATA;

   for (int i=1;i<4;i++) {    // ��� ������ PER,HOU,DAY
      for (int j=0;j<3;j++) { // ����� ����0,1,2
        Mem_Rdtdata(i,j,pd);    // type=(0-2) �⥭�� �㬬��p� �� ��p���
         ks = Mem_Crc16((uint8_t*)pd,sizeof(struct tdata)-2);
         if (pd->checksum == ks) {// ����p��쭠� �㬬�
         }
         else {
            printf("������ ����p������ ����� �������p�: ����� %d, ��� %d\n",i,j);

            pd->num     = 0;  //���-�� ����p����� ���祭��
            pd->seconds = 0;  //���-�� ����p����� ���祭�� � ᥪ㭤��
            pd->p  = 0.0;
            pd->dp_Qr = 0.0;
            pd->t  = 0.0;
            pd->q  = 0.0;
            pd->RO = 0.0;
            pd->N2 = 0.0;
            pd->CO2 = 0.0;
            pd->Heat = 0.0;
            pd->FullHeat = 0.0;

            pd->checksum = Mem_Crc16((uint8_t*)pd,sizeof(struct tdata)-2);

            Mem_Wrtdata(i,j,TDATA); // run=(0-2)
            Mem_Rdtdata(i,j,pd); // typeArch i=(1-3) ������ �������p� �� ��p���
                              
            if (pd->checksum == Mem_Crc16((uint8_t*)pd,sizeof(struct tdata)-2)) {
               printf("�������p �������, ����p. ����� ��pp������: ����� %d, ��� %d\n",i,j);
            }
         }
      }
   }
}
//---------------------------------------------------------------------------
void Setperiodtdata() {                 // ��� �㬬�p������ ��p�����᪨� ������ �� ��⪥
   struct tdata    *pd;                 // ��� �㬬�p������ ������
   struct memdata  *pmemdata;           // ��p���p� ����� ���� - 26 ����
   int             numrun;              // ⥪�騩 ����p ��⪨
   float              q,qq;             // ⥪�饥 ���祭�� ��ꥬ�
   char OdoMask;                        // ��᪠ ��⮪ ��� ���ਧ�樨
   int iOdoMax;                         // ��饥 �᫮ ��⮪ ��� ���ਧ�樨
   int j;
   int Takt;
   int Sp;
   int Sc;
   struct IdRun *prun;
//   char far *pENP;
   double *v;

   pd = &TDATA;
   pmemdata = &MEMDATA;
   numrun = Time_GetNumrun();      // ������ �������� ����p� �����
//printf("NumR=%d\n",numrun);
   prun = &CONFIG_IDRUN[numrun];    // ��������� �� ��p����p� �������� ����p� �����

   pmemdata->STARTTIME = BINTIME;
   pmemdata->stat = 0;        // ������ �������� ������ � p������
   pmemdata->count = 1;   // �p��� ���������� � ��������
   if (b_Massomer[numrun]) //Micromotion
   {   pmemdata->p  = CALCDATA[numrun].Pa;   //Pin;
   }
   else
   pmemdata->p  = CALCDATA[numrun].Pin; // ���. ��� �����. �������� �� ����� �p��p����, ���/��2;
   pmemdata->dp_Qr  = (!b_Counter[numrun]) ?
         IndDP[numrun] : (GLCONFIG.NumRun) ? CALCDATA[numrun].Qw_s : 0.0;
       
//printf("mem Qw_s=%f dP=%f\n",CALCDATA[numrun].Qw_s,pmemdata->dp);
   pmemdata->t  = CALCDATA[numrun].t;   // �����������, �p. �������;
   if (b_Massomer[numrun])
   pmemdata->RO   = CONFIG_ADRUN[numrun].ROdens;
   else
   pmemdata->RO   = CALCDATA[numrun].ROnom;
                                  // ���� 
   { pmemdata->CO2 = CALCDATA[numrun].NCO2;
     ConstCorr(CO2, &pmemdata->CO2, CONFIG_IDRUN[numrun].constdata);
   }
   pmemdata->N2  = CALCDATA[numrun].NN2;
   pmemdata->Heat = CALCDATA[numrun].Heat;
//float fdp = pmemdata->dp_Qr;
   ConstCorr(P, &pmemdata->p, CONFIG_IDRUN[numrun].constdata);
//   ConstCorr((b_Counter[numrun]) ? QW : DP,
//      &fdp, CONFIG_IDRUN[numrun].constdata);
   ConstCorr(T, &pmemdata->t, CONFIG_IDRUN[numrun].constdata);
   ConstCorr(RO, &pmemdata->RO, CONFIG_IDRUN[numrun].constdata);
   ConstCorr(N2, &pmemdata->N2, CONFIG_IDRUN[numrun].constdata);
   ConstCorr(Heat, &pmemdata->Heat, CONFIG_IDRUN[numrun].constdata);
   if (GLCONFIG.NumRun) {                // p��室 �� ⠪� p����� ��⥬�
     if (b_Massomer[numrun]) //Micromotion
       { pmemdata->q = IndQ[numrun];
//         pmemdata->FullHeat =0.0;
       }
      else
     {
      pmemdata->q = IndQ[numrun]/3600.0;//������� �� 1 ���
                                        // ������� �� 1 ���
      pmemdata->FullHeat = CALCDATA[numrun].FullHeat/3600.0;
     }
   }
   else {
      pmemdata->q = 0.0;
      pmemdata->FullHeat = 0.0;
   }
                                        // ����p��쭠� �㬬�
//   if (NoWriteCycle[numrun]==0)    // ������� ������ ������ ��� ������ ������ ����� ������ ���
//{  // ������ ������������ ������
    WriteCycleData(numrun, MEMDATA);
//printf("Mem_DayWr\n");
//}
//   else
//   NoWriteCycle[numrun] = 0;
// ������� V�� ������������ � ���
//   if (CONFIG_IDRUN[numrun].TypeRun == 10)  //&&(Tin>0))
//   {
//      pENP = (char far *)MK_FP(SEGMENT,6156+numrun*8);
//      v = &Vwsr[numrun];
//printf("VWsr[%d]=%lf\n",nRun_s,Vwsr[nRun_s]);
//      _fmemcpy(pENP,v,8); // ������ V�� � ���
//  }

   Mem_Rdtdata(PER,numrun,pd);     // ������ �������p� �� ��p���
      if (pd->num == 0)
      {                  // ��p��� ������ � �������p
      pd->STARTTIME = BINTIME;
                             // ��������������� ������ ��������� ������������
                           // �� ��������������� �������� ������� ���������� ����������� ������
      pd->STARTTIME.seconds = 0;
      pd->STARTTIME.minutes /= GLCONFIG.Period;
      pd->STARTTIME.minutes *= GLCONFIG.Period;
      pd->ENDTIME   = BINTIME;
//    }
//   if (pd->num == 0) {         //��p��� ������ � �������p
      pd->stat  = 0;               // ������ �������� ������ � p������ 
      pd->num++;              // ���-�� ����p����� ��������
                          // ���-�� ����p����� �������� � ��������
      pd->seconds = 1;
       if (b_Massomer[numrun]) //Micromotion
      {
       Vb[numrun] =pd->p;
       if (fabs(Vb[numrun]) < 0.001) Vb[numrun] = CALCDATA[numrun].Pa; //Pin;
//printf("1) Vb= %f Pin=%f \n",Vb[numrun],CALCDATA[numrun].Pin);

       if (fabs(CALCDATA[numrun].Pa) < 0.001)       //Pin
          pd->dp_Qr= 0.0;
       else
	    pd->dp_Qr= CALCDATA[numrun].Pa-Vb[numrun]; //Pin
       Vb[numrun] = CALCDATA[numrun].Pa;
       pd->RO   = CONFIG_ADRUN[numrun].ROdens;
      }
      else //�� ��������
      {
      pd->p     = CALCDATA[numrun].Pin; // ���. ��� �����. �������� �� ����� �p��p����, ���/��2
      pd->RO    = CALCDATA[numrun].ROnom; // ���������
      pd->dp_Qr = (!b_Counter[numrun]) ? IndDP[numrun] : CALCDATA[numrun].Qw_s;
      pd->N2    = CALCDATA[numrun].NN2;   // �����
      pd->CO2   = CALCDATA[numrun].NCO2;   // ���������� ���
      pd->Vru_total = CONFIG_ADRUN[numrun].Vtot_l;
      pd->Heat  = CALCDATA[numrun].Heat;   // ��������������
      }
      pd->t     = CALCDATA[numrun].t;    // �����������, �p. �������;

      if (GLCONFIG.NumRun) {         // p����� �� ���� p����� �������
        if (b_Massomer[numrun])
          {  if (CALCDATA[numrun].t > prun->R0min)
                pd->q = pd->dp_Qr / (CALCDATA[numrun].t);
                else pd->q = pd->dp_Qr/prun->dPhmin;
          }
         else
         pd->q = IndQ[numrun]/3600.0;
         pd->FullHeat = CALCDATA[numrun].FullHeat/3600.0;  //������� �� ������
      }
      else {
         pd->q = 0.0;
         pd->FullHeat = 0.0;  //������� �� ������
      }
//      if (GetFullVolFlag() == 1) {       // ������� �������� ������ � �������
//         if (AllAlarmCount[numrun].Flag > 0) {
//            pd->q      = 0.0;          
//         }
//      }
   }
   else
   {                     // ����������� ������ � �������p
      pd->ENDTIME = BINTIME;
      pd->stat  |= 0;               // ������ �������� ������ � p������
      pd->num++;                // ���-�� ����p����� ��������
						// ���-�� ����p����� �������� � ��������
      pd->seconds += 1;
     if (b_Massomer[numrun]) //Micromotion
     { if (fabs(CALCDATA[numrun].Pa) < 0.001)  pd->p =Vb[numrun];
	   else pd->p = CALCDATA[numrun].Pa;//in;
       pd->RO  += CONFIG_ADRUN[numrun].ROdens;
       if (fabs(Vb[numrun]) < 0.001) Vb[numrun] = CALCDATA[numrun].Pa;//Pin;
       float q1= (pd->p - Vb[numrun]);
	   pd->dp_Qr += q1; // pd->p - Vb[numrun];
       Vb[numrun] = pd->p;
       if (CALCDATA[numrun].t > prun->R0min)
       pd->q  += q1 / (CALCDATA[numrun].t); // ������� ������
       else pd->q  += q1/prun->dPhmin;
//printf("Vb=%f pd->dp=%f pd->p=%f\n",Vb[numrun],pd->dp,pd->p);
     }
      else  // �� ��������
      {                                   // ���� ��� ������
      pd->p += CALCDATA[numrun].Pin;    // ���. ��� �����. �������� �� �室� �p��p����, ���/�2
      pd->dp_Qr += 	 (!(b_Counter[numrun])) ?  IndDP[numrun] : CALCDATA[numrun].Qw/3600.0; //Qw_s;
//printf("run=%d Qsumr=%f Qw_s=%f\n",numrun,pd->dp,CALCDATA[numrun] .Qw_s);
      pd->RO    += CALCDATA[numrun].ROnom; // ���⭮���
      pd->N2    += CALCDATA[numrun].NN2;   // ����
      pd->CO2   += CALCDATA[numrun].NCO2;  // 㣫� ���� ���
      pd->Heat  += CALCDATA[numrun].Heat;  // 㤥�쭠� ⥯��⢮୮���
      if (GLCONFIG.NumRun)            // p��室 �� ⠪� p����� ��⥬�
      { 
//	    if (GetFullVolFlag() == 1) {   //������� �������� ������ � �������
//            if (!(AllAlarmCount[numrun].Flag > 0)) {
//               pd->q += IndQ[numrun]/3600,0;
//               pd->FullHeat += CALCDATA[numrun].FullHeat/3600,0;  
//	          }
//      }              //  ������� ������� ������ � �������
//      else 
        {
            pd->q += IndQ[numrun]/(double)3600;
            pd->FullHeat += CALCDATA[numrun].FullHeat/3600.0; // ������� �� 1 ���
        }
      }  
      else  {
         pd->q = 0.0;
         pd->FullHeat = 0.0;  // ������ ⥯��⢮୮���
        }
   }
      pd->t += CALCDATA[numrun].t;      // ���������, �p. ������;
  }

	// ����p������ �����
   pd->checksum = Mem_Crc16((uint8_t*)pd,sizeof(struct tdata)-2);
// printf("checksum=%04X\n",pd->checksum);
   Mem_Wrtdata(PER,numrun,TDATA);               // run=(0-2) ������ �㬬��p� �� ��p���
   if (GLCONFIG.NumRun) {
     Persist_AddQTotal(numrun, IndQ[numrun]/3600.0);
   }
/*
//������ ��娢� ���������� � SD
 MSDdataMgn *pbl;
 char nt,nt2,nt4; // str[40];
 int nz,m,k;
 long KBl,bl,t1,lTmp;
 char far *pb;

// Takt = (GLCONFIG.NumRun < 3) ? 2 : (numrun<2)? 4 : 2;
 Takt = 2;
 Sp = ARCM * Takt;
 Sc = (BINTIME.minutes % Takt) * 60 + BINTIME.seconds;
 nt = BINTIME.seconds % Takt;
// nt2 = BINTIME.seconds % 2;
// nt4 = BINTIME.seconds % 4;
//printf("s=%d nt=%d numrun=%d\n",BINTIME.seconds,nt, numrun);

 if ((nt == numrun)&&(SysTakt==1) || (SysTakt==2))
 {// printf("Bnew[%d]=%d NzBlk=%d nt=%d\n",numrun,Bnew[numrun],NzBlk[numrun],nt);

  nz=0;
  if (Bnew[numrun])
  { for (j=0; j<4; j++)
   if (Sc < Sp*(j+1))
    { nz = (Sc - j*Sp)/Takt-1;
      break;
    }
    Bnew[numrun] = 0;
    lTmp =  BinToSec(BINTIME);
    if (NzBlk[numrun] < 0)
    {
     KBl = (lTmp - SearchMgn[numrun][1].begNblk)/(ARCM*Takt)-1;
     bl=KBl;
//printf("tbeg=%ld ",SearchMgn[numrun][1].begNblk);
     if ((lTmp - SearchMgn[numrun][1].begNblk) % (ARCM*Takt)  == 0)
        nz =  -1;
     if (!((SearchMgn[numrun][0].begNblk == (ARCMBL-1))&& //  ��娢  �� ����
        (SearchMgn[numrun][0].endNblk == 0)))
     {
      if (SearchMgn[numrun][0].endNblk < SearchMgn[numrun][0].begNblk)
        { bl = KBl + SearchMgn[numrun][0].begNblk;
//         if (bl >= ARCMBL) bl = bl- ARCMBL;
        }
      t1 = SearchMgn[numrun][1].endNblk; //begNblk
      SearchMgn[numrun][1].endNblk = t1 + (KBl+1)*ARCM*Takt+Takt ;// lTmp-(nz+2)*Takt;
      if (bl >= ARCMBL) { SearchMgn[numrun][0].endNblk = bl-ARCMBL; }
      else SearchMgn[numrun][0].endNblk = bl;
      if ((SearchMgn[numrun][0].begNblk == 0) && (bl >= ARCMBL) || (SearchMgn[numrun][0].begNblk > 0))
         { SearchMgn[numrun][0].begNblk = bl++;
          if (bl >= ARCMBL)  SearchMgn[numrun][0].begNblk = bl-ARCMBL;
          SearchMgn[numrun][1].begNblk = SearchMgn[numrun][1].endNblk - ARCMBL*ARCM*Takt;
         }
      if ((kSDm[numrun] > 1) && (KBl > 0))
     { SearchMgn[numrun][kSDm[numrun]].endNblk = SearchMgn[numrun][0].endNblk;
//printf("m%d begBl=%lu endBl=%lu\n", numrun,
//     SearchMgn[numrun][kSDm[numrun]].begNblk,SearchMgn[numrun][kSDm[numrun]].endNblk);
     }
//printf("Blk_end=%ld lTmp=%ld KBl=%ld kSDm=%d\n",SearchMgn[numrun][0].endNblk,lTmp,KBl,kSDm[numrun]);
      m=0;
      if (SearchMgn[numrun][0].endNblk < SearchMgn[numrun][0].begNblk)
      { // ��।������ ����稭� ᤢ��� m
        bl=SearchMgn[numrun][0].endNblk;
        for (k=2; k<kSDm[numrun]; k++)
        { if (bl <= SearchMgn[numrun][k].begNblk) break;
          if ((bl > SearchMgn[numrun][k].begNblk)
             && (bl < SearchMgn[numrun][k].endNblk))
             { SearchMgn[numrun][k].begNblk = bl; break;}
          if (bl >= SearchMgn[numrun][k].begNblk) m++;
        }
         // ᤢ�� SearchMgn �� m
//printf("sdvig=%d bl=%d\n",m,bl);
        if (m>0)
        for (k=2; k<(kSDm[numrun]-m+1); k++)
        { SearchMgn[numrun][k].begNblk = SearchMgn[numrun][k+m].begNblk;
          SearchMgn[numrun][k].endNblk = SearchMgn[numrun][k+m].endNblk;
        }
//printf("kSDm=%d\n",kSDm[numrun]);
        kSDm[numrun] -= m;
      } // if ��।������ ����稭� ᤢ���
       outportb(0x1D0,BASE_CFG_PAGE);// ��⠭����� ��࠭��� ���
       poke(SEGMENT,16344+numrun*2,kSDm[numrun]); //��࠭��� kSDm
       pb = (char far *)MK_FP(SEGMENT,9624+numrun*1120); //  ���� ���᪠ � ���
    if ((kSDm[numrun] >= 0)&&(kSDm[numrun] < MSEACHL))
       _fmemcpy(pb,&SearchMgn[numrun][0],(kSDm[numrun]+1)*8); // ������ ���ᨢ� ���᪠ � ���

    } //��娢 �� ����
   }// if (NzBlk[numrun] < 0)
     // ���������� ����� �� 0 �� nz
//printf("Nz1=%d ",NzBlk[numrun]);
      for (j=nz; j>=0; j--)
         {  lTmp = lTmp - (long)Takt;
           SecToBin(&lTmp,&BLOKMGN[numrun].BlMgn[j].STARTTIME);
           memset(&BLOKMGN[numrun].BlMgn[j].Q,0,28);
            str_blk_mgn_wr(numrun,j);
         }
    NzBlk[numrun] = nz;
//printf("Nz2=%d lTmp=%ld\n",NzBlk[numrun],lTmp);
  } //if (Bnew[numrun])

   NzBlk[numrun]++;
   if ((NzBlk[numrun]<0)||(NzBlk[numrun] >= ARCM)) NzBlk[numrun]=0;
   pbl = &BLOKMGN[numrun].BlMgn[NzBlk[numrun]];
   pbl->STARTTIME=BINTIME;
   pbl->Q = pmemdata->q*3600/SysTakt;    // ��室 �� ��.��. �� ��ਮ�
   if (((GLCONFIG.IDRUN[numrun].TypeRun) == 4) || ((GLCONFIG.IDRUN[numrun].TypeRun) == 5)
     || ((GLCONFIG.IDRUN[numrun].TypeRun) == 10))
     {   pbl->Qru=pmemdata->dp*3600/SysTakt;   // 14 ��室 � ��  ��� 0
//         pbl->Vru = pmemdata->dp*(Takt/SysTakt);                  // 22 ��ꥬ � �� ��� ��. ��९��
       if (SysTakt == 2)
         pbl->Vru = pmemdata->dp;
       else
         pbl->Vru = pmemdata->dp + Vru1[numrun];
     }
   else   // ��९��
   { pbl->Qru = 0;
   pbl->Vru = pmemdata->dp;                  // 22 ��ꥬ � �� ��� ��. ��९��
   }
//   pbl->Vs = pmemdata->q*(Takt/SysTakt);      // 18 ��ꥬ �� ��.��. �� ��ਮ�
     if (SysTakt == 2)
      pbl->Vs = pmemdata->q;
     else
      pbl->Vs = pmemdata->q + Vs1[numrun];
   pbl->p=pmemdata->p;                   // 26 �p����� ��������
   pbl->t = pmemdata->t;                   // 30 �p����� ⥬������
   pbl->Ro = pmemdata->RO;                  // 34 �।��� ���⭮���/printf("NzBlk[%d]=%d\n",numrun,NzBlk[numrun]);
   str_blk_mgn_wr(numrun,NzBlk[numrun]);

 char s,s1,s2;
        s = BINTIME.seconds;
        s1 = (s+1) % 30;  //29,59
        s2 = (s+2) % 30;  //28,58
   if (((numrun==0) && (nt==0) || (numrun==1) && (nt==1) && (SysTakt==1)
      || (numrun==2)))
   {// printf("Conf s=%d\n",BINTIME.seconds);
     if (((s ==5) || (s==4)&&(GLCONFIG.NumRun==1))  && ((BINTIME.minutes % 2)==0))
        InQueue(3,Wr_blk_Conf);
     else
     if ((numrun == 0)&&(s2==0))       // 1-� ��⪠ �� 28,58 ᥪ
     {
       runSDm=0;
//    printf("  MGN  numrun=%d s=%d Ctime=%d\n",numrun,BINTIME.seconds,counttime);
       InQueue(3,blk_mgn_wrM);
     }
     else
     if ((numrun > 0)&&(s1==0))     // ᥪ 29, 59
     {
       runSDm=1;                     // 2-� � 3-� ��⪠
       InQueue(3,blk_mgn_wrM);
     }
    }
 } // if (nt == numrun)

      Vru1[numrun] = pmemdata->dp;
      Vs1[numrun] = pmemdata->q;

// ���਩�� ��ꥬ�
     if (AllAlarmCount[numrun].Flag > 0 || MinAlarmCount[numrun].Flag > 0) {
      if (AllAlarmCount[numrun].Flag > 0) {    // ���稪 ���਩ > 0
         outportb(0x1D0,BASE_CFG_PAGE); // ����� ��࠭��� ���䨣��樨

//           (ValarmSumm2+numrun)->VTotWc += IndQs[numrun]/(float)(3600/SysTakt);
   if (((GLCONFIG.IDRUN[numrun].TypeRun) == 4) || ((GLCONFIG.IDRUN[numrun].TypeRun) == 5)
     || ((GLCONFIG.IDRUN[numrun].TypeRun) == 10))
            (ValarmSumm2+numrun)->VTotWc += IndDP[numrun]/(float)(3600/SysTakt);
           (ValarmSumm2+numrun)->VTotSt += IndQ[numrun]/(float)(3600/SysTakt);
					// �������� �६� ��饥
         (ValarmSumm2+numrun)->TTot += SysTakt;
      }
//printf("TTot2= %d \n", (ValarmSumm2+numrun)->TTot);

      MinAlarmCount[numrun].Flag = GetVminFlag(numrun);

//    printf("SetValarmtdata - MinAlarmCount[numrun].Flag=%d\n",MinAlarmCount[numrun].Flag);

      if (MinAlarmCount[numrun].Flag > 0) {    // ���稪 ���਩ > 0
					// �������� �६� Qmin
         (ValarmSumm2+numrun)->Tqmin += SysTakt;
                                        // ���������� ��ꥬ
         (ValarmSumm2+numrun)->VAddSt += Qadsc[numrun]/(float)(3600/SysTakt);
      }
      (ValarmSumm2+numrun)->Crc = Crc16((unsigned char *)(ValarmSumm2+numrun),sizeof(struct valarm_data)-2);
   }
*/   
uint8_t i,ii;
///////////////// ��⠢�� ��� ����� ���ਧ�樨 /////////////
   if ((GLCONFIG.vol_odor != 0.0)&& (((numrun==1)&&(!(Bterm))) || numrun==0 || numrun==2))
    {      // �᫨ ������ ���ਧ���
                                        // ��᪠ ��⮪
      OdoMask = INTPAR.OdoMask;
         q = 0.0;
                                        // �᫨ ���������� ��᪠
      if (PrevOdoMask != OdoMask) {     // ��� ���.�᫮���
         OdoRest = 0.0;                 // ���⮪
         iQodo = 0;                     // ������
         Qodo[0] = Qodo[1] = Qodo[2] = 0.0;   // ��室�
      }

      if (OdoMask  != 0) {              // ���� �� ��⪨ ��� ���ਧ�樨
                                        // ��⪠ ������
         if ((OdoMask & (1<<numrun)) != 0)
        {
            iOdoMax = 0;                // ����. �᫮ ��⮪ � ���ਧ�樨
            for (ii=0; ii<GLCONFIG.NumRun; ii++)
               if (((OdoMask & (1<<ii)) != 0) && ((Bterm==0)||(ii!=1)))
               {   iOdoMax++;
//printf("iOdoMax=%d\n",iOdoMax);
               }
	    if (iQodo >= iOdoMax)
            {     // 㦥 �� ��⪨
               j = 0;                   // ᤢ�� ���� ��ꥬ��
                                        // ��ᬮ�� ��� ��⮪
               for (ii=0; ii<GLCONFIG.NumRun; ii++)
                                        // �᫨ ��⪠ ������ � ���ਧ�樨
                  if (((OdoMask & (1<<ii)) != 0) && ((Bterm==0)||(ii!=1)))
                   {
                                        // ᤢ����� ����� �� 1 ����
                     Qodo[j] = 0.0; //Qodo[j+1];
                     j++;
                   }
	       iQodo = iOdoMax - 1;
            }     // if (iQodo >= iOdoMax)
                                        // ��室 ⥪�饩 ��⪨
                                        // �ਢ������ � ᥪ㭤�
             if (b_Counter[numrun])
            { Qodo[iQodo] =  CALCDATA[numrun].Qw_s*CALCDATA[numrun].kQind;
//printf("numrun=%d, Qodo=%f kQind=%f Qw_s=%f\n", numrun,Qodo[iQodo],CALCDATA[numrun].kQind,CALCDATA[numrun].Qw_s);
            }
                 else
                  Qodo[iQodo] = IndQ[numrun]/3600.0;
//printf("numrun=%d  Qodo=%f\n",numrun, Qodo[iQodo]);
	     iQodo++;                    // �த������ ������
//printf("run=%d iQOdo=%d Bterm=%d\n",numrun,iQodo,Bterm);
         for (i=0; i<iQodo; i++) {     // �� �ᥬ ��⪠�
//printf("Qodo=%f  ", Qodo[iQodo-1]);
            q = q + Qodo[i];
         }
//        } //if ((OdoMask & (1<<numrun)) != 0)

         q += OdoRest;                     // �������� ���⮪
                                           // ���客�� �� ������� �� 0
         if ((GLCONFIG.vol_odor > 0.0) && (OdoMask != 0))
					   // �᫮ �����ᮢ � �뤠�
            OdoImp = (int)(q/(double)GLCONFIG.vol_odor);
         else
            OdoImp = 0;
//printf("q+R=%f OdoImp=%d s=%d c=%d run=%d\n",q,OdoImp,BINTIME.seconds,counttime,numrun);
         if (OdoImp > 1)
         {
            Fod = 20;             // 蠣 �뤠� �����ᮢ
            OdoImp = 2;
         }
         else
	    Fod = 40;                       // 蠣 �뤠� �����ᮢ
         OdoRest = q - OdoImp*GLCONFIG.vol_odor;

                                           // ��������� �������� ��� �뤠� �����ᮢ
         for (i=0; i<OdoImp; i++)
            OdoTab[OdoWrInd].StartTaktList[i] = (OdoImp-i-1)*Fod;
         OdoTab[OdoWrInd].Nimp = OdoImp;
         OdoTab[OdoWrInd].ImpLen = Fod/2;

	 if (OdoTab[OdoRdInd].Nimp == 0) {
            if (OdoRdInd == 0) {
               OdoRdInd = 1;
               OdoWrInd = 0;
            }
            else {
               OdoRdInd = 0;
               OdoWrInd = 1;
            }
         }
        } //if ((OdoMask & (1<<numrun)) != 0)
      }  //if (OdoMask  != 0) {
      else {                            // ��⮪ ��� ���ਧ�樨 ���
         OdoRest = 0.0;                 // ���⮪
         iQodo = 0;                     // ������
         Qodo[0] = Qodo[1] = Qodo[2] = 0.0;   // ��室�
      }
      PrevOdoMask = OdoMask;
   }
CONFIG_ADRUN[numrun].Qw_s = 0.0;
CALCDATA[numrun] .Qw_s = 0.0;
//printf("Takt=%d run=%d\n",counttime,numrun);
}
//---------------------------------------------------------------------------
void Sethourtdata()   //��� �㬬�p������ ������ �� �� � ������ ��p�����᪮�� ����
{ struct bintime        *pt;  //����筠� ��p���p� �p�����
  unsigned int          numrun;
  struct tdata          *ptdata;//㪠��⥫� �� ��p�����᪨� �㬬��p
  struct tdata          *phdata;//㪠��⥫� �� �ᮢ�� �㬬��p
  struct memdata  *pmpd;  //��p���p� ����� ��p-�� ���� - 26 ����
  int Takt;
  long  Sp;

   numrun = Time_GetNumrun();           // �⥭�� ⥪�饣� ����p� ��⪨
  
    pt = &BINTIME;
//printf("minutes=%d  seconds=%d run=%d\n",pt->minutes+1,pt->seconds,numrun);
  if( (((pt->minutes+1)%GLCONFIG.Period)==0) && ((pt->seconds+1)>=60))
  {
    ptdata = &TDATA;  //㪠��⥫� �� ��p�����᪨� �㬬��p
    phdata = &HDATA;  //㪠��⥫� �� �ᮢ�� �㬬��p
//printf("run=%d sec=%d min=%d\n",numrun,pt->seconds,pt->minutes);
//    Nsp[numrun] =0;
//    Nsp1[numrun]=0;

    Mem_Rdtdata(HOU,numrun,phdata);  //type=(0-2) �⥭�� �㬬��p� �� ��
//  HDATA = TDATA;    //��� �㬬�p������ ������ �� ��
    Mem_Rdtdata(PER,numrun,ptdata);  //type=(0-2) �⥭�� �㬬��p� �� ��p���
//--------- ��砫�: �����⮢�� �p����� ���祭�� � ��p�����᪮� �㬬��p� ----------
//printf("min=%d sec=%d num=%d \n",pt->minutes, pt->seconds, ptdata->num);
      if(ptdata->num)
     {
     if (!b_Massomer[numrun]) //Micromotion
      {
       ptdata->p      /= (float)ptdata->num; //�p����� ���祭��
        if (!b_Counter[numrun]) ptdata->dp_Qr /= (float)ptdata->num; //�p����� ���祭��
        ptdata->N2    /= (float)ptdata->num; //�p����� ���祭��
  	    ptdata->CO2   /= (float)ptdata->num; //�p����� ���祭��
        ptdata->Heat  /= (float)ptdata->num; //�p����� ���祭��
      }
        ptdata->t     /= (float)ptdata->num; //�p����� ���祭��
        ptdata->RO    /= (float)ptdata->num; //�p����� ���祭��
     }
     else
      {  ptdata->p      = 0.0;  //�p����� ���祭��
	     ptdata->dp_Qr     = 0.0;  //�p����� ���祭��
         ptdata->t      = 0.0;  //�p����� ���祭��
	     ptdata->q      = 0.0;  //�㬬�
         ptdata->RO     = 0.0; // �p����� ���祭��
	     ptdata->N2     = 0.0; // �p����� ���祭��
         ptdata->CO2    = 0.0; // �p����� ���祭��
         ptdata->Heat   = 0.0; // �p����� ���祭��
	     ptdata->FullHeat = 0.0; // �㬬�
      }
//--------- �����: �����⮢�� �p����� ���祭�� ----------
    pmpd = &MEMDATA;//��p���p� ����� ��p�����᪮�� ���� - 26 ����
    pmpd->STARTTIME = ptdata->STARTTIME;
//printf("pm.min=%d pm.sec=%d run=%d\n",pmpd->STARTTIME.minutes, pmpd->STARTTIME.seconds, numrun);
    pmpd->stat    = ptdata->stat;     //����� ��室��� ������ � p��室�
      if(ptdata->seconds % 60)
      { pmpd->count = ptdata->seconds / 60;
	pmpd->count++;  //�p��� ���������� � ������
      }
      else  pmpd->count = ptdata->seconds / 60;  //�p��� ���������� � ������
    pmpd->p       = ptdata->p;  //�p����� ���祭��
    pmpd->dp_Qr      = ptdata->dp_Qr; //�p����� ���祭��
    pmpd->t       = ptdata->t;  //�p����� ���祭��
    pmpd->q       = ptdata->q;  //�㬬�
    pmpd->FullHeat = ptdata->FullHeat;   // �㬬�

    pmpd->RO      = ptdata->RO;   // �p����� ���祭��
    pmpd->N2     = ptdata->N2;  // �p����� ���祭��
    pmpd->CO2    = ptdata->CO2; // �p����� ���祭��
    pmpd->Heat    = ptdata->Heat; // �p����� ���祭��
/**/
//float fdp = pmpd->dp_Qr;      
    ConstCorr(P, &pmpd->p, CONFIG_IDRUN[numrun].constdata);
//    ConstCorr( (b_Counter[numrun]) ? QW : DP,
//       &fdp, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(T, &pmpd->t, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(RO, &pmpd->RO, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(N2, &pmpd->N2, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(CO2,&pmpd->CO2, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(Heat, &pmpd->Heat, CONFIG_IDRUN[numrun].constdata);
/*
//������ ��娢� ��ਮ���᪨� � SD
 MSDdataPer *pbl;
 char nt;
 int nz,m,k,j;
 long KBl,bl,t1,lTmp,bl1;
 char far *pb;

 Takt = pmpd->count; // ⠪� ����� � ������
// nt = BINTIME.seconds % (SysTakt+1);
//printf("sec=%d\n",BINTIME.seconds);
// if (nt == abs(numrun-SysTakt))
 {
//printf("NzBlkp[%d]=%d\n",numrun,NzBlkp[numrun]);
  nz=-1;  Sp=0;
  if (BnewP[numrun])
  {  BnewP[numrun] = 0;
    lTmp =  BinToSec(BINTIME)/(60*Takt);  // ������
     Sp = (lTmp - SearchPer[numrun][1].endNblk);//-ARCP; // �ய�饭� ����ᥩ
//printf("lTmp=%ld Sp=%ld\n",lTmp,Sp);
     nz = Sp % ARCP-1;  // ����� ��ப� ��� �����
     KBl = Sp / ARCP;  // ������⢮ �ய�饭��� ������
     bl=KBl;
//printf("nz=%d KBl=%ld\n",NzBlkp[numrun],KBl);
    if (NzBlkp[numrun] < 0)
    {
     if (!((SearchPer[numrun][0].begNblk == (ARCPBL-1))&& //  ��娢  �� ����
        (SearchPer[numrun][0].endNblk == 0)))
     {
//      if (SearchPer[numrun][0].endNblk < SearchPer[numrun][0].begNblk)
        { bl = KBl + SearchPer[numrun][0].endNblk;
        }
      t1 = SearchPer[numrun][1].endNblk; //begNblk;
//printf("t1=%ld Kbl=%d\n",t1,KBl,numrun);
      SearchPer[numrun][1].endNblk = t1 + KBl*ARCP ;
      if (bl >= ARCPBL) { SearchPer[numrun][0].endNblk = bl-ARCPBL; }
      else SearchPer[numrun][0].endNblk = bl;

      if ((SearchPer[numrun][0].begNblk == 0) && (bl >= ARCPBL) || (SearchPer[numrun][0].begNblk > 0))
       { bl1 = SearchPer[numrun][0].endNblk+1;
           if (bl1==ARCPBL) bl1 = 0;
           SearchPer[numrun][0].begNblk = bl1;
           SearchPer[numrun][1].begNblk = SearchPer[numrun][1].endNblk - (long)ARCPBL*ARCP;
         }

      if ((kSDp[numrun] > 1) && (KBl > 0))
    {  SearchPer[numrun][kSDp[numrun]].endNblk = SearchPer[numrun][0].endNblk;
//printf("p%d begBl=%lu endBl=%lu\n", numrun,
//     SearchPer[numrun][kSDp[numrun]].begNblk,SearchPer[numrun][kSDp[numrun]].endNblk);
     }
//printf("Blk_end=%ld lTmp=%ld KBl=%ld kSDp=%d\n",SearchPer[numrun][0].endNblk,lTmp,KBl,kSDp[numrun]);
      m=0;
      if (SearchPer[numrun][0].endNblk < SearchPer[numrun][0].begNblk)
      { // ��।������ ����稭� ᤢ��� m
        bl=SearchPer[numrun][0].endNblk;
        for (k=2; k<kSDp[numrun]; k++)
        { if (bl <= SearchPer[numrun][k].begNblk) break;
          if ((bl > SearchPer[numrun][k].begNblk)
             && (bl < SearchPer[numrun][k].endNblk))
             { SearchPer[numrun][k].begNblk = bl; break;}
          if (bl >= SearchPer[numrun][k].begNblk) m++;
        }
         // ᤢ�� SearchPer �� m
//printf("sdvig=%d bl=%d\n",m,bl);
        if (m>0)
        for (k=2; k<(kSDp[numrun]-m+1); k++)
        { SearchPer[numrun][k].begNblk = SearchPer[numrun][k+m].begNblk;
          SearchPer[numrun][k].endNblk = SearchPer[numrun][k+m].endNblk;
        }
//printf("kSDp=%d\n",kSDp[numrun]);
        kSDp[numrun] -= m;
      } // if ��।������ ����稭� ᤢ���
       outportb(0x1D0,BASE_CFG_PAGE);// ��⠭����� ��࠭��� ���
       poke(SEGMENT,INDPER+numrun*2,kSDp[numrun]); //��࠭��� kSDp
       pb = (char far *)MK_FP(SEGMENT,MSPER+numrun*LMSBYTE); //  ���� ���᪠ � ���
    if ((kSDp[numrun] >= 0)&&(kSDp[numrun] < MSEACHL))
       _fmemcpy(pb,&SearchPer[numrun][0],(kSDp[numrun]+1)*8); // ������ ���ᨢ� ���᪠ � ���
    } //��娢 �� ����

   }// if (NzBlkp[numrun] < 0)
     // ���������� ����� �� 0 �� nz
     if ((SearchPer[numrun][0].begNblk == (ARCPBL-1)) && //  ��娢   ����
        (SearchPer[numrun][0].endNblk == 0))
    { nz = -1;  NzBlkp[numrun] = -1;}
      if (NzBlkp[numrun] < 0)
      {  k=0;
      }
       else
       { k=NzBlkp[numrun]+1;
//           if (Sp < 0)
//           nz = Sp + ARCP-1;
       }
      for (j=nz; j>=k; j--)
         {  lTmp = lTmp - 1; //(long)Takt;
            bl = lTmp*(60*Takt);
           SecToBin(&bl,&BLOKPER[numrun].BlPer[j].STARTTIME);
           memset(&BLOKPER[numrun].BlPer[j].count,0,21);
            str_blk_per_wr(numrun,j);
         }
    NzBlkp[numrun] = nz;
//printf("Nzp=%d\n",NzBlkp[numrun]);

//printf("Nz2=%d lTmp=%ld\n",NzBlkp[numrun],lTmp);
  }// if (BnewP[numrun])
   NzBlkp[numrun]++;
   if ((NzBlkp[numrun]<0)||(NzBlkp[numrun] >= ARCP)) NzBlkp[numrun]=0;
   pbl = &BLOKPER[numrun].BlPer[NzBlkp[numrun]];
   pbl->STARTTIME=BINTIME;
   pbl->count = pmpd->count;
   pbl->Vsu = pmpd->q;    // ��ꥬ �� ��.��. �� ��ਮ�
   if (((CONFIG_IDRUN[numrun].TypeRun) == 4) || ((CONFIG_IDRUN[numrun].TypeRun) == 5)
     || ((CONFIG_IDRUN[numrun].TypeRun) == 10))
     {
         pbl->Vru = pmpd->dp;   // ��ꥬ � ��
     }
   else
   {
     pbl->Vru = pmpd->dp; //  ��. ��९��
   }
   pbl->p = pmpd->p;                   // 26 �p����� ��������
   pbl->t = pmpd->t;                   // 30 �p����� ⥬������
   pbl->Ro = pmpd->RO;                  // 34 �।��� ���⭮���
//printf("NzBlkp[%d]=%d\n",numrun,NzBlkp[numrun]);
   str_blk_per_wr(numrun,NzBlkp[numrun]);
 } // ����� ᪮��� if (nt == numrun)
*/
//-------------------------------------------------
    if(phdata->num == 0)//��p��� ������ � �ᮢ�� �㬬��p
    {
      phdata->STARTTIME = ptdata->STARTTIME;
      // ᪮�४�஢��� ��砫� ���ࢠ�� ����������
      // �� �।�����饣� ��⭮�� ������ ���������� �ᮢ�� ������
      phdata->STARTTIME.seconds = 0;
      phdata->STARTTIME.minutes = 0;

      phdata->stat   = ptdata->stat;   //����� ��室��� ������ � p��室�
      phdata->seconds= ptdata->seconds;
      phdata->p      = ptdata->p;
      phdata->dp_Qr     = ptdata->dp_Qr;
      phdata->t      = ptdata->t;
      phdata->q      = ptdata->q;
      phdata->FullHeat = ptdata->FullHeat;
      phdata->RO     = ptdata->RO;
      phdata->N2    = ptdata->N2;
      phdata->CO2   = ptdata->CO2;
      phdata->Heat   = ptdata->Heat;
    }
    else  //����� �p��� ��p���
    { phdata->stat   |= ptdata->stat;
      phdata->seconds+= ptdata->seconds;
      if (b_Massomer[numrun]) //Micromotion
       phdata->p     = ptdata->p;  //��᫥���� ���祭��
      else
      { phdata->p      += ptdata->p;  //�p����� ���祭��
        phdata->FullHeat += ptdata->FullHeat;
        phdata->RO     += ptdata->RO;   // �p����� ���祭��
        phdata->N2    += ptdata->N2;
        phdata->CO2   += ptdata->CO2;
        phdata->Vru_total = ptdata->Vru_total;
        phdata->Heat   += ptdata->Heat;
      }
      phdata->dp_Qr     += ptdata->dp_Qr; //�p����� ���祭��
      phdata->t      += ptdata->t;  //�p����� ���祭��
      phdata->q      += ptdata->q;  //�㬬�
    }
//    sprintf(str,"Run=%d Num=%d Sec=%d\n\r",numrun,ptdata->num,ptdata->seconds);
//    HAL_UART_Transmit_IT(&huart2,(uint8_t*)str,sizeof(str));

    phdata->num++;      //���-�� ����p����� ���祭�� �ᮢ��
      phdata->ENDTIME   = ptdata->ENDTIME;

      ptdata->num = 0;    //���-�� ����p����� ���祭�� ��ਮ���᪨�
      ptdata->q   = 0.0;  //⥪�饥 ���祭��
      ptdata->FullHeat = 0.0;           // ⥪�饥 ���祭��
/*!!!*/
	if(b_Counter[numrun])
	 ptdata->dp_Qr = 0.0;
    TDATA.checksum = Mem_Crc16((uint8_t*)&TDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(PER,numrun,TDATA);//������ ��p�����᪮�� �㬬��p� - ���-�� ���.=0

//    TDATA = HDATA;
    HDATA.checksum = Mem_Crc16((uint8_t*)&HDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(HOU,numrun,HDATA);//������ �ᮢ��� �㬬��p�
//  pmpd->crc = Mem_Crc16((uint8_t*)&pmpd,sizeof(struct memdata)-2);//����p��쭠� �㬬�
//printf("run=%d pm.m=%d pm.s=%d\n",numrun,pmpd->STARTTIME.minutes,pmpd->STARTTIME.seconds);
// ������ ������ � ������������� �����
    MemdataWr(PER,numrun,&pArchiv.di1p[numrun],&pArchiv.di2p[numrun],MEMDATA);
  }
/*
  else  // if( (((pt->minutes+1)%GLCONFIG.Period)==0) &&

    if (((pt->seconds ==2) && (numrun==0)
    || ((pt->seconds ==1) || (pt->seconds ==3)) && (numrun==2)
      || (pt->seconds ==1) && (numrun==1)) && (NzBlkp[numrun] == (ARCP-1)))

    {  runSDp = ((numrun==2)&&(pt->seconds ==1)) ? 1 : numrun;
//printf("   PER numrun=%d Ctime=%d\n",numrun,counttime);
       InQueue(3,blk_per_wrP);
    }
*/  
}
//---------------------------------------------------------------------------
void Setdaytdata()   //��� �㬬�p������ ������ �� ��⪨ � ������ �ᮢ��� ����
{ struct bintime  *pt;  //����筠� ��p���p� �p�����
  unsigned int    numrun;
  struct tdata    *ptdata;//㪠��⥫� �� �ᮢ�� �㬬��p
  struct tdata    *phdata;//㪠��⥫� �� ����� �㬬��p
  struct memdata  *pmpd;  //��p���p� ����� �ᮢ��� ���� - 26 ����
  float fdp;
    
  numrun = Time_GetNumrun();           // �⥭�� ⥪�饣� ����p� ��⪨

    pt = &BINTIME;
  if ( (pt->minutes==59) && ((pt->seconds+1)>=60) )
  {
//TestPeriodRes();//!!
    ptdata = &TDATA;  //㪠��⥫� �� �ᮢ�� �㬬��p
    phdata = &HDATA;  //㪠��⥫� �� ����� �㬬��p


    Mem_Rdtdata(DAY,numrun,phdata);  //type=(0-2) �⥭�� �㬬��p� �� ��⪨
    Mem_Rdtdata(HOU,numrun,ptdata);  //type=(0-2) �⥭�� �㬬��p� �� ��
//--------- ��砫�: �����⮢�� �p����� ���祭�� � �ᮢ�� �㬬��p� ----------
    if(ptdata->num)
    {  if (!b_Massomer[numrun]) //Micromotion
	  {
	    ptdata->p      /= (float)ptdata->num; //�p����� ���祭��
        if (!b_Counter[numrun]) ptdata->dp_Qr /= (float)ptdata->num; //�p����� ���祭��
        ptdata->N2    /= (float)ptdata->num;  // �p����� ���祭��
        ptdata->CO2   /= (float)ptdata->num; // �p����� ���祭��
        ptdata->Heat   /= (float)ptdata->num; // �p����� ���祭��
      }
        ptdata->t      /= (float)ptdata->num; //�p����� ���祭��
        ptdata->RO     /= (float)ptdata->num;  // �p����� ���祭��
    }
    else
      {  ptdata->p      = 0.0;  //�p����� ���祭��
	     ptdata->dp_Qr     = 0.0;  //�p����� ���祭��
         ptdata->t      = 0.0;  //�p����� ���祭��
         ptdata->q      = 0.0;  //�㬬�
         ptdata->FullHeat = 0.0; // �㬬�

         ptdata->RO     = 0.0; // �।��� ���祭��
         ptdata->N2    = 0.0; // �p����� ���祭��
	     ptdata->CO2   = 0.0; // �p����� ���祭��
	     ptdata->Heat   = 0.0; // �p����� ���祭��
      }
//--------- �����: �����⮢�� �p����� ���祭�� ----------
    pmpd = &MEMDATA;  //��p���p� ����� �ᮢ��� ���� - 26 ����
    pmpd->STARTTIME = ptdata->STARTTIME;
    pmpd->stat    = ptdata->stat;     //����� ��室��� ������ � p��室�
      if(ptdata->seconds % 60)
      { pmpd->count = ptdata->seconds / 60;
        pmpd->count++;  //�p��� ���������� � ������
      }
      else  pmpd->count = ptdata->seconds / 60;  //�p��� ���������� � ������
    pmpd->p       = ptdata->p;  //�p����� ���祭��
    pmpd->dp_Qr      = ptdata->dp_Qr; //�p����� ���祭��
    pmpd->t       = ptdata->t;  //�p����� ���祭��
    pmpd->q       = ptdata->q;  //�㬬�
    pmpd->FullHeat  = ptdata->FullHeat; // �㬬�

    pmpd->RO      = ptdata->RO;   // �p����� ���祭��
    pmpd->N2     = ptdata->N2;  // �p����� ���祭��
    pmpd->CO2    = ptdata->CO2; // �p����� ���祭��
    pmpd->Heat    = ptdata->Heat; // �p����� ���祭��
/**/
    fdp =  pmpd->dp_Qr;
    ConstCorr(P, &pmpd->p, CONFIG_IDRUN[numrun].constdata);
    ConstCorr((b_Counter[numrun]) ? QW : DP,
       &fdp, CONFIG_IDRUN[numrun].constdata);
    pmpd->dp_Qr = fdp;  
    ConstCorr(T, &pmpd->t, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(RO, &pmpd->RO, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(N2, &pmpd->N2, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(CO2, &pmpd->CO2, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(Heat, &pmpd->Heat, CONFIG_IDRUN[numrun].constdata);
/**/
    if(phdata->num == 0)//��p��� ������ � �������� �������p
    {
      phdata->STARTTIME = ptdata->STARTTIME;

     // ��������������� ������ ��������� ����������
    // �� ��������������� �������� ������� ���������� �������� ������
//      TimeCorrectionForDay(&phdata->STARTTIME, GLCONFIG.ContrHour);

      phdata->stat   = ptdata->stat;   //����� ��室��� ������ � p��室�
      phdata->p      = ptdata->p;
      phdata->dp_Qr     = ptdata->dp_Qr;
      phdata->t      = ptdata->t;
      phdata->q      = ptdata->q;
      phdata->FullHeat = ptdata->FullHeat;

      phdata->RO     = ptdata->RO;
      phdata->N2    = ptdata->N2;
      phdata->CO2   = ptdata->CO2;
      phdata->Heat   = ptdata->Heat;
    }
    else  //����� �p��� ��p���
    { phdata->stat   |= ptdata->stat; //����� ����� ���筮�� ����
      if (b_Massomer[numrun]) //Micromotion
       phdata->p      = ptdata->p;  //�p����� ���祭��
      else
     { phdata->p      += ptdata->p;  //�p����� ���祭��
       phdata->FullHeat += ptdata->FullHeat; // �㬬�
       phdata->RO     += ptdata->RO;   // �p����� ���祭��
       phdata->N2    += ptdata->N2;  // �p����� ���祭��
       phdata->CO2  += ptdata->CO2; // �p����� ���祭��
       phdata->Vru_total = ptdata->Vru_total;
       phdata->Heat   += ptdata->Heat; // �p����� ���祭��
     }
      phdata->dp_Qr     += ptdata->dp_Qr; //�p����� ���祭��
      phdata->t      += ptdata->t;  //�p����� ���祭��
      phdata->q      += ptdata->q;  //�㬬�
    }
      phdata->num++;       //���-�� ����p����� ���祭��
      phdata->ENDTIME   = ptdata->ENDTIME;;
      ptdata->num = 0;
      ptdata->q   = 0.0;
      ptdata->FullHeat = 0.0;
	  if(b_Counter[numrun])
	    ptdata->dp_Qr = 0.0;
/*
  if ((GLCONFIG.IDRUN[numrun].TypeRun) == 10)
   {	  r = Vr_h[numrun];
   }
*/
    TDATA.checksum = Mem_Crc16((uint8_t*)&TDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(HOU,numrun,TDATA);//������ �ᮢ��� �㬬��p� - ���-�� ���.=0
//    TDATA = HDATA;
    HDATA.checksum = Mem_Crc16((uint8_t*)&HDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(DAY,numrun,HDATA);                      //������ ���筮�� �㬬��p�
// ������ �������� ������
    MemdataWr(HOU,numrun,&pArchiv.di1h[numrun],&pArchiv.di2h[numrun],MEMDATA);
//TestPeriodRead();//!!
  }
//printf("Daytdata run=%d Ctime=%d\n", numrun,counttime);

}
//---------------------------------------------------------------------------
void Setmonthtdata() { //������ ���筮�� ����
   struct bintime  *pt;       // ����筠� ��p���p� �p�����
   unsigned int    numrun;
   struct tdata    *ptdata;   // 㪠��⥫� �� �ᮢ�� �㬬��p
   struct tdata    *phdata;   // 㪠��⥫� �� ����� �㬬��p
   struct memdata  *pmpd;     // ��p���p� ����� ���� - 26 ����
   long  ValarmMin, lTmp;
   struct bintime binTmp;
   float fdp;
   
//printf("Ctime=%d\n",counttime);
   pt = &BINTIME;
   numrun = Time_GetNumrun();           // �⥭�� ⥪�饣� ����p� ��⪨

   if (((pt->hours+1)==GLCONFIG.ContrHour) && (pt->minutes==59)
      && ((pt->seconds+1)>=60))
    {
      ptdata = &TDATA;           // 㪠��⥫� �� ����� �㬬��p

      CONFIG_ADRUN[numrun].Ksq = 0.0;  // ����� ��।����� �� �訡�� �裡 �� MODBUS
      Mem_Rdtdata(DAY,numrun,ptdata);     // type=(0-2) �⥭�� �㬬��p� �� ��⪨

      //--------- ��砫�: �����⮢�� �p����� ���祭�� � ���筮� �㬬��p� ----------
   if (ptdata->num) {
	if (!b_Massomer[numrun] ) //Micromotion
	{
	 ptdata->p /= (float)ptdata->num;    // �p����� ���祭��
	 if ((!b_Counter[numrun]))
	   ptdata->dp_Qr /= (float)ptdata->num; // �p����� ���祭��
	 ptdata->N2  /= (float)ptdata->num;
	 ptdata->CO2 /= (float)ptdata->num;
	 ptdata->Heat /= (float)ptdata->num;
	}

	 ptdata->t /= (float)ptdata->num;    // �p����� ���祭��
	 ptdata->RO   /= (float)ptdata->num;
   }
   else {
	 ptdata->p      = 0.0;  //�p����� ���祭��
	 ptdata->dp_Qr          = 0.0;  //�p����� ���祭��
	 ptdata->t      = 0.0;  //�p����� ���祭��
	 ptdata->q      = 0.0;  //�㬬�
	 ptdata->FullHeat = 0.0;        // �㬬�
	 ptdata->RO     = 0.0;          // �।��� ���祭��
	 ptdata->N2    = 0.0;
	 ptdata->CO2   = 0.0;
	 ptdata->Heat   = 0.0;
   }
      //--------- �����: �����⮢�� �p����� ���祭�� ----------

      pmpd = &MEMDATA;//��p���p� ����� ���� - 26 ����

      pmpd->STARTTIME = ptdata->STARTTIME;
      pmpd->stat    = ptdata->stat;     //����� ��室��� ������ � p��室�
      pmpd->p       = ptdata->p;  //�p����� ���祭��
      pmpd->dp_Qr   = ptdata->dp_Qr; //�p����� ���祭��
      pmpd->t       = ptdata->t;  //�p����� ���祭��
      fdp =  pmpd->dp_Qr;
      pmpd->RO      = ptdata->RO;   // �p����� ���祭��
      pmpd->N2      = ptdata->N2;  // �p����� ���祭��
      pmpd->CO2     = CONFIG_ADRUN[numrun].Vtot_l/1000.0;
      pmpd->Heat    = ptdata->Heat; // �p����� ���祭��
      ConstCorr(P, &pmpd->p, CONFIG_IDRUN[numrun].constdata);
      ConstCorr((b_Counter[numrun]) ? QW : DP, &fdp, CONFIG_IDRUN[numrun].constdata);
      pmpd->dp_Qr = fdp;
      ConstCorr(T,   &pmpd->t, CONFIG_IDRUN[numrun].constdata);
      ConstCorr(RO,  &pmpd->RO,   CONFIG_IDRUN[numrun].constdata);
      ConstCorr(N2,  &pmpd->N2,  CONFIG_IDRUN[numrun].constdata);
      ConstCorr(CO2, &pmpd->CO2, CONFIG_IDRUN[numrun].constdata);
      ConstCorr(Heat,&pmpd->Heat, CONFIG_IDRUN[numrun].constdata);

      pmpd->q       = ptdata->q;  // �㬬�
      pmpd->FullHeat = ptdata->FullHeat; // �㬬�

//      Mem_DayStereoCountAlarm(pmpd->dp,numrun);

      Mem_AddToMonth();  // �������� � ������ �����

      ptdata->num = 0;
      ptdata->q   = 0.0;
      ptdata->FullHeat = 0.0;

      if (b_Counter[numrun])
	 ptdata->dp_Qr = 0.0;

//      if ((GLCONFIG.IDRUN[numrun].TypeRun) == 10)
//       pmpd->RO = Vr_d[numrun];
						   //����p��쭠� �㬬�
      ptdata->checksum = Mem_Crc16((uint8_t*)ptdata,sizeof(struct tdata)-2);

      Mem_Wrtdata(DAY,numrun,TDATA);                       // ������ ���筮�� �㬬��p�
      //����p��쭠� �㬬�
//    pmpd->checksum = Crc16Int(pmpd,sizeof(struct memdata)-2);
      Mem_DayWr(numrun);   
// ���਩�� ��ꥬ�
/*
					// �஢�ઠ �� ������ � ��娢
					//  ���.����. ���਩ ��� �� V����.- ᥪ㭤�
      ValarmMin = INTPAR.AlarmVolumeMinTime;

					// �᫨ ��饥 �६� ���਩ > ���������
					// �� 㬮�砭�� 60 ᥪ.
					// ��� �뫨 �몫�祭��
      if (((ValarmSumm+numrun)->TTot+(ValarmSumm2+numrun)->TTot) > ValarmMin
//	  ((ValarmSumm+numrun)->Tqmin+(ValarmSumm2+numrun)->Tqmin) > ValarmMin )
    ||  ((ValarmSumm+numrun)->Toff+(ValarmSumm2+numrun)->Toff) > ValarmMin)    // ������� � ��娢
       {  if (((ValarmSumm+numrun)->VTotSt+(ValarmSumm2+numrun)->VTotSt) >pmpd->q)
           {  (ValarmSumm+numrun)->VTotSt = pmpd->q;
              (ValarmSumm2+numrun)->VTotSt = 0;
               if ((GLCONFIG.IDRUN[numrun].TypeRun) == 4
               || (GLCONFIG.IDRUN[numrun].TypeRun) == 5
               || (GLCONFIG.IDRUN[numrun].TypeRun) == 10)
              {(ValarmSumm+numrun)->VTotWc = pmpd->dp;
              (ValarmSumm2+numrun)->VTotWc = 0;}
           }
	 WriteValarmData(GetValarmPage(numrun),numrun);
        }
      lTmp = BinToSec(BINTIME)+4L;
      SecToBin(&lTmp,&binTmp);
      ValarmSummIni(binTmp,numrun);
*/
   }
   
//printf("***m run=%d  c=%d\n",numrun,counttime);
}
//---------------------------------------------------------------------------
void Mem_Clrtdata(char empRun)   //�p�� �㬬��p��
{ int i,j,k;

  // ����� ��⥬���� �६���
  struct bintime _BINTIME = BINTIME;

  struct tdata    *ptdata;   //��� �㬬�p������ ������
  ptdata = &TDATA;
  ptdata->stat    = 0;    //����� ��室��� ������ � p��室�
  ptdata->num     = 0;    //���-�� ����p����� ���祭��
  ptdata->seconds = 0;    //���-�� ����p����� ���祭�� � ᥪ㭤��
  ptdata->p       = 0.0;
  ptdata->dp_Qr      = 0.0;
  ptdata->t       = 0.0;
  ptdata->q       = 0.0;
  ptdata->RO       = 0.0;
  ptdata->N2      = 0.0;
  ptdata->CO2     = 0.0;
  ptdata->Heat     = 0.0;
  ptdata->FullHeat = 0.0;

  // ���� ���⮢��� �६��� ���樨஢��� �।�����騬
  // ���� �����⮬ ����������
  for(i=1;i<4;i++)
  {
   if (((i==1)&&(empRun & 0x20)) || ((i==2)&&(empRun & 0x40))
      ||((i==3)&&(empRun & 0x80)))
   {
// printf(" i=%d\n",i);
    ptdata->STARTTIME = _BINTIME;
    ptdata->STARTTIME.seconds = 0;
    ptdata->STARTTIME.minutes /= GLCONFIG.Period;
    ptdata->STARTTIME.minutes *= GLCONFIG.Period;
    ptdata->checksum = Mem_Crc16((uint8_t*)ptdata,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(i,0,TDATA);              // run=(0-2) ������ ��p�����᪮�� �㬬��p�

    ptdata->STARTTIME = _BINTIME;
    ptdata->STARTTIME.seconds = 0;
    ptdata->STARTTIME.minutes = 0;
    ptdata->checksum = Mem_Crc16((uint8_t*)ptdata,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(i,1,TDATA);               // run=(0-2) ������ �ᮢ��� �㬬��p�

    ptdata->STARTTIME = _BINTIME;
//    TimeCorrectionForDay(&ptdata->STARTTIME, GLCONFIG.ContrHour);
    ptdata->checksum = Mem_Crc16((uint8_t*)ptdata,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(i,2,TDATA);              // run=(0-2) ������ ���筮�� �㬬��p�

//           // ���㫨��  ������ �㬬����
//    outportb(0x1D0,BASE_CFG_PAGE);      // ��⠭���� ��p����� �����
//         for (k=0; k<128; k++)
//           pokeb(SEGMENT,5120+i*128+k,0);
   }
  }
}
//---------------------------------------------------------------------------
//���⪠ ��p����� ����⥫���
void Mem_AuditClr(unsigned char empRun)
{ int i;
/*
    if (empRun & 0x20)
  { i=0;
    SetPage(Mem_GetAuditPage(i));
    poke(SEGMENT,0,0);
    poke(SEGMENT,2,ARCAU);
  }
  if (empRun & 0x40)//&&(GLCONFIG.NumRun > 1))
  { i=1;
    SetPage(Mem_GetAuditPage(i));
    poke(SEGMENT,0,0);
    poke(SEGMENT,2,ARCAU);
  }
  if (empRun & 0x80)//&&(GLCONFIG.NumRun > 2))
  { i=2;
    SetPage(Mem_GetAuditPage(i));
    poke(SEGMENT,0,0);
    poke(SEGMENT,2,ARCAU);
  }
*/
}
//---------------------------------------------------------------------------
//�室�� ��p����p� - ����p �����, ����p ��p�����

uint8_t Mem_AuditRd (uint8_t run, uint16_t ip, uint8_t* MemDat)
{ if (ip > pArchiv.di2Au[run]) return 1;
   if  (ReadDataW25_IT(AUD, run, ip ,MemDat)) 
       return 1;    
   return 0;
}

//---------------------------------------------------------------------------
//��砫쭮� ���祭��, ᬥ饭��, ��p����; ����. ������ = ARCAU-1=947
//unsigned int Mem_AuditNextIndex (unsigned int start, unsigned int index, unsigned char page) {
//   unsigned int  startindex,endindex;
//}
//---------------------------------------------------------------------------
//���⪠ ��p����� ���p��
void Mem_AlarmClr(unsigned char empRun)
{ int i;
/*
    if (empRun & 0x20)
  { i = 0;
    poke(SEGMENT,0,0);
    poke(SEGMENT,2,1152);  //1152 �����
  }
  if (empRun & 0x40)//&&(GLCONFIG.NumRun > 1))
  { i = 1;
    SetPage(Mem_AlarmGetPage(i));
    poke(SEGMENT,0,0);
    poke(SEGMENT,2,1152);  //1152 �����
  }
  if (empRun & 0x80)//&&(GLCONFIG.NumRun > 2))
  { i = 2;
    SetPage(Mem_AlarmGetPage(i));
    poke(SEGMENT,0,0);
    poke(SEGMENT,2,1152);  //1152 �����
  }
*/
}
//---------------------------------------------------------------------------
unsigned int Mem_AlarmTestCrc()
{ struct memalarm   *pcrc;
  pcrc = &MEMALARM;   //��p���p� ����� �p娢� ���p��
  if(pcrc->crc ==  Mem_Crc16((uint8_t*)pcrc, sizeof(struct memalarm)-2)) return 0;//������ ����p��쭮� �㬬�
  else return 1;
}
//---------------------------------------------------------------------------
void Mem_AlarmSetCrc()
{ struct memalarm   *pcrc;
  pcrc = &MEMALARM;   //��p���p� ����� �p娢� ���p��
  pcrc->crc =  Mem_Crc16((uint8_t*)pcrc, sizeof(struct memalarm)-2);//������ ����p��쭮� �㬬�
}
//---------------------------------------------------------------------------
//�室�� ��p����p� - ����p �����, ����p ��p�����
uint8_t Mem_AlarmRd(uint16_t run, uint16_t ip, uint8_t* MemDat)
{ if (ip > pArchiv.di2Al[run]) return 1;
   if  (ReadDataW25_IT(ALR, run, ip ,MemDat)) 
       return 1;    
   return 0;
}
//---------------------------------------------------------------------------
//��砫쭮� ���祭��, ᬥ饭��, ��p����; ����. ������ = 1151
/*unsigned int Mem_AlarmNextIndex(unsigned int start, unsigned int index, unsigned char page)
{ unsigned int  startindex,endindex;
    if(index > 1151) return 65535;  //��室 �室���� ᬥ饭�� �� ��������

asm   cli

  outportb(0x1d0,page); //��⠭���� ��p����� �����
    _ES = SEGMENT;       //��p�� ��᪨��p� ��p�����
    _DI = 0;
asm   mov ax,word ptr es:[di]   //������ ��p���� ����ᠭ���� �������
    startindex  = _AX;
asm   mov ax,word ptr es:[di+2] //������ ��᫥����� ����ᠭ���� �������
    endindex    = _AX;

asm   sti

    if(endindex > (ARCAL-1)) return 65535; //��� ����ᥩ � ��p����
    if(startindex > endindex)         //��p���� ��������� ���������
    { start += index;
      if(start > (ARCAL-1)) start -= ARCAL;
      return start;
    }
    else  //���p �������� ���筮 ��� ��p�室 㪠��⥫�� �p�� �p�����
    { start += index;
      if(start <= endindex)  return start;
      else return 65535;
    }
}
*/
//---------------------------------------------------------------------------
//����� ��������������, �������� � ��������� ���������� = ����� �� ������ �����
float  Mem_GetDayQ(uint8_t run)
{ float value;
    Mem_Rdtdata(PER,run,&TDATA);         //type=(0-2) �⥭�� �㬬��p� �� ��p���
    value = TDATA.q; //���祭��
    Mem_Rdtdata(HOU,run,&TDATA);         //type=(0-2) �⥭�� �㬬��p� �� ��
    value += TDATA.q;//���祭��
    Mem_Rdtdata(DAY,run,&TDATA);         //type=(0-2) �⥭�� �㬬��p� �� ��⪨
    value += TDATA.q;//���祭��
  return value;
}
//---------------------------------------------------------------------------
//������ �������� �������� ������� �� ������� ����� �� ��������� �����
float Mem_GetDayE(unsigned char run)
{ float value;
    Mem_Rdtdata(PER,run,&TDATA);         //type=(0-2) ������ �������p� �� ��p���
    value = TDATA.FullHeat; //��������
    Mem_Rdtdata(HOU,run,&TDATA);         //type=(0-2) ������ �������p� �� ���
    value += TDATA.FullHeat;//��������
    Mem_Rdtdata(DAY,run,&TDATA);         //type=(0-2) ������ �������p� �� �����
    value += TDATA.FullHeat;//��������
  return value;
}
//---------------------------------------------------------------------------
void Mem_AlarmSetStart() {
   uint8_t i;
   for (i=0; i<GLCONFIG.NumRun; i++) {
           // ⨯ ���p�� - ����祭�� �� RESET
      Mem_AlarmWrCodRun(i,0x08,Mem_GetDayQ(i));
   }
}
//---------------------------------------------------------------------------
void Mem_AlarmSetStop() {
   uint8_t i;

   for (i=0; i<GLCONFIG.NumRun; i++) {
        memcpy(&MEMALARM.TIME,&POWER_OFF_TIME,6);
      MEMALARM.type = 0x88;             // ⨯ ���p�� - RESET
      MEMALARM.value = Mem_GetDayQ(i);
      Mem_AlarmWr(i,MEMALARM); // ������ ���� ���p��
   }
}
//---------------------------------------------------------------------------
//����p��� �室��� ��p����p�� ���稪�� - ��. ����p���� � ���祭��
//unsigned int Mem_AlarmSetValEdErr()
uint16_t Mem_AlarmSetValErr(uint16_t nRun)
{
   struct IdRun    *prun;               // 㪠��⥫� �� ��p����p� ��⪨
   struct AdRun    *aprun;              // 㪠��⥫� �� ��p����p� ��⪨
   unsigned int    *pbuferr;
   unsigned int    *perr;
   unsigned int    err;
   float           value, Qru;
   struct calcdata         *pcalc;

   prun = &CONFIG_IDRUN[nRun];        // 㪠��⥫� �� ⥪���� ����
   aprun = &CONFIG_ADRUN[nRun];       // 㪠��⥫� �� ⥪���� ����
   pcalc = &CALCDATA[nRun];

   switch (nRun) {                      // �롮p ���p� �� ����p� ��⪨
      case 0: pbuferr = membuferrrun0; break;
      case 1: pbuferr = membuferrrun1; break;
      case 2: pbuferr = membuferrrun2; break;
   }

   perr = coderrgr;                     // 㪠��⥫� �� ���� �訡�� ���������

                                        //-- ��⠭���� �訡�� ��������� �� ���稪� ��������
   if (prun->constdata & 2) {           // �������� � ���稪��
      if ((aprun->statP & 0x04) == 0) { // ��� �訡�� ��. ����p����
                                        // ��ॢ�� ������ ����७�� �������� � ���/�2
         value =  aprun->P_s;
         switch (ChkSensMax(2,value,prun->Pmax)) {
            case 0:  err = perr[1]; break;
            case 1:  err = perr[0]; break;
            default: err = pbuferr[4];
         }
         if (err != pbuferr[4]) {       // ����� ����襣�
            Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
            pbuferr[4] = err;          // �p�᢮��� ����� ���祭��
         }

         switch(ChkSensMin(2,value,prun->Pmin)) {
            case 0:  err = perr[3]; break;
            case 1:  err = perr[2]; break;
            default: err = pbuferr[5];
         }
//printf("val=%f Pmin=%f\n",value,prun->Pmin);
                                        // ����� ����襣�
         if ((err != pbuferr[5]) && (prun->Pmin != 0.0)) {
            Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
            pbuferr[5] = err;           // �p�᢮��� ����� ���祭��
         }
      }
      if (value < prun->Pmax)
         aprun->statP &= 0xF7;          // �p. �訡�� ���������
   }
   else {                               // �������� - ���⠭�
      err = (prun->constP > prun->Pmax) ? perr[0] : perr[1];
      if (err != pbuferr[4]) {          // ����� ����襣�
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[4] = err;              // �p�᢮��� ����� ���祭��
      }

      err = (prun->constP < prun->Pmin) ? perr[2] : perr[3];
      if (err != pbuferr[5]) {          // ����� ����襣�
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[5] = err;              // �p�᢮��� ����� ���祭��
      }
   }
                                        //-- ����� ��⠭���� �訡�� �� ���稪� ��������

                                        //-- ��⠭���� �訡�� ��������� �� ���稪� ⥬��p���p�
   if (prun->constdata & 4) {           // ⥬��p���p� � ���稪��
      if ((aprun->statt & 0x04) == 0) {  // ��� �訡�� ��. ����p����
         switch(ChkSensMax(1,aprun->t,prun->tmax)) {
            case 0:  err = perr[5]; break;
            case 1:  err = perr[4]; break;
            default: err = pbuferr[6];
         }
         if (err != pbuferr[6]) {       // ����� ����襣�
            Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
            pbuferr[6] = err;           // �p�᢮��� ����� ���祭��
         }
         switch(ChkSensMin(1,aprun->t,prun->tmin)) {
            case 0:  err = perr[7]; break;
            case 1:  err = perr[6]; break;
            default: err = pbuferr[7];
         }
         if (err != pbuferr[7]) {       // ����� ����襣�
            Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
            pbuferr[7] = err;           // �p�᢮��� ����� ���祭��
         }
      }
   }
   else {                               // ⥬��p���p� - ����⠭�
      err = (prun->constt > prun->tmax) ? perr[4] : perr[5];
      if (err != pbuferr[6]) {          // ����� ����襣�
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[6] = err;              // �p�᢮��� ����� ���祭��
      }
//printf("constT=%f tmin=%f\n",prun->constt, prun->tmin);
      err = (prun->constt < prun->tmin) ? perr[6] : perr[7];
      if (err != pbuferr[7]) {          // ����� ����襣�
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[7] = err;              // �p�᢮��� ����� ���祭��
//printf("err=%02X\n",err);
      }
   }
                                        //-- ����� ��⠭���� �訡�� �� ���稪� ⥬��p���p�

                                        //-- ��⠭���� �訡�� ��������� �� ���稪�� ��p�����
// if (prun->TypeRun != 4) {            // ᯮᮡ ����p���� �� ��⪥ - ����p����
   if (((prun->TypeRun) != 4) && ((prun->TypeRun) != 5) && ((prun->TypeRun) != 10)) {
      if (prun->constdata & 1) {        // ��p���� � ���稪��
       if (((prun->TypeRun) == 2)||((prun->TypeRun) == 3)) {
// ������ ��९��                 // ��� �訡�� ��. ����p����
         if ((aprun->statdPl & 0x04) == 0) {
                                        // ��ॢ�� ������ ����७�� ������� ��९��� � ���/�2
           value = GetCalibrValue(nRun,3,aprun->dPl);
/*
            value =
                                        // ��� -> ���/�2
               (aprun->eddpl == 0x0C) ? aprun->dPl * 101.9716 :
                                        // ��/�2 -> ���/�2
               (aprun->eddpl == 0x09) ? aprun->dPl * 10.0 :
                                       // ���/�2 -> ���/�2
               (aprun->eddpl == 0x0A) ? aprun->dPl * 10000.0 :
                aprun->dPl;              //0x82 0xEE: ���/�2 -> ���/�2
*/
               switch(ChkSensMin(4,value,prun->dPhmin)) {
                  case 0:  err = perr[15]; break;
                  case 1:  err = perr[14]; break;
                  default: err = pbuferr[11];
               }
                                        // ����� ����襣�
               if ((err != pbuferr[11]) && (prun->dPhmin != 0.0)) {
                  Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
                  pbuferr[11] = err;    // �p�᢮��� ����� ���祭��
               }
            } //if ((aprun->statdPl & 0x04) == 0)
          }   // if (((prun->TypeRun) == 2)||((prun->TypeRun) == 3))
                                        // ��� �訡�� ��. ����p����
         if ((aprun->statdPh & 0x04) == 0) {
                                        // ��ॢ�� ������ ����७�� ���孥�� ��९��� � ���/�2
/*            value =
                                        // ��� -> ���/�2
               (aprun->eddph == 0x0C) ? aprun->dPh_s * 101.9716 :
                                        // ��/�2 -> ���/�2
               (aprun->eddph == 0x09) ? aprun->dPh_s * 10.0 :
                                        // ���/�2 -> ���/�2
               (aprun->eddph == 0x0A) ? aprun->dPh_s * 10000.0 :
                aprun->dPh_s;              //0x82 0xEE: ���/�2 -> ���/�2
*/
            switch(ChkSensMax(3,pcalc->DP,prun->dPhmax)) {  //value
               case 0:  err = perr[9]; break;
               case 1:  err = perr[8]; break;
               default: err = pbuferr[8];
            }
            if (err != pbuferr[8]) {     // ����� ����襣�
               Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
               pbuferr[8] = err;         // �p�᢮��� ����� ���祭��
            }
                                         // ᤢ����� ��p����
            if (((prun->TypeRun) != 2)&&((prun->TypeRun) != 3))   //�����p�� ��p����
            {   switch(ChkSensMin(3,pcalc->DP,prun->dPhmin)) {
                  case 0:  err = perr[11]; break;
                  case 1:  err = perr[10]; break;
                  default: err = pbuferr[9];
               }
                                        // ����� ����襣�
               if ((err != pbuferr[9]) && (prun->dPhmin != 0.0)) {
                  Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
                  pbuferr[9] = err;     // �p�᢮��� ����� ���祭��
               }
            }  // if (((prun->TypeRun) != 2)&&((prun->TypeRun) != 3))
         } //if ((aprun->statdPh & 0x04) == 0)
      }   //if (prun->constdata & 1)
      else {  // ��p���� - ����⠭�
         err = (prun->constdP > prun->dPhmax) ? perr[8] : perr[9];
         if (err != pbuferr[8]) {       // ����� ����襣�
            Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
            pbuferr[8] = err;           // �p�᢮��� ����� ���祭��

         }
                                        // ᤢ����� ��p����
/**********************************************************************
         if (GetDpMinShelf() == 0) {    // �c�� ��� ��ᠤ�� �� dPmin
**********************************************************************/
            if (((prun->TypeRun) == 2) || ((prun->TypeRun) == 3)) {
               err = (prun->constdP < prun->dPhmin) ? perr[14] : perr[15];
                                        // ����� ����襣�
               if (err != pbuferr[11]) {
                  Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
                  pbuferr[11] = err;    // �p�᢮��� ����� ���祭��
               }
            }
            else {                      // �����p�� ��p����
               err = (prun->constdP < prun->dPhmin) ? perr[10] : perr[11];
               if (err != pbuferr[9]) { // ����� ����襣�
                  Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
                  pbuferr[9] = err;     // �p�᢮��� ����� ���祭��
               }
            }
      }
   }
                                        //-- ����� ��⠭���� �訡�� �� ���稪�� ��p�����

                                        //-- ��⠭���� �訡�� ��������� �� ��p������� ���稪� -------------
   if ((prun->TypeRun) == 4 || (prun->TypeRun) == 5 || (prun->TypeRun) == 10) {   // ᯮᮡ ����p���� �� ��⪥

      if (prun->constdata & 8) {        // ����� � ���稪�
      if  ((prun->TypeRun) == 10)
         Qru = IndQs[nRun];
         else
         Qru = GetQwRaw(nRun);
//            Qru =prun->Qw;
         switch(ChkSensMax(5,Qru,prun->Qmax)) {
            case 0:  err = perr[17]; break;
            case 1:  err = perr[16]; break;
            default: err = pbuferr[12];
         }
         if (err != pbuferr[12]) {      // ����� ����襣�
            Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
            pbuferr[12] = err;          // �p�᢮��� ����� ���祭��
         }
      if (startT >= 7)
      {
         switch(ChkSensMin(5,Qru,prun->Qmin)) {

            case 0:                     // ����� ���ਨ
               err = perr[19];
               break;
            case 1:                     // ��砫� ���ਨ
               err = perr[18];
               break;
            default: err = pbuferr[13];
         }
                                        // ����� ����襣�
         if ((err != pbuferr[13]) && (prun->Qmin > 0.0)) {
               Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
               pbuferr[13] = err;          // �p�᢮��� ����� ���祭��
         }
//      }
//printf("Qru=%f  Qw=%f\n",Qru,prun->Qw);
//        if (prun->Qw < prun->dPots) {     // ����� Qst
       if (Qru < prun->dPots) {     // ����� Qst
         if (QstErrFlag[nRun] == 0) {         // ���ਨ �� �� �뫮
            Mem_AlarmWrCodRun(nRun,0xB1,Mem_GetDayQ(nRun));
            QstErrFlag[nRun] = 1;             // ��⠭����� �ਧ���
           }
        }
//      if (prun->Qw >= prun->dPots) {    // ����� Qst
       if (Qru >= prun->dPots) {    // ����� Qst
         if (QstErrFlag[nRun] == 1) {         // ����� ����
            Mem_AlarmWrCodRun(nRun,0x31,Mem_GetDayQ(nRun));
            QstErrFlag[nRun] = 0;             // ���� ��
         }
      }
     } //if (startT >= 6*SysTakt)
   }
      else {                            // ����⠭�
         err = (prun->constqw > prun->Qmax) ? perr[16] : perr[17];
         if (err != pbuferr[12]) {      // ����� ����襣�
            Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
            pbuferr[12] = err;          // �p�᢮��� ����� ���祭��
         }
         err = (prun->constqw < prun->Qmin) ? perr[18] : perr[19];
         if (err != pbuferr[13]) {      // ����� ����襣�
               Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
               pbuferr[13] = err;       // �p�᢮��� ����� ���祭��
         }
        if (prun->constqw < prun->dPots) {     // ����� Qst
         if (QstErrFlag[nRun] == 0) {         // ���ਨ �� �� �뫮
            Mem_AlarmWrCodRun(nRun,0xB1,Mem_GetDayQ(nRun));
            QstErrFlag[nRun] = 1;             // ��⠭����� �ਧ���
         }
        }
//      }
        if (prun->constqw >= prun->dPots) {    // ����� Qst
          if (QstErrFlag[nRun] == 1) {         // ����� ����
            Mem_AlarmWrCodRun(nRun,0x31,Mem_GetDayQ(nRun));
            QstErrFlag[nRun] = 0;             // ���� ��
          }
        }
   }}                                    //-- ����� ��⠭���� �訡�� �� ��p������� ���稪� -------
                                        //-- ����p��� ᮮ⭮襭�� �p���� ���稪�� ��p����� � �p����� ��p����祭��
                                        // ᤢ����� ��p����
   if (((prun->TypeRun) ==2)||((prun->TypeRun) == 3)) {
      err = ( (prun->ValueSwitch >= prun->dPhmin) && (prun->ValueSwitch <= prun->ValueSwitch) ) ? perr[21] : perr[20];
      if (err != pbuferr[14]) {
         Mem_AlarmWrCodRun(nRun,err,Mem_GetDayQ(nRun));
         pbuferr[14] = err;             // �p�᢮��� ����� ���祭��
      }
   }
                                        //-- ����� ����p��� ᮮ⭮襭�� �p���� ���稪�� ��p����� � �p����� ��p����祭��

   return 0;
}
//---------------------------------------------------------------------------
/****************************************************************************
//��⠭���� ��p�� �訡��
void Mem_AlarmSetDataErr()
{ unsigned int    *perr;  //㪠��⥫� �� ���p �訡��
  struct IdRun    *prun;  //㪠��⥫� �� ��p����p� ��⪨
  float           value;
  unsigned int    type;

  perr = &buferr0[Time_GetCalcNumRun()][0];  //�訡�� �� ⥪�饬 ⠪� p�����
  prun = &GLCONFIG.IDRUN[Time_GetCalcNumRun()];
}
****************************************************************************/
//---------------------------------------------------------------------------
//��⠭���� ���p� 䨪�樨 �訡�� � "����訡�筮�" ���ﭨ�
void Mem_AlarmSetNoalarm()
{ unsigned int    *pbuferr1;  //㪠��⥫� �� ����� ���p� �訡��
  uint8_t i;
    /***********************
unsigned int          membuferrrun0[20];//�訡�� �� �p����饬 ⠪� p�����
unsigned int          membuferrrun1[20];//�訡�� �� �p����饬 ⠪� p�����
unsigned int          membuferrrun2[20];//�訡�� �� �p����饬 ⠪� p�����
unsigned int coderred[]={0xA7,0x27,0xA8,0x28,0xA9,0x29,0xAA,0x2A};
unsigned int coderrgr[]= {0xAB,0x2B,0xAC,0x2C,0xAD,0x2D,0xAE,0x2E,\
                          0xAF,0x2F,0xB0,0x30,0xB1,0x31,0xB2,0x32,\
                          0xB3,0x33,0xB4,0x34,0xB5,0x35};
                        ***************************************/
//    pbuferr1[18] = 0x05;  //���p殮��� ��⠭�� > 10,6 �
  for(i=0;i<4;i++)  membuferrrun0[i] = coderred[i*2+1];
  for(i=0;i<4;i++)  membuferrrun1[i] = coderred[i*2+1];
  for(i=0;i<4;i++)  membuferrrun2[i] = coderred[i*2+1];
  for(i=0;i<11;i++) membuferrrun0[i+4] = coderrgr[i*2+1];
  for(i=0;i<11;i++) membuferrrun1[i+4] = coderrgr[i*2+1];
  for(i=0;i<11;i++) membuferrrun2[i+4] = coderrgr[i*2+1];
}
//---------------------------------------------------------------------------
//������ ���� ���p�� � ��p��� ���p��
void Mem_AlarmWrCodHart(uint16_t cod, float value) {
    struct memalarm *pmemal;
    int numrun;

    numrun = Time_GetNumrun();

    pmemal = &MEMALARM;                 // ��p���p� ����� �p娢� ���p��
    pmemal->TIME = BINTIME;             // ����筠� ��p���p� �p�����
    pmemal->type = cod;
    pmemal->value = Mem_GetDayQ(numrun);// ���祭�� ��ꥬ� �� ⥪�騥 ��⪨

    pmemal->value = value;              // ���祭�� ��� ���� - ᢮�
                                        // ������ ���� ���p��
    Mem_AlarmWr(numrun,MEMALARM);
    //    Mem_AlarmWr(Mem_AlarmGetPage(numrun),0);
}
//---------------------------------------------------------------------------
//������ ���� ���p�� � ��p��� ���p��
//---------------------------------------------------------------------------
void Mem_AlarmWrCodRun(uint8_t Run, uint16_t cod, float value) {
   struct memalarm *pmemal;
//printf("Run=%d cod=%X value=%f\n",Run,cod,value);
   pmemal       = &MEMALARM;            // ��p���p� ����� �p娢� ���p��
   pmemal->TIME = BINTIME;              // ����筠� ��p���p� �p�����
   pmemal->type = cod;
   pmemal->value = value;               // ���祭�� ��� ���� - ᢮�

   Mem_AlarmWr(Run,MEMALARM);  // ������ ���� ���p��
//   SetValarmCount(cod,Run,value);       // ��⠭���� ���稪�� ���਩
}
//---------------------------------------------------------------------------
//������ ���� ���p�� � ��p��� ���p��
//---------------------------------------------------------------------------
void Mem_AlarmWrCod(uint8_t Run, uint16_t cod, char *b) {
   struct memalarmC *pmemal;
//printf("Run=%d cod=%X value=%f\n",Run,cod,value);
   pmemal       = &MEMALARMC;            // ��p���p� ����� �p娢� ���p��
   pmemal->TIME = BINTIME;              // ����筠� ��p���p� �p�����
   pmemal->type = cod;
   memcpy(pmemal->C,b,4);               // ���祭�� ��� ���� - ᢮�
   Mem_AlarmWr(Run,MEMALARM);  
     // ������ ���� ���p��
}
//-/-------------------------------------------------------------------------
//������ ���� ���p�� �� ��⠭�� � ��p���� ���p��
void Mem_AlarmWrCodPower(uint16_t cod)
{ struct memalarm *pmemal;
  int             numrun;
    pmemal = &MEMALARM;       //��p���p� ����� �p娢� ���p��
    pmemal->TIME = BINTIME;   //����筠� ��p���p� �p�����
    pmemal->type = cod;
    numrun = GLCONFIG.NumRun; //���-�� ��⮪
    for(int i=0;i<numrun;i++)
    { pmemal->value = Mem_GetDayQ(i); //���祭�� ��ꥬ� �� ⥪�騥 ��⪨
      Mem_AlarmWr(i,MEMALARM);   //������ ���� ���p��
    }
}
//---------------------------------------------------------------------------
uint16_t Mem_Crc16(uint8_t* buf, uint16_t count)
{
   uint16_t crc,bNeedXor;
   int i,j;

   crc = 0xFFFF;

   for (i=0; i<count; i++) {
      crc ^= buf[i];//buf[18]
      for (j=0; j<8; j++) {
         bNeedXor = crc & 0x0001;
         crc >>= 1;
         if (bNeedXor)
            crc ^= 0xA001;
      }
   }
   return crc;
}
//---------------------------------------------------------------------------
// �����頥� 1, �᫨ ���⮢�� �६� ����� � ��娢� (MEMDATA.STARTTIME)
// �ॢ�蠥� ����筮� �६� � ����� (STARTENDTIME.ENDTIME)
//
/*
int Mem_VTestReqTimeEnd(struct startendtime *STARTENDTIM) { //����p��� ᮮ⭮襭�� �p���� � �p娢� � ���p��
   long t1,t2;
if (ValarmData.STARTTIME.month==0) ValarmData.STARTTIME.month=1;
if (ValarmData.STARTTIME.date==0) ValarmData.STARTTIME.date=1;
   t1 = BinToSec(ValarmData.STARTTIME);
   t2 = BinToSec(STARTENDTIM->ENDTIME);
//printf("y=%d m=%d d=%d\n",STARTENDTIME.ENDTIME.year,STARTENDTIME.ENDTIME.month,STARTENDTIME.ENDTIME.date);
//printf("sy=%d sm=%d sd=%d\n",ValarmData.STARTTIME.year,ValarmData.STARTTIME.month,ValarmData.STARTTIME.date);
//printf("t1=%ld t2=%ld\n",t1,t2);
   if (t1 > t2)
      return 1;
   else
      return 0;
}
//---------------------------------------------------------------------------
// �����頥� 1, �᫨ ���⮢�� �६� ����� � ��娢� (MEMDATA_1.STARTTIME)
// �ॢ�蠥� ����筮� �६� � ����� (STARTENDTIME_1.ENDTIME)
//
int Mem_VTestReqTimeEnd_1() {//����p��� ᮮ⭮襭�� �p���� � �p娢� � ���p��
   long t1,t2;

   t1 = BinToSec(ValarmData_1.STARTTIME);
   t2 = BinToSec(STARTENDTIME_1.ENDTIME);

   if (t1 > t2)
      return 1;
   else
      return 0;

}
*/
//---------------------------------------------------------------------------
// ��� �����p������ ������ � ������� �������p� � ������ ��p����������� ������
void    Mem_StartPeriodHour(uint16_t numrun) //��pp����� �������p�� �p� ���p��
{
  struct tdata          *ptdata;//��������� �� ��p���������� �������p
  struct tdata          *phdata;//��������� �� ������� �������p
  struct memdata  *pmpd;  //��p����p� ������ ��p���-�� ������ - 26 ����

    ptdata = &TDATA;  //��������� �� ��p���������� �������p
    phdata = &HDATA;    //��������� �� ������� �������p
//    numrun = Time_GetNumrun(); //�⥭�� ⥪�饣� ����p� ��⪨
    Mem_Rdtdata(HOU,numrun,phdata);  // ������ �������p� �� ���
//    HDATA = TDATA;    //��� �㬬�p������ ������ �� ��
    Mem_Rdtdata(PER,numrun,ptdata);  //������ �������p� �� ��p���
//--------- ������: ���������� �p����� �������� � ��p���������� �������p� ----------
      if(ptdata->num)
      { if(!b_Massomer[numrun]) 
        { ptdata->p /= (float)ptdata->num; //�p����� ���祭��
          if (!b_Counter[numrun]) ptdata->dp_Qr /= (float)ptdata->num; //�p����� ���祭��
          ptdata->N2   /= (float)ptdata->num;
          ptdata->CO2  /= (float)ptdata->num;
          ptdata->Heat /= (float)ptdata->num;
        }
        ptdata->t      /= (float)ptdata->num; //�p����� ��������                                       
        ptdata->RO   /= (float)ptdata->num;
      }
      else
      {
         return;
/*
         ptdata->p      = 0.0;  //�p����� ���祭��
         ptdata->dp     = 0.0;  //�p����� ���祭��
         ptdata->t      = 0.0;  //�p����� ���祭��
         ptdata->q      = 0.0;  //�㬬�
*/
      }
//--------- �����: ���������� �p����� �������� ----------
    pmpd = &MEMDATA;//��p����p� ������ ��p����������� �����
    pmpd->STARTTIME = ptdata->STARTTIME;
    pmpd->stat    = ptdata->stat;   
      if(ptdata->seconds % 60)
      { pmpd->count = ptdata->seconds / 60;
        pmpd->count++;  //�p��� ���������� � �������
      }
      else  pmpd->count = ptdata->seconds / 60;  //�p��� ���������� � �������
    pmpd->p       = ptdata->p;  //�p����� ��������
    pmpd->dp_Qr      = ptdata->dp_Qr;  //�p����� ��������
    pmpd->t       = ptdata->t;   //�p����� ��������
    pmpd->q       = ptdata->q;  //�����
    pmpd->FullHeat = ptdata->FullHeat;   // �����

    pmpd->RO       = ptdata->RO;         // �p����� ���祭��
    pmpd->N2      = ptdata->N2;        // �p����� ���祭��
    pmpd->CO2     = ptdata->CO2;       // �p����� ���祭��
    pmpd->Heat     = ptdata->Heat;       // �p����� ���祭��
/**/
    ConstCorr(P, &pmpd->p, CONFIG_IDRUN[numrun].constdata);
//    ConstCorr((b_Counter[numrun]) ? QW : DP,
//       &pmpd->dp_Qr, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(T, &pmpd->t, CONFIG_IDRUN[numrun].constdata);

    ConstCorr(RO,   &pmpd->RO,   CONFIG_IDRUN[numrun].constdata);
    ConstCorr(N2,  &pmpd->N2,  CONFIG_IDRUN[numrun].constdata);
    ConstCorr(CO2, &pmpd->CO2, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(Heat, &pmpd->Heat, CONFIG_IDRUN[numrun].constdata);
/**/
//-------------------------------------------------
    if(phdata->num == 0)//��p��� ������ � ������� �������p
    {
      phdata->STARTTIME = ptdata->STARTTIME;

 // ��������������� ������ ��������� ����������
      // �� ��������������� �������� ������� ���������� ������� ������      phdata->STARTTIME.seconds = 0;
      phdata->STARTTIME.minutes = 0;

      phdata->stat   = ptdata->stat;   
      phdata->seconds= ptdata->seconds;
      phdata->p      = ptdata->p;
      phdata->dp_Qr     = ptdata->dp_Qr;
      phdata->t      = ptdata->t;
      phdata->q      = ptdata->q;
      phdata->FullHeat = ptdata->FullHeat;

      phdata->RO       = ptdata->RO;
      phdata->N2      = ptdata->N2;
      phdata->CO2     = ptdata->CO2;
      phdata->Heat     = ptdata->Heat;
    }
    else  //������ �p��� ��p���
    { phdata->stat   |= ptdata->stat;
      phdata->seconds+= ptdata->seconds;
      phdata->p      += ptdata->p;  //�p����� ���祭��
      phdata->dp_Qr     += ptdata->dp_Qr; //�p����� ���祭��
      phdata->t      += ptdata->t;  //�p����� ���祭��
      phdata->q      += ptdata->q;  //�㬬�
      phdata->FullHeat += ptdata->FullHeat;

      phdata->RO     += ptdata->RO;     // �p����� ���祭��
      phdata->N2    += ptdata->N2;    // �p����� ���祭��
      phdata->CO2   += ptdata->CO2;   // �p����� ���祭��

      phdata->Heat   += ptdata->Heat;   // �p����� ���祭��
    }
      phdata->num++;      //���-�� ����p����� ���祭��
      phdata->ENDTIME   = ptdata->ENDTIME;;
      ptdata->num = 0;    //���-�� ����p����� ���祭��
      ptdata->q   = 0.0;  //⥪�饥 ���祭��
      ptdata->FullHeat   = 0.0;            // ⥪�饥 ���祭��
/*!!!*/
        if(b_Counter[numrun]) ptdata->dp_Qr = 0.0;
/*!!!*/
    TDATA.checksum = Mem_Crc16((uint8_t*)&TDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(PER,numrun,TDATA);//������ ��p�����᪮�� �㬬��p� - ���-�� ���.=0
 //   TDATA = HDATA;
    HDATA.checksum = Mem_Crc16((uint8_t*)&HDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(DAY,numrun,HDATA);//������ �ᮢ��� �㬬��p�
//  pmpd->checksum = Mem_Crc16(pmpd,sizeof(struct memdata)-2);//����p��쭠� �㬬�
    MemdataWr(PER,numrun,&pArchiv.di1p[numrun],&pArchiv.di2p[numrun],MEMDATA);//������ ��p�����᪮�� ����
}
//---------------------------------------------------------------------------
// ���४�� �ᮢ��� � ���筮�� �㬬��஢ � ������ � �ᮢ�� ����
//
void Mem_StartHourDay(uint8_t numrun)
{

  struct tdata    *ptdata;//㪠��⥫� �� �ᮢ�� �㬬��p
  struct tdata    *phdata;//㪠��⥫� �� ����� �㬬��p
  struct memdata  *pmpd;  //��p���p� ����� �ᮢ��� ���� - 26 ����

    ptdata = &TDATA;  //㪠��⥫� �� �ᮢ�� �㬬��p
    phdata = &HDATA;  //㪠��⥫� �� ����� �㬬��p
//    numrun = Time_GetNumrun(); //�⥭�� ⥪�饣� ����p� ��⪨
    Mem_Rdtdata(DAY,numrun,ptdata);  //type=(0-2) �⥭�� �㬬��p� �� ��⪨
//    HDATA = TDATA;    //��� �㬬�p������ ������ �� ��⪨
    Mem_Rdtdata(HOU,numrun,phdata);  //type=(0-2) �⥭�� �㬬��p� �� ��
//--------- ��砫�: �����⮢�� �p����� ���祭�� � �ᮢ�� �㬬��p� ----------
      if(ptdata->num)
      { if(!b_Massomer[numrun]) {
       ptdata->p      /= (float)ptdata->num; //�p����� ���祭��
          if (!b_Counter[numrun])
           ptdata->dp_Qr /= (float)ptdata->num; //�p����� ���祭��
        ptdata->N2    /= (float)ptdata->num;
//    if (((GLCONFIG.IDRUN[numrun].TypeRun) != 4) && ((GLCONFIG.IDRUN[numrun].TypeRun) != 5)
//          && ((GLCONFIG.IDRUN[numrun].TypeRun) != 10))
        ptdata->CO2   /= (float)ptdata->num;
        ptdata->Heat   /= (float)ptdata->num;
       }
        ptdata->t      /= (float)ptdata->num; //�p����� ���祭��
        ptdata->RO     /= (float)ptdata->num;
      }
      else
      {
         return;
      }
//--------- �����: �����⮢�� �p����� ���祭�� ----------
    pmpd = &MEMDATA;  //��p���p� ����� �ᮢ��� ���� - 26 ����
    pmpd->STARTTIME = ptdata->STARTTIME;
    pmpd->stat    = ptdata->stat;     //����� ��室��� ������ � p��室�
      if(ptdata->seconds % 60)
      { pmpd->count = ptdata->seconds / 60;
        pmpd->count++;  //�p��� ���������� � ������
      }
      else  pmpd->count = ptdata->seconds / 60;  //�p��� ���������� � ������
    pmpd->p       = ptdata->p;  //�p����� ���祭��
    pmpd->dp_Qr      = ptdata->dp_Qr; //�p����� ���祭��
    pmpd->t       = ptdata->t;  //�p����� ���祭��
    pmpd->q       = ptdata->q;  //�㬬�

    pmpd->RO       = ptdata->RO;  //�p����� ���祭��
    pmpd->N2      = ptdata->N2; //�p����� ���祭��
    pmpd->CO2     = ptdata->CO2;  //�p����� ���祭��
    pmpd->Heat     = ptdata->Heat;  //�p����� ���祭��
    pmpd->FullHeat = ptdata->FullHeat;  //�㬬�
/**/
    ConstCorr(P, &pmpd->p, CONFIG_IDRUN[numrun].constdata);
//    ConstCorr((b_Counter) ? QW : DP,
//       &pmpd->dp_Qr, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(T, &pmpd->t, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(RO,  &pmpd->RO,   CONFIG_IDRUN[numrun].constdata);
    ConstCorr(N2,  &pmpd->N2,  CONFIG_IDRUN[numrun].constdata);
    ConstCorr(CO2, &pmpd->CO2, CONFIG_IDRUN[numrun].constdata);
    ConstCorr(Heat, &pmpd->Heat, CONFIG_IDRUN[numrun].constdata);
/**/
    if(phdata->num == 0)//��p��� ������ � ����� �㬬��p
    {
      phdata->STARTTIME = ptdata->STARTTIME;

      // ᪮�४�஢��� ��砫� ���ࢠ�� ����������
      // �� �।�����饣� ��⭮�� ������ ���������� ������ ������
//      TimeCorrectionForDay(&phdata->STARTTIME, GLCONFIG.ContrHour);

      phdata->stat   = ptdata->stat;   //����� ��室��� ������ � p��室�
      phdata->p      = ptdata->p;
      phdata->dp_Qr     = ptdata->dp_Qr;
      phdata->t      = ptdata->t;
      phdata->q      = ptdata->q;

      phdata->RO       = ptdata->RO;
      phdata->N2      = ptdata->N2;
      phdata->CO2     = ptdata->CO2;
      phdata->Heat     = ptdata->Heat;
      phdata->FullHeat = ptdata->FullHeat;
    }
    else  //����� �p��� ��p���
    { phdata->stat   |= ptdata->stat; //����� ����� ���筮�� ����
      phdata->p      += ptdata->p;  //�p����� ���祭��
      phdata->dp_Qr     += ptdata->dp_Qr; //�p����� ���祭��
      phdata->t      += ptdata->t;  //�p����� ���祭��
      phdata->q      += ptdata->q;  //�㬬�

      phdata->RO       += ptdata->RO;   // �p����� ���祭��
      phdata->N2      += ptdata->N2;  // �p����� ���祭��
      phdata->CO2     = CONFIG_ADRUN[numrun].Vtot_l;
      phdata->Heat     += ptdata->Heat; // �p����� ���祭��
                                        // �㬬�
      phdata->FullHeat += ptdata->FullHeat;
    }
      phdata->num++;       //���-�� ����p����� ���祭��
      phdata->ENDTIME   = ptdata->ENDTIME;
      ptdata->num = 0;
      ptdata->q   = 0.0;
      ptdata->FullHeat = 0.0;

/*!!!*/
      if(b_Counter[numrun])  ptdata->dp_Qr = 0.0;
/*!!!*/
    TDATA.checksum = Mem_Crc16((uint8_t*)&TDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(HOU,numrun,TDATA);//������ �ᮢ��� �㬬��p� - ���-�� ���.=0
//    TDATA = HDATA;
    HDATA.checksum = Mem_Crc16((uint8_t*)&HDATA,sizeof(struct tdata)-2);//����p��쭠� �㬬�
    Mem_Wrtdata(DAY,numrun,HDATA);                      //������ ���筮�� �㬬��p�
//  pmpd->checksum = Mem_Crc16(pmpd,sizeof(struct memdata)-2);//����p��쭠� �㬬�
    MemdataWr(HOU,numrun,&pArchiv.di1h[numrun],&pArchiv.di2h[numrun],MEMDATA);//������ ��p�����᪮�� ����

    }
//---------------------------------------------------------------------------
// ���४�� ���筮�� � ��饣� �㬬��஢ � ������ � ����� ����
//
void Mem_StartDayMonth(struct bintime BTime, uint8_t numrun) {

   struct tdata    *ptdata;   // 㪠��⥫� �� �ᮢ�� �㬬��p
   struct tdata    *phdata;   // 㪠��⥫� �� ����� �㬬��p
   struct memdata  *pmpd;     // ��p���p� ����� ���� - 26 ����
   long  ValarmMin, lTmp;
   struct bintime binTmp;
   ptdata = &TDATA;           // 㪠��⥫� �� ����� �㬬��p

//   numrun = Time_GetNumrun(); // �⥭�� ⥪�饣� ����p� ��⪨

   Mem_Rdtdata(DAY,numrun,ptdata);     // type=(0-2) �⥭�� �㬬��p� �� ��⪨

   //--------- ��砫�: �����⮢�� �p����� ���祭�� � ���筮� �㬬��p� ----------
   if (ptdata->num) {
      if(!b_Massomer[numrun]) {
      ptdata->p /= (float)ptdata->num;      // �p����� ���祭��
      if (!b_Counter[numrun])
         ptdata->dp_Qr /= (float)ptdata->num;  // �p����� ���祭��
      ptdata->N2    /= (float)ptdata->num; // �p����� ���祭��
    if (!b_Counter[numrun])
      ptdata->CO2   /= (float)ptdata->num; // �p����� ���祭��
      ptdata->Heat   /= (float)ptdata->num; // �p����� ���祭��
      }
      ptdata->t      /= (float)ptdata->num; // �p����� ���祭��
      ptdata->RO     /= (float)ptdata->num; // �p����� ���祭��
   }
   else
      return;
   //--------- �����: �����⮢�� �p����� ���祭�� ----------

   pmpd = &MEMDATA;//��p���p� ����� ���� - 26 ����

   pmpd->STARTTIME = ptdata->STARTTIME;
   pmpd->stat    = ptdata->stat; // ����� ��室��� ������ � p��室�
   pmpd->p       = ptdata->p;    // �p����� ���祭��
   pmpd->dp_Qr      = ptdata->dp_Qr;   // �p����� ���祭��
   pmpd->t       = ptdata->t;    // �p����� ���祭��

   pmpd->RO      = ptdata->RO;          // �p����� ���祭��
   pmpd->N2     = ptdata->N2;         // �p����� ���祭��
   pmpd->CO2    = ptdata->CO2;        // �p����� ���祭��
   pmpd->Heat    = ptdata->Heat;        // �p����� ���祭��

   ConstCorr(P, &pmpd->p, CONFIG_IDRUN[numrun].constdata);
//   ConstCorr((b_Counter[numrun]) ? QW : DP,
//      &pmpd->dp_Qr, CONFIG_IDRUN[numrun].constdata);
   ConstCorr(T, &pmpd->t, CONFIG_IDRUN[numrun].constdata);
   ConstCorr(RO,  &pmpd->RO,   CONFIG_IDRUN[numrun].constdata);
   ConstCorr(N2,  &pmpd->N2,  CONFIG_IDRUN[numrun].constdata);
    if (!b_Counter[numrun])
   ConstCorr(CO2, &pmpd->CO2, CONFIG_IDRUN[numrun].constdata);
   ConstCorr(Heat, &pmpd->Heat, CONFIG_IDRUN[numrun].constdata);

   pmpd->q       = ptdata->q;    // �㬬� �� ��⪨
   pmpd->FullHeat = ptdata->FullHeat;   // �㬬� �� ��⪨

//   Mem_DayStereoCountAlarm(pmpd->dp,numrun);

   Mem_CheckMonth(BTime);    // ��᫥���� ������ �㬬����

   ptdata->num = 0;
   ptdata->q   = 0.0;
   ptdata->FullHeat = 0.0;

   if (b_Counter[numrun])   ptdata->dp_Qr = 0.0;
                                 // ����p��쭠� �㬬�
   ptdata->checksum = Mem_Crc16((uint8_t*)ptdata,sizeof(struct tdata)-2);

   Mem_Wrtdata(DAY,numrun,TDATA);        // ������ ���筮�� �㬬��p�
                                 // ����p��쭠� �㬬�
// pmpd->checksum = Mem_Crc16(pmpd,sizeof(struct memdata)-2);
                                 // ������ ���筮�� ����
   Mem_DayWr(numrun);   
   //MemdataWr(DAY,numrun,&pArchiv.di1d[numrun],&pArchiv.di2d[numrun],MEMDATA);
// ���਩�� ��ꥬ�
/*
      outportb(0x1D0,BASE_CFG_PAGE);    // ����� ��࠭��� ���䨣��樨
                                        // �஢�ઠ �� ������ � ��娢
                                        //  ���.����. ���਩ ��� �� V����.- ᥪ㭤�
      ValarmMin = INTPAR.AlarmVolumeMinTime;

                                        // �᫨ ��饥 �६� ���਩ > ���������
                                        // �� 㬮�砭�� 60 ᥪ.
                                        // ��� �뫨 �몫�祭��
      if (((ValarmSumm+numrun)->TTot+(ValarmSumm2+numrun)->TTot) > ValarmMin ||
          ((ValarmSumm+numrun)->Tqmin+(ValarmSumm2+numrun)->Tqmin) > ValarmMin ||
          ((ValarmSumm+numrun)->Toff+(ValarmSumm2+numrun)->Toff) > ValarmMin
         )                              // ������� � ��娢
       {  if (((ValarmSumm+numrun)->VTotSt+(ValarmSumm2+numrun)->VTotSt) >pmpd->q)
           {  (ValarmSumm+numrun)->VTotSt = pmpd->q;
              (ValarmSumm2+numrun)->VTotSt = 0;
               if ((GLCONFIG.IDRUN[numrun].TypeRun) == 4
               || (GLCONFIG.IDRUN[numrun].TypeRun) == 5)
              {(ValarmSumm+numrun)->VTotWc = pmpd->dp;
              (ValarmSumm2+numrun)->VTotWc = 0;}
           }
         WriteValarmData(GetValarmPage(numrun),numrun);
        }
      lTmp = BinToSec(BINTIME)+4L;
      SecToBin(&lTmp,&binTmp);
      ValarmSummIni(binTmp,numrun);
*/
}
//---------------------------------------------------------------------------
void Mem_AlarmSetPowerOff() {
   struct bintime WriteTime; 
//       CycleTime,                   // �� 横����� ������
//           PeriodTime,                  // �� ᥪ㭤���� �㬬���
//           HourTime,                    // �� �ᮢ��� �㬬���
                              // �६�, �����뢠���� � ��娢
//   time_t  tCyc,tPer,tHou;              // �� �� �६��� � ᥪ㭤��
//   int k,j;
   uint8_t i;

   for (i=0;i<GLCONFIG.NumRun;i++) {              // ����筠� ��p���p� �p�����    
      memcpy(&MEMALARM.TIME,&POWER_OFF_TIME,6);
      MEMALARM.type = 0x87;             // ⨯ ���p�� - �몫�祭�� ��⠭��
      MEMALARM.value = Mem_GetDayQ(i);
     Mem_AlarmWr(i,MEMALARM);                                    // ������ ���� ���p��      Mem_AlarmWr(i,MEMALARM);  
   }
}
//---------------------------------------------------------------------------
void Mem_AlarmSetPowerOn() {
   uint8_t k;
   for (k=0;k<GLCONFIG.NumRun;k++) {    // ����筠� ��p���p� �p�����
       memcpy(&MEMALARM.TIME,&BINTIME,6); 
      MEMALARM.type = 0x07;   // ⨯ ���p�� - ����祭�� ��⠭��
      MEMALARM.value = Mem_GetDayQ(k);
      Mem_AlarmWr(k,MEMALARM);   //������ ���� ���p��
   }
}
//---------------------------------------------------------------------------

void ReadMonthSumm (unsigned char nRun) {
                                       // �ᯮ������� �㬬��஢ � ���
}


//---------------------------------------------------------------------------
/*
void WriteMonthSumm(unsigned char nRun) {

   outportb(0x1D0,BASE_CFG_PAGE);       // ��⠭���� ��p����� �����

   FP_SEG(MS) = SEGMENT;
   FP_OFF(MS) = 5120+128*nRun;

   _fmemcpy(MS,(char *)&MonthSumm[nRun],128);
}
*/
//---------------------------------------------------------------------------
void Mem_AddToMonth () {
   int  nRun;

   nRun = Time_GetNumrun();
   ReadMonthSumm(nRun);                 // �⥭�� ������� �㬬��஢
                                        // �������� ��।�� ��⪨
   MonthSumm[nRun].MonthV     += TDATA.q;
   MonthSumm[nRun].MonthFHeat += TDATA.FullHeat;

   if (BINTIME.date==1) {               // �᫨ 1 �᫮
                                        // ������� �㬬� � �।��騩 �����
      MonthSumm[nRun].PrevMonthV     = MonthSumm[nRun].MonthV;
      MonthSumm[nRun].PrevMonthFHeat = MonthSumm[nRun].MonthFHeat;

      MonthSumm[nRun].MonthV     = 0.0; // ���㫨�� �㬬� ⥪�饣� �����
      MonthSumm[nRun].MonthFHeat = 0.0;
   }
//   WriteMonthSumm(nRun);                // ������� �㬬����
}
//---------------------------------------------------------------------------
void Mem_CheckMonth (struct bintime BTime) {
   unsigned char nRun;                  // ��⪠


   nRun = Time_GetNumrun();             // �⥭�� ⥪�饣� ����p� ��⪨

   ReadMonthSumm(nRun);                 // �⥭�� ������� �㬬��஢
                                        // �������� ��।�� ��⪨
   MonthSumm[nRun].MonthV     += TDATA.q;
   MonthSumm[nRun].MonthFHeat += TDATA.FullHeat;

                                        // �᫨ ���. ��᫥ 1-�� � �몫. � �।�. �����
                                        // �몫. 30.05.02 - ���. 2.06.02
                                        // �몫. 30.05.02 - ���. 13.06.02 (� ����� ����)
   if (((BINTIME.date > 1) && (BTime.month < BINTIME.month) && (BTime.year == BINTIME.year)) ||
                                        // �몫. 30.05.02 - ���. 2.02.03 (� ࠧ��� �����)
      ((BINTIME.date > 1) && (BTime.year < BINTIME.year)) ||
                                        // �몫. 30.05.02 - ���. 1.06.02 � 10.36
       ((BINTIME.date == 1) && (BINTIME.hours >= GLCONFIG.ContrHour)))
     {
      int month = (BINTIME.month >= BTime.month) ? BINTIME.month : BINTIME.month + 12;
                                        // ࠧ��� ����� ������ �����
      if (((month - BTime.month) > 1) &&
         ((BINTIME.date >1) || ((BINTIME.date == 1) && (BINTIME.hours >= GLCONFIG.ContrHour)))) {
                                        // ���㫨�� �㬬� �।��饣� �����
         MonthSumm[nRun].PrevMonthV     = 0.0;
         MonthSumm[nRun].PrevMonthFHeat = 0.0;
      }
      else {                            // ������� �㬬� � �।��騩 �����
         MonthSumm[nRun].PrevMonthV     = MonthSumm[nRun].MonthV;
         MonthSumm[nRun].PrevMonthFHeat = MonthSumm[nRun].MonthFHeat;
      }
      MonthSumm[nRun].MonthV     = 0.0; // ���㫨�� �㬬� ⥪�饣� �����
      MonthSumm[nRun].MonthFHeat = 0.0;
   }

//   WriteMonthSumm(nRun);                // ������� �㬬����
}
//------------------------------------------------
float GetVm (unsigned char nRun) {
   float q;

   ReadMonthSumm(nRun);                 // �⥭�� ������� �㬬��஢

   if (((BINTIME.date == 1) && (BINTIME.hours >= GLCONFIG.ContrHour)) ||
       ((BINTIME.date == 2) && (BINTIME.hours < GLCONFIG.ContrHour)))
      q = MonthSumm[nRun].PrevMonthV;   // �।��騩 �����
   else
      q = MonthSumm[nRun].MonthV;       // ⥪�騩 �����

   return q;
}

//-------------------------------------------------
/*
void Mem_AlarmHartComOn()  {  // ��砫� ०��� HART-COM
int num;
   for (int i=0;i<GLCONFIG.NumRun;i++) {    // ����筠� ��p���p� �p�����
      MEMALARM.TIME = BINTIME;
      MEMALARM.type = 0x83;   // ⨯ ���p�� - ��砫� ०��� HART-COM
      MEMALARM.value = Mem_GetDayQ((unsigned char)i);
      Mem_AlarmWr(Mem_AlarmGetPage((unsigned char)i),0); //������ ���� ���p��
 }
      if (MEMSECUR.Port == 1)
      { strncpy(MEMSECUR.login,Tlogin1,4);
	MEMSECUR.TIMEBEG = BINTIME;
	memset(&MEMSECUR.TIMEEND,0,6);
	HARTTimeb1 = BINTIME;
	MEMSECUR.mode  = 11;
//	MEMSECUR.Port = 1;
      }
      else if (MEMSECUR.Port == 2)
      { strncpy(MEMSECUR.login,Tlogin,4);
	MEMSECUR.TIMEBEG = BINTIME;
	memset(&MEMSECUR.TIMEEND,0,6); //MEMSECUR.TIMEEND = BINTIME;
	HARTTimeb = BINTIME;
	MEMSECUR.mode  = 11;
      }
      num = Mem_SecurWr_2(Mem_SecurGetPage(),3,MEMSECUR.Port);
}

//-------------------------------------------------
void Mem_AlarmHartComOff() {  // ����� ०��� HART-COM
int num;
   for (int i=0;i<GLCONFIG.NumRun;i++) {    // ����筠� ��p���p� �p�����
      MEMALARM.TIME = BINTIME;
      MEMALARM.type = 0x03;   // ⨯ ���p�� - ����� ०��� HART-COM
      MEMALARM.value = Mem_GetDayQ((unsigned char)i);
      Mem_AlarmWr(Mem_AlarmGetPage((unsigned char)i),0); //������ ���� ���p��
    }
      if (MEMSECUR.Port == 1)
      { strncpy(MEMSECUR.login,Tlogin1,4);
	MEMSECUR.TIMEBEG = HARTTimeb1;
	MEMSECUR.TIMEEND = BINTIME;
	MEMSECUR.mode  = 11;
//	MEMSECUR.Port = 1;
      }
      else
      { strncpy(MEMSECUR.login,Tlogin,4);
	MEMSECUR.TIMEBEG = HARTTimeb;
	MEMSECUR.TIMEEND = BINTIME;
	MEMSECUR.mode  = 11;
	MEMSECUR.Port = 2;
      }
       num = Mem_SecurWr_2(Mem_SecurGetPage(),4,MEMSECUR.Port);
//printf("num=%d hour=%d min=%d sec=%d\n",num,BINTIME.hours, BINTIME.minutes, BINTIME.seconds);
}
*/
//------------------------------------------------------
void IniChkCfg() { // ��砫쭠� ��⠭���� ����஫� ��࠭���� ���䨣��権 � ���
   PrevHour = sTime.Hours;
   CurrHour = PrevHour;                 // �ࠢ���� ��� ���祭��
}

//------------------------------------------------------
void ChkCfg() {                         // ����஫� ��࠭���� ���䨣��権 � ���
   unsigned char Val1,Val2;             // १���� �⥭�� ���䨣��権
   
   CurrHour = sTime.Hours;        // ������ �⤥�쭮 ���
   if (CurrHour != PrevHour) {          // ��� �� ᮢ����
      Mem_ConfigRdChk(1);               // ������ �᭮���� �� ���
//      if (RES_GLCONFIG.crc == Mem_Crc16(&RES_GLCONFIG,sizeof(struct glconfig)-2))
         Val1 = 1;                      // ��ਠ
//      else                              // �㬬� �� ᮢ����
//         Val1 = 0;                      // �� ��ଠ

      Mem_ConfigRdChk(2);               // ������ १�ࢭ�� �� ���
//      if (RES_GLCONFIG.crc == Mem_Crc16(&RES_GLCONFIG,sizeof(struct glconfig)-2))
         Val2 = 1;                      // ��ਠ
//      else                              // �㬬� �� ᮢ����
//         Val2 = 0;                      // �� ��ଠ
// if ((Val1 + Val2) < 2)
//printf("H=%X Val1=%d Val2=%d\n",CurrHour,Val1,Val2);
      if (Val1 != Val2) {               // �᫨ ���� �� � ��ଥ
         if (Val1 == 1) {               // ��ଠ�쭠� - �᭮����
            Mem_ConfigRdChk(1);         // ������ �᭮���� �� ���
            Mem_ConfigWrChk(2);         // ������� १�ࢭ�� � ���
         }
         if (Val2 == 1) {               // ��ଠ�쭠� - १ࢭ��
            Mem_ConfigRdChk(2);         // ������ १�ࢭ�� �� ���
            Mem_ConfigWrChk(1);         // ������� �᭮���� � ���
         }
      }                                 // �᫨ ��� � ��ଥ ��� ��� � ����ଥ
                                        // � ��祣� �� ������

      PrevHour = CurrHour;              // ��������� �।��饥
   }
}
//---------------------------------------------

uint16_t ChkSensMax (uint16_t Sens, float Val, float Max)
{
   float Delta;

   switch (Sens) {
      case 1:           // ⥬������
         Delta = 0.3;
         break;
      case 2:           // ��������
         Delta = 0.12;
         break;
      case 3:           // ���孨� ��९��
         Delta = 8.0;
         break;
      case 4:           // ������ ��९��
         Delta = 2.0;
         break;
      case 5:           // ���稪  Q�� max
         Delta = 2.0;
         break;
   }

   if (Val > Max)
      return 1;

   if (Val < (Max - Delta))
      return 0;
   else
      return 2;
}
//---------------------------------------------
uint16_t ChkSensMin (uint16_t Sens, float Val, float Min)
{
   float Delta;

   switch (Sens) {
      case 1:           // ⥬������
         Delta = 0.3;
         break;
      case 2:           // ��������
         Delta = 0.04;
         break;
      case 3:           // ���孨� ��९��
         Delta = 1.0;
         break;
      case 4:           // ������ ��९��
         Delta = 0.8;
         break;
      case 5:           // ���稪   Qmin
         Delta = 1.0;
         break;
      case 6:           // ���窠
         if (Min < 1.0)  Delta = 0.0;
         else Delta = 0.5;
         break;
   }

   if (Val < Min)
      return 1;

   if (Val > (Min + Delta))
      return 0;
   else
      return 2;
}
//---------------------------------------------------------------------------
int GetFullVolFlag() {                  // �ਧ��� ������� ��ꥬ� � ��娢��
   int iTmp;

   iTmp = INTPAR.FullNormVolume;        // �ਧ��� ������� ��ꥬ� � ��娢��

   return iTmp;
}
//---------------------------------------------------------------------------
/*
void Mem_IntParRd(unsigned int adz) {
   char far *IntPars;

//   disable();                           // ���p���� �p�p뢠���

   outportb(0x1d0,BASE_CFG_PAGE);       // ��⠭����� ��p����� �����
   IntPars = (char far *)MK_FP(SEGMENT,adz);
asm cli                                        // ������ �� ��� � ���
   _fmemcpy((char far *)&INTPAR,IntPars,sizeof(struct intpar));
asm sti
//   enable();                            // p��p���� �p�p뢠���
}
//---------------------------------------------------------------------------
void Mem_IntParWr() {
   char far *IntPars;
//   disable();                           // ���p���� �p�p뢠���
   outportb(0x1d0,BASE_CFG_PAGE);       // ��⠭����� ��p����� �����
   INTPAR.crc = Mem_Crc16(&INTPAR,sizeof(struct intpar)-2);
   IntPars = (char far *)MK_FP(SEGMENT,3584);
asm cli                                        // ������� �� ��� � ���
   _fmemcpy(IntPars,(char far *)&INTPAR,sizeof(struct intpar));
asm sti
   IntPars = (char far *)MK_FP(SEGMENT,3630);
asm cli                        // ������� १�ࢭ�� ����� �� ��� � ���
   _fmemcpy(IntPars,(char far *)&INTPAR,sizeof(struct intpar));
asm sti
//   enable();                            // p��p���� �p�p뢠���
}
*/
//---------------------------------------------------------------------------
/*
void  Mem_LoginRd() {
   char far *LgPars;

   outportb(0x1d0,RESERV_CFG_PAGE);       // ��⠭����� ��p����� �����
   LgPars = (char far *)MK_FP(SEGMENT,4096);
asm cli                                        // ������� �� ��� � ���
   _fmemcpy((char far *)LogPassw,LgPars,sizeof(LogPassw));
asm sti
}
//---------------------------------------------------------------------------
void  Mem_LoginWr() {
   char far *LgPars;

   outportb(0x1d0,RESERV_CFG_PAGE);       // ��⠭����� ��p����� �����
   LgPars = (char far *)MK_FP(SEGMENT,4096);
asm cli                                        // ������� �� ��� � ���
   _fmemcpy(LgPars,(char far *)LogPassw,sizeof(LogPassw));
asm sti
}
//---------------------------------------------------------------------------
					// ������ ���� ������᭮��
unsigned int _fastcall Mem_SecurWr_2(unsigned char numpage,unsigned char r, char nPort) {
   unsigned int C;


//printf("di1=%d di2=%d d10=%d d20=%d d11=%d d21=%d\n",di1,di2,di1p[0][0],di2p[0][0],di1p[1][0],di2p[1][0]);
   if ((r==1)||(r==3)||(r==5))
   { asm   mov ax,word ptr es:[di]
     C = _AX;
     if (nPort == 2)
     di1p[1][(r-1)/2] = C;
     else
     di1p[0][(r-1)/2] = C;
     asm   mov ax,word ptr es:[di+2]
     C = _AX;
     if (nPort == 2)
     di2p[1][(r-1)/2] = C;
     else
     di2p[0][(r-1)/2] = C;
   }
   else if ((r==2)||(r==4)||(r==6))  // ����� ᥠ�� �裡
   {
     if (nPort == 2)
     C=di1p[1][(r/2)-1];
     else
     C=di1p[0][(r/2)-1];
     asm   mov ax,C   //di1p[2][(r/2)-1]
     asm   mov word ptr es:[di],ax
     if (nPort == 2)
     C=di2p[1][(r/2)-1];
     else
     C=di2p[0][(r/2)-1];
     asm   mov ax,C   //di2p[1][r/2-1]
     asm   mov word ptr es:[di+2],ax
   }
   asm   mov ax,ARCH_SECURITY-1            // ���ᨬ��쭮� ���祭�� ᬥ饭�� = 599
   asm   cmp ax,word ptr es:[di+2]      // �p���. 㪠��⥫� �� ���. ����ᠭ�� �������
   asm   jnc m1                         // �᫨ next  > 599
   asm   mov word ptr es:[di],0         // �᫨ ����� ���ᨬ� - 㪠��⥫� �� ��p�� ����ᠭ�� �������
   asm   mov word ptr es:[di+2],0       // ��᫥ ���樠����樨 - 㪠��⥫� �� ��᫥���� ����ᠭ�� �������
// asm   mov ax,word ptr es:[di]
// asm   mov bx,word ptr es:[di+2]      // 㪠��⥫� �� ������� ��� �����
//printf("di1=%d d2=%d\n",_AX,_BX);
   asm   jmp m3
m1:
   asm   jnz m2
   asm   mov word ptr es:[di+2],0       // �᫨ next  == 599, next  = 0
   asm   mov word ptr es:[di],1         // first == 1
   asm   jmp m3
m2:                                     // next < 599
   asm   mov ax,ARCH_SECURITY-1
   asm   mov dx,word ptr es:[di]        // 㪠��⥫� �� ��p��
   asm   cmp word ptr es:[di+2],dx      // �p������ 㪠��⥫� �� ��᫥���� � ��p��
   asm   jnb m4
   asm   mov word ptr es:[di+2],dx      // �� ��p�� �����, 祬 �� ��᫥����
   asm   inc word ptr es:[di]
   asm   cmp ax,word ptr es:[di]
   asm   jnb m3                       // ���� < 599
   asm   mov word ptr es:[di],0        // ���� > 599
   asm   jmp m3
m4:
   asm   inc word ptr es:[di+2]
m3:
   asm   mov bx,word ptr es:[di+2]      // 㪠��⥫� �� ������� ��� �����
   asm   mov ax,LEN_SECURITY                      // p����p ��p���p� 18
   asm   mul bx
   asm   mov di,ax                      // ᬥ饭�� �� ������� ��� �����
   _DI = _DI + BEGADDR_SECURITY;
//printf("DIw=%d\n",_DI);
   _ES = SEGMENT+0x10;   // ������ ��p�� ������ ������
   _SI = (unsigned int)&MEMSECUR;       // ��p���p� ����� ���� ����� - 17 ����

   asm   cld                            // ���p����� �p� �����
   asm   mov cx,LEN_SECURITY
   asm   rep movsb                      // ������ 17 ���⮢
   asm   sti                         // p��p���� �p�p뢠���
   _ES = SEGMENT;                        // ��p�� ��p����� �����
   _DI = BEGADDR_SECURITY;               //5270
   if ((r==2)||(r==4)||(r==6))       // �����襭�� ����樨
   { asm   mov ax,di1
     asm   mov word ptr es:[di],ax
     asm   mov ax,di2
     asm   mov word ptr es:[di+2],ax
   }
   else
   { asm   mov ax,word ptr es:[di]
     di1 = _AX;  // 㪠��⥫� �� ���� ������� ��娢�
     asm   mov ax,word ptr es:[di+2]
     di2 = _AX;
   }
//printf("WR di1=%d di2=%d d10=%d d20=%d d11=%d d21=%d\n",di1,di2,di1p[0][0],di2p[0][0],di1p[1][0],di2p[1][0]);
   return di2; // 㪠��⥫� �� ��᫥���� ����ᠭ�� �������
}
*/
//---------------------------------------------------------------------------
//�室�� ��p����p� - ����p �����, ����p ��p�����

uint16_t Mem_SecurRd(uint16_t num, uint8_t numpage)//�⥭�� ����� ���� ���p��
{ if(num > (ARCH_SECURITY-1)) return 1;
}
//---------------------------------------------------------------------------
uint16_t Mem_SecurSearchStart(uint8_t run,struct startendtime *STARTENDTIM)
{ unsigned int        endelement;
}
//---------------------------------------------------------------------------
uint16_t Mem_SecurSearchStart2(uint8_t run,struct startendtime *STARTENDTIM)
{ unsigned int        startelement;
}
//---------------------------------------------------------------------------
uint16_t Mem_SecurSearchEnd(uint8_t numpage,struct startendtime *STARTENDTIM)
{ unsigned int        startelement;
}
//---------------------------------------------------------------------------
//��砫쭮� ���祭��, ᬥ饭��, ��p����; ����. ������ = 600
uint16_t Mem_SecurNextIndex(uint16_t start, uint16_t index, uint8_t page)
{ unsigned int  startindex,endindex;
    if(index > (ARCH_SECURITY-1)) return 65535;  //��室 �室���� ᬥ饭�� �� ��������
}
//------------------------------------------------------
// ᪮�४�஢��� ��砫� ���ࢠ�� ����������
// �� �।�����饣� ��⭮�� ������ ���������� ������ ������
//---------------------------------------------------------------------------
/*
void TimeCorrectionForDay(struct bintime* pbt, unsigned short contrHour) {
   tm tmBlock;

   tmBlock.tm_isdst = -1;
   tmBlock.tm_yday = 0;
   tmBlock.tm_wday = 0;

   tmBlock.tm_sec = 0;
   tmBlock.tm_min = 0;
   tmBlock.tm_hour = contrHour;
   tmBlock.tm_mday = pbt->date;
   tmBlock.tm_mon = pbt->month - 1;
   tmBlock.tm_year = pbt->year + 100;

   if (tmBlock.tm_year>137)
      tmBlock.tm_year  = 137;

   if(pbt->hours < contrHour)
      tmBlock.tm_mday--;

   mktime(&tmBlock);

   pbt->seconds = tmBlock.tm_sec;
   pbt->minutes = tmBlock.tm_min;
   pbt->hours = tmBlock.tm_hour;
   pbt->date = tmBlock.tm_mday;
   pbt->month = tmBlock.tm_mon + 1;
   pbt->year = tmBlock.tm_year - 100;
}
*/
//---------------------------------------------------------------------------
