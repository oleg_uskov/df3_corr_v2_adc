/*
 * Pressure.h
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */

#ifndef SRC_PRESS_H_
#define SRC_PRESS_H_

#include <array>
#include <math.h>

	struct PressCalibrate_point
	{
		float P;
		float U;
	};


// Класс для вычисление температуры газа на овновании значиний от АЦП
class Pressure {
public:
	Pressure();
	virtual ~Pressure();

	static constexpr float internal_Vref = 1.170; /*внутреннее опорное напряжение в вольтах */

/********************************************************************************
 * Константы для расчета давления 
 *********************************************************************************/
	PressCalibrate_point point_min_P = {0.0, 0.0}; // стартовая точка калибровки
	PressCalibrate_point point_mid_P = {0.0, 0.0}; // средняя точка калибровки
	PressCalibrate_point point_max_P = {0.0, 0.0}; // стартовая точка калибровки	
 /*********************************************************************************/
	

	
	float CalculateRealPressure(void);
	
private:

};

#endif /* SRC_TEMPERATURESENSOR_H_ */
