/*
 * Temperature.h
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */

#ifndef SRC_TEMPERATURE_H_
#define SRC_TEMPERATURE_H_

#include <array>
#include <math.h>
#include "Sensors.h"

struct TemperCalibrate_point
{
    float R;
    float U;
};


// тип термо элемента
enum TermoTypeElement
{
	PLATINUM_3_911 = 1,
	PLATINUM_3_851 = 2,
	COPPER_4_28 = 3,
	COPPER_4_26 = 4,
};


// Класс для вычисление температуры газа на овновании значиний от АЦП
class Temperature {
public:
	Temperature();
	virtual ~Temperature();

/********************************************************************************
 * Константы для расчета температуты 
 *********************************************************************************/
	static constexpr float Ro = 100.0; /*сопротивление при 0 градусов по Цельсию*/

	inline static constexpr std::array<float,4> D_3_851_array = {255.819, 9.14550, -2.92363, 1.79090}; /*массив коэффициентов D для α = 3,851e-3*/
	static constexpr double A_3_851 = 3.9083e-3; /* платина*/
	static constexpr double B_3_851 = -5.775e-7; /* платина*/

	inline static constexpr std::array<float,4> D_3_911_array = {251.903, 8.80035, -2.91506, 1.67611}; /*массив коэффициентов D для α = 3,911e-3*/
	static constexpr double A_3_911 = 3.9690e-3; /*платина*/
	static constexpr double B_3_911 = -5.841e-7; /*платина*/

	inline static constexpr std::array<float,4> D_4_28_array = {233.87, 7.9370,  -2.0062,  -0.3953}; /*массив коэффициентов D для α = 3,851e-3*/
	static constexpr double A_4_28 = 4.28e-3; /*коэффициентов D для α = 4,28e-3 (медь) */
	static constexpr double A_4_26 = 4.26e-3; /*коэффициентов D для α = 4,26e-3 (медь) */
/*********************************************************************************/


	float Resist_ADC1;
	float Resist_ADC2;
	float DemferTime = 4; /* текущее время демфирования */
	float Udemf ; /* среднее напряжение за время демфирования */

	TemperCalibrate_point point_min_T1 = {84.27, 0.0813}; //   0 -40 градусов, Pt100
	TemperCalibrate_point point_max_T1 = {123.242, 0.12102}; // 60 градусов, Pt100
	TemperCalibrate_point point_min_T2 = {84.03, 0.0832}; // -40 градусов, 100П
	TemperCalibrate_point point_max_T2 = {123.61, 0.1231}; // 60 градусов, 100П
	

	float CalculateRealTemperatureRun(void);
	float GetCurrentResistance(float, enum NameOfADC nameOfADC); /* */
	float GetTemperature(float U_average, enum NameOfADC nameOfADC); /* */
	float ReadReferenceVoltage(enum NameOfADC nameOfADC); /*получение опорного напряжения*/
	//void WriteTemperToFlash(void);
	void WriteConfigToFlash(void);
	void ReadConfigFromFlash(void);
	TermoTypeElement currentTermoTypeElement_T1 = PLATINUM_3_851;
	TermoTypeElement currentTermoTypeElement_T2 = PLATINUM_3_911;
	// ADC_state currentADC_state = IDLE;
	

	
private:


};

#endif /* SRC_TEMPERATURESENSOR_H_ */
