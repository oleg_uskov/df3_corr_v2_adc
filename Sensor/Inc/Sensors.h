/*
 * Sensors.h
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */

#ifndef SRC_SENSOR_H_
#define SRC_SENSOR_H_

#include <array>
#include <math.h>

// struct Calibrate_point
// {
    // float R;
    // float U;
// };

// единицы измерения
enum Units
{
	celsiy = 0x20,
	pascal = 0x5C,
	mVolt = 0xFE,
};

// состояния работы с тремя АЦП
enum ADC_state
{
	IDLE,
	WAIT_ENVIR_TEMPERATURE,
	WAIT_TEMPERATURE_1,
	WAIT_TEMPERATURE_2,
	PRESSURE_CHOOSE_CHANNAL,
	WAIT_PRESSURE_OUT,
	WAIT_PRESSURE_IN,
};

// Имя АЦП
enum NameOfADC
{
	TEMPERATURE_1 = 1,
	TEMPERATURE_2 = 2,
	PRESSURE = 3,
};

//тип термо элемента
// enum TermoTypeElement
// {
	// PLATINUM_3_911 = 1,
	// PLATINUM_3_851 = 2,
	// COPPER_4_28 = 3,
	// COPPER_4_26 = 4,
// };


// Класс для вычисление температуры газа на овновании значиний от АЦП
class Sensors {
public:
	Sensors();
	virtual ~Sensors();

	static constexpr float internal_Vref = 1.170; /*внутреннее опорное напряжение в вольтах */

/********************************************************************************
 * Данные от трех АЦП 
 *********************************************************************************/
	float Vref_T1 = 0.400; /*измеренное реальное напряжение в вольтах */
	float Vref_T2 = 0.400; /*измеренное реальное напряжение в вольтах */
	uint16_t EnvirTemperature_ADC;
	float ADC1_evg_data;
	float ADC2_evg_data;
	float ADC3out_evg_data;
	float ADC3in_evg_data;

	float Voltage_ADC1;
	float Voltage_ADC2;
	float Voltage_pressIn;
	float Voltage_pressOut;
	float Reference_ADC1;
	float Reference_ADC2;

	float EnvirTemperature; /* температура окружающей среды */
	float Temperature_1channel = 0.0;
	float Temperature_2channel = 0.0;
	float Pressure_in_channel = 0.0;
//	float Pressure_out_channel = 0.0;	


	int Run(void);
	void Init(void);
	float CalculateRealTemperatureRun(void);
	float ReadReferenceVoltage(enum NameOfADC nameOfADC); /*получение опорного напряжения*/
	void WriteTemperToFlash(void);
	ADC_state currentADC_state = IDLE;
	ADC_state lastPRESSURE_state = WAIT_PRESSURE_IN;
	
	float CalculateRealPressure(void);
	
//	unsigned cntADC1;
//	unsigned cntADC2;
//	unsigned cntADC3;
	
private:
	//unsigned m_number_of_conver;
	unsigned cnt1_samples;
	unsigned cnt2_samples;
	unsigned cnt3_samples;
	unsigned cnt4_samples;

	unsigned long m_summa_samples_1;
	unsigned long m_summa_samples_2;
	unsigned long m_summa_samples_3;
	unsigned long m_summa_samples_4;

	bool m_finish_init = false;

};

#endif /* SRC_TEMPERATURESENSOR_H_ */
