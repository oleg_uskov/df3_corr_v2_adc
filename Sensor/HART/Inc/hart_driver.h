#ifndef _HART_DRIVER_H
#define _HART_DRIVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

#define TRUE  1
#define FALSE 0

#define SYSCLK_FREQUENCY (20000000)
#define TICKS_SECOND     (1000)

typedef enum
{
	HT_SERICAL_NONE,
	HT_SERICAL_EVEN,
	HT_SERICAL_ODD,
}parity_type;

/* serial declaration */
//unsigned char serial_init(unsigned int baud,
//		unsigned char data_bit,
//		parity_type parity,
//		unsigned char stop_bit);
void serial_enable(unsigned char rx_enable, unsigned char tx_enable);
unsigned char serial_put_bytes(unsigned char * byte, unsigned char cnt);
unsigned char serial_get_byte(unsigned char *byte);
/* virtual function interface */
extern void (*SerialReceiveMsg)(void);
extern void (*SerialSendMsg)(void);

/* UART declaration */
void HART_FinishRx();
void HART_FinishTx();



/* critical function declaration */
void enter_critical_section(void);
void exit_critical_section(void);

#ifdef __cplusplus
}
#endif

#endif
