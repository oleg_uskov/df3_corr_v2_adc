#include "hart_driver.h"

void (*SerialReceiveMsg)(void);
void (*SerialSendMsg)(void);

extern UART_HandleTypeDef huart1;

int last_state= 0;
int last_stateTx= 0;

unsigned char received_byte = 0;

void serial_enable(unsigned char rx_enable, unsigned char tx_enable)
{
	enter_critical_section( );
    if( rx_enable )
    {
    	HAL_GPIO_WritePin(U1_RTS_GPIO_Port, U1_RTS_Pin, GPIO_PIN_RESET);/* включение приема по UART1*/
    	HAL_UART_Receive_IT(&huart1, &received_byte, 1);
    	last_state = 1;
    }
    else
    {
    	HAL_UART_AbortReceive_IT(&huart1);
    	last_state = 2;
    }
    if( tx_enable )
    {
    	HAL_GPIO_WritePin(U1_RTS_GPIO_Port, U1_RTS_Pin, GPIO_PIN_SET);/* включение передачи по UART1*/
    	SerialSendMsg();
    	last_stateTx = 1;
    }
    else
    {
    	//HAL_UART_AbortTransmit_IT(&huart1);
    	last_stateTx = 2;
    }
    exit_critical_section( );
}

unsigned char serial_put_bytes(unsigned char * byte, unsigned char cnt)
{
	HAL_GPIO_WritePin(U1_RTS_GPIO_Port, U1_RTS_Pin, GPIO_PIN_SET);/* включение передачи по UART1*/
	HAL_UART_Transmit_IT(&huart1, (uint8_t*)byte, cnt);
	return TRUE;
}

unsigned char serial_get_byte(unsigned char *byte)
{
	*byte = received_byte;
	HAL_GPIO_WritePin(U1_RTS_GPIO_Port, U1_RTS_Pin, GPIO_PIN_RESET);/* включение приема по UART1*/
	HAL_UART_Receive_IT(&huart1, &received_byte, 1);
	return TRUE;
}


void HART_FinishRx(){
		SerialReceiveMsg();
		last_state = 3;
}

void HART_FinishTx(){
		SerialSendMsg();
		last_stateTx = 3;
}


void
enter_critical_section( void )
{
	//__disable_irq();
}

void
exit_critical_section( void )
{
	//__enable_irq();
}


