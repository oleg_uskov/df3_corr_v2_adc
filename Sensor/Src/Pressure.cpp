/*
 * Pressure.cpp
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */

#include "Pressure.h"
#include "main.h"
//#include <string>

#include "AD7792.h"				// AD7792 definitions.

extern UART_HandleTypeDef huart1;
extern SPI_HandleTypeDef hspi3;
extern addrTemper addrTemper1;

extern "C" {
	extern void P23K256_Write_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
	extern uint8_t P23K256_Read_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *rxData, uint16_t RxSize);
}

Pressure::Pressure()
{
}

Pressure::~Pressure() {
}


/********************************************************************************
 * Расчет текущего давления
 *********************************************************************************/
// float Pressure::GetCurrentPressure(float U_av, enum NameOfADC nameOfADC)
// {
	// C = (y1 – y3) / [(x3 - x2) * (x1 - x3)] - (y1 – y2) / [(x1 - x2) * (x3 - x2)];
	// B = (y1 – y2) / (x1 - x2) – C* (x1 + x2);   
	// A = 1/3 * [(y1 + y2 + y3 ) – B*( x1 + x2 + x3) - C*( x12 + x22 + + x32)];

	// у = A + B * x + C * x2;
	
	
	
// }