/***************************************************************************//**
 *   @file   Communication.c
 *   @brief  Implementation of Communication Driver.
 *   @author DBogdan (dragos.bogdan@analog.com)
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: 501
*******************************************************************************/

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/
#include <ADCCommunication.h>

extern SPI_HandleTypeDef hspi1;

/***************************************************************************//**
 * @brief Writes data to SPI.
 *
 * @param data - Write data buffer:
 *               - first byte is the chip select number;
 *               - from the second byte onwards are located data bytes to write.
 * @param bytesNumber - Number of bytes to write.
 *
 * @return Number of written bytes.
*******************************************************************************/
unsigned char SPI_Write(unsigned char* data,
                        unsigned char bytesNumber)
{
	unsigned char chipSelect    = data[0];
	unsigned char writeData[4]	= {0, 0, 0, 0};
	unsigned char byte          = 0;
	for(byte = 0;byte < bytesNumber;byte ++)
	{
		writeData[byte] = data[byte + 1];
	}

	if      (chipSelect == 1) HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_RESET);
	else if (chipSelect == 2) HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_RESET);
	else if (chipSelect == 3) HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_RESET);
	//HAL_SPI_Transmit(&hspi1, &data[1], bytesNumber, HAL_MAX_DELAY);
	for(byte = 0;byte < bytesNumber;byte ++)
    {
		if (HAL_SPI_Transmit(&hspi1, (uint8_t *)&writeData[byte], 1, 5000) != HAL_OK) {
			Error_Handler();
		}
    }
	if      (chipSelect == 1) HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_SET);
	else if (chipSelect == 2) HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_SET);
	else if (chipSelect == 3) HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_SET);

	return(bytesNumber);
}

/***************************************************************************//**
 * @brief Reads data from SPI.
 *
 * @param data - As an input parameter, data represents the write buffer:
 *               - first byte is the chip select number;
 *               - from the second byte onwards are located data bytes to write.
 *               As an output parameter, data represents the read buffer:
 *               - from the first byte onwards are located the read data bytes. 
 * @param bytesNumber - Number of bytes to write.
 *
 * @return Number of written bytes.
*******************************************************************************/
unsigned char SPI_Read(unsigned char* data,
                       unsigned char bytesNumber)
{
  	unsigned char chipSelect    = data[0];
	unsigned char writeData[4]  = {0, 0, 0, 0};
    unsigned char readData[4]	= {0, 0, 0, 0};
    unsigned char byte          = 0;

    for(byte = 0;byte < bytesNumber;byte ++)
    {
        writeData[byte] = data[byte + 1];
        data[byte + 1] = 0;
    }

	if      (chipSelect == 1) HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_RESET);
	else if (chipSelect == 2) HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_RESET);
	else if (chipSelect == 3) HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_RESET);
	for(byte = 0;byte < bytesNumber;byte ++)
    {
		if (HAL_SPI_TransmitReceive(&hspi1, (uint8_t *)&writeData[byte], (uint8_t *)&readData[byte], 1, 5000) != HAL_OK) 
//		if (HAL_SPI_TransmitReceive_IT(&hspi1, (uint8_t *)&writeData[byte], (uint8_t *)&readData[byte], bytesNumber) != HAL_OK) 
		{
			Error_Handler();
		}
    }
	if      (chipSelect == 1) HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_SET);
	else if (chipSelect == 2) HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_SET);
	else if (chipSelect == 3) HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_SET);

    for(byte = 0;byte < bytesNumber;byte ++)
    {
        data[byte] = readData[byte];
    }
	//HAL_SPI_Receive(&hspi1, &data[1], bytesNumber, HAL_MAX_DELAY);


	return(bytesNumber);
}

/***************************************************************************//**
 * @brief Установка выбранного выхода CS в 0.
 *
 * @param numberCS - номер выбранного интерфейса SPI:
 *

 *
 * @return
*******************************************************************************/
void Set_CS_LOW(unsigned char numberCS){
	if      (numberCS == 1) HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_RESET);
	else if (numberCS == 2) HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_RESET);
	else if (numberCS == 3) HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_RESET);
	else{
		HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_SET);
	}

}

/***************************************************************************//**
 * @brief Установка выбранного выхода CS в 1.
 *
 * @param numberCS - номер выбранного интерфейса SPI:
 *

 *
 * @return
*******************************************************************************/
void Set_CS_HIGH(unsigned char numberCS){
	if      (numberCS == 1) HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_SET);
	else if (numberCS == 2) HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_SET);
	else if (numberCS == 3) HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_SET);
	else{
		HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_SET);
	}
}
