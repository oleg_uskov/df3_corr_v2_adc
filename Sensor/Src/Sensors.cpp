/*
 * Sensors.cpp
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */
#include "stdio.h"
#include "string.h"
#include "Sensors.h"
#include "Temperature.h"
#include "Pressure.h"


#include "main.h"
//#include <string>

#include "AD7792.h"				// AD7792 definitions.

extern UART_HandleTypeDef huart1;
extern SPI_HandleTypeDef hspi3;
extern addrTemper addrTemper1;
extern unsigned int cnt1, cnt2;

extern Temperature temper;
extern Pressure press;
uint8_t str20[30];
float Vin, Vout;

extern "C" {
	extern void P23K256_Write_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
	extern uint8_t P23K256_Read_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *rxData, uint16_t RxSize);
	extern void LCDPrint(uint8_t stroka, uint8_t colonka, char * Text);
}

Sensors::Sensors()
{
	cnt1_samples = 0;
	cnt2_samples = 0;
	cnt3_samples = 0;
	m_summa_samples_1 = 0;
	m_summa_samples_2 = 0;
	m_summa_samples_3 = 0;
}



Sensors::~Sensors() {

}

/********************************************************************************
 * Инициализация всех АЦП
 *********************************************************************************/
void Sensors::Init() {
	temper.ReadConfigFromFlash(); // восстановление кофнигурационных файлов из Flash

	AD7792_Reset(TEMPERATURE_1);
	AD7792_Reset(TEMPERATURE_2);
	AD7792_Reset(PRESSURE);	
	if (!AD7792_Init(TEMPERATURE_1))
		Error_Handler();
	if (!AD7792_Init(TEMPERATURE_2))
		Error_Handler();
	if (!AD7792_Init(PRESSURE))
		Error_Handler();

/*настройка первого АЦП*/
    AD7792_SetRegisterValue(AD7792_REG_IO,
    		AD7792_IEXCEN(AD7792_EN_IXCEN_1mA), //устанвка тока 1 мА на оба источника
			1, TEMPERATURE_1);

    AD7792_SetGain(AD7792_GAIN_2, TEMPERATURE_1);                // set gain to 2
    AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_1);     // use AIN1(+) - AIN1(-)
    AD7792_SetMode(AD7792_MODE_CONT, TEMPERATURE_1); // режим непрерывного преобразования

    Vref_T1 = ReadReferenceVoltage(TEMPERATURE_1);
    //AD7792_SetUNIPOLAR(TEMPERATURE_1);

    //AD7792_SetIntReference(AD7792_REFSEL_INT);    // select internal 1.17V reference
//    AD7792_Calibrate(AD7792_MODE_CAL_INT_ZERO,
//                     AD7792_CH_AIN1P_AIN1M);      // Internal Zero-Scale Calibration

//	AD7792_SetChannel(AD7792_CH_TEMP);

    //AD7792_SetMode(AD7792_MODE_SINGLE, TEMPERATURE_1); // режим одиночного преобразования

/*настройка второго АЦП*/
    AD7792_SetRegisterValue(AD7792_REG_IO,
    		AD7792_IEXCEN(AD7792_EN_IXCEN_1mA), //устанвка тока 1 мА на оба источника
			1, TEMPERATURE_2);

    AD7792_SetGain(AD7792_GAIN_2, TEMPERATURE_2);                // set gain to 2
    AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_2);     // use AIN1(+) - AIN1(-)
	AD7792_SetMode(AD7792_MODE_CONT, TEMPERATURE_2); // режим непрерывного преобразования
	Vref_T2 = ReadReferenceVoltage(TEMPERATURE_2);

/*настройка третьего АЦП - Давления*/
    AD7792_SetRegisterValue(AD7792_REG_IO,
    		AD7792_IEXCEN(AD7792_EN_IXCEN_210uA), // 1mA, 210ua устанвка тока 0.210 мА на оба источника
			1, PRESSURE);

    AD7792_SetGain(AD7792_GAIN_1, PRESSURE);                // set gain to xxx
    AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, PRESSURE);     // установка канала, где вход датчика давления
	AD7792_SetMode(AD7792_MODE_CONT, PRESSURE); // режим непрерывного преобразования
	AD7792_SetIntReference(AD7792_REFSEL_INT, PRESSURE);    // select internal 1.17V reference

	m_finish_init = true;

}

/********************************************************************************
 * Автомат последовательного считывания данных с АЦП
 *********************************************************************************/
int Sensors::Run() {

//	AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_1);
//	m_summa_ADC1 += AD7792_SingleConversion(TEMPERATURE_1);
//	m_number_of_conver++;
//	AD7792_SetChannel(AD7792_CH_TEMP, TEMPERATURE_1);
//	EnvirTemperature = ((AD7792_SingleConversion(TEMPERATURE_1)- 32768) * 117000 /(32768 * 81)) - 276;
//
	int ready = ADC_DATA_READY;
	switch(currentADC_state){
	case IDLE:
		if (m_finish_init) {

			if(cnt2_samples == 0) { //читаем только раз за цикл
				AD7792_SetChannel(AD7792_CH_TEMP, TEMPERATURE_1);
				currentADC_state = WAIT_ENVIR_TEMPERATURE;
			}
			else
				currentADC_state = WAIT_TEMPERATURE_1;
		}
		break;
	case WAIT_ENVIR_TEMPERATURE:
			Set_CS_LOW(TEMPERATURE_1);
			if(ready){
				EnvirTemperature_ADC = (AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0)); // 0xFFFF -  потому что перепут в схеме подключения резистора
				EnvirTemperature = (1170.0 * (EnvirTemperature_ADC - 32768)/(32768 * 0.81)) - 276; // перевод с градусы Цельсия
				AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, TEMPERATURE_1);
				Set_CS_HIGH(TEMPERATURE_1);
				currentADC_state = WAIT_TEMPERATURE_2;
			}
		break;
	case WAIT_TEMPERATURE_1:
		Set_CS_LOW(TEMPERATURE_1);
		if(ready){
//			m_summa_samples_1 += (0xFFFF - AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0)); // 0xFFFF -  потому что перепут в схеме подключения резистора
			m_summa_samples_1 += (AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0));
			Set_CS_HIGH(TEMPERATURE_1);
			cnt1_samples++;
			currentADC_state = WAIT_TEMPERATURE_2;
		}
		break;
	case WAIT_TEMPERATURE_2:
		Set_CS_LOW(TEMPERATURE_2);
		if(ADC_DATA_READY){
//			m_summa_samples_2 += (0xFFFF - AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0)); // 0xFFFF -  потому что перепут в схеме подключения резистора
			m_summa_samples_2 += (AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0)); 
			Set_CS_HIGH(TEMPERATURE_2);
			cnt2_samples++;
			currentADC_state = PRESSURE_CHOOSE_CHANNAL;
		}
		break;
		
	case PRESSURE_CHOOSE_CHANNAL:
			if(cnt1_samples == 0) { //читаем вход датчика давления только раз за цикл
				AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, PRESSURE);
				AD7792_SetGain(AD7792_GAIN_1, PRESSURE);
				currentADC_state = WAIT_PRESSURE_IN;
			}
			else{
				currentADC_state = WAIT_PRESSURE_OUT;
			}
		break;		
		
	case WAIT_PRESSURE_IN:
		Set_CS_LOW(PRESSURE);
		if(ADC_DATA_READY){
			ADC3in_evg_data = AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0);
			AD7792_SetChannel(AD7792_CH_AIN2P_AIN2M, PRESSURE);  // настройка выхода датчика
			AD7792_SetGain(AD7792_GAIN_32, PRESSURE); // настройка выхода датчика			
			Set_CS_HIGH(PRESSURE);
			currentADC_state = IDLE;//WAIT_PRESSURE_OUT;
		}
		break;
	case WAIT_PRESSURE_OUT:
		Set_CS_LOW(PRESSURE);
		if(ADC_DATA_READY){
			m_summa_samples_3 += AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0);
			Set_CS_HIGH(PRESSURE);
			cnt3_samples++;
			//lastPRESSURE_state = WAIT_PRESSURE_OUT;
			currentADC_state = IDLE; 
		}
		break;
		
	default:
		//ConsolePrint("Unknown state", 14);
		break;
	}
return ready;
}

/********************************************************************************
 * Получение среднего значения от АЦП и расчет текущей температуры
 *********************************************************************************/
float Sensors::CalculateRealTemperatureRun() {

	/* вычисляем среднее значение */
	if (cnt1_samples != 0) ADC1_evg_data = m_summa_samples_1/cnt1_samples;

	if (cnt2_samples != 0) ADC2_evg_data = m_summa_samples_2/cnt2_samples;
//    cntADC1 = cnt1_samples;
//    cntADC2 = cnt2_samples;
	/* вычисляем текущее напряжение на резисторе */
        if (cnt1_samples > 0) 
	{ Voltage_ADC1 = Vref_T1 *  (ADC1_evg_data - 32768)/32768/2; // делим на 2 тк коэф. усиления 2
	    Temperature_1channel = temper.GetTemperature(Voltage_ADC1, TEMPERATURE_1);
         }
        if (cnt2_samples > 0) 
        { Voltage_ADC2 = Vref_T2 *  (ADC2_evg_data - 32768)/32768/2; // делим на 2 тк коэф. усиления 2
	   Temperature_2channel = temper.GetTemperature(Voltage_ADC2, TEMPERATURE_2);
	 }  
	m_summa_samples_1 = 0;
	m_summa_samples_2 = 0;
	cnt1_samples = 0;
	cnt2_samples = 0;
	currentADC_state = IDLE;
	WriteTemperToFlash();
	return Temperature_1channel;
}

/********************************************************************************
 * Получение среднего значения от АЦП и расчет текущего давления
 *********************************************************************************/
float Sensors::CalculateRealPressure() {

	/* вычисляем среднее значение */
	if (cnt3_samples != 0) ADC3out_evg_data = m_summa_samples_3/cnt3_samples;
	//if (cnt4_samples != 0) ADC3in_evg_data = m_summa_samples_4/cnt4_samples;	

	//cntADC3 = cnt3_samples;
	m_summa_samples_3 = 0;
	cnt3_samples = 0;
	m_summa_samples_4 = 0;
	cnt4_samples = 0;	
	currentADC_state = IDLE;
	/* вычисляем текущее напряжение на резисторе */
	Voltage_pressOut = 1000 * internal_Vref * (ADC3out_evg_data - 32768)/32768/32; // делим на 32 тк коэф. усиления 32
	Voltage_pressIn = 1000 * internal_Vref * (ADC3in_evg_data - 32768)/32768/1; // делим на 2 тк коэф. усиления 2
    Vout = ADC3out_evg_data;
    Vin = ADC3in_evg_data ;    
//      memset(str20,0,20);
//      sprintf((char*)str20,"P %7.3f %8.4f  ",Voltage_pressIn,Voltage_pressOut/2.226);
//      LCDPrint(0, 0, (char*)str20);
//      sprintf((char*)str20,"P %7.0f %7.0f   ",ADC3in_evg_data ,ADC3out_evg_data);
//      LCDPrint(1, 0, (char*)str20);


	//WriteTemperToFlash();
	return Voltage_pressOut;
}

/********************************************************************************
 * Запись текущей температуры во flash
 *********************************************************************************/
void Sensors::WriteTemperToFlash()
{
//	float_to_data(flash_data_buf,&Temperature_1channel);
//	float_to_data(flash_data_buf,&EnvirTemperature);
	float data[] = {Temperature_1channel,
					ADC1_evg_data,
					Temperature_2channel,
					ADC2_evg_data,
					EnvirTemperature,
					(float)EnvirTemperature_ADC,
	};
	uint8_t * p_data = reinterpret_cast<uint8_t *>(&data);
	P23K256_Write_Data(&hspi3, addrTemper1.addrDataTemper,	p_data, sizeof(data));

}

/********************************************************************************
 * Чтение опорного напряжения
 *********************************************************************************/
float Sensors::ReadReferenceVoltage(enum NameOfADC nameOfADC) {
	m_finish_init = false; //запрет основного цикла работа
	constexpr uint8_t NUMBER_SAMPLE = 4;
	while(currentADC_state != IDLE) {};
	AD7792_SetIntReference(AD7792_REFSEL_INT, nameOfADC);    // select internal 1.17V reference
	AD7792_SetChannel(AD7792_CH_AIN3P_AIN3M, nameOfADC);
	float refVolt = 0;
	float Reference_code = 0;
	Set_CS_LOW(nameOfADC);
    for(int count = 0; count < NUMBER_SAMPLE; count ++)
    {
        AD7792_WaitRdyGoLow();
    	Reference_code += (AD7792_GetRegisterValue(AD7792_REG_DATA, 2, 0));
    }
    Reference_code = Reference_code / NUMBER_SAMPLE;

	if (nameOfADC == TEMPERATURE_1){
		Reference_ADC1 = Reference_code;
	}
	else if (nameOfADC == TEMPERATURE_2){
		Reference_ADC2 = Reference_code; ;
	}
	
	refVolt = internal_Vref * (Reference_code - 32768) / 32768 /2; // перевод в вольт
	AD7792_SetChannel(AD7792_CH_AIN1P_AIN1M, nameOfADC);
    AD7792_SetIntReference(AD7792_REFSEL_EXT, nameOfADC);    // select external
	Set_CS_HIGH(nameOfADC);

	m_finish_init = true; //разрешение основного цикла работа
	return refVolt;

}
