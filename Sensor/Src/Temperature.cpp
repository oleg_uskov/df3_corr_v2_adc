/*
 * Temperature.cpp
 *
 *  Created on: Jan 5, 2021
 *      Author: Oleg
 */

#include "Temperature.h"
#include "main.h"
//#include <string>

#include "AD7792.h"				// AD7792 definitions.

extern UART_HandleTypeDef huart1;
extern SPI_HandleTypeDef hspi3;
extern addrTemper addrTemper1;
extern uint8_t str90[90];
extern UART_HandleTypeDef huart2;

extern "C" {
	extern void P23K256_Write_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *txData, uint16_t TxSize);
	extern uint8_t P23K256_Read_Data(SPI_HandleTypeDef *hspi, uint8_t *Adress, uint8_t *rxData, uint16_t RxSize);
}
Temperature temperat;

Temperature::Temperature()
{
}



Temperature::~Temperature() {
}


/********************************************************************************
 * Расчет текущей температуры
 *********************************************************************************/
float Temperature::GetTemperature(float U_average, enum NameOfADC nameOfADC)
{
	TermoTypeElement type = (nameOfADC == TEMPERATURE_1) ? currentTermoTypeElement_T1 : currentTermoTypeElement_T2;

	float currResist = GetCurrentResistance(U_average, nameOfADC);
	float Temperatur = 0.0;
	if (type == PLATINUM_3_851){
		if (currResist < Ro){ // температура меньше 0
			for (char i=1; i < D_3_851_array.size()  + 1; i++) {
				Temperatur += D_3_851_array[i-1] * pow( (currResist/Ro - 1), i );
			}
		}else{
			Temperatur = (sqrt(pow(A_3_851,2) - 4 * B_3_851 * (1 - currResist/Ro)) - A_3_851) / (2 * B_3_851);

		}
	} else if (type == PLATINUM_3_911){
		if (currResist < Ro){ // температура меньше 0
			for (char i=1; i < D_3_911_array.size()  + 1; i++) {
				Temperatur += D_3_911_array[i-1] * pow( (currResist/Ro - 1), i );
			}
		}else{
			Temperatur = (sqrt(pow(A_3_911,2) - 4 * B_3_911 * (1 - currResist/Ro)) - A_3_911) / (2 * B_3_911);

		}
	} else if (type == COPPER_4_28){
		if (currResist < Ro){
			for (char i=1; i < D_4_28_array.size() + 1; i++) {
				Temperatur += D_4_28_array[i-1] *  pow( (currResist/Ro - 1) , i);
			}
		}else{
			Temperatur = (currResist/Ro - 1)/A_4_28;
		}
	} else if (type == COPPER_4_26){
		Temperatur = (currResist/Ro - 1)/A_4_26;
	}else{
		Temperatur = 0.0;
		//Error_Handler();
	}
//memset(str90,0,40);
//std::swprintf((wchar_t*)str90,12, (const wchar_t *)"T=%f\n\r",Temperatur);
//HAL_GPIO_WritePin(GPIOD, U2_RTS_Pin, GPIO_PIN_SET);   //RTS USART2 set
//HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),100);

return Temperatur;
}


/********************************************************************************
 * Расчет текущего сопротивления
 *********************************************************************************/
float Temperature::GetCurrentResistance(float U_av, enum NameOfADC nameOfADC)
{
	float SummaADC = 0;
	uint8_t N = 2 * (uint8_t)DemferTime; /* количество отсчетов за время демфировани (*2 тк 2 раза в секунду считываем) */
	static int numb_new_data1 = 1; /* количество заполненных отсчетов в массиве*/
	static int numb_new_data2 = 1; /* количество заполненных отсчетов в массиве*/
	static std::array<float,20> shift_reg_temper1 = {0,}; /*буфер для хранения данных для подсчета скользящего среднего */
	static std::array<float,20> shift_reg_temper2 = {0,}; /*буфер для хранения данных для подсчета скользящего среднего */

	if (N > shift_reg_temper1.size())
		return 0.0;

	if (nameOfADC == TEMPERATURE_1){
		for (char i=0; i < N-1; i++) {
			shift_reg_temper1[i] =shift_reg_temper1[i+1];
			SummaADC += shift_reg_temper1[i];
		}
		shift_reg_temper1[N-1] = U_av;
		SummaADC += shift_reg_temper1[N-1];
		if (numb_new_data1 < N){
		   Udemf = SummaADC / numb_new_data1;
		   numb_new_data1++;
		}
		else
		   Udemf = SummaADC / N ;
	}
	else if (nameOfADC == TEMPERATURE_2){
		for (char i=0; i < N-1; i++) {
			shift_reg_temper2[i] =shift_reg_temper2[i+1];
			SummaADC += shift_reg_temper2[i];
		}
		shift_reg_temper2[N-1] = U_av;
		SummaADC += shift_reg_temper2[N-1];
		if (numb_new_data2 < N){
		   Udemf = SummaADC / numb_new_data2;
		   numb_new_data2++;
		}
		else
		   Udemf = SummaADC / N ;
	}


	float currResist ; // текущее значпние сопротивления
//point_min_T2.U = 0.0897;
//point_min_T2.R = 84.03;
	if (nameOfADC == TEMPERATURE_1){
		Resist_ADC1 = currResist = ((Udemf - point_min_T1.U) * (point_max_T1.R - point_min_T1.R))/ (point_max_T1.U - point_min_T1.U) + point_min_T1.R; // формула линейной интерполяции;
	}
	else if (nameOfADC == TEMPERATURE_2){
		Resist_ADC2 = currResist = ((Udemf - point_min_T2.U) * (point_max_T2.R - point_min_T2.R))/ (point_max_T2.U - point_min_T2.U) + point_min_T2.R;
	}
//memset(str90,0,40);
//std::swprintf((wchar_t*)str90,30, (const wchar_t*)"ADC=%d U=%f R=%f  ",nameOfADC, Udemf, currResist);
//HAL_GPIO_WritePin(GPIOD, U2_RTS_Pin, GPIO_PIN_SET);   //RTS USART2 set
//HAL_UART_Transmit(&huart2,(uint8_t*)str90,sizeof(str90),1000);

	return currResist;
}

/********************************************************************************
 * Запись во flash текущего состояния
 *********************************************************************************/

void Temperature::WriteConfigToFlash()
{
//	float_to_data(flash_data_buf,&Temperature_1channel);
//	float_to_data(flash_data_buf,&EnvirTemperature);
	float data[] = {point_min_T1.R,  //11*4=44 байта 
					point_min_T1.U,
					point_max_T1.R,
					point_max_T1.U,
					point_min_T2.R,
					point_min_T2.U,
					point_max_T2.R,
					point_max_T2.U,
					DemferTime,
					(float)currentTermoTypeElement_T1,
					(float)currentTermoTypeElement_T2,
	};
	uint8_t * p_data = reinterpret_cast<uint8_t *>(&data);
	P23K256_Write_Data(&hspi3, addrTemper1.addrConfigTemper, p_data, sizeof(data));
}
void WriteCalibrADC_ToFlash()
{
    temperat.WriteConfigToFlash();
}

/********************************************************************************
 * Чтение из flash текущего состояния
 *********************************************************************************/
void Temperature::ReadConfigFromFlash()
{
	float data[16];  // 16*4= 64 байта
    P23K256_Read_Data(&hspi3, addrTemper1.addrConfigTemper, (uint8_t*)&data,sizeof(data));
	point_min_T1.R                 = data[0];
	point_min_T1.U                 = data[1];
	point_max_T1.R                 = data[2];
	point_max_T1.U                 = data[3];
	point_min_T2.R                 = data[4];
	point_min_T2.U                 = data[5];
	point_max_T2.R                 = data[6];
	point_max_T2.U                 = data[7];
	DemferTime                     = data[8];
	currentTermoTypeElement_T1     = (TermoTypeElement)data[9];
	currentTermoTypeElement_T2     = (TermoTypeElement)data[10];
}
void ReadCalibrADC_FromFlash()
{
    temperat.ReadConfigFromFlash();
}

