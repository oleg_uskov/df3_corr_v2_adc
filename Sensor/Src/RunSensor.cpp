/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.cpp
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "Sensors.h"
#include "PackageTransciever.h"
#include "hart_driver.h"
//#include "AD7792.h"				// AD7792 definitions.
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef void(*pf)(void);
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
extern RTC_HandleTypeDef hrtc;

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;

extern TIM_HandleTypeDef htim16;
extern TIM_HandleTypeDef htim17;

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;

extern  uint32_t t_17, t_16;
extern unsigned char Flag_Hartservice;
///* USER CODE BEGIN PV */
//uint32_t t_17,_secTakt,counttime, t_16;
//Time     TIME_SETTIME, BINTIME; 
//  RTC_TimeTypeDef sTime;  //= {0};
//  RTC_DateTypeDef sDate; //= {0};
//  RTC_AlarmTypeDef sAlarm;// = {0};
//uint32_t alt1, alt2, alt3, count1, count2, count3;
//uint32_t ctim1, ctim2, ctim3;
//uint8_t  bflag_c1,bflag_c2;
//uint32_t intr1, intr2, intr3;
//uint32_t Interval[3]= {0,0,0};  //интервад между импульсами
//float Qw_s1, Qw_s3, Qw_s2;  // секундный расход счетчиков газа
//uint16_t P = 0;
//uint32_t k,in1,in2,k1;
////char strd[40];
//char str90[90];

int InQueue(int n, pf func);
Sensors sensor;
Temperature temper;
Pressure press;
PackageTransciever HART;
extern uint8_t T_500, T_10;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/

/* USER CODE BEGIN PFP */

//void LiquidCrystal(GPIO_TypeDef *gpioport,uint16_t d0, uint16_t d1,
//    uint16_t d2, uint16_t d3,uint16_t d4, uint16_t d5, uint16_t d6, uint16_t d7);
//void LCDPrint(uint8_t stroka, uint8_t colonka, char * Text);
//void setCursor(uint8_t col, uint8_t row);
//size_t print(const char *);


/* USER CODE END PFP */


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */

void InitSensor(void)
{
	// HAL_GPIO_WritePin(GPIOD, LED_Pin, GPIO_PIN_SET); // подстветка дисплея ВКЛ


	// HAL_TIM_Base_Start_IT(&htim17);
	// HAL_TIM_Base_Start_IT(&htim16);
	HAL_GPIO_WritePin(GPIOA, ADC1_Pin, GPIO_PIN_SET);//отключение АЦП1
	HAL_GPIO_WritePin(GPIOA, ADC2_Pin, GPIO_PIN_SET);//отключение АЦП2
	HAL_GPIO_WritePin(GPIOA, ADC3_Pin, GPIO_PIN_SET);//отключение АЦП3
	sensor.Init();
	HART.Init();

}
//void CalculateTemper()
//{
//	sensor.CalculateRealTemperatureRun();
//	sensor.CalculateRealPressure();   
//}
void RunHart(void)
{
    if (Flag_Hartservice == 1)
	HART.HART_run();
}	
//     if (((t_17 % 100) == 0) && (T_500 == 0))
//     {
 //       T_500 = 1; 
//		sensor.CalculateRealTemperatureRun();  
//		sensor.CalculateRealPressure(); 
//	 }
//     if (((t_17 == 10) || (t_17 == 110)) && (T_500 == 1))  T_500 = 0;

//	if (((t_16 % 10) == 0) && (T_10 == 0))
//    {// каждые 10 мс
//        T_10 = 1;   
//		sensor.Run(); /*периодически запускаем считывание данных со всех АЦП */
//	}
//    if ((t_16 % 10) != 0)  T_10 = 0;
//	if ((t_16 % 2) == 0) {// каждые 2мс
//		HART.TimerTick();
//	}
//}
//----------------------------------------------------------------
void RunCalculate()
{ 
	    sensor.CalculateRealTemperatureRun();  
	    sensor.CalculateRealPressure(); 
}
//--------------------------------------------------------------------
int Runsensor()
{ int sn;	
    // каждые 10 мс  
	sn = sensor.Run(); /*периодически запускаем считывание данных со всех АЦП */
    return sn;
}
/********************************************************************************
* запуск HART
 *********************************************************************************/
void  HART_start()
{
	serial_enable(true,false);  // разрешить прием
}

/********************************************************************************
* остановка HART
 *********************************************************************************/
void HART_stop()
{
	serial_enable(false,false); //запретить прием и передачу
}

/********************************************************************************
* возвращает значение температуры по первому каналу
 *********************************************************************************/
float Temperature1()
{
	return sensor.Temperature_1channel;
}

/********************************************************************************
* возвращает значение температуры по второму каналу
 *********************************************************************************/
float Temperature2()
{
	return sensor.Temperature_2channel;
}

/********************************************************************************
* возвращает значение входного напряжение датчика давления
 *********************************************************************************/
float PressIn()
{
	return sensor.Voltage_pressIn ;
}
float CodePIn()
{
	return sensor.ADC3in_evg_data - 32768.0;
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/********************************************************************************
* возвращает значение выходного напряжение датчика давления
 *********************************************************************************/
float PressOut()
{
	return sensor.Voltage_pressOut;
}
float CodePOut()
{
	return sensor.ADC3out_evg_data - 32768.0;
}

//unsigned cntADC1()
//{
//	return sensor.cntADC1;
//}

//unsigned cntADC2()
//{
//	return sensor.cntADC2;
//}

//unsigned cntADC3()
//{
//	return sensor.cntADC3;
//}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/